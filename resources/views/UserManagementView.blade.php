<!DOCTYPE html>
<html lang="en">
@include('layouts.Head')
<body>
  <div class="container-scroller">
    @include('layouts.Header')
    <div class="container-fluid page-body-wrapper">
      @include('layouts.Menu')
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="page-header row">
            <h3 class="page-title">
              User Management
            </h3>
          </div>
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb breadcrumb-custom">
              <li class="breadcrumb-item"><a href="#"><i class="fas fa-users-cog menu-icon"></i> Admin Tools</a></li>
              <li class="breadcrumb-item active" aria-current="page"><span><i class="fas fa-file-alt menu-icon"></i> User Management </span></li>
            </ol>
          </nav>
           <div class="col-md-12" id="UserMessage">
            @if(Session::has('message'))
              <div class="alert alrtDanger {{ Session::get('alert-class', 'alert-info') }}">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>{{ Session::get('message') }}</strong>
              </div>
            @endif
          </div>
          <div class="allDet">
            <div class="col-lg-12 col-md-12 col-sm-12 col-12 comP">
              <div class="row">
                <div class="col-md-3 styBord">
                  <div class="col">
                    <select class="form-control fonT boxH" id="Company">
                      <option value="All">All Company</option>
                     
                    </select>
                  </div>
                  <div class="col-md-12 form-group tadmPadd mb-0">
                    <div class="form-check form-check-flat form-check-primary adComp adComp-primary disPlayt">
                      <label class="form-check-label adComp-label sa">
                      <input type="checkbox" class="form-check-input" id="ShowInactive" 
                        onclick="GetCompanyUsingShwowInactiveAndTest();">
                        Show Inactive
                      </label>
                    </div>
                    <div class="form-check form-check-flat form-check-primary adComp adComp-primary disPlayt">
                      <label class="form-check-label adComp-label si">
                      <input type="checkbox" class="form-check-input" id="ShowTest"
                        onclick="GetCompanyUsingShwowInactiveAndTest();">
                        Show Test
                      </label>
                    </div>
                  </div>
                </div>
                <div class="col-md-2 form-group tadmPadd labFontt newFoGrp">
                  <input type="text" class="form-control txtPadd boxH" id="FirstName" placeholder="First Name">
                </div>
                <div class="col-md-2 form-group tadmPadd labFontt newFoGrp">
                  <input type="text" class="form-control txtPadd boxH" id="LastName" placeholder="Last Name">
                </div>
                <div class="col-md-3 form-group tadmPadd labFontt newFoGrp">
                  <input type="email" class="form-control boxH" id="Email" placeholder="Email">
                </div>
                <div class="col-md-2 form-group tadmPadd labFontt newFoGrp">
                  <input type="text" class="form-control txtPadd boxH" id="Phone"  placeholder="Telephone">
                </div> 
              </div>
              <div class="row">  
                <div class="col-md-3 newFoGrp">
                  <select class="form-control fonT" id="UserRole">
                    <option value="">All Role</option>
                    @foreach($UserRole as $Key=>$Value)
                    <option value="{{$Key}}">{{$Value}}</option>
                    @endforeach
                  </select>
                </div>
                <div class="col-md-3 newFoGrp">
                  <select class="form-control fonT" id="UserStatus" >
                    <option value="">All Status</option>
                    <option value="1">Active</option>
                    <option value="0">In-Active</option>
                  </select>
                </div> 
                <div class="col-md-2 col-6">
                   <button type="button" class="btn btn-success cbtnPadd sercFontt"
                   id="SearchBtn" onclick="SearchUserList();"> Search </button>
                </div>
                <div class="col-md-2 col-6 offset-md-2 ">
                 <!--  <a href="{{route('admin.UserAddView')}}" class="btn btn-success btn-fw cbtnPadd sercFontt fl dwSCSV Flo">Add New User</a> -->
                   <button type="button" class="btn btn-success btn-fw cbtnPadd sercFontt fl dwSCSV Flo"
                    onclick="AddUserModal();">Add New User</button>
                </div> 
              </div>
              <div id="dataTable_wrapper" class="dataTables_wrapper dt-bootstrap4">
                <input type="hidden" name="numofrecords" id="numofrecords" value='10'>
              </div>
            </div>
          </div>
       </div>
       @include('layouts.Footer')
    </div>
    </div>
  </div>
  <!-- Add Window -->
  <div class="modal fade" id="AddUserModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header mHBg">
          <h4 class="modal-title">Add New User</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <form action="<?php echo route('admin.AddUserDetails'); ?>" name="UserAddForm" id="UserAddForm" method="POST">
            <div class="row">
              <input type="hidden" name="_token" id="_token" value="<?php echo  csrf_token(); ?>">                
                 <div class="col-md-3 form-group tadmPadd labFontt">
                  <label for="exampleInputEmail1">Company Name:</label>
                    <select class="form-control fonT" id="CompanyIDAdd" name="CompanyID" onchange="HideErr('CompanyIDAddErr');">
                      <option value="">Select Company</option>
                      <?php foreach($CompanyList as $cl){ ?>
                      <option value="<?=$cl->CompanyID;?>"
                        ><?=$cl->CompanyName;?> (CID:<?=$cl->CompanyID;?></option>
                      <?php }?>
                    </select>
                    <span id="CompanyIDAddErr" class="errordisp"></span>
                 </div>                               
                  <div class="col-md-3 form-group tadmPadd labFontt">
                  <label for="exampleInputEmail1">User Role:</label>
                    <select class="form-control fonT" id="UserRoleAdd" name="UserRole" onchange="HideErr('UserRoleAddErr');">
                      <option value="">Select User Role</option>
                        <?php foreach($UserRole as $Key=>$Value){ ?>
                        <option value="<?=$Key;?>"
                        ><?=$Value;?></option>
                        <?php } ?>
                    </select>
                    <span id="UserRoleAddErr" class="errordisp"></span>
                 </div> 
                 <div class="col-md-3 form-group tadmPadd labFontt">
                    <label for="exampleInputEmail1">First Name:</label>
                    <input type="text" onkeypress="HideErr('UserFirstNameAddErr');" value="" id="UserFirstNameAdd" name="UserFirstName" class="form-control txtPadd" placeholder="First Name">
                    <span id="UserFirstNameAddErr" class="errordisp"></span>
                 </div>
                 <div class="col-md-3 form-group tadmPadd labFontt">
                    <label for="exampleInputEmail1">Last Name:</label>
                    <input type="text" onkeypress="HideErr('UserLastNameAddErr');" value="" id="UserLastNameAdd" name="UserLastName" class="form-control txtPadd" placeholder="Last Name">
                    <span id="UserLastNameAddErr" class="errordisp"></span>
                 </div>                              
              </div>
              <div class="row">
                 
                <div class="col-md-3 form-group tadmPadd labFontt">
                    <label for="exampleInputEmail1">Email:</label>
                    <input type="email" onkeypress="HideErr('UserEmailAddErr');" value="" id="UserEmailAdd" name="UserEmail" class="form-control camH" id="exampleInputEmail1" placeholder="Email">
                    <span id="UserEmailAddErr" class="errordisp"></span>
                 </div>
                 <div class="col-md-3 form-group tadmPadd labFontt">
                    <label for="exampleInputEmail1">Telephone:</label>
                    <input type="text" onkeypress="HideErr('UserTelAddErr');" value="" id="UserTelAdd" name="UserTel" class="form-control txtPadd" placeholder="Telephone">
                    <span id="UserTelAddErr" class="errordisp"></span>
                 </div>
                 <div class="col-md-3 form-group tadmPadd labFontt">
                    <label for="exampleInputEmail1">Skype ID:</label>
                    <input type="text" onkeypress="HideErr('UserSkypeAddErr');" value="" id="UserSkypeAdd" name="UserSkype" class="form-control txtPadd" placeholder="Skype ID">
                    <span id="UserSkypeAddErr" class="errordisp"></span>
                 </div> 
                  <div class="col-md-3 form-group tadmPadd labFontt">
                    <label for="exampleInputEmail1">User Status:</label>
                    <div class="onoffswitch4">
                      <input type="checkbox" name="UserStatus" class="onoffswitch4-checkbox" id="UserStatusAdd" 
                        checked>
                      <label class="onoffswitch4-label" for="UserStatusAdd">
                          <span class="onoffswitch4-inner"></span>
                          <span class="onoffswitch4-switch"></span>
                      </label>
                    </div>
                  </div> 
              </div>
              <div class="row" >                                               
                  <div class="col-md-2 form-group tadmPadd labFontt">
                    <label for="exampleInputEmail1">Show Specs:</label>
                    <div class="onoffswitch6">
                      <input type="checkbox" class="onoffswitch6-checkbox" 
                      id="ShowAPISpecsAdd" name="ShowAPISpecs" checked>
                      <label class="onoffswitch6-label" for="ShowAPISpecsAdd">
                          <span class="onoffswitch6-inner"></span>
                          <span class="onoffswitch6-switch"></span>
                      </label>
                    </div>
                  </div>                                
                  <div class="col-md-2 form-group tadmPadd labFontt">
                    <label for="exampleInputEmail1">Show Affiliate Links:</label>
                    <div class="onoffswitch7">
                      <input type="checkbox" class="onoffswitch7-checkbox" 
                      id="ShowAffLinksAdd" name="ShowAffLinks" checked>
                      <label class="onoffswitch7-label" for="ShowAffLinksAdd">
                          <span class="onoffswitch7-inner"></span>
                          <span class="onoffswitch7-switch"></span>
                      </label>
                    </div>
                  </div> 
                  <div class="col-md-3 form-group tadmPadd labFontt" >
                    <label for="exampleInputPassword1">Password:</label>
                    <input type="text" onkeypress="HideErr('PasswordAddErr');" class="form-control camH" id="PasswordAdd" name="Password" placeholder="Password">
                    <span id="PasswordAddErr" class="errordisp"></span>
                 </div>
                 <div class="col-md-3 form-group tadmPadd labFontt" >
                    <label for="exampleInputPassword1">Confirm Password:</label>
                    <input type="text" onkeypress="HideErr('ConfirmPasswordAddErr');" class="form-control camH" id="ConfirmPasswordAdd" name="ConfirmPassword" placeholder="Confirm Password">
                    <span id="ConfirmPasswordAddErr" class="errordisp"></span>
                 </div>
                 <div class="col-md-2 form-group tadmPadd labFontt"  >
                    <label class="gP" for="exampleInputPassword1">Generate Password:</label>
                    <button type="button" onclick="GeneratePasswordAdd();" class="btn btn-success fonT noBoradi cbtnPadd sercFontt gpBg">Generate Password</button>
                 </div>                                                          
              </div>
               <div class="row" >
                <div class="col-md-6 form-group mb-0 labFontt">
                    <label for="exampleFormControlTextarea1">User Notes:</label>
                    <textarea class="form-control" id="UserNotesAdd" name="UserNotes" rows="3"></textarea>
                  </div>
                  <div class="col-md-6 form-group tadmPadd labFonsize">
                    <div class="form-check form-check-flat form-check-primary adComp adComp-primary mt-4">
                       <label class="form-check-label adComp-label labLh">
                       <input type="checkbox" class="form-check-input" id="SendCredentials" name="SendCredentials">
                        Would you like to email credentials to the user?
                       </label>
                    </div>
                  </div>            
              </div>
              <div class="row btnFlo">
                 <button type="button" id="UserSaveBtn" class="btn btn-success btn-fw noBoradi mr-2" onclick="AddUserDetails();" >Save</button>
                 <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
              </div>
           </form>
        </div>
      </div>        
    </div>
  </div> 
  <!-- Edit Window -->
  <div class="modal fade" id="EditUserModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header mHBg">
          <h4 class="modal-title">Edit New User</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body" id="EditUserDiv">
          
        </div>
        <div class="modal-footer">

        </div>
      </div>        
    </div>
  </div>
  <input type="hidden" name="url" id="url" value='{{URL::to("/")}}'>
  <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
  <input type="hidden" name="loading" id="loading" value='{{ asset("Admin/images/loading.gif") }}'>
  @include('layouts.Script')
  <script src="{{asset('Ajax/UserManagement.js')}}"></script> 
  <script>
  $(document).ready(function() {
    $('#sandbox-container .input-daterange').datepicker({});

    $('#datepicker').datepicker({
      weekStart: 0,
      daysOfWeekHighlighted: "6,0",
      autoclose: true,
      todayHighlight: true,
      dateFormat: 'mm/dd/yy'
    });
    $('#datepicker').datepicker("setDate", new Date());
  });
  </script>
</body>
