<!DOCTYPE html>
<html lang="en">
@include('layouts.Head')
<body>
  <div class="container-scroller">
    @include('layouts.Header')
    <div class="container-fluid page-body-wrapper">
      @include('layouts.Menu')
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="page-header row">
            <div class="col-md-6 col-sm-6">
              <h3 class="page-title">
                <i class="fa fa-home menu-icon"></i> Dashboard
              </h3>
            </div>
          </div>
          <?php
          if($HasVendor>0)
          {
            $SumTotal       = 0;
            $SumAccepted    = 0;
            $SumRejected    = 0;
            $SumTotalRev    = 0;
            foreach($DashboardVendorReport as $Key=>$Value) 
            { 
              $ReturnedLead = 0;
              if($Value->Returned > 0)
              $ReturnedLead = $Value->Returned;
              
              $Total = 0;
              if($Value->Total > 0)
              $Total = $Value->Total;
              
              $Accepted = 0;
              if($Value->Accepted > 0)
              $Accepted = $Value->Accepted;
              
              $Rejected = ($Total - $Accepted) - $ReturnedLead;
              
              $TotalRev = 0;
              if($Value->TotalVendorPrice > 0)
              $TotalRev = $Value->TotalVendorPrice;
              

              $SumTotal       = $SumTotal+$Total;
              $SumAccepted    = $SumAccepted+$Accepted;
              $SumRejected    = $SumRejected+$Rejected;
              $SumTotalRev    = $SumTotalRev+$TotalRev;

            }
            ?>
            <div class="row grid-margin">
              <div class="col-12">
                <div class="card card-statistics newBgclr">
                  <div class="card-body newCardBody">
                    <div class="d-flex flex-column flex-md-row align-items-center justify-content-between">
                        <div class="col-md-3 col-12 statistics-item stMobpadd">
                          <p>
                            <i class="icon-sm fas fa-user mr-2"></i>
                            Total Leads In
                          </p>
                          <h2><?php echo $SumTotal; ?></h2>
                        </div>
                        <div class="col-md-3 col-12 statistics-item stMobpadd">
                          <p>
                            <i class="icon-sm fas fa-thumbs-up mr-2"></i>
                            Sold Leads
                          </p>
                          <h2><?php echo $SumAccepted; ?></h2>
                        </div>
                        <div class="col-md-3 col-12 statistics-item stMobpadd">
                          <p>
                            <i class="icon-sm fas fa-ban mr-2"></i>
                            Rejected Leads
                          </p>
                          <h2><?php echo $SumRejected; ?></h2>                       
                        </div>
                        <div class="col-md-3 col-12 statistics-item stMobpadd">
                          <p class="allp">
                            <i class="icon-sm fas fa-chart-line mr-2"></i>
                            Total Revenue
                          </p>
                          <h2>$<?php echo number_format($SumTotalRev,2); ?></h2>
                        </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          <?php
          } 
          ?>

          <?php
          if($HasBuyer>0)
          {
            $SumTotal       = 0;
            $SumAccepted    = 0;
            $SumRejected    = 0;
            $SumTotalCost    = 0;
            foreach($DashboardBuyerReport as $Key=>$Value) 
            { 
              $ReturnedLead = 0;
              if($Value->Returned > 0)
              $ReturnedLead = $Value->Returned;
              
              $Total = 0;
              if($Value->Total > 0)
              $Total = $Value->Total;
              
              $Accepted = 0;
              if($Value->Accepted > 0)
              $Accepted = $Value->Accepted;
              
              $Rejected = ($Total - $Accepted) - $ReturnedLead;
              
              $TotalCost = 0;
              if($Value->TotalBuyerPrice > 0)
              $TotalCost = $Value->TotalBuyerPrice;
              

              $SumTotal       = $SumTotal+$Total;
              $SumAccepted    = $SumAccepted+$Accepted;
              $SumRejected    = $SumRejected+$Rejected;
              $SumTotalCost   = $SumTotalCost+$TotalCost;

            }
            ?>
            <div class="row grid-margin">
              <div class="col-12">
                <div class="card card-statistics newBgclr">
                  <div class="card-body newCardBody">
                    <div class="d-flex flex-column flex-md-row align-items-center justify-content-between">
                        <div class="col-md-3 col-12 statistics-item stMobpadd">
                          <p>
                            <i class="icon-sm fas fa-user mr-2"></i>
                            Total Leads Out
                          </p>
                          <h2><?php echo $SumTotal; ?></h2>
                        </div>
                        <div class="col-md-3 col-12 statistics-item stMobpadd">
                          <p>
                            <i class="icon-sm fas fa-thumbs-up mr-2"></i>
                            Purchased Leads
                          </p>
                          <h2><?php echo $SumAccepted; ?></h2>
                        </div>
                        <div class="col-md-3 col-12 statistics-item stMobpadd">
                          <p>
                            <i class="icon-sm fas fa-ban mr-2"></i>
                            Rejected Leads
                          </p>
                          <h2><?php echo $SumRejected; ?></h2>                       
                        </div>
                        <div class="col-md-3 col-12 statistics-item stMobpadd">
                          <p class="allp">
                            <i class="icon-sm fas fa-chart-line mr-2"></i>
                            Total Cost
                          </p>
                          <h2>$<?php echo number_format($SumTotalCost,2); ?></h2>
                        </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          <?php
          } 
          ?>

          <div class="row grid-margin">
              <?php 
              $DivCounter = 1;
              $CampaignListArray=array();
              ///Vendor
              $TotalLeadVendorArray=array();
              $AcceptedLeadVendorArray=array();
              $RejectedLeadVendorArray=array();
              $TotalRevenueVendorArray=array();
              ///Buyer
              $TotalLeadBuyerArray=array();
              $AcceptedLeadBuyerArray=array();
              $RejectedLeadBuyerArray=array();
              $TotalCostBuyerArray=array();

              $ListCSS = 'frstUl';
              $HasVendorCSS = 'secUl';
              $HasBuyerCSS = 'thrdUl';
              if($HasVendor>0 && $HasBuyer>0)
              {
                $ListCSS = 'frstUl';
                $HasVendorCSS = 'secUl';
                $HasBuyerCSS = 'thrdUl';
              }
              else if($HasVendor<=0 && $HasBuyer>0)
              {
                $ListCSS = 'frstUlRight';
                $HasVendorCSS = 'secUl';
                $HasBuyerCSS = 'thrdUlRight';
              }
              else if($HasVendor>0 && $HasBuyer<=0)
              {
                $ListCSS = 'frstUlRight';
                $HasVendorCSS = 'thrdUlRight';
                $HasBuyerCSS = 'thrdUl';
              }

              foreach($CampaignList as $cl) 
              {     
                $CampaignName = $cl->CampaignName;
                array_push($CampaignListArray, $cl->CampaignName);          
                ?>
                <div class="col-md-4 col-12 allCamMobPadd">
                  <div class="paydayBox">
                    <div class="paydayHead">
                      <h5 class="text-left"><?php echo $cl->CampaignName; ?></h5>
                    </div>
                    
                    <div class="paydaySold">
                      <?php if($HasVendor>0){ ?>
                      <h5 class="text-left">Sold</h5>
                      <?php } ?>
                    </div>
                    
                    
                    <div class="paydayPurchased">
                      <?php if($HasBuyer>0){ ?>
                      <h5 class="text-left">Purchased</h5>
                      <?php } ?>
                    </div>  
                                      
                    <div class="paydayList">
                      <ul class="<?php echo $ListCSS; ?>">
                        <li>Total Leads</li>
                        <li>Accepted Leads</li>
                        <li>Rejected Leads</li>
                        <li>Total Cost</li>
                        <li>Total Revenue</li>
                      </ul>
                      <?php 
                      if($HasVendor>0)
                      {
                        $CampaignID = $cl->CampaignID;                        
                        if(array_key_exists($CampaignID,$DashboardVendorReport))
                        {
                          $ReturnedLeadVendor = 0;
                          if($DashboardVendorReport[$CampaignID]->Returned > 0)
                          $ReturnedLeadVendor = $DashboardVendorReport[$CampaignID]->Returned;

                          $TotalLeadVendor = 0;
                          if($DashboardVendorReport[$CampaignID]->Total > 0)
                          $TotalLeadVendor = $DashboardVendorReport[$CampaignID]->Total;
                          
                          $AcceptedLeadVendor = 0;
                          if($DashboardVendorReport[$CampaignID]->Accepted > 0)
                          $AcceptedLeadVendor = $DashboardVendorReport[$CampaignID]->Accepted;
                          
                          $RejectedLeadVendor = ($TotalLeadVendor - $AcceptedLeadVendor) - $ReturnedLeadVendor;

                          $TotalRevVendor = 0;
                          if($DashboardVendorReport[$CampaignID]->TotalVendorPrice > 0)
                          $TotalRevVendor = $DashboardVendorReport[$CampaignID]->TotalVendorPrice;


                          array_push($TotalLeadVendorArray, $TotalLeadVendor);
                          array_push($AcceptedLeadVendorArray, $AcceptedLeadVendor);
                          array_push($RejectedLeadVendorArray, $RejectedLeadVendor);
                          array_push($TotalRevenueVendorArray, $TotalRevVendor);
                        }
                      ?>
                      <ul class="<?php echo $HasVendorCSS; ?>" >
                        <li><a class="sLink" href="javascript:void(0);" onclick="RedirectToLead('<?php echo $CampaignName;?>','All','Sold');" ><?php echo $TotalLeadVendor; ?></a></li>
                        <li><a class="sLink" href="javascript:void(0);" onclick="RedirectToLead('<?php echo $CampaignName;?>','0_1','Sold');"><?php echo $AcceptedLeadVendor; ?></a></li>
                        <li><a class="sLink" href="javascript:void(0);" onclick="RedirectToLead('<?php echo $CampaignName;?>','0_0','Sold');"><?php echo $RejectedLeadVendor; ?></a></li>
                        <li>$0.00</li>
                        <li>$<?php echo number_format($TotalRevVendor,2); ?></li>
                      </ul>
                      <?php }?>
                      <?php 
                      if($HasBuyer>0)
                      {
                        $CampaignID = $cl->CampaignID;                        
                        if(array_key_exists($CampaignID,$DashboardBuyerReport))
                        {
                          $ReturnedLeadBuyer = 0;
                          if($DashboardBuyerReport[$CampaignID]->Returned > 0)
                          $ReturnedLeadBuyer = $DashboardBuyerReport[$CampaignID]->Returned;

                          $TotalLeadBuyer = 0;
                          if($DashboardBuyerReport[$CampaignID]->Total > 0)
                          $TotalLeadBuyer = $DashboardBuyerReport[$CampaignID]->Total;
                          
                          $AcceptedLeadBuyer = 0;
                          if($DashboardBuyerReport[$CampaignID]->Accepted > 0)
                          $AcceptedLeadBuyer = $DashboardBuyerReport[$CampaignID]->Accepted;
                          
                          $RejectedLeadBuyer = ($TotalLeadBuyer - $AcceptedLeadBuyer) - $ReturnedLeadBuyer;

                          $TotalCostBuyer = 0;
                          if($DashboardBuyerReport[$CampaignID]->TotalBuyerPrice > 0)
                          $TotalCostBuyer = $DashboardBuyerReport[$CampaignID]->TotalBuyerPrice;

                          array_push($TotalLeadBuyerArray, $TotalLeadBuyer);
                          array_push($AcceptedLeadBuyerArray, $AcceptedLeadBuyer);
                          array_push($RejectedLeadBuyerArray, $RejectedLeadBuyer);
                          array_push($TotalCostBuyerArray, $TotalCostBuyer);
                        }
                      ?>
                      <ul class="<?php echo $HasBuyerCSS; ?>" >
                        <li><a class="fLink" href="javascript:void(0);" onclick="RedirectToLead('<?php echo $CampaignName;?>','All','Purchased');" ><?php echo $TotalLeadBuyer; ?></a></li>
                        <li><a class="fLink" href="javascript:void(0);" onclick="RedirectToLead('<?php echo $CampaignName;?>','0_1','Purchased');" ><?php echo $AcceptedLeadBuyer; ?></a></li>
                        <li><a class="fLink" href="javascript:void(0);" onclick="RedirectToLead('<?php echo $CampaignName;?>','0_0','Purchased');" ><?php echo $RejectedLeadBuyer; ?></a></li>
                        <li>$<?php echo number_format($TotalCostBuyer,2); ?></li>
                        <li>$0.00</li>
                      </ul>
                      <?php }?>                      
                    </div>
                  </div>
                </div>
              <?php 
                if($DivCounter==3){
                  echo '</div>';
                  echo '<div class="row grid-margin">';
                  $DivCounter = 0;
                }
                $DivCounter++;
              }
              ?>
          </div>
          <?php 
          if($HasVendor>0)
          {
          ?>
         <!--  <div class="row mt-4">
            <div class="col-lg-6 grid-margin stretch-card">
              <div class="card graphCard">
                <div class="card-body">
                  <h4 class="card-title">Vendor Lead Data Graph</h4>
                  <div class="clrBox">
                    <ul>
                      <li class="red"></li>
                      <li>Total Leads In</li>
                      <li class="green"></li>
                      <li>Accepted Lead</li>
                      <li class="blue"></li>
                      <li>Rejected Lead</li>
                    </ul>
                  </div>
                  <div class="ct-chart ct-perfect-fourth" id="ct-chart-horizontal-bar-Vendor"></div>
                </div>
              </div>
            </div>
            <div class="col-lg-6 grid-margin stretch-card">
              <div class="card graphCard">
                <div class="card-body">
                  <h4 class="card-title">Vendor Revenue Data Graph</h4>
                   <div class="clrBox">
                    <ul>
                      <li class="blue"></li>
                      <li>Total Revenue</li>
                    </ul>
                  </div>
                  <div class="ct-chart ct-perfect-fourth" id="ct-chart-horizontal-bar1-Vendor"></div>
                </div>
              </div>
            </div>
          </div> -->
          <?php 
          }
          ?>
          <?php 
          if($HasBuyer>0)
          {
          ?>
         <!--  <div class="row mt-4">
            <div class="col-lg-6 grid-margin stretch-card">
              <div class="card graphCard">
                <div class="card-body">
                  <h4 class="card-title">Buyer Lead Data Graph</h4>
                  <div class="clrBox">
                    <ul>
                      <li class="red"></li>
                      <li>Total Leads Out</li>
                      <li class="green"></li>
                      <li>Sold Lead</li>
                      <li class="blue"></li>
                      <li>Rejected Lead</li>
                    </ul>
                  </div>
                  <div class="ct-chart ct-perfect-fourth" id="ct-chart-horizontal-bar-Buyer"></div>
                </div>
              </div>
            </div>
            <div class="col-lg-6 grid-margin stretch-card">
              <div class="card graphCard">
                <div class="card-body">
                  <h4 class="card-title">Buyer Cost Data Graph</h4>
                   <div class="clrBox">
                    <ul>
                      <li class="blue"></li>
                      <li>Total Cost</li>
                    </ul>
                  </div>
                  <div class="ct-chart ct-perfect-fourth" id="ct-chart-horizontal-bar1-Buyer"></div>
                </div>
              </div>
            </div>
          </div> -->
          <?php 
          }
          ?>
          
        </div>
        @include('layouts.Footer')
      </div>
    </div>
  </div>
  <input type="hidden" name="url" id="url" value='{{URL::to("/")}}'>
  <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
  <input type="hidden" name="loading" id="loading" value='{{ asset("Admin/images/loading_image.gif") }}'>
  @include('layouts.Script')
  <script src="{{asset('Ajax/Dashboard.js')}}"></script>
  <script type="text/javascript" language="javascript">
    var CampaignArray = new Array();
    <?php foreach($CampaignListArray as $key => $val){ ?>
        CampaignArray.push('<?php echo $val; ?>');
    <?php } ?>   
  </script>
  <script type="text/javascript" language="javascript">
    var TotalLeadVendorArray = new Array();
    <?php foreach($TotalLeadVendorArray as $key => $val){ ?>
        TotalLeadVendorArray.push('<?php echo $val; ?>');
    <?php } ?>
    var AcceptedLeadVendorArray = new Array();
    <?php foreach($AcceptedLeadVendorArray as $key => $val){ ?>
        AcceptedLeadVendorArray.push('<?php echo $val; ?>');
    <?php } ?>
    var RejectedLeadVendorArray = new Array();
    <?php foreach($RejectedLeadVendorArray as $key => $val){ ?>
        RejectedLeadVendorArray.push('<?php echo $val; ?>');
    <?php } ?>
    var TotalRevenueVendorArray = new Array();
    <?php foreach($TotalRevenueVendorArray as $key => $val){ ?>
        TotalRevenueVendorArray.push('<?php echo $val; ?>');
    <?php } ?>    
  </script>
  <script type="text/javascript" language="javascript">
    var TotalLeadBuyerArray = new Array();
    <?php foreach($TotalLeadBuyerArray as $key => $val){ ?>
        TotalLeadBuyerArray.push('<?php echo $val; ?>');
    <?php } ?>
    var AcceptedLeadBuyerArray = new Array();
    <?php foreach($AcceptedLeadBuyerArray as $key => $val){ ?>
        AcceptedLeadBuyerArray.push('<?php echo $val; ?>');
    <?php } ?>
    var RejectedLeadBuyerArray = new Array();
    <?php foreach($RejectedLeadBuyerArray as $key => $val){ ?>
        RejectedLeadBuyerArray.push('<?php echo $val; ?>');
    <?php } ?>
    var TotalCostBuyerArray = new Array();
    <?php foreach($TotalCostBuyerArray as $key => $val){ ?>
        TotalCostBuyerArray.push('<?php echo $val; ?>');
    <?php } ?>    
  </script>
  <script type="text/javascript">
    $(document).ready(function () {
        var currenttime = '<? print date("F d, Y H:i:s", time())?>' 

        var serverdate = new Date(currenttime)

        function padlength(what) {
            var output = (what.toString().length == 1) ? "0" + what : what
            return output
        }

        function displaytime() {
            serverdate.setSeconds(serverdate.getSeconds() + 1)
            var timestring = padlength(serverdate.getHours()) + ":" + padlength(serverdate.getMinutes()) + ":" + padlength(serverdate.getSeconds())
            document.getElementById("time").innerHTML = timestring
        }


        window.onload = function () {
            setInterval(displaytime, 1000)
        }
    });

     //Vendor Lead Data Graph
    if ($('#ct-chart-horizontal-bar-Vendor').length) {
      new Chartist.Bar('#ct-chart-horizontal-bar-Vendor', {
        labels: CampaignArray,
        series: [{
            "name": "Total Lead In",
            "data": TotalLeadVendorArray
          },
          {
            "name": "Accepted Lead",
            "data": AcceptedLeadVendorArray
          },
          {
            "name": "Rejected Lead",
            "data": RejectedLeadVendorArray
          }
        ]
      }, {
        seriesBarDistance: 10,
        reverseData: true,
        horizontalBars: false,
        height: '350px',
        fullWidth: true,
        chartPadding: {
          top: 30,
          left: 0,
          right: 0,
          bottom: 0
        }
      });
    }


    //Vendor Revenue Data Graph    
    if ($('#ct-chart-horizontal-bar1-Vendor').length) {
      new Chartist.Bar('#ct-chart-horizontal-bar1-Vendor', {
        labels: CampaignArray,
        series: [{
            "name": "Total Revenue",
            "data": TotalRevenueVendorArray
          }
        ]
      }, {
        seriesBarDistance: 10,
        reverseData: true,
        horizontalBars: false,
        height: '350px',
        fullWidth: true,
        chartPadding: {
          top: 30,
          left: 0,
          right: 0,
          bottom: 0
        }
      });
    }


    //Buyer Lead Data Graph
    if ($('#ct-chart-horizontal-bar-Buyer').length) {
      new Chartist.Bar('#ct-chart-horizontal-bar-Buyer', {
        labels: CampaignArray,
        series: [{
            "name": "Total Lead In",
            "data": TotalLeadBuyerArray
          },
          {
            "name": "Sold Lead",
            "data": AcceptedLeadBuyerArray
          },
          {
            "name": "Rejected Lead",
            "data": RejectedLeadBuyerArray
          }
        ]
      }, {
        seriesBarDistance: 10,
        reverseData: true,
        horizontalBars: false,
        height: '350px',
        fullWidth: true,
        chartPadding: {
          top: 30,
          left: 0,
          right: 0,
          bottom: 0
        }
      });
    }


    //Buyer Revenue Data Graph   
    if ($('#ct-chart-horizontal-bar1-Buyer').length) {
      new Chartist.Bar('#ct-chart-horizontal-bar1-Buyer', {
        labels: CampaignArray,
        series: [{
            "name": "Total Cost",
            "data": TotalCostBuyerArray
          }
        ]
      }, {
        seriesBarDistance: 10,
        reverseData: true,
        horizontalBars: false,
        height: '350px',
        fullWidth: true,
        chartPadding: {
          top: 30,
          left: 0,
          right: 0,
          bottom: 0
        }
      });
    }
</script>
</body>
