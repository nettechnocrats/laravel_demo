<!DOCTYPE html>
<html lang="en">
@include('layouts.Head')
<body>
  <div class="container-scroller">
    @include('layouts.Header')
    <div class="container-fluid page-body-wrapper">
      @include('layouts.Menu')
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="page-header row">
            <h3 class="page-title">
              View Lead Report Summary
            </h3>
          </div>
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb breadcrumb-custom">
              <li class="breadcrumb-item"><a href="#"><i class="fas fa-users-cog menu-icon"></i> Report</a></li>
              <li class="breadcrumb-item"><a href="#"><i class="fas fa-file-alt menu-icon"></i> Lead Report</a></li>
              <li class="breadcrumb-item active" aria-current="page"><span class="brFont"><i class="fas fa-file-alt menu-icon"></i> View Lead Report Summary</span></li>
            </ol>
          </nav>
          <?php if($CampaignDetails->CampaignType=='DirectPost'){ ?>
          <div class="allDet">
             <div class="col-lg-12 col-md-12 col-sm-12 col-12 comP">
                <form class="boxS">
                   <div class="row">
                      <div class="col-md-12 grid-margin stretch-card mt-4">
                         <div class="card">
                            <div class="card-body pingPotab">
                               <div class="tab-content noTabbord pingTcon">
                                  <div class="tab-pane fade show active" id="ping" role="tabpanel" aria-labelledby="ping-tab">
                                     <div class="media">
                                        <div class="media-body">                                           
                                            <div class="row mt-5">
                                              <div class="col-md-4 leadInfo">
                                                <h5>Inbound Lead Info</h5>
                                                <ul>
                                                    <li><strong>Lead ID:</strong>  <?php echo $LeadInboundDetails->LeadID ?></li>
                                                    <li><strong>VendorID:</strong> <?php echo $LeadInboundDetails->AdminLabel.' ('.$LeadInboundDetails->VendorID.')'; ?></li>
                                                    <li><strong>Vendor Price:</strong> $<?php echo number_format($LeadInboundDetails->VendorPrice, 2, '.', ''); ?></li>                                                   
                                                    <li><strong>DirectPost Response:</strong> <?php echo $LeadInboundDetails->DirectPostResponse ?></li>
                                                    <li><strong>DirectPost Status:</strong><?php echo ($LeadInboundDetails->DirectPostStatus == '1') ? 'Accepted' : 'Rejected'; ?></li>
                                                    <li><strong>DirectPost Date:</strong> <?php echo $LeadInboundDetails->DirectPostDate ?></li>
                                                    <li><strong>Test Mode:</strong> <?php echo $LeadInboundDetails->TestMode ?></li>
                                                    <li><strong>Total Time:</strong> <?php echo number_format($TotalTime->DirectPostTime, 3); ?> secs</li>
                                                </ul>
                                              </div>
                                              <div class="col-md-4 leadInfo">
                                                <h5>
                                                   Vendor DirectPost Data
                                                  </h5>
                                                  <?php
                                                    $VendorPostData = $LeadInboundDetails->DirectPostData;
                                                     
                                                    if(!empty($PostedData))
                                                    {
                                                        $VendorPostData = str_replace($PostedData['SSN'], "*********", $VendorPostData);
                                                        $VendorPostData = str_replace($PostedData['BankRoutingNumber'], "**********", $VendorPostData);
                                                        $VendorPostData = str_replace($PostedData['BankAccountNumber'], "**********", $VendorPostData); 
                                                    }
                                                  ?>
                                                <textarea rows="20" cols="49"><?php echo $VendorPostData; ?></textarea>
                                              </div>
                                              <div class="col-md-4 leadInfo">
                                                <h5>
                                                  Vendor DirectPost Response
                                                </h5>
                                                <?php 
                                                   $VendorResponseData = $LeadInboundDetails->DirectPostResponseData;
                                                ?>
                                                <textarea rows="20" cols="49"><?php echo $VendorResponseData; ?></textarea>
                                              </div>
                                            </div>
                                            <?php foreach($BuyerAttemptedList as $key=>$value){ 
                                              $DivColor="style='background-color:#efadad;'";
                                              if($value->DirectPostStatus == '1')
                                              {
                                                $DivColor="style='background-color:#bcd8bc;'";
                                              }

                                              ?>
                                            <div class="row mt-5" <?php echo $DivColor; ?>>
                                                <div class="col-md-4 leadInfo">
                                                  <h5>Outbound Lead Info</h5>
                                                  <ul>
                                                     <li><strong>OutBound ID:</strong><?php echo $value->ID ?></li>
                                                      <li><strong>Lead ID:</strong><?php echo $value->LeadID ?></li>                                                    
                                                      <li><strong>Buyer ID:</strong> <?php echo $value->AdminLabel; ?> (<?php echo $value->BuyerID; ?>)</li>
                                                      <li><strong>Tier ID:</strong> <?php echo $value->BuyerTierID ?> </li>
                                                      <li><strong>Buyer Price:</strong>$<?php echo number_format($value->DirectPostPrice, 2, '.', ''); ?> </li>                                                   
                                                      <li><strong>DirectPost Response:</strong> <?php echo $value->DirectPostResponse ?> </li>
                                                      <li><strong>DirectPost Status:</strong><?php echo ($value->DirectPostStatus == '1') ? 'Accepted' : 'Rejected'; ?></li>
                                                      <li><strong>DirectPost Time:</strong> <?php echo $value->DirectPostTime ?></li>
                                                      <li><strong>RedirectURL:</strong><?php echo $value->RedirectURL ?> </li>
                                                      <li><strong>IsRedirected:</strong> <?php echo ($value->IsRedirected == '1' && $value->DirectPostStatus == '1') ? 'Yes' : 'No' ?></li>
                                                      <li><strong>RedirectedTime:</strong> <?php echo $value->IsRedirectedTime ?></li>
                                                  </ul>
                                                </div>
                                                <div class="col-md-4 leadInfo">
                                                    <h5>
                                                    Buyer DirectPost Data
                                                    </h5>
                                                    <?php 
                                                    $BuyerDirectPostData = $value->DirectPostData;                                             
                                                    ?>
                                                  <textarea rows="20" cols="49"><?php echo $BuyerDirectPostData; ?></textarea>
                                                </div>
                                                <div class="col-md-4 leadInfo">
                                                  <h5>
                                                    Buyer DirectPost Response
                                                  </h5>
                                                  <textarea rows="20" cols="49"><?php echo $value->DirectPostResponseData; ?></textarea>
                                                </div>
                                            </div>
                                            <?php }?>
                                        </div>
                                     </div>
                                  </div>
                               </div>
                            </div>
                         </div>
                      </div>
                   </div>
                </form>
             </div>
          </div>
          <?php } else{ ?>

          <div class="allDet">
           <div class="col-lg-12 col-md-12 col-sm-12 col-12 comP">
              <form class="boxS">
                 <div class="row">
                    <div class="col-md-12 grid-margin stretch-card mt-4">
                       <div class="card">
                          <div class="card-body pingPotab">
                             <ul class="nav nav-tabs noTabbord" role="tablist">
                                <li class="nav-item">
                                   <a class="nav-link company-link active" id="ping-tab" data-toggle="tab" href="#ping" role="tab" aria-controls="home-1" aria-selected="true">Ping</a>
                                </li>
                                <li class="nav-item">
                                   <a class="nav-link company-link" id="post-tab" data-toggle="tab" href="#post" role="tab" aria-controls="profile-1" aria-selected="false">Post</a>
                                </li>
                             </ul>
                             <div class="tab-content noTabbord pingTcon">
                                <div class="tab-pane fade show active" id="ping" role="tabpanel" aria-labelledby="ping-tab">
                                   <div class="media">
                                      <div class="media-body">
                                          <div class="row">
                                            <div class="col-md-4 leadInfo">
                                               <h5>Inbound Lead Info</h5>
                                               <ul>
                                                    <li><strong>Lead ID:</strong>  <?php echo $LeadInboundDetails->LeadID ?></li>
                                                    <li><strong>VendorID:</strong> <?php echo $LeadInboundDetails->AdminLabel.' ('.$LeadInboundDetails->VendorID.')'; ?></li>
                                                    <li><strong>Vendor Price:</strong> $<?php echo number_format($LeadInboundDetails->VendorPrice, 2, '.', ''); ?></li>                                                   
                                                    <li><strong>Ping Response:</strong> <?php echo $LeadInboundDetails->PingResponse ?></li>
                                                    <li><strong>Ping Status:</strong> <?php echo ($LeadInboundDetails->PingStatus == '1') ? 'Accepted' : 'Rejected'; ?></li>
                                                    <li><strong>Ping Date:</strong> <?php echo $LeadInboundDetails->PingDate ?></li>
                                                    <li><strong>Test Mode:</strong> <?php echo $LeadInboundDetails->TestMode ?></li>
                                                    <li><strong>Total Time:</strong> <?php echo number_format($TotalTime->PingTime, 3); ?> secs </li>
                                               </ul>
                                            </div>
                                            <div class="col-md-4 leadInfo">
                                               <h5>Vendor Ping Data</h5>
                                                <?php
                                                  $VendorPingData = $LeadInboundDetails->PingData;
                                                  
                                                ?>
                                               <textarea rows="20" cols="49"><?php echo $VendorPingData; ?></textarea>
                                            </div>
                                            <div class="col-md-4 leadInfo">
                                               <h5>Vendor Ping Response Data</h5>
                                                <?php
                                                  $VendorPingResponseData = $LeadInboundDetails->PingResponseData;
                                                  
                                                ?>
                                               <textarea rows="20" cols="49"><?php echo $VendorPingResponseData; ?></textarea>
                                            </div>
                                          </div>

                                          <?php foreach($BuyerAttemptedList as $key=>$value){ 

                                              $DivColor="style='background-color:#efadad;'";
                                              if($value->PingStatus == '1')
                                              {
                                                $DivColor="style='background-color:#bcd8bc;'";
                                              }
                                              ?>
                                          <div class="row" <?php echo $DivColor; ?>>
                                              <div class="col-md-4 leadInfo">
                                                 <h5>Outbound Lead Info</h5>
                                                 <ul>
                                                      <li><strong>OutBound ID:</strong><?php echo $value->ID ?></li>
                                                      <li><strong>Lead ID:</strong><?php echo $value->LeadID ?></li>
                                                      <li><strong>Buyer ID:</strong><?php echo $value->AdminLabel; ?> (<?php echo $value->BuyerID; ?>)</li>
                                                      <li><strong>Buyer Price:</strong> $<?php echo number_format($value->PingPrice, 2, '.', ''); ?></li>                                                   
                                                      <li><strong>Ping Response:</strong> <?php echo $value->PingResponse ?></li>
                                                      <li><strong>Ping Status:</strong> <?php echo ($value->PingStatus == '1') ? 'Accepted' : 'Rejected'; ?></li>
                                                      <li><strong>Ping Time:</strong> <?php echo $value->PingTime ?></li>
                                                      <li><strong>Ping Date:</strong> <?php echo $value->PingDate ?></li>
                                                 </ul>
                                              </div>
                                              <div class="col-md-4 leadInfo">
                                                  <h5>Buyer Ping Data</h5>
                                                  <?php 
                                                  $BuyerPingData = $value->PingData;
                                                  ?>
                                                 <textarea rows="20" cols="49"><?php echo $BuyerPingData; ?></textarea>
                                              </div>
                                              <div class="col-md-4 leadInfo">
                                                 <h5>Buyer Ping Response Data</h5>
                                                 <textarea rows="20" cols="49"><?php echo $value->PingResponseData; ?></textarea>
                                              </div>
                                          </div>
                                          <?php }?>
                                      </div>
                                   </div>
                                </div>
                                <div class="tab-pane fade" id="post" role="tabpanel" aria-labelledby="post-tab">
                                   <div class="media">
                                      <div class="media-body">
                                          <div class="row">
                                            <div class="col-md-4 leadInfo">
                                               <h5>Inbound Lead Info</h5>
                                               <ul>
                                                  <li><strong>Lead ID:</strong>  <?php echo $LeadInboundDetails->LeadID ?></li>
                                                  <li><strong>VendorID:</strong> <?php echo $LeadInboundDetails->AdminLabel.' ('.$LeadInboundDetails->VendorID.')'; ?></li>
                                                  <li><strong>Vendor Price:</strong> $<?php echo number_format($LeadInboundDetails->VendorPrice, 2, '.', ''); ?></li>                                                   
                                                  <li><strong>Post Response:</strong> <?php echo $LeadInboundDetails->PostResponse ?></li>
                                                  <li><strong>Post Status:</strong> <?php echo ($LeadInboundDetails->PostStatus == '1') ? 'Accepted' : 'Rejected'; ?></li>
                                                  <li><strong>Post Date:</strong> <?php echo $LeadInboundDetails->PostDate ?></li>
                                                  <li><strong>Test Mode:</strong> <?php echo $LeadInboundDetails->TestMode ?></li>
                                                  <li><strong>Total Time:</strong> <?php echo number_format($TotalTime->PostTime, 3); ?> secs </li>
                                               </ul>
                                            </div>
                                            <div class="col-md-4 leadInfo">
                                               <h5>Vendor Post Data</h5>
                                                <?php
                                                  $VendorPostData = $LeadInboundDetails->PostData;
                                                  
                                                ?>
                                               <textarea rows="20" cols="49"><?php echo $VendorPostData; ?></textarea>
                                            </div>
                                            <div class="col-md-4 leadInfo">
                                               <h5>Vendor Post Response Data</h5>
                                                <?php
                                                  $VendorPostResponseData = $LeadInboundDetails->PostResponseData;
                                                  
                                                ?>
                                               <textarea rows="20" cols="49"><?php echo $VendorPostResponseData; ?></textarea>
                                            </div>
                                          </div>
                                          <?php foreach($BuyerAttemptedList as $key=>$value){ 
                                            $DivColor="style='background-color:#efadad;'";
                                            if($value->PostStatus == '1')
                                            {
                                              $DivColor="style='background-color:#bcd8bc;'";
                                            }
                                            ?>
                                          <div class="row" <?php echo $DivColor; ?>>
                                              <div class="col-md-4 leadInfo">
                                                 <h5>Outbound Lead Info</h5>
                                                 <ul>
                                                      <li><strong>OutBound ID:</strong><?php echo $value->ID ?></li>
                                                      <li><strong>Lead ID:</strong><?php echo $value->LeadID ?></li>
                                                      <li><strong>Buyer ID:</strong><?php echo $value->AdminLabel; ?> (<?php echo $value->BuyerID; ?>)</li>
                                                      <li><strong>Buyer Price:</strong>$<?php echo number_format($value->PostPrice, 2, '.', ''); ?> </li>                                                   
                                                      <li><strong>Post Response:</strong> <?php echo $value->PostResponse ?></li>
                                                      <li><strong>Post Status:</strong> <?php echo ($value->PostStatus == '1') ? 'Accepted' : 'Rejected'; ?></li>
                                                      <li><strong>Post Time:</strong><?php echo $value->PostTime ?></li>
                                                      <li><strong>Post Date:</strong> <?php echo $value->PostTime ?></li>
                                                 </ul>
                                              </div>
                                              <div class="col-md-4 leadInfo">
                                                 <h5>Buyer Post Data</h5>
                                                 <?php 
                                                  $BuyerPostData = $value->PostData;
                                                  ?>
                                                 <textarea rows="20" cols="49"><?php echo $BuyerPostData; ?></textarea>
                                              </div>
                                              <div class="col-md-4 leadInfo">
                                                 <h5>Buyer Post Response Data</h5>
                                                 <textarea rows="20" cols="49"><?php echo $value->PostResponseData; ?></textarea>
                                              </div>
                                          </div>
                                          <?php }?>
                                      </div>
                                   </div>
                                </div>
                             </div>
                          </div>
                       </div>
                    </div>
                 </div>
              </form>
           </div>
        </div>
          <?php } ?>
       </div>
       @include('layouts.Footer')
    </div>
    </div>
  </div>
  <input type="hidden" name="url" id="url" value='{{URL::to("/")}}'>
  <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
  <input type="hidden" name="loading" id="loading" value='{{ asset("Admin/images/loading_image.gif") }}'>
  @include('layouts.Script')
  <script src="{{asset('Ajax/ViewProcessedLeadReport.js')}}"></script> 
  <script>
  $(document).ready(function() {
    $('#sandbox-container .input-daterange').datepicker({});

    $('#datepicker').datepicker({
      weekStart: 0,
      daysOfWeekHighlighted: "6,0",
      autoclose: true,
      todayHighlight: true,
      dateFormat: 'mm/dd/yy'
    });
    $('#datepicker').datepicker("setDate", new Date());
  });
  </script>
</body>
