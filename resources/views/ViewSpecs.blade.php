<?php
$Integration_Email = "IntegrationEmail";
$Integration_EmailArray = DB::table('master_config')
                    ->select('ParameterValue')
                    ->where('ParameterName',$Integration_Email)
                    ->first();
$IntegrationEmail = $Integration_EmailArray->ParameterValue;

$API_Domain = "APIDomain";
$API_DomainArray = DB::table('master_config')
                    ->select('ParameterValue')
                    ->where('ParameterName',$API_Domain)
                    ->first();
$APIDomain = $API_DomainArray->ParameterValue;

$NextPayDate = date("Y-m-d", strtotime( "next friday" ));
/*$date = strtotime("+14 day");
$NextPayDate2 = date("Y-m-d", $date);*/

$Date = $NextPayDate;
$NextPayDate2 = date('Y-m-d', strtotime($Date. ' + 14 days'));

$IPAddress=$IPAddress;
$UserAgent=$UserAgent;
$VendorID="";
$APIKey="";
if(!empty($VendorDetails))
{
    $VendorID=$VendorDetails->VendorID;
    $APIKey=$VendorDetails->APIKey; 
}
$FirstName="";
$LastName="";
$Email="";
$PrimaryPhone="";
$SecondaryPhone="";
$ReferencePhone="";
if(!empty($UserDetails))
{
    $FirstName=$UserDetails->UserFirstName;
    $LastName=$UserDetails->UserLastName; 
    $Email=$UserDetails->UserEmail; 
    $PrimaryPhone=$UserDetails->UserTel; 
    $SecondaryPhone=intval($PrimaryPhone)+1; 
    $ReferencePhone=intval($PrimaryPhone)+2; 
}
$Address="";
$Zip="";
$Employer="";
$WorkPhone="";
$BankPhone="";
$State="";
$City="";
if(!empty($CompanyDetails))
{
    if($CompanyDetails->CompanyAddress1 != '') 
        $Address=$CompanyDetails->CompanyAddress1;
    else $Address="340 S Lemon Ave #1838";
    if($CompanyDetails->CompanyStateCounty != '')
        $State=$CompanyDetails->CompanyStateCounty;
    else $State="CA";
    if($CompanyDetails->CompanyCity != '')
        $City = $CompanyDetails->CompanyCity;
    else $City="Walnut";
    $Zip=$CompanyDetails->CompanyZipPostCode;     
    $Employer=$CompanyDetails->CompanyName;     
    $WorkPhone=$CompanyDetails->CompanyTel;     
    $BankPhone=intval($WorkPhone)+2;   
}
$UniversalLeadID = "";
$UniversalLeadID = GUIDv4();
function GUIDv4($trim = true)
{
    // Windows
    if (function_exists('com_create_guid') === true) {
        if ($trim === true)
        return trim(com_create_guid(), '{}');
        else
        return com_create_guid();
    }

    // OSX/Linux
    if (function_exists('openssl_random_pseudo_bytes') === true) {
        $data = openssl_random_pseudo_bytes(16);
        $data[6] = chr(ord($data[6]) & 0x0f | 0x40);    // set version to 0100
        $data[8] = chr(ord($data[8]) & 0x3f | 0x80);    // set bits 6-7 to 10
        return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
    }

    // Fallback (PHP 4.2+)
    mt_srand((double)microtime() * 10000);
    $charid = strtolower(md5(uniqid(rand(), true)));
    $hyphen = chr(45);                  // "-"
    $lbrace = $trim ? "" : chr(123);    // "{"
    $rbrace = $trim ? "" : chr(125);    // "}"
    $guidv4 = $lbrace.
    substr($charid,  0,  8).$hyphen.
    substr($charid,  8,  4).$hyphen.
    substr($charid, 12,  4).$hyphen.
    substr($charid, 16,  4).$hyphen.
    substr($charid, 20, 12).
    $rbrace;
    return $guidv4;
}

$dt = date('Y-m-d H:i:s');
$dob = date("Y-m-d");
$edt = date("Y-m-d", strtotime("+1 day"));
$sdt = date("Y-m-d", strtotime("-3 years"));
?>
<!DOCTYPE html>
<html lang="en">
@include('layouts.Head')
<body>
  <div class="container-scroller">
    @include('layouts.Header')
    <div class="container-fluid page-body-wrapper">
      @include('layouts.Menu')
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="page-header row">
            <h3 class="page-title">
              API Specs
            </h3>
          </div>
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb breadcrumb-custom">
              <li class="breadcrumb-item"><a href="#"><i class="fab fa-microsoft menu-icon"></i> View Specs</a></li>
              <li class="breadcrumb-item active" aria-current="page"><span>
                  <i class="{{$CampaignSpecsDetails->Icon}}"></i> {{$CampaignDetails->CampaignName}} </span></li>
            </ol>
          </nav> 
          <div class="specsDwn mb-2"> 
            <a class="btn btn-xs btn-success pull-right" href="<?php echo '/GeneratePDF/'.$CampaignID; ?>">Download PDF</a> 
          </div>      
          <div class="allDet">
           <?php
            Session::forget('Specs'); 
            $String =  $CampaignSpecsDetails->Specs;
            $String =  str_replace('[--VendorID--]', $VendorID, $String);
            $String =  str_replace('[--APIKey--]', $APIKey, $String);
            $String =  str_replace('[--FirstName--]', $FirstName, $String);
            $String =  str_replace('[--LastName--]', $LastName, $String);
            $String =  str_replace('[--Email--]', $Email, $String);
            $String =  str_replace('[--Address--]', $Address, $String);
            $String =  str_replace('[--State--]', $State, $String);
            $String =  str_replace('[--City--]', $City, $String);
            $String =  str_replace('[--Zip--]', $Zip, $String);
            $String =  str_replace('[--PrimaryPhone--]', $PrimaryPhone, $String);
            $String =  str_replace('[--SecondaryPhone--]', $SecondaryPhone, $String);
            $String =  str_replace('[--HomePhone--]', $PrimaryPhone, $String);
            $String =  str_replace('[--CellPhone--]', $SecondaryPhone, $String);
            $String =  str_replace('[--Employer--]', $Employer, $String);
            $String =  str_replace('[--WorkPhone--]', $WorkPhone, $String);            
            $String =  str_replace('[--BankPhone--]', $BankPhone, $String);            
            $String =  str_replace('[--ReferencePhone--]', $ReferencePhone, $String);            
            $String =  str_replace('[--UniversalLeadID--]', $UniversalLeadID, $String);
            $String =  str_replace('[--ULeadID--]', $UniversalLeadID, $String);
            $String =  str_replace('[--IPAddress--]', $IPAddress, $String);            
            $String =  str_replace('[--UserAgent--]', $UserAgent, $String);            
            $String =  str_replace('[--NextPayDate--]', $NextPayDate, $String);            
            $String =  str_replace('[--NextPayDate2--]', $NextPayDate2, $String);
            $String =  str_replace('[--Date--]', $dt, $String);
            $String =  str_replace('[--DOB--]', $dob, $String);
            $String =  str_replace('[--SDate--]', $sdt, $String);
            $String =  str_replace('[--EDate--]', $edt, $String);
            $String =  str_replace('[--IntegrationEmail--]', $IntegrationEmail, $String);
            $String =  str_replace('[--APIDomain--]', $APIDomain, $String);
            Session::put('Specs', $String);
            echo $String;
            ?>
          </div>
       </div>
       @include('layouts.Footer')
    </div>
    </div>
  </div>
  <input type="hidden" name="url" id="url" value='{{URL::to("/")}}'>
  <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
  <input type="hidden" name="loading" id="loading" value='{{ asset("Admin/images/loading_image.gif") }}'>
  @include('layouts.Script')
  <script>

  </script>
</body>
