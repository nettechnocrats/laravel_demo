<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="shortcut icon" href="{{ asset('Admin/images/favicon.ico') }}" type="image/x-icon">
  <title>{{$Title}}</title>
  <link rel="stylesheet" href="{{asset('Admin/vendors/iconfonts/font-awesome/css/all.min.css')}}">
  <link rel="stylesheet" href="{{asset('Admin/vendors/css/vendor.bundle.base.css')}}">
  <link rel="stylesheet" href="{{asset('Admin/vendors/css/vendor.bundle.addons.css')}}">
  <link rel="stylesheet" href="{{asset('Admin/css/style.css')}}">
  <link rel="shortcut icon" href="#" />
</head>
