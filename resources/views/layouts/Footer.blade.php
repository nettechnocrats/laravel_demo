<footer class="footer">
	<div class="d-sm-flex justify-content-center justify-content-sm-between">
		<span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © <?php echo date("Y"); ?> <a href="https://revjolt.com/" target="_blank">REVJOLT</a> LLC, 340 S LEMON AVE #1838, WALNUT, CA 91789. All rights reserved.</span>
	</div>
</footer>
