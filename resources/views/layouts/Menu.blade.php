<?php
$UserRole = Session::get('user_role');
$CompanyID = Session::get('user_companyID');
$UserID = Session::get('user_id');
$HasBuyer = DB::table('partner_buyer_campaigns')
        ->select('*')
        ->where('CompanyID', $CompanyID)
        ->count();
$HasVendor = DB::table('partner_vendor_campaigns')
        ->select('*')
        ->where('CompanyID', $CompanyID)
        ->count();
$Vendor = DB::table('partner_vendor_campaigns')
        ->select('*')
        ->where('CompanyID', $CompanyID)
        ->first();
$Specs = DB::table('campaign_specs as cs')
                ->join('master_campaigns as ms', 'ms.CampaignID', 'cs.CampaignID')
                ->join('partner_vendor_campaigns as pvc', 'ms.CampaignID', 'pvc.CampaignID')
                ->select('cs.CampaignID', 'ms.CampaignName', 'cs.Icon')
                ->where('cs.Specs', '!=', '')
                ->where('pvc.CompanyID', $CompanyID)
                ->where('ms.HasSpecs', 1)
                ->distinct()->get(['pvc.CampaignID']);
$ShowAPISpecs= DB::table('partner_users')
        ->select('ShowAPISpecs')
        ->where('UserID', $UserID)
        ->first();
$VendorID = "";
if (!empty($Vendor)) {
    $VendorID = $Vendor->VendorID;
}
?>
<nav class="sidebar sidebar-offcanvas bxSd" id="sidebar">
	<ul class="nav">
	  <li class="nav-item nav-profile">
	    <div class="nav-link">
	      <div class="profile-image">
	        <img src="{{asset('Admin/images/faces/face-icon.png')}}" alt="image"/>
	      </div>
	      <div class="profile-name">
	        <p class="name">
	          Welcome {{Session::get('user_firstname')}}
	        </p>
	        <p class="designation">
	        	@if(Session::get('user_role')=='2')
	        	Admin
	        	@elseif(Session::get('user_role')=='3')
	        	User
	        	@endif
	        </p>
	      </div>
	    </div>
	  </li>
	  <li class="nav-item nav-itemW" id='dashboard'>
	    <a class="nav-link" href="{{route('admin.Dashboard')}}">
	      <i class="fa fa-home menu-icon"></i>
	      <span class="menu-title">Dashboard</span>
	    </a>
	  </li>
	  <li class="nav-item nav-itemW">
	    <a class="nav-link" data-toggle="collapse" href="#page-layouts" aria-expanded="false" aria-controls="page-layouts">
	      <i class="fas fa-file-alt menu-icon"></i>
	      <span class="menu-title">Reports</span>
	      <i class="menu-arrow"></i>
	    </a>
	    <div class="collapse" id="page-layouts">
	      <ul class="nav flex-column sub-menu">
	        <li class="nav-item"> <a class="nav-link" href="{{route('admin.LeadReportView')}}"><i class="fas fa-file-alt iconMarg"></i>Lead Report</a></li>	                  
	      	@if($HasVendor!=0)
			<li class="nav-item"> <a class="nav-link" href="{{route('admin.VendorCommissionReportView')}}"><i class="fas fa-file-alt iconMarg"></i>Vendor Commission Report</a></li>
			@endif
	        @if($HasBuyer!=0)
			<li class="nav-item"> <a class="nav-link" href="{{route('admin.BuyerInvoiceReportView')}}"><i class="fas fa-file-alt iconMarg"></i>Buyer Invoice Report</a></li>                
			@endif
	      	
	       	<li class="nav-item"> <a class="nav-link" href="{{route('admin.AffiliateReportView')}}"><i class="fas fa-file-alt iconMarg"></i>Referral Commision</a></li>
	       	<li class="nav-item"> <a class="nav-link" href="{{route('admin.WebsiteVisitorReportView')}}"><i class="fas fa-file-alt iconMarg"></i>Website Visitor Report</a></li>
	       	<li class="nav-item"> <a class="nav-link" href="{{route('admin.VendorResponseView')}}"><i class="fas fa-file-alt iconMarg"></i>Vendor Response Report</a></li>
	       	<li class="nav-item"> <a class="nav-link" href="{{route('admin.BuyerResponseView')}}"><i class="fas fa-file-alt iconMarg"></i>Buyer Response Report</a></li>
	       	<li class="nav-item"> <a class="nav-link" href="{{route('admin.ErrorLogView')}}"><i class="fas fa-file-alt iconMarg"></i>Error Log Report</a></li>
	       </ul>
	    </div>
	  </li>
	  <li class="nav-item nav-itemW">
	    <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
	      <i class="fas fa-users-cog menu-icon"></i>
	      <span class="menu-title">Super Admin Tools</span>
	      <i class="menu-arrow"></i>
	    </a>
	    <div class="collapse" id="ui-basic">
	       <ul class="nav flex-column sub-menu">
	        <li class="nav-item"> <a class="nav-link" href="{{route('admin.CampaignManagementView')}}"><i class="fas fa-user-cog iconMarg"></i> Campiagn Management</a></li>
	        <li class="nav-item"> <a class="nav-link" href="{{route('admin.CompanyManagementView')}}"><i class="fas fa-users menu-icon"></i> Company Management</a></li>
	        <li class="nav-item"> <a class="nav-link" href="{{route('admin.MappingList')}}"><i class="fas fa-store iconMarg"></i> Vendor/Buyer Mapping</a></li>
	       </ul>
	      </div>
	  </li>
	  <li class="nav-item nav-itemW">
	    <a class="nav-link" data-toggle="collapse" href="#ui-basic1" aria-expanded="false" aria-controls="ui-basic">
	      <i class="fas fa-wrench menu-icon"></i>
	      <span class="menu-title">Admin Tools</span>
	      <i class="menu-arrow"></i>
	    </a>
	    <div class="collapse" id="ui-basic1">
	      <ul class="nav flex-column sub-menu">
	        <li class="nav-item"> <a class="nav-link" href="{{route('admin.UserManagementView')}}"><i class="fas fa-user iconMarg"></i>User Management</a></li>
	        <li class="nav-item"> <a class="nav-link" href="{{route('admin.VendorManagementView')}}"><i class="fas fa-user-plus iconMarg"></i>Vendor Management</a></li>
	        <li class="nav-item"> <a class="nav-link" href="{{route('admin.BuyerManagementView')}}"><i class="fas fa-user-plus iconMarg"></i>Buyer Management</a></li>
	      </ul>
	      </div>
	  </li>	 
		@if($HasBuyer!=0 || $HasVendor!=0)
		<li class="nav-item nav-itemW">
			<a class="nav-link" data-toggle="collapse" href="#form-elements" aria-expanded="false" aria-controls="form-elements">
				<i class="fas fa-reply-all menu-icon"></i>
				<span class="menu-title">Returns</span>
				<i class="menu-arrow"></i>
			</a>
			<div class="collapse" id="form-elements">
				<ul class="nav flex-column sub-menu">
					@if($HasBuyer!=0)
					<li class="nav-item"><a class="nav-link" href="{{route('admin.ImportReturnsView')}}">
						<i class="fas fa-file-upload iconMarg"></i>Upload Returns</a></li>                
					@endif
                    @if($HasVendor!=0)
					<li class="nav-item"><a class="nav-link" href="{{route('admin.ExportReturnsView')}}">
						<i class="fas fa-file-download iconMarg"></i>Download Returns</a></li>
					@endif
				</ul>
			</div>
		</li>
		@endif
	    @if(!empty($Vendor))
	      @if ($ShowAPISpecs->ShowAPISpecs == '1')
		  <li class="nav-item nav-itemW">
		    <a class="nav-link" data-toggle="collapse" href="#editors" aria-expanded="false" aria-controls="editors">
		      <i class="fab fa-microsoft menu-icon"></i>
		      <span class="menu-title">API Specs</span>
		      <i class="menu-arrow"></i>
		    </a>
		    <div class="collapse" id="editors">
		      <ul class="nav flex-column sub-menu">
		      	@if($HasVendor!=0)
	                @foreach($Specs as $s)
			        <li class="nav-item">
			        	<a class="nav-link" 
			        		href="<?php echo '/view-specs/' . $s->CampaignID . '/' . base64_encode($VendorID); ?>">
			        		<i class="<?php echo $s->Icon; ?>"></i><?php echo $s->CampaignName; ?>
			        	</a>
			        </li>
			        @endforeach
                @endif
		      </ul>
		    </div>
		  </li>
		  @endif
       @endif
	</ul>
</nav>
