<nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row default-layout-navbar navBxsd">
  <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
    <a class="navbar-brand brand-logo" href="{{route('admin.Dashboard')}}"><img src="{{asset('Admin/images/logo.png')}}" alt="logo"/></a>
    <a class="navbar-brand brand-logo-mini" href="{{route('admin.Dashboard')}}"><img src="{{asset('Admin/images/small-logo.png')}}" alt="logo"/></a>
  </div>
  <div class="navbar-menu-wrapper d-flex align-items-stretch">
    <button class="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
      <span class="fas fa-bars"></span>
    </button>
    <ul class="navbar-nav">
      <li class="nav-item nav-search d-none d-md-flex">
        <div class="nav-link">
          <!-- <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text">
                <i class="fas fa-search"></i>
              </span>
            </div>
            <input type="text" class="form-control sercFont" placeholder="Search" aria-label="Search">
          </div> -->
        </div>
      </li>
    </ul>
    <ul class="navbar-nav navbar-nav-right logg topMenuW">
	    <li class="riGht">
	       <strong>Logged in as:</strong> {{Session::get('user_firstname')}}&nbsp;{{Session::get('user_lastname')}}<br/>
			   <strong>IP:</strong> {{ $_SERVER['REMOTE_ADDR'] }}
			   <strong>Time:</strong> <span id="time"><?php echo date('H:i:s', time()); ?></span>
		  </li>          
      <li class="nav-item nav-profile dropdown dropIcon">
        <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" id="profileDropdown">
          <img src="{{asset('Admin/images/faces/face-icon.png')}}" alt="profile"/>
        </a>
        <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="profileDropdown">
          <a class="dropdown-item itemPadd" href="{{route('admin.Profile')}}">
            <i class="fas fa-user-circle text-primary"></i>
            User Profile
          </a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item itemPadd" href="{{route('admin.logout')}}">
            <i class="fas fa-power-off text-primary"></i>
            Logout
          </a>
        </div>
      </li>
    </ul>
    <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
      <span class="fas fa-bars"></span>
    </button>
  </div>
</nav>
