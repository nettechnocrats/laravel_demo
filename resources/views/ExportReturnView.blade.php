<!DOCTYPE html>
<html lang="en">
@include('layouts.Head')
<body>
  <div class="container-scroller">
    @include('layouts.Header')
    <div class="container-fluid page-body-wrapper">
      @include('layouts.Menu')
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="page-header row">
            <h3 class="page-title">
              Download Returns
            </h3>
          </div>
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb breadcrumb-custom">
              <li class="breadcrumb-item"><a href="#"><i class="fas fa-users-cog menu-icon"></i> Returns</a></li>
              <li class="breadcrumb-item active" aria-current="page"><span class="brFont"><i class="fas fa-file-import menu-icon"></i> Download Returns</span></li>
            </ol>
          </nav>
           <div class="col-md-12" id="ErrorMessage">
           
          </div>
          <div class="allDet">
           <div class="col-lg-12 col-md-12 col-sm-12 col-12 comP">
              <div id="dataTable_wrapper" class="dataTables_wrapper dt-bootstrap4">
                 <div class="row">
                    <div class="col-md-3 form-group newFoGrp">
                      <select class="form-control fonT" id="Campaign">
                          @foreach($CampaignDropDown as $cdd)
                          <option value="{{$cdd->CampaignID}}">{{$cdd->CampaignName}}</option>
                          @endforeach
                      </select>
                    </div>
                    <div class="col-md-3 form-group newFoGrp">
                      <div class="input-daterange input-group" id="datepicker">
                          <input type="text" value="{{$StartDate}}" class="input-sm form-control newPadd datePadd fonT" name="start" id="start"/>
                            <span class="input-group-addon fonT">To</span>
                          <input type="text" value="{{$EndDate}}" class="input-sm form-control newPadd datePadd fonT" name="end" id="end"/>
                       </div>
                    </div>
                    <div class="col-md-2 form-group newFoGrp">
                      <button type="button" onclick="SearchExportReport();" class="btn btn-success noBoradi nPadd cbtnPadd sercFontt ">Search</button>                   
                    </div>
                    <div class="col-md-4 form-group newFoGrp">
                      <span id="DownloadBtn" class="Floa"><span>
                    </div>
                 </div>
                 <div id="ExportReport">
                  <input type="hidden" name="numofrecords" id="numofrecords" value='10'>
                 </div>                 
              </div>
           </div>
          </div>
       </div>
       @include('layouts.Footer')
    </div>
    </div>
  </div>
  <input type="hidden" name="url" id="url" value='{{URL::to("/")}}'>
  <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
  <input type="hidden" name="loading" id="loading" value='{{ asset("Admin/images/loading.gif") }}'>
  @include('layouts.Script')
  <script src="{{asset('Ajax/ExportReturns.js')}}"></script> 
  <script>
  $(document).ready(function() {
    $('#sandbox-container .input-daterange').datepicker({});

    $('#datepicker').datepicker({
      weekStart: 0,
      daysOfWeekHighlighted: "6,0",
      autoclose: true,
      todayHighlight: true,
      format: 'yyyy-mm-dd'
    });
    $('#datepicker').datepicker("setDate", new Date());
  });
  </script>
</body>
