<!DOCTYPE html>
<html lang="en">
@include('layouts.Head')
<body>
  <div class="container-scroller">
    @include('layouts.Header')
    <div class="container-fluid page-body-wrapper">
      @include('layouts.Menu')
      <div class="main-panel">
        <div class="content-wrapper">
          <!-- <div class="page-header row">
            <h3 class="page-title">
              Website Visitor Report
            </h3>
          </div> -->
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb breadcrumb-custom">
              <li class="breadcrumb-item"><a href="#"><i class="fas fa-file-alt menu-icon"></i> Reports</a></li>
              <li class="breadcrumb-item active" aria-current="page"><span><i class="fas fa-eye menu-icon"></i> Error Log Report</span></li>
            </ol>
          </nav>
           <div class="allDet">
              <div class="col-lg-12 col-md-12 col-sm-12 col-12 comP">                
                  <div class="boxS">                    
                    <div id="dataTable_wrapper" class="dataTables_wrapper dt-bootstrap4">
                      <input type="hidden" name="numofrecords" id="numofrecords" value='10'>                        
                    </div>
                  </div>
              </div>
          </div>
       </div>
       @include('layouts.Footer')
    </div>
    </div>
  </div>
  <input type="hidden" name="url" id="url" value='{{URL::to("/")}}'>
  <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
  <input type="hidden" name="loading" id="loading" value='{{ asset("Admin/images/loading_image.gif") }}'>
  @include('layouts.Script')
  <script src="https://code.highcharts.com/highcharts.js"></script>
  <script src="https://code.highcharts.com/modules/exporting.js"></script>
  <script src="{{asset('Ajax/ErrorLog.js')}}"></script> 
  <script>
  $(document).ready(function() {
    $('#sandbox-container .input-daterange').datepicker({});

    $('#datepicker').datepicker({
      weekStart: 0,
      daysOfWeekHighlighted: "6,0",
      autoclose: true,
      todayHighlight: true,
      dateFormat: 'mm/dd/yy'
    });
    $('#datepicker').datepicker("setDate", new Date());
  });
  
  </script>
</body>
