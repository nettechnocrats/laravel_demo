<!DOCTYPE html>
<html lang="en">
@include('layouts.Head')
<body>
  <div class="container-scroller">
    @include('layouts.Header')
    <div class="container-fluid page-body-wrapper">
      @include('layouts.Menu')
      <div class="main-panel">
        <div class="content-wrapper">
         <!--  <div class="page-header row">
            <h3 class="page-title">
              Add New Campaign
            </h3>
          </div> -->
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb breadcrumb-custom">
              <li class="breadcrumb-item"><a href="#"><i class="fas fa-users-cog menu-icon"></i> Super Admin Tools</a></li>
              <li class="breadcrumb-item"><a href="{{ route('admin.CompanyManagementView') }}"><i class="fas fa-users menu-icon"></i> Company Management </a></li>
              <li class="breadcrumb-item active" aria-current="page"><span><i class="fas fa-plus-square menu-icon"></i> Edit Company Details </span></li>
            </ol>
          </nav>
          <div class="col-md-12">
            @if(Session::has('message'))
              <div class="alert alrtDanger {{ Session::get('alert-class', 'alert-info') }}">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>{{ Session::get('message') }}</st rong>
              </div>
            @endif
          </div>
          <!--  -->
          <div class="allDet">
             <div class="col-md-12 grid-margin stretch-card">
                <div class="card">
                   <div class="card-body">
                      <div class="accordion" id="accordionExample">
                         <div class="card z-depth-0 bordered addMargBo">
                            <div class="card-header" id="headingOne">
                               <h5 class="mb-0">
                                  <button class="btn btn-link tabStyle" type="button" id="comapnyTab" data-target="#CompanyOne" 
                                  aria-controls="CompanyOne">
                                  Comapny
                                  </button>
                                  <button class="btn btn-link tabStyleHiden collapsed" id="userTab" type="button" data-target="#UserOne"
                                     aria-controls="UserOne">
                                  User
                                  </button>
                                  <button class="btn btn-link tabStyleHiden collapsed" id="vendorTab" type="button" data-target="#VendorOne" 
                                    aria-controls="VendorOne">
                                  Vendor
                                  </button>
                                  <button class="btn btn-link tabStyleHiden collapsed" id="buyerTab" type="button" data-target="#BuyerOne" 
                                    aria-controls="BuyerOne">
                                  Buyer
                                  </button>
                               </h5>
                            </div>
                            <div id="CompanyOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                               <div class="card-body">
                                  <div class="tab-pane show active" id="company-1" role="tabpanel" aria-labelledby="company-tab">
                                     <div class="col-lg-12 col-md-12 col-sm-12 col-12 comP">
                                        <form name="UpdateCompanyForm" id="UpdateCompanyForm" method="POST"
                                          action="{{route('admin.SaveCompanyDetails')}}" enctype="multipart/form-data" >
                                        <input type="hidden" id="CompanyID" name="CompanyID" value="{{$CompanyDetails->CompanyID}}">
                                        <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                                        <div class="row">
                                            <div class="col-md-2 form-group tadmPadd labFontt">
                                               <label for="exampleInputEmail1">Company ID:</label>
                                               <input type="text" class="form-control txtPadd" placeholder="Company ID"
                                                value="{{$CompanyDetails->CompanyID}}" id="CompanyID" name="CompanyID" 
                                                onkeypress="HideErr('CompanyNameErr');" disabled>
                                                <span id="CompanyIDErr"></span>
                                            </div>
                                            <div class="col-md-4 form-group tadmPadd labFontt">
                                               <label for="exampleInputEmail1">Company Name:</label>
                                               <input type="text" class="form-control txtPadd" placeholder="Company Name"
                                                value="{{$CompanyDetails->CompanyName}}" id="CompanyName" name="CompanyName" onkeypress="HideErr('CompanyNameErr');">
                                                <span id="CompanyNameErr"></span>
                                            </div>
                                            <div class="col-md-3 form-group tadmPadd labFontt">
                                               <label for="exampleInputEmail1">Telephone:</label>
                                               <input type="text" class="form-control txtPadd" placeholder="Company Telephone"
                                                value="{{$CompanyDetails->CompanyTel}}" id="CompanyTel" name="CompanyTel">
                                                <span id="CompanyTelErr"></span>
                                            </div>
                                            <div class="col-md-3 form-group tadmPadd labFontt">
                                               <label for="exampleInputEmail1">WebSite:</label>
                                               <input type="text" class="form-control txtPadd" placeholder="Company WebSite"
                                                value="{{$CompanyDetails->CompanyWebSite}}" id="CompanyWebSite" name="CompanyWebSite">
                                                <span id="CompanyWebSiteErr"></span>
                                            </div>                                              
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4 form-group labFontt">
                                               <label for="exampleInputEmail1">Address 1 :</label>
                                               <input type="text" class="form-control txtPadd" placeholder=""
                                                value="{{$CompanyDetails->CompanyAddress1}}" id="CompanyAddress1" name="CompanyAddress1">
                                                <span id="CompanyAddress1Err"></span>
                                            </div>
                                            <div class="col-md-4 form-group tadmPadd labFontt">
                                               <label for="exampleInputEmail1">Address 2:</label>
                                               <input type="text" class="form-control txtPadd" placeholder=""
                                                value="{{$CompanyDetails->CompanyAddress2}}" id="CompanyAddress2" name="CompanyAddress2">
                                                <span id="CompanyAddress2Err"></span>
                                            </div>
                                            <div class="col-md-4 form-group tadmPadd labFontt">
                                               <label for="exampleInputEmail1">Address 3:</label>
                                               <input type="text" class="form-control txtPadd" placeholder=""
                                                value="{{$CompanyDetails->CompanyAddress3}}" id="CompanyAddress3" name="CompanyAddress3">
                                                <span id="CompanyAddress3Err"></span>
                                            </div>
                                        </div>
                                        <div class="row">.
                                            <?php if($CompanyDetails->CompanyStatus=='1'){ ?>
                                            <div class="col-md-4 form-group tadmPadd labFontt">
                                               <label for="exampleInputEmail1">Company Status:</label>
                                               <div class="onoffswitch4">
                                                  <input type="checkbox" name="CompanyStatus" class="onoffswitch4-checkbox" 
                                                    id="CompanyStatus" checked>
                                                  <label class="onoffswitch4-label" for="CompanyStatus">
                                                  <span class="onoffswitch4-inner"></span>
                                                  <span class="onoffswitch4-switch"></span>
                                                  </label>
                                               </div>
                                            </div>
                                            <?php } else {?>
                                            <div class="col-md-4 form-group tadmPadd labFontt">
                                               <label for="exampleInputEmail1">Company Status:</label>
                                               <div class="onoffswitch4">
                                                  <input type="checkbox" name="CompanyStatus" class="onoffswitch4-checkbox" 
                                                    id="CompanyStatus" >
                                                  <label class="onoffswitch4-label" for="CompanyStatus">
                                                  <span class="onoffswitch4-inner"></span>
                                                  <span class="onoffswitch4-switch"></span>
                                                  </label>
                                               </div>
                                            </div>
                                            <?php }?>
                                            <?php if($CompanyDetails->CompanyTestMode=='1'){ ?>
                                            <div class="col-md-4 form-group tadmPadd labFontt">
                                               <label for="exampleInputEmail1">Company Mode:</label>
                                               <div class="onoffswitch5">
                                                  <input type="checkbox" name="CompanyTestMode" class="onoffswitch5-checkbox" 
                                                  id="CompanyTestMode" >
                                                  <label class="onoffswitch5-label" for="CompanyMode">
                                                  <span class="onoffswitch5-inner"></span>
                                                  <span class="onoffswitch5-switch"></span>
                                                  </label>
                                               </div>
                                            </div> 
                                            <?php } else {?> 
                                            <div class="col-md-4 form-group tadmPadd labFontt">
                                               <label for="exampleInputEmail1">Company Mode:</label>
                                               <div class="onoffswitch5">
                                                  <input type="checkbox" name="CompanyMode" class="onoffswitch5-checkbox" 
                                                  id="CompanyMode" checked>
                                                  <label class="onoffswitch5-label" for="CompanyMode">
                                                  <span class="onoffswitch5-inner"></span>
                                                  <span class="onoffswitch5-switch"></span>
                                                  </label>
                                               </div>
                                            </div>
                                            <?php }?>                                              
                                        </div>
                                        <div class="row">
                                            <?php 
                                            $StateDiv="style='display:none;'";
                                            if($CompanyDetails->CompanyStateCounty=='US')
                                            {
                                              $StateDiv="style='display:block;'";
                                            }
                                            ?>
                                            <div class="col-md-2 form-group labFontt">
                                               <label for="exampleInputEmail1">City:</label>
                                               <input type="text" class="form-control txtPadd" placeholder="City"
                                               id="CompanyCity" name="CompanyCity" value="{{$CompanyDetails->CompanyCity}}">
                                               <span id="CompanyCityErr"></span>
                                            </div>
                                            <div class="col-md-2 form-group mb-0 tadmPadd labFontt" id="StateDiv" <?php echo $StateDiv;  ?>>
                                               <label for="exampleFormControlSelect1">State:</label>
                                               <select class="form-control fonT" id="CompanyStateCounty" name="CompanyStateCounty">
                                                  <option value="">Select State</option>
                                                  @foreach($StatesList as $s)
                                                  <option value="{{$s->State}}"
                                                    <?php if($CompanyDetails->CompanyStateCounty==$s->State){ echo "selected";} ?>>{{$s->State}}</option>
                                                  @endforeach
                                               </select>
                                               <span id="CompanyStateCountyErr"></span>
                                            </div>
                                            <div class="col-md-2 form-group mb-0 tadmPadd labFontt">
                                               <label for="exampleInputEmail1" id="CompanyZipPostCodeLable">Zip Code:</label>
                                               <input type="text" class="form-control txtPadd" placeholder="Zip Code"
                                               id="CompanyZipPostCode" name="CompanyZipPostCode" onkeypress="HideErr('CompanyZipPostCodeErr');"
                                               value="{{$CompanyDetails->CompanyZipPostCode}}">
                                               <span id="CompanyZipPostCodeErr"></span>
                                            </div>
                                            <div class="col-md-2 form-group mb-0 labFontt">
                                               <label for="exampleFormControlSelect1">Country:</label>
                                               <select class="form-control fonT" id="CompanyCountry" name="CompanyCountry">
                                                  @foreach($CountryList as $c)
                                                  <option value="{{$c->Code}}"
                                                    <?php if($CompanyDetails->CompanyStateCounty==$c->Code){ echo "selected";} ?>>{{$c->CountryName}}</option>
                                                  @endforeach
                                               </select>
                                               <span id="CompanyCountryErr"></span>
                                            </div>
                                            <div class="col-md-2 form-group mb-0 labFontt">
                                               <label for="exampleFormControlSelect1">Currency:</label>
                                               <select class="form-control fonT" id="CompanyCurrency" name="CompanyCurrency">
                                                  <option value="USD-$" <?php if($CompanyDetails->CompanyCurrency=="USD-$"){ echo "selected";} ?>>USD-$</option>
                                               </select>
                                               <span id="CompanyCurrencyErr"></span>
                                            </div>                                              
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4 form-group mb-0 labFontt">
                                              <label for="exampleFormControlTextarea1">Company Notes:</label>
                                              <textarea class="form-control" id="CompanyNotes" name="CompanyNotes" rows="3">{{$CompanyDetails->CompanyNotes}}</textarea>
                                              <span id="CompanyNotesErr"></span>
                                            </div>
                                             <?php
                                              if($CompanyDetails->UploadedFiles!='')
                                              {
                                                ?>
                                                <div class="col-md-4 form-group mb-0 labFontt">
                                                  <span id="UploadFileMessage"></span>
                                                <label for="exampleFormControlTextarea1">Uploaded Files:</label>
                                                  <table class="table">
                                                    <thead>
                                                      <tr>
                                                        <th>Uploaded File</th>
                                                        <th>Action</th>
                                                      </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php
                                                    $UploadedFiles = json_decode($CompanyDetails->UploadedFiles);
                                                    foreach($UploadedFiles as $upf)
                                                    {
                                                      ?>
                                                      <tr id="UploadedFileTR_<?php echo $upf->id; ?>">
                                                        <td>
                                                          <a href="{{ asset('uploads').'/'.$upf->filename }}" 
                                                            target="_blank">{{$upf->filename}}</a>
                                                        </td>
                                                        <td>
                                                          <a class="btn btn-danger btn-sm" href="javascript:void(0);" 
                                                              onclick="DeleteCompanyUploadedFile('<?php echo $CompanyDetails->CompanyID; ?>','<?php echo $upf->id; ?>');">
                                                              Delete
                                                          </a>
                                                          <input type="hidden" name="OldUploadedFiles[]" value="<?php echo $upf->filename; ?>">
                                                        </td>
                                                      </tr>
                                                      <?php
                                                    }
                                                    ?>
                                                    </tbody>
                                                  </table>
                                                </div>
                                                <?php
                                              } 
                                              ?>
                                            <div class="col-md-4 form-group labFontt tablabFontt">
                                               <label>File: (You Can Select Multiple Files)</label>
                                               <div class="custom-file">
                                                  <input type="file" class="custom-file-input" id="Companyfiles" name="Companyfiles[]">
                                                  <label class="custom-file-label fileLabel" for="Companyfiles">No File Chosen</label>
                                               </div>
                                            </div>
                                        </div>                                   
                                        <div class="row">
                                            <div class="col-md-6  nextttBtn">
                                              <button type="button" class="btn btn-success btn-fw noBoradi mr-2" id="UpdateCompanyBtn">Update</button>
                                              <a href="{{ route('admin.CompanyManagementView') }}" class="btn btn-danger btn-fw noBoradi">Cancel</a>
                                            </div>
                                        </div> 
                                      </form>   
                                     </div>
                                  </div>
                               </div>
                            </div>
                         </div>
                         <div class="card z-depth-0 bordered addMargBo">
                            <div class="card-header addPaddMarg" id="headingOne">
                              <span id='UserMessage'></span>
                            </div>
                            <div id="UserOne" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                               <div class="card-body">
                                  <div class="tab-pane" id="user-1" role="tabpanel" aria-labelledby="user-tab">
                                    <div class="addUser">
                                      <div class="col-lg-12 col-md-12 col-sm-12 col-12 comP">
                                        <div class="row">
                                          <div class="col-md-3 form-group tadmPadd labFontt newFoGrp">
                                            <input type="text" class="form-control txtPadd boxH" id="FirstNameUser" placeholder="First Name">
                                          </div>
                                          <div class="col-md-3 form-group tadmPadd labFontt newFoGrp">
                                            <input type="text" class="form-control txtPadd boxH" id="LastNameUser" placeholder="Last Name">
                                          </div>
                                          <div class="col-md-3 form-group tadmPadd labFontt newFoGrp">
                                            <input type="email" class="form-control boxH" id="EmailUser" placeholder="Email">
                                          </div>
                                          <div class="col-md-3 form-group tadmPadd labFontt newFoGrp">
                                            <input type="text" class="form-control txtPadd boxH" id="PhoneUser"  placeholder="Telephone">
                                          </div> 
                                        </div>
                                        <div class="row">  
                                          <div class="col-md-3 newFoGrp">
                                            <select class="form-control fonT" id="UserRoleUser">
                                              <option value="">All Role</option>
                                              @foreach($UserRole as $Key=>$Value)
                                              <option value="{{$Key}}">{{$Value}}</option>
                                              @endforeach
                                            </select>
                                          </div>
                                          <div class="col-md-3 newFoGrp">
                                            <select class="form-control fonT" id="UserStatusUser" >
                                              <option value="">All Status</option>
                                              <option value="1">Active</option>
                                              <option value="0">In-Active</option>
                                            </select>
                                          </div> 
                                          <div class="col-md-2 col-6">
                                             <button type="button" class="btn btn-success cbtnPadd sercFontt"
                                             id="SearchBtn" onclick="SearchUserList();"> Search </button>
                                          </div>
                                          <div class="col-md-2 col-6 offset-md-2 ">
                                           <!--  <a href="{{route('admin.UserAddView')}}" class="btn btn-success btn-fw cbtnPadd sercFontt fl dwSCSV Flo">Add New User</a> -->
                                             <button type="button" class="btn btn-success btn-fw cbtnPadd sercFontt fl dwSCSV Flo"
                                              onclick="AddUserModal();">Add New User</button>
                                          </div> 
                                        </div>
                                        <div id="dataTable_wrapper_User" class="dataTables_wrapper dt-bootstrap4">
                                          <input type="hidden" name="numofrecords" id="numofrecordsUser" value='10'>
                                        </div>
                                      </div>     
                                    </div>
                                  </div>
                               </div>
                            </div>
                         </div>
                         <div class="card z-depth-0 bordered addMargBo">
                            <div class="card-header addPaddMarg" id="headingThree">
                              <span id='VendorMessage'></span>
                            </div>
                            <div id="VendorOne" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                               <div class="card-body">
                                  <div class="tab-pane" id="vendor-1" role="tabpanel" aria-labelledby="vendor-tab">
                                    <div class="addUser">
                                      <div class="col-lg-12 col-md-12 col-sm-12 col-12 comP">
                                        <div class="row">             
                                          <div class="col-md-2 form-group newFoGrp">
                                            <select class="form-control fonT" id="CampaignVendor" >
                                                @foreach($CampaignDropDown as $cdd)
                                                <option value="{{$cdd->CampaignID}}">{{$cdd->CampaignName}}</option>
                                                @endforeach
                                            </select>
                                          </div>
                                          <div class="col-md-2 form-group tadmPadd labFontt newFoGrp ">
                                            <input type="text" class="form-control txtPadd boxH" id="VendorIDVendor" placeholder="Vendor ID">
                                          </div>
                                          <div class="col-md-2 form-group tadmPadd labFontt newFoGrp ">
                                            <input type="text" class="form-control txtPadd boxH" id="VendorNameVendor" placeholder="Admin Label">
                                          </div>
                                          <div class="col-md-2 form-group newFoGrp">
                                            <select class="form-control fonT" id="VendorStatusVendor">                     
                                                <option value="All">All Status</option>
                                                <option value="1">Active</option>
                                                <option value="0">InActive</option>                      
                                            </select>
                                          </div>
                                          <div class="col-md-1 form-group tadmPadd labFontt newFoGrp">
                                             <button type="button" class="btn btn-success fonT mW nF tW fl noBoradi sercFontt"
                                             id="SearchBtn" onclick="SearchVendorList();"><i class="fa fa-search"></i> Search </button>
                                          </div> 
                                          <div class="col-md-3 form-group tadmPadd labFontt newFoGrp">
                                            <button type="button" class="btn btn-success btn-fw cbtnPadd sercFontt fl dwSCSV Flo"
                                                onclick="AddVendorModal();">Add New Vendor</button>
                                          </div>
                                        </div>
                                        <div id="dataTable_wrapper_Vendor" class="dataTables_wrapper dt-bootstrap4">
                                          <input type="hidden" name="numofrecords" id="numofrecordsVendor" value='10'>
                                        </div>
                                      </div>    
                                    </div>
                                  </div>
                               </div>
                            </div>
                         </div>
                         <div class="card z-depth-0 bordered">
                            <div class="card-header addPaddMarg" id="headingThree">
                              <span id='BuyerMessage'></span>
                            </div>
                            <div id="BuyerOne" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                               <div class="card-body">
                                  <div class="tab-pane" id="buyer-1" role="tabpanel" aria-labelledby="buyer-tab">
                                     <div class="addUser">
                                        <div class="allDet">
                                          <div class="col-lg-12 col-md-12 col-sm-12 col-12 comP">
                                            <div class="row">               
                                              <div class="col-md-2 form-group newFoGrp">
                                                <select class="form-control fonT" id="CampaignBuyer">
                                                    @foreach($CampaignDropDown as $cdd)
                                                    <option value="{{$cdd->CampaignID}}">{{$cdd->CampaignName}}</option>
                                                    @endforeach
                                                </select>
                                              </div>
                                              <div class="col-md-2 form-group tadmPadd labFontt newFoGrp">
                                                <input type="text" class="form-control txtPadd boxH" id="BuyerNameBuyer" placeholder="Buyer Name">
                                              </div>
                                              <div class="col-md-2 form-group newFoGrp">
                                                <select class="form-control fonT" id="StatusBuyer">
                                                   <option value="">All Status</option>
                                                   <option value="1">Active</option>
                                                   <option value="0">Inactive</option>
                                                </select>
                                              </div>
                                              <div class="col-md-2 form-group newFoGrp">
                                                <select class="form-control fonT" id="BuyerModeBuyer">
                                                    <option value="">All Mode</option>
                                                   <option value="0">Live Mode</option>
                                                   <option value="1">Test Mode</option>
                                                </select>
                                              </div>
                                              <div class="col-md-1 form-group tadmPadd labFontt newFoGrp">
                                                 <button type="button" class="btn btn-success fonT mW nF tW fl noBoradi sercFontt"
                                                 id="SearchBtn" onclick="SearchBuyerList();"><i class="fa fa-search"></i> Search </button>
                                              </div> 
                                              <div class="col-md-3 form-group tadmPadd labFontt newFoGrp">
                                                <button type="button" class="btn btn-success btn-fw cbtnPadd sercFontt fl dwSCSV Flo"
                                                    onclick="AddBuyerModal();">Add New Buyer</button>
                                              </div>
                                            </div>
                                            <div id="dataTable_wrapper_Buyer" class="dataTables_wrapper dt-bootstrap4">
                                              <input type="hidden" name="numofrecords" id="numofrecordsBuyer" value='10'>
                                            </div>
                                          </div>
                                        </div>
                                     </div>
                                  </div>
                               </div>
                            </div>
                         </div>
                      </div>
                   </div>
                </div>
             </div>
          </div>
          <!--  -->
        </div>
       @include('layouts.Footer')
    </div>
    </div>
  </div>
  <!-- Add User Window -->
  <div class="modal fade" id="AddUserModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header mHBg">
          <h4 class="modal-title">Add New User</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <form action="<?php echo route('admin.AddUserDetails'); ?>" name="UserAddForm" id="UserAddForm" method="POST">
            <div class="row">
              <input type="hidden" name="_token" id="_token" value="<?php echo  csrf_token(); ?>">
              <input type="hidden" name="CompanyID" id="CompanyID" value="<?php echo  $CompanyDetails->CompanyID; ?>">
              <input type="hidden" name="From" id="From" value="Company">
                 <div class="col-md-3 form-group tadmPadd labFontt">
                    <label for="exampleInputEmail1">First Name:</label>
                    <input type="text" onkeypress="HideErr('UserFirstNameAddUserErr');" value="" id="UserFirstNameAddUser" name="UserFirstName" class="form-control txtPadd" placeholder="First Name">
                    <span id="UserFirstNameAddUserErr" class="errordisp"></span>
                 </div>
                 <div class="col-md-3 form-group tadmPadd labFontt">
                    <label for="exampleInputEmail1">Last Name:</label>
                    <input type="text" onkeypress="HideErr('UserLastNameAddUserErr');" value="" id="UserLastNameAddUser" name="UserLastName" class="form-control txtPadd" placeholder="Last Name">
                    <span id="UserLastNameAddUserErr" class="errordisp"></span>
                 </div> 
                  <div class="col-md-3 form-group tadmPadd labFontt">
                    <label for="exampleInputEmail1">Email:</label>
                    <input type="email" onkeypress="HideErr('UserEmailAddUserErr');" value="" id="UserEmailAddUser" name="UserEmail" class="form-control camH" id="exampleInputEmail1" placeholder="Email">
                    <span id="UserEmailAddUserErr" class="errordisp"></span>
                 </div>
                 <div class="col-md-3 form-group tadmPadd labFontt">
                    <label for="exampleInputEmail1">Telephone:</label>
                    <input type="text" onkeypress="HideErr('UserTelAddUserErr');" value="" id="UserTelAddUser" name="UserTel" class="form-control txtPadd" placeholder="Telephone">
                    <span id="UserTelAddUserErr" class="errordisp"></span>
                 </div>                             
              </div>
              <div class="row">
                  <div class="col-md-3 form-group tadmPadd labFontt">
                    <label for="exampleInputEmail1">Skype ID:</label>
                    <input type="text" onkeypress="HideErr('UserSkypeAddUserErr');" value="" id="UserSkypeAddUser" name="UserSkype" class="form-control txtPadd" placeholder="Skype ID">
                    <span id="UserSkypeAddUserErr" class="errordisp"></span>
                  </div> 
                  
                   <div class="col-md-3 form-group tadmPadd labFontt">
                    <label for="exampleInputEmail1">User Role:</label>
                      <select class="form-control fonT" id="UserRoleAddUser" name="UserRole" onchange="HideErr('UserRoleAddUserErr');">
                        <option value="">Select User Role</option>
                          <?php foreach($UserRole as $Key=>$Value){ ?>
                          <option value="<?=$Key;?>"
                          ><?=$Value;?></option>
                          <?php } ?>
                      </select>
                      <span id="UserRoleAddUserErr" class="errordisp"></span>
                   </div> 
                   <div class="col-md-2 form-group tadmPadd labFontt">
                    <label for="exampleInputEmail1">User Status:</label>
                    <div class="onoffswitch4">
                      <input type="checkbox" name="UserStatus" class="onoffswitch4-checkbox" id="UserStatusAddUser" 
                        checked>
                      <label class="onoffswitch4-label" for="UserStatusAddUser">
                          <span class="onoffswitch4-inner"></span>
                          <span class="onoffswitch4-switch"></span>
                      </label>
                    </div>
                  </div> 
                   <div class="col-md-2 form-group tadmPadd labFontt">
                    <label for="exampleInputEmail1">Show Specs:</label>
                    <div class="onoffswitch6">
                      <input type="checkbox" class="onoffswitch6-checkbox" 
                      id="ShowAPISpecsAddUser" name="ShowAPISpecs" checked>
                      <label class="onoffswitch6-label" for="ShowAPISpecsAddUser">
                          <span class="onoffswitch6-inner"></span>
                          <span class="onoffswitch6-switch"></span>
                      </label>
                    </div>
                  </div>                                
                  <div class="col-md-2 form-group tadmPadd labFontt">
                    <label for="exampleInputEmail1">Show Affiliate Links:</label>
                    <div class="onoffswitch7">
                      <input type="checkbox" class="onoffswitch7-checkbox" 
                      id="ShowAffLinksAddUser" name="ShowAffLinks" checked>
                      <label class="onoffswitch7-label" for="ShowAffLinksAddUser">
                          <span class="onoffswitch7-inner"></span>
                          <span class="onoffswitch7-switch"></span>
                      </label>
                    </div>
                  </div> 
              </div>
              <div class="row" > 
                  <div class="col-md-3 form-group tadmPadd labFontt" >
                    <label for="exampleInputPassword1">Password:</label>
                    <input type="text" onkeypress="HideErr('PasswordAddUserErr');" class="form-control camH" id="PasswordAddUser" name="Password" placeholder="Password">
                    <span id="PasswordAddUserErr" class="errordisp"></span>
                 </div>
                 <div class="col-md-3 form-group tadmPadd labFontt" >
                    <label for="exampleInputPassword1">Confirm Password:</label>
                    <input type="text" onkeypress="HideErr('ConfirmPasswordAddUserErr');" class="form-control camH" id="ConfirmPasswordAddUser" name="ConfirmPassword" placeholder="Confirm Password">
                    <span id="ConfirmPasswordAddUserErr" class="errordisp"></span>
                 </div>
                 <div class="col-md-3 form-group tadmPadd labFontt"  >
                    <label class="gP" for="exampleInputPassword1">Generate Password:</label>
                    <button type="button" onclick="GeneratePasswordAdd();" class="btn btn-success fonT noBoradi cbtnPadd sercFontt gpBg">Generate Password</button>
                 </div>                                                          
              </div>
               <div class="row" >
                <div class="col-md-6 form-group mb-0 labFontt">
                    <label for="exampleFormControlTextarea1">User Notes:</label>
                    <textarea class="form-control" id="UserNotesAddUser" name="UserNotes" rows="3"></textarea>
                  </div>
                  <div class="col-md-6 form-group tadmPadd labFonsize">
                    <div class="form-check form-check-flat form-check-primary adComp adComp-primary mt-4">
                       <label class="form-check-label adComp-label labLh">
                       <input type="checkbox" class="form-check-input" id="SendCredentials" name="SendCredentials">
                        Would you like to email credentials to the user?
                       </label>
                    </div>
                  </div>            
              </div>
              <div class="row btnFlo">
                 <button type="button" id="UserSaveBtn" class="btn btn-success btn-fw noBoradi mr-2" onclick="AddUserDetails();" >Save</button>
                 <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
              </div>
           </form>
        </div>
      </div>        
    </div>
  </div> 
  <!-- Edit User Window -->
  <div class="modal fade" id="EditUserModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header mHBg">
          <h4 class="modal-title">Edit New User</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body" id="EditUserDiv">
          
        </div>
        <div class="modal-footer">

        </div>
      </div>        
    </div>
  </div>
  <!-- Add Vendor Window -->
  <div class="modal fade" id="AddVendorModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header mHBg">
          <h4 class="modal-title">Add New Vendor</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <form action="<?php echo route('admin.AddVendorDetails'); ?>" name="VendorAddForm" id="VendorAddForm" method="POST">                          
            <input type="hidden" name="_token" id="_token" value="<?php echo csrf_token(); ?>">                          
            <input type="hidden" name="From" id="From" value="Company">                          
            <input type="hidden" name="Company" id="company" value="<?php echo $CompanyDetails->CompanyID; ?>">                          
            <input type="hidden" name="CampaignType" id="CampaignTypeAddVendor" value="">                          
            <div class="row">
              <div class="col-md-12" id="MessageAdd">              
              </div>
            </div>
            <div class="row">
              <div class="col-md-4 form-group mb-0 tadmPadd labFontt">
                  <label for="exampleFormControlSelect1">Campaign:</label>
                  <select class="form-control fonT" id="CampaignAddVendor" name="Campaign" 
                      onchange="GetCampaignDetailsAdd(),HideErr('CompanyAddVendorErr');">
                      <?php foreach($CampaignDropDown as $cdd){ ?>
                      <option value="<?php echo $cdd->CampaignID; ?>">
                          <?php echo $cdd->CampaignName; ?></option>
                      <?php } ?>  
                  </select>
                  <span id="CampaignAddVendorErr"></span>
              </div>
              <div class="col-md-3 form-group tadmPadd labFontt">
                <label for="exampleInputEmail1">Admin Label:</label>
                  <input type="text" class="form-control txtPadd" 
                  value="" id="AdminLabelAddVendor" name="AdminLabel"
                  onkeypress="HideErr('AdminLabelAddVendorErr');">
                  <span id="AdminLabelAddVendorErr"></span>
              </div>
              <div class="col-md-2 form-group tadmPadd btnCen">
                <button type="button" id="CopyAdminLable" onclick="CopyAdminLableAddVendor();" class="btn btn-success noBoradi mr-2">&gt;&gt;</button>
              </div>
              <div class="col-md-3 form-group tadmPadd labFontt">
                <label for="exampleInputEmail1">Partner Label:</label>
                  <input type="text" class="form-control txtPadd" 
                    value="" id="PartnerLabelAddVendor" name="PartnerLabel"
                    onkeypress="HideErr('PartnerLabelAddVendorErr');">
                  <span id="PartnerLabelAddVendorErr"></span>
              </div>
            </div> 
            <div class="row">
              <div class="col-md-3 form-group mb-0 tadmPadd labFontt">
                <label for="PayoutCalculation">Payout Calculation:</label>
                  <select class="form-control fonT" id="PayoutCalculationAddVendor" name="PayoutCalculation" >
                    <option value="RevShare" >RevShare</option>
                    <option value="FixedPrice" >Fixed Price</option>
                    <option value="TierPrice" >Tier Price</option>
                    <option value="MinPrice" >Min Price</option>
                    <option value="BucketPrice" >Bucket Price</option>
                    <option value="BucketSystemBySubID" >Bucket System By SubID</option>
                  </select>
              </div>
              <?php 
                $PayoutCalculationRevShare="style='display:block'";
                $PayoutCalculationTierPrice="style='display:none'";
                $PayoutCalculationFixedPrice="style='display:none'";
              ?>
              <div class="col-md-2 form-group tadmPadd labFontt" <?php echo $PayoutCalculationRevShare ?>  
                  id="RevsharePercentInputAdd">
                <label for="exampleInputEmail1">Revshare: </label>
                <input type="text" class="form-control txtPadd" value="70" 
                placeholder="70" id="RevshareAddVendor" name="Revshare" onkeypress="HideErr('RevshareAddVendorErr');">
                <span id="RevshareAddVendorErr"></span>
              </div>
              <div class="col-md-2 form-group tadmPadd labFontt" <?php echo $PayoutCalculationFixedPrice ?> 
                  id="FixedPriceInputAdd">
                <label for="exampleInputEmail1">Fixed Price:</label>
                <input type="text" class="form-control txtPadd" value="1.00" 
                placeholder="70" id="FixedPriceAddVendor" name="FixedPrice" onkeypress="HideErr('FixedPriceAddVendorErr');">
                <span id="FixedPriceAddVendorErr"></span>
              </div>
              <div class="col-md-5 form-group tadmPadd labFontt" <?php echo $PayoutCalculationTierPrice ?> 
                  id="TierPriceInputAdd">
                <label for="exampleInputEmail1">Revshare Percent (for when Tier Payout is $0.00):</label>
                <input type="text" class="form-control txtPadd" value="70"
                 placeholder="70" id="TierPriceAddVendor" name="TierPrice" onkeypress="HideErr('TierPriceAddVendorErr');">
                <span id="TierPriceAddVendorErr"></span>
              </div>
              <div class="col-md-2 form-group tadmPadd labFontt">
                <label for="exampleInputEmail1">Daily Cap:</label>
                  <input type="text"  value="5" 
                    id="DailyCapAddVendor" name="DailyCap" class="form-control txtPadd" placeholder="Daily Cap"
                    onkeypress="HideErr('DailyCapAddVendorErr');">
                  <span id="DailyCapAddVendorErr"></span>
              </div>
              <div class="col-md-2 form-group tadmPadd labFontt">
                <label for="exampleInputEmail1">Total Cap:</label>
                  <input type="text" value="5" 
                    id="TotalCapAddVendor" name="TotalCap" class="form-control txtPadd" placeholder="Total Cap"
                    onkeypress="HideErr('TotalCapAddVendorErr');">
                  <span id="TotalCapAddVendorErr"></span>
              </div>
            </div>
            <div class="row">       
              <div class="col-md-2 form-group tadmPadd labFontt">
                <label for="exampleInputEmail1">Vendor Status:</label>
                  <div class="onoffswitch8">
                    <input type="checkbox" name="VendorStatus" class="onoffswitch8-checkbox" 
                    id="VendorStatusAddVendor" checked="">
                    <label class="onoffswitch8-label" for="VendorStatusAddVendor">
                      <span class="onoffswitch8-inner"></span>
                      <span class="onoffswitch8-switch"></span>
                    </label>
                  </div>
              </div>
              <div class="col-md-2 form-group tadmPadd labFontt">
                <label for="exampleInputEmail1">Vendor Mode?</label>
                  <div class="onoffswitch9">
                    <input type="checkbox" name="VendorTestMode" class="onoffswitch9-checkbox"
                       id="VendorTestModeAddVendor" >
                    <label class="onoffswitch9-label" for="VendorTestModeAddVendor">
                      <span class="onoffswitch9-inner"></span>
                      <span class="onoffswitch9-switch"></span>
                    </label>
                  </div>
              </div>
              <?php 
              $MaxTimeOutInput = "style='display:none;'";
              $MaxPingTimeOutInput = "style='display:none;'";
              $MaxPostTimeOutInput = "style='display:none;'";
              ?>
               <div class="col-md-2 form-group tadmPadd labFontt" id="MaxTimeOutInputAddVendor" <?php echo $MaxTimeOutInput; ?>>
                <label for="exampleInputEmail1">Max Time Out:</label>
                  <input type="text" class="form-control txtPadd" placeholder="0" 
                  id="MaxTimeOutAddVendor" name="MaxTimeOut" value="0" onkeypress="HideErr('MaxTimeOutAddVendorErr');">
                  <span id="MaxTimeOutAddVendorErr"></span>
              </div>
              <div class="col-md-2 form-group tadmPadd labFontt" id="MaxPingTimeOutInputAddVendor" <?php echo $MaxPingTimeOutInput; ?>>
                <label for="exampleInputEmail1">Max Ping Time Out:</label>
                  <input type="text" class="form-control txtPadd" placeholder="0" 
                  id="MaxPingTimeOutAddVendor" name="MaxPingTimeOut" value="0" onkeypress="HideErr('MaxPingTimeOutAddVendorErr');">
                  <span id="MaxPingTimeOutAddVendorErr"></span>
              </div>
              <div class="col-md-2 form-group tadmPadd labFontt" id="MaxPostTimeOutInputAddVendor" <?php echo $MaxPostTimeOutInput; ?>>
                <label for="exampleInputEmail1">Max Post Time Out:</label>
                  <input type="text" class="form-control txtPadd" placeholder="0" 
                  id="MaxPostTimeOutAddVendor" name="MaxPostTimeOut" value="0" onkeypress="HideErr('MaxPostTimeOutAddVendorErr');">
                  <span id="MaxPostTimeOutAddVendorErr"></span>
              </div> 
              <div class="col-md-2 form-group tadmPadd labFontt">
                <label for="exampleInputEmail1">Tracking Pixel Status?</label>
                  <div class="onoffswitch10">
                    <input type="checkbox" name="TrackingPixelStatus" class="onoffswitch10-checkbox"
                     id="TrackingPixelStatus" >
                    <label class="onoffswitch10-label" for="TrackingPixelStatus">
                      <span class="onoffswitch10-inner"></span>
                      <span class="onoffswitch10-switch"></span>
                    </label>
                  </div>
              </div>
            </div> 
            <div class="row">             
              
              <div class="col-md-3 form-group mb-0 tadmPadd labFontt">
                <label for="exampleFormControlSelect1">Tracking Pixel Type:</label>
                  <select class="form-control fonT" id="TrackingPixelType" name="TrackingPixelType">
                    <option value="Pixel" >Pixel</option>
                    <option value="PostBack" >Post Back</option>
                  </select>
              </div>
              <div class="col-md-2 form-group mb-0 labFontt">
                <label for="exampleFormControlSelect1">Insert Token:</label>
                  <select class="form-control" id="insert_Token_type" name="insert_Token_type" onchange="updateTextBoxAddVendor(this)">
                    <option value="aid">aid</option>
                    <option value="SubID">SubID</option>
                    <option value="TestMode">TestMode</option>
                    <option value="TransactionID">TransactionID</option>
                    <option value="VendorLeadPrice">VendorLeadPrice</option>
                </select>
              </div>
              <div class="col-md-5 form-group mb-0 labFontt">
                <label for="exampleFormControlTextarea1">Tracking Pixel Code:</label>
                <textarea class="form-control" id="TrackingPixelCodeAddVendor" name="TrackingPixelCode" rows="3"></textarea>
              </div>
            </div>
            <div class="row">              
              <div class="col-md-5 form-group mb-0 labFontt">
                <label for="exampleFormControlTextarea1">Vendor Notes:</label>
                <textarea class="form-control" id="VendorNotes" name="VendorNotes" rows="3"></textarea>
              </div>
            </div>
            <div class="row btnFlo">
                <div class="col-md-12 form-group mb-0 labFontt">
                  <button type="button" class="btn btn-success btn-fw noBoradi mr-2" onclick="AddVendorDetails();">Save</button>
                  <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </div>
          </form>   
        </div>
        <div class="modal-footer">

        </div>
      </div>        
    </div>
  </div> 
  <!-- Edit Vendor Window -->
  <div class="modal fade" id="EditVendorModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header mHBg">
          <h4 class="modal-title">Edit Vendor Details</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body" id="EditVendorDiv">
          
        </div>
        <div class="modal-footer">

        </div>
      </div>        
    </div>
  </div>
  <!-- Clone Window -->
  <div class="modal fade" id="CloneVendorModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header mHBg">
          <h4 class="modal-title">Clone Vendor Details</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body" id="CloneVendorDiv">
          
        </div>
        <div class="modal-footer">

        </div>
      </div>        
    </div>
  </div>

  <!-- Add Window -->
  <div class="modal fade" id="AddBuyerModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header mHBg">
          <h4 class="modal-title">Add New Buyer</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
            
        </div>
        <div class="modal-footer">
          <form action="{{route('admin.AddBuyerDetails')}}" name="BuyerAddForm" 
          id="BuyerAddForm" method="POST" enctype="multipart/form-data">                          
          <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
          <input type="hidden" name="Company" id="Company" value="{{ $CompanyDetails->CompanyID }}">
          <input type="hidden" name="CampaignType" id="CampaignTypeAddBuyer" value="">
          <div class="row"> 
            <div class="col-md-3 form-group mb-0 tadmPadd labFontt">
                <label for="exampleFormControlSelect1">Campaign:</label>
                <select class="form-control fonT" id="CampaignAddBuyer" name="Campaign"  
                    onchange="GetCampaignDetailsAddBuyer(),HideErr('CompanyAddBuyerErr');">
                    @foreach($CampaignDropDown as $cdd)
                    <option value="{{$cdd->CampaignID}}">{{$cdd->CampaignName}}</option>
                    @endforeach
                </select>
                <span id="CampaignAddBuyerErr"></span>
            </div>
            <div class="col-md-3 form-group tadmPadd labFontt">
              <label for="exampleInputEmail1">Admin Label:</label>
                <input type="text" class="form-control txtPadd" 
                value="" id="AdminLabelAddBuyer" name="AdminLabel"
                onkeypress="HideErr('AdminLabelAddBuyerErr');">
                 <span id="AdminLabelAddBuyerErr"></span>
            </div>
            <div class="col-md-2 form-group tadmPadd btnCen">
              <button type="button"  onclick="CopyAdminLableAddBuyer();" class="btn btn-success noBoradi mr-2">&gt;&gt;</button>
            </div>
            <div class="col-md-3 form-group tadmPadd labFontt">
              <label for="exampleInputEmail1">Partner Label:</label>
                <input type="text" class="form-control txtPadd" 
                  value="" id="PartnerLabelAddBuyer" name="PartnerLabel"
                  onkeypress="HideErr('PartnerLabelAddBuyerErr');">
                   <span id="PartnerLabelAddBuyerErr"></span>
            </div>
          </div>
          <div class="row">  
            <div class="col-md-2 form-group tadmPadd labFontt" id="BuyerTierIDInputAddBuyer" style='display:block;'>
              <label for="exampleInputEmail1">Buyer Tier ID:</label>
                <input type="text" onkeypress="HideErr('BuyerTierIDAddBuyerErr');" value="" 
                  id="BuyerTierID" name="BuyerTierIDAddBuyer" class="form-control txtPadd" placeholder="Buyer Tier ID">
                <span id="BuyerTierIDAddBuyerErr"></span>
            </div>          
            <div class="col-md-2 form-group tadmPadd labFontt">
              <label for="exampleInputEmail1">Integration File Name:</label>
                <input type="text"value="" 
                  id="IntegrationFileNameAddBuyer" name="IntegrationFileName" class="form-control txtPadd" placeholder="Integration File Name"
                  onkeypress="HideErr('IntegrationFileNameAddBuyerErr');">
                <span id="IntegrationFileNameAddBuyerErr"></span>
            </div>
            <div class="col-md-2 form-group mb-0 tadmPadd labFontt">
              <label for="PayoutCalculation">Payout Calculation:</label>
                <select class="form-control fonT" id="PayoutCalculationAddBuyer" name="PayoutCalculation" 
                  onchange="PayoutCalculationAddBuyerFunction();">
                  <option value="RevShare" >RevShare</option>
                  <option value="FixedPrice" >Fixed Price</option>
                </select>
            </div>
            <?php 
            $PayoutCalculationRevShare="style='display:block'";
            $PayoutCalculationFixedPrice="style='display:none'";
            ?>
            <div class="col-md-2 form-group tadmPadd labFontt" id="RevshareInputAddBuyer" <?php echo $PayoutCalculationRevShare ?>>
              <label for="exampleInputEmail1">RevShare: </label>
              <input type="text" class="form-control txtPadd" value="70.00" 
              placeholder="70" id="RevshareAddBuyer" name="Revshare" onkeypress="HideErr('RevshareAddBuyerErr');">
              <span id="RevshareAddBuyerErr"></span>
            </div>
            <div class="col-md-2 form-group tadmPadd labFontt" id="FixedPriceInputAddBuyer" <?php echo $PayoutCalculationFixedPrice ?> 
                id="FixedPriceInput">
              <label for="exampleInputEmail1">Fixed Price:</label>
              <input type="text" class="form-control txtPadd" value="70.00" 
              placeholder="70" id="FixedPriceAddBuyer" name="FixedPrice" onkeypress="HideErr('FixedPriceAddBuyerErr');">
              <span id="FixedPriceAddBuyerErr"></span>
            </div>
            <div class="col-md-2 form-group tadmPadd labFontt">
              <label for="exampleInputEmail1">Daily Cap:</label>
                <input type="text" value="5" 
                  id="DailyCapAddBuyer" name="DailyCap" class="form-control txtPadd" placeholder="Daily Cap"
                  onkeypress="HideErr('DailyCapAddBuyerErr');">
                <span id="DailyCapAddBuyerErr"></span>
            </div>
            <div class="col-md-2 form-group tadmPadd labFontt">
              <label for="exampleInputEmail1">Total Cap:</label>
                <input type="text" value="5" 
                  id="TotalCapAddBuyer" name="TotalCap" class="form-control txtPadd" placeholder="Total Cap"
                  onkeypress="HideErr('TotalCapAddBuyerErr');">
                <span id="TotalCapAddBuyerErr"></span>
            </div>
          </div>                         
          <div class="row">                        
            <div class="col-md-2 form-group tadmPadd labFontt">
              <label for="exampleInputEmail1">Buyer Status:</label>
                <div class="onoffswitch8">
                  <input type="checkbox" name="BuyerStatus" class="onoffswitch8-checkbox" 
                  id="BuyerStatusAddBuyer" checked="">
                  <label class="onoffswitch8-label" for="BuyerStatusAddBuyer">
                    <span class="onoffswitch8-inner"></span>
                    <span class="onoffswitch8-switch"></span>
                  </label>
                </div>
            </div>
            <div class="col-md-2 form-group tadmPadd labFontt">
              <label for="exampleInputEmail1">Buyer Mode?</label>
                <div class="onoffswitch9">
                  <input type="checkbox" name="BuyerTestMode" class="onoffswitch9-checkbox"
                     id="BuyerTestModeAddBuyer" >
                  <label class="onoffswitch9-label" for="BuyerTestModeAddBuyer">
                    <span class="onoffswitch9-inner"></span>
                    <span class="onoffswitch9-switch"></span>
                  </label>
                </div>
            </div>
            <div class="col-md-2 form-group tadmPadd labFontt">
              <label for="exampleInputEmail1">EWS Status</label>
                <div class="onoffswitch8">
                  <input type="checkbox" name="EWSStatus" class="onoffswitch8-checkbox"
                     id="EWSStatusAdd" >
                  <label class="onoffswitch8-label" for="EWSStatusAdd">
                    <span class="onoffswitch8-inner"></span>
                    <span class="onoffswitch8-switch"></span>
                  </label>
                </div>
            </div>
            <div class="col-md-2 form-group tadmPadd labFontt" id="MaxTimeOutInputAddBuyer" style='display:none;'>
              <label for="exampleInputEmail1">Max Time Out:</label>
                <input type="text" class="form-control txtPadd" placeholder="0" 
                id="MaxTimeOutAddBuyer" name="MaxTimeOut" value="0" onkeypress="HideErr('MaxTimeOutAddBuyerErr');">
                <span id="MaxTimeOutAddBuyerErr"></span>
            </div>
            <div class="col-md-2 form-group tadmPadd labFontt" id="MinTimeOutInputAddBuyer" style='display:none;'>
              <label for="exampleInputEmail1">Min Time Out:</label>
                <input type="text" class="form-control txtPadd" placeholder="0" 
                id="MinTimeOutAddBuyer" name="MinTimeOut" value="0" onkeypress="HideErr('MinTimeOutAddBuyerErr');">
                <span id="MinTimeOutAddBuyerErr"></span>
            </div>

            <div class="col-md-2 form-group tadmPadd labFontt" id="MaxPingTimeOutInputAddBuyer" style='display:none;'>
              <label for="exampleInputEmail1">Max Ping Time Out:</label>
                <input type="text" class="form-control txtPadd" placeholder="0" 
                id="MaxPingTimeOutAddBuyer" name="MaxPingTimeOut" value="0" onkeypress="HideErr('MaxPingTimeOutAddBuyerErr');">
                <span id="MaxPingTimeOutAddBuyerErr"></span>
            </div>
            <div class="col-md-2 form-group tadmPadd labFontt" id="MaxPostTimeOutInputAddBuyer" style='display:none;'>
              <label for="exampleInputEmail1">Max Post Time Out:</label>
                <input type="text" class="form-control txtPadd" placeholder="0" 
                id="MaxPostTimeOutAddBuyer" name="MaxPostTimeOut" value="0" onkeypress="HideErr('MaxPostTimeOutAddBuyerErr');">
                <span id="MaxPostTimeOutAddBuyerErr"></span>
            </div>
            <div class="col-md-2 form-group tadmPadd labFontt" id="MaxPingsPerMinuteInputAddBuyer" style='display:none;'>
              <label for="exampleInputEmail1">Max Pings Per Minute:</label>
                <input type="text" class="form-control txtPadd" placeholder="0" 
                id="MaxPingsPerMinuteAddBuyer" name="MaxPingsPerMinute" value="0" onkeypress="HideErr('MaxPingsPerMinuteAddBuyerErr');">
                <span id="MaxPingsPerMinuteAddBuyerErr"></span>
            </div>
          </div>                           
          <div class="row">
              <div class="col-md-6 form-group tadmPadd labFontt" style='display:none;' id="PingTestURLInputAddBuyer">
                <label for="exampleInputEmail1">Ping Test URL:</label>
                  <input type="text" class="form-control txtPadd" 
                  value="" id="PingTestURLAddBuyer" name="PingTestURL"
                  onkeypress="HideErr('PingTestURLAddBuyerErr');">
                   <span id="PingTestURLAddBuyerErr"></span>
              </div>
              <div class="col-md-6 form-group tadmPadd labFontt" style='display:none;' id="PingLiveURLInputAddBuyer">
                <label for="exampleInputEmail1">Ping Live URL:</label>
                  <input type="text" class="form-control txtPadd" 
                    value="" id="PingLiveURLAddBuyer" name="PingLiveURL"
                    onkeypress="HideErr('PingLiveURLAddBuyerErr');">
                     <span id="PingLiveURLAddBuyerErr"></span>
              </div>
              <div class="col-md-6 form-group tadmPadd labFontt" style='display:none;' id="PostTestURLInputAddBuyer">
                <label for="exampleInputEmail1">Post Test URL:</label>
                  <input type="text" class="form-control txtPadd" 
                  value="" id="PostTestURLAddBuyer" name="PostTestURL"
                  onkeypress="HideErr('PostTestURLAddBuyerErr');">
                   <span id="PostTestURLAddBuyerErr"></span>
              </div>
              <div class="col-md-6 form-group tadmPadd labFontt" style='display:none;' id="PostLiveURLInputAddBuyer">
                <label for="exampleInputEmail1">Post Live URL:</label>
                  <input type="text" class="form-control txtPadd" 
                    value="" id="PostLiveURLAddBuyer" name="PostLiveURL"
                    onkeypress="HideErr('PostLiveURLAddBuyerErr');">
                     <span id="PostLiveURLAddBuyerErr"></span>
              </div>
              <div class="col-md-6 form-group tadmPadd labFontt" style='display:none;' id="DirectPostTestURLInputAddBuyer">
                <label for="exampleInputEmail1">DirectPost Test URL:</label>
                  <input type="text" class="form-control txtPadd" 
                  value="" id="DirectPostTestURLAddBuyer" name="DirectPostTestURL"
                  onkeypress="HideErr('DirectPostTestURLAddBuyerErr');">
                   <span id="DirectPostTestURLAddBuyerErr"></span>
              </div>
              <div class="col-md-6 form-group tadmPadd labFontt" style='display:none;' id="DirectPostLiveURLInputAddBuyer">
                <label for="exampleInputEmail1">DirectPost Live URL:</label>
                  <input type="text" class="form-control txtPadd" 
                    value="" id="DirectPostLiveURLAddBuyer" name="DirectPostLiveURL"
                    onkeypress="HideErr('DirectPostLiveURLAddBuyerErr');">
                     <span id="DirectPostLiveURLAddBuyerErr"></span>
              </div>
          </div>
          <div class="row">
            <div class="col-md-2 form-group mb-0 tadmPadd labFontt">
                <label for="exampleFormControlSelect1">Request Method:</label>
                <select class="form-control fonT" id="RequestMethodAddBuyer" name="RequestMethod" >
                    <option value="1" >POST</option>
                    <option value="2" >GET</option>
                    <option value="3" >XmlinPost</option>
                    <option value="4" >XmlPost</option>
                    <option value="5" >InlineXML</option>
                </select>
                <span id="RequestMethodErr"></span>
            </div>
            <div class="col-md-2 form-group mb-0 tadmPadd labFontt">
                <label for="exampleFormControlSelect1">Request Header:</label>
                <select class="form-control fonT" id="RequestHeaderAddBuyer" name="RequestHeader" >
                    <option value="1" >Content-Type: application/x-www-form-urlencoded</option>
                    <option value="2" >Content-Type:text/xml; charset=utf-8 </option>
                    <option value="3" >Content-type: application/xml </option>
                    <option value="4" >Content-type: application/json</option>
                </select>
                <span id="RequestHeaderErr"></span>
            </div>
            <div class="col-md-2 form-group tadmPadd labFontt">
              <label for="exampleInputEmail1">Parameter1:</label>
                <input type="text" class="form-control txtPadd" 
                value="" id="Parameter1AddBuyer" name="Parameter1"
                onkeypress="HideErr('Parameter1AddBuyerErr');">
                 <span id="Parameter1AddBuyerErr"></span>
            </div>
            <div class="col-md-2 form-group tadmPadd labFontt">
              <label for="exampleInputEmail1">Parameter2:</label>
                <input type="text" class="form-control txtPadd" 
                value="" id="Parameter2AddBuyer" name="Parameter2"
                onkeypress="HideErr('Parameter2AddBuyerErr');">
                 <span id="Parameter2AddBuyerErr"></span>
            </div>
            <div class="col-md-2 form-group tadmPadd labFontt">
              <label for="exampleInputEmail1">Parameter3:</label>
                <input type="text" class="form-control txtPadd" 
                value="" id="Parameter3AddBuyer" name="Parameter3"
                onkeypress="HideErr('Parameter3AddErr');">
                 <span id="Parameter3AddErr"></span>
            </div>
            <div class="col-md-2 form-group tadmPadd labFontt">
              <label for="exampleInputEmail1">Parameter4:</label>
                <input type="text" class="form-control txtPadd" 
                value="" id="Parameter4AddBuyer" name="Parameter4"
                onkeypress="HideErr('Parameter4AddBuyerErr');">
                 <span id="Parameter4AddBuyerErr"></span>
            </div>
          </div>
           <div class="row">
            <div class="col-md-4 form-group labFontt tablabFontt">
              <label>File: (You Can Select Multiple Files)</label>
              <div class="custom-file">
                <input type="file" multiple class="custom-file-input" id="filesAddBuyer" name="filesAddBuyer[]" multiple>
                <label class="custom-file-label fileLabel" for="files" id="fileLabelAddBuyer">No File Chosen</label>
              </div>
            </div>                            
            <div class="col-md-4 form-group mb-0 labFontt">
              <label for="exampleFormControlTextarea1">Buyer Notes:</label>
              <textarea class="form-control" id="BuyerNotesAddBuyer" name="BuyerNotes" rows="3"></textarea>
            </div>
          </div>
          <div class="row btnFlo">
                <div class="col-md-12 form-group mb-0 labFontt">
                  <button type="button" class="btn btn-success btn-fw noBoradi mr-2" onclick="AddBuyerDetails();">Save</button>
                  <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </form>
        </div>
      </div>        
    </div>
  </div> 
  <!-- Edit Window -->
  <div class="modal fade" id="EditBuyerModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header mHBg">
          <h4 class="modal-title">Edit Buyer Details</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body" id="EditBuyerDiv">
          
        </div>
        <div class="modal-footer">

        </div>
      </div>        
    </div>
  </div>
 <!-- Clone Window -->
  <div class="modal fade" id="CloneBuyerModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header mHBg">
          <h4 class="modal-title">Clone Buyer Details</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body" id="CloneBuyerDiv">
          
        </div>
        <div class="modal-footer">

        </div>
      </div>        
    </div>
  </div>
  <?php

  ?>
  <input type="hidden" name="CompanyIDForAllSearch" id="CompanyIDForAllSearch" value='{{ $CompanyDetails->CompanyID}}'>
  <input type="hidden" name="url" id="url" value='{{URL::to("/")}}'>
  <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
  <input type="hidden" name="loading" id="loading" value='{{ asset("Admin/images/loading_image.gif") }}'>
  @include('layouts.Script')
  <script src="{{asset('Ajax/CompanyManagementEdit.js')}}"></script> 
  <script>
  </script>
</body>
