<!DOCTYPE html>
<html lang="en">
@include('layouts.Head')
<body>
  <div class="container-scroller">
    @include('layouts.Header')
    <div class="container-fluid page-body-wrapper">
      @include('layouts.Menu')
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="page-header row">
            <h3 class="page-title">
              User Management
            </h3>
          </div>
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb breadcrumb-custom">
              <li class="breadcrumb-item"><a href="#"><i class="fas fa-users-cog menu-icon"></i> Admin Tools</a></li>
              <li class="breadcrumb-item"><a href="{{ route('admin.UserManagementView') }}"><i class="fas fa-users menu-icon"></i> User Management </a></li>
              <li class="breadcrumb-item active" aria-current="page"><span><i class="fas fa-plus-square menu-icon"></i> Edit User Details </span></li>
            </ol>
          </nav>
          <div class="col-md-12">
            @if(Session::has('message'))
              <div class="alert alrtDanger {{ Session::get('alert-class', 'alert-info') }}">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>{{ Session::get('message') }}</strong>
              </div>
            @endif
          </div>
          <div class="allDet">
             <div class="col-md-12 grid-margin stretch-card">
                <div class="card">
                   <div class="card-body">                      
                     <div class="addUser">
                          <div class="row">
                            <div class="page-header-below">
                               <h3 class="page-title-below">
                                  Edit Users Details
                               </h3>
                            </div>                            
                          </div>                          
                         <form action="{{route('admin.SaveUserDetails')}}" name="UserSaveForm" id="UserSaveForm" method="POST">
                            <input type="hidden" name="UserID" id="UserID" value="{{$UserDetails->UserID}}">
                            <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                            <div class="row">
                              <div class="col-md-3 form-group tadmPadd labFontt">
                                  <label for="exampleInputEmail1">UserID:</label>
                                  <input disabled type="text" onkeypress="HideErr('UserIDErr');" value="{{$UserDetails->UserID}}"  class="form-control txtPadd" placeholder="UserID">
                                  <span id="UserIDErr" class="errordisp"></span>
                               </div>
                               <div class="col-md-3 form-group tadmPadd labFontt">
                                <label for="exampleInputEmail1">Company Name:</label>
                                  <select class="form-control fonT" id="CompanyID" name="CompanyID" onchange="HideErr('CompanyIDErr');">
                                    <option value="">Select Company</option>
                                    @foreach($CompanyList as $cl)
                                    <option value="{{$cl->CompanyID}}"
                                      <?php if($cl->CompanyID==$UserDetails->CompanyID){ echo "selected";} ?>>{{$cl->CompanyName}} (CID:{{$cl->CompanyID}})</option>
                                    @endforeach
                                  </select>
                                  <span id="CompanyIDErr" class="errordisp"></span>
                               </div>                               
                                <div class="col-md-3 form-group tadmPadd labFontt">
                                <label for="exampleInputEmail1">User Role:</label>
                                  <select class="form-control fonT" id="UserRole" name="UserRole" onchange="HideErr('UserRoleErr');">
                                    <option value="">Select User Role</option>
                                      @foreach($UserRole as $Key=>$Value)
                                      <option value="{{$Key}}"
                                      <?php if($Key==$UserDetails->UserRole){ echo "selected";} ?>>{{$Value}}</option>
                                      @endforeach
                                  </select>
                                  <span id="UserRoleErr" class="errordisp"></span>
                               </div>                               
                            </div>
                            <div class="row">
                               <div class="col-md-3 form-group tadmPadd labFontt">
                                  <label for="exampleInputEmail1">First Name:</label>
                                  <input type="text" onkeypress="HideErr('UserFirstNameErr');" value="{{$UserDetails->UserFirstName}}" id="UserFirstName" name="UserFirstName" class="form-control txtPadd" placeholder="First Name">
                                  <span id="UserFirstNameErr" class="errordisp"></span>
                               </div>
                               <div class="col-md-3 form-group tadmPadd labFontt">
                                  <label for="exampleInputEmail1">Last Name:</label>
                                  <input type="text" onkeypress="HideErr('UserLastNameErr');" value="{{$UserDetails->UserLastName}}" id="UserLastName" name="UserLastName" class="form-control txtPadd" placeholder="Last Name">
                                  <span id="UserLastNameErr" class="errordisp"></span>
                               </div>
                              <div class="col-md-3 form-group tadmPadd labFontt">
                                  <label for="exampleInputEmail1">Email:</label>
                                  <input type="email" onkeypress="HideErr('UserEmailErr');" value="{{$UserDetails->UserEmail}}" id="UserEmail" name="UserEmail" class="form-control camH" id="exampleInputEmail1" placeholder="Email">
                                  <span id="UserEmailErr" class="errordisp"></span>
                               </div>
                            </div>
                            <div class="row">
                               <div class="col-md-3 form-group tadmPadd labFontt">
                                  <label for="exampleInputEmail1">Telephone:</label>
                                  <input type="text" onkeypress="HideErr('UserTelErr');" value="{{$UserDetails->UserTel}}" id="UserTel" name="UserTel" class="form-control txtPadd" placeholder="Telephone">
                                  <span id="UserTelErr" class="errordisp"></span>
                               </div>
                               <div class="col-md-3 form-group tadmPadd labFontt">
                                  <label for="exampleInputEmail1">Skype ID:</label>
                                  <input type="text" onkeypress="HideErr('UserSkypeErr');" value="{{$UserDetails->UserSkype}}" id="UserSkype" name="UserSkype" class="form-control txtPadd" placeholder="Skype ID">
                                  <span id="UserSkypeErr" class="errordisp"></span>
                               </div> 
                               <?php if($UserDetails->UserStatus=='1'){?>
                                <div class="col-md-2 form-group tadmPadd labFontt">
                                  <label for="exampleInputEmail1">User Status:</label>
                                  <div class="onoffswitch4">
                                    <input type="checkbox" name="UserStatus" class="onoffswitch4-checkbox" id="UserStatus" 
                                      checked>
                                    <label class="onoffswitch4-label" for="UserStatus">
                                        <span class="onoffswitch4-inner"></span>
                                        <span class="onoffswitch4-switch"></span>
                                    </label>
                                  </div>
                                </div> 
                                <?php } else { ?>
                                 <div class="col-md-2 form-group tadmPadd labFontt">
                                  <label for="exampleInputEmail1">User Status:</label>
                                  <div class="onoffswitch4">
                                    <input type="checkbox" name="UserStatus" class="onoffswitch4-checkbox" id="UserStatus" 
                                      >
                                    <label class="onoffswitch4-label" for="UserStatus">
                                        <span class="onoffswitch4-inner"></span>
                                        <span class="onoffswitch4-switch"></span>
                                    </label>
                                  </div>
                                </div>                                                               
                                <?php } ?>
                                <?php if($UserDetails->ShowAPISpecs=='1'){?>                                
                                <div class="col-md-2 form-group tadmPadd labFontt">
                                  <label for="exampleInputEmail1">Show Specs:</label>
                                  <div class="onoffswitch6">
                                    <input type="checkbox" class="onoffswitch6-checkbox" 
                                    id="ShowAPISpecs" name="ShowAPISpecs" checked>
                                    <label class="onoffswitch6-label" for="myonoffswitch7">
                                        <span class="onoffswitch6-inner"></span>
                                        <span class="onoffswitch6-switch"></span>
                                    </label>
                                  </div>
                                </div>
                                <?php } else { ?>
                                <div class="col-md-2 form-group tadmPadd labFontt">
                                  <label for="exampleInputEmail1">Show Specs:</label>
                                  <div class="onoffswitch6">
                                    <input type="checkbox" class="onoffswitch6-checkbox" 
                                    id="ShowAPISpecs" name="ShowAPISpecs" >
                                    <label class="onoffswitch6-label" for="ShowAPISpecs">
                                        <span class="onoffswitch6-inner"></span>
                                        <span class="onoffswitch6-switch"></span>
                                    </label>
                                  </div>
                                </div>
                                <?php } ?>
                                <?php if($UserDetails->ShowAffLinks=='1'){?>                                 
                                <div class="col-md-2 form-group tadmPadd labFontt">
                                  <label for="exampleInputEmail1">Show Affiliate Links:</label>
                                  <div class="onoffswitch7">
                                    <input type="checkbox" class="onoffswitch7-checkbox" 
                                    id="ShowAffLinks" name="ShowAffLinks" checked>
                                    <label class="onoffswitch7-label" for="ShowAffLinks">
                                        <span class="onoffswitch7-inner"></span>
                                        <span class="onoffswitch7-switch"></span>
                                    </label>
                                  </div>
                                </div>
                                <?php } else { ?>
                                <div class="col-md-2 form-group tadmPadd labFontt">
                                  <label for="exampleInputEmail1">Show Affiliate Links:</label>
                                  <div class="onoffswitch7">
                                    <input type="checkbox" class="onoffswitch7-checkbox"
                                    id="ShowAffLinks" name="ShowAffLinks">
                                    <label class="onoffswitch7-label" for="ShowAffLinks">
                                        <span class="onoffswitch7-inner"></span>
                                        <span class="onoffswitch7-switch"></span>
                                    </label>
                                  </div>
                                </div>
                                <?php } ?>                                                              
                            </div>
                            <div class="row" >
                               <div class="col-md-3 form-group tadmPadd labFonsize">
                                  <div class="form-check form-check-flat form-check-primary adComp adComp-primary mt-4">
                                     <label class="form-check-label adComp-label">
                                     <input type="checkbox" class="form-check-input" id="ChangePassword" name="ChangePassword">
                                     Do You Want To Change Password? 
                                     </label>
                                  </div>
                               </div>
                               <div class="col-md-3 form-group tadmPadd labFontt" id="PasswordDiv" style="display:none;">
                                  <label for="exampleInputPassword1">Password:</label>
                                  <input type="text" onkeypress="HideErr('PasswordErr');" class="form-control camH" id="Password" name="Password" placeholder="Password">
                                  <span id="PasswordErr" class="errordisp"></span>
                               </div>
                               <div class="col-md-3 form-group tadmPadd labFontt" id="ConfirmPasswordDiv" style="display:none;">
                                  <label for="exampleInputPassword1">Confirm Password:</label>
                                  <input type="text" onkeypress="HideErr('ConfirmPasswordErr');" class="form-control camH" id="ConfirmPassword" name="ConfirmPassword" placeholder="Confirm Password">
                                  <span id="ConfirmPasswordErr" class="errordisp"></span>
                               </div>
                               <div class="col-md-3 form-group tadmPadd labFontt" id="GeneratePasswordDiv" style="display:none;">
                                  <label class="gP" for="exampleInputPassword1">Generate Password:</label>
                                  <button type="button" onclick="GeneratePassword();" class="btn btn-success fonT noBoradi cbtnPadd sercFontt gpBg">Generate Password</button>
                               </div>
                            </div>
                             <div class="row" >
                               <div class="col-md-4 form-group tadmPadd labFonsize">
                                  <div class="form-check form-check-flat form-check-primary adComp adComp-primary mt-4">
                                     <label class="form-check-label adComp-label">
                                     <input type="checkbox" class="form-check-input" id="SendCredentials" name="SendCredentials">
                                      Would you like to email credentials to the user?
                                     </label>
                                  </div>
                               </div>
                               <div class="col-md-4 form-group mb-0 labFontt">
                                  <label for="exampleFormControlTextarea1">User Notes:</label>
                                  <textarea class="form-control" id="UserNotes" name="UserNotes" rows="3">{{$UserDetails->UserNotes}}</textarea>
                                </div>
                            </div>
                            <div class="row">
                               <button type="button" id="UserSaveBtn" class="btn btn-success btn-fw noBoradi mr-2">Update</button>
                               <a href="{{ route('admin.UserManagementView') }}" class="btn btn-danger btn-fw noBoradi">Cancel</a>
                            </div>
                         </form>
                      </div>
                   </div>
                </div>
             </div>
          </div>
       </div>
       @include('layouts.Footer')
    </div>
    </div>
  </div>
  <input type="hidden" name="url" id="url" value='{{URL::to("/")}}'>
  <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
  <input type="hidden" name="loading" id="loading" value='{{ asset("Admin/images/loading_image.gif") }}'>
  @include('layouts.Script')
  <script src="{{asset('Ajax/UserManagement.js')}}"></script> 
  <script>
  $(document).ready(function() {
    $('#sandbox-container .input-daterange').datepicker({});

    $('#datepicker').datepicker({
      weekStart: 0,
      daysOfWeekHighlighted: "6,0",
      autoclose: true,
      todayHighlight: true,
      dateFormat: 'mm/dd/yy'
    });
    $('#datepicker').datepicker("setDate", new Date());
  });
  </script>
</body>
