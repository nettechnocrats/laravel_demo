<!DOCTYPE html>
<html lang="en">
@include('layouts.Head')
<body>
  <div class="container-scroller">
    @include('layouts.Header')
    <div class="container-fluid page-body-wrapper">
      @include('layouts.Menu')
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper newConWrap">
          <div class="page-header row">
            <div class="col-md-6 col-sm-6">
              <h3 class="page-title">
                <i class="fa fa-user-circle menu-icon"></i> User Profile
              </h3>
            </div>
          </div>
          @if(Session::has('message'))
            <div class="alert alrtDanger {{ Session::get('alert-class', 'alert-info') }}">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <strong>{{ Session::get('message') }}</strong>
            </div>
          @endif
          <form id="ProfileForm" action="{{route('admin.ProfileUpdate')}}" method="POST">
          <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
          <div class="row grid-margin noRghtPadd">
            <div class="col-md-6 col-12 noRghtPadd">
              <div class="user-info-form">
                <div class="user-info-heading">
                  <h5>Company Info</h5>
                </div>
                <div class="userinfo-body">
                  <div class="form-group row noRghtPadd pt-3 pl-3 mb-2">
                    <label class="col-sm-3 control-label pt-2"><strong>Name</strong></label>
                    <div class="col-sm-8">
                      <input type="text" readonly class="form-control formBord" onkeypress="HideErr('CompanyNameErr');" name="CompanyName" id="CompanyName" placeholder="Name" value="{{$GetUserInfo->CompanyName}}">
                      <span id="CompanyNameErr"></span>
                    </div>
                  </div>
                  <div class="form-group row noRghtPadd pt-3 pl-3 mb-2">
                    <label class="col-sm-3 control-label pt-2"><strong>Address 1</strong></label>
                    <div class="col-sm-8">
                      <input type="text" readonly  class="form-control formBord" onkeypress="HideErr('CompanyAddress1Err');" name="CompanyAddress1" id="CompanyAddress1" placeholder="Address 1" value="{{$GetUserInfo->CompanyAddress1}}">
                      <span id="CompanyAddress1Err"></span>
                    </div>
                  </div>
                  <div class="form-group row noRghtPadd pt-3 pl-3 mb-2">
                    <label class="col-sm-3 control-label pt-2"><strong>Address 2</strong></label>
                    <div class="col-sm-8">
                      <input type="text" readonly class="form-control formBord" onkeypress="HideErr('CompanyAddress2Err');" name="CompanyAddress2" id="CompanyAddress2" placeholder="Address 2" value="{{$GetUserInfo->CompanyAddress2}}">
                      <span id="CompanyAddress2Err"></span>
                    </div>
                  </div>
                  <div class="form-group row noRghtPadd pt-3 pl-3 mb-2">
                    <label class="col-sm-3 control-label pt-2"><strong>City</strong></label>
                    <div class="col-sm-8">
                      <input type="text" readonly class="form-control formBord" onkeypress="HideErr('CompanyCityErr');" name="CompanyCity" id="CompanyCity" placeholder="City" value="{{$GetUserInfo->CompanyCity}}">
                      <span id="CompanyCityErr"></span>
                    </div>
                  </div>
                  <div class="form-group row noRghtPadd pt-3 pl-3 mb-2">
                    <label class="col-sm-3 control-label pt-2"><strong>State/County</strong></label>
                    <div class="col-sm-8">
                      <input type="text" readonly class="form-control formBord" onkeypress="HideErr('CompanyStateCountryErr');" name="CompanyStateCountry" id="CompanyStateCountry" placeholder="State/County" value="{{$GetUserInfo->CompanyStateCounty}}">
                      <span id="CompanyStateCountryErr"></span>
                    </div>
                  </div>
                  <div class="form-group row noRghtPadd pt-3 pl-3 mb-2">
                    <label class="col-sm-3 control-label pt-2"><strong>Zip</strong></label>
                    <div class="col-sm-8">
                      <input type="text" readonly class="form-control formBord" onkeypress="HideErr('CompanyZipPostCodeErr');" name="CompanyZipPostCode" id="CompanyZipPostCode" placeholder="Zip" value="{{$GetUserInfo->CompanyZipPostCode}}">
                      <span id="CompanyZipPostCodeErr"></span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6 col-12">
              <div class="user-info-form">
                <div class="user-info-heading">
                  <h5>User Info</h5>
                </div>
                <div class="userinfo-body">
                  <div class="form-group row noRghtPadd pt-3 pl-3 mb-2">
                    <label class="col-sm-3 control-label pt-2"><strong>First Name</strong></label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control formBord" onkeypress="HideErr('UserFirstNameErr');"  name="UserFirstName" id="UserFirstName" placeholder="First Name" value="{{$GetUserInfo->UserFirstName}}">
                      <span id="UserFirstNameErr"></span>
                    </div>
                  </div>
                  <div class="form-group row noRghtPadd pt-3 pl-3 mb-2">
                    <label class="col-sm-3 control-label pt-2"><strong>Last Name</strong></label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control formBord" onkeypress="HideErr('UserLastNameErr');"  name="UserLastName" id="UserLastName" placeholder="Last Name" value="{{$GetUserInfo->UserLastName}}">
                      <span id="UserLastNameErr"></span>
                    </div>
                  </div>
                  <div class="form-group row noRghtPadd pt-3 pl-3 mb-2">
                    <label class="col-sm-3 control-label pt-2"><strong>Email Address</strong></label>
                    <div class="col-sm-8">
                      <input type="text"  readonly class="form-control formBord" id="useremail" placeholder="Email Address" value="{{$GetUserInfo->UserEmail}}">
                    </div>
                  </div>
                  </div>
                  <div class="user-info-heading">
                    <h5 class="pt-2">Change Password</h5>
                  </div>
                  <div class="userinfo-body">
                  <div class="form-group row noRghtPadd pt-3 pl-3 mb-2">
                    <label class="col-sm-3 control-label pt-2"><strong>New Password</strong></label>
                    <div class="col-sm-8">
                      <input type="password" class="form-control formBord" onkeypress="HideErr('UserPasswordErr'),HideErr('UserPasswordConfirmErr');" placeholder="Password" name="UserPassword" id="UserPassword">
                      <span id="UserPasswordErr"></span>
                    </div>
                  </div>
                  <div class="form-group row noRghtPadd pt-3 pl-3 mb-2">
                    <label class="col-sm-3 control-label pt-2"><strong>Confirm Password</strong></label>
                    <div class="col-sm-8">
                      <input type="password" class="form-control formBord" onkeypress="HideErr('UserPasswordConfirmErr');" placeholder="Confirm Password" name="UserPasswordConfirm" id="UserPasswordConfirm">
                      <span id="UserPasswordConfirmErr"></span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="subm-btn mt-5">
                <div class="form-group pt-2">
                  <div class="col-sm-12 text-center">
                    <button type="button" class="btn btn-info" id="ProfileBtn">Submit</button>
                    <a href="{{route('admin.Dashboard')}}" class="btn btn-danger" id="CancelBtn">Cancel</a>
                  </div>
                </div>
              </div>
        </form>
        </div>
        @include('layouts.Footer')
      </div>
    </div>
  </div>
  @include('layouts.Script')
  <input type="hidden" name="url" id="url" value='{{URL::to("/")}}'>
  <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
  <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js"></script> 
  <script src="{{asset('Ajax/Profile.js')}}"></script> 
</script>
</body>
