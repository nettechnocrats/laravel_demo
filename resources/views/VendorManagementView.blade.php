<!DOCTYPE html>
<html lang="en">
@include('layouts.Head')
<body>
  <div class="container-scroller">
    @include('layouts.Header')
    <div class="container-fluid page-body-wrapper">
      @include('layouts.Menu')
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="page-header row">
            <h3 class="page-title">
              Vendor Management
            </h3>
          </div>
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb breadcrumb-custom">
              <li class="breadcrumb-item"><a href="#"><i class="fas fa-users-cog menu-icon"></i> Admin Tools</a></li>
              <li class="breadcrumb-item active" aria-current="page"><span><i class="fas fa-user-plus menu-icon"></i> Vendor Management </span></li>
            </ol>
          </nav>
          <div class="col-md-12" id="VendorMessage">
            @if(Session::has('message'))
              <div class="alert alrtDanger {{ Session::get('alert-class', 'alert-info') }}">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>{{ Session::get('message') }}</strong>
              </div>
            @endif
          </div>
          <div class="allDet">
            <div class="col-lg-12 col-md-12 col-sm-12 col-12 comP">
              <div class="row">  
                <div class="col-md-3 styBord">
                  <div class="col">
                    <select class="form-control fonT boxH" id="Company">
                      <option value="All">All Company</option>
                     
                    </select>
                  </div>
                  <div class="col-md-12 form-group tadmPadd mb-0">
                    <div class="form-check form-check-flat form-check-primary adComp adComp-primary disPlayt">
                      <label class="form-check-label adComp-label sa">
                      <input type="checkbox" class="form-check-input" id="ShowInactive" 
                        onclick="GetCompanyUsingShwowInactiveAndTest();">
                        Show Inactive
                      </label>
                    </div>
                    <div class="form-check form-check-flat form-check-primary adComp adComp-primary disPlayt">
                      <label class="form-check-label adComp-label si">
                      <input type="checkbox" class="form-check-input" id="ShowTest"
                        onclick="GetCompanyUsingShwowInactiveAndTest();">
                        Show Test
                      </label>
                    </div>
                  </div>
                </div>             
                <div class="col-md-2 form-group newFoGrp">
                  <select class="form-control fonT" id="Campaign" >
                      @foreach($CampaignDropDown as $cdd)
                      <option value="{{$cdd->CampaignID}}">{{$cdd->CampaignName}}</option>
                      @endforeach
                  </select>
                </div>
                <div class="col-md-2 form-group tadmPadd labFontt newFoGrp ">
                  <input type="text" class="form-control txtPadd boxH" id="VendorID" placeholder="Vendor ID">
                </div>
                <div class="col-md-2 form-group tadmPadd labFontt newFoGrp ">
                  <input type="text" class="form-control txtPadd boxH" id="VendorName" placeholder="Admin Label">
                </div>
                <div class="col-md-2 form-group newFoGrp">
                  <select class="form-control fonT" id="VendorStatus">                     
                      <option value="All">All Status</option>
                      <option value="1">Active</option>
                      <option value="0">InActive</option>                      
                  </select>
                </div>
                <div class="col-md-1 form-group tadmPadd labFontt newFoGrp">
                   <button type="button" class="btn btn-success fonT mW nF tW fl noBoradi sercFontt"
                   id="SearchBtn" onclick="SearchVendorList();"><i class="fa fa-search"></i> Search </button>
                </div> 
              </div>
              <div class="row">
                <div class="col-md-3 offset-md-9 form-group tadmPadd labFontt newFoGrp">
                  <button type="button" class="btn btn-success btn-fw cbtnPadd sercFontt fl dwSCSV Flo"
                      onclick="AddVendorModal();">Add New Vendor</button>
                </div>
              </div>
              <div id="dataTable_wrapper" class="dataTables_wrapper dt-bootstrap4">
                <input type="hidden" name="numofrecords" id="numofrecords" value='10'>
              </div>
            </div>
          </div>
       </div>
       @include('layouts.Footer')
    </div>
    </div>
  </div>

  <!-- Add Window -->
  <div class="modal fade" id="AddVendorModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header mHBg">
          <h4 class="modal-title">Add New Vendor</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <form action="<?php echo route('admin.AddVendorDetails'); ?>" name="VendorAddForm" id="VendorAddForm" method="POST">                          
            <input type="hidden" name="_token" id="_token" value="<?php echo csrf_token(); ?>">  
            <input type="hidden" name="CampaignTypeAdd" id="CampaignTypeAdd" value="">                        
            <div class="row">
              <div class="col-md-12" id="MessageAdd">              
              </div>
            </div>
            <div class="row">
              <div class="col-md-4 form-group mb-0 tadmPadd labFontt">
                <label for="exampleFormControlSelect1">Company:</label>
                  <select class="form-control fonT" id="CompanyAdd" name="Company"
                    onchange="HideErr('CompanyAddErr');">
                    <option value="">Select Company</option>
                    <?php foreach($CompanyList as $cl){ ?>
                    <option value="<?php echo $cl->CompanyID; ?>">
                      <?php echo $cl->CompanyName; ?></option>
                    <?php } ?>                                 
                  </select>
                  <span id="CompanyAddErr"></span>
                   <div class="form-check form-check-flat form-check-primary adComp adComp-primary disPlay tabdisPlay tMrg">
                    <label class="form-check-label adComp-label">
                    <input type="checkbox" class="form-check-input" id="ShowInactiveAdd" 
                    onclick="GetCompanyUsingShwowInactiveAndTestAdd();">
                      Show Inactive
                    <i class="input-helper"></i></label>
                  </div>
                  <div class="form-check form-check-flat form-check-primary adComp adComp-primary disPlay tabdisPlay tMrg">
                    <label class="form-check-label adComp-label">
                    <input type="checkbox" class="form-check-input" id="ShowTestAdd" 
                    onclick="GetCompanyUsingShwowInactiveAndTestAdd();">
                      Show Test
                    <i class="input-helper"></i></label>
                  </div>
              </div>
              <div class="col-md-3 form-group mb-0 tadmPadd labFontt">
                  <label for="exampleFormControlSelect1">Campaign:</label>
                  <select class="form-control fonT" id="CampaignAdd" name="Campaign" 
                      onchange="GetCampaignDetailsAdd(),HideErr('CompanyAddErr');">
                      <?php foreach($CampaignDropDown as $cdd){ ?>
                      <option value="<?php echo $cdd->CampaignID; ?>">
                          <?php echo $cdd->CampaignName; ?></option>
                      <?php } ?>  
                  </select>
                  <span id="CampaignAddErr"></span>
              </div>
              <div class="col-md-2 form-group tadmPadd labFontt">
                <label for="exampleInputEmail1">Admin Label:</label>
                  <input type="text" class="form-control txtPadd" 
                  value="" id="AdminLabelAdd" name="AdminLabel"
                  onkeypress="HideErr('AdminLabelAddErr');">
                  <span id="AdminLabelAddErr"></span>
              </div>
              <div class="col-md-1 form-group tadmPadd btnCen">
                <button type="button" id="CopyAdminLable" onclick="CopyAdminLableAdd();" class="btn btn-success noBoradi mr-2">&gt;&gt;</button>
              </div>
              <div class="col-md-2 form-group tadmPadd labFontt">
                <label for="exampleInputEmail1">Partner Label:</label>
                  <input type="text" class="form-control txtPadd" 
                    value="" id="PartnerLabelAdd" name="PartnerLabel"
                    onkeypress="HideErr('PartnerLabelAddErr');">
                  <span id="PartnerLabelAddErr"></span>
              </div>
            </div> 
            <div class="row">
              <div class="col-md-3 form-group mb-0 tadmPadd labFontt">
                <label for="PayoutCalculation">Payout Calculation:</label>
                  <select class="form-control fonT" id="PayoutCalculationAdd" name="PayoutCalculation" >
                    <option value="RevShare" >RevShare</option>
                    <option value="FixedPrice" >Fixed Price</option>
                    <option value="TierPrice" >Tier Price</option>
                    <option value="MinPrice" >Min Price</option>
                    <option value="BucketPrice" >Bucket Price</option>
                    <option value="BucketSystemBySubID" >Bucket System By SubID</option>
                  </select>
              </div>
              <?php 
                $PayoutCalculationRevShare="style='display:block'";
                $PayoutCalculationTierPrice="style='display:none'";
                $PayoutCalculationFixedPrice="style='display:none'";
              ?>
              <div class="col-md-2 form-group tadmPadd labFontt" <?php echo $PayoutCalculationRevShare ?>  
                  id="RevsharePercentInputAdd">
                <label for="exampleInputEmail1">Revshare: </label>
                <input type="text" class="form-control txtPadd" value="70" 
                placeholder="70" id="RevshareAdd" name="Revshare" onkeypress="HideErr('RevshareAddErr');">
                <span id="RevshareAddErr"></span>
              </div>
              <div class="col-md-2 form-group tadmPadd labFontt" <?php echo $PayoutCalculationFixedPrice ?> 
                  id="FixedPriceInputAdd">
                <label for="exampleInputEmail1">Fixed Price:</label>
                <input type="text" class="form-control txtPadd" value="1.00" 
                placeholder="70" id="FixedPriceAdd" name="FixedPrice" onkeypress="HideErr('FixedPriceAddErr');">
                <span id="FixedPriceAddErr"></span>
              </div>
              <div class="col-md-5 form-group tadmPadd labFontt" <?php echo $PayoutCalculationTierPrice ?> 
                  id="TierPriceInputAdd">
                <label for="exampleInputEmail1">Revshare Percent (for when Tier Payout is $0.00):</label>
                <input type="text" class="form-control txtPadd" value="70"
                 placeholder="70" id="TierPriceAdd" name="TierPrice" onkeypress="HideErr('TierPriceAddErr');">
                <span id="TierPriceAddErr"></span>
              </div>
              <div class="col-md-2 form-group tadmPadd labFontt">
                <label for="exampleInputEmail1">Daily Cap:</label>
                  <input type="text"  value="5" 
                    id="DailyCapAdd" name="DailyCap" class="form-control txtPadd" placeholder="Daily Cap"
                    onkeypress="HideErr('DailyCapAddErr');">
                  <span id="DailyCapAddErr"></span>
              </div>
              <div class="col-md-2 form-group tadmPadd labFontt">
                <label for="exampleInputEmail1">Total Cap:</label>
                  <input type="text" value="5" 
                    id="TotalCapAdd" name="TotalCap" class="form-control txtPadd" placeholder="Total Cap"
                    onkeypress="HideErr('TotalCapAddErr');">
                  <span id="TotalCapAddErr"></span>
              </div>
            </div>
            <div class="row">       
              <div class="col-md-2 form-group tadmPadd labFontt">
                <label for="exampleInputEmail1">Vendor Status:</label>
                  <div class="onoffswitch8">
                    <input type="checkbox" name="VendorStatus" class="onoffswitch8-checkbox" 
                    id="VendorStatusAdd" checked="">
                    <label class="onoffswitch8-label" for="VendorStatusEdit">
                      <span class="onoffswitch8-inner"></span>
                      <span class="onoffswitch8-switch"></span>
                    </label>
                  </div>
              </div>
              <div class="col-md-2 form-group tadmPadd labFontt">
                <label for="exampleInputEmail1">Vendor Mode?</label>
                  <div class="onoffswitch9">
                    <input type="checkbox" name="VendorTestMode" class="onoffswitch9-checkbox"
                       id="VendorTestModeAdd" >
                    <label class="onoffswitch9-label" for="VendorTestMode">
                      <span class="onoffswitch9-inner"></span>
                      <span class="onoffswitch9-switch"></span>
                    </label>
                  </div>
              </div>
              
             
              <?php 
              $MaxTimeOutInput = "style='display:none;'";
              $MaxPingTimeOutInput = "style='display:none;'";
              $MaxPostTimeOutInput = "style='display:none;'";
              ?>
               <div class="col-md-2 form-group tadmPadd labFontt" id="MaxTimeOutInputAdd" <?php echo $MaxTimeOutInput; ?>>
                <label for="exampleInputEmail1">Max Time Out:</label>
                  <input type="text" class="form-control txtPadd" placeholder="0" 
                  id="MaxTimeOutAdd" name="MaxTimeOut" value="0" onkeypress="HideErr('MaxTimeOutAddErr');">
                  <span id="MaxTimeOutAddErr"></span>
              </div>
              <div class="col-md-2 form-group tadmPadd labFontt" id="MaxPingTimeOutInputAdd" <?php echo $MaxPingTimeOutInput; ?>>
                <label for="exampleInputEmail1">Max Ping Time Out:</label>
                  <input type="text" class="form-control txtPadd" placeholder="0" 
                  id="MaxPingTimeOutAdd" name="MaxPingTimeOut" value="0" onkeypress="HideErr('MaxPingTimeOutAddErr');">
                  <span id="MaxPingTimeOutAddErr"></span>
              </div>
              <div class="col-md-2 form-group tadmPadd labFontt" id="MaxPostTimeOutInputAdd" <?php echo $MaxPostTimeOutInput; ?>>
                <label for="exampleInputEmail1">Max Post Time Out:</label>
                  <input type="text" class="form-control txtPadd" placeholder="0" 
                  id="MaxPostTimeOutAdd" name="MaxPostTimeOut" value="0" onkeypress="HideErr('MaxPostTimeOutAddErr');">
                  <span id="MaxPostTimeOutAddErr"></span>
              </div>
              <div class="col-md-2 form-group tadmPadd labFontt">
                <label for="exampleInputEmail1">Tracking Pixel Status?</label>
                  <div class="onoffswitch10">
                    <input type="checkbox" name="TrackingPixelStatus" class="onoffswitch10-checkbox"
                     id="TrackingPixelStatus" checked="">
                    <label class="onoffswitch10-label" for="TrackingPixelStatus">
                      <span class="onoffswitch10-inner"></span>
                      <span class="onoffswitch10-switch"></span>
                    </label>
                  </div>
              </div>
            </div>
            <div class="row">  
              <div class="col-md-2 form-group mb-0 tadmPadd labFontt">
                <label for="exampleFormControlSelect1">Tracking Pixel Type:</label>
                  <select class="form-control fonT" id="TrackingPixelType" name="TrackingPixelType">
                    <option value="Pixel" >Pixel</option>
                    <option value="PostBack" >Post Back</option>
                  </select>
              </div>
              <div class="col-md-2 form-group mb-0 labFontt">
                <label for="exampleFormControlSelect1">Insert Token:</label>
                  <select class="form-control" id="insert_Token_type" name="insert_Token_type" onchange="updateTextBox(this)">
                    <option value="aid">aid</option>
                    <option value="SubID">SubID</option>
                    <option value="TestMode">TestMode</option>
                    <option value="TransactionID">TransactionID</option>
                    <option value="VendorLeadPrice">VendorLeadPrice</option>
                </select>
              </div>
              <div class="col-md-5 form-group mb-0 labFontt">
                <label for="exampleFormControlTextarea1">Tracking Pixel Code:</label>
                <textarea class="form-control" id="TrackingPixelCode" name="TrackingPixelCode" rows="3"></textarea>
              </div>
            </div>
            <div class="row">              
              <div class="col-md-5 form-group mb-0 labFontt">
                <label for="exampleFormControlTextarea1">Vendor Notes:</label>
                <textarea class="form-control" id="VendorNotes" name="VendorNotes" rows="3"></textarea>
              </div>
            </div>
            <div class="row btnFlo">
                <div class="col-md-12 form-group mb-0 labFontt">
                  <button type="button" class="btn btn-success btn-fw noBoradi mr-2" onclick="AddVendorDetails();">Save</button>
                  <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </div>
          </form>   
        </div>
        <div class="modal-footer">

        </div>
      </div>        
    </div>
  </div> 
  <!-- Edit Window -->
  <div class="modal fade" id="EditVendorModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header mHBg">
          <h4 class="modal-title">Edit Vendor Details</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body" id="EditVendorDiv">
          
        </div>
        <div class="modal-footer">

        </div>
      </div>        
    </div>
  </div>
 <!-- Clone Window -->
  <div class="modal fade" id="CloneVendorModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header mHBg">
          <h4 class="modal-title">Clone Vendor Details</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body" id="CloneVendorDiv">
          
        </div>
        <div class="modal-footer">

        </div>
      </div>        
    </div>
  </div>
  <input type="hidden" name="url" id="url" value='{{URL::to("/")}}'>
  <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
  <input type="hidden" name="loading" id="loading" value='{{ asset("Admin/images/loading.gif") }}'>
  @include('layouts.Script')
  <script src="{{asset('Ajax/VendorManagement.js')}}"></script> 
  <script>
  $(document).ready(function() {
    $('#sandbox-container .input-daterange').datepicker({});

    $('#datepicker').datepicker({
      weekStart: 0,
      daysOfWeekHighlighted: "6,0",
      autoclose: true,
      todayHighlight: true,
      dateFormat: 'mm/dd/yy'
    });
    $('#datepicker').datepicker("setDate", new Date());

    GetCompanyUsingShwowInactiveAndTest();
  });
  </script>
</body>
