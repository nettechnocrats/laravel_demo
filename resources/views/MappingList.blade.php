<!DOCTYPE html>
<html lang="en">
@include('layouts.Head')
<body>
  <div class="container-scroller">
    @include('layouts.Header')
    <div class="container-fluid page-body-wrapper">
      @include('layouts.Menu')
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="page-header row">
            <h3 class="page-title">
              User Management
            </h3>
          </div>
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb breadcrumb-custom">
              <li class="breadcrumb-item"><a href="#"><i class="fas fa-users-cog menu-icon"></i> Admin Tools</a></li>
              <li class="breadcrumb-item active" aria-current="page"><span><i class="fas fa-file-alt menu-icon"></i> User Management </span></li>
            </ol>
          </nav>
           <div class="col-md-12">
            @if(Session::has('message'))
              <div class="alert alrtDanger {{ Session::get('alert-class', 'alert-info') }}">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>{{ Session::get('message') }}</strong>
              </div>
            @endif
          </div>
          <div class="allDet">
             <div class="col-lg-12 col-md-12 col-sm-12 col-12 comP">
                <form class="boxS">
                <div class="row">
                   <div class="col-md-8 row">
                      <div class="col-md-4">
                          <select class="form-control fonT" id="Campaign" onchange="GetVendorBuyerUsingCampaignID();">
                              @foreach($CampaignDropDown as $cdd)
                              <option value="{{$cdd->CampaignID}}">{{$cdd->CampaignName}}</option>
                              @endforeach
                          </select>
                      </div>
                      <div class="col-md-4">
                         <div class="col-md-12 form-group mb-0 tadmPadd">
                            <select class="form-control fonT" id="VendorCompany" onchange="GetVendorUsingVendorCompany();">
                              <option value="All">All Vendor Company</option>                              
                           </select>
                         </div>
                         <div class="row">
                            <div class="col-md-12 form-group tadmPadd mb-0">
                               <div class="form-check form-check-flat form-check-primary adComp adComp-primary disPlayt">
                                  <label class="form-check-label adComp-label sa">
                                  <input type="checkbox" class="form-check-input" id="ShowInactiveVendorCompanyID">
                                  Show Inactive
                                  </label>
                               </div>
                               <div class="form-check form-check-flat form-check-primary adComp adComp-primary disPlayt">
                                  <label class="form-check-label adComp-label si">
                                  <input type="checkbox" class="form-check-input" id="ShowTestVendorCompanyID">
                                  Show Test
                                  </label>
                               </div>
                            </div>
                         </div>
                      </div>
                      <div class="col-md-4">
                         <div class="col-md-12 form-group mb-0 tadmPadd">
                            <select class="form-control fonT" id="BuyerCompany" onchange="GetBuyerUsingBuyerCompany();">
                              <option value="All">All Buyer Company</option>
                                
                            </select>
                         </div>
                         <div class="row">
                            <div class="col-md-12 form-group tadmPadd mb-0">
                               <div class="form-check form-check-flat form-check-primary adComp adComp-primary disPlayt">
                                  <label class="form-check-label adComp-label sa">
                                  <input type="checkbox" class="form-check-input" id="ShowInactiveBuyerCompanyID">
                                  Show Inactive
                                  </label>
                               </div>
                               <div class="form-check form-check-flat form-check-primary adComp adComp-primary disPlayt">
                                  <label class="form-check-label adComp-label si">
                                  <input type="checkbox" class="form-check-input" id="ShowTestBuyerCompanyID">
                                  Show Test
                                  </label>
                               </div>
                            </div>
                         </div>
                      </div>
                      <div class="col-md-4">
                         <div class="col-md-12 form-group mb-0 tadmPadd">
                             <select class="form-control fonT" id="Vendor" >
                              <option value="All">All Vendor</option>
                               
                           </select>
                         </div>
                         <div class="row">
                            <div class="col-md-12 form-group tadmPadd mb-0">
                               <div class="form-check form-check-flat form-check-primary adComp adComp-primary disPlayt">
                                  <label class="form-check-label adComp-label sa">
                                  <input type="checkbox" class="form-check-input" id="ShowInactiveVendor" onchange="GetVendorUsingVendorCompany();">
                                  Show Inactive
                                  </label>
                               </div>
                               <div class="form-check form-check-flat form-check-primary adComp adComp-primary disPlayt" onchange="GetVendorUsingVendorCompany();">
                                  <label class="form-check-label adComp-label si" id="ShowTestVendor">
                                  <input type="checkbox" class="form-check-input">
                                  Show Test
                                  </label>
                               </div>
                            </div>
                         </div>
                      </div>
                      
                   </div>
                   <div class="col-md-4 row ml-3">
                      <div class="col-md-4">
                         <button type="button" class="btn btn-success cbtnPadd sercFontt"
                          onclick="SearchMappingList();">
                          <i class="fa fa-search"></i>Search</button>
                      </div>
                      <div class="col-md-4">
                         <button class="btn btn-success cbtnPadd btC sercFontt">Sort</button>
                      </div>
                      <div class="col-md-4">
                         <button class="btn btn-success cbtnPadd btC sercFontt">Save</button>
                      </div>                      
                      <div class="row mt-3 ml-1">
                         <div class="col-md-6">
                            <a href="#" data-toggle="modal" data-target="#addnewmapping"><button class="btn btn-success btn-fw cbtnPadd sercFontt mb-3">Add New Mapping</button></a>
                         </div>
                         <div class="col-md-6">
                            <a href="#" data-toggle="modal" data-target="#clonemapping"><button class="btn btn-success btn-fw cbtnPadd sercFontt mb-3 mr-2" >Clone Mappings</button></a>
                         </div>
                      </div>
                   </div>
                </div>
                <div class="row">
                   <div class="table-responsive">
                      <table class="table table-bordered tableFont txtL nTabl">
                         <thead class="thead-dark">
                            <tr>
                               <th scope="col" style="padding: 0.684rem; font-size: 0.8rem;">Campaign</th>
                               <th scope="col" style="padding: 0.684rem; font-size: 0.8rem;">Vendor Company</th>
                               <th scope="col" style="padding: 0.684rem; font-size: 0.8rem;">Vendor</th>
                               <th scope="col" style="padding: 0.684rem; font-size: 0.8rem;">Fixed Price</th>
                               <th scope="col" style="padding: 0.684rem; width: 8%; font-size: 0.8rem;">Sort Order</th>
                               <th scope="col" style="padding: 0.684rem; font-size: 0.8rem;">Status</th>
                               <th scope="col" style="padding: 0.684rem; font-size: 0.8rem;">Action</th>
                            </tr>
                         </thead>
                         <tbody>
                            <tr>
                               <th scope="row" style="vertical-align: middle;"><span style="color: Dodgerblue; "><i class="fas fa-arrows-alt fa-lg fa-fw"></i></span> Internal Agent (VID:1/CID:1)</th>
                               <td>AllTiers</td>
                               <td>Payday Test Buyer $30 (BID:116/CID:1)</td>
                               <td style="text-align: center;">30.00</td>
                               <td style="text-align: center; width: 6%;">
                                  <div class="col-md-12 form-group tadmPadd labFontt mb-0">
                                     <input type="text" class="form-control txtPadd1" placeholder="1">
                                  </div>
                               </td>
                               <td style="text-align: center;">
                                  <div class="custom-control custom-checkbox noCush">
                                     <input type="checkbox" checked="" class="custom-control-input noCush-input" id="customCheck2" name="example2">
                                     <label class="custom-control-label noCush-label mb-3" for="customCheck2"></label>
                                  </div>
                               </td>
                               <td style="text-align: center;">
                                  <div class="form-group tadmPadd wiDt">
                                     <div class="form-check form-check-flat form-check-primary adComp adComp-primary">
                                        <label class="form-check-label adComp-label mb-0">
                                        <input type="checkbox" class="form-check-input">
                                        </label>
                                     </div>
                                  </div>
                               </td>
                            </tr>
                            <tr>
                               <th scope="row" style="vertical-align: middle;"><span style="color: Dodgerblue; "><i class="fas fa-arrows-alt fa-lg fa-fw"></i></span> Internal Agent (VID:1/CID:1)</th>
                               <td>AllTiers</td>
                               <td>Payday Test Buyer2 $30 (BID:283/CID:1)</td>
                               <td style="text-align: center;">30.00</td>
                               <td style="text-align: center; width: 6%;">
                                  <div class="col-md-12 form-group tadmPadd labFontt mb-0">
                                     <input type="text" class="form-control txtPadd1" placeholder="1">
                                  </div>
                               </td>
                               <td style="text-align: center;">
                                  <div class="custom-control custom-checkbox noCush">
                                     <input type="checkbox" checked="" class="custom-control-input noCush-input" id="customCheck3" name="example2">
                                     <label class="custom-control-label noCush-label mb-3" for="customCheck3"></label>
                                  </div>
                               </td>
                               <td style="text-align: center;">
                                  <div class="form-group tadmPadd wiDt">
                                     <div class="form-check form-check-flat form-check-primary adComp adComp-primary">
                                        <label class="form-check-label adComp-label mb-0">
                                        <input type="checkbox" class="form-check-input">
                                        </label>
                                     </div>
                                  </div>
                               </td>
                            </tr>
                            <tr>
                               <th scope="row" style="vertical-align: middle;"><span style="color: Dodgerblue; "><i class="fas fa-arrows-alt fa-lg fa-fw"></i></span> Internal Agent (VID:1/CID:1)</th>
                               <td>AllTiers</td>
                               <td>Payday Test Buyer3 $30 (BID:284/CID:1)</td>
                               <td style="text-align: center;">30.00</td>
                               <td style="text-align: center; width: 6%;">
                                  <div class="col-md-12 form-group tadmPadd labFontt mb-0">
                                     <input type="text" class="form-control txtPadd1" placeholder="1">
                                  </div>
                               </td>
                               <td style="text-align: center;">
                                  <div class="custom-control custom-checkbox noCush">
                                     <input type="checkbox" checked="" class="custom-control-input noCush-input" id="customCheck4" name="example2">
                                     <label class="custom-control-label noCush-label mb-3" for="customCheck4"></label>
                                  </div>
                               </td>
                               <td style="text-align: center;">
                                  <div class="form-group tadmPadd wiDt">
                                     <div class="form-check form-check-flat form-check-primary adComp adComp-primary">
                                        <label class="form-check-label adComp-label mb-0">
                                        <input type="checkbox" class="form-check-input">
                                        </label>
                                     </div>
                                  </div>
                               </td>
                            </tr>
                            <tr>
                               <th scope="row" style="vertical-align: middle;"><span style="color: Dodgerblue; "><i class="fas fa-arrows-alt fa-lg fa-fw"></i></span> Internal Agent (VID:1/CID:1)</th>
                               <td>AllTiers</td>
                               <td>Payday Test Buyer4 $30 (BID:285/CID:1)</td>
                               <td style="text-align: center;">30.00</td>
                               <td style="text-align: center; width: 6%;">
                                  <div class="col-md-12 form-group tadmPadd labFontt mb-0">
                                     <input type="text" class="form-control txtPadd1" placeholder="1">
                                  </div>
                               </td>
                               <td style="text-align: center;">
                                  <div class="custom-control custom-checkbox noCush">
                                     <input type="checkbox" checked="" class="custom-control-input noCush-input" id="customCheck5" name="example2">
                                     <label class="custom-control-label noCush-label mb-3" for="customCheck5"></label>
                                  </div>
                               </td>
                               <td style="text-align: center;">
                                  <div class="form-group tadmPadd wiDt">
                                     <div class="form-check form-check-flat form-check-primary adComp adComp-primary">
                                        <label class="form-check-label adComp-label mb-0">
                                        <input type="checkbox" class="form-check-input">
                                        </label>
                                     </div>
                                  </div>
                               </td>
                            </tr>
                            <tr>
                               <th scope="row" style="vertical-align: middle;"><span style="color: Dodgerblue; "><i class="fas fa-arrows-alt fa-lg fa-fw"></i></span> Internal Agent (VID:1/CID:1)</th>
                               <td>AllTiers</td>
                               <td>TEST (BID:228/CID:1)</td>
                               <td style="text-align: center;">10.00</td>
                               <td style="text-align: center; width: 6%;">
                                  <div class="col-md-12 form-group tadmPadd labFontt mb-0">
                                     <input type="text" class="form-control txtPadd1" placeholder="2">
                                  </div>
                               </td>
                               <td style="text-align: center;">
                                  <div class="custom-control custom-checkbox noCush">
                                     <input type="checkbox" checked="" class="custom-control-input noCush-input" id="customCheck6" name="example2">
                                     <label class="custom-control-label noCush-label mb-3" for="customCheck6"></label>
                                  </div>
                               </td>
                               <td style="text-align: center;">
                                  <div class="form-group tadmPadd wiDt">
                                     <div class="form-check form-check-flat form-check-primary adComp adComp-primary">
                                        <label class="form-check-label adComp-label mb-0">
                                        <input type="checkbox" class="form-check-input">
                                        </label>
                                     </div>
                                  </div>
                               </td>
                            </tr>
                            <tr>
                               <th scope="row" style="vertical-align: middle;"><span style="color: Dodgerblue; "><i class="fas fa-arrows-alt fa-lg fa-fw"></i></span> Internal Agent (VID:1/CID:1)</th>
                               <td>AllTiers</td>
                               <td>Astoria Payday Tier $1 (BID:314/CID:259)</td>
                               <td style="text-align: center;">1.00</td>
                               <td style="text-align: center; width: 6%;">
                                  <div class="col-md-12 form-group tadmPadd labFontt mb-0">
                                     <input type="text" class="form-control txtPadd1" placeholder="2">
                                  </div>
                               </td>
                               <td style="text-align: center;">
                                  <div class="custom-control custom-checkbox noCush">
                                     <input type="checkbox" checked="" class="custom-control-input noCush-input" id="customCheck7" name="example2">
                                     <label class="custom-control-label noCush-label mb-3" for="customCheck7"></label>
                                  </div>
                               </td>
                               <td style="text-align: center;">
                                  <div class="form-group tadmPadd wiDt">
                                     <div class="form-check form-check-flat form-check-primary adComp adComp-primary">
                                        <label class="form-check-label adComp-label mb-0">
                                        <input type="checkbox" class="form-check-input">
                                        </label>
                                     </div>
                                  </div>
                               </td>
                            </tr>
                            <tr>
                               <th scope="row" style="vertical-align: middle;"><span style="color: Dodgerblue; "><i class="fas fa-arrows-alt fa-lg fa-fw"></i></span> Internal Agent (VID:1/CID:1)</th>
                               <td>AllTiers</td>
                               <td>EPCVIPPayday (BID:316/CID:260)</td>
                               <td style="text-align: center;">1.00</td>
                               <td style="text-align: center; width: 6%;">
                                  <div class="col-md-12 form-group tadmPadd labFontt mb-0">
                                     <input type="text" class="form-control txtPadd1" placeholder="3">
                                  </div>
                               </td>
                               <td style="text-align: center;">
                                  <div class="custom-control custom-checkbox noCush">
                                     <input type="checkbox" checked="" class="custom-control-input noCush-input" id="customCheck8" name="example2">
                                     <label class="custom-control-label noCush-label mb-3" for="customCheck8"></label>
                                  </div>
                               </td>
                               <td style="text-align: center;">
                                  <div class="form-group tadmPadd wiDt">
                                     <div class="form-check form-check-flat form-check-primary adComp adComp-primary">
                                        <label class="form-check-label adComp-label mb-0">
                                        <input type="checkbox" class="form-check-input">
                                        </label>
                                     </div>
                                  </div>
                               </td>
                            </tr>
                         </tbody>
                      </table>
                   </div>
                </div>
             </div>
          </div>
       </div>
       @include('layouts.Footer')
    </div>
    </div>
  </div>
  <div class="modal fade" id="clonemapping" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
    <div class="modal-dialog" role="document">
       <div class="modal-content">
          <div class="modal-header">
             <h4 class="modal-title" id="exampleModalLabel-2">Clone Mapping</h4>
             <button type="button" class="close" data-dismiss="modal" aria-label="Close">
             <span aria-hidden="true">&times;</span>
             </button>
          </div>
          <div class="modal-body venModbody">
             <h5>Vendor Company</h5>
             <div class="row">
                <div class="col-md-12">
                   <select class="form-control modfonT" id="exampleFormControlSelect1">
                      <option>revJOLT.com (CID:1)</option>
                      <option>Select Vendor Company</option>
                      <option>a p systems pvt lmt (CID:251)</option>
                      <option>Mint Global (CID:18)</option>
                      <option>test company 123 (CID:250)</option>
                   </select>
                </div>
                <div class="col-md-12 form-group tadmPadd mb-0">
                   <div class="form-check form-check-flat form-check-primary adComp adComp-primary disPlayt">
                      <strong><label class="form-check-label adComp-label">
                      <input type="checkbox" class="form-check-input">
                      Show Inactive
                      </label></strong>
                   </div>
                   <div class="form-check form-check-flat form-check-primary adComp adComp-primary disPlayt">
                      <strong><label class="form-check-label adComp-label">
                      <input type="checkbox" class="form-check-input">
                      Show Test
                      </label></strong>
                   </div>
                </div>
             </div>
             <div class="vHeadmod pt-2">
                <h5>Vendor</h5>
                <div class="form-group mb-0">
                   <select multiple class="form-control multiselOut" id="exampleSelect2" size="10">
                      <option>FastPayDayWorld.com (VID:49 / CID:1)</option>
                      <option>InstantPayDayWorld.com (VID:52 / CID:1)</option>
                      <option>Lenovo (VID:319 / CID:1)</option>
                      <option>LifestylePayday.com (VID:66 / CID:1)</option>
                      <option>OnlinePaydayQuick.com (VID:47 / CID:1)</option>
                      <option>revJOLT (VID:154 / CID:1)</option>
                      <option>revtestmap (VID:379 / CID:1)</option>
                      <option>TakeCashAdvance.com (VID:46 / CID:1)</option>
                      <option>TEST 12 (VID:352 / CID:1)</option>
                      <option>TheAdvancePaydayLoan.com (VID:51 / CID:1)</option>
                      <option>ThePayDayGroup.com (VID:50 / CID:1)</option>
                      <option>ThePayDayOnline.com (VID:48 / CID:1)</option>
                   </select>
                </div>
             </div>
          </div>
          <div class="modal-footer venModfoot">
             <button type="button" class="btn btn-success cbtnPadd sercFontt">Save</button>
          </div>
       </div>
    </div>
  </div>
  <div class="modal fade" id="addnewmapping" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
    <div class="modal-dialog" role="document">
       <div class="modal-content">
          <div class="modal-header">
             <h4 class="modal-title" id="exampleModalLabel-2">Add New Mapping</h4>
             <button type="button" class="close" data-dismiss="modal" aria-label="Close">
             <span aria-hidden="true">&times;</span>
             </button>
          </div>
          <div class="modal-body venModbody">
             <h5>Tiers</h5>
             <div class="row">
                <form class="allT">
                <div class="col-md-12">
                   <select class="form-control modfonT" id="exampleFormControlSelect2">
                      <option>AllTiers</option>
                      <option>Tier1</option>
                      <option>Tier2</option>
                      <option>Tier3</option>
                      <option>Tier4</option>
                   </select>
                </div>
             </div>
             <div class="vHeadmod pt-2">
                <h5>Buyer Company</h5>
                <div class="row">
                   <form class="allT">
                   <div class="col-md-12">
                      <select class="form-control modfonT" id="exampleFormControlSelect3">
                         <option>All Buyer Company</option>
                         <option>11111111 (CID:213)</option>
                         <option>Astoria (CID:259)</option>
                         <option>AvenueLink (CID:248)</option>
                         <option>Dot818 (CID:235)</option>
                         <option>LeadTrove (CID:78)</option>
                         <option>revJOLT.com (CID:1)</option>
                      </select>
                   </div>
                   <div class="col-md-12 form-group tadmPadd mb-0">
                      <div class="form-check form-check-flat form-check-primary adComp adComp-primary disPlayt">
                         <strong><label class="form-check-label adComp-label">
                         <input type="checkbox" class="form-check-input">
                         Show Inactive
                         </label></strong>
                      </div>
                      <div class="form-check form-check-flat form-check-primary adComp adComp-primary disPlayt">
                         <strong><label class="form-check-label adComp-label">
                         <input type="checkbox" class="form-check-input">
                         Show Test
                         </label></strong>
                      </div>
                   </div>
                </div>
                <h5>Buyer</h5>
                <div class="form-group mb-0">
                   <select multiple class="form-control multiselOut" id="exampleSelect3" size="10">
                      <option>(BID:189 / CID:120)</option>
                      <option>abc (BID:206 / CID:9)</option>
                      <option>abc123 (BID:292 / CID:9)</option>
                      <option>aiabc (BID:187 / CID:9)</option>
                      <option>AllTier123(BID:120 / CID:1)</option>
                      <option>AllTier1234(BID:121 / CID:1)</option>
                      <option>AllTier12345(BID:122 / CID:1)</option>
                      <option>Astoria Payday Tier $1 (BID:314 / CID:259)</option>
                      <option>AvenueLinkPayday (BID:278 / CID:248)</option>
                      <option>Dot818Payday (BID:275 / CID:235)</option>
                      <option>LeadTrove $1 Static (BID:229 / CID:78)</option>
                      <option>Payday 123 (BID:293 / CID:1)</option>
                   </select>
                </div>
                <div class="col-md-12 form-group tadmPadd">
                   <div class="form-check form-check-flat form-check-primary adComp adComp-primary mt-4">
                      <label class="form-check-label adComp-label">
                      <input type="checkbox" class="form-check-input"> Show All
                      </label>
                   </div>
                </div>
             </div>
          </div>
          <div class="modal-footer venModfoot">
             <button type="button" class="btn btn-success cbtnPadd sercFontt">Save</button>
          </div>
       </div>
    </div>
  </div>
  <input type="hidden" name="url" id="url" value='{{URL::to("/")}}'>
  <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
  <input type="hidden" name="loading" id="loading" value='{{ asset("Admin/images/loading.gif") }}'>
  @include('layouts.Script')
  <script src="{{asset('Ajax/VendorBuyerMapping.js')}}"></script> 
  <script>
  $(document).ready(function() {
    $('#sandbox-container .input-daterange').datepicker({});

    $('#datepicker').datepicker({
      weekStart: 0,
      daysOfWeekHighlighted: "6,0",
      autoclose: true,
      todayHighlight: true,
      dateFormat: 'mm/dd/yy'
    });
    $('#datepicker').datepicker("setDate", new Date());
    GetVendorBuyerUsingCampaignID();
  });
  </script>
</body>
