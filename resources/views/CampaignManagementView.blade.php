<!DOCTYPE html>
<html lang="en">
@include('layouts.Head')
<body>
  <div class="container-scroller">
    @include('layouts.Header')
    <div class="container-fluid page-body-wrapper">
      @include('layouts.Menu')
      <div class="main-panel">
        <div class="content-wrapper">
         <!--  <div class="page-header row">
            <h3 class="page-title">
              Campaign Management
            </h3>
          </div> -->
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb breadcrumb-custom">
              <li class="breadcrumb-item"><a href="#"><i class="fas fa-users-cog menu-icon"></i> Super Admin Tools</a></li>
              <li class="breadcrumb-item active" aria-current="page"><span><i class="fas fa-user-cog iconMarg"></i> Campaign Management </span></li>
            </ol>
          </nav>
           <div class="col-md-12">
            @if(Session::has('message'))
              <div class="alert alrtDanger {{ Session::get('alert-class', 'alert-info') }}">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>{{ Session::get('message') }}</strong>
              </div>
            @endif
          </div>
          <div class="col-md-12" id="ResponseMessage">
          </div>
          <div class="allDet">
            <div class="col-lg-12 col-md-12 col-sm-12 col-12 comP">
              <div class="row">
                <div class="col-md-2 form-group tadmPadd labFontt newFoGrp">
                  <input type="text" class="form-control txtPadd boxH" id="CampaignName" placeholder="Campaign Name">
                </div>
                <div class="col-md-2 newFoGrp">
                  <select class="form-control fonT" id="CampaignType">
                    <option value="">All Type</option>
                    @foreach($CampaignType as $ct)
                    <option value="{{$ct->CampaignType}}">{{$ct->CampaignType}}</option>
                    @endforeach
                  </select>
                </div>
                <div class="col-md-2 newFoGrp">
                  <select class="form-control fonT" id="MaintenanceMode">
                    <option value="">All Mode</option>
                    <option value="1">Yes</option>
                    <option value="0">No</option>
                  </select>
                </div>
                <div class="col-md-2 col-6">
                   <button type="button" class="btn btn-success cbtnPadd sercFontt"
                   id="SearchBtn" onclick="SearchCampaignList();"> <i class="fa fa-search"></i> Search </button>
                </div>
                <div class="col-md-4 col-6">
                  <a href="{{route('admin.CampaignAddView')}}" class="btn btn-success btn-fw cbtnPadd sercFontt fl dwSCSV Flo">Add New Campaign</a>
                </div>  
              </div>
              <div id="dataTable_wrapper" class="dataTables_wrapper dt-bootstrap4">
                <input type="hidden" name="numofrecords" id="numofrecords" value='10'>
              </div>
            </div>
          </div>
       </div>
       @include('layouts.Footer')
    </div>
    </div>
  </div>
  <input type="hidden" name="url" id="url" value='{{URL::to("/")}}'>
  <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
  <input type="hidden" name="loading" id="loading" value='{{ asset("Admin/images/loading.gif") }}'>
  @include('layouts.Script')
  <script src="{{asset('Ajax/CampaignManagement.js')}}"></script> 
  <script>
  $(document).ready(function() {    
      GetCampaignList(1);
  });
  </script>
</body>
