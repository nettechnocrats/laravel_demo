<!DOCTYPE html>
<html lang="en">
@include('layouts.Head')
<body>
  <div class="container-scroller">
    @include('layouts.Header')
    <div class="container-fluid page-body-wrapper">
      @include('layouts.Menu')
      <div class="main-panel">
        <div class="content-wrapper">
         <!--  <div class="page-header row">
            <h3 class="page-title">
              Add New Campaign
            </h3>
          </div> -->
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb breadcrumb-custom">
              <li class="breadcrumb-item"><a href="#"><i class="fas fa-users-cog menu-icon"></i> Super Admin Tools</a></li>
              <li class="breadcrumb-item"><a href="{{ route('admin.CompanyManagementView') }}"><i class="fas fa-users menu-icon"></i> Company Management </a></li>
              <li class="breadcrumb-item active" aria-current="page"><span><i class="fas fa-plus-square menu-icon"></i> Add Company Details </span></li>
            </ol>
          </nav>
          <div class="col-md-12">
            @if(Session::has('message'))
              <div class="alert alrtDanger {{ Session::get('alert-class', 'alert-info') }}">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>{{ Session::get('message') }}</strong>
              </div>
            @endif
          </div>
          <div class="allDet">
             <!-- TABS START -->
             <div class="col-md-12 grid-margin stretch-card">
                <div class="card">
                   <div class="card-body">
                      <form action="{{route('admin.AddCompanyDetails')}}" name="CompanyAddForm" id="CompanyAddForm" 
                          method="POST" enctype="mupltipart/form-data">
                        <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                        <div class="accordion" id="accordionExample">
                           <div class="card z-depth-0 bordered addMargBo">
                              <div class="card-header" id="headingOne">
                                 <h5 class="mb-0">
                                    <button class="btn btn-link tabStyle" type="button" id="comapny" >
                                    Comapny
                                    </button>
                                    <button class="btn btn-link tabStyleHiden collapsed" id="user" type="button" >
                                    User
                                    </button>
                                    <button class="btn btn-link tabStyleHiden collapsed" id="vendor" type="button" 
                                    style="display:none;">Vendor
                                    </button>
                                    <button class="btn btn-link tabStyleHiden collapsed" id="buyer" type="button" 
                                    style="display:none;">Buyer
                                    </button>
                                 </h5>
                              </div>
                              <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                                 <div class="card-body">                                  
                                    <div class="tab-pane show active" id="company-1" role="tabpanel" aria-labelledby="company-tab">
                                      <div class="row">
                                           <div class="page-header-below">
                                              <h3 class="page-title-below">
                                                 Add Company Details
                                              </h3>
                                           </div>
                                        </div>
                                       <div class="col-lg-12 col-md-12 col-sm-12 col-12 comP">                                        
                                             <div class="row">
                                                <div class="col-md-4 form-group tadmPadd labFontt">
                                                   <label for="exampleInputEmail1">Company Name:</label>
                                                   <input type="text" class="form-control txtPadd" placeholder="Company Name"
                                                    value="" id="CompanyName" name="CompanyName" onkeypress="HideErr('CompanyNameErr');">
                                                    <span id="CompanyNameErr"></span>
                                                </div>
                                                <div class="col-md-4 form-group tadmPadd labFontt">
                                                   <label for="exampleInputEmail1">Telephone:</label>
                                                   <input type="text" class="form-control txtPadd" placeholder="Company Telephone"
                                                    value="" id="CompanyTel" name="CompanyTel">
                                                    <span id="CompanyTelErr"></span>
                                                </div>
                                                <div class="col-md-4 form-group tadmPadd labFontt">
                                                   <label for="exampleInputEmail1">WebSite:</label>
                                                   <input type="text" class="form-control txtPadd" placeholder="Company WebSite"
                                                    value="" id="CompanyWebSite" name="CompanyWebSite">
                                                    <span id="CompanyWebSiteErr"></span>
                                                </div>                                              
                                             </div>
                                             <div class="row">
                                                <div class="col-md-4 form-group labFontt">
                                                   <label for="exampleInputEmail1">Address 1 :</label>
                                                   <input type="text" class="form-control txtPadd" placeholder=""
                                                    value="" id="CompanyAddress1" name="CompanyAddress1">
                                                    <span id="CompanyAddress1Err"></span>
                                                </div>
                                                <div class="col-md-4 form-group tadmPadd labFontt">
                                                   <label for="exampleInputEmail1">Address 2:</label>
                                                   <input type="text" class="form-control txtPadd" placeholder=""
                                                    value="" id="CompanyAddress2" name="CompanyAddress2">
                                                    <span id="CompanyAddress2Err"></span>
                                                </div>
                                                <div class="col-md-4 form-group tadmPadd labFontt">
                                                   <label for="exampleInputEmail1">Address 3:</label>
                                                   <input type="text" class="form-control txtPadd" placeholder=""
                                                    value="" id="CompanyAddress3" name="CompanyAddress3">
                                                    <span id="CompanyAddress3Err"></span>
                                                </div>
                                             </div>
                                             <div class="row">
                                                <div class="col-md-4 form-group tadmPadd labFontt">
                                                   <label for="exampleInputEmail1">Company Status:</label>
                                                   <div class="onoffswitch4">
                                                      <input type="checkbox" name="CompanyStatus" class="onoffswitch4-checkbox" 
                                                        id="CompanyStatus" checked>
                                                      <label class="onoffswitch4-label" for="CompanyStatus">
                                                      <span class="onoffswitch4-inner"></span>
                                                      <span class="onoffswitch4-switch"></span>
                                                      </label>
                                                   </div>
                                                </div>
                                                <div class="col-md-4 form-group tadmPadd labFontt">
                                                   <label for="exampleInputEmail1">Company Mode:</label>
                                                   <div class="onoffswitch5">
                                                      <input type="checkbox" name="CompanyTestMode" class="onoffswitch5-checkbox" 
                                                      id="CompanyTestMode" checked>
                                                      <label class="onoffswitch5-label" for="CompanyTestMode">
                                                      <span class="onoffswitch5-inner"></span>
                                                      <span class="onoffswitch5-switch"></span>
                                                      </label>
                                                   </div>
                                                </div>                                                
                                             </div>
                                             <div class="row">
                                                <div class="col-md-2 form-group labFontt">
                                                   <label for="exampleInputEmail1">City:</label>
                                                   <input type="text" class="form-control txtPadd" placeholder="City"
                                                   id="CompanyCity" name="CompanyCity">
                                                   <span id="CompanyCityErr"></span>
                                                </div>
                                                <div class="col-md-2 form-group mb-0 tadmPadd labFontt" id="StateDiv">
                                                   <label for="exampleFormControlSelect1">State:</label>
                                                   <select class="form-control fonT" id="CompanyStateCounty" name="CompanyStateCounty">
                                                      <option value="">Select State</option>
                                                      @foreach($StatesList as $s)
                                                      <option value="{{$s->State}}">{{$s->State}}</option>
                                                      @endforeach
                                                   </select>
                                                   <span id="CompanyStateCountyErr"></span>
                                                </div>
                                                <div class="col-md-2 form-group mb-0 tadmPadd labFontt">
                                                   <label for="exampleInputEmail1" id="CompanyZipPostCodeLable">Zip Code:</label>
                                                   <input type="text" class="form-control txtPadd" placeholder="Zip Code"
                                                   id="CompanyZipPostCode" name="CompanyZipPostCode" onkeypress="HideErr('CompanyZipPostCodeErr');">
                                                   <span id="CompanyZipPostCodeErr"></span>
                                                </div>
                                                <div class="col-md-2 form-group mb-0 labFontt">
                                                   <label for="exampleFormControlSelect1">Country:</label>
                                                   <select class="form-control fonT" id="CompanyCountry" name="CompanyCountry">
                                                      @foreach($CountryList as $c)
                                                      <option value="{{$c->Code}}"
                                                        <?php if($c->Code=="US"){ echo "selected";} ?>>{{$c->CountryName}}</option>
                                                      @endforeach
                                                   </select>
                                                   <span id="CompanyCountryErr"></span>
                                                </div>
                                                <div class="col-md-2 form-group mb-0 labFontt">
                                                   <label for="exampleFormControlSelect1">Currency:</label>
                                                   <select class="form-control fonT" id="CompanyCurrency" name="CompanyCurrency">
                                                      <option value="USD-$">USD-$</option>
                                                   </select>
                                                   <span id="CompanyCurrencyErr"></span>
                                                </div>                                              
                                             </div>
                                             <div class="row">
                                                <div class="col-md-4 form-group mb-0 labFontt">
                                                  <label for="exampleFormControlTextarea1">Company Notes:</label>
                                                  <textarea class="form-control" id="CompanyNotes" name="CompanyNotes" rows="3"></textarea>
                                                  <span id="CompanyNotesErr"></span>
                                                </div>
                                                <div class="col-md-4 form-group labFontt tablabFontt">
                                                   <label>File: (You Can Select Multiple Files)</label>
                                                   <div class="custom-file">
                                                      <input type="file" class="custom-file-input" id="Companyfiles" name="Companyfiles[]">
                                                      <label class="custom-file-label fileLabel" for="Companyfiles">No File Chosen</label>
                                                   </div>
                                                </div>
                                             </div>                                        
                                       </div>
                                       <div class="row">
                                          <div class="col-md-6 form-check form-check-flat form-check-primary adComp adComp-primary">
                                             <!-- <label class="form-check-label adComp-label">
                                             <input type="checkbox" class="form-check-input" id="AddUser" name="AddUser" >
                                             Do you want add user to this company
                                             </label> -->
                                          </div>
                                          <div class="col-md-6 nextttBtn">
                                             <input type="button" class="btn btn-primary nBtnFlo" id="CompanyToUserBtn" value="Next">
                                          </div>
                                       </div>                                    
                                    </div>                                  
                                 </div>
                              </div>
                           </div>
                           <div class="card z-depth-0 bordered addMargBo">
                              <div class="card-header addPaddMarg" id="headingOne">                               
                              </div>
                              <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                                 <div class="card-body">                                  
                                    <div class="tab-pane" id="user-1" role="tabpanel" aria-labelledby="user-tab">                                     
                                       <div class="addUser">
                                          <div class="row">
                                             <div class="page-header-below">
                                                <h3 class="page-title-below">
                                                   Add Users Details
                                                </h3>
                                             </div>
                                          </div>                                       
                                            <div class="row">
                                               <div class="col-md-3 form-group tadmPadd labFontt">
                                                  <label for="exampleInputEmail1">First Name:</label>
                                                  <input type="text" onkeypress="HideErr('UserFirstNameErr');" 
                                                  value="" id="UserFirstName" name="UserFirstName" class="form-control txtPadd" placeholder="First Name">
                                                  <span id="UserFirstNameErr" class="errordisp"></span>
                                               </div>
                                               <div class="col-md-3 form-group tadmPadd labFontt">
                                                  <label for="exampleInputEmail1">Last Name:</label>
                                                  <input type="text" onkeypress="HideErr('UserLastNameErr');" 
                                                  value="" id="UserLastName" name="UserLastName" class="form-control txtPadd" placeholder="Last Name">
                                                  <span id="UserLastNameErr" class="errordisp"></span>
                                               </div>
                                               <div class="col-md-3 form-group tadmPadd labFontt">
                                                  <label for="exampleInputEmail1">Email:</label>
                                                  <input type="email" onkeypress="HideErr('UserEmailErr');" 
                                                  value="" id="UserEmail" name="UserEmail" class="form-control camH" id="exampleInputEmail1" placeholder="Email">
                                                  <span id="UserEmailErr" class="errordisp"></span>
                                               </div>
                                            </div>
                                            <div class="row">                                             
                                               <div class="col-md-3 form-group tadmPadd labFontt">
                                                  <label for="exampleInputEmail1">Telephone:</label>
                                                  <input type="text" onkeypress="HideErr('UserTelErr');" 
                                                  value="" id="UserTel" name="UserTel" class="form-control txtPadd" placeholder="Telephone">
                                                  <span id="UserTelErr" class="errordisp"></span>
                                               </div>
                                               <div class="col-md-3 form-group tadmPadd labFontt">
                                                  <label for="exampleInputEmail1">Skype ID:</label>
                                                  <input type="text" onkeypress="HideErr('UserSkypeErr');" 
                                                  value="" id="UserSkype" name="UserSkype" class="form-control txtPadd" placeholder="Skype ID">
                                                  <span id="UserSkypeErr" class="errordisp"></span>
                                               </div>
                                               <div class="col-md-2 form-group mb-0 tadmPadd labFontt">
                                                  <label for="PayoutCalculation">User Role:</label>
                                                    <select class="form-control fonT" id="UserRole" name="UserRole" onchange="HideErr('UserRoleErr');">
                                                      <option value="">Select User Role</option>
                                                      <option value="1">Super Admin</option>
                                                      <option value="2">Admin</option>
                                                      <option value="3">User</option>
                                                    </select>
                                                   <span id="UserRoleErr" class="errordisp"></span>
                                                </div>                                                              
                                            </div>
                                             <div class="row"> 
                                                <div class="col-md-3 form-group tadmPadd labFontt">
                                                  <label for="exampleInputEmail1">User Status:</label>
                                                  <div class="onoffswitch4">
                                                    <input type="checkbox" name="UserStatus" class="onoffswitch4-checkbox" 
                                                    id="UserStatus" checked>
                                                    <label class="onoffswitch4-label" for="UserStatus">
                                                        <span class="onoffswitch4-inner"></span>
                                                        <span class="onoffswitch4-switch"></span>
                                                    </label>
                                                  </div>
                                                </div>
                                                <div class="col-md-3 form-group tadmPadd labFontt">
                                                   <label for="exampleInputEmail1">Show Specs:</label>
                                                   <div class="onoffswitch6">
                                                      <input type="checkbox" name="ShowAPISpecs" class="onoffswitch6-checkbox" id="ShowAPISpecs" checked>
                                                      <label class="onoffswitch6-label" for="ShowAPISpecs">
                                                      <span class="onoffswitch6-inner"></span>
                                                      <span class="onoffswitch6-switch"></span>
                                                      </label>
                                                   </div>
                                                </div>
                                                <div class="col-md-3 form-group tadmPadd labFontt">
                                                   <label for="exampleInputEmail1">Show Affiliate Links:</label>
                                                   <div class="onoffswitch7">
                                                      <input type="checkbox" name="ShowAffLinks" class="onoffswitch7-checkbox" id="ShowAffLinks" checked>
                                                      <label class="onoffswitch7-label" for="ShowAffLinks">
                                                      <span class="onoffswitch7-inner"></span>
                                                      <span class="onoffswitch7-switch"></span>
                                                      </label>
                                                   </div>
                                                </div>                                                              
                                            </div>
                                            <div class="row" >
                                               <div class="col-md-3 form-group tadmPadd labFontt">
                                                  <label for="exampleInputPassword1">Password:</label>
                                                  <input type="text" onkeypress="HideErr('PasswordErr');" class="form-control camH" 
                                                  id="Password" name="Password" placeholder="Password">
                                                  <span id="PasswordErr"  class="errordisp"></span>
                                               </div>
                                               <div class="col-md-3 form-group tadmPadd labFontt">
                                                  <label for="exampleInputPassword1">Confirm Password:</label>
                                                  <input type="text" onkeypress="HideErr('ConfirmPasswordErr');" class="form-control camH" 
                                                  id="ConfirmPassword" name="ConfirmPassword" placeholder="Confirm Password">
                                                  <span id="ConfirmPasswordErr"  class="errordisp"></span>
                                               </div>
                                               <div class="col-md-3 form-group tadmPadd labFontt">
                                                  <label class="gP" for="exampleInputPassword1">Generate Password:</label>
                                                  <button type="button" onclick="GeneratePassword();" class="btn btn-success fonT noBoradi cbtnPadd sercFontt gpBg">Generate Password</button>
                                               </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4 form-group mb-0 labFontt">
                                                  <label for="exampleFormControlTextarea1">User Notes:</label>
                                                  <textarea class="form-control" id="UserNotes" name="UserNotes" rows="3"></textarea>
                                                  <span id="UserNotesErr"></span>
                                                </div>
                                            </div> 
                                             <div class="row" >
                                               <div class="col-md-4 form-group tadmPadd labFonsize">
                                                  <div class="form-check form-check-flat form-check-primary adComp adComp-primary mt-4">
                                                     <label class="form-check-label adComp-label">
                                                     <input type="checkbox" class="form-check-input" id="SendCredentialsMail" name="SendCredentialsMail">
                                                      Would you like to email credentials to the user?
                                                     </label>
                                                  </div>
                                               </div>
                                               <div class="col-md-4 form-group tadmPadd labFonsize">
                                                  <div class="form-check form-check-flat form-check-primary adComp adComp-primary mt-4">
                                                     <label class="form-check-label adComp-label">
                                                     <input type="checkbox" class="form-check-input" id="AddVendor" name="AddVendor" onclick="OpenVendor();">
                                                      Would you like to add Vendor to the company?
                                                     </label>
                                                  </div>
                                               </div>
                                               <div class="col-md-4 form-group tadmPadd labFonsize">
                                                  <div class="form-check form-check-flat form-check-primary adComp adComp-primary mt-4">
                                                     <label class="form-check-label adComp-label">
                                                     <input type="checkbox" class="form-check-input" id="AddBuyer" name="AddBuyer" onclick="OpenBuyer();">
                                                      Would you like to add Buyer to the company?
                                                     </label>
                                                  </div>
                                               </div>
                                            </div>
                                             <div class="row">
                                                <div class="col-md-6 nextttBtn">
                                                   <input type="button" class="btn btn-primary pBtnFlo" id="UserToCompanyBtn" value="Previous">
                                                </div>
                                                <div class="col-md-6 nextttBtn" style="display:none;" id="NextBtnOnUserToVendor">
                                                   <input type="button" class="btn btn-primary nBtnFlo" id="UserToVendorBtn" name="UserToVendorBtn" value="Next">
                                                </div>
                                                <div class="col-md-6 nextttBtn" style="display:none;" id="NextBtnOnUserToBuyer">
                                                   <input type="button" class="btn btn-primary nBtnFlo" id="UserToBuyerBtn" name="UserToBuyerBtn" value="Next">
                                                </div>
                                                <div class="col-md-6 nextttBtn" id="SaveBtnOnUser">
                                                   <button type="button" class="btn btn-primary nBtnFlo" id="AddCompanyBtnOnUser">Save</button>
                                                </div>
                                             </div>
                                       </div>                                    
                                    </div>                                  
                                 </div>
                              </div>
                           </div>
                           <div class="card z-depth-0 bordered addMargBo">
                              <div class="card-header addPaddMarg" id="headingThree">                               
                              </div>
                              <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                                 <div class="card-body">                                 
                                    <div class="tab-pane" id="vendor-1" role="tabpanel" aria-labelledby="vendor-tab">                                     
                                       <div class="addUser">
                                          <div class="row">
                                             <div class="page-header-below">
                                                <h3 class="page-title-below">
                                                   Add New Vendor Details
                                                </h3>
                                             </div>
                                          </div>    

                                             <div class="row">
                                                <div class="col-md-4 form-group mb-0 tadmPadd labFontt">
                                                    <label for="exampleFormControlSelect1">Campaign:</label>
                                                    <select class="form-control fonT" id="CampaignVendor" name="CampaignVendor" 
                                                        onchange="GetCampaignDetailsVendor(),HideErr('CampaignVendorErr');">
                                                        @foreach($CampaignDropDown as $cdd)
                                                        <option value="{{$cdd->CampaignID}}">{{$cdd->CampaignName}}</option>
                                                        @endforeach
                                                    </select>
                                                    <span id="CampaignVendorErr"></span>
                                                </div>
                                                <div class="col-md-2 form-group tadmPadd labFontt">
                                                  <label for="exampleInputEmail1">Admin Label:</label>
                                                    <input type="text" class="form-control txtPadd" 
                                                    value="" id="AdminLabelVendor" name="AdminLabelVendor" 
                                                    onkeypress="HideErr('AdminLabelVendorErr');">
                                                    <span id="AdminLabelVendorErr"></span>
                                                </div>
                                                <div class="col-md-2 form-group tadmPadd btnCen">
                                                  <button type="button" id="CopyAdminLableVendor" class="btn btn-success noBoradi mr-2">&gt;&gt;</button>
                                                </div>
                                                <div class="col-md-2 form-group tadmPadd labFontt">
                                                  <label for="exampleInputEmail1">Partner Label:</label>
                                                    <input type="text" class="form-control txtPadd" 
                                                      value="" id="PartnerLabelVendor" name="PartnerLabelVendor"
                                                      onkeypress="HideErr('PartnerLabelVendorErr');">
                                                      <span id="PartnerLabelVendorErr"></span>
                                                </div>
                                             </div>
                                             <div class="row">                                              
                                               <div class="col-md-2 form-group mb-0 tadmPadd labFontt">
                                                  <label for="PayoutCalculation">Payout Calculation:</label>
                                                    <select class="form-control fonT" id="PayoutCalculationVendor" name="PayoutCalculationVendor">
                                                      <option value="RevShare">RevShare</option>
                                                      <option value="FixedPrice">Fixed Price</option>
                                                      <option value="TierPrice">Tier Price</option>
                                                      <option value="MinPrice">Min Price</option>
                                                      <option value="BucketPrice">Bucket Price</option>
                                                      <option value="BucketSystemBySubID">Bucket System By SubID</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-2 form-group tadmPadd labFontt"   
                                                    id="RevsharePercentInputVendor">
                                                  <label for="exampleInputEmail1">Revshare: </label>
                                                  <input type="text" class="form-control txtPadd" value="70.00"
                                                   placeholder="70" id="RevshareVendor" name="RevshareVendor" onkeypress="HideErr('RevshareVendorErr');">
                                                  <span id="RevshareVendorErr"></span>
                                                </div>
                                                <div class="col-md-2 form-group tadmPadd labFontt" style='display:none' 
                                                    id="FixedPriceInputVendor">
                                                  <label for="exampleInputEmail1">Fixed Price:</label>
                                                  <input type="text" class="form-control txtPadd" value="1.00" 
                                                    placeholder="70" id="FixedPriceVendor" name="FixedPriceVendor" onkeypress="HideErr('FixedPriceVendorErr');">
                                                  <span id="FixedPriceVendorErr"></span>
                                                </div>
                                                <div class="col-md-4 form-group tadmPadd labFontt" style='display:none' 
                                                    id="TierPriceInputVendor">
                                                  <label for="exampleInputEmail1">Revshare Percent (for when Tier Payout is $0.00):</label>
                                                  <input type="text" class="form-control txtPadd" value="70.00" 
                                                    placeholder="70" id="TierPriceVendor" name="TierPriceVendor" onkeypress="HideErr('TierPriceVendorErr');">
                                                  <span id="TierPriceVendorErr"></span>
                                                </div>
                                             </div>
                                             <div class="row">
                                                 <div class="col-md-1 form-group tadmPadd labFontt">
                                                    <label for="exampleInputEmail1">Daily Cap:</label>
                                                      <input type="text" onkeypress="HideErr('DailyCapVendorErr');" value="5" 
                                                        id="DailyCapVendor" name="DailyCapVendor" class="form-control txtPadd" placeholder="Daily Cap">
                                                      <span id="DailyCapVendorErr"></span>
                                                  </div>
                                                  <div class="col-md-1 form-group tadmPadd labFontt">
                                                    <label for="exampleInputEmail1">Total Cap:</label>
                                                      <input type="text" onkeypress="HideErr('TotalCapVendorErr');" value="5" 
                                                        id="TotalCapVendor" name="TotalCapVendor" class="form-control txtPadd" placeholder="Total Cap">
                                                      <span id="TotalCapVendorErr"></span>
                                                  </div>
                                                  <div class="col-md-2 form-group tadmPadd labFontt">
                                                    <label for="exampleInputEmail1">Vendor Status:</label>
                                                      <div class="onoffswitch8">
                                                        <input type="checkbox" name="VendorStatus" class="onoffswitch8-checkbox" 
                                                        id="VendorStatus" checked="">
                                                        <label class="onoffswitch8-label" for="VendorStatus">
                                                          <span class="onoffswitch8-inner"></span>
                                                          <span class="onoffswitch8-switch"></span>
                                                        </label>
                                                      </div>
                                                  </div>
                                                  <div class="col-md-2 form-group tadmPadd labFontt">
                                                    <label for="exampleInputEmail1">Vendor Mode?</label>
                                                      <div class="onoffswitch9">
                                                        <input type="checkbox" name="VendorTestMode" class="onoffswitch9-checkbox"
                                                           id="VendorTestMode" checked="">
                                                        <label class="onoffswitch9-label" for="VendorTestMode">
                                                          <span class="onoffswitch9-inner"></span>
                                                          <span class="onoffswitch9-switch"></span>
                                                        </label>
                                                      </div>
                                                  </div>
                                                  <div class="col-md-2 form-group mb-0 tadmPadd labFontt">
                                                    <label for="exampleFormControlSelect1">Tracking Pixel Type:</label>
                                                      <select class="form-control fonT" id="TrackingPixelType" name="TrackingPixelType">
                                                        <option value="Pixel" >Pixel</option>
                                                        <option value="PostBack" >Post Back</option>
                                                      </select>
                                                  </div>
                                                  <div class="col-md-2 form-group tadmPadd labFontt">
                                                    <label for="exampleInputEmail1">Tracking Pixel Status?</label>
                                                      <div class="onoffswitch10">
                                                        <input type="checkbox" name="TrackingPixelStatus" class="onoffswitch10-checkbox"
                                                         id="TrackingPixelStatus" checked="">
                                                        <label class="onoffswitch10-label" for="TrackingPixelStatus">
                                                          <span class="onoffswitch10-inner"></span>
                                                          <span class="onoffswitch10-switch"></span>
                                                        </label>
                                                      </div>
                                                  </div>
                                             </div>
                                             <div class="row">
                                              <div class="col-md-2 form-group tadmPadd labFontt" id="MaxTimeOutInputVendor" style="display:none;">
                                                <label for="exampleInputEmail1">Max Time Out:</label>
                                                  <input type="text" class="form-control txtPadd" placeholder="0" 
                                                  id="MaxTimeOutVendor" name="MaxTimeOutVendor" value="0" onkeypress="HideErr('MaxTimeOutVendorErr');">
                                                  <span id="MaxTimeOutVendorErr"></span>
                                              </div>
                                              <div class="col-md-2 form-group tadmPadd labFontt" id="MaxPingTimeOutInputVendor" style="display:block;">
                                                <label for="exampleInputEmail1">Max Ping Time Out:</label>
                                                  <input type="text" class="form-control txtPadd" placeholder="0" 
                                                  id="MaxPingTimeOutVendor" name="MaxPingTimeOutVendor" value="0" onkeypress="HideErr('MaxPingTimeOutVendorErr');">
                                                  <span id="MaxPingTimeOutVendorErr"></span>
                                              </div>
                                              <div class="col-md-2 form-group tadmPadd labFontt" id="MaxPostTimeOutInputVendor" style="display:block;">
                                                <label for="exampleInputEmail1">Max Post Time Out:</label>
                                                  <input type="text" class="form-control txtPadd" placeholder="0" 
                                                  id="MaxPostTimeOutVendor" name="MaxPostTimeOutVendor" value="0" onkeypress="HideErr('MaxPostTimeOutVendorErr');">
                                                  <span id="MaxPostTimeOutVendorErr"></span>
                                              </div>
                                              <div class="col-md-4 form-group tadmPadd labFontt" >
                                                <label for="exampleInputEmail1">Return API URL:</label>
                                                  <input type="text" class="form-control txtPadd" placeholder="Return API URL" 
                                                  id="ReturnApiUrlVendor" name="ReturnApiUrlVendor" value="" onkeypress="HideErr('ReturnApiUrlVendorErr');">
                                                  <span id="ReturnApiUrlVendorErr"></span>
                                              </div>
                                             </div>
                                             <div class="row">
                                                 <div class="col-md-2 form-group mb-0 labFontt">
                                                    <label for="exampleFormControlSelect1">Insert Token:</label>
                                                      <select class="form-control" id="insert_Token_type" name="insert_Token_type" onchange="updateTextBox(this)">
                                                        <option value="aid">aid</option>
                                                        <option value="SubID">SubID</option>
                                                        <option value="TestMode">TestMode</option>
                                                        <option value="TransactionID">TransactionID</option>
                                                        <option value="VendorLeadPrice">VendorLeadPrice</option>
                                                    </select>
                                                  </div>
                                                  <div class="col-md-5 form-group mb-0 labFontt">
                                                    <label for="exampleFormControlTextarea1">Tracking Pixel Code:</label>
                                                    <textarea class="form-control" id="TrackingPixelCode" name="TrackingPixelCode" rows="3"></textarea>
                                                  </div>
                                                  <div class="col-md-5 form-group mb-0 labFontt">
                                                    <label for="exampleFormControlTextarea1">Vendor Notes:</label>
                                                    <textarea class="form-control" id="VendorNotes" name="VendorNotes" rows="3"></textarea>
                                                  </div>
                                             </div>

                                             <div class="row">
                                                <div class="col-md-6 nextttBtn">
                                                   <input type="button" class="btn btn-primary pBtnFlo" id="VendorToUserBtn" value="Previous">
                                                </div>
                                                <div class="col-md-6 nextttBtn" id="NextBtnOnVendor" style="display:none;">
                                                   <input type="button" class="btn btn-primary nBtnFlo" id="VendorToBuyerBtn" value="Next">
                                                </div>
                                                <div class="col-md-6 nextttBtn" id="SaveBtnOnVendor">
                                                   <button type="button" class="btn btn-primary nBtnFlo" id="AddCompanyBtnOnVendor">Save</button>
                                                </div>
                                             </div>                                        
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="card z-depth-0 bordered">
                              <div class="card-header addPaddMarg" id="headingThree"></div>
                              <div id="collapseFour" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                                 <div class="card-body">                                  
                                    <div class="tab-pane" id="buyer-1" role="tabpanel" aria-labelledby="buyer-tab">                                    
                                       <div class="addUser">
                                          <div class="row">
                                             <div class="page-header-below">
                                                <h3 class="page-title-below">
                                                   Add New Buyer Details
                                                </h3>
                                             </div>
                                          </div>                                        
                                           <div class="row">
                                             <div class="col-md-4 form-group mb-0 tadmPadd labFontt">
                                                <label for="exampleFormControlSelect1">Campaign:</label>
                                                <select class="form-control fonT" id="CampaignBuyer" name="CampaignBuyer" 
                                                    onchange="GetCampaignDetailsBuyer(),HideErr('CampaignBuyerErr');">
                                                    @foreach($CampaignDropDown as $cdd)
                                                    <option value="{{$cdd->CampaignID}}">{{$cdd->CampaignName}}</option>
                                                    @endforeach
                                                </select>
                                                <span id="CampaignBuyerErr"></span>
                                            </div>
                                            <div class="col-md-2 form-group tadmPadd labFontt">
                                              <label for="exampleInputEmail1">Admin Label:</label>
                                                <input type="text" class="form-control txtPadd" 
                                                value="" id="AdminLabelBuyer" name="AdminLabelBuyer" 
                                                onkeypress="HideErr('AdminLabelBuyerErr');">
                                                <span id="AdminLabelBuyerErr"></span>
                                            </div>
                                            <div class="col-md-2 form-group tadmPadd btnCen">
                                              <button type="button" id="CopyAdminLableBuyer" class="btn btn-success noBoradi mr-2">&gt;&gt;</button>
                                            </div>
                                            <div class="col-md-2 form-group tadmPadd labFontt">
                                              <label for="exampleInputEmail1">Partner Label:</label>
                                                <input type="text" class="form-control txtPadd" 
                                                  value="" id="PartnerLabelBuyer" name="PartnerLabelBuyer"
                                                  onkeypress="HideErr('PartnerLabelBuyerErr');">
                                                  <span id="PartnerLabelBuyerErr"></span>
                                            </div>
                                           </div>
                                           <div class="row">
                                              <div class="col-md-2 form-group tadmPadd labFontt">
                                                <label for="exampleInputEmail1">Integration File Name:</label>
                                                  <input type="text"value="" 
                                                    id="IntegrationFileName" name="IntegrationFileName" class="form-control txtPadd" placeholder="Integration File Name"
                                                    onkeypress="HideErr('IntegrationFileNameErr');">
                                                  <span id="IntegrationFileNameErr"></span>
                                              </div>
                                              <div class="col-md-2 form-group mb-0 tadmPadd labFontt">
                                                <label for="PayoutCalculation">Payout Calculation:</label>
                                                  <select class="form-control fonT" id="PayoutCalculationBuyer" name="PayoutCalculationBuyer">
                                                    <option value="RevShare" >RevShare</option>
                                                    <option value="FixedPrice" >Fixed Price</option>
                                                  </select>
                                              </div>
                                              <div class="col-md-2 form-group tadmPadd labFontt"  id="RevshareInputBuyer">
                                                <label for="exampleInputEmail1">RevShare: </label>
                                                <input type="text" class="form-control txtPadd" value="0.00" 
                                                placeholder="70" id="RevshareBuyer" name="RevshareBuyer" onkeypress="HideErr('RevshareBuyerErr');">
                                                <span id="RevshareBuyerErr"></span>
                                              </div>
                                              <div class="col-md-2 form-group tadmPadd labFontt" style="display:none;" id="FixedPriceInputBuyer">
                                                <label for="exampleInputEmail1">Fixed Price:</label>
                                                <input type="text" class="form-control txtPadd" value="2.00" 
                                                placeholder="70" id="FixedPriceBuyer" name="FixedPriceBuyer" onkeypress="HideErr('FixedPriceBuyerErr');">
                                                <span id="FixedPriceBuyerErr"></span>
                                              </div>
                                           </div>
                                           <div class="row">
                                              <div class="col-md-2 form-group tadmPadd labFontt" id="BuyerTierIDInput" style='display:none;'>
                                                <label for="exampleInputEmail1">Buyer Tier ID:</label>
                                                  <input type="text" onkeypress="HideErr('BuyerTierIDErr');" value="0" 
                                                    id="BuyerTierID" name="BuyerTierID" class="form-control txtPadd" placeholder="Buyer Tier ID">
                                                  <span id="BuyerTierIDErr"></span>
                                              </div>
                                              <div class="col-md-1 form-group tadmPadd labFontt">
                                                <label for="exampleInputEmail1">Daily Cap:</label>
                                                  <input type="text" id="DailyCapBuyer" name="DailyCapBuyer" class="form-control txtPadd" placeholder="Daily Cap"
                                                    onkeypress="HideErr('DailyCapBuyerErr');" value="10">
                                                  <span id="DailyCapBuyerErr"></span>
                                              </div>
                                              <div class="col-md-1 form-group tadmPadd labFontt">
                                                <label for="exampleInputEmail1">Total Cap:</label>
                                                  <input type="text" id="TotalCapBuyer" name="TotalCapBuyer" class="form-control txtPadd" placeholder="Total Cap"
                                                    onkeypress="HideErr('TotalCapBuyerErr');" value="10">
                                                  <span id="TotalCapBuyerErr"></span>
                                              </div>
                                              <div class="col-md-2 form-group tadmPadd labFontt">
                                                <label for="exampleInputEmail1">Buyer Status:</label>
                                                  <div class="onoffswitch8">
                                                    <input type="checkbox" name="BuyerStatus" class="onoffswitch8-checkbox" 
                                                    id="BuyerStatus" checked="">
                                                    <label class="onoffswitch8-label" for="BuyerStatus">
                                                      <span class="onoffswitch8-inner"></span>
                                                      <span class="onoffswitch8-switch"></span>
                                                    </label>
                                                  </div>
                                              </div>
                                              <div class="col-md-2 form-group tadmPadd labFontt">
                                                <label for="exampleInputEmail1">Buyer Mode?</label>
                                                  <div class="onoffswitch9">
                                                    <input type="checkbox" name="BuyerTestMode" class="onoffswitch9-checkbox"
                                                       id="BuyerTestMode" checked="">
                                                    <label class="onoffswitch9-label" for="BuyerTestMode">
                                                      <span class="onoffswitch9-inner"></span>
                                                      <span class="onoffswitch9-switch"></span>
                                                    </label>
                                                  </div>
                                              </div>
                                              <div class="col-md-2 form-group tadmPadd labFontt" id="MaxTimeOutInputBuyer" style='display:none;'>
                                                <label for="exampleInputEmail1">Max Time Out:</label>
                                                  <input type="text" class="form-control txtPadd" placeholder="0" 
                                                  id="MaxTimeOutBuyer" name="MaxTimeOutBuyer" value="30" onkeypress="HideErr('MaxTimeOutBuyerErr');">
                                                  <span id="MaxTimeOutBuyerErr"></span>
                                              </div>
                                              <div class="col-md-2 form-group tadmPadd labFontt" id="MinTimeOutInputBuyer" style='display:none;'>
                                                <label for="exampleInputEmail1">Min Time Out:</label>
                                                  <input type="text" class="form-control txtPadd" placeholder="0" 
                                                  id="MinTimeOutBuyer" name="MinTimeOutBuyer" value="5" onkeypress="HideErr('MinTimeOutBuyerErr');">
                                                  <span id="MinTimeOutBuyerErr"></span>
                                              </div>

                                              <div class="col-md-2 form-group tadmPadd labFontt" id="MaxPingTimeOutInputBuyer" style='display:none;'>
                                                <label for="exampleInputEmail1">Max Ping Time Out:</label>
                                                  <input type="text" class="form-control txtPadd" placeholder="0" 
                                                  id="MaxPingTimeOutBuyer" name="MaxPingTimeOutBuyer" value="10" onkeypress="HideErr('MaxPingTimeOutBuyerErr');">
                                                  <span id="MaxPingTimeOutBuyerErr"></span>
                                              </div>
                                              <div class="col-md-2 form-group tadmPadd labFontt" id="MaxPostTimeOutInputBuyer" style='display:none;'>
                                                <label for="exampleInputEmail1">Max Post Time Out:</label>
                                                  <input type="text" class="form-control txtPadd" placeholder="0" 
                                                  id="MaxPostTimeOutBuyer" name="MaxPostTimeOutBuyer" value="20" onkeypress="HideErr('MaxPostTimeOutBuyerErr');">
                                                  <span id="MaxPostTimeOutBuyerErr"></span>
                                              </div>
                                              <div class="col-md-2 form-group tadmPadd labFontt" id="MaxPingsPerMinuteInputBuyer" style='display:none;'>
                                                <label for="exampleInputEmail1">Max Pings Per Minute:</label>
                                                  <input type="text" class="form-control txtPadd" placeholder="0" 
                                                  id="MaxPingsPerMinuteBuyer" name="MaxPingsPerMinuteBuyer" value="0" onkeypress="HideErr('MaxPingsPerMinuteBuyerErr');">
                                                  <span id="MaxPingsPerMinuteBuyerErr"></span>
                                              </div>
                                           </div>
                                           <div class="row">
                                              <div class="col-md-6 form-group tadmPadd labFontt" id="PingTestURLInput">
                                                <label for="exampleInputEmail1">Ping Test URL:</label>
                                                  <input type="text" class="form-control txtPadd" 
                                                  value="" id="PingTestURL" name="PingTestURL"
                                                  onkeypress="HideErr('PingTestURLErr');">
                                                   <span id="PingTestURLErr"></span>
                                              </div>
                                              <div class="col-md-6 form-group tadmPadd labFontt" id="PingLiveURLInput">
                                                <label for="exampleInputEmail1">Ping Live URL:</label>
                                                  <input type="text" class="form-control txtPadd" 
                                                    value="" id="PingLiveURL" name="PingLiveURL"
                                                    onkeypress="HideErr('PingLiveURLErr');">
                                                     <span id="PingLiveURLErr"></span>
                                              </div>
                                              <div class="col-md-6 form-group tadmPadd labFontt" id="PostTestURLInput">
                                                <label for="exampleInputEmail1">Post Test URL:</label>
                                                  <input type="text" class="form-control txtPadd" 
                                                  value="" id="PostTestURL" name="PostTestURL"
                                                  onkeypress="HideErr('PostTestURLErr');">
                                                   <span id="PostTestURLErr"></span>
                                              </div>
                                              <div class="col-md-6 form-group tadmPadd labFontt" id="PostLiveURLInput">
                                                <label for="exampleInputEmail1">Post Live URL:</label>
                                                  <input type="text" class="form-control txtPadd" 
                                                    value="" id="PostLiveURL" name="PostLiveURL"
                                                    onkeypress="HideErr('PostLiveURLErr');">
                                                     <span id="PostLiveURLErr"></span>
                                              </div>

                                              <div class="col-md-6 form-group tadmPadd labFontt" style='display:none;' id="DirectPostTestURLInput">
                                                <label for="exampleInputEmail1">DirectPost Test URL:</label>
                                                  <input type="text" class="form-control txtPadd" 
                                                  value="" id="DirectPostTestURL" name="DirectPostTestURL"
                                                  onkeypress="HideErr('DirectPostTestURLErr');">
                                                   <span id="DirectPostTestURLErr"></span>
                                              </div>
                                              <div class="col-md-6 form-group tadmPadd labFontt" style='display:none;' id="DirectPostLiveURLInput">
                                                <label for="exampleInputEmail1">DirectPost Live URL:</label>
                                                  <input type="text" class="form-control txtPadd" 
                                                    value="" id="DirectPostLiveURL" name="DirectPostLiveURL"
                                                    onkeypress="HideErr('DirectPostLiveURLErr');">
                                                     <span id="DirectPostLiveURLErr"></span>
                                              </div>
                                           </div>
                                           <div class="row">
                                              <div class="col-md-2 form-group mb-0 tadmPadd labFontt">
                                                  <label for="exampleFormControlSelect1">Request Method:</label>
                                                  <select class="form-control fonT" id="RequestMethod" name="RequestMethod" >
                                                      <option value="1" >POST</option>
                                                      <option value="2" >GET</option>
                                                      <option value="3" >XmlinPost</option>
                                                      <option value="4" >XmlPost</option>
                                                      <option value="5" >InlineXML</option>
                                                  </select>
                                                  <span id="RequestMethodErr"></span>
                                              </div>
                                              <div class="col-md-2 form-group mb-0 tadmPadd labFontt">
                                                  <label for="exampleFormControlSelect1">Request Header:</label>
                                                  <select class="form-control fonT" id="RequestHeader" name="RequestHeader" >
                                                      <option value="1" >Content-Type: application/x-www-form-urlencoded</option>
                                                      <option value="2" >Content-Type:text/xml; charset=utf-8 </option>
                                                      <option value="3" >Content-type: application/xml </option>
                                                      <option value="4" >Content-type: application/json</option>
                                                  </select>
                                                  <span id="RequestHeaderErr"></span>
                                              </div>
                                              <div class="col-md-2 form-group tadmPadd labFontt">
                                                <label for="exampleInputEmail1">Parameter1:</label>
                                                  <input type="text" class="form-control txtPadd" 
                                                  value="" id="Parameter1" name="Parameter1"
                                                  onkeypress="HideErr('Parameter1Err');">
                                                   <span id="Parameter1Err"></span>
                                              </div>
                                              <div class="col-md-2 form-group tadmPadd labFontt">
                                                <label for="exampleInputEmail1">Parameter2:</label>
                                                  <input type="text" class="form-control txtPadd" 
                                                  value="" id="Parameter2" name="Parameter2"
                                                  onkeypress="HideErr('Parameter2Err');">
                                                   <span id="Parameter2Err"></span>
                                              </div>
                                              <div class="col-md-2 form-group tadmPadd labFontt">
                                                <label for="exampleInputEmail1">Parameter3:</label>
                                                  <input type="text" class="form-control txtPadd" 
                                                  value="" id="Parameter3" name="Parameter3"
                                                  onkeypress="HideErr('Parameter3Err');">
                                                   <span id="Parameter3Err"></span>
                                              </div>
                                              <div class="col-md-2 form-group tadmPadd labFontt">
                                                <label for="exampleInputEmail1">Parameter4:</label>
                                                  <input type="text" class="form-control txtPadd" 
                                                  value="" id="Parameter4" name="Parameter4"
                                                  onkeypress="HideErr('Parameter4Err');">
                                                   <span id="Parameter4Err"></span>
                                              </div>
                                           </div>
                                           <div class="row">
                                              <div class="col-md-4 form-group labFontt tablabFontt">
                                                <label>File: (You Can Select Multiple Files)</label>
                                                <div class="custom-file">
                                                  <input type="file" multiple class="custom-file-input" id="Buyerfiles" name="Buyerfiles[]" multiple>
                                                  <label class="custom-file-label fileLabel" for="Buyerfiles">No File Chosen</label>
                                                </div>
                                              </div>                            
                                              <div class="col-md-4 form-group mb-0 labFontt">
                                                <label for="exampleFormControlTextarea1">Buyer Notes:</label>
                                                <textarea class="form-control" id="BuyerNotes" name="BuyerNotes" rows="3"></textarea>
                                              </div>
                                            </div>
                                           <div class="row">
                                              <div class="col-md-6 nextttBtn"  id="BuyerToVendor" style="display:none;">
                                                 <input type="button" class="btn btn-primary pBtnFlo" id="BuyerToVendorBtn" value="Previous">
                                              </div>
                                              <div class="col-md-6 nextttBtn" id="BuyerToUser" style="display:none;">
                                                   <input type="button" class="btn btn-primary pBtnFlo" id="BuyerToUserBtn" value="Previous">
                                                </div>
                                              <div class="col-md-6 nextttBtn">
                                                 <button type="button" class="btn btn-primary nBtnFlo" id="AddCompanyBtnOnBuyer">Save</button>
                                              </div>
                                           </div>                                        
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                      </form>
                   </div>
                </div>
             </div>
          </div>
       </div>
       @include('layouts.Footer')
    </div>
    </div>
  </div>
  <input type="hidden" name="CampaignTypeVendor" id="CampaignTypeVendor" value=''>
  <input type="hidden" name="CampaignTypeBuyer" id="CampaignTypeBuyer" value=''>
  <input type="hidden" name="url" id="url" value='{{URL::to("/")}}'>
  <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
  <input type="hidden" name="loading" id="loading" value='{{ asset("Admin/images/loading_image.gif") }}'>
  @include('layouts.Script')
  <script src="{{asset('Ajax/CompanyManagementAdd.js')}}"></script> 
  <script>
  //GetCampaignDetailsVendor();
  //GetCampaignDetailsBuyer();
  </script>
</body>
