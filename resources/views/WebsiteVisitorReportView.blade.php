<!DOCTYPE html>
<html lang="en">
@include('layouts.Head')
<body>
  <div class="container-scroller">
    @include('layouts.Header')
    <div class="container-fluid page-body-wrapper">
      @include('layouts.Menu')
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="page-header row">
            <h3 class="page-title">
              Website Visitor Report
            </h3>
          </div>
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb breadcrumb-custom">
              <li class="breadcrumb-item"><a href="#"><i class="fas fa-file-alt menu-icon"></i> Reports</a></li>
              <li class="breadcrumb-item active" aria-current="page"><span><i class="fas fa-eye menu-icon"></i> Website Visitor Report</span></li>
            </ol>
          </nav>
           <div class="allDet">
              <div class="col-lg-12 col-md-12 col-sm-12 col-12 comP">                  
                  <div class="row">
                      <div class="col-md-3 form-group">
                          <div class="input-daterange input-group" id="datepicker">
                              <input type="text" value="{{date('m/d/Y')}}" class="input-sm form-control camH datePadd fonT" name="start" id="start" />
                              <span class="input-group-addon fonT">To</span>
                              <input type="text" value="{{date('m/d/Y')}}" class="input-sm form-control camH datePadd fonT" name="end" id="end" />
                          </div>
                      </div>                      
                      <div class="col-md-2 form-group tadmPadd">
                           <select class="form-control fonT" id="VendorID"  >
                               <option value="">Vendor</option>
                                @foreach($VendorList as $vl)
                                <option value="{{$vl->VendorID}}"> {{$vl->PartnerLabel}} ({{$vl->VendorID}}) </option>
                                @endforeach
                          </select>
                      </div> 
                      <div class="col-md-2 form-group">
                          <input type="text" class="form-control fonT camH" id="SubID" placeholder="Sub-ID">
                      </div> 
                      <div class="col-md-2 form-group">
                        <input type="text" class="form-control fonT camH" id="URL" placeholder="URL">
                      </div>
                      <div class="col-md-2 form-group">
                        <input type="text" class="form-control fonT camH" id="IPAddress" placeholder="IP Address">
                      </div>
                      <div class="col-md-1 form-group">
                          <button type="button" onclick="SearchWebsiteVisitorReport();" class="btn btn-success fonT mW nF fl tW noBoradi cbtnPadd sercFontt">
                            <i class="fa fa-search"></i> Search</button>
                      </div>                                             
                  </div>
                  <div class="boxS">
                    <div class="row">
                        <div class="col-md-5 mt-4">
                            <div class="col-md-6 tHits">
                                <p>Total Hits: <span id="Hits">0</span></p>
                            </div>
                            <div class="col-md-6 uIpadd">
                                <p>Unique IP Address: <span id="IPAddresss">0</span></p>
                            </div>
                        </div>
                        <div id="container" style="width: 100%; max-width: 1000px; margin: 0 auto"></div>
                    </div>
                    <div id="dataTable_wrapper" class="dataTables_wrapper dt-bootstrap4">
                      <input type="hidden" name="numofrecords" id="numofrecords" value='10'>
                        
                    </div>
                  </div>
              </div>
          </div>
       </div>
       @include('layouts.Footer')
    </div>
    </div>
  </div>
  <input type="hidden" name="url" id="url" value='{{URL::to("/")}}'>
  <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
  <input type="hidden" name="loading" id="loading" value='{{ asset("Admin/images/loading_image.gif") }}'>
  @include('layouts.Script')
  <script src="https://code.highcharts.com/highcharts.js"></script>
  <script src="https://code.highcharts.com/modules/exporting.js"></script>
  <script src="{{asset('Ajax/WebsiteVisitorReport.js')}}"></script> 
  <script>
  $(document).ready(function() {
    $('#sandbox-container .input-daterange').datepicker({});

    $('#datepicker').datepicker({
      weekStart: 0,
      daysOfWeekHighlighted: "6,0",
      autoclose: true,
      todayHighlight: true,
      dateFormat: 'mm/dd/yy'
    });
    $('#datepicker').datepicker("setDate", new Date());
  });
  
  </script>
</body>
