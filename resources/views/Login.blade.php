<!DOCTYPE html>
<html lang="en">
@include('layouts.Head')
<?php
$username = "";                          
$password = "";
$remember = "";
if(isset($_COOKIE["username"]))
{
  if($_COOKIE["username"]!="")
  {
    $username = $_COOKIE["username"];
    $password = $_COOKIE["password"];
    $remember = $_COOKIE["remember"];
  }

}
?>
<body>
  <div class="container-scroller">
    <nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row default-layout-navbar navBxsd">
      <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
        <a class="navbar-brand brand-logo" href="index.html"><img src="{{asset('Admin/images/logo.png')}}" alt="logo"/></a>
        <a class="navbar-brand brand-logo-mini" href="index.html"><img src="{{asset('Admin/images/small-logo.png')}}" alt="logo"/></a>
      </div>      
    </nav>
    <div class="container-fluid page-body-wrapper logInPadd">
      <div class="main-panel logInW">
        <div class="content-wrapper">
          <div class="container h-100">
            <div class="d-flex justify-content-center h-100">
              <div class="user_card">
                <div class="d-flex justify-content-center">
                  <div class="brand_logo_container">
                    <img src="{{asset('Admin/images/faces/default-avatar.png')}}" class="brand_logo" alt="Logo">
                  </div>
                  <div class="logInCont">
                    <h4>Ping/Post Account Access</h4>
                    <div class="logInContp">
                      <p>This portal is for accessing our Ping/Post API system. If you are looking for our CPA, CPC, CPL, CPM, etc network then please visit:</p>
                      <a href="https://revjolt.hasoffers.com/">https://revjolt.hasoffers.com/</a>
                    </div>
                  </div>
                </div>
                @if(Session::has('message'))
                  <div class="alert alrtDanger {{ Session::get('alert-class', 'alert-info') }}">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>{{ Session::get('message') }}</strong>
                  </div>
                @endif
                <div class="d-flex justify-content-center form_container nFormCont">
                  <div class="formW">
                  <form action="{{route('admin.AuthenticateLogin')}}" method="POST" id="LoginForm" name="LoginForm">
                    <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                    <div class="input-group mb-3 marBott">
                      <div class="input-group-append">
                        <span class="input-group-text1"><i class="fas fa-user"></i></span>
                      </div>
                      <input type="text" name="email" id="email" class="form-control input_user loginEmail" value="<?php echo $username; ?>" 
                          placeholder="Email" onkeypress="HideErr('emailErr');">
                      <span id="emailErr"></span>
                    </div>
                    <div class="input-group mb-2 mBott">
                      <div class="input-group-append">
                        <span class="input-group-text1"><i class="fas fa-key"></i></span>
                      </div>
                      <input type="password" name="password" id="password" class="form-control input_pass loginPass" value="<?php echo $password; ?>" 
                          placeholder="Password" onkeypress="HideErr('passwordErr');">
                      <span id="passwordErr"></span>
                    </div>
                    <div class="form-group mb-3 mBott">
                      <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="remember" name="remember" <?php if($remember!="") { echo "checked"; } ?>>
                        <label class="custom-control-label custCh" for="remember">Keep Me Logged In For 2 Weeks.</label>
                      </div>
                    </div>
                  </form>
                </div>
                </div>
                <div class="d-flex justify-content-center login_container">
                  <button type="button" name="button" class="btn login_btn newLogPadd" id="LoginBtn">Access</button>
                </div>
                <div class="mt-2">
                  <div class="d-flex justify-content-center links donAcc">
                    Don't have an account? <a href="#" class="ml-2">Sign Up</a>
                  </div>
                  <div class="d-flex justify-content-center links forgPass">
                    <a href="#">Forgot your password?</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        @include('layouts.Footer')
      </div>
    </div>
  </div>
  @include('layouts.Script')
  <input type="hidden" name="url" id="url" value='{{URL::to("/")}}'>
  <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
  <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js"></script> 
  <script src="{{asset('Ajax/Login.js')}}"></script> 
</body>
