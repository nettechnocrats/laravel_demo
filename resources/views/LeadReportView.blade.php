<!DOCTYPE html>
<html lang="en">
@include('layouts.Head')

<body>
  <div class="container-scroller">
    @include('layouts.Header')
    <div class="container-fluid page-body-wrapper">
      @include('layouts.Menu')
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="page-header row">
            <h3 class="page-title">
              Reports
            </h3>
          </div>
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb breadcrumb-custom">
              <li class="breadcrumb-item"><a href="#"><i class="fas fa-file-alt menu-icon"></i> Reports</a></li>
              <li class="breadcrumb-item active" aria-current="page"><span><i class="fas fa-file-alt menu-icon"></i> Lead Report</span></li>
            </ol>
          </nav>
          <div class="col-md-12" id="ReturnCodeMsgg">
          </div>
          <div class="allDet">
             <div class="col-lg-12 col-md-12 col-sm-12 col-12 comP">
                <form class="boxS">
                   <div class="row">
                      <div class="col-md-3 form-group newFoGrp">
                         <select class="form-control fonT" id="Campaign" onchange="GetVendorBuyerUsingCampaignID();">
                            @foreach($CampaignDropDown as $cdd)
                            <option value="{{$cdd->CampaignID}}"
                              <?php if ($SessionCampaignID == $cdd->CampaignID) {echo "selected";} ?>>{{$cdd->CampaignName}}</option>
                            @endforeach
                         </select>
                      </div> 
                      <div class="col-md-3 form-group newFoGrp">
                         <div class="input-daterange input-group" id="datepicker">
                            <input type="text" value="{{$StartDate}}" class="input-sm form-control camH datePadd fonT" name="start" id="StartDate"/>
                            <span class="input-group-addon fonT">To</span>
                            <input type="text" value="{{$EndDate}}" class="input-sm form-control camH datePadd fonT" name="end" id="EndDate"/>
                         </div>
                      </div>                    
                                             
                      <div class="col-md-2 form-group tadmPadd labFontt newFoGrp">
                        <input type="text" class="form-control boxH" id="SubID" placeholder="SubID">
                      </div> 
                      <div class="col-md-2 form-group tadmPadd labFontt newFoGrp">
                        <input type="text" class="form-control boxH" id="Email" placeholder="Email">
                      </div> 
                      <div class="col-md-2 form-group tadmPadd labFontt newFoGrp">
                        <input type="text" class="form-control boxH" id="LeadID" placeholder="LeadID">
                      </div>                      
                   </div>
                   <div class="row">
                      <div class="col-md-3 form-group mb-0 newFoGrp">
                         <select class="form-control fonT" id="VendorCompany" onchange="GetVendorUsingVendorCompany();">
                            <option value="All">All Vendor Company</option>
                              
                         </select>
                      </div>
                      <div class="col-md-3 form-group mb-0 newFoGrp">
                         <select class="form-control fonT" id="Vendor" >
                            <option value="All">All Vendor</option>
                             
                         </select>
                      </div>
                      <div class="col-md-3 form-group mb-0 newFoGrp">
                         <select class="form-control fonT" id="BuyerCompany" onchange="GetBuyerUsingBuyerCompany();">
                            <option value="All">All Buyer Company</option>
                              
                         </select>
                      </div>
                      <div class="col-md-3 form-group mb-0 newFoGrp">
                         <select class="form-control fonT" id="Buyer" >
                            <option value="All">All Buyer</option>
                             
                         </select>
                      </div>                      
                   </div>
                   <div class="row">
                      <div class="col-md-2 form-group newFoGrp">
                         <select class="form-control fonT" id="Status" >
                            <option value="All" <?php if ($SessionStatus == "All") {echo "selected";} ?>>All Status</option>
                            <option value="0">All Live</option>
                            <option value="0_1" <?php if ($SessionStatus == "0_1") {echo "selected";} ?>>Live Accepted</option>
                            <option value="0_0" <?php if ($SessionStatus == "0_0") {echo "selected";} ?>>Live Rejected</option>
                            <option value="0_2">Live Returned</option>
                            <option value="1">All Test</option>
                            <option value="1_1">Test Accepted</option>
                            <option value="1_0">Test Rejected</option>
                            <option value="1_2">Test Returned</option>
                         </select>
                      </div>
                      <div class="col-md-2 form-group" id="RedirectedDiv" >
                         <select class="form-control fonT" id="Redirected" >
                            <option value="All">All Redirect Status</option>
                            <option value="1">Redirected</option>
                            <option value="0">Non-Redirected</option>
                         </select>
                      </div>
                      <div class="col-md-2 form-group mb-0" id="TiersDiv" >
                         <select class="form-control fonT" id="Tiers" >
                            <option value="All">All Tiers</option>
                              @foreach($TiersDropDown as $t)
                              <option value="{{$t->TierID}}">{{$t->TierID}}</option>
                              @endforeach
                         </select>
                      </div>
                      <div class="col-md-2 form-group newFoGrp" id="PingPostStatusDiv" >
                         <select class="form-control fonT" id="PingPostStatus" >
                            <option value="">Ping Or Post </option>
                            <option value="Ping">PING</option>
                            <option value="Post">POST</option>
                         </select>
                      </div>                     
                      
                      <div class="col-md-2 form-group tadmPadd labFontt newFoGrp" id="SearchBtnDiv">
                         <button type="button" class="btn btn-success fonT mW nF fl noBoradi cbtnPadd sercFontt"
                         id="SearchBtn" onclick="SearchViewProcessedLeadReport();"><i class="fa fa-search"></i> Search Leads</button>
                      </div>
                   </div>
                </form>
                <div class="boxS" id="Lead">
                  <input type="hidden" name="numofrecords" id="numofrecords" value='10'>
                </div>
              </div>
          </div>
       </div>
       @include('layouts.Footer')
    </div>
    </div>
  </div>


  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header leadRetHead">
          <h4 class="modal-title">Lead To Return: <span id="LeadID"></span></h4>
          <button type="button" class="close leadRetClose" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body" id="ReturnDiv"></div>
      </div>      
    </div>
  </div>


  <input type="hidden" name="url" id="url" value='{{URL::to("/")}}'>
  <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
  <input type="hidden" name="loading" id="loading" value='{{ asset("Admin/images/loading.gif") }}'>
  @include('layouts.Script')
  <script src="{{asset('Ajax/LeadReport.js')}}"></script> 
  <script>
  $(document).ready(function() {
    $('#sandbox-container .input-daterange').datepicker({});

    $('#datepicker').datepicker({
      weekStart: 0,
      daysOfWeekHighlighted: "6,0",
      autoclose: true,
      todayHighlight: true,
      format: 'yyyy-mm-dd'
    });
    $('#datepicker').datepicker("setDate", new Date());
  });
  </script>
</body>