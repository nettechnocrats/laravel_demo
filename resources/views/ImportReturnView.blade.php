<!DOCTYPE html>
<html lang="en">
@include('layouts.Head')
<body>
  <div class="container-scroller">
    @include('layouts.Header')
    <div class="container-fluid page-body-wrapper">
      @include('layouts.Menu')
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="page-header row">
            <h3 class="page-title">
              Upload Returns
            </h3>
          </div>
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb breadcrumb-custom">
              <li class="breadcrumb-item"><a href="#"><i class="fas fa-users-cog menu-icon"></i> Returns</a></li>
              <li class="breadcrumb-item active" aria-current="page"><span class="brFont"><i class="fas fa-file-import menu-icon"></i> Upload Returns</span></li>
            </ol>
          </nav>
          <div class="col-md-12">
            @if(Session::has('message'))
              <div class="alert alrtDanger {{ Session::get('alert-class', 'alert-info') }}">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>{{ Session::get('message') }}</strong>
              </div>
            @endif
          </div>
          <div class="allDet">
           <div class="col-lg-12 col-md-12 col-sm-12 col-12 comP">
              <div id="dataTable_wrapper" class="dataTables_wrapper dt-bootstrap4">
                  <form action="{{route('admin.ImportReturnsProcess')}}" enctype="multipart/form-data" name="SaveReturnsFrom" id="SaveReturnsFrom" method="POST">
                  <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}"> 
                  <div class="row">                    
                    <div class="col-md-3 form-group newFoGrp">
                      <select class="form-control fonT" id="Campaign" name="Campaign" onchange="GetReturnCodeList();">
                          @foreach($CampaignDropDown as $cdd)
                          <option value="{{$cdd->CampaignID}}">{{$cdd->CampaignName}}</option>
                          @endforeach
                      </select>
                    </div>
                    <div class="col-md-3 form-group newFoGrp">
                      <div class="custom-file">
                        <input onchnage="HideErr('FileErr');" type="file" class="custom-file-input" id="File" name="file">
                        <span class="Del" id="DelFile" style="display:none;"><i class="fas fa-times"></i></span>
                        <label class="custom-file-label fileLabel" id="NameFile" for="customFile">No File Chosen</label>
                      </div>
                      <span id="FileErr" class="errordisp"></span>
                    </div>
                    <div class="col-md-3 form-group newFoGrp">
                      <div class="input-daterange input-group" id="datepicker">
                        <input type="text" value="{{$StartDate}}" class="input-sm form-control camH datePadd fonT dtZindex" name="start" id="StartDate"/>
                        <span class="input-group-addon fonT">To</span>
                        <input type="text" value="{{$EndDate}}" class="input-sm form-control camH datePadd fonT" name="end" id="EndDate"/>
                      </div>
                    </div> 
                    <div class="col-md-3 form-group newFoGrp">
                      <button type="button" id="SaveReturnsBtn" class="btn btn-success noBoradi nPadd cbtnPadd sercFontt imB">Import</button>
                       <a href="{{asset('ReturnsSample/RealTimeReturnSample.csv')}}" class="btn btn-success noBoradi nPadd cbtnPadd sercFontt dwSCSV Floa">Download Sample CSV</a>
                    </div> 
                 </div>
                 </form>                 
                 <div class="row">
                    <div class="col-md-6 pl-0">
                       <div class="table-responsive">
                          <table class="table table-bordered tableFont txtL nTabl bt">
                             <thead>
                                <tr>
                                   <th colspan="6" class="text-center pt-1 pb-1" style="font-size: 1rem;">Parameters Specs</th>
                                </tr>
                             </thead>
                             <thead class="thead-dark">
                                <tr>
                                   <th scope="col" style="padding: 0.684rem; font-size: 0.8rem; font-weight: 700;">Parameters:</th>
                                   <th scope="col" style="padding: 0.684rem; font-size: 0.8rem; font-weight: 700;">Value:</th>
                                   <th scope="col" style="padding: 0.684rem; font-size: 0.8rem; font-weight: 700;">Required?:</th>
                                </tr>
                             </thead>
                             <tbody>
                                <tr>
                                    <td>LeadID</td>
                                    <td>OUR LeadID for the lead (if known) </td>
                                    <td>Yes, if LeadEmail NOT provided</td>
                                </tr>
                                <tr>
                                    <td>LeadEmail</td>
                                    <td>If LeadID is Blank Then Required</td>
                                    <td>Yes, if LeadID NOT provided</td>
                                </tr>
                                <tr>
                                    <td>ReturnCode</td>
                                    <td>Please see ReturnCode List Below</td>
                                    <td>Yes</td>
                                </tr>
                                <tr>
                                    <td>ReturnMessage</td>
                                    <td>Free Text Explaining Return Reason</td>
                                    <td>No</td>
                                </tr>
                             </tbody>
                          </table>
                       </div>
                    </div>
                    <div class="col-md-6 pr-0 padLeft">
                       <div class="table-responsive">
                          <table class="table table-bordered tableFont txtL nTabl bt ">
                             <thead>
                                <tr>
                                   <th colspan="6" class="text-center pt-1 pb-1" style="font-size: 1rem;">Return Code List</th>
                                </tr>
                             </thead>
                             <thead class="thead-dark">
                                <tr>
                                   <th scope="col" style="width: 19%; padding: 0.684rem; font-size: 0.8rem; font-weight: 700;">Return Code:</th>
                                   <th scope="col" style="padding: 0.684rem; font-size: 0.8rem; font-weight: 700;">Return Reason:</th>
                                </tr>
                             </thead>
                             <tbody id="ReturnCodeListTable">
                                
                             </tbody>
                          </table>
                       </div>
                    </div>
                 </div>
                 <div id="UploadedFiles">
                  <input type="hidden" name="numofrecords" id="numofrecords" value='10'>
                 </div>
              </div>
           </div>
        </div>
       </div>
       @include('layouts.Footer')
    </div>
    </div>
  </div>
  <input type="hidden" name="url" id="url" value='{{URL::to("/")}}'>
  <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
  <input type="hidden" name="loading" id="loading" value='{{ asset("Admin/images/loading_image.gif") }}'>
  @include('layouts.Script')
  <script src="{{asset('Ajax/ImportReturns.js')}}"></script> 
  <script>
 $(document).ready(function() {
    $('#sandbox-container .input-daterange').datepicker({});

    $('#datepicker').datepicker({
      weekStart: 0,
      daysOfWeekHighlighted: "6,0",
      autoclose: true,
      todayHighlight: true,
      format: 'yyyy-mm-dd'
    });
    $('#datepicker').datepicker("setDate", new Date());
  });
  </script>
</body>
