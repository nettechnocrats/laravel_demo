<!DOCTYPE html>
<html lang="en">
@include('layouts.Head')
<body>
  <div class="container-scroller">
    @include('layouts.Header')
    <div class="container-fluid page-body-wrapper">
      @include('layouts.Menu')
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="page-header row">
            <h3 class="page-title">
             Vendor Management 
            </h3>
          </div>
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb breadcrumb-custom">
              <li class="breadcrumb-item"><a href="#"><i class="fas fa-users-cog menu-icon"></i> Admin Tools</a></li>
              <li class="breadcrumb-item"><a href="{{route('admin.VendorManagementView')}}"><i class="fas fa-users menu-icon"></i> Vendor Management </a></li>
              <li class="breadcrumb-item active" aria-current="page"><span><i class="fas fa-user-plus menu-icon"></i> Edit Vendor Details </span></li>
            </ol>
          </nav>
          <div class="col-md-12">
            @if(Session::has('message'))
              <div class="alert alrtDanger {{ Session::get('alert-class', 'alert-info') }}">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>{{ Session::get('message') }}</strong>
              </div>
            @endif
          </div>
          <div class="col-md-12" id="Message">
          </div>
          <div class="allDet">
             <div class="col-md-12 grid-margin stretch-card">
                <div class="card">
                   <div class="card-body">                      
                     <div class="addUser">
                          <div class="row">
                            <div class="page-header-below">
                               <h3 class="page-title-below">
                                  Edit Vendor Details
                               </h3>
                            </div>                            
                          </div>                          
                         
                         <form action="{{route('admin.SaveVendorDetails')}}" name="VendorUpdateForm" id="VendorUpdateForm" method="POST">
                          <input type="hidden" name="VendorID" id="VendorID" value="{{$VendorDetails->VendorID}}">
                          <input type="hidden" name="VendorApiKey" id="VendorApiKey" value="{{$VendorDetails->APIKey}}">
                          <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                          <input type="hidden" name="CampaignType" id="CampaignType" value="{{$CampaignDetails->CampaignType}}">
                          <div class="row">
                            <div class="col-md-2 form-group mb-0 tadmPadd labFontt">
                              <label for="exampleFormControlSelect1">Vendor ID:</label>
                              <input type="text" disabled class="form-control txtPadd" 
                              value="{{$VendorDetails->VendorID}}" placeholder="">
                            </div>
                            <div class="col-md-4 form-group mb-0 tadmPadd labFontt">
                              <label for="exampleFormControlSelect1">Vendor API-KEY:</label>
                              <input type="text" disabled class="form-control txtPadd" 
                                value="{{$VendorDetails->APIKey}}" placeholder="">
                            </div>
                          </div> 
                          <div class="row">
                            <div class="col-md-4 form-group mb-0 tadmPadd labFontt">
                                <label for="exampleFormControlSelect1">Campaign:</label>
                                <select class="form-control fonT" id="Campaign" name="Campaign" 
                                    onchange="GetCampaignDetails(),HideErr('CompanyErr');">
                                    @foreach($CampaignDropDown as $cdd)
                                    <option value="{{$cdd->CampaignID}}"
                                      <?php if($cdd->CampaignID==$VendorDetails->CampaignID){echo "selected";}?>>{{$cdd->CampaignName}}</option>
                                    @endforeach
                                </select>
                                <span id="CampaignErr"></span>
                            </div>
                            <div class="col-md-4 form-group mb-0 tadmPadd labFontt">
                              <label for="exampleFormControlSelect1">Company:</label>
                                <select class="form-control fonT" id="Company" name="Company"
                                  onchange="HideErr('CompanyErr');">
                                  <option value="">Select Company</option>
                                  @foreach($CompanyList as $cl)
                                  <option value="{{$cl->CompanyID}}"
                                  <?php if($cl->CompanyID==$VendorDetails->CompanyID){echo "selected";}?>>{{$cl->CompanyName}}</option>
                                  @endforeach                                 
                                </select>
                                <span id="CompanyErr"></span>
                            </div>
                            <div class="col-md-4 form-group tadmPadd">
                              <div class="form-check form-check-flat form-check-primary adComp adComp-primary disPlay tabdisPlay mt-4">
                                <label class="form-check-label adComp-label">
                                <input type="checkbox" class="form-check-input" id="ShowInactive"
                                onclick="GetCompanyUsingShwowInactiveAndTest();">
                                  Show Inactive
                                <i class="input-helper"></i></label>
                              </div>
                              <div class="form-check form-check-flat form-check-primary adComp adComp-primary disPlay tabdisPlay mt-4">
                                <label class="form-check-label adComp-label">
                                <input type="checkbox" class="form-check-input" id="ShowTest"
                                onclick="GetCompanyUsingShwowInactiveAndTest();">
                                  Show Test
                                <i class="input-helper"></i></label>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-2 form-group tadmPadd labFontt">
                              <label for="exampleInputEmail1">Admin Label:</label>
                                <input type="text" class="form-control txtPadd" 
                                value="{{$VendorDetails->AdminLabel}}" id="AdminLabel" name="AdminLabel"
                                onkeypress="HideErr('AdminLabelErr');">
                                 <span id="AdminLabelErr"></span>
                            </div>
                            <div class="col-md-2 form-group tadmPadd btnCen">
                              <button type="button" id="CopyAdminLable" class="btn btn-success noBoradi mr-2">&gt;&gt;</button>
                            </div>
                            <div class="col-md-2 form-group tadmPadd labFontt">
                              <label for="exampleInputEmail1">Partner Label:</label>
                                <input type="text" class="form-control txtPadd" 
                                  value="{{$VendorDetails->PartnerLabel}}" id="PartnerLabel" name="PartnerLabel"
                                  onkeypress="HideErr('PartnerLabelErr');">
                                   <span id="PartnerLabelErr"></span>
                            </div>
                            <div class="col-md-2 form-group mb-0 tadmPadd labFontt">
                              <label for="PayoutCalculation">Payout Calculation:</label>
                                <select class="form-control fonT" id="PayoutCalculation" name="PayoutCalculation">
                                  <option value="RevShare" <?php if($VendorDetails->PayoutCalculation=="RevShare"){ echo "selected"; } ?>>RevShare</option>
                                  <option value="FixedPrice" <?php if($VendorDetails->PayoutCalculation=="FixedPrice"){ echo "selected"; } ?>>Fixed Price</option>
                                  <option value="TierPrice" <?php if($VendorDetails->PayoutCalculation=="TierPrice"){ echo "selected"; } ?>>Tier Price</option>
                                  <option value="MinPrice" <?php if($VendorDetails->PayoutCalculation=="MinPrice"){ echo "selected"; } ?>>Min Price</option>
                                  <option value="BucketPrice" <?php if($VendorDetails->PayoutCalculation=="BucketPrice"){ echo "selected"; } ?>>Bucket Price</option>
                                  <option value="BucketSystemBySubID" <?php if($VendorDetails->PayoutCalculation=="BucketSystemBySubID"){ echo "selected"; } ?>>Bucket System By SubID</option>
                                </select>
                            </div>
                            <?php 
                            $PayoutCalculation = $VendorDetails->PayoutCalculation;
                            if($PayoutCalculation=='FixedPrice')
                            { 
                                 $PayoutCalculationFixedPrice="style='display:block'";
                                 $PayoutCalculationTierPrice="style='display:none'";
                                 $PayoutCalculationRevShare="style='display:none'";
                            } 
                            else if($PayoutCalculation=='RevShare')
                            {
                                 $PayoutCalculationRevShare="style='display:block'";
                                 $PayoutCalculationTierPrice="style='display:none'";
                                 $PayoutCalculationFixedPrice="style='display:none'";
                            }
                             else if($PayoutCalculation=='TierPrice')
                            {
                                 $PayoutCalculationRevShare="style='display:none'";
                                 $PayoutCalculationTierPrice="style='display:block'";
                                 $PayoutCalculationFixedPrice="style='display:none'";
                            }
                            ?>
                            <div class="col-md-2 form-group tadmPadd labFontt" <?php echo $PayoutCalculationRevShare ?>  
                                id="RevsharePercentInput">
                              <label for="exampleInputEmail1">Revshare: </label>
                              <input type="text" class="form-control txtPadd" value="{{$VendorDetails->RevsharePercent}}" 
                              placeholder="70" id="Revshare" name="Revshare" onkeypress="HideErr('RevshareErr');">
                              <span id="RevshareErr"></span>
                            </div>
                            <div class="col-md-2 form-group tadmPadd labFontt" <?php echo $PayoutCalculationFixedPrice ?> 
                                id="FixedPriceInput">
                              <label for="exampleInputEmail1">Fixed Price:</label>
                              <input type="text" class="form-control txtPadd" value="{{$VendorDetails->FixedPrice}}" 
                              placeholder="70" id="FixedPrice" name="FixedPrice" onkeypress="HideErr('FixedPriceErr');">
                              <span id="FixedPriceErr"></span>
                            </div>
                            <div class="col-md-4 form-group tadmPadd labFontt" <?php echo $PayoutCalculationTierPrice ?> 
                                id="TierPriceInput">
                              <label for="exampleInputEmail1">Revshare Percent (for when Tier Payout is $0.00):</label>
                              <input type="text" class="form-control txtPadd" value="{{$VendorDetails->RevsharePercent}}"
                               placeholder="70" id="TierPrice" name="TierPrice" onkeypress="HideErr('TierPriceErr');">
                              <span id="TierPriceErr"></span>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-1 form-group tadmPadd labFontt">
                              <label for="exampleInputEmail1">Daily Cap:</label>
                                <input type="text" onkeypress="HideErr('DailyCapErr');" value="{{$VendorDetails->DailyCap}}" 
                                  id="DailyCap" name="DailyCap" class="form-control txtPadd" placeholder="Daily Cap"
                                  onkeypress="HideErr('TierPriceErr');">
                                <span id="DailyCapErr"></span>
                            </div>
                            <div class="col-md-1 form-group tadmPadd labFontt">
                              <label for="exampleInputEmail1">Total Cap:</label>
                                <input type="text" onkeypress="HideErr('TotalCapErr');" value="{{$VendorDetails->TotalCap}}" 
                                  id="TotalCap" name="TotalCap" class="form-control txtPadd" placeholder="Total Cap"
                                  onkeypress="HideErr('TotalCapErr');">
                                <span id="TotalCapErr"></span>
                            </div>
                            <?php if($VendorDetails->VendorStatus=='1'){ ?>
                            <div class="col-md-2 form-group tadmPadd labFontt">
                              <label for="exampleInputEmail1">Vendor Status:</label>
                                <div class="onoffswitch8">
                                  <input type="checkbox" name="VendorStatus" class="onoffswitch8-checkbox" 
                                  id="VendorStatus" checked="">
                                  <label class="onoffswitch8-label" for="VendorStatus">
                                    <span class="onoffswitch8-inner"></span>
                                    <span class="onoffswitch8-switch"></span>
                                  </label>
                                </div>
                            </div>
                            <?php } else {?>
                            <div class="col-md-2 form-group tadmPadd labFontt">
                              <label for="exampleInputEmail1">Vendor Status:</label>
                                <div class="onoffswitch8">
                                  <input type="checkbox" name="VendorStatus" class="onoffswitch8-checkbox" 
                                  id="VendorStatus">
                                  <label class="onoffswitch8-label" for="VendorStatus">
                                    <span class="onoffswitch8-inner"></span>
                                    <span class="onoffswitch8-switch"></span>
                                  </label>
                                </div>
                            </div>
                            <?php }?>
                            <?php if($VendorDetails->VendorTestMode=='1'){ ?>
                            <div class="col-md-2 form-group tadmPadd labFontt">
                              <label for="exampleInputEmail1">Vendor Mode?</label>
                                <div class="onoffswitch9">
                                  <input type="checkbox" name="VendorTestMode" class="onoffswitch9-checkbox"
                                     id="VendorTestMode" checked="">
                                  <label class="onoffswitch9-label" for="VendorTestMode">
                                    <span class="onoffswitch9-inner"></span>
                                    <span class="onoffswitch9-switch"></span>
                                  </label>
                                </div>
                            </div>
                            <?php } else {?>
                            <div class="col-md-2 form-group tadmPadd labFontt">
                              <label for="exampleInputEmail1">Vendor Mode?</label>
                                <div class="onoffswitch9">
                                  <input type="checkbox" name="VendorTestMode" class="onoffswitch9-checkbox" 
                                    id="VendorTestMode" >
                                  <label class="onoffswitch9-label" for="VendorTestMode">
                                    <span class="onoffswitch9-inner"></span>
                                    <span class="onoffswitch9-switch"></span>
                                  </label>
                                </div>
                            </div>
                            <?php }?>
                            <div class="col-md-2 form-group mb-0 tadmPadd labFontt">
                              <label for="exampleFormControlSelect1">Tracking Pixel Type:</label>
                                <select class="form-control fonT" id="TrackingPixelType" name="TrackingPixelType">
                                  <option value="Pixel" <?php if($VendorDetails->TrackingPixelType=='Pixel'){echo "selected";}?>>Pixel</option>
                                  <option value="PostBack" <?php if($VendorDetails->TrackingPixelType=='PostBack'){echo "selected";}?>>Post Back</option>
                                </select>
                            </div>
                            <?php if($VendorDetails->TrackingPixelStatus=='1'){ ?>
                            <div class="col-md-2 form-group tadmPadd labFontt">
                              <label for="exampleInputEmail1">Tracking Pixel Status?</label>
                                <div class="onoffswitch10">
                                  <input type="checkbox" name="TrackingPixelStatus" class="onoffswitch10-checkbox"
                                   id="TrackingPixelStatus" checked="">
                                  <label class="onoffswitch10-label" for="TrackingPixelStatus">
                                    <span class="onoffswitch10-inner"></span>
                                    <span class="onoffswitch10-switch"></span>
                                  </label>
                                </div>
                            </div>
                            <?php } else {?>
                            <div class="col-md-2 form-group tadmPadd labFontt">
                              <label for="exampleInputEmail1">Tracking Pixel Status?</label>
                                <div class="onoffswitch10">
                                  <input type="checkbox" name="TrackingPixelStatus" class="onoffswitch10-checkbox"
                                   id="TrackingPixelStatus" >
                                  <label class="onoffswitch10-label" for="TrackingPixelStatus">
                                    <span class="onoffswitch10-inner"></span>
                                    <span class="onoffswitch10-switch"></span>
                                  </label>
                                </div>
                            </div>
                            <?php }?> 
                          </div>
                          <div class="row">
                            <?php 
                            if($CampaignDetails->CampaignType=="DirectPost"){                           
                              $MaxTimeOutInput = "style='display:block;'";
                              $MaxPingTimeOutInput = "style='display:none;'";
                              $MaxPostTimeOutInput = "style='display:none;'";
                            }else if($CampaignDetails->CampaignType=="PingPost"){
                              $MaxTimeOutInput = "style='display:none;'";
                              $MaxPingTimeOutInput = "style='display:block;'";
                              $MaxPostTimeOutInput = "style='display:block;'";
                            }
                            ?>
                             <div class="col-md-2 form-group tadmPadd labFontt" id="MaxTimeOutInput" <?php echo $MaxTimeOutInput; ?>>
                              <label for="exampleInputEmail1">Max Time Out:</label>
                                <input type="text" class="form-control txtPadd" placeholder="0" 
                                id="MaxTimeOut" name="MaxTimeOut" value="{{$VendorDetails->MaxTimeOut}}" onkeypress="HideErr('MaxTimeOutErr');">
                                <span id="MaxTimeOutErr"></span>
                            </div>
                            <div class="col-md-2 form-group tadmPadd labFontt" id="MaxPingTimeOutInput" <?php echo $MaxPingTimeOutInput; ?>>
                              <label for="exampleInputEmail1">Max Ping Time Out:</label>
                                <input type="text" class="form-control txtPadd" placeholder="0" 
                                id="MaxPingTimeOut" name="MaxPingTimeOut" value="{{$VendorDetails->MaxPingTimeOut}}" onkeypress="HideErr('MaxPingTimeOutErr');">
                                <span id="MaxPingTimeOutErr"></span>
                            </div>
                            <div class="col-md-2 form-group tadmPadd labFontt" id="MaxPostTimeOutInput" <?php echo $MaxPostTimeOutInput; ?>>
                              <label for="exampleInputEmail1">Max Post Time Out:</label>
                                <input type="text" class="form-control txtPadd" placeholder="0" 
                                id="MaxPostTimeOut" name="MaxPostTimeOut" value="{{$VendorDetails->MaxPostTimeOut}}" onkeypress="HideErr('MaxPostTimeOutErr');">
                                <span id="MaxPostTimeOutErr"></span>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-2 form-group mb-0 labFontt">
                              <label for="exampleFormControlSelect1">Insert Token:</label>
                                <select class="form-control" id="insert_Token_type" name="insert_Token_type" onchange="updateTextBox(this)">
                                  <option value="aid">aid</option>
                                  <option value="SubID">SubID</option>
                                  <option value="TestMode">TestMode</option>
                                  <option value="TransactionID">TransactionID</option>
                                  <option value="VendorLeadPrice">VendorLeadPrice</option>
                              </select>
                            </div>
                            <div class="col-md-5 form-group mb-0 labFontt">
                              <label for="exampleFormControlTextarea1">Tracking Pixel Code:</label>
                              <textarea class="form-control" id="TrackingPixelCode" name="TrackingPixelCode" rows="3">{{$VendorDetails->TrackingPixelCode}}</textarea>
                            </div>
                            <div class="col-md-5 form-group mb-0 labFontt">
                              <label for="exampleFormControlTextarea1">Vendor Notes:</label>
                              <textarea class="form-control" id="VendorNotes" name="VendorNotes" rows="3">{{$VendorDetails->VendorNotes}}</textarea>
                            </div>
                          </div>
                          <div class="row">
                           <button type="button" class="btn btn-warning btn-fw noBoradi mr-2" 
                              id="SendMailbtn">Send E-Mail To Admin User.</button>
                          </div>
                          <div class="row">
                            <button type="button" class="btn btn-success btn-fw noBoradi mr-2" id="VendorUpdateBtn">Save</button>
                            <a href="{{ route('admin.VendorManagementView') }}" class="btn btn-danger btn-fw noBoradi">Cancel</a>
                          </div>
                        </form>
                      </div>
                   </div>
                </div>
             </div>
          </div>
       </div>
       @include('layouts.Footer')
    </div>
    </div>
  </div>
  <input type="hidden" name="url" id="url" value='{{URL::to("/")}}'>
  <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
  <input type="hidden" name="loading" id="loading" value='{{ asset("Admin/images/loading_image.gif") }}'>
  @include('layouts.Script')
  <script src="{{asset('Ajax/VendorManagement.js')}}"></script> 
  <script>
  $(document).ready(function() {
    $('#sandbox-container .input-daterange').datepicker({});

    $('#datepicker').datepicker({
      weekStart: 0,
      daysOfWeekHighlighted: "6,0",
      autoclose: true,
      todayHighlight: true,
      dateFormat: 'mm/dd/yy'
    });
    $('#datepicker').datepicker("setDate", new Date());
  });
  </script>
</body>
