<!DOCTYPE html>
<html lang="en">
@include('layouts.Head')
<body>
  <div class="container-scroller">
    @include('layouts.Header')
    <div class="container-fluid page-body-wrapper">
      @include('layouts.Menu')
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="page-header row">
            <h3 class="page-title">
             Buyer Management 
            </h3>
          </div>
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb breadcrumb-custom">
              <li class="breadcrumb-item"><a href="#"><i class="fas fa-users-cog menu-icon"></i> Admin Tools</a></li>
              <li class="breadcrumb-item"><a href="{{route('admin.BuyerManagementView')}}"><i class="fas fa-users menu-icon"></i> Buyer Management </a></li>
              <li class="breadcrumb-item active" aria-current="page"><span><i class="fas fa-user-plus menu-icon"></i> Edit Buyer Details </span></li>
            </ol>
          </nav>
          <div class="col-md-12">
            @if(Session::has('message'))
              <div class="alert alrtDanger {{ Session::get('alert-class', 'alert-info') }}">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>{{ Session::get('message') }}</strong>
              </div>
            @endif
          </div>
          <div class="col-md-12" id="Message">
          </div>
          <div class="allDet">
             <div class="col-md-12 grid-margin stretch-card">
                <div class="card">
                   <div class="card-body">                      
                     <div class="addUser">
                          <div class="row">
                            <div class="page-header-below">
                               <h3 class="page-title-below">
                                  Edit Buyer Details
                               </h3>
                            </div>                            
                          </div>                          
                         
                         <form action="{{route('admin.SaveBuyerDetails')}}" name="BuyerUpdateForm" 
                          id="BuyerUpdateForm" method="POST" enctype="multipart/form-data">
                          <input type="hidden" name="BuyerID" id="BuyerID" value="{{$BuyerDetails->BuyerID}}">
                          <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                          <input type="hidden" name="CampaignType" id="CampaignType" value="{{$CampaignDetails->CampaignType}}">
                          <input type="hidden" name="Campaign" id="Campaign" value="{{$CampaignDetails->CampaignID}}">
                          <div class="row">
                            <div class="col-md-2 form-group mb-0 tadmPadd labFontt">
                              <label for="exampleFormControlSelect1">Buyer ID:</label>
                              <input type="text" disabled class="form-control txtPadd" 
                              value="{{$BuyerDetails->BuyerID}}" placeholder="">
                            </div>                          
                            <div class="col-md-2 form-group mb-0 tadmPadd labFontt">
                                <label for="exampleFormControlSelect1">Campaign:</label>
                                <select class="form-control fonT" id="Campaign" name="Campaign" disabled 
                                    onchange="GetCampaignDetails(),HideErr('CompanyErr');">
                                    @foreach($CampaignDropDown as $cdd)
                                    <option value="{{$cdd->CampaignID}}"
                                      <?php if($cdd->CampaignID==$BuyerDetails->CampaignID){echo "selected";}?>>{{$cdd->CampaignName}}</option>
                                    @endforeach
                                </select>
                                <span id="CampaignErr"></span>
                            </div>
                            <div class="col-md-2 form-group mb-0 tadmPadd labFontt">
                              <label for="exampleFormControlSelect1">Company:</label>
                                <select class="form-control fonT" id="Company" name="Company"
                                  onchange="HideErr('CompanyErr');">
                                  <option value="">Select Company</option>
                                  @foreach($CompanyList as $cl)
                                  <option value="{{$cl->CompanyID}}"
                                  <?php if($cl->CompanyID==$BuyerDetails->CompanyID){echo "selected";}?>>{{$cl->CompanyName}}</option>
                                  @endforeach                                 
                                </select>
                                <span id="CompanyErr"></span>
                            </div>
                            <div class="col-md-3 form-group tadmPadd">
                              <div class="form-check form-check-flat form-check-primary adComp adComp-primary disPlay tabdisPlay mt-4">
                                <label class="form-check-label adComp-label">
                                <input type="checkbox" class="form-check-input" id="ShowInactive"
                                onclick="GetCompanyUsingShwowInactiveAndTest();">
                                  Show Inactive
                                <i class="input-helper"></i></label>
                              </div>
                              <div class="form-check form-check-flat form-check-primary adComp adComp-primary disPlay tabdisPlay mt-4">
                                <label class="form-check-label adComp-label">
                                <input type="checkbox" class="form-check-input" id="ShowTest"
                                onclick="GetCompanyUsingShwowInactiveAndTest();">
                                  Show Test
                                <i class="input-helper"></i></label>
                              </div>
                            </div>
                           </div>
                          <div class="row">
                            <div class="col-md-2 form-group tadmPadd labFontt">
                              <label for="exampleInputEmail1">Admin Label:</label>
                                <input type="text" class="form-control txtPadd" 
                                value="{{$BuyerDetails->AdminLabel}}" id="AdminLabel" name="AdminLabel"
                                onkeypress="HideErr('AdminLabelErr');">
                                 <span id="AdminLabelErr"></span>
                            </div>
                            <div class="col-md-1 form-group tadmPadd btnCen">
                              <button type="button" id="CopyAdminLable" class="btn btn-success noBoradi mr-2">&gt;&gt;</button>
                            </div>
                            <div class="col-md-2 form-group tadmPadd labFontt">
                              <label for="exampleInputEmail1">Partner Label:</label>
                                <input type="text" class="form-control txtPadd" 
                                  value="{{$BuyerDetails->PartnerLabel}}" id="PartnerLabel" name="PartnerLabel"
                                  onkeypress="HideErr('PartnerLabelErr');">
                                   <span id="PartnerLabelErr"></span>
                            </div>
                            <div class="col-md-2 form-group tadmPadd labFontt">
                              <label for="exampleInputEmail1">Integration File Name:</label>
                                <input type="text"value="{{$BuyerDetails->IntegrationFileName}}" 
                                  id="IntegrationFileName" name="IntegrationFileName" class="form-control txtPadd" placeholder="Integration File Name"
                                  onkeypress="HideErr('IntegrationFileNameErr');">
                                <span id="IntegrationFileNameErr"></span>
                            </div>
                            <div class="col-md-2 form-group mb-0 tadmPadd labFontt">
                              <label for="PayoutCalculation">Payout Calculation:</label>
                                <select class="form-control fonT" id="PayoutCalculation" name="PayoutCalculation">
                                  <option value="RevShare" <?php if($BuyerDetails->PayoutCalculation=="RevShare"){ echo "selected"; } ?>>RevShare</option>
                                  <option value="FixedPrice" <?php if($BuyerDetails->PayoutCalculation=="FixedPrice"){ echo "selected"; } ?>>Fixed Price</option>
                                </select>
                            </div>
                            <?php 
                            $PayoutCalculation = $BuyerDetails->PayoutCalculation;
                            if($PayoutCalculation=='FixedPrice')
                            { 
                                 $PayoutCalculationFixedPrice="style='display:block'";
                                 $PayoutCalculationRevShare="style='display:none'";
                            } 
                            else if($PayoutCalculation=='RevShare')
                            {
                                 $PayoutCalculationRevShare="style='display:block'";
                                 $PayoutCalculationFixedPrice="style='display:none'";
                            }
                            ?>
                            <div class="col-md-2 form-group tadmPadd labFontt" <?php echo $PayoutCalculationRevShare ?>  
                                id="RevsharePercentInput">
                              <label for="exampleInputEmail1">RevShare: </label>
                              <input type="text" class="form-control txtPadd" value="{{$BuyerDetails->FixedPrice}}" 
                              placeholder="70" id="Revshare" name="Revshare" onkeypress="HideErr('RevshareErr');">
                              <span id="RevshareErr"></span>
                            </div>
                            <div class="col-md-2 form-group tadmPadd labFontt" <?php echo $PayoutCalculationFixedPrice ?> 
                                id="FixedPriceInput">
                              <label for="exampleInputEmail1">Fixed Price:</label>
                              <input type="text" class="form-control txtPadd" value="{{$BuyerDetails->FixedPrice}}" 
                              placeholder="70" id="FixedPrice" name="FixedPrice" onkeypress="HideErr('FixedPriceErr');">
                              <span id="FixedPriceErr"></span>
                            </div>
                          </div>                         
                          <div class="row">
                            <?php 
                            if($CampaignDetails->CampaignType=="DirectPost"){                           
                              ?>
                              <div class="col-md-2 form-group tadmPadd labFontt">
                                <label for="exampleInputEmail1">Buyer Tier ID:</label>
                                  <input type="text" onkeypress="HideErr('BuyerTierIDErr');" value="{{$BuyerDetails->BuyerTierID}}" 
                                    id="BuyerTierID" name="BuyerTierID" class="form-control txtPadd" placeholder="Buyer Tier ID">
                                  <span id="BuyerTierIDErr"></span>
                              </div>
                              <?php
                            }
                            ?>
                            
                            <div class="col-md-1 form-group tadmPadd labFontt">
                              <label for="exampleInputEmail1">Daily Cap:</label>
                                <input type="text" value="{{$BuyerDetails->DailyCap}}" 
                                  id="DailyCap" name="DailyCap" class="form-control txtPadd" placeholder="Daily Cap"
                                  onkeypress="HideErr('DailyCapErr');">
                                <span id="DailyCapErr"></span>
                            </div>
                            <div class="col-md-1 form-group tadmPadd labFontt">
                              <label for="exampleInputEmail1">Total Cap:</label>
                                <input type="text" value="{{$BuyerDetails->TotalCap}}" 
                                  id="TotalCap" name="TotalCap" class="form-control txtPadd" placeholder="Total Cap"
                                  onkeypress="HideErr('TotalCapErr');">
                                <span id="TotalCapErr"></span>
                            </div>
                            <?php if($BuyerDetails->BuyerStatus=='1'){ ?>
                            <div class="col-md-2 form-group tadmPadd labFontt">
                              <label for="exampleInputEmail1">Buyer Status:</label>
                                <div class="onoffswitch8">
                                  <input type="checkbox" name="BuyerStatus" class="onoffswitch8-checkbox" 
                                  id="BuyerStatus" checked="">
                                  <label class="onoffswitch8-label" for="BuyerStatus">
                                    <span class="onoffswitch8-inner"></span>
                                    <span class="onoffswitch8-switch"></span>
                                  </label>
                                </div>
                            </div>
                            <?php } else {?>
                            <div class="col-md-2 form-group tadmPadd labFontt">
                              <label for="exampleInputEmail1">Buyer Status:</label>
                                <div class="onoffswitch8">
                                  <input type="checkbox" name="BuyerStatus" class="onoffswitch8-checkbox" 
                                  id="BuyerStatus">
                                  <label class="onoffswitch8-label" for="BuyerStatus">
                                    <span class="onoffswitch8-inner"></span>
                                    <span class="onoffswitch8-switch"></span>
                                  </label>
                                </div>
                            </div>
                            <?php }?>
                            <?php if($BuyerDetails->BuyerTestMode=='1'){ ?>
                            <div class="col-md-2 form-group tadmPadd labFontt">
                              <label for="exampleInputEmail1">Buyer Mode?</label>
                                <div class="onoffswitch9">
                                  <input type="checkbox" name="BuyerTestMode" class="onoffswitch9-checkbox"
                                     id="BuyerTestMode" checked="">
                                  <label class="onoffswitch9-label" for="BuyerTestMode">
                                    <span class="onoffswitch9-inner"></span>
                                    <span class="onoffswitch9-switch"></span>
                                  </label>
                                </div>
                            </div>
                            <?php } else {?>
                            <div class="col-md-2 form-group tadmPadd labFontt">
                              <label for="exampleInputEmail1">Buyer Mode?</label>
                                <div class="onoffswitch9">
                                  <input type="checkbox" name="BuyerTestMode" class="onoffswitch9-checkbox" 
                                    id="BuyerTestMode" >
                                  <label class="onoffswitch9-label" for="BuyerTestMode">
                                    <span class="onoffswitch9-inner"></span>
                                    <span class="onoffswitch9-switch"></span>
                                  </label>
                                </div>
                            </div>
                            <?php }?>
                            <?php 
                            if($CampaignDetails->CampaignType=="DirectPost"){                           
                              $MaxTimeOutInput = "style='display:block;'";
                              $MinTimeOutInput = "style='display:block;'";
                              $MaxPingTimeOutInput = "style='display:none;'";
                              $MaxPostTimeOutInput = "style='display:none;'";
                              $MaxPingsPerMinuteInput = "style='display:none;'";
                            }else if($CampaignDetails->CampaignType=="PingPost"){
                              $MaxTimeOutInput = "style='display:none;'";
                              $MinTimeOutInput = "style='display:none;'";
                              $MaxPingTimeOutInput = "style='display:block;'";
                              $MaxPostTimeOutInput = "style='display:block;'";
                              $MaxPingsPerMinuteInput = "style='display:block;'";
                            }
                            ?>
                            <div class="col-md-2 form-group tadmPadd labFontt" id="MaxTimeOutInput" <?php echo $MaxTimeOutInput; ?>>
                              <label for="exampleInputEmail1">Max Time Out:</label>
                                <input type="text" class="form-control txtPadd" placeholder="0" 
                                id="MaxTimeOut" name="MaxTimeOut" value="{{$BuyerDetails->MaxTimeOut}}" onkeypress="HideErr('MaxTimeOutErr');">
                                <span id="MaxTimeOutErr"></span>
                            </div>
                            <div class="col-md-2 form-group tadmPadd labFontt" id="MinTimeOutInput" <?php echo $MinTimeOutInput; ?>>
                              <label for="exampleInputEmail1">Min Time Out:</label>
                                <input type="text" class="form-control txtPadd" placeholder="0" 
                                id="MinTimeOut" name="MinTimeOut" value="{{$BuyerDetails->MinTimeOut}}" onkeypress="HideErr('MinTimeOutErr');">
                                <span id="MinTimeOutErr"></span>
                            </div>

                            <div class="col-md-2 form-group tadmPadd labFontt" id="MaxPingTimeOutInput" <?php echo $MaxPingTimeOutInput; ?>>
                              <label for="exampleInputEmail1">Max Ping Time Out:</label>
                                <input type="text" class="form-control txtPadd" placeholder="0" 
                                id="MaxPingTimeOut" name="MaxPingTimeOut" value="{{$BuyerDetails->MaxPingTimeOut}}" onkeypress="HideErr('MaxPingTimeOutErr');">
                                <span id="MaxPingTimeOutErr"></span>
                            </div>
                            <div class="col-md-2 form-group tadmPadd labFontt" id="MaxPostTimeOutInput" <?php echo $MaxPostTimeOutInput; ?>>
                              <label for="exampleInputEmail1">Max Post Time Out:</label>
                                <input type="text" class="form-control txtPadd" placeholder="0" 
                                id="MaxPostTimeOut" name="MaxPostTimeOut" value="{{$BuyerDetails->MaxPostTimeOut}}" onkeypress="HideErr('MaxPostTimeOutErr');">
                                <span id="MaxPostTimeOutErr"></span>
                            </div>
                            <div class="col-md-2 form-group tadmPadd labFontt" id="MaxPingsPerMinuteInput" <?php echo $MaxPingsPerMinuteInput; ?>>
                              <label for="exampleInputEmail1">Max Pings Per Minute:</label>
                                <input type="text" class="form-control txtPadd" placeholder="0" 
                                id="MaxPingsPerMinute" name="MaxPingsPerMinute" value="{{$BuyerDetails->MaxPingsPerMinute}}" onkeypress="HideErr('MaxPingsPerMinuteErr');">
                                <span id="MaxPingsPerMinuteErr"></span>
                            </div>
                          </div>                             
                          <div class="row">
                            <?php 
                            if($CampaignDetails->CampaignType=="PingPost")
                            {                           
                              ?>
                              <div class="col-md-6 form-group tadmPadd labFontt">
                                <label for="exampleInputEmail1">Ping Test URL:</label>
                                  <input type="text" class="form-control txtPadd" 
                                  value="{{$BuyerDetails->PingTestURL}}" id="PingTestURL" name="PingTestURL"
                                  onkeypress="HideErr('PingTestURLErr');">
                                   <span id="PingTestURLErr"></span>
                              </div>
                              <div class="col-md-6 form-group tadmPadd labFontt">
                                <label for="exampleInputEmail1">Ping Live URL:</label>
                                  <input type="text" class="form-control txtPadd" 
                                    value="{{$BuyerDetails->PingLiveURL}}" id="PingLiveURL" name="PingLiveURL"
                                    onkeypress="HideErr('PingLiveURLErr');">
                                     <span id="PingLiveURLErr"></span>
                              </div>
                              <div class="col-md-6 form-group tadmPadd labFontt">
                                <label for="exampleInputEmail1">Post Test URL:</label>
                                  <input type="text" class="form-control txtPadd" 
                                  value="{{$BuyerDetails->PostTestURL}}" id="PostTestURL" name="PostTestURL"
                                  onkeypress="HideErr('PostTestURLErr');">
                                   <span id="PostTestURLErr"></span>
                              </div>
                              <div class="col-md-6 form-group tadmPadd labFontt">
                                <label for="exampleInputEmail1">Post Live URL:</label>
                                  <input type="text" class="form-control txtPadd" 
                                    value="{{$BuyerDetails->PostLiveURL}}" id="PostLiveURL" name="PostLiveURL"
                                    onkeypress="HideErr('PostLiveURLErr');">
                                     <span id="PostLiveURLErr"></span>
                              </div>
                              <?php  
                            }
                            else if($CampaignDetails->CampaignType=="DirectPost")
                            {
                              ?>
                              <div class="col-md-6 form-group tadmPadd labFontt">
                                <label for="exampleInputEmail1">DirectPost Test URL:</label>
                                  <input type="text" class="form-control txtPadd" 
                                  value="{{$BuyerDetails->PostTestURL}}" id="DirectPostTestURL" name="DirectPostTestURL"
                                  onkeypress="HideErr('DirectPostTestURLErr');">
                                   <span id="DirectPostTestURLErr"></span>
                              </div>
                              <div class="col-md-6 form-group tadmPadd labFontt">
                                <label for="exampleInputEmail1">DirectPost Live URL:</label>
                                  <input type="text" class="form-control txtPadd" 
                                    value="{{$BuyerDetails->PostLiveURL}}" id="DirectPostLiveURL" name="DirectPostLiveURL"
                                    onkeypress="HideErr('DirectPostLiveURLErr');">
                                     <span id="DirectPostLiveURLErr"></span>
                              </div>
                              <?php
                            }
                            ?>
                          </div>
                          <div class="row">
                            <div class="col-md-2 form-group mb-0 tadmPadd labFontt">
                                <label for="exampleFormControlSelect1">Request Method:</label>
                                <select class="form-control fonT" id="RequestMethod" name="RequestMethod" >
                                    <option value="1" <?php if($BuyerDetails->RequestMethod=='1'){echo "selected";}?>>POST</option>
                                    <option value="2" <?php if($BuyerDetails->RequestMethod=='2'){echo "selected";}?>>GET</option>
                                    <option value="3" <?php if($BuyerDetails->RequestMethod=='3'){echo "selected";}?>>XmlinPost</option>
                                    <option value="4" <?php if($BuyerDetails->RequestMethod=='4'){echo "selected";}?>>XmlPost</option>
                                    <option value="5" <?php if($BuyerDetails->RequestMethod=='5'){echo "selected";}?>>InlineXML</option>
                                </select>
                                <span id="RequestMethodErr"></span>
                            </div>
                            <div class="col-md-2 form-group mb-0 tadmPadd labFontt">
                                <label for="exampleFormControlSelect1">Request Header:</label>
                                <select class="form-control fonT" id="RequestHeader" name="RequestHeader" >
                                    <option value="1" <?php if($BuyerDetails->RequestHeader=='1'){echo "selected";}?>>Content-Type: application/x-www-form-urlencoded</option>
                                    <option value="2" <?php if($BuyerDetails->RequestHeader=='2'){echo "selected";}?>>Content-Type:text/xml; charset=utf-8 </option>
                                    <option value="3" <?php if($BuyerDetails->RequestHeader=='3'){echo "selected";}?>>Content-type: application/xml </option>
                                    <option value="4" <?php if($BuyerDetails->RequestHeader=='4'){echo "selected";}?>>Content-type: application/json</option>
                                </select>
                                <span id="RequestHeaderErr"></span>
                            </div>
                            <div class="col-md-2 form-group tadmPadd labFontt">
                              <label for="exampleInputEmail1">Parameter1:</label>
                                <input type="text" class="form-control txtPadd" 
                                value="{{$BuyerDetails->Parameter1}}" id="Parameter1" name="Parameter1"
                                onkeypress="HideErr('Parameter1Err');">
                                 <span id="Parameter1Err"></span>
                            </div>
                            <div class="col-md-2 form-group tadmPadd labFontt">
                              <label for="exampleInputEmail1">Parameter2:</label>
                                <input type="text" class="form-control txtPadd" 
                                value="{{$BuyerDetails->Parameter2}}" id="Parameter2" name="Parameter2"
                                onkeypress="HideErr('Parameter2Err');">
                                 <span id="Parameter2Err"></span>
                            </div>
                            <div class="col-md-2 form-group tadmPadd labFontt">
                              <label for="exampleInputEmail1">Parameter3:</label>
                                <input type="text" class="form-control txtPadd" 
                                value="{{$BuyerDetails->Parameter3}}" id="Parameter3" name="Parameter3"
                                onkeypress="HideErr('Parameter3Err');">
                                 <span id="Parameter3Err"></span>
                            </div>
                            <div class="col-md-2 form-group tadmPadd labFontt">
                              <label for="exampleInputEmail1">Parameter4:</label>
                                <input type="text" class="form-control txtPadd" 
                                value="{{$BuyerDetails->Parameter4}}" id="Parameter4" name="Parameter4"
                                onkeypress="HideErr('Parameter4Err');">
                                 <span id="Parameter4Err"></span>
                            </div>
                          </div>
                           <div class="row">
                            <div class="col-md-4 form-group labFontt tablabFontt">
                              <label>File: (You Can Select Multiple Files)</label>
                              <div class="custom-file">
                                <input type="file" multiple class="custom-file-input" id="files" name="files[]" multiple>
                                <label class="custom-file-label fileLabel" for="files">No File Chosen</label>
                              </div>
                            </div>
                            <?php
                            if($BuyerDetails->UploadedFiles!='')
                            {
                              ?>
                              <div class="col-md-4 form-group mb-0 labFontt">
                                <span id="UploadFileMessage"></span>
                              <label for="exampleFormControlTextarea1">Uploaded Files:</label>
                                <table class="table">
                                  <thead>
                                    <tr>
                                      <th>Uploaded File</th>
                                      <th>Action</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                  <?php
                                  $UploadedFiles = json_decode($BuyerDetails->UploadedFiles);
                                  foreach($UploadedFiles as $upf)
                                  {
                                    ?>
                                    <tr id="UploadedFileTR_<?php echo $upf->id; ?>">
                                      <td>
                                        <a href="{{ asset('uploads').'/'.$upf->filename }}" 
                                          target="_blank">{{$upf->filename}}</a>
                                      </td>
                                      <td>
                                        <a class="btn btn-danger btn-sm" href="javascript:void(0);" 
                                            onclick="DeleteBuyerUploadedFile('<?php echo $BuyerDetails->BuyerID; ?>','<?php echo $upf->id; ?>');">
                                            Delete
                                        </a>
                                        <input type="hidden" name="OldUploadedFiles[]" value="<?php echo $upf->filename; ?>">
                                      </td>
                                    </tr>
                                    <?php
                                  }
                                  ?>
                                  </tbody>
                                </table>
                              </div>
                              <?php
                            } 
                            ?>
                            <div class="col-md-4 form-group mb-0 labFontt">
                              <label for="exampleFormControlTextarea1">Buyer Notes:</label>
                              <textarea class="form-control" id="BuyerNotes" name="BuyerNotes" rows="3">{{$BuyerDetails->BuyerNotes}}</textarea>
                            </div>
                          </div>
                          <div class="row">
                            <button type="button" class="btn btn-success btn-fw noBoradi mr-2" id="BuyerUpdateBtn">Save</button>
                            <a href="{{ route('admin.BuyerManagementView') }}" class="btn btn-danger btn-fw noBoradi">Cancel</a>
                          </div>
                        </form>
                      </div>
                   </div>
                </div>
             </div>
          </div>
       </div>
       @include('layouts.Footer')
    </div>
    </div>
  </div>
  <input type="hidden" name="url" id="url" value='{{URL::to("/")}}'>
  <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
  <input type="hidden" name="loading" id="loading" value='{{ asset("Admin/images/loading_image.gif") }}'>
  @include('layouts.Script')
  <script src="{{asset('Ajax/BuyerManagement.js')}}"></script> 
  <script>
  $(document).ready(function() {
    $('#sandbox-container .input-daterange').datepicker({});

    $('#datepicker').datepicker({
      weekStart: 0,
      daysOfWeekHighlighted: "6,0",
      autoclose: true,
      todayHighlight: true,
      dateFormat: 'mm/dd/yy'
    });
    $('#datepicker').datepicker("setDate", new Date());
  });
  </script>
</body>
