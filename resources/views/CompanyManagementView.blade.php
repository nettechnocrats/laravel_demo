<!DOCTYPE html>
<html lang="en">
@include('layouts.Head')
<body>
  <div class="container-scroller">
    @include('layouts.Header')
    <div class="container-fluid page-body-wrapper">
      @include('layouts.Menu')
      <div class="main-panel">
        <div class="content-wrapper">
         <!--  <div class="page-header row">
            <h3 class="page-title">
              Campaign Management
            </h3>
          </div> -->
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb breadcrumb-custom">
              <li class="breadcrumb-item"><a href="#"><i class="fas fa-users-cog menu-icon"></i> Super Admin Tools</a></li>
              <li class="breadcrumb-item active" aria-current="page"><span><i class="fas fa-users menu-icon"></i> Company Management </span></li>
            </ol>
          </nav>
           <div class="col-md-12">
            @if(Session::has('message'))
              <div class="alert alrtDanger {{ Session::get('alert-class', 'alert-info') }}">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>{{ Session::get('message') }}</strong>
              </div>
            @endif
          </div>
          <div class="col-md-12" id="ResponseMessage">
          </div>
          <div class="allDet">
            <div class="col-lg-12 col-md-12 col-sm-12 col-12 comP">
              <div class="row">
                <div class="col-md-3 form-group tadmPadd labFontt newFoGrp">
                  <input type="text" class="form-control txtPadd boxH" id="CompanyName" placeholder="Company Name">
                </div>
                <div class="col-md-3 form-group tadmPadd labFontt newFoGrp">
                  <input type="text" class="form-control txtPadd boxH" id="CompanyTel" placeholder="Company Telephone">
                </div>
                <div class="col-md-3 form-group tadmPadd labFontt newFoGrp">
                  <input type="text" class="form-control txtPadd boxH" id="MappedUser" placeholder="Mapped User">
                </div>
                <div class="col-md-3 form-group tadmPadd labFontt newFoGrp">
                  <input type="text" class="form-control txtPadd boxH" id="MappedBuyer" placeholder="Mapped Buyer">
                </div>
              </div>
              <div class="row">
                <div class="col-md-3 form-group newFoGrp">
                   <div class="input-daterange input-group" id="datepicker">
                      <input type="text" value="{{$StartDate}}" class="input-sm form-control camH datePadd fonT" name="start" id="StartDate"/>
                      <span class="input-group-addon fonT">To</span>
                      <input type="text" value="{{$EndDate}}" class="input-sm form-control camH datePadd fonT" name="end" id="EndDate"/>
                   </div>
                </div>                
                <div class="col-md-2 newFoGrp">
                  <select class="form-control fonT" id="CompanyStatus">
                    <option value="">All Status</option>
                    <option value="1">Active</option>
                    <option value="0">InActive</option>                    
                  </select>
                </div>
                 <div class="col-md-2 newFoGrp">
                  <select class="form-control fonT" id="CompanyTestMode">
                    <option value="">All Mode</option>
                    <option value="0">Live Mode</option>
                    <option value="1">Test Mode</option>                    
                  </select>
                </div>
                <div class="col-md-3 col-6">
                   <button type="button" class="btn btn-success cbtnPadd sercFontt"
                   id="SearchBtn" onclick="SearchCompanyList();"> <i class="fa fa-search"></i> Search </button>
                </div>
                <div class="col-md-2 col-6">
                  <a href="{{route('admin.CompanyAddView')}}">
                    <button type="button" class="btn btn-success fonT mW nF fl noBoradi cbtnPadd sercFontt">
                      <i class="fas fa-plus fnnPadd"></i>New Company
                    </button>
                  </a>
                </div>  
              </div>
              <div id="dataTable_wrapper" class="dataTables_wrapper dt-bootstrap4">
                <input type="hidden" name="numofrecords" id="numofrecords" value='10'>
              </div>
            </div>
          </div>
       </div>
       @include('layouts.Footer')
    </div>
    </div>
  </div>
  <input type="hidden" name="url" id="url" value='{{URL::to("/")}}'>
  <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
  <input type="hidden" name="loading" id="loading" value='{{ asset("Admin/images/loading.gif") }}'>
  @include('layouts.Script')
  <script src="{{asset('Ajax/CompanyManagementAdd.js')}}"></script> 
  <script>
  $(document).ready(function() {    
      GetCompanyList(1);
  });
  </script>
   <script>
  $(document).ready(function() {
    $('#sandbox-container .input-daterange').datepicker({});

    $('#datepicker').datepicker({
      weekStart: 0,
      daysOfWeekHighlighted: "6,0",
      autoclose: true,
      todayHighlight: true,
      format: 'yyyy-mm-dd'
    });
    $('#datepicker').datepicker("setDate", new Date());
  });
  </script>
</body>
