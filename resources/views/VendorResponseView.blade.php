<!DOCTYPE html>
<html lang="en">
@include('layouts.Head')
<body>
  <div class="container-scroller">
    @include('layouts.Header')
    <div class="container-fluid page-body-wrapper">
      @include('layouts.Menu')
      <div class="main-panel">
        <div class="content-wrapper">
          <!-- <div class="page-header row">
            <h3 class="page-title">
              Buyer Invoice Report
            </h3>
          </div> -->
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb breadcrumb-custom">
              <li class="breadcrumb-item"><a href="#"><i class="fas fa-file-alt menu-icon"></i> Reports</a></li>
              <li class="breadcrumb-item active" aria-current="page"><span><i class="fas fa-file-alt menu-icon"></i> Vendor Response Report</span></li>
            </ol>
          </nav>
          <div class="allDet">
             <div class="col-lg-12 col-md-12 col-sm-12 col-12 comP">
                <form class="boxS">
                    <div class="row">
                      <div class="col-md-3 form-group newFoGrp">
                         <select class="form-control fonT" id="Campaign">
                            @foreach($CampaignDropDown as $cdd)
                            <option value="{{$cdd->CampaignID}}">{{$cdd->CampaignName}}</option>
                            @endforeach
                         </select>
                      </div>
                      <div class="col-md-3 form-group newFoGrp">
                         <div class="input-daterange input-group" id="datepicker">
                            <input type="text" value="{{$StartDate}}" class="input-sm form-control camH datePadd fonT" name="start" id="StartDate"/>
                            <span class="input-group-addon fonT">To</span>
                            <input type="text" value="{{$EndDate}}" class="input-sm form-control camH datePadd fonT" name="end" id="EndDate"/>
                         </div>
                      </div> 
                      <div class="col-md-3 form-group mb-0 newFoGrp">
                         <select class="form-control fonT" id="VendorCompany" onchange="GetVendorUsingVendorCompany();">
                            <option value="All">All Vendor Company</option>
                              
                         </select>
                      </div>
                      <div class="col-md-3 form-group mb-0 newFoGrp">
                         <select class="form-control fonT" id="Vendor" >
                            <option value="All">All Vendor</option>
                             
                         </select>
                      </div>                    
                    </div>
                    <div class="row">                     
                      <div class="col-md-3 form-group mb-0 newFoGrp">
                         <select class="form-control fonT" id="Action" >
                            <option value="DirectPost" class="dp">DirectPost</option>
                            <option value="Ping" class="pp">Ping</option>
                            <option value="Post" class="pp">Post</option>
                            <option value="PingPost" class="pp">PingPost</option>                             
                         </select>
                      </div>
                      <div class="col-md-3 form-group mb-0 newFoGrp">
                         <select class="form-control fonT" id="Response" >
                            <option value="">Select Response</option>                             
                         </select>
                      </div>
                      <div class="col-md-3 form-group mb-0 newFoGrp">
                         <select class="form-control fonT" id="Sort" >
                            <option value="ASC">Ascending</option>
                            <option value="DESC" selected>Descending </option>                             
                         </select>
                      </div>                      
                      <div class="col-md-3 form-group mb-0 mbCenter msP">
                         <button type="button" class="btn btn-success fonT mW nF tW fl noBoradi sercFontt"
                         id="SearchBtn" onclick="SearchVendorResponseList();"><i class="fa fa-search"></i> Search </button>
                      </div> 
                    </div> 
                </form>
                <div class="boxS">                    
                  <div id="dataTable_wrapper" class="dataTables_wrapper dt-bootstrap4">
                    <input type="hidden" name="numofrecords" id="numofrecords" value='10'>                        
                  </div>
                </div>
              </div>
          </div>
       </div>
       @include('layouts.Footer')
    </div>
    </div>
  </div>
  <input type="hidden" name="url" id="url" value='{{URL::to("/")}}'>
  <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
  <input type="hidden" name="loading" id="loading" value='{{ asset("Admin/images/loading.gif") }}'>
  @include('layouts.Script')
  <script src="{{asset('Ajax/VendorResponse.js')}}"></script> 
  <script>
  $(document).ready(function() {
    $('#sandbox-container .input-daterange').datepicker({});

    $('#datepicker').datepicker({
      weekStart: 0,
      daysOfWeekHighlighted: "6,0",
      autoclose: true,
      todayHighlight: true,
      format: 'yyyy-mm-dd'
    });
    $('#datepicker').datepicker("setDate", new Date());
  });
  </script>
</body>
