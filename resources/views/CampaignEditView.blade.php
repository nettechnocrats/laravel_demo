<!DOCTYPE html>
<html lang="en">
@include('layouts.Head')
<body>
  <div class="container-scroller">
    @include('layouts.Header')
    <div class="container-fluid page-body-wrapper">
      @include('layouts.Menu')
      <div class="main-panel">
        <div class="content-wrapper">
          <!-- <div class="page-header row">
            <h3 class="page-title">
              Edit Campaign
            </h3>
          </div> -->
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb breadcrumb-custom">
              <li class="breadcrumb-item"><a href="#"><i class="fas fa-users-cog menu-icon"></i></i> Super Admin Tools</a></li>
              <li class="breadcrumb-item"><a href="{{ route('admin.CampaignManagementView') }}"><i class="fas fa-user-cog iconMarg"></i> Campaign Management </a></li>
              <li class="breadcrumb-item active" aria-current="page"><span><i class="fas fa-user-cog iconMarg"></i> Edit Campaign </span></li>
            </ol>
          </nav>
          <div class="col-md-12">
            @if(Session::has('message'))
              <div class="alert alrtDanger {{ Session::get('alert-class', 'alert-info') }}">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>{{ Session::get('message') }}</strong>
              </div>
            @endif
          </div>
          <div class="allDet">
             <div class="col-md-12 grid-margin stretch-card">
                <div class="card">
                   <div class="card-body">                      
                     <div class="addUser">
                          <div class="row">
                            <div class="page-header-below">
                               <h3 class="page-title-below">
                                  Edit Campaign Details
                               </h3>
                            </div>                            
                          </div>                          
                         <form action="{{route('admin.SaveCampaignDetails')}}" name="CampaignEditForm" id="CampaignEditForm" method="POST">
                            <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="CampaignID" id="CampaignID" value="{{ $CampaignDetails->CampaignID }}">
                            <div class="row">
                               <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                                <div class="row">
                                    <div class="col-md-6 col-sm-6 form-group tadmPadd labFontt">
                                      <label for="exampleFormControlSelect1">Campaign Name:</label>
                                      <input type="text" class="form-control txtPadd" value="{{ $CampaignDetails->CampaignName }}" onkeypress="HideErr('CampaignNameErr');" id="CampaignName" name="CampaignName" placeholder="Campaign Name">
                                      <span id="CampaignNameErr"></span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 form-group labFontt">
                                      <label for="exampleFormControlSelect1">Campaign Type:</label>
                                      <select class="form-control fonT" onchange="HideErr('CampaignTypeErr');" id="CampaignType"  name="CampaignType">
                                        <option value="">Select Type</option>
                                        @foreach($CampaignType as $ct)
                                        <option value="{{$ct->CampaignType}}"
                                          <?php if($CampaignDetails->CampaignType==$ct->CampaignType){ echo "selected"; } ?>>{{$ct->CampaignType}}</option>
                                        @endforeach
                                      </select>
                                      <span id="CampaignTypeErr"></span>
                                    </div>
                                </div>                                
                                <div class="row">
                                  <?php 
                                  $MaintenanceMode = 'checked';
                                  if($CampaignDetails->MaintenanceMode=='0')
                                  {
                                    $MaintenanceMode = '';
                                  }
                                  ?>
                                  <div class="col-md-6 form-group tadmPadd labFontt">
                                    <label for="exampleInputEmail1">Maintenance Mode?</label>
                                    <div class="onoffswitch35">
                                      <input type="checkbox" class="onoffswitch35-checkbox" id="MaintenanceMode"
                                        name="MaintenanceMode" <?php echo $MaintenanceMode; ?>>
                                      <label class="onoffswitch35-label" for="MaintenanceMode">
                                      <span class="onoffswitch35-inner"></span>
                                      <span class="onoffswitch35-switch"></span>
                                      </label>
                                    </div>
                                  </div>
                                  <?php 
                                  $ShowInDashboard = 'checked';
                                  if($CampaignDetails->ShowInDashboard=='0')
                                  {
                                    $ShowInDashboard = '';
                                  }
                                  ?>
                                  <div class="col-md-6 form-group tadmPadd labFontt">
                                    <label for="exampleInputEmail1">Show In Dashboard?</label>
                                    <div class="onoffswitch36">
                                      <input type="checkbox"  class="onoffswitch36-checkbox" id="ShowInDashboard" 
                                        name="ShowInDashboard"  <?php echo $ShowInDashboard; ?>>
                                      <label class="onoffswitch36-label" for="ShowInDashboard">
                                      <span class="onoffswitch36-inner"></span>
                                      <span class="onoffswitch36-switch"></span>
                                      </label>
                                    </div>
                                  </div>
                                </div>
                                <div class="row">
                                  <?php 
                                  $HasTiers = 'checked';
                                  if($CampaignDetails->HasTiers=='0')
                                  {
                                    $HasTiers = '';
                                  }
                                  ?>
                                  <div class="col-md-6 form-group tadmPadd labFontt">
                                    <label for="exampleInputEmail1">Has Tiers?</label>
                                    <div class="onoffswitch37">
                                      <input type="checkbox"  class="onoffswitch37-checkbox" id="HasTiers" 
                                        name="HasTiers" <?php echo $HasTiers; ?>>
                                      <label class="onoffswitch37-label" for="HasTiers">
                                      <span class="onoffswitch37-inner"></span>
                                      <span class="onoffswitch37-switch"></span>
                                      </label>
                                    </div>
                                  </div>
                                  <?php 
                                  $HasSpecs = 'checked';
                                  if($CampaignDetails->HasSpecs=='0')
                                  {
                                    $HasSpecs = '';
                                  }
                                  ?>
                                  <div class="col-md-6 form-group tadmPadd labFontt">
                                    <label for="exampleInputEmail1">Make Specs Visible to Vendors?</label>
                                    <div class="onoffswitch38">
                                      <input type="checkbox"  class="onoffswitch38-checkbox" id="HasSpecs" 
                                        name="HasSpecs" <?php echo $HasSpecs; ?>>
                                      <label class="onoffswitch38-label" for="HasSpecs">
                                      <span class="onoffswitch38-inner"></span>
                                      <span class="onoffswitch38-switch"></span>
                                      </label>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                                <div class="row">
                                  <div class="col-md-12 form-group mb-0 labFontt">
                                    <label for="exampleFormControlTextarea1">Specs Content:</label>
                                    <textarea class="form-control specH" id="Specs" name="Specs" rows="3">{{ $CampaignSpecs->Specs }}</textarea>
                                  </div>
                                </div>
                              </div>                               
                            </div>
                            <div class="row">
                               <button type="button" id="CampaignEditBtn" class="btn btn-success btn-fw noBoradi mr-2">Save</button>
                               <a href="{{ route('admin.CampaignManagementView') }}" class="btn btn-danger btn-fw noBoradi">Cancel</a>
                            </div>
                         </form>
                      </div>
                   </div>
                </div>
             </div>
          </div>
       </div>
       @include('layouts.Footer')
    </div>
    </div>
  </div>
  <input type="hidden" name="url" id="url" value='{{URL::to("/")}}'>
  <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
  <input type="hidden" name="loading" id="loading" value='{{ asset("Admin/images/loading_image.gif") }}'>
  @include('layouts.Script')
  <script src="{{asset('Ajax/CampaignManagement.js')}}"></script> 
</body>
