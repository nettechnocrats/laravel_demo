<!DOCTYPE html>
<html lang="en">
@include('layouts.Head')
<body>
  <div class="container-scroller">
    @include('layouts.Header')
    <div class="container-fluid page-body-wrapper">
      @include('layouts.Menu')
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="page-header row">
            <h3 class="page-title">
              Buyer Management
            </h3>
          </div>
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb breadcrumb-custom">
              <li class="breadcrumb-item"><a href="#"><i class="fas fa-users-cog menu-icon"></i> Admin Tools</a></li>
              <li class="breadcrumb-item active" aria-current="page"><span><i class="fas fa-user-plus menu-icon"></i> Buyer Management </span></li>
            </ol>
          </nav>
          <div class="col-md-12" id="BuyerMessage">
            @if(Session::has('message'))
              <div class="alert alrtDanger {{ Session::get('alert-class', 'alert-info') }}">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>{{ Session::get('message') }}</strong>
              </div>
            @endif
          </div>
          <div class="allDet">
            <div class="col-lg-12 col-md-12 col-sm-12 col-12 comP">
              <div class="row">
                <div class="col-md-3 styBord">
                  <div class="col">
                    <select class="form-control fonT boxH" id="Company">
                      <option value="All">All Company</option>
                     
                    </select>
                  </div>
                  <div class="col-md-12 form-group tadmPadd mb-0">
                    <div class="form-check form-check-flat form-check-primary adComp adComp-primary disPlayt">
                      <label class="form-check-label adComp-label sa">
                      <input type="checkbox" class="form-check-input" id="ShowInactive" 
                        onclick="GetCompanyUsingShwowInactiveAndTest();">
                        Show Inactive
                      </label>
                    </div>
                    <div class="form-check form-check-flat form-check-primary adComp adComp-primary disPlayt">
                      <label class="form-check-label adComp-label si">
                      <input type="checkbox" class="form-check-input" id="ShowTest"
                        onclick="GetCompanyUsingShwowInactiveAndTest();">
                        Show Test
                      </label>
                    </div>
                  </div>
                </div>                
                <div class="col-md-2 form-group newFoGrp">
                  <select class="form-control fonT" id="Campaign">
                      @foreach($CampaignDropDown as $cdd)
                      <option value="{{$cdd->CampaignID}}">{{$cdd->CampaignName}}</option>
                      @endforeach
                  </select>
                </div>
                <div class="col-md-2 form-group tadmPadd labFontt newFoGrp">
                  <input type="text" class="form-control txtPadd boxH" id="BuyerName" placeholder="Buyer Name">
                </div>
                <div class="col-md-2 form-group newFoGrp">
                  <select class="form-control fonT" id="Status">
                     <option value="">All Status</option>
                     <option value="1">Active</option>
                     <option value="0">Inactive</option>
                  </select>
                </div>
                <div class="col-md-2 form-group newFoGrp">
                  <select class="form-control fonT" id="BuyerMode">
                      <option value="">All Mode</option>
                     <option value="0">Live Mode</option>
                     <option value="1">Test Mode</option>
                  </select>
                </div>
                <div class="col-md-1 form-group tadmPadd labFontt newFoGrp">
                   <button type="button" class="btn btn-success fonT mW nF tW fl noBoradi sercFontt"
                   id="SearchBtn" onclick="SearchVendorList();"><i class="fa fa-search"></i> Search </button>
                </div> 
              </div>
              <div class="row">
                <div class="col-md-3 offset-md-9 form-group tadmPadd labFontt newFoGrp">
                  <button type="button" class="btn btn-success btn-fw cbtnPadd sercFontt fl dwSCSV Flo"
                      onclick="AddBuyerModal();">Add New Buyer</button>
                </div>
              </div>
              <div id="dataTable_wrapper" class="dataTables_wrapper dt-bootstrap4">
                <input type="hidden" name="numofrecords" id="numofrecords" value='10'>
              </div>
            </div>
          </div>
       </div>
       @include('layouts.Footer')
    </div>
    </div>
  </div>
  <!-- Add Window -->
  <div class="modal fade" id="AddBuyerModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header mHBg">
          <h4 class="modal-title">Add New Buyer</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
            
        </div>
        <div class="modal-footer">
          <form action="{{route('admin.AddBuyerDetails')}}" name="BuyerAddForm" 
          id="BuyerAddForm" method="POST" enctype="multipart/form-data">                          
          <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
          <input type="hidden" name="CampaignType" id="CampaignTypeAdd" value="">
          <div class="row"> 
            <div class="col-md-4 form-group mb-0 tadmPadd labFontt">
              <label for="exampleFormControlSelect1">Company:</label>
                <select class="form-control fonT" id="CompanyAdd" name="Company"
                  onchange="HideErr('CompanyAddErr');">
                  <option value="">Select Company</option>
                  @foreach($CompanyList as $cl)
                  <option value="{{$cl->CompanyID}}">{{$cl->CompanyName}}</option>
                  @endforeach                                 
                </select>
                <span id="CompanyAddErr"></span>
                <div class="form-check form-check-flat form-check-primary adComp adComp-primary disPlay tabdisPlay tMrg">
                  <label class="form-check-label adComp-label">
                  <input type="checkbox" class="form-check-input" id="ShowInactiveAdd"
                  onclick="GetCompanyUsingShwowInactiveAndTest2();">
                    Show Inactive
                  <i class="input-helper"></i></label>
                </div>
                <div class="form-check form-check-flat form-check-primary adComp adComp-primary disPlay tabdisPlay tMrg">
                  <label class="form-check-label adComp-label">
                  <input type="checkbox" class="form-check-input" id="ShowTestAdd"
                  onclick="GetCompanyUsingShwowInactiveAndTest2();">
                    Show Test
                  <i class="input-helper"></i></label>
                </div>
            </div>
            <div class="col-md-3 form-group mb-0 tadmPadd labFontt">
                <label for="exampleFormControlSelect1">Campaign:</label>
                <select class="form-control fonT" id="CampaignAdd" name="Campaign"  
                    onchange="GetCampaignDetailsAdd(),HideErr('CompanyAddErr');">
                    @foreach($CampaignDropDown as $cdd)
                    <option value="{{$cdd->CampaignID}}">{{$cdd->CampaignName}}</option>
                    @endforeach
                </select>
                <span id="CampaignAddErr"></span>
            </div>
            <div class="col-md-2 form-group tadmPadd labFontt">
              <label for="exampleInputEmail1">Admin Label:</label>
                <input type="text" class="form-control txtPadd" 
                value="" id="AdminLabelAdd" name="AdminLabel"
                onkeypress="HideErr('AdminLabelAddErr');">
                 <span id="AdminLabelAddErr"></span>
            </div>
            <div class="col-md-1 form-group tadmPadd btnCen">
              <button type="button" id="CopyAdminLable" onclick="CopyAdminLableAdd();" class="btn btn-success noBoradi mr-2">&gt;&gt;</button>
            </div>
            <div class="col-md-2 form-group tadmPadd labFontt">
              <label for="exampleInputEmail1">Partner Label:</label>
                <input type="text" class="form-control txtPadd" 
                  value="" id="PartnerLabelAdd" name="PartnerLabel"
                  onkeypress="HideErr('PartnerLabelAddErr');">
                   <span id="PartnerLabelAddErr"></span>
            </div>
          </div>
          <div class="row">  
            <div class="col-md-2 form-group tadmPadd labFontt" id="BuyerTierIDInputAdd" style='display:block;'>
              <label for="exampleInputEmail1">Buyer Tier ID:</label>
                <input type="text" onkeypress="HideErr('BuyerTierIDAddErr');" value="" 
                  id="BuyerTierID" name="BuyerTierIDAdd" class="form-control txtPadd" placeholder="Buyer Tier ID">
                <span id="BuyerTierIDAddErr"></span>
            </div>          
            <div class="col-md-2 form-group tadmPadd labFontt">
              <label for="exampleInputEmail1">Integration File Name:</label>
                <input type="text"value="" 
                  id="IntegrationFileNameAdd" name="IntegrationFileName" class="form-control txtPadd" placeholder="Integration File Name"
                  onkeypress="HideErr('IntegrationFileNameAddErr');">
                <span id="IntegrationFileNameAddErr"></span>
            </div>
            <div class="col-md-2 form-group mb-0 tadmPadd labFontt">
              <label for="PayoutCalculation">Payout Calculation:</label>
                <select class="form-control fonT" id="PayoutCalculationAdd" name="PayoutCalculation" 
                  onchange="PayoutCalculationAddFunction();">
                  <option value="RevShare" >RevShare</option>
                  <option value="FixedPrice" >Fixed Price</option>
                </select>
            </div>
            <?php 
            $PayoutCalculationRevShare="style='display:block'";
            $PayoutCalculationFixedPrice="style='display:none'";
            ?>
            <div class="col-md-2 form-group tadmPadd labFontt" id="RevshareInputAdd" <?php echo $PayoutCalculationRevShare ?>>
              <label for="exampleInputEmail1">RevShare: </label>
              <input type="text" class="form-control txtPadd" value="70.00" 
              placeholder="70" id="RevshareAdd" name="Revshare" onkeypress="HideErr('RevshareAddErr');">
              <span id="RevshareAddErr"></span>
            </div>
            <div class="col-md-2 form-group tadmPadd labFontt" id="FixedPriceInputAdd" <?php echo $PayoutCalculationFixedPrice ?> 
                id="FixedPriceInput">
              <label for="exampleInputEmail1">Fixed Price:</label>
              <input type="text" class="form-control txtPadd" value="70.00" 
              placeholder="70" id="FixedPriceAdd" name="FixedPrice" onkeypress="HideErr('FixedPriceAddErr');">
              <span id="FixedPriceAddErr"></span>
            </div>
            <div class="col-md-2 form-group tadmPadd labFontt">
              <label for="exampleInputEmail1">Daily Cap:</label>
                <input type="text" value="5" 
                  id="DailyCapAdd" name="DailyCap" class="form-control txtPadd" placeholder="Daily Cap"
                  onkeypress="HideErr('DailyCapAddErr');">
                <span id="DailyCapAddErr"></span>
            </div>
            <div class="col-md-2 form-group tadmPadd labFontt">
              <label for="exampleInputEmail1">Total Cap:</label>
                <input type="text" value="5" 
                  id="TotalCapAdd" name="TotalCap" class="form-control txtPadd" placeholder="Total Cap"
                  onkeypress="HideErr('TotalCapAddErr');">
                <span id="TotalCapAddErr"></span>
            </div>
          </div>                         
          <div class="row">                        
            <div class="col-md-2 form-group tadmPadd labFontt">
              <label for="exampleInputEmail1">Buyer Status:</label>
                <div class="onoffswitch8">
                  <input type="checkbox" name="BuyerStatus" class="onoffswitch8-checkbox" 
                  id="BuyerStatusAdd" checked="">
                  <label class="onoffswitch8-label" for="BuyerStatusAdd">
                    <span class="onoffswitch8-inner"></span>
                    <span class="onoffswitch8-switch"></span>
                  </label>
                </div>
            </div>
            <div class="col-md-2 form-group tadmPadd labFontt">
              <label for="exampleInputEmail1">Buyer Mode?</label>
                <div class="onoffswitch9">
                  <input type="checkbox" name="BuyerTestMode" class="onoffswitch9-checkbox"
                     id="BuyerTestModeAdd" >
                  <label class="onoffswitch9-label" for="BuyerTestModeAdd">
                    <span class="onoffswitch9-inner"></span>
                    <span class="onoffswitch9-switch"></span>
                  </label>
                </div>
            </div>
            <div class="col-md-2 form-group tadmPadd labFontt">
              <label for="exampleInputEmail1">EWS Status</label>
                <div class="onoffswitch8">
                  <input type="checkbox" name="EWSStatus" class="onoffswitch8-checkbox"
                     id="EWSStatusAdd" >
                  <label class="onoffswitch8-label" for="EWSStatusAdd">
                    <span class="onoffswitch8-inner"></span>
                    <span class="onoffswitch8-switch"></span>
                  </label>
                </div>
            </div>
            <div class="col-md-2 form-group tadmPadd labFontt" id="MaxTimeOutInputAdd" style='display:none;'>
              <label for="exampleInputEmail1">Max Time Out:</label>
                <input type="text" class="form-control txtPadd" placeholder="0" 
                id="MaxTimeOutAdd" name="MaxTimeOut" value="0" onkeypress="HideErr('MaxTimeOutAddErr');">
                <span id="MaxTimeOutAddErr"></span>
            </div>
            <div class="col-md-2 form-group tadmPadd labFontt" id="MinTimeOutInputAdd" style='display:none;'>
              <label for="exampleInputEmail1">Min Time Out:</label>
                <input type="text" class="form-control txtPadd" placeholder="0" 
                id="MinTimeOutAdd" name="MinTimeOut" value="0" onkeypress="HideErr('MinTimeOutAddErr');">
                <span id="MinTimeOutAddErr"></span>
            </div>

            <div class="col-md-2 form-group tadmPadd labFontt" id="MaxPingTimeOutInputAdd" style='display:none;'>
              <label for="exampleInputEmail1">Max Ping Time Out:</label>
                <input type="text" class="form-control txtPadd" placeholder="0" 
                id="MaxPingTimeOutAdd" name="MaxPingTimeOut" value="0" onkeypress="HideErr('MaxPingTimeOutAddErr');">
                <span id="MaxPingTimeOutAddErr"></span>
            </div>
            <div class="col-md-2 form-group tadmPadd labFontt" id="MaxPostTimeOutInputAdd" style='display:none;'>
              <label for="exampleInputEmail1">Max Post Time Out:</label>
                <input type="text" class="form-control txtPadd" placeholder="0" 
                id="MaxPostTimeOutAdd" name="MaxPostTimeOut" value="0" onkeypress="HideErr('MaxPostTimeOutAddErr');">
                <span id="MaxPostTimeOutAddErr"></span>
            </div>
            <div class="col-md-2 form-group tadmPadd labFontt" id="MaxPingsPerMinuteInputAdd" style='display:none;'>
              <label for="exampleInputEmail1">Max Pings Per Minute:</label>
                <input type="text" class="form-control txtPadd" placeholder="0" 
                id="MaxPingsPerMinuteAdd" name="MaxPingsPerMinute" value="0" onkeypress="HideErr('MaxPingsPerMinuteAddErr');">
                <span id="MaxPingsPerMinuteAddErr"></span>
            </div>
          </div>                           
          <div class="row">
              <div class="col-md-6 form-group tadmPadd labFontt" style='display:none;' id="PingTestURLInputAdd">
                <label for="exampleInputEmail1">Ping Test URL:</label>
                  <input type="text" class="form-control txtPadd" 
                  value="" id="PingTestURLAdd" name="PingTestURL"
                  onkeypress="HideErr('PingTestURLAddErr');">
                   <span id="PingTestURLAddErr"></span>
              </div>
              <div class="col-md-6 form-group tadmPadd labFontt" style='display:none;' id="PingLiveURLInputAdd">
                <label for="exampleInputEmail1">Ping Live URL:</label>
                  <input type="text" class="form-control txtPadd" 
                    value="" id="PingLiveURLAdd" name="PingLiveURL"
                    onkeypress="HideErr('PingLiveURLAddErr');">
                     <span id="PingLiveURLAddErr"></span>
              </div>
              <div class="col-md-6 form-group tadmPadd labFontt" style='display:none;' id="PostTestURLInputAdd">
                <label for="exampleInputEmail1">Post Test URL:</label>
                  <input type="text" class="form-control txtPadd" 
                  value="" id="PostTestURLAdd" name="PostTestURL"
                  onkeypress="HideErr('PostTestURLAddErr');">
                   <span id="PostTestURLAddErr"></span>
              </div>
              <div class="col-md-6 form-group tadmPadd labFontt" style='display:none;' id="PostLiveURLInputAdd">
                <label for="exampleInputEmail1">Post Live URL:</label>
                  <input type="text" class="form-control txtPadd" 
                    value="" id="PostLiveURLAdd" name="PostLiveURL"
                    onkeypress="HideErr('PostLiveURLAddErr');">
                     <span id="PostLiveURLAddErr"></span>
              </div>
              <div class="col-md-6 form-group tadmPadd labFontt" style='display:none;' id="DirectPostTestURLInputAdd">
                <label for="exampleInputEmail1">DirectPost Test URL:</label>
                  <input type="text" class="form-control txtPadd" 
                  value="" id="DirectPostTestURLAdd" name="DirectPostTestURL"
                  onkeypress="HideErr('DirectPostTestURLAddErr');">
                   <span id="DirectPostTestURLAddErr"></span>
              </div>
              <div class="col-md-6 form-group tadmPadd labFontt" style='display:none;' id="DirectPostLiveURLInputAdd">
                <label for="exampleInputEmail1">DirectPost Live URL:</label>
                  <input type="text" class="form-control txtPadd" 
                    value="" id="DirectPostLiveURLAdd" name="DirectPostLiveURL"
                    onkeypress="HideErr('DirectPostLiveURLAddErr');">
                     <span id="DirectPostLiveURLAddErr"></span>
              </div>
          </div>
          <div class="row">
            <div class="col-md-2 form-group mb-0 tadmPadd labFontt">
                <label for="exampleFormControlSelect1">Request Method:</label>
                <select class="form-control fonT" id="RequestMethodAdd" name="RequestMethod" >
                    <option value="1" >POST</option>
                    <option value="2" >GET</option>
                    <option value="3" >XmlinPost</option>
                    <option value="4" >XmlPost</option>
                    <option value="5" >InlineXML</option>
                </select>
                <span id="RequestMethodErr"></span>
            </div>
            <div class="col-md-2 form-group mb-0 tadmPadd labFontt">
                <label for="exampleFormControlSelect1">Request Header:</label>
                <select class="form-control fonT" id="RequestHeaderAdd" name="RequestHeader" >
                    <option value="1" >Content-Type: application/x-www-form-urlencoded</option>
                    <option value="2" >Content-Type:text/xml; charset=utf-8 </option>
                    <option value="3" >Content-type: application/xml </option>
                    <option value="4" >Content-type: application/json</option>
                </select>
                <span id="RequestHeaderErr"></span>
            </div>
            <div class="col-md-2 form-group tadmPadd labFontt">
              <label for="exampleInputEmail1">Parameter1:</label>
                <input type="text" class="form-control txtPadd" 
                value="" id="Parameter1Add" name="Parameter1"
                onkeypress="HideErr('Parameter1AddErr');">
                 <span id="Parameter1AddErr"></span>
            </div>
            <div class="col-md-2 form-group tadmPadd labFontt">
              <label for="exampleInputEmail1">Parameter2:</label>
                <input type="text" class="form-control txtPadd" 
                value="" id="Parameter2Add" name="Parameter2"
                onkeypress="HideErr('Parameter2AddErr');">
                 <span id="Parameter2AddErr"></span>
            </div>
            <div class="col-md-2 form-group tadmPadd labFontt">
              <label for="exampleInputEmail1">Parameter3:</label>
                <input type="text" class="form-control txtPadd" 
                value="" id="Parameter3Add" name="Parameter3"
                onkeypress="HideErr('Parameter3AddErr');">
                 <span id="Parameter3AddErr"></span>
            </div>
            <div class="col-md-2 form-group tadmPadd labFontt">
              <label for="exampleInputEmail1">Parameter4:</label>
                <input type="text" class="form-control txtPadd" 
                value="" id="Parameter4Add" name="Parameter4"
                onkeypress="HideErr('Parameter4AddErr');">
                 <span id="Parameter4AddErr"></span>
            </div>
          </div>
           <div class="row">
            <div class="col-md-4 form-group labFontt tablabFontt">
              <label>File: (You Can Select Multiple Files)</label>
              <div class="custom-file">
                <input type="file" multiple class="custom-file-input" id="files" name="filesAdd[]" multiple>
                <label class="custom-file-label fileLabel" for="files">No File Chosen</label>
              </div>
            </div>                            
            <div class="col-md-4 form-group mb-0 labFontt">
              <label for="exampleFormControlTextarea1">Buyer Notes:</label>
              <textarea class="form-control" id="BuyerNotesAdd" name="BuyerNotes" rows="3"></textarea>
            </div>
          </div>
          <div class="row btnFlo">
                <div class="col-md-12 form-group mb-0 labFontt">
                  <button type="button" class="btn btn-success btn-fw noBoradi mr-2" onclick="AddBuyerDetails();">Save</button>
                  <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </form>
        </div>
      </div>        
    </div>
  </div> 
  <!-- Edit Window -->
  <div class="modal fade" id="EditBuyerModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header mHBg">
          <h4 class="modal-title">Edit Buyer Details</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body" id="EditBuyerDiv">
          
        </div>
        <div class="modal-footer">

        </div>
      </div>        
    </div>
  </div>
 <!-- Clone Window -->
  <div class="modal fade" id="CloneBuyerModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header mHBg">
          <h4 class="modal-title">Clone Buyer Details</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body" id="CloneBuyerDiv">
          
        </div>
        <div class="modal-footer">

        </div>
      </div>        
    </div>
  </div>

  <input type="hidden" name="url" id="url" value='{{URL::to("/")}}'>
  <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
  <input type="hidden" name="loading" id="loading" value='{{ asset("Admin/images/loading.gif") }}'>
  @include('layouts.Script')
  <script src="{{asset('Ajax/BuyerManagement.js')}}"></script> 
  <script>
  $(document).ready(function() {
    $('#sandbox-container .input-daterange').datepicker({});

    $('#datepicker').datepicker({
      weekStart: 0,
      daysOfWeekHighlighted: "6,0",
      autoclose: true,
      todayHighlight: true,
      dateFormat: 'mm/dd/yy'
    });
    $('#datepicker').datepicker("setDate", new Date());
  });
  </script>
</body>
