<!DOCTYPE html>
<html lang="en">
@include('layouts.Head')
<body>
  <div class="container-scroller">
    @include('layouts.Header')
    <div class="container-fluid page-body-wrapper">
      @include('layouts.Menu')
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="page-header row">
            <h3 class="page-title">
              User Management
            </h3>
          </div>
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb breadcrumb-custom">
              <li class="breadcrumb-item"><a href="#"><i class="fas fa-users-cog menu-icon"></i> Admin Tools</a></li>
              <li class="breadcrumb-item"><a href="{{ route('admin.UserManagementView') }}"><i class="fas fa-users menu-icon"></i> User Management </a></li>
              <li class="breadcrumb-item active" aria-current="page"><span><i class="fas fa-plus-square menu-icon"></i> Add User Details </span></li>
            </ol>
          </nav>
          <div class="col-md-12">
            @if(Session::has('message'))
              <div class="alert alrtDanger {{ Session::get('alert-class', 'alert-info') }}">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>{{ Session::get('message') }}</strong>
              </div>
            @endif
          </div>
          <div class="allDet">
             <div class="col-md-12 grid-margin stretch-card">
                <div class="card">
                   <div class="card-body">                      
                     <div class="addUser">
                          <div class="row">
                            <div class="page-header-below">
                               <h3 class="page-title-below">
                                  Add Users Details
                               </h3>
                            </div>                            
                          </div>                          
                         <form action="{{route('admin.AddUserDetails')}}" name="UserAddForm" id="UserAddForm" method="POST">
                            <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                            <div class="row">
                               <div class="col-md-3 form-group tadmPadd labFontt">
                                  <label for="exampleInputEmail1">Company:</label>
                                  <input disabled type="text" onkeypress="HideErr('CompanyIDErr');" value="{{$CompanyDetails->CompanyName}}"  class="form-control txtPadd" placeholder="CompanyID">
                                  <span id="CompanyIDErr" class="errordisp"></span>
                               </div>
                               <div class="col-md-3 form-group tadmPadd labFontt">
                                  <label for="exampleInputEmail1">First Name:</label>
                                  <input type="text" onkeypress="HideErr('UserFirstNameErr');" value="" id="UserFirstName" name="UserFirstName" class="form-control txtPadd" placeholder="First Name">
                                  <span id="UserFirstNameErr" class="errordisp"></span>
                               </div>
                               <div class="col-md-3 form-group tadmPadd labFontt">
                                  <label for="exampleInputEmail1">Last Name:</label>
                                  <input type="text" onkeypress="HideErr('UserLastNameErr');" value="" id="UserLastName" name="UserLastName" class="form-control txtPadd" placeholder="Last Name">
                                  <span id="UserLastNameErr" class="errordisp"></span>
                               </div>
                               <div class="col-md-3 form-group tadmPadd labFontt">
                                  <label for="exampleInputEmail1">Email:</label>
                                  <input type="email" onkeypress="HideErr('UserEmailErr');" value="" id="UserEmail" name="UserEmail" class="form-control camH" id="exampleInputEmail1" placeholder="Email">
                                  <span id="UserEmailErr" class="errordisp"></span>
                               </div>
                            </div>
                            <div class="row">
                               
                               <div class="col-md-3 form-group tadmPadd labFontt">
                                  <label for="exampleInputEmail1">Telephone:</label>
                                  <input type="text" onkeypress="HideErr('UserTelErr');" value="" id="UserTel" name="UserTel" class="form-control txtPadd" placeholder="Telephone">
                                  <span id="UserTelErr" class="errordisp"></span>
                               </div>
                               <div class="col-md-3 form-group tadmPadd labFontt">
                                  <label for="exampleInputEmail1">Skype ID:</label>
                                  <input type="text" onkeypress="HideErr('UserSkypeErr');" value="" id="UserSkype" name="UserSkype" class="form-control txtPadd" placeholder="Skype ID">
                                  <span id="UserSkypeErr" class="errordisp"></span>
                               </div>

                              <div class="col-md-4 form-group tadmPadd labFontt">
                                <label for="exampleInputEmail1">User Status:</label>
                                <div class="onoffswitch4">
                                  <input type="checkbox" name="UserStatus" class="onoffswitch4-checkbox" id="UserStatus" 
                                    checked>
                                  <label class="onoffswitch4-label" for="UserStatus">
                                      <span class="onoffswitch4-inner"></span>
                                      <span class="onoffswitch4-switch"></span>
                                  </label>
                                </div>
                              </div>                                                              
                            </div>
                            <div class="row" >
                               <div class="col-md-3 form-group tadmPadd labFontt">
                                  <label for="exampleInputPassword1">Password:</label>
                                  <input type="text" onkeypress="HideErr('PasswordErr');" class="form-control camH" id="Password" name="Password" placeholder="Password">
                                  <span id="PasswordErr"  class="errordisp"></span>
                               </div>
                               <div class="col-md-3 form-group tadmPadd labFontt">
                                  <label for="exampleInputPassword1">Confirm Password:</label>
                                  <input type="text" onkeypress="HideErr('ConfirmPasswordErr');" class="form-control camH" id="ConfirmPassword" name="ConfirmPassword" placeholder="Confirm Password">
                                  <span id="ConfirmPasswordErr"  class="errordisp"></span>
                               </div>
                               <div class="col-md-3 form-group tadmPadd labFontt">
                                  <label class="gP" for="exampleInputPassword1">Generate Password:</label>
                                  <button type="button" onclick="GeneratePassword();" class="btn btn-success fonT noBoradi cbtnPadd sercFontt gpBg">Generate Password</button>
                               </div>
                            </div>
                             <div class="row" >
                               <div class="col-md-4 form-group tadmPadd labFonsize">
                                  <div class="form-check form-check-flat form-check-primary adComp adComp-primary mt-4">
                                     <label class="form-check-label adComp-label">
                                     <input type="checkbox" class="form-check-input" id="SendCredentials" name="SendCredentials">
                                      Would you like to email credentials to the user?
                                     </label>
                                  </div>
                               </div>
                            </div>
                            <div class="row">
                               <button type="button" id="UserAddBtn" class="btn btn-success btn-fw noBoradi mr-2">Save</button>
                               <a href="{{ route('admin.UserManagementView') }}" class="btn btn-danger btn-fw noBoradi">Cancel</a>
                            </div>
                         </form>
                      </div>
                   </div>
                </div>
             </div>
          </div>
       </div>
       @include('layouts.Footer')
    </div>
    </div>
  </div>
  <input type="hidden" name="url" id="url" value='{{URL::to("/")}}'>
  <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
  <input type="hidden" name="loading" id="loading" value='{{ asset("Admin/images/loading_image.gif") }}'>
  @include('layouts.Script')
  <script src="{{asset('Ajax/UserManagement.js')}}"></script> 
  <script>
  $(document).ready(function() {
    $('#sandbox-container .input-daterange').datepicker({});

    $('#datepicker').datepicker({
      weekStart: 0,
      daysOfWeekHighlighted: "6,0",
      autoclose: true,
      todayHighlight: true,
      dateFormat: 'mm/dd/yy'
    });
    $('#datepicker').datepicker("setDate", new Date());
  });
  </script>
</body>
