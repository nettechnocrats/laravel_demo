<?php
$BuyerDetails = []; 
$CompanyID          = '';
$CampaignID         = '1';
$CampaignType         = '';
$BuyerTierID        = '';
$AdminLabel         = '';
$PartnerLabel       = '';
$IntegrationFileName= '';
$PayoutCalculation  = '';
$FixedPrice         = 2.00;
$MinimumPayout      = 0.00;
$BuyerStatus        = 1;
$BuyerTestMode      = 1;
$DailyCap           = 10;
$TotalCap           = 10;
$PingTestURL        = '';
$PingLiveURL        = ''; 
$PostTestURL        = '';
$PostLiveURL        = '';
$RequestMethod      = '';
$RequestHeader      = '';
$MaxTimeOut         = 30;
$MinTimeOut         = 20;
$MaxPingTimeOut     = '';
$MaxPostTimeOut     = '';
$MaxPingsPerMinute  = '';
$Parameter1         = '';
$Parameter2         = '';
$Parameter3         = '';
$Parameter4         = '';
$UploadedFiles      = '';
$BuyerNotes         = ''; 
$ShowTest           = 0; 
$ShowInactive       = 1; 
$IfClone = "No";
if(Session::has('BuyerDetails'))
{
    $BuyerDetails       = Session::get('BuyerDetails'); 
    $CompanyID          = $BuyerDetails['CompanyID'];
    $CampaignID         = $BuyerDetails['CampaignID'];
    $CampaignType       = $BuyerDetails['CampaignType'];
    $BuyerTierID        = $BuyerDetails['BuyerTierID'];
    $AdminLabel         = $BuyerDetails['AdminLabel'];
    $PartnerLabel       = $BuyerDetails['PartnerLabel'];
    $IntegrationFileName= $BuyerDetails['IntegrationFileName'];
    $PayoutCalculation  = $BuyerDetails['PayoutCalculation'];
    $FixedPrice         = $BuyerDetails['FixedPrice'];
    $MinimumPayout      = $BuyerDetails['FixedPrice'];
    $BuyerStatus        = $BuyerDetails['BuyerStatus'];
    $BuyerTestMode      = $BuyerDetails['BuyerTestMode'];
    $DailyCap           = $BuyerDetails['DailyCap'];
    $TotalCap           = $BuyerDetails['TotalCap'];
    $PingTestURL        = $BuyerDetails['PingTestURL'];
    $PingLiveURL        = $BuyerDetails['PingLiveURL']; 
    $PostTestURL        = $BuyerDetails['PostTestURL'];
    $PostLiveURL        = $BuyerDetails['PostLiveURL'];
    $RequestMethod      = $BuyerDetails['RequestMethod'];
    $RequestHeader      = $BuyerDetails['RequestHeader'];
    $MaxTimeOut         = $BuyerDetails['MaxTimeOut'];
    $MinTimeOut         = $BuyerDetails['MinTimeOut'];
    $MaxPingTimeOut     = $BuyerDetails['MaxPingTimeOut'];
    $MaxPostTimeOut     = $BuyerDetails['MaxPostTimeOut'];
    $MaxPingsPerMinute  = '';
    $Parameter1         = $BuyerDetails['Parameter1']; 
    $Parameter2         = $BuyerDetails['Parameter2'];
    $Parameter3         = $BuyerDetails['Parameter3'];
    $Parameter4         = $BuyerDetails['Parameter4'];
    $UploadedFiles      = $BuyerDetails['UploadedFiles'];
    $BuyerNotes         = $BuyerDetails['BuyerNotes']; 
    $ShowTest           = $BuyerDetails['ShowTest']; 
    $ShowInactive       = $BuyerDetails['ShowInactive']; 
    $IfClone            = $BuyerDetails['IfClone'];
    Session::forget('BuyerDetails');
}
?>
<!DOCTYPE html>
<html lang="en">
@include('layouts.Head')
<body>
  <div class="container-scroller">
    @include('layouts.Header')
    <div class="container-fluid page-body-wrapper">
      @include('layouts.Menu')
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="page-header row">
            <h3 class="page-title">
             Buyer Management 
            </h3>
          </div>
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb breadcrumb-custom">
              <li class="breadcrumb-item"><a href="#"><i class="fas fa-users-cog menu-icon"></i> Admin Tools</a></li>
              <li class="breadcrumb-item"><a href="{{route('admin.BuyerManagementView')}}"><i class="fas fa-users menu-icon"></i> Buyer Management </a></li>
              <li class="breadcrumb-item active" aria-current="page"><span><i class="fas fa-user-plus menu-icon"></i> Add Buyer Details </span></li>
            </ol>
          </nav>
          <div class="col-md-12">
            @if(Session::has('message'))
              <div class="alert alrtDanger {{ Session::get('alert-class', 'alert-info') }}">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>{{ Session::get('message') }}</strong>
              </div>
            @endif
          </div>
          <div class="col-md-12" id="Message">
          </div>
          <div class="allDet">
             <div class="col-md-12 grid-margin stretch-card">
                <div class="card">
                   <div class="card-body">                      
                     <div class="addUser">
                          <div class="row">
                            <div class="page-header-below">
                               <h3 class="page-title-below">
                                  Add Buyer Details
                               </h3>
                            </div>                            
                          </div>                          
                         
                         <form action="{{route('admin.AddBuyerDetails')}}" name="BuyerAddForm" 
                          id="BuyerAddForm" method="POST" enctype="multipart/form-data">                          
                          <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                          <input type="hidden" name="CampaignType" id="CampaignType" value="{{ $CampaignType }}">
                          <div class="row">                          
                            <div class="col-md-2 form-group mb-0 tadmPadd labFontt">
                                <label for="exampleFormControlSelect1">Campaign:</label>
                                <select class="form-control fonT" id="Campaign" name="Campaign"  
                                    onchange="GetCampaignDetails(),HideErr('CompanyErr');">
                                    @foreach($CampaignDropDown as $cdd)
                                    <option value="{{$cdd->CampaignID}}"
                                      <?php if($cdd->CampaignID==$CampaignID){echo "selected";}?>>{{$cdd->CampaignName}}</option>
                                    @endforeach
                                </select>
                                <span id="CampaignErr"></span>
                            </div>
                            <div class="col-md-2 form-group mb-0 tadmPadd labFontt">
                              <label for="exampleFormControlSelect1">Company:</label>
                                <select class="form-control fonT" id="Company" name="Company"
                                  onchange="HideErr('CompanyErr');">
                                  <option value="">Select Company</option>
                                  @foreach($CompanyList as $cl)
                                  <option value="{{$cl->CompanyID}}"
                                  <?php if($cl->CompanyID==$CompanyID){echo "selected";}?>>{{$cl->CompanyName}}</option>
                                  @endforeach                                 
                                </select>
                                <span id="CompanyErr"></span>
                            </div>
                            <div class="col-md-3 form-group tadmPadd">
                              <div class="form-check form-check-flat form-check-primary adComp adComp-primary disPlay tabdisPlay mt-4">
                                <label class="form-check-label adComp-label">
                                <input type="checkbox" class="form-check-input" id="ShowInactive"
                                onclick="GetCompanyUsingShwowInactiveAndTest();">
                                  Show Inactive
                                <i class="input-helper"></i></label>
                              </div>
                              <div class="form-check form-check-flat form-check-primary adComp adComp-primary disPlay tabdisPlay mt-4">
                                <label class="form-check-label adComp-label">
                                <input type="checkbox" class="form-check-input" id="ShowTest"
                                onclick="GetCompanyUsingShwowInactiveAndTest();">
                                  Show Test
                                <i class="input-helper"></i></label>
                              </div>
                            </div>
                           </div>
                          <div class="row">
                            <div class="col-md-2 form-group tadmPadd labFontt">
                              <label for="exampleInputEmail1">Admin Label:</label>
                                <input type="text" class="form-control txtPadd" 
                                value="{{$AdminLabel}}" id="AdminLabel" name="AdminLabel"
                                onkeypress="HideErr('AdminLabelErr');">
                                 <span id="AdminLabelErr"></span>
                            </div>
                            <div class="col-md-1 form-group tadmPadd btnCen">
                              <button type="button" id="CopyAdminLable" class="btn btn-success noBoradi mr-2">&gt;&gt;</button>
                            </div>
                            <div class="col-md-2 form-group tadmPadd labFontt">
                              <label for="exampleInputEmail1">Partner Label:</label>
                                <input type="text" class="form-control txtPadd" 
                                  value="{{$PartnerLabel}}" id="PartnerLabel" name="PartnerLabel"
                                  onkeypress="HideErr('PartnerLabelErr');">
                                   <span id="PartnerLabelErr"></span>
                            </div>
                            <div class="col-md-2 form-group tadmPadd labFontt">
                              <label for="exampleInputEmail1">Integration File Name:</label>
                                <input type="text"value="{{$IntegrationFileName}}" 
                                  id="IntegrationFileName" name="IntegrationFileName" class="form-control txtPadd" placeholder="Integration File Name"
                                  onkeypress="HideErr('IntegrationFileNameErr');">
                                <span id="IntegrationFileNameErr"></span>
                            </div>
                            <div class="col-md-2 form-group mb-0 tadmPadd labFontt">
                              <label for="PayoutCalculation">Payout Calculation:</label>
                                <select class="form-control fonT" id="PayoutCalculation" name="PayoutCalculation">
                                  <option value="RevShare" <?php if($PayoutCalculation=="RevShare"){ echo "selected"; } ?>>RevShare</option>
                                  <option value="FixedPrice" <?php if($PayoutCalculation=="FixedPrice"){ echo "selected"; } ?>>Fixed Price</option>
                                </select>
                            </div>
                            <?php 
                            if($PayoutCalculation=='FixedPrice')
                            { 
                                 $PayoutCalculationFixedPrice="style='display:block'";
                                 $PayoutCalculationRevShare="style='display:none'";
                            } 
                            else if($PayoutCalculation=='RevShare')
                            {
                                 $PayoutCalculationRevShare="style='display:block'";
                                 $PayoutCalculationFixedPrice="style='display:none'";
                            }
                            else
                            {
                                 $PayoutCalculationFixedPrice="style='display:block'";
                                 $PayoutCalculationRevShare="style='display:none'";
                            }
                            ?>
                            <div class="col-md-2 form-group tadmPadd labFontt" <?php echo $PayoutCalculationRevShare ?>  
                                id="RevsharePercentInput">
                              <label for="exampleInputEmail1">RevShare: </label>
                              <input type="text" class="form-control txtPadd" value="{{$FixedPrice}}" 
                              placeholder="70" id="Revshare" name="Revshare" onkeypress="HideErr('RevshareErr');">
                              <span id="RevshareErr"></span>
                            </div>
                            <div class="col-md-2 form-group tadmPadd labFontt" <?php echo $PayoutCalculationFixedPrice ?> 
                                id="FixedPriceInput">
                              <label for="exampleInputEmail1">Fixed Price:</label>
                              <input type="text" class="form-control txtPadd" value="{{$FixedPrice}}" 
                              placeholder="70" id="FixedPrice" name="FixedPrice" onkeypress="HideErr('FixedPriceErr');">
                              <span id="FixedPriceErr"></span>
                            </div>
                          </div>                         
                          <div class="row">
                            <?php 
                            if($CampaignType=="DirectPost"){                           
                              $BuyerTierIDInput="style='display:block;'";
                            }
                            else{
                              $BuyerTierIDInput="style='display:none;'";
                            }
                            ?>
                            <div class="col-md-2 form-group tadmPadd labFontt" id="BuyerTierIDInput" <?php echo $BuyerTierIDInput; ?>>
                              <label for="exampleInputEmail1">Buyer Tier ID:</label>
                                <input type="text" onkeypress="HideErr('BuyerTierIDErr');" value="{{$BuyerTierID}}" 
                                  id="BuyerTierID" name="BuyerTierID" class="form-control txtPadd" placeholder="Buyer Tier ID">
                                <span id="BuyerTierIDErr"></span>
                            </div>
                            <div class="col-md-1 form-group tadmPadd labFontt">
                              <label for="exampleInputEmail1">Daily Cap:</label>
                                <input type="text" value="{{$DailyCap}}" 
                                  id="DailyCap" name="DailyCap" class="form-control txtPadd" placeholder="Daily Cap"
                                  onkeypress="HideErr('DailyCapErr');">
                                <span id="DailyCapErr"></span>
                            </div>
                            <div class="col-md-1 form-group tadmPadd labFontt">
                              <label for="exampleInputEmail1">Total Cap:</label>
                                <input type="text" value="{{$TotalCap}}" 
                                  id="TotalCap" name="TotalCap" class="form-control txtPadd" placeholder="Total Cap"
                                  onkeypress="HideErr('TotalCapErr');">
                                <span id="TotalCapErr"></span>
                            </div>
                            <?php if($BuyerStatus=='1'){ ?>
                            <div class="col-md-2 form-group tadmPadd labFontt">
                              <label for="exampleInputEmail1">Buyer Status:</label>
                                <div class="onoffswitch8">
                                  <input type="checkbox" name="BuyerStatus" class="onoffswitch8-checkbox" 
                                  id="BuyerStatus" checked="">
                                  <label class="onoffswitch8-label" for="BuyerStatus">
                                    <span class="onoffswitch8-inner"></span>
                                    <span class="onoffswitch8-switch"></span>
                                  </label>
                                </div>
                            </div>
                            <?php } else {?>
                            <div class="col-md-2 form-group tadmPadd labFontt">
                              <label for="exampleInputEmail1">Buyer Status:</label>
                                <div class="onoffswitch8">
                                  <input type="checkbox" name="BuyerStatus" class="onoffswitch8-checkbox" 
                                  id="BuyerStatus">
                                  <label class="onoffswitch8-label" for="BuyerStatus">
                                    <span class="onoffswitch8-inner"></span>
                                    <span class="onoffswitch8-switch"></span>
                                  </label>
                                </div>
                            </div>
                            <?php }?>
                            <?php if($BuyerTestMode=='1'){ ?>
                            <div class="col-md-2 form-group tadmPadd labFontt">
                              <label for="exampleInputEmail1">Buyer Mode?</label>
                                <div class="onoffswitch9">
                                  <input type="checkbox" name="BuyerTestMode" class="onoffswitch9-checkbox"
                                     id="BuyerTestMode" checked="">
                                  <label class="onoffswitch9-label" for="BuyerTestMode">
                                    <span class="onoffswitch9-inner"></span>
                                    <span class="onoffswitch9-switch"></span>
                                  </label>
                                </div>
                            </div>
                            <?php } else {?>
                            <div class="col-md-2 form-group tadmPadd labFontt">
                              <label for="exampleInputEmail1">Buyer Mode?</label>
                                <div class="onoffswitch9">
                                  <input type="checkbox" name="BuyerTestMode" class="onoffswitch9-checkbox" 
                                    id="BuyerTestMode" >
                                  <label class="onoffswitch9-label" for="BuyerTestMode">
                                    <span class="onoffswitch9-inner"></span>
                                    <span class="onoffswitch9-switch"></span>
                                  </label>
                                </div>
                            </div>
                            <?php }?>
                            <?php 
                            if($CampaignType=="DirectPost"){                           
                              $MaxTimeOutInput = "style='display:block;'";
                              $MinTimeOutInput = "style='display:block;'";
                              $MaxPingTimeOutInput = "style='display:none;'";
                              $MaxPostTimeOutInput = "style='display:none;'";
                              $MaxPingsPerMinuteInput = "style='display:none;'";
                            }else if($CampaignType=="PingPost"){
                              $MaxTimeOutInput = "style='display:none;'";
                              $MinTimeOutInput = "style='display:none;'";
                              $MaxPingTimeOutInput = "style='display:block;'";
                              $MaxPostTimeOutInput = "style='display:block;'";
                              $MaxPingsPerMinuteInput = "style='display:block;'";
                            }
                            else {
                              $MaxTimeOutInput = "style='display:none;'";
                              $MinTimeOutInput = "style='display:none;'";
                              $MaxPingTimeOutInput = "style='display:block;'";
                              $MaxPostTimeOutInput = "style='display:block;'";
                              $MaxPingsPerMinuteInput = "style='display:block;'";
                            }
                            ?>
                            <div class="col-md-2 form-group tadmPadd labFontt" id="MaxTimeOutInput" <?php echo $MaxTimeOutInput; ?>>
                              <label for="exampleInputEmail1">Max Time Out:</label>
                                <input type="text" class="form-control txtPadd" placeholder="0" 
                                id="MaxTimeOut" name="MaxTimeOut" value="{{$MaxTimeOut}}" onkeypress="HideErr('MaxTimeOutErr');">
                                <span id="MaxTimeOutErr"></span>
                            </div>
                            <div class="col-md-2 form-group tadmPadd labFontt" id="MinTimeOutInput" <?php echo $MinTimeOutInput; ?>>
                              <label for="exampleInputEmail1">Min Time Out:</label>
                                <input type="text" class="form-control txtPadd" placeholder="0" 
                                id="MinTimeOut" name="MinTimeOut" value="{{$MinTimeOut}}" onkeypress="HideErr('MinTimeOutErr');">
                                <span id="MinTimeOutErr"></span>
                            </div>

                            <div class="col-md-2 form-group tadmPadd labFontt" id="MaxPingTimeOutInput" <?php echo $MaxPingTimeOutInput; ?>>
                              <label for="exampleInputEmail1">Max Ping Time Out:</label>
                                <input type="text" class="form-control txtPadd" placeholder="0" 
                                id="MaxPingTimeOut" name="MaxPingTimeOut" value="{{$MaxPingTimeOut}}" onkeypress="HideErr('MaxPingTimeOutErr');">
                                <span id="MaxPingTimeOutErr"></span>
                            </div>
                            <div class="col-md-2 form-group tadmPadd labFontt" id="MaxPostTimeOutInput" <?php echo $MaxPostTimeOutInput; ?>>
                              <label for="exampleInputEmail1">Max Post Time Out:</label>
                                <input type="text" class="form-control txtPadd" placeholder="0" 
                                id="MaxPostTimeOut" name="MaxPostTimeOut" value="{{$MaxPostTimeOut}}" onkeypress="HideErr('MaxPostTimeOutErr');">
                                <span id="MaxPostTimeOutErr"></span>
                            </div>
                            <div class="col-md-2 form-group tadmPadd labFontt" id="MaxPingsPerMinuteInput" <?php echo $MaxPingsPerMinuteInput; ?>>
                              <label for="exampleInputEmail1">Max Pings Per Minute:</label>
                                <input type="text" class="form-control txtPadd" placeholder="0" 
                                id="MaxPingsPerMinute" name="MaxPingsPerMinute" value="{{$MaxPingsPerMinute}}" onkeypress="HideErr('MaxPingsPerMinuteErr');">
                                <span id="MaxPingsPerMinuteErr"></span>
                            </div>
                          </div>                             
                          <div class="row">
                            <?php 
                            if($CampaignType=="PingPost")
                            {                           
                              ?>
                              <div class="col-md-6 form-group tadmPadd labFontt">
                                <label for="exampleInputEmail1">Ping Test URL:</label>
                                  <input type="text" class="form-control txtPadd" 
                                  value="{{$PingTestURL}}" id="PingTestURL" name="PingTestURL"
                                  onkeypress="HideErr('PingTestURLErr');">
                                   <span id="PingTestURLErr"></span>
                              </div>
                              <div class="col-md-6 form-group tadmPadd labFontt">
                                <label for="exampleInputEmail1">Ping Live URL:</label>
                                  <input type="text" class="form-control txtPadd" 
                                    value="{{$PingLiveURL}}" id="PingLiveURL" name="PingLiveURL"
                                    onkeypress="HideErr('PingLiveURLErr');">
                                     <span id="PingLiveURLErr"></span>
                              </div>
                              <div class="col-md-6 form-group tadmPadd labFontt">
                                <label for="exampleInputEmail1">Post Test URL:</label>
                                  <input type="text" class="form-control txtPadd" 
                                  value="{{$PostTestURL}}" id="PostTestURL" name="PostTestURL"
                                  onkeypress="HideErr('PostTestURLErr');">
                                   <span id="PostTestURLErr"></span>
                              </div>
                              <div class="col-md-6 form-group tadmPadd labFontt">
                                <label for="exampleInputEmail1">Post Live URL:</label>
                                  <input type="text" class="form-control txtPadd" 
                                    value="{{$PostLiveURL}}" id="PostLiveURL" name="PostLiveURL"
                                    onkeypress="HideErr('PostLiveURLErr');">
                                     <span id="PostLiveURLErr"></span>
                              </div>
                              <?php  
                            }
                            else if($CampaignType=="DirectPost")
                            {
                              ?>
                              <div class="col-md-6 form-group tadmPadd labFontt">
                                <label for="exampleInputEmail1">DirectPost Test URL:</label>
                                  <input type="text" class="form-control txtPadd" 
                                  value="{{$PostTestURL}}" id="DirectPostTestURL" name="DirectPostTestURL"
                                  onkeypress="HideErr('DirectPostTestURLErr');">
                                   <span id="DirectPostTestURLErr"></span>
                              </div>
                              <div class="col-md-6 form-group tadmPadd labFontt">
                                <label for="exampleInputEmail1">DirectPost Live URL:</label>
                                  <input type="text" class="form-control txtPadd" 
                                    value="{{$PostLiveURL}}" id="DirectPostLiveURL" name="DirectPostLiveURL"
                                    onkeypress="HideErr('DirectPostLiveURLErr');">
                                     <span id="DirectPostLiveURLErr"></span>
                              </div>
                              <?php
                            }
                            ?>
                          </div>
                          <div class="row">
                            <div class="col-md-2 form-group mb-0 tadmPadd labFontt">
                                <label for="exampleFormControlSelect1">Request Method:</label>
                                <select class="form-control fonT" id="RequestMethod" name="RequestMethod" >
                                    <option value="1" <?php if($RequestMethod=='1'){echo "selected";}?>>POST</option>
                                    <option value="2" <?php if($RequestMethod=='2'){echo "selected";}?>>GET</option>
                                    <option value="3" <?php if($RequestMethod=='3'){echo "selected";}?>>XmlinPost</option>
                                    <option value="4" <?php if($RequestMethod=='4'){echo "selected";}?>>XmlPost</option>
                                    <option value="5" <?php if($RequestMethod=='5'){echo "selected";}?>>InlineXML</option>
                                </select>
                                <span id="RequestMethodErr"></span>
                            </div>
                            <div class="col-md-2 form-group mb-0 tadmPadd labFontt">
                                <label for="exampleFormControlSelect1">Request Header:</label>
                                <select class="form-control fonT" id="RequestHeader" name="RequestHeader" >
                                    <option value="1" <?php if($RequestHeader=='1'){echo "selected";}?>>Content-Type: application/x-www-form-urlencoded</option>
                                    <option value="2" <?php if($RequestHeader=='2'){echo "selected";}?>>Content-Type:text/xml; charset=utf-8 </option>
                                    <option value="3" <?php if($RequestHeader=='3'){echo "selected";}?>>Content-type: application/xml </option>
                                    <option value="4" <?php if($RequestHeader=='4'){echo "selected";}?>>Content-type: application/json</option>
                                </select>
                                <span id="RequestHeaderErr"></span>
                            </div>
                            <div class="col-md-2 form-group tadmPadd labFontt">
                              <label for="exampleInputEmail1">Parameter1:</label>
                                <input type="text" class="form-control txtPadd" 
                                value="{{$Parameter1}}" id="Parameter1" name="Parameter1"
                                onkeypress="HideErr('Parameter1Err');">
                                 <span id="Parameter1Err"></span>
                            </div>
                            <div class="col-md-2 form-group tadmPadd labFontt">
                              <label for="exampleInputEmail1">Parameter2:</label>
                                <input type="text" class="form-control txtPadd" 
                                value="{{$Parameter2}}" id="Parameter2" name="Parameter2"
                                onkeypress="HideErr('Parameter2Err');">
                                 <span id="Parameter2Err"></span>
                            </div>
                            <div class="col-md-2 form-group tadmPadd labFontt">
                              <label for="exampleInputEmail1">Parameter3:</label>
                                <input type="text" class="form-control txtPadd" 
                                value="{{$Parameter3}}" id="Parameter3" name="Parameter3"
                                onkeypress="HideErr('Parameter3Err');">
                                 <span id="Parameter3Err"></span>
                            </div>
                            <div class="col-md-2 form-group tadmPadd labFontt">
                              <label for="exampleInputEmail1">Parameter4:</label>
                                <input type="text" class="form-control txtPadd" 
                                value="{{$Parameter4}}" id="Parameter4" name="Parameter4"
                                onkeypress="HideErr('Parameter4Err');">
                                 <span id="Parameter4Err"></span>
                            </div>
                          </div>
                           <div class="row">
                            <div class="col-md-4 form-group labFontt tablabFontt">
                              <label>File: (You Can Select Multiple Files)</label>
                              <div class="custom-file">
                                <input type="file" multiple class="custom-file-input" id="files" name="files[]" multiple>
                                <label class="custom-file-label fileLabel" for="files">No File Chosen</label>
                              </div>
                            </div>                            
                            <div class="col-md-4 form-group mb-0 labFontt">
                              <label for="exampleFormControlTextarea1">Buyer Notes:</label>
                              <textarea class="form-control" id="BuyerNotes" name="BuyerNotes" rows="3">{{$BuyerNotes}}</textarea>
                            </div>
                          </div>
                          <div class="row">
                            <button type="button" class="btn btn-success btn-fw noBoradi mr-2" id="BuyerAddBtn">Save</button>
                            <a href="{{ route('admin.BuyerManagementView') }}" class="btn btn-danger btn-fw noBoradi">Cancel</a>
                          </div>
                        </form>
                      </div>
                   </div>
                </div>
             </div>
          </div>
       </div>
       @include('layouts.Footer')
    </div>
    </div>
  </div>
  <input type="hidden" name="url" id="url" value='{{URL::to("/")}}'>
  <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
  <input type="hidden" name="loading" id="loading" value='{{ asset("Admin/images/loading_image.gif") }}'>
  @include('layouts.Script')
  <script src="{{asset('Ajax/BuyerManagement.js')}}"></script> 
  <script>
  $(document).ready(function() {
    $('#sandbox-container .input-daterange').datepicker({});

    $('#datepicker').datepicker({
      weekStart: 0,
      daysOfWeekHighlighted: "6,0",
      autoclose: true,
      todayHighlight: true,
      dateFormat: 'mm/dd/yy'
    });
    $('#datepicker').datepicker("setDate", new Date());
  });
  </script>
</body>
