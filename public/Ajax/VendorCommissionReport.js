$(document).ready(function() {    
    GetVendorBuyerUsingCampaignID();
    GetVendorCommissionReport();
});
function SearchVendorCommissionReport()
{
    GetVendorCommissionReport();
}
function GetVendorCommissionReport()
{
    var loading=$("#loading").val();
    $("#dataTable").html('<center><img src="'+loading+'" style="width: 200px"></center>'); 
    var token           = $("#_token").val();
    var url             = $("#url").val(); 

    var CampaignID      = $("#Campaign").val(); 
    var VendorCompanyID = $("#VendorCompany").val();
    var BuyerCompanyID  = $("#BuyerCompany").val();
    var BuyerID         = $("#Buyer").val();

    var StartDate       = $("#StartDate").val();
    var EndDate         = $("#EndDate").val();
    
     var DataStr        = {'_token':token,'CampaignID':CampaignID,'StartDate':StartDate,'EndDate':EndDate,'VendorCompanyID':VendorCompanyID,'BuyerCompanyID':BuyerCompanyID,'BuyerID':BuyerID};
    $.ajax({
        type: "POST",
        url:  url+"/GetVendorCommissionReport",
        data: DataStr,
        success: function(data) 
        {
            $("#dataTable").html(data);         
        }
    }); 
}
function GetVendorBuyerUsingCampaignID()
{ 
    var token           = $("#_token").val();
    var url             = $("#url").val(); 

    var CampaignID      = $("#Campaign").val();     
    var DataStr         = {'_token':token,'CampaignID':CampaignID};
    $.ajax({
        type: "POST",
        url:  url+"/GetVendorBuyerUsingCampaignID",
        data: DataStr,
        dataType: 'json',
        success: function(data) 
        {
            $("#VendorCompany").html(data.VendorCompany);
            $("#BuyerCompany").html(data.BuyerCompany);
            $("#Buyer").html(data.Buyer);       
        }
    }); 
}

function GetBuyerUsingBuyerCompany()
{
    var BuyerCompanyID = $("#BuyerCompany").val();
    var CampaignID      = $("#Campaign").val(); 
    var token = $("#_token").val();
    var url = $("#url").val();

    var DataStr = {'_token': token, 'BuyerCompanyID': BuyerCompanyID, 'CampaignID': CampaignID};
    $.ajax({
        type: "POST",
        url: url + "/GetBuyerUsingBuyerCompany",
        data: DataStr,
        success: function (data)
        {
            $("#Buyer").html(data);
        }
    });
}