$(document).ready(function() { 
    GetReturnCodeList();   
    GetUploadedFilesDetails(1);
});
function Pagination(page)
{
    GetUploadedFilesDetails(page);
}
function SearchUploadedFilesDetails()
{
    GetUploadedFilesDetails(1);
}
function GetUploadedFilesDetails(page)
{
    var loading=$("#loading").val();
    //$("#Lead").html('<table class="table table-striped table-bordered" style="background-color: white;"><tr><td colspan="13"><center><img src="'+loading+'" style="width: 200px"></center></td></tr></table>'); 

    var token           = $("#_token").val();
    var url             = $("#url").val(); 
    var numofrecords    = $('#numofrecords').val();
    var DataStr         = {'_token':token,'page':page,'numofrecords':numofrecords};
    $.ajax({
        type: "POST",
        url:  url+"/UploadedFilesDetails",
        data: DataStr,
        success: function(data) 
        {
            $("#UploadedFiles").html(data);         
        }
    }); 
}


function GetReturnCodeList()
{
    var token           = $("#_token").val();
    var url             = $("#url").val(); 
    var CampaignID      = $("#Campaign").val();
   
    var DataStr         = {'_token':token,'CampaignID':CampaignID};
    $.ajax({
        type: "POST",
        url:  url+"/GetReturnCodeList",
        data: DataStr,
        success: function(data) 
        {
            $("#ReturnCodeListTable").html(data);         
        }
    }); 
}


$("#SaveReturnsBtn").click(function()
{
    var fileExtension = ['csv'];
    
    var file = $("#File").val();
    var flag = 1;
    
    if(file=="")
    {
        $("#FileErr").css("color",'red');
        $("#FileErr").html("Please select a file first, Before click on the import button.");
        $("#FileErr").show();
        flag = 0;
    }
    if(file!="")
    {
        if ($.inArray(file.split('.').pop().toLowerCase(), fileExtension) == -1) 
        {
            $("#FileErr").css("color",'red');
            $("#FileErr").html("Only formats are allowed : "+fileExtension.join(', '));
            $("#FileErr").show();
            flag = 0;
        }
    }

    if(flag==1)
    {
        $("#SaveReturnsFrom").submit();
    }
});


$('#File').on('change',function(e){
    var fileSelected = $(this).val();
    if(fileSelected!="")
    {
        var fileName = e.target.files[0].name;
        $('#NameFile').html(fileName);
        $("#FileErr").hide();
        $("#DelFile").show();
    }
    else
    {
        $('#NameFile').html('No File Chosen');
        $("#FileErr").hide();
        $("#DelFile").hide();
    }
    
});

$('#DelFile').on('click',function(e){
    $('#File').val('');
    $('#NameFile').html('No File Chosen');
    $("#DelFile").hide();
    $("#FileErr").hide();
});