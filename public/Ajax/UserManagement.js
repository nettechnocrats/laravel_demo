$(document).ready(function() {    
    GetCompanyUsingShwowInactiveAndTest();
    GetUserList(1);
});
function Pagination(page)
{
    GetUserList(page);
}
function SearchUserList()
{
    GetUserList(1);
}
function GetUserList(page)
{
    var numofrecords    = $('#numofrecords').val();
    var loading=$("#loading").val();
    if(numofrecords==10)
    {
        $("#dataTable_wrapper").html('<center><img src="'+loading+'" style="width: 200px"><input type="hidden" name="numofrecords" id="numofrecords" value="10"></center>'); 
    }
    else
    {
        $("#dataTable_wrapper").html('<center><img src="'+loading+'" style="width: 200px"></center>'); 
    }
    var token           = $("#_token").val();
    var url             = $("#url").val(); 

    var CompanyID       = $("#Company").val();
    var FirstName       = $("#FirstName").val();
    var LastName        = $("#LastName").val();
    var Email           = $("#Email").val();
    var Phone           = $("#Phone").val();
    var UserStatus      = $("#UserStatus").val();
    var UserRole        = $("#UserRole").val();
    
    var DataStr         = {'_token':token,'page':page,'numofrecords':numofrecords,'CompanyID':CompanyID,'FirstName':FirstName,'LastName':LastName,'Email':Email,'Phone':Phone, 'UserStatus':UserStatus,'UserRole':UserRole};
    $.ajax({
        type: "POST",
        url:  url+"/GetUserList",
        data: DataStr,
        success: function(data) 
        {
            $("#dataTable_wrapper").html(data);         
        }
    }); 
}

function ChangeUserStatus(UID, Status)
{
    
    var token           = $("#_token").val();
    var url             = $("#url").val(); 
    
    var DataStr         = {'_token':token,'UID':UID,'Status':Status};
    $.ajax({
        type: "POST",
        url:  url+"/ChangeUserStatus",
        data: DataStr,
        dataType: 'json',
        success: function(data) 
        {
            if(data.Status==1)
            {
                $("#UserMessage").html(data.Msg);                         
            }
            else
            {                
                $("#UserMessage").html(data.Msg);                         
            }
            setInterval(function(){ $('#UserMessage').fadeOut(); }, 5000);
        }
    });
}
function ChangePasswordShowHide()
{
    if ($("#ChangePassword").prop('checked')) 
    {
        $("#PasswordDiv").show();
        $("#ConfirmPasswordDiv").show();
        $("#GeneratePasswordDiv").show();

        $("#PasswordErr").hide();
        $("#ConfirmPasswordErr").hide();

        $("#Password").val('');
        $("#ConfirmPassword").val('');

        $('#SendCredentials').prop('checked', true);
    }
    else 
    {
        $("#PasswordDiv").hide();
        $("#ConfirmPasswordDiv").hide();
        $("#GeneratePasswordDiv").hide();

        $("#PasswordErr").hide();
        $("#ConfirmPasswordErr").hide();

        $("#Password").val('');
        $("#ConfirmPassword").val('');

        $('#SendCredentials').prop('checked', false);
    }
}
function SaveUserDetails()
{

    var flag            =1;
    var CompanyID       = $("#CompanyID").val();  
    var UserRole        = $("#UserRoleEdit").val();  
    var UserFirstName   = $("#UserFirstName").val();  
    var UserLastName    = $("#UserLastName").val();  
    var UserEmail       = $("#UserEmail").val();  
    var UserTel         = $("#UserTel").val();  
    var UserSkype       = $("#UserSkype").val();  

    var ChangePassword  = $("#ChangePassword").val();

    var Password        = $("#Password").val();  
    var ConfirmPassword = $("#ConfirmPassword").val(); 

    var token           = $("#_token").val();
    var url             = $("#url").val();  

    if(CompanyID=="")
    {
        $("#CompanyIDErr").css('color','red');
        $("#CompanyIDErr").html('Please Select Company Name.');
        $("#CompanyIDErr").show();
        flag =0;
    }
    if(UserRole=="")
    {
        $("#UserRoleEditErr").css('color','red');
        $("#UserRoleEditErr").html('Please Select UserRole.');
        $("#UserRoleEditErr").show();
        flag =0;
    }
    if(UserFirstName=="")
    {
        $("#UserFirstNameErr").css('color','red');
        $("#UserFirstNameErr").html('Please Enter First Name.');
        $("#UserFirstNameErr").show();
        flag =0;
    }
    if(UserEmail=="")
    {
        $("#UserEmailErr").css('color','red');
        $("#UserEmailErr").html('Please Enter E-mail.');
        $("#UserEmailErr").show();
        flag =0;
    }
    else if(UserEmail!="")
    {
        if(!ValidateEmail(UserEmail))
        {
            $("#UserEmailErr").css('color','red');
            $("#UserEmailErr").html('Please Enter Valid E-mail');
            $("#UserEmailErr").show();
            flag=0;
        }
    }
    if(UserTel!="")
    {
        if(!ValidatePhone(UserTel))
        {
            $("#UserTelErr").css('color','red');
            $("#UserTelErr").html('Please Enter Valid Telephone');
            $("#UserTelErr").show();
            flag=0;
        }
    }    
    if($("#ChangePassword").prop('checked'))
    {
        if (Password == "")
        {
            $("#PasswordErr").css("color", 'red');
            $("#PasswordErr").html("Please Enter Password.");
            $("#PasswordErr").show();
            flag = 0;
        }
        else
        {
            if (Password.length < 8)
            {
                $("#PasswordErr").css("color", 'red');
                $("#PasswordErr").html("Password Length Should More Then or Equeal To 8 Digit.");
                $("#PasswordErr").show();
                flag = 0;
            }
            else
            {
                if (!ValidatePwd(Password))
                {
                    $("#PasswordErr").css("color", 'red');
                    $("#PasswordErr").html("Password Should Contain at least 1 Alphabet, 1 Number and 1 Special Character.");
                    $("#PasswordErr").show();
                    flag = 0;
                }
            }
        }
        if (ConfirmPassword == "")
        {
            $("#ConfirmPasswordErr").css("color", 'red');
            $("#ConfirmPasswordErr").html("Please Enter Confirm Password.");
            $("#ConfirmPasswordErr").show();
            flag = 0;
        }
        if (Password != ConfirmPassword)
        {
            $("#ConfirmPasswordErr").css("color", 'red');
            $("#ConfirmPasswordErr").html("Confirm Password Not Match.");
            $("#ConfirmPasswordErr").show();
            flag = 0;
        }
    }
    if(flag ==1)
    {        
        $("#UserSaveForm").submit();
    }
}

function HideErr(id)
{
    $("#"+id).hide();
}
function ValidateEmail(email)
{
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function ValidatePwd(password)
{
    return !!password.match(/^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%* #+=\(\)\^?&])[A-Za-z\d$@$!%* #+=\(\)\^?&]{8,}$/);
}

function ValidatePhone(tel) {
    var reg = /^[0-9-\s\+?\(\)]{10,20}$/;
    return reg.test(tel);
}

function GeneratePassword()
{
   
    var token           = $("#_token").val();
    var url             = $("#url").val(); 

    
    var DataStr         = {'_token':token};
    $.ajax({
        type: "POST",
        url:  url+"/GeneratePassword",
        data: DataStr,
        success: function(pass) 
        {
            $("#Password").val(pass);         
            $("#ConfirmPassword").val(pass);

            $("#PasswordErr").hide();         
            $("#ConfirmPasswordErr").hide();         
        }
    }); 
}
function GeneratePasswordAdd()
{
   
    var token           = $("#_token").val();
    var url             = $("#url").val(); 

    
    var DataStr         = {'_token':token};
    $.ajax({
        type: "POST",
        url:  url+"/GeneratePassword",
        data: DataStr,
        success: function(pass) 
        {
            $("#PasswordAdd").val(pass);         
            $("#ConfirmPasswordAdd").val(pass);

            $("#PasswordAddErr").hide();         
            $("#ConfirmPasswordAddErr").hide();         
        }
    }); 
}
function IsUserEmailExistFunction()
{
    var UserEmail       = $("#UserEmailAdd").val(); 
    var token = $("#_token").val();
    var url = $("#url").val();
    var DataStr = {_token : token,UserEmail:UserEmail};
    var result;
    $.ajax({
            type:"POST",
            url :url+"/IsUserEmailExist",
            data : DataStr,
            async : false,
            success:function(data)
            {
               result = data;
            }
    });
    return result;
}
function AddUserDetails(){

    var flag            =1;
    var UserRole        = $("#UserRoleAdd").val();  
    var CompanyID       = $("#CompanyIDAdd").val();  
    var UserFirstName   = $("#UserFirstNameAdd").val();  
    var UserLastName    = $("#UserLastNameAdd").val();  
    var UserEmail       = $("#UserEmailAdd").val();  
    var UserTel         = $("#UserTelAdd").val();  
    var UserSkype       = $("#UserSkypeAdd").val();  

    var Password        = $("#PasswordAdd").val();  
    var ConfirmPassword = $("#ConfirmPasswordAdd").val(); 

    var token           = $("#_token").val();
    var url             = $("#url").val();  

    if(CompanyID=="")
    {
        $("#CompanyIDAddErr").css('color','red');
        $("#CompanyIDAddErr").html('Please Select Company Name.');
        $("#CompanyIDAddErr").show();
        flag =0;
    }
    if(UserRole=="")
    {
        $("#UserRoleAddErr").css('color','red');
        $("#UserRoleAddErr").html('Please Select UserRole.');
        $("#UserRoleAddErr").show();
        flag =0;
    }
    if(UserFirstName=="")
    {
        $("#UserFirstNameAddErr").css('color','red');
        $("#UserFirstNameAddErr").html('Please Enter First Name.');
        $("#UserFirstNameAddErr").show();
        flag =0;
    }
    if(UserEmail=="")
    {
        $("#UserEmailAddErr").css('color','red');
        $("#UserEmailAddErr").html('Please Enter E-mail.');
        $("#UserEmailAddErr").show();
        flag =0;
    }
    else if(UserEmail!="")
    {
        if(!ValidateEmail(UserEmail))
        {
            $("#UserEmailAddErr").css('color','red');
            $("#UserEmailAddErr").html('Please Enter Valid E-mail');
            $("#UserEmailAddErr").show();
            flag=0;
        }
    }
    if(UserTel!="")
    {
        if(!ValidatePhone(UserTel))
        {
            $("#UserTelAddErr").css('color','red');
            $("#UserTelAddErr").html('Please Enter Valid Telephone');
            $("#UserTelAddErr").show();
            flag=0;
        }
    }    
    if (Password == "")
    {
        $("#PasswordAddErr").css("color", 'red');
        $("#PasswordAddErr").html("Please Enter Password.");
        $("#PasswordAddErr").show();
        flag = 0;
    }
    else
    {
        if (Password.length < 8)
        {
            $("#PasswordAddErr").css("color", 'red');
            $("#PasswordAddErr").html("Password Length Should More Then or Equeal To 8 Digit.");
            $("#PasswordAddErr").show();
            flag = 0;
        }
        else
        {
            if (!ValidatePwd(Password))
            {
                $("#PasswordAddErr").css("color", 'red');
                $("#PasswordAddErr").html("Password Should Contain at least 1 Alphabet, 1 Number and 1 Special Character.");
                $("#PasswordAddErr").show();
                flag = 0;
            }
        }
    }
    if (ConfirmPassword == "")
    {
        $("#ConfirmPasswordAddErr").css("color", 'red');
        $("#ConfirmPasswordAddErr").html("Please Enter Confirm Password.");
        $("#ConfirmPasswordAddErr").show();
        flag = 0;
    }
    if (Password != ConfirmPassword)
    {
        $("#ConfirmPasswordAddErr").css("color", 'red');
        $("#ConfirmPasswordAddErr").html("Confirm Password Not Match.");
        $("#ConfirmPasswordAddErr").show();
        flag = 0;
    }
    if(flag ==1)
    {        
        
        if(UserEmail!='')
        {
            var IsUserEmailExist = IsUserEmailExistFunction();
            if(IsUserEmailExist==true)
            {
                $("#UserEmailAddErr").css('color','red');
                $("#UserEmailAddErr").html('Same E-mail Already Exist.');
                $("#UserEmailAddErr").show();
                flags=0;
            }
            else
            {
                $("#UserAddForm").submit();
            }
        }
    }
}


function GetCompanyUsingShwowInactiveAndTest()
{
    var token = $("#_token").val();
    var url = $("#url").val();
    var ShowInactive = '0';
    var ShowTest = '0';
    if ($("#ShowInactive").prop('checked') == true) {
        ShowInactive = '1';
    }
    if ($("#ShowTest").prop('checked') == true) {
        ShowTest = '1';
    }
    var DataStr = {'_token': token, 'ShowInactive': ShowInactive, 'ShowTest': ShowTest};
    $.ajax({
        type: "POST",
        url: url + "/GetCompanyUsingShwowInactiveAndTest",
        data: DataStr,
        success: function (data)
        {
            $("#Company").html(data);        }
    });
}
//////////////////////////
function AddUserModal()
{
    $("#UserRoleAddErr").hide();  
    $("#CompanyIDAddErr").hide();  
    $("#UserFirstNameAddErr").hide();  
    $("#UserLastNameAddErr").hide();  
    $("#UserEmailAddErr").hide();  
    $("#UserTelAddErr").hide();  
    $("#UserSkypeAddErr").hide();  
    $("#PasswordAddErr").hide();  
    $("#ConfirmPasswordAddErr").hide(); 
    
    $("#AddUserModal").modal('show');
}
function GetUserDetails(UserID)
{
    var token = $("#_token").val();
    var url = $("#url").val();
    
    var DataStr = {'_token': token, 'UserID': UserID};
    $.ajax({
        type: "POST",
        url: url + "/GetUserDetails",
        data: DataStr,
        success: function (data)
        {
            $("#EditUserDiv").html(data);     
            $("#EditUserModal").modal('show');       
        }
    });
}