
$("#comapnyTab").on("click", function() 
{
  $("#UserOne").collapse('hide');  
  $("#VendorOne").collapse('hide');
  $("#BuyerOne").collapse('hide');
  $("#CompanyOne").collapse('show');

  $("#CompanyOne").addClass('show');
  $("#UserOne").removeClass('show');  
  $("#VendorOne").removeClass('show');
  $("#BuyerOne").removeClass('show');
  
  $("#comapnyTab").removeClass('tabStyleHiden');
  $("#comapnyTab").addClass('tabStyle');

  $("#userTab").addClass('tabStyleHiden');
  $("#userTab").removeClass('tabStyle');

  $("#vendorTab").addClass('tabStyleHiden');
  $("#vendorTab").removeClass('tabStyle');

  $("#buyerTab").addClass('tabStyleHiden');
  $("#buyerTab").removeClass('tabStyle');


});

$("#userTab").on("click", function() 
{
  $("#CompanyOne").collapse('hide');
  $("#VendorOne").collapse('hide');
  $("#BuyerOne").collapse('hide');
  $("#UserOne").collapse('show');  

  $("#CompanyOne").removeClass('show');
  $("#UserOne").addClass('show');  
  $("#VendorOne").removeClass('show');
  $("#BuyerOne").removeClass('show');

  $("#comapnyTab").addClass('tabStyleHiden');
  $("#comapnyTab").removeClass('tabStyle');

  $("#userTab").removeClass('tabStyleHiden');
  $("#userTab").addClass('tabStyle');

  $("#vendorTab").addClass('tabStyleHiden');
  $("#vendorTab").removeClass('tabStyle');

  $("#buyerTab").addClass('tabStyleHiden');
  $("#buyerTab").removeClass('tabStyle');

});
$("#vendorTab").on("click", function() 
{
  $("#CompanyOne").collapse('hide');
  $("#UserOne").collapse('hide');  
  $("#BuyerOne").collapse('hide');
  $("#VendorOne").collapse('show');

  $("#CompanyOne").removeClass('show');
  $("#UserOne").removeClass('show');  
  $("#VendorOne").addClass('show');
  $("#BuyerOne").removeClass('show');

  $("#comapnyTab").addClass('tabStyleHiden');
  $("#comapnyTab").removeClass('tabStyle');

  $("#userTab").addClass('tabStyleHiden');
  $("#userTab").removeClass('tabStyle');

  $("#vendorTab").removeClass('tabStyleHiden');
  $("#vendorTab").addClass('tabStyle');

  $("#buyerTab").addClass('tabStyleHiden');
  $("#buyerTab").removeClass('tabStyle');

});
$("#buyerTab").on("click", function() 
{
  $("#CompanyOne").collapse('hide');
  $("#UserOne").collapse('hide');  
  $("#VendorOne").collapse('hide');
  $("#BuyerOne").collapse('show');

  $("#CompanyOne").removeClass('show');
  $("#UserOne").removeClass('show');  
  $("#VendorOne").removeClass('show');
  $("#BuyerOne").addClass('show');

  $("#comapnyTab").addClass('tabStyleHiden');
  $("#comapnyTab").removeClass('tabStyle');

  $("#userTab").addClass('tabStyleHiden');
  $("#userTab").removeClass('tabStyle');

  $("#vendorTab").addClass('tabStyleHiden');
  $("#vendorTab").removeClass('tabStyle');

  $("#buyerTab").removeClass('tabStyleHiden');
  $("#buyerTab").addClass('tabStyle');

});
function HideErr(id)
{
    $("#"+id).hide();
} 
$("#CompanyCountry").change(function(){
    var CompanyCountry        =$("#CompanyCountry").val();
    if(CompanyCountry!="US")
    {
        $("#StateDiv").hide();
        $("#CompanyZipPostCodeLable").text("Postal Code");
        $('#CompanyZipPostCode').attr("placeholder", "Postal Code");
    }
    else
    {
        $("#StateDiv").show();
        $("#CompanyZipPostCodeLable").text("Zip Code");
        $('#CompanyZipPostCode').attr("placeholder", "Zip Code");
    }
});

$("#CompanyZipPostCode").keyup(function(){
    var CompanyCountry      = $("#CompanyCountry").val();
    var CompanyZipPostCode  = $("#CompanyZipPostCode").val();
    var token           = $("#_token").val();
    var url             = $("#url").val();
    if(CompanyZipPostCode.length>=5 && CompanyCountry=="US")
    {
        var DataStr         = {'_token':token,'CompanyZipPostCode':CompanyZipPostCode};
        $.ajax({
            type: "POST",
            url:  url+"/GetCityZipCodeState",
            data: DataStr,
            dataType: "json",
            success: function(data)
            {
                if(data.Status==1)
                {
                    $("#CompanyStateCounty").val(data.state);
                    $("#CompanyCity").val(data.city);
                }
                else
                {
                    $("#CompanyStateCounty").val('');
                    $("#CompanyCity").val('');
                }
                
            }
        });
    }
});
function DeleteCompanyUploadedFile(CompanyID, ID)
{
    var token           = $("#_token").val();
    var url             = $("#url").val();
    var DataStr         = {'_token':token,'CompanyID':CompanyID,'ID':ID};
    $.ajax({
        type: "POST",
        url:  url+"/DeleteCompanyUploadedFile",
        data: DataStr,
        dataType: "json",
        success: function(data)
        {
            if(data.Status==1)
            {
                $("#UploadedFileTR_"+ID).remove();
                $("#UploadFileMessage").html(data.Message);
            } 
            else
            {
                $("#UploadFileMessage").html(data.Message);
            }        
        }
    });
}
$("#UpdateCompanyBtn").click(function(){
    var flag                    = 1;
    var CompanyName             = $("#CompanyName").val();    
    var CompanyCountry          = $("#CompanyCountry").val();
    var CompanyZipPostCode      = $("#CompanyZipPostCode").val();

    if(CompanyName=="")
    {
        $("#CompanyNameErr").css("color",'red');
        $("#CompanyNameErr").html("Please Enter Company Name.");
        $("#CompanyNameErr").show();
        flag=0;
    }
    if(CompanyCountry=="")
    {
        $("#CompanyCountryErr").css("color",'red');
        $("#CompanyCountryErr").html("Please Enter Country.");
        $("#CompanyCountryErr").show();
        flag=0;
    }
    if(CompanyZipPostCode!="")
    {
        if(CompanyZipPostCode.length<4 || CompanyZipPostCode.length>10)
        {
            $("#CompanyZipPostCodeErr").css("color",'red');
            $("#CompanyZipPostCodeErr").html("Please Enter 5 Digit Valid Zip Code.");
            $("#CompanyZipPostCodeErr").show();
            flag=0;
        }
    }
    if(flag==1)
    {
        $("#UpdateCompanyForm").submit();
    }
});
function GeneratePassword()
{  
    var token           = $("#_token").val();
    var url             = $("#url").val(); 
  
    var DataStr         = {'_token':token};
    $.ajax({
        type: "POST",
        url:  url+"/GeneratePassword",
        data: DataStr,
        success: function(pass) 
        {
            $("#Password").val(pass);         
            $("#ConfirmPassword").val(pass);

            $("#PasswordErr").hide();         
            $("#ConfirmPasswordErr").hide();         
        }
    }); 
}
function IsUserEmailExistFunction()
{
    var UserEmail       = $("#UserEmail").val(); 
    var token = $("#_token").val();
    var url = $("#url").val();
    var DataStr = {_token : token,UserEmail:UserEmail};
    var result;
    $.ajax({
            type:"POST",
            url :url+"/IsUserEmailExist",
            data : DataStr,
            async : false,
            success:function(data)
            {
               result = data;
            }
    });
    return result;
}
function ValidateEmail(email)
{
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function ValidatePwd(password)
{
    return !!password.match(/^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%* #+=\(\)\^?&])[A-Za-z\d$@$!%* #+=\(\)\^?&]{8,}$/);
}

function ValidatePhone(tel) {
    var reg = /^[0-9-\s\+?\(\)]{10,20}$/;
    return reg.test(tel);
}
//User JS/////////////////////////////////////////////
$(document).ready(function(){
  GetUserListFromCompany(1);
  GetVendorListFromCompany(1);
  GetBuyerListFromCompany(1)
  GetCampaignDetailsAddVendor();
  GetCampaignDetailsAddBuyer();
});
function SearchUserList(){
  GetUserListFromCompany(1);
}
function PaginationUser(page)
{
    GetUserListFromCompany(page);
}
function GetUserListFromCompany(page)
{
    var numofrecords    = $('#numofrecordsUser').val();
    var loading=$("#loading").val();
    if(numofrecords==10)
    {
        $("#dataTable_wrapper").html('<center><img src="'+loading+'" style="width: 200px"><input type="hidden" name="numofrecords" id="numofrecordsUser" value="10"></center>'); 
    }
    else
    {
        $("#dataTable_wrapper").html('<center><img src="'+loading+'" style="width: 200px"></center>'); 
    }
    var token           = $("#_token").val();
    var url             = $("#url").val(); 

    var CompanyID       = $("#CompanyIDForAllSearch").val();
    var FirstName       = $("#FirstNameUser").val();
    var LastName        = $("#LastNameUser").val();
    var Email           = $("#EmailUser").val();
    var Phone           = $("#PhoneUser").val();
    var UserStatus      = $("#UserStatusUser").val();
    var UserRole        = $("#UserRoleUser").val();
    
    var DataStr         = {'_token':token,'page':page,'numofrecords':numofrecords,'CompanyID':CompanyID,'FirstName':FirstName,'LastName':LastName,'Email':Email,'Phone':Phone, 'UserStatus':UserStatus,'UserRole':UserRole};
    $.ajax({
        type: "POST",
        url:  url+"/GetUserListFromCompany",
        data: DataStr,
        success: function(data) 
        {
            $("#dataTable_wrapper_User").html(data);         
        }
    }); 
}
function AddUserModal()
{
    $("#UserRoleAddUserErr").hide();  
    $("#CompanyIDAddUserErr").hide();  
    $("#UserFirstNameAddUserErr").hide();  
    $("#UserLastNameAddUserErr").hide();  
    $("#UserEmailAddUserErr").hide();  
    $("#UserTelAddUserErr").hide();  
    $("#UserSkypeAddUserErr").hide();  
    $("#PasswordAddUserErr").hide();  
    $("#ConfirmPasswordAddUserErr").hide(); 
    
    $("#AddUserModal").modal('show');
}
function GeneratePasswordAdd()
{
   
    var token           = $("#_token").val();
    var url             = $("#url").val(); 

    
    var DataStr         = {'_token':token};
    $.ajax({
        type: "POST",
        url:  url+"/GeneratePassword",
        data: DataStr,
        success: function(pass) 
        {
            $("#PasswordAddUser").val(pass);         
            $("#ConfirmPasswordAddUser").val(pass);

            $("#PasswordAddUserErr").hide();         
            $("#ConfirmPasswordAddUserErr").hide();         
        }
    }); 
}
function AddUserDetails(){

    var flag            =1;
    var UserRole        = $("#UserRoleAddUser").val();  
    var UserFirstName   = $("#UserFirstNameAddUser").val();  
    var UserLastName    = $("#UserLastNameAddUser").val();  
    var UserEmail       = $("#UserEmailAddUser").val();  
    var UserTel         = $("#UserTelAddUser").val();  
    var UserSkype       = $("#UserSkypeAddUser").val();  

    var UPassword       = $("#PasswordAddUser").val();  
    var ConfirmPassword = $("#ConfirmPasswordAddUser").val(); 

    var token           = $("#_token").val();
    var url             = $("#url").val();  

    if(UserRole=="")
    {
        $("#UserRoleAddUserErr").css('color','red');
        $("#UserRoleAddUserErr").html('Please Select UserRole.');
        $("#UserRoleAddUserErr").show();
        flag =0;
    }
    if(UserFirstName=="")
    {
        $("#UserFirstNameAddUserErr").css('color','red');
        $("#UserFirstNameAddUserErr").html('Please Enter First Name.');
        $("#UserFirstNameAddUserErr").show();
        flag =0;
    }
    if(UserEmail=="")
    {
        $("#UserEmailAddUserErr").css('color','red');
        $("#UserEmailAddUserErr").html('Please Enter E-mail.');
        $("#UserEmailAddUserErr").show();
        flag =0;
    }
    else if(UserEmail!="")
    {
        if(!ValidateEmail(UserEmail))
        {
            $("#UserEmailAddUserErr").css('color','red');
            $("#UserEmailAddUserErr").html('Please Enter Valid E-mail');
            $("#UserEmailAddUserErr").show();
            flag=0;
        }
    }
    if(UserTel!="")
    {
        if(!ValidatePhone(UserTel))
        {
            $("#UserTelAddUserErr").css('color','red');
            $("#UserTelAddUserErr").html('Please Enter Valid Telephone');
            $("#UserTelAddUserErr").show();
            flag=0;
        }
    }    
    if (UPassword == "")
    {
        $("#PasswordAddUserErr").css("color", 'red');
        $("#PasswordAddUserErr").html("Please Enter Password.");
        $("#PasswordAddUserErr").show();
        flag = 0;
    }
    else
    {
        if (UPassword.length < 8)
        {
            $("#PasswordAddUserErr").css("color", 'red');
            $("#PasswordAddUserErr").html("Password Length Should More Then or Equeal To 8 Digit.");
            $("#PasswordAddUserErr").show();
            flag = 0;
        }
        else
        {
            if (!ValidatePwd(UPassword))
            {
                $("#PasswordAddUserErr").css("color", 'red');
                $("#PasswordAddUserErr").html("Password Should Contain at least 1 Alphabet, 1 Number and 1 Special Character.");
                $("#PasswordAddUserErr").show();
                flag = 0;
            }
        }
    }
    if (ConfirmPassword == "")
    {
        $("#ConfirmPasswordAddUserErr").css("color", 'red');
        $("#ConfirmPasswordAddUserErr").html("Please Enter Confirm Password.");
        $("#ConfirmPasswordAddUserErr").show();
        flag = 0;
    }
    if (UPassword != ConfirmPassword)
    {
        $("#ConfirmPasswordAddUserErr").css("color", 'red');
        $("#ConfirmPasswordAddUserErr").html("Confirm Password Not Match.");
        $("#ConfirmPasswordAddUserErr").show();
        flag = 0;
    }
    if(flag ==1)
    {        
        
        if(UserEmail!='')
        {
            var IsUserEmailExist = IsUserEmailExistFunction();
            if(IsUserEmailExist==true)
            {
                $("#UserEmailAddUserErr").css('color','red');
                $("#UserEmailAddUserErr").html('Same E-mail Already Exist.');
                $("#UserEmailAddUserErr").show();
                flags=0;
            }
            else
            {
                $("#UserAddForm").submit();
            }
        }
    }
}
function IsUserEmailExistFunction()
{
    var UserEmail       = $("#UserEmailAddUser").val(); 
    var token = $("#_token").val();
    var url = $("#url").val();
    var DataStr = {_token : token,UserEmail:UserEmail};
    var result;
    $.ajax({
            type:"POST",
            url :url+"/IsUserEmailExist",
            data : DataStr,
            async : false,
            success:function(data)
            {
               result = data;
            }
    });
    return result;
}
function GetUserDetailsFromCompany(UserID)
{
    var token = $("#_token").val();
    var url = $("#url").val();
    
    var DataStr = {'_token': token, 'UserID': UserID};
    $.ajax({
        type: "POST",
        url: url + "/GetUserDetailsFromCompany",
        data: DataStr,
        success: function (data)
        {
            $("#EditUserDiv").html(data);     
            $("#EditUserModal").modal('show');       
        }
    });
}
function ChangePasswordShowHide()
{
    if ($("#ChangePassword").prop('checked')) 
    {
        $("#PasswordDiv").show();
        $("#ConfirmPasswordDiv").show();
        $("#GeneratePasswordDiv").show();

        $("#PasswordErr").hide();
        $("#ConfirmPasswordErr").hide();

        $("#Password").val('');
        $("#ConfirmPassword").val('');

        $('#SendCredentials').prop('checked', true);
    }
    else 
    {
        $("#PasswordDiv").hide();
        $("#ConfirmPasswordDiv").hide();
        $("#GeneratePasswordDiv").hide();

        $("#PasswordErr").hide();
        $("#ConfirmPasswordErr").hide();

        $("#Password").val('');
        $("#ConfirmPassword").val('');

        $('#SendCredentials').prop('checked', false);
    }
}
function SaveUserDetails()
{

    var flag            =1;
    var CompanyID       = $("#CompanyID").val();  
    var UserRole        = $("#UserRoleEdit").val();  
    var UserFirstName   = $("#UserFirstName").val();  
    var UserLastName    = $("#UserLastName").val();  
    var UserEmail       = $("#UserEmail").val();  
    var UserTel         = $("#UserTel").val();  
    var UserSkype       = $("#UserSkype").val();  

    var ChangePassword  = $("#ChangePassword").val();

    var Password        = $("#Password").val();  
    var ConfirmPassword = $("#ConfirmPassword").val(); 

    var token           = $("#_token").val();
    var url             = $("#url").val();  

    if(CompanyID=="")
    {
        $("#CompanyIDErr").css('color','red');
        $("#CompanyIDErr").html('Please Select Company Name.');
        $("#CompanyIDErr").show();
        flag =0;
    }
    if(UserRole=="")
    {
        $("#UserRoleEditErr").css('color','red');
        $("#UserRoleEditErr").html('Please Select UserRole.');
        $("#UserRoleEditErr").show();
        flag =0;
    }
    if(UserFirstName=="")
    {
        $("#UserFirstNameErr").css('color','red');
        $("#UserFirstNameErr").html('Please Enter First Name.');
        $("#UserFirstNameErr").show();
        flag =0;
    }
    if(UserEmail=="")
    {
        $("#UserEmailErr").css('color','red');
        $("#UserEmailErr").html('Please Enter E-mail.');
        $("#UserEmailErr").show();
        flag =0;
    }
    else if(UserEmail!="")
    {
        if(!ValidateEmail(UserEmail))
        {
            $("#UserEmailErr").css('color','red');
            $("#UserEmailErr").html('Please Enter Valid E-mail');
            $("#UserEmailErr").show();
            flag=0;
        }
    }
    if(UserTel!="")
    {
        if(!ValidatePhone(UserTel))
        {
            $("#UserTelErr").css('color','red');
            $("#UserTelErr").html('Please Enter Valid Telephone');
            $("#UserTelErr").show();
            flag=0;
        }
    }    
    if($("#ChangePassword").prop('checked'))
    {
        if (Password == "")
        {
            $("#PasswordErr").css("color", 'red');
            $("#PasswordErr").html("Please Enter Password.");
            $("#PasswordErr").show();
            flag = 0;
        }
        else
        {
            if (Password.length < 8)
            {
                $("#PasswordErr").css("color", 'red');
                $("#PasswordErr").html("Password Length Should More Then or Equeal To 8 Digit.");
                $("#PasswordErr").show();
                flag = 0;
            }
            else
            {
                if (!ValidatePwd(Password))
                {
                    $("#PasswordErr").css("color", 'red');
                    $("#PasswordErr").html("Password Should Contain at least 1 Alphabet, 1 Number and 1 Special Character.");
                    $("#PasswordErr").show();
                    flag = 0;
                }
            }
        }
        if (ConfirmPassword == "")
        {
            $("#ConfirmPasswordErr").css("color", 'red');
            $("#ConfirmPasswordErr").html("Please Enter Confirm Password.");
            $("#ConfirmPasswordErr").show();
            flag = 0;
        }
        if (Password != ConfirmPassword)
        {
            $("#ConfirmPasswordErr").css("color", 'red');
            $("#ConfirmPasswordErr").html("Confirm Password Not Match.");
            $("#ConfirmPasswordErr").show();
            flag = 0;
        }
    }
    if(flag ==1)
    {        
        $("#UserSaveForm").submit();
    }
}
function ChangeUserStatus(UID, Status)
{
    
    var token           = $("#_token").val();
    var url             = $("#url").val(); 
    
    var DataStr         = {'_token':token,'UID':UID,'Status':Status};
    $.ajax({
        type: "POST",
        url:  url+"/ChangeUserStatus",
        data: DataStr,
        dataType: 'json',
        success: function(data) 
        {
            $("#UserMessage").show(); 
            if(data.Status==1)
            {
                $("#UserMessage").html(data.Msg);                         
            }
            else
            {                
                $("#UserMessage").html(data.Msg);                         
            }
            setInterval(function(){ $('#UserMessage').fadeOut(); }, 5000);
        }
    });
}
//Vendor JS/////////////////////////////////////////////
function SearchVendorList(){
  GetVendorListFromCompany(1);
}
function GetCampaignDetailsEdit()
{
    var CampaignID = $("#CampaignEditVendor").val();
    var token = $("#_token").val();
    var url = $("#url").val();
    var DataStr = {_token : token,CampaignID:CampaignID};
    var result;
    $.ajax({
            type:"POST",
            url :url+"/GetCampaignDetails",
            data : DataStr,
            dataType : 'json',
            success:function(data)
            {
                if(data.CampaignType == 'DirectPost')
                { 
                    $("#CampaignTypeEdit").val(data.CampaignType);
                    $("#MaxTimeOutInputEditVendor").css('display','block');
                    $("#MaxPingTimeOutInputEditVendor").css('display','none');
                    $("#MaxPostTimeOutInputEditVendor").css('display','none');
                } 
                else if(data.CampaignType == 'PingPost')
                {
                    $("#CampaignTypeEdit").val(data.CampaignType);
                    $("#MaxTimeOutInputEditVendor").css('display','none');
                    $("#MaxPingTimeOutInputEditVendor").css('display','block');
                    $("#MaxPostTimeOutInputEditVendor").css('display','block');
                }
            }
    });
    return result;
}
function GetCampaignDetailsAddVendor()
{
    var CampaignID = $("#CampaignAddVendor").val();
    var token = $("#_token").val();
    var url = $("#url").val();
    var DataStr = {_token : token,CampaignID:CampaignID};
    var result;
    $.ajax({
            type:"POST",
            url :url+"/GetCampaignDetails",
            data : DataStr,
            dataType : 'json',
            success:function(data)
            {
                if(data.CampaignType == 'DirectPost')
                { 
                    $("#CampaignTypeAddVendor").val(data.CampaignType);
                    $("#MaxTimeOutInputAddVendor").css('display','block');
                    $("#MaxPingTimeOutInputAddVendor").css('display','none');
                    $("#MaxPostTimeOutInputAddVendor").css('display','none');
                } 
                else if(data.CampaignType == 'PingPost')
                {
                    $("#CampaignTypeAddVendor").val(data.CampaignType);
                    $("#MaxTimeOutInputAddVendor").css('display','none');
                    $("#MaxPingTimeOutInputAddVendor").css('display','block');
                    $("#MaxPostTimeOutInputAddVendor").css('display','block');
                }
            }
    });
    return result;
}
function AddVendorModal()
{
    $("#CampaignAddErr").hide();  
    $("#CompanyAddErr").hide();  
    $("#AdminLabelAddErr").hide();  
    $("#PartnerLabelAddErr").hide();  
    $("#PayoutCalculationAddErr").hide();  
    $("#RevshareAddErr").hide();  
    $("#FixedPriceAddErr").hide();  
    $("#DailyCapAddErr").hide();  
    $("#TotalCapAddErr").hide();  
    $("#MaxTimeOutAddErr").hide(); 
    $("#MaxPingTimeOutAddErr").hide(); 
    $("#MaxPostTimeOutAddErr").hide();
    
    $("#AddVendorModal").modal('show');
}
function GetVendorListFromCompany(page)
{
    var numofrecords    = $('#numofrecordsVendor').val();

    var loading=$("#loading").val();
    if(numofrecords==10)
    {
        $("#dataTable_wrapper_Vendor").html('<center><img src="'+loading+'" style="width: 200px"><input type="hidden" name="numofrecords" id="numofrecordsVendor" value="10"></center>'); 
    }
    else
    {
        $("#dataTable_wrapper_Vendor").html('<center><img src="'+loading+'" style="width: 200px"></center>'); 
    }
    var token           = $("#_token").val();
    var url             = $("#url").val();     
    var ShowInactive    = 0;
    var ShowTest        = 0;
    var CompanyID       = $("#CompanyIDForAllSearch").val();
    var Campaign        = $("#CampaignVendor").val();
    var VendorID        = $("#VendorIDVendor").val();
    var VendorName      = $("#VendorNameVendor").val();
    var VendorStatus    = $("#VendorStatusVendor").val();
    
    var DataStr         = {'_token':token,'page':page,'numofrecords':numofrecords,'Campaign':Campaign,'VendorName':VendorName,'CompanyID':CompanyID,'VendorID':VendorID,'VendorStatus':VendorStatus,'ShowInactive':ShowInactive,'ShowTest':ShowTest};
    $.ajax({
        type: "POST",
        url:  url+"/GetVendorListFromCompany",
        data: DataStr,
        success: function(data) 
        {
            $("#dataTable_wrapper_Vendor").html(data);         
        }
    }); 
}
function AddVendorDetails()
{
    var flag            = 1;
    var CampaignType    = $("#CampaignTypeVendor").val();  
    var Campaign        = $("#CampaignAddVendor").val(); 
    var AdminLabel      = $("#AdminLabelAddVendor").val();  
    var PartnerLabel    = $("#PartnerLabelAddVendor").val();  
    var PayoutCalculation = $("#PayoutCalculationAddVendor").val();  
    var RevsharePercent = $("#RevshareAddVendor").val();  
    var FixedPrice      = $("#FixedPriceAddVendor").val();  
    var DailyCap        = $("#DailyCapAddVendor").val();  
    var TotalCap        = $("#TotalCapAddVendor").val();  
    var MaxTimeOut      = $("#MaxTimeOutAddVendor").val(); 
    var MaxPingTimeOut  = $("#MaxPingTimeOutAddVendor").val(); 
    var MaxPostTimeOut  = $("#MaxPostTimeOutAddVendor").val(); 
    var token           = $("#_token").val();
    var url             = $("#url").val();      

    if(Campaign=="")
    {
        $("#CampaignAddVendorErr").css('color','red');
        $("#CampaignAddVendorErr").html('Please Select Campaign.');
        $("#CampaignAddVendorErr").show();
        flag =0;
    }
    if(PartnerLabel=="")
    {
        $("#PartnerLabelAddVendorErr").css('color','red');
        $("#PartnerLabelAddVendorErr").html('Please Enter Partner Label.');
        $("#PartnerLabelAddVendorErr").show();
        flag =0;
    }
    if(AdminLabel=="")
    {
        $("#AdminLabelAddVendorErr").css('color','red');
        $("#AdminLabelAddVendorErr").html('Please Enter Admin Label.');
        $("#AdminLabelAddVendorErr").show();
        flag =0;
    }               
    if(PayoutCalculation=="")
    {
        $("#PayoutCalculationVendorErr").css('color','red');
        $("#PayoutCalculationVendorErr").html('Please Enter Payout Calculation Type.');
        $("#PayoutCalculationVendorErr").show();
        flag =0;
    }
    if(PayoutCalculation=="RevShare")
    {
        if(RevsharePercent=="")
        {
            $("#RevshareAddVendorErr").css('color','red');
            $("#RevshareAddVendorErr").html('Please Enter Revshare Percent.');
            $("#RevshareAddVendorErr").show();
            flag =0;
        }
    }
    if(PayoutCalculation=="FixedPrice")
    {
        if(FixedPrice=="" )
        {
            $("#FixedPriceAddVendorErr").css('color','red');
            $("#FixedPriceAddVendorErr").html('Please Enter Fixed Price.');
            $("#FixedPriceAddVendorErr").show();
            flag =0;
        }
    }
    if(DailyCap=="")
    {
        $("#DailyCapAddVendorErr").css('color','red');
        $("#DailyCapAddVendorErr").html('Required.');
        $("#DailyCapAddVendorErr").show();
        flag =0;
    }
    if(TotalCap=="")
    {
        $("#TotalCapAddVendorErr").css('color','red');
        $("#TotalCapAddVendorErr").html('Required.');
        $("#TotalCapAddVendorErr").show();
        flag =0;
    }
    if(CampaignType=="DirectPost")
    {
        if(MaxTimeOut=="")
        {
            $("#MaxTimeOutAddVendorErr").css('color','red');
            $("#MaxTimeOutAddVendorErr").html('Please Enter Max Time Out Values.');
            $("#MaxTimeOutAddVendorErr").show();
            flag =0;
        }
        else if(MaxTimeOut!="")
        {
            if(isNaN(MaxTimeOut))
            {
                $("#MaxTimeOutAddVendorErr").css('color','red');
                $("#MaxTimeOutAddVendorErr").html('Max Time Out Values Should Be Numeric.');
                $("#MaxTimeOutAddVendorErr").show();
                flag =0;
            }
        }
    }
    if(CampaignType=="PingPost")
    {
        if(MaxPingTimeOut=="")
        {
            $("#MaxPingTimeOutAddVendorErr").css('color','red');
            $("#MaxPingTimeOutAddVendorErr").html('Please Enter Max Time Out Values.');
            $("#MaxPingTimeOutAddVendorErr").show();
            flag =0;
        }
        else if(MaxPingTimeOut!="")
        {
            if(isNaN(MaxPingTimeOut))
            {
                $("#MaxPingTimeOutAddVendorErr").css('color','red');
                $("#MaxPingTimeOutAddVendorErr").html('Max Time Out Values Should Be Numeric.');
                $("#MaxPingTimeOutAddVendorErr").show();
                flag =0;
            }
        }
        if(MaxPostTimeOut=="")
        {
            $("#MaxPostTimeOutAddVendorErr").css('color','red');
            $("#MaxPostTimeOutAddVendorErr").html('Please Enter Max Time Out Values.');
            $("#MaxPostTimeOutAddVendorErr").show();
            flag =0;
        }
        else if(MaxPostTimeOut!="")
        {
            if(isNaN(MaxPostTimeOut))
            {
                $("#MaxPostTimeOutAddVendorErr").css('color','red');
                $("#MaxPostTimeOutAddVendorErr").html('Max Time Out Values Should Be Numeric.');
                $("#MaxPostTimeOutAddVendorErr").show();
                flag =0;
            }
        }              
    }
    if(flag ==1)
    {
        if(AdminLabel!='')
        {
            var IsVendorNameExist = IsVendorNameExistFunction();
            if(IsVendorNameExist==true)
            {
                $("#AdminLabelAddVendorErr").css('color','red');
                $("#AdminLabelAddVendorErr").html('Same Admin Label Already Exist.');
                $("#AdminLabelAddVendorErr").show();
                flag=0;
            }
            else
            {
                $("#VendorAddForm").submit();
            }
        }
        
    }
      
}
function IsVendorNameExistFunction()
{
    var VendorName       = $("#AdminLabelAddVendor").val(); 
    var token = $("#_token").val();
    var url = $("#url").val();
    var DataStr = {_token : token,'VendorName':VendorName};
    var result;
    $.ajax({
            type:"POST",
            url :url+"/IsVendorNameExist",
            data : DataStr,
            async : false,
            success:function(data)
            {
               result = data;
            }
    });
    return result;
}
function updateTextBoxAddVendor(token_value)
{        
    var $txt = jQuery("#TrackingPixelCodeAddVendor");
    var caretPos = $txt[0].selectionStart;
    var textAreaTxt = $txt.val();
    var txtToAdd = "[--" + token_value.value + "--]";
    $txt.val(textAreaTxt.substring(0, caretPos) + txtToAdd + textAreaTxt.substring(caretPos) );
    //$('#tracking_pixel_code').append(token_value.value +"="+ "[--" + token_value.value + "--]");
}
function updateTextBoxEditVendor(token_value)
{        
    var $txt = jQuery("#TrackingPixelCodeEditVendor");
    var caretPos = $txt[0].selectionStart;
    var textAreaTxt = $txt.val();
    var txtToAdd = "[--" + token_value.value + "--]";
    $txt.val(textAreaTxt.substring(0, caretPos) + txtToAdd + textAreaTxt.substring(caretPos) );
    //$('#tracking_pixel_code').append(token_value.value +"="+ "[--" + token_value.value + "--]");
}

$("#PayoutCalculationAddVendor").change(function(){

     var PayoutCalculation  = $("#PayoutCalculationAddVendor").val();

     if(PayoutCalculation=="RevShare")
     {
        $("#RevsharePercentInputAdd").show();
        $("#FixedPriceInputAdd").hide();
        $("#TierPriceInputAdd").hide();
     }
     else if(PayoutCalculation=="MinPrice")
     {
        $("#RevsharePercentInputAdd").show();
        $("#FixedPriceInputAdd").hide();
        $("#TierPriceInputAdd").hide();
     }
     else if(PayoutCalculation=="TierPrice")
     {
       $("#RevsharePercentInputAdd").hide();
        $("#FixedPriceInputAdd").hide();
        $("#TierPriceInputAdd").show();
     }
     else if(PayoutCalculation=="FixedPrice")
     {
        $("#RevsharePercentInputAdd").hide();
        $("#FixedPriceInputAdd").show();
        $("#TierPriceInputAdd").hide();
     }
     else if(PayoutCalculation=="BucketPrice" || PayoutCalculation=="BucketSystemBySubID")
     {
        $("#RevsharePercentInputAdd").show();
        $("#FixedPriceInputAdd").show();
        $("#TierPriceInputAdd").hide();
     }
});
function PayoutCalculationChange(){

     var PayoutCalculation  = $("#PayoutCalculationEditVendor").val();

     if(PayoutCalculation=="RevShare")
     {
        $("#RevsharePercentInputEditVendor").show();
        $("#FixedPriceInputEditVendor").hide();
        $("#TierPriceInputEditVendor").hide();
     }
     else if(PayoutCalculation=="MinPrice")
     {
        $("#RevsharePercentInputEditVendor").show();
        $("#FixedPriceInputEditVendor").hide();
        $("#TierPriceInputEditVendor").hide();
     }
     else if(PayoutCalculation=="TierPrice")
     {
       $("#RevsharePercentInputEditVendor").hide();
        $("#FixedPriceInputEditVendor").hide();
        $("#TierPriceInputEditVendor").show();
     }
     else if(PayoutCalculation=="FixedPrice")
     {
        $("#RevsharePercentInputEditVendor").hide();
        $("#FixedPriceInputEditVendor").show();
        $("#TierPriceInputEditVendor").hide();
     }
     else if(PayoutCalculation=="BucketPrice" || PayoutCalculation=="BucketSystemBySubID")
     {
        $("#RevsharePercentInputEditVendor").show();
        $("#FixedPriceInputEditVendor").show();
        $("#TierPriceInputEditVendor").hide();
     }
}
function CopyAdminLableAddVendor(){
    var AdminLabel= $("#AdminLabelAddVendor").val();
    $("#PartnerLabelAddVendor").val(AdminLabel);

    $("#AdminLabelAddVendorErr").hide();
    $("#PartnerLabelAddVendorErr").hide();
}
function CopyAdminLableEditVendorLabel(){
    
    var AdminLabel= $("#AdminLabelEditVendor").val();
    $("#PartnerLabelEditVendor").val(AdminLabel);

    $("#AdminLabelEditVendorErr").hide();
    $("#PartnerLabelEditVendorErr").hide();
}
function GetVendorDetailsFromCompany(VendorID)
{
    var token = $("#_token").val();
    var url = $("#url").val();
    
    var DataStr = {'_token': token, 'VendorID': VendorID};
    $.ajax({
        type: "POST",
        url: url + "/GetVendorDetailsFromCompany",
        data: DataStr,
        success: function (data)
        {
            $("#EditVendorDiv").html(data);     
            $("#EditVendorModal").modal('show');       
        }
    });
}

function SaveVendorDetails()
{
    var flag            = 1;
    var FormType        = $("#FormType").val();  
    var CampaignType    = $("#CampaignTypeEdit").val();  
    var Campaign        = $("#CampaignEditVendor").val(); 
    var AdminLabel      = $("#AdminLabelEditVendor").val();  
    var PartnerLabel    = $("#PartnerLabelEditVendor").val();  
    var PayoutCalculation          = $("#PayoutCalculationEditVendor").val();  
    var RevsharePercent = $("#RevshareEditVendor").val();  
    var FixedPrice      = $("#FixedPriceEditVendor").val();  
    var DailyCap        = $("#DailyCapEditVendor").val();  
    var TotalCap        = $("#TotalCapEditVendor").val();  
    var MaxTimeOut      = $("#MaxTimeOutEditVendor").val(); 
    var MaxPingTimeOut  = $("#MaxPingTimeOutEditVendor").val(); 
    var MaxPostTimeOut  = $("#MaxPostTimeOutEditVendor").val();  
    var token           = $("#_token").val();
    var url             = $("#url").val();      

    if(Campaign=="")
    {
        $("#CampaignEditVendorErr").css('color','red');
        $("#CampaignEditVendorErr").html('Please Select Campaign.');
        $("#CampaignEditVendorErr").show();
        flag =0;
    }
    if(PartnerLabel=="")
    {
        $("#PartnerLabelEditVendorErr").css('color','red');
        $("#PartnerLabelEditvErr").html('Please Enter Partner Label.');
        $("#PartnerLabelEditVendorErr").show();
        flag =0;
    }
    if(AdminLabel=="")
    {
        $("#AdminLabelEditVendorErr").css('color','red');
        $("#AdminLabelEditVendorErr").html('Please Enter Admin Label.');
        $("#AdminLabelEditVendorErr").show();
        flag =0;
    }               
    if(PayoutCalculation=="")
    {
        $("#PayoutCalculationVendorErr").css('color','red');
        $("#PayoutCalculationVendorErr").html('Please Enter Payout Calculation Type.');
        $("#PayoutCalculationVendorErr").show();
        flag =0;
    }
    if(PayoutCalculation=="RevShare")
    {
        if(RevsharePercent=="")
        {
            $("#RevshareEditVendorErr").css('color','red');
            $("#RevshareEditVendorErr").html('Please Enter Revshare Percent.');
            $("#RevshareEditVendorErr").show();
            flag =0;
        }
    }
    if(PayoutCalculation=="FixedPrice")
    {
        if(FixedPrice=="" )
        {
            $("#FixedPriceEditVendorErr").css('color','red');
            $("#FixedPriceEditVendorErr").html('Please Enter Fixed Price.');
            $("#FixedPriceEditVendorErr").show();
            flag =0;
        }
    }
    if(DailyCap=="")
    {
        $("#DailyCapEditVendorErr").css('color','red');
        $("#DailyCapEditVendorErr").html('Required.');
        $("#DailyCapEditVendorErr").show();
        flag =0;
    }
    if(TotalCap=="")
    {
        $("#TotalCapEditVendorErr").css('color','red');
        $("#TotalCapEditVendorErr").html('Required.');
        $("#TotalCapEditVendorErr").show();
        flag =0;
    }
    if(CampaignType=="DirectPost")
    {
        if(MaxTimeOut=="")
        {
            $("#MaxTimeOutEditVendorErr").css('color','red');
            $("#MaxTimeOutEditvErr").html('Please Enter Max Time Out Values.');
            $("#MaxTimeOutEditVendorErr").show();
            flag =0;
        }
        else if(MaxTimeOut!="")
        {
            if(isNaN(MaxTimeOut))
            {
                $("#MaxTimeOutEditVendorErr").css('color','red');
                $("#MaxTimeOutEditVendorErr").html('Max Time Out Values Should Be Numeric.');
                $("#MaxTimeOutEditVendorErr").show();
                flag =0;
            }
        }
    }
    if(CampaignType=="PingPost")
    {
        if(MaxPingTimeOut=="")
        {
            $("#MaxPingTimeOutEditVendorErr").css('color','red');
            $("#MaxPingTimeOutEditVendorErr").html('Please Enter Max Time Out Values.');
            $("#MaxPingTimeOutEditVendorErr").show();
            flag =0;
        }
        else if(MaxPingTimeOut!="")
        {
            if(isNaN(MaxPingTimeOut))
            {
                $("#MaxPingTimeOutEditVendorErr").css('color','red');
                $("#MaxPingTimeOutEditVendorErr").html('Max Time Out Values Should Be Numeric.');
                $("#MaxPingTimeOutEditVendorErr").show();
                flag =0;
            }
        }
        if(MaxPostTimeOut=="")
        {
            $("#MaxPostTimeOutEditVendorErr").css('color','red');
            $("#MaxPostTimeOutEditVendorErr").html('Please Enter Max Time Out Values.');
            $("#MaxPostTimeOutEditVendorErr").show();
            flag =0;
        }
        else if(MaxPostTimeOut!="")
        {
            if(isNaN(MaxPostTimeOut))
            {
                $("#MaxPostTimeOutEditVendorErr").css('color','red');
                $("#MaxPostTimeOutEditVendorErr").html('Max Time Out Values Should Be Numeric.');
                $("#MaxPostTimeOutEditVendorErr").show();
                flag =0;
            }
        }              
    }
    if(flag ==1)
    {
        if(FormType=='Update')
        {
            $("#VendorUpdateForm").submit();
        }
        else if(FormType=='Clone')
        {
            if(AdminLabel!='')
            {
                var IsVendorNameExist = IsVendorNameExistFunctionForClone();
                if(IsVendorNameExist==true)
                {
                    $("#AdminLabelEditVendorErr").css('color','red');
                    $("#AdminLabelEditVendorErr").html('Same Admin Label Already Exist.');
                    $("#AdminLabelEditVendorErr").show();
                    flag=0;
                }
                else
                {
                    $("#VendorUpdateForm").submit();
                }
            }
        }            
    }
      
}

function GetVendorCloneDetailsFromCompany(VendorID)
{
    var token = $("#_token").val();
    var url = $("#url").val();
    
    var DataStr = {'_token': token, 'VendorID': VendorID};
    $.ajax({
        type: "POST",
        url: url + "/GetVendorCloneDetailsFromCompany",
        data: DataStr,
        success: function (data)
        {
            $("#CloneVendorDiv").html(data);     
            $("#CloneVendorModal").modal('show');       
        }
    });
}
function IsVendorNameExistFunctionForClone()
{
    var VendorName       = $("#AdminLabelEditVendor").val(); 
    var token = $("#_token").val();
    var url = $("#url").val();
    var DataStr = {_token : token,'VendorName':VendorName};
    var result;
    $.ajax({
            type:"POST",
            url :url+"/IsVendorNameExist",
            data : DataStr,
            async : false,
            success:function(data)
            {
               result = data;
            }
    });
    return result;
}
function ChangeVendorStatus(VID, Status)
{    
    var token           = $("#_token").val();
    var url             = $("#url").val();     
    var DataStr         = {'_token':token,'VID':VID,'Status':Status};
    $.ajax({
        type: "POST",
        url:  url+"/ChangeVendorStatus",
        data: DataStr,
        dataType: 'json',
        success: function(data) 
        {
            if(data.Status==1)
            {
                $("#VendorMessage").html(data.Msg); 
                $("#VendorMessage").show();
                $("#StatusHtml_"+VID).html(data.StatusHtml);                             
            }
            else
            {                
                $("#VendorMessage").html(data.Msg);
                $("#VendorMessage").show();
                $("#StatusHtml_"+VID).html(data.StatusHtml);                             
            }
            setInterval(function(){ $('#VendorMessage').fadeOut(); }, 5000);
        }
    });
}
function ChangeVendorTestMode(VID, Mode)
{    
    var token           = $("#_token").val();
    var url             = $("#url").val();     
    var DataStr         = {'_token':token,'VID':VID,'Mode':Mode};
    $.ajax({
        type: "POST",
        url:  url+"/ChangeVendorTestMode",
        data: DataStr,
        dataType: 'json',
        success: function(data) 
        {
            if(data.Status==1)
            {
                $("#VendorMessage").html(data.Msg); 
                $("#VendorMessage").show();                        
                $("#ModeHtml_"+VID).html(data.ModeHtml);                         
            }
            else
            {                
                $("#VendorMessage").html(data.Msg);  
                $("#VendorMessage").show();                       
                $("#ModeHtml_"+VID).html(data.ModeHtml);                         
            }
            setInterval(function(){ $('#VendorMessage').fadeOut(); }, 5000);
        }
    });
}
//Buyer JS////////////////////////////////////
function AddBuyerModal()
{ 
    $("#AdminLabelAddBuyerErr").hide();  
    $("#PartnerLabelAddBuyerErr").hide();  
    $("#IntegrationFileNameAddBuyerErr").hide();  
    $("#PayoutCalculationAddBuyerErr").hide();  
    $("#RevshareAddBuyerErr").hide();  
    $("#FixedPriceAddBuyerErr").hide();  
    $("#DailyCapAddBuyerErr").hide();  
    $("#TotalCapAddBuyerErr").hide();  
    $("#MaxTimeOutAddBuyerErr").hide(); 
    $("#MinTimeOutAddBuyerErr").hide(); 
    $("#MaxPingTimeOutAddBuyerErr").hide(); 
    $("#MaxPostTimeOutAddBuyerErr").hide(); 
    $("#MaxPingsPerMinuteAddBuyerErr").hide(); 
    
    $("#AddBuyerModal").modal('show');
}
function GetBuyerListFromCompany(page)
{
    var numofrecords    = $('#numofrecordsBuyer').val();
    var loading=$("#loading").val();
    if(numofrecords==10)
    { 
        $("#dataTable_wrapper").html('<center><img src="'+loading+'" style="width: 200px"><input type="hidden" name="numofrecords" id="numofrecordsBuyer" value="10"></center>'); 
    }
    else
    {
        $("#dataTable_wrapper").html('<center><img src="'+loading+'" style="width: 200px"></center>'); 
    }
    var token           = $("#_token").val();
    var url             = $("#url").val(); 

    var ShowInactive    = 0;
    var ShowTest        = 0;
    
    var Company         = $("#CompanyIDForAllSearch").val();
    var Campaign        = $("#CampaignBuyer").val();
    var BuyerName       = $("#BuyerNameBuyer").val();
    var Status          = $("#StatusBuyer").val();
    var BuyerMode       = $("#BuyerModeBuyer").val();
    
    var DataStr         = {'_token':token,'page':page,'numofrecords':numofrecords,'Company':Company,'Campaign':Campaign,'BuyerName':BuyerName,'BuyerMode':BuyerMode,'Status':Status,'ShowInactive':ShowInactive,'ShowTest':ShowTest};
    $.ajax({
        type: "POST",
        url:  url+"/GetBuyerListFromCompany",
        data: DataStr,
        success: function(data) 
        {
            $("#dataTable_wrapper_Buyer").html(data);         
        }
    }); 
}
function SearchBuyerList()
{
    GetBuyerListFromCompany(1);
}
function GetCampaignDetailsAddBuyer()
{
    var CampaignID = $("#CampaignAddBuyer").val();
    var token = $("#_token").val();
    var url = $("#url").val();
    var DataStr = {_token : token,CampaignID:CampaignID};
    var result;
    $.ajax({
            type:"POST",
            url :url+"/GetCampaignDetails",
            data : DataStr,
            dataType : 'json',
            success:function(data)
            {
                if(data.CampaignType == 'DirectPost')
                { 
                    $("#CampaignTypeAddBuyer").val(data.CampaignType);
                    $("#BuyerTierIDInputAddBuyer").css('display','block');

                    $("#MaxTimeOutInputAddBuyer").css('display','block');
                    $("#MinTimeOutInputAddBuyer").css('display','block');

                    $("#MaxPingTimeOutInputAddBuyer").css('display','none');
                    $("#MaxPostTimeOutInputAddBuyer").css('display','none');
                    $("#MaxPingsPerMinuteInputAddBuyer").css('display','none');

                    $("#PingTestURLInputAddBuyer").css('display','none');
                    $("#PingLiveURLInputAddBuyer").css('display','none');
                    $("#PostTestURLInputAddBuyer").css('display','none');
                    $("#PostLiveURLInputAddBuyer").css('display','none');
                    $("#DirectPostTestURLInputAddBuyer").css('display','block');
                    $("#DirectPostLiveURLInputAddBuyer").css('display','block');

                } 
                else if(data.CampaignType == 'PingPost')
                {
                    $("#CampaignTypeAddBuyer").val(data.CampaignType);
                    $("#BuyerTierIDInputAddBuyer").css('display','none');

                    $("#MaxTimeOutInputAddBuyer").css('display','none');
                    $("#MinTimeOutInputAddBuyer").css('display','none');

                    $("#MaxPingTimeOutInputAddBuyer").css('display','block');
                    $("#MaxPostTimeOutInputAddBuyer").css('display','block');
                    $("#MaxPingsPerMinuteInputAddBuyer").css('display','block');

                    $("#PingTestURLInputAddBuyer").css('display','block');
                    $("#PingLiveURLInputAddBuyer").css('display','block');
                    $("#PostTestURLInputAddBuyer").css('display','block');
                    $("#PostLiveURLInputAddBuyer").css('display','block');
                    $("#DirectPostTestURLInputAddBuyer").css('display','none');
                    $("#DirectPostLiveURLInputAddBuyer").css('display','none');


                }
            }
    });
    return result;
}
function CopyAdminLableAddBuyer(){
    var AdminLabel= $("#AdminLabelAddBuyer").val();
    $("#PartnerLabelAddBuyer").val(AdminLabel);

    $("#AdminLabelAddBuyerErr").hide();
    $("#PartnerLabelAddBuyerErr").hide();
}
function AddBuyerDetails()
{
    var flag            = 1;
    var CampaignType    = $("#CampaignTypeAddBuyer").val(); 
    var AdminLabel      = $("#AdminLabelAddBuyer").val();  
    var PartnerLabel    = $("#PartnerLabelAddBuyer").val();  
    var IntegrationFileName    = $("#IntegrationFileNameAddBuyer").val();  
    var PayoutCalculation      = $("#PayoutCalculationAddBuyer").val();  
    var Revshare        = $("#RevshareAddBuyer").val();  
    var FixedPrice      = $("#FixedPriceAddBuyer").val();  
    var DailyCap        = $("#DailyCapAddBuyer").val();  
    var TotalCap        = $("#TotalCapAddBuyer").val();  
    var MaxTimeOut      = $("#MaxTimeOutAddBuyer").val(); 
    var MinTimeOut      = $("#MinTimeOutAddBuyer").val(); 
    var MaxPingTimeOut  = $("#MaxPingTimeOutAddBuyer").val(); 
    var MaxPostTimeOut  = $("#MaxPostTimeOutAddBuyer").val(); 
    var MaxPingsPerMinute  = $("#MaxPingsPerMinuteAddBuyer").val();  
    var token           = $("#_token").val();
    var url             = $("#url").val();      
   
    if(PartnerLabel=="")
    {
        $("#PartnerLabelAddBuyerErr").css('color','red');
        $("#PartnerLabelAddBuyerErr").html('Please Enter Partner Label.');
        $("#PartnerLabelAddBuyerErr").show();
        flag =0;
    }
    if(AdminLabel=="")
    {
        $("#AdminLabelAddBuyerErr").css('color','red');
        $("#AdminLabelAddBuyerErr").html('Please Enter Admin Label.');
        $("#AdminLabelAddBuyerErr").show();
        flag =0;
    }
    if(IntegrationFileName=="")
    {
        $("#IntegrationFileNameAddBuyerErr").css('color','red');
        $("#IntegrationFileNameAddBuyerErr").html('Please Enter Integration File Name.');
        $("#IntegrationFileNameAddBuyerErr").show();
        flag =0;
    }               
    if(PayoutCalculation=="")
    {
        $("#PayoutCalculationAddBuyerErr").css('color','red');
        $("#PayoutCalculationAddBuyerErr").html('Please Enter Payout Calculation Type.');
        $("#PayoutCalculationAddBuyerErr").show();
        flag =0;
    }
    if(PayoutCalculation=="Revshare")
    {
        if(Revshare=="")
        {
            $("#RevshareAddBuyerErr").css('color','red');
            $("#RevshareAddBuyerErr").html('Please Enter Revshare Percent.');
            $("#RevshareAddBuyerErr").show();
            flag =0;
        }
    }
    if(PayoutCalculation=="FixedPrice")
    {
        if(FixedPrice=="" )
        {
            $("#FixedPriceAddBuyerErr").css('color','red');
            $("#FixedPriceAddBuyerErr").html('Please Enter Fixed Price.');
            $("#FixedPriceAddBuyerErr").show();
            flag =0;
        }
    }
    if(DailyCap=="")
    {
        $("#DailyCapAddBuyerErr").css('color','red');
        $("#DailyCapAddBuyerErr").html('Required.');
        $("#DailyCapAddBuyerErr").show();
        flag =0;
    }
    else if(DailyCap!="")
    {
        if(isNaN(DailyCap))
        {
            $("#DailyCapAddBuyerErr").css('color','red');
            $("#DailyCapAddBuyerErr").html('Daily Cap Values Should Be Numeric.');
            $("#DailyCapAddBuyerErr").show();
            flag =0;
        }
    }
    if(TotalCap=="")
    {
        $("#TotalCapAddBuyerErr").css('color','red');
        $("#TotalCapAddBuyerErr").html('Required.');
        $("#TotalCapAddBuyerErr").show();
        flag =0;
    }
    else if(TotalCap!="")
    {
        if(isNaN(TotalCap))
        {
            $("#TotalCapAddBuyerErr").css('color','red');
            $("#TotalCapAddBuyerErr").html('Total Cap Values Should Be Numeric.');
            $("#TotalCapAddBuyerErr").show();
            flag =0;
        }
    }
    if(CampaignType=="DirectPost")
    {
        if(MaxTimeOut=="")
        {
            $("#MaxTimeOutAddBuyerErr").css('color','red');
            $("#MaxTimeOutAddBuyerErr").html('Please Enter Max Time Out Values.');
            $("#MaxTimeOutAddBuyerErr").show();
            flag =0;
        }
        else if(MaxTimeOut!="")
        {
            if(isNaN(MaxTimeOut))
            {
                $("#MaxTimeOutAddBuyerErr").css('color','red');
                $("#MaxTimeOutAddBuyerErr").html('Max Time Out Values Should Be Numeric.');
                $("#MaxTimeOutAddBuyerErr").show();
                flag =0;
            }
        }
        if(MinTimeOut=="")
        {
            $("#MinTimeOutAddBuyerErr").css('color','red');
            $("#MinTimeOutAddBuyerErr").html('Please Enter Min Time Out Values.');
            $("#MinTimeOutAddBuyerErr").show();
            flag =0;
        }
        else if(MinTimeOut!="")
        {
            if(isNaN(MinTimeOut))
            {
                $("#MinTimeOutAddBuyerErr").css('color','red');
                $("#MinTimeOutAddBuyerErr").html('Min Time Out Values Should Be Numeric.');
                $("#MinTimeOutAddBuyerErr").show();
                flag =0;
            }
        }
    }
    if(CampaignType=="PingPost")
    {
        if(MaxPingTimeOut=="")
        {
            $("#MaxPingTimeOutAddBuyerErr").css('color','red');
            $("#MaxPingTimeOutAddBuyerErr").html('Please Enter Max Time Out Values.');
            $("#MaxPingTimeOutAddBuyerErr").show();
            flag =0;
        }
        else if(MaxPingTimeOut!="")
        {
            if(isNaN(MaxPingTimeOut))
            {
                $("#MaxPingTimeOutAddBuyerErr").css('color','red');
                $("#MaxPingTimeOutAddBuyerErr").html('Max Time Out Values Should Be Numeric.');
                $("#MaxPingTimeOutAddBuyerErr").show();
                flag =0;
            }
        }
        if(MaxPostTimeOut=="")
        {
            $("#MaxPostTimeOutAddBuyerErr").css('color','red');
            $("#MaxPostTimeOutAddBuyerErr").html('Please Enter Max Time Out Values.');
            $("#MaxPostTimeOutAddBuyerErr").show();
            flag =0;
        }
        else if(MaxPostTimeOut!="")
        {
            if(isNaN(MaxPostTimeOut))
            {
                $("#MaxPostTimeOutAddBuyerErr").css('color','red');
                $("#MaxPostTimeOutAddBuyerErr").html('Max Time Out Values Should Be Numeric.');
                $("#MaxPostTimeOutAddBuyerErr").show();
                flag =0;
            }
        } 
        if(MaxPingsPerMinute=="")
        {
            $("#MaxPingsPerMinuteAddBuyerErr").css('color','red');
            $("#MaxPingsPerMinuteAddBuyerErr").html('Please Enter Max Pings Per Minute Values.');
            $("#MaxPingsPerMinuteAddBuyerErr").show();
            flag =0;
        }
        else if(MaxPingsPerMinute!="")
        {
            if(isNaN(MaxPingsPerMinute))
            {
                $("#MaxPingsPerMinuteAddBuyerErr").css('color','red');
                $("#MaxPingsPerMinuteAddBuyerErr").html('Max Pings Per Minute Values Should Be Numeric.');
                $("#MaxPingsPerMinuteAddBuyerErr").show();
                flag =0;
            }
        }              
    }
    if(flag ==1)
    {
        
        if(AdminLabel!='')
        {
            var IsBuyerNameExist = IsBuyerNameExistFunction();

            if(IsBuyerNameExist==true)
            {
                $("#AdminLabelAddBuyerErr").css('color','red');
                $("#AdminLabelAddBuyerErr").html('Same Admin Label Already Exist.');
                $("#AdminLabelAddBuyerErr").show();
                flag=0;
            }
            else
            {
                $("#BuyerAddForm").submit();
            }
        }
    }
      
}
function IsBuyerNameExistFunction()
{
    var BuyerName       = $("#AdminLabelAddBuyer").val(); 
    var token = $("#_token").val();
    var url = $("#url").val();
    var DataStr = {_token : token,'BuyerName':BuyerName};
    var result;
    $.ajax({
            type:"POST",
            url :url+"/IsBuyerNameExist",
            data : DataStr,
            async : false,
            success:function(data)
            {
               result = data;
            }
    });
    return result;
}
function PayoutCalculationAddBuyerFunction(){
     var PayoutCalculation  = $("#PayoutCalculationAddBuyer").val();

     if(PayoutCalculation=="RevShare")
     {
        $("#RevshareInputAddBuyer").show();
        $("#FixedPriceInputAddBuyer").hide();
     }
     else if(PayoutCalculation=="FixedPrice")
     {
        $("#RevshareInputAddBuyer").hide();
        $("#FixedPriceInputAddBuyer").show();
     }
}
$('#filesAddBuyer').change(function (e) {
    var files = [];
    for (var i = 0; i < $(this)[0].files.length; i++) {
        files.push($(this)[0].files[i].name);
    }
    $(this).next('#fileLabelAddBuyer').html(files.join(', '));
});
function ChangeBuyerStatus(BID, Status)
{    
    var token           = $("#_token").val();
    var url             = $("#url").val();     
    var DataStr         = {'_token':token,'BID':BID,'Status':Status};
    $.ajax({
        type: "POST",
        url:  url+"/ChangeBuyerStatus",
        data: DataStr,
        dataType: 'json',
        success: function(data) 
        {
            if(data.Status==1)
            {
                $("#BuyerMessage").html(data.Msg); 
                $("#BuyerMessage").show();
                $("#StatusHtmlBuyer_"+BID).html(data.StatusHtml);                             
            }
            else
            {                
                $("#BuyerMessage").html(data.Msg);
                $("#BuyerMessage").show();
                $("#StatusHtmlBuyer_"+BID).html(data.StatusHtml);                             
            }
            setInterval(function(){ $('#BuyerMessage').fadeOut(); }, 5000);
        }
    });
}
function ChangeBuyerTestMode(BID, Mode)
{    
    var token           = $("#_token").val();
    var url             = $("#url").val();     
    var DataStr         = {'_token':token,'BID':BID,'Mode':Mode};
    $.ajax({
        type: "POST",
        url:  url+"/ChangeBuyerTestMode",
        data: DataStr,
        dataType: 'json',
        success: function(data) 
        {
            if(data.Status==1)
            {
                $("#BuyerMessage").html(data.Msg);  
                $("#BuyerMessage").show();                       
                $("#ModeHtmlBuyer_"+BID).html(data.ModeHtml);                         
            }
            else
            {                
                $("#BuyerMessage").html(data.Msg);  
                $("#BuyerMessage").show();                       
                $("#ModeHtmlBuyer_"+BID).html(data.ModeHtml);                         
            }
            setInterval(function(){ $('#BuyerMessage').fadeOut(); }, 5000);
        }
    });
}


function GetBuyerDetailsFromCompany(BuyerID)
{
    var token = $("#_token").val();
    var url = $("#url").val();
    
    var DataStr = {'_token': token, 'BuyerID': BuyerID};
    $.ajax({
        type: "POST",
        url: url + "/GetBuyerDetailsFromCompany",
        data: DataStr,
        success: function (data)
        {
            $("#EditBuyerDiv").html(data);     
            $("#EditBuyerModal").modal('show');       
        }
    });
}
function GetCampaignDetailsEditBuyer()
{
    var CampaignID = $("#CampaignEditBuyer").val();
    var token = $("#_token").val();
    var url = $("#url").val();
    var DataStr = {_token : token,CampaignID:CampaignID};
    var result;
    $.ajax({
            type:"POST",
            url :url+"/GetCampaignDetails",
            data : DataStr,
            dataType : 'json',
            success:function(data)
            {
                if(data.CampaignType == 'DirectPost')
                { 
                    $("#CampaignTypeEditBuyer").val(data.CampaignType);
                    $("#BuyerTierIDInputEditBuyer").css('display','block');

                    $("#MaxTimeOutInputEditBuyer").css('display','block');
                    $("#MinTimeOutInputEditBuyer").css('display','block');

                    $("#MaxPingTimeOutInputEditBuyer").css('display','none');
                    $("#MaxPostTimeOutInputEditBuyer").css('display','none');
                    $("#MaxPingsPerMinuteInputEditBuyer").css('display','none');

                    $("#PingTestURLInputEditBuyer").css('display','none');
                    $("#PingLiveURLInputEditBuyer").css('display','none');
                    $("#PostTestURLInputEditBuyer").css('display','none');
                    $("#PostLiveURLInputEditBuyer").css('display','none');
                    $("#DirectPostTestURLInputEditBuyer").css('display','block');
                    $("#DirectPostLiveURLInputEditBuyer").css('display','block');

                } 
                else if(data.CampaignType == 'PingPost')
                {
                    $("#CampaignTypeEditBuyer").val(data.CampaignType);
                    $("#BuyerTierIDInputEditBuyer").css('display','none');

                    $("#MaxTimeOutInputEditBuyer").css('display','none');
                    $("#MinTimeOutInputEditBuyer").css('display','none');

                    $("#MaxPingTimeOutInputEditBuyer").css('display','block');
                    $("#MaxPostTimeOutInputEditBuyer").css('display','block');
                    $("#MaxPingsPerMinuteInputEditBuyer").css('display','block');

                    $("#PingTestURLInputEditBuyer").css('display','block');
                    $("#PingLiveURLInputEditBuyer").css('display','block');
                    $("#PostTestURLInputEditBuyer").css('display','block');
                    $("#PostLiveURLInputEditBuyer").css('display','block');
                    $("#DirectPostTestURLInputEditBuyer").css('display','none');
                    $("#DirectPostLiveURLInputEditBuyer").css('display','none');


                }
            }
    });
    return result;
}

function SaveBuyerDetails()
{
    var flag            = 1;
    var FormType        = $("#FormTypeEditBuyer").val(); 
    var CampaignType    = $("#CampaignTypeEditBuyer").val(); 
    var AdminLabel      = $("#AdminLabelEditBuyer").val();  
    var PartnerLabel    = $("#PartnerLabelEditBuyer").val();  
    var IntegrationFileName    = $("#IntegrationFileNameEditBuyer").val();  
    var PayoutCalculation      = $("#PayoutCalculationEditBuyer").val();  
    var Revshare        = $("#RevshareEditBuyer").val();  
    var FixedPrice      = $("#FixedPriceEditBuyer").val();  
    var DailyCap        = $("#DailyCapEditBuyer").val();  
    var TotalCap        = $("#TotalCapEditBuyer").val();  
    var MaxTimeOut      = $("#MaxTimeOutEditBuyer").val(); 
    var MinTimeOut      = $("#MinTimeOutEditBuyer").val(); 
    var MaxPingTimeOut  = $("#MaxPingTimeOutEditBuyer").val(); 
    var MaxPostTimeOut  = $("#MaxPostTimeOutEditBuyer").val(); 
    var MaxPingsPerMinute  = $("#MaxPingsPerMinuteEditBuyer").val(); 
           
    
    if(PartnerLabel=="")
    {
        $("#PartnerLabelEditBuyerErr").css('color','red');
        $("#PartnerLabelEditBuyerErr").html('Please Enter Partner Label.');
        $("#PartnerLabelEditBuyerErr").show();
        flag =0;
    }
    if(AdminLabel=="")
    {
        $("#AdminLabelEditBuyerErr").css('color','red');
        $("#AdminLabelEditBuyerErr").html('Please Enter Admin Label.');
        $("#AdminLabelEditBuyerErr").show();
        flag =0;
    }
    if(IntegrationFileName=="")
    {
        $("#IntegrationFileNameEditBuyerErr").css('color','red');
        $("#IntegrationFileNameEditBuyerErr").html('Please Enter Integration File Name.');
        $("#IntegrationFileNameEditBuyerErr").show();
        flag =0;
    }               
    if(PayoutCalculation=="")
    {
        $("#PayoutCalculationEditBuyerErr").css('color','red');
        $("#PayoutCalculationEditvErr").html('Please Enter Payout Calculation Type.');
        $("#PayoutCalculationEditBuyerErr").show();
        flag =0;
    }
    if(PayoutCalculation=="Revshare")
    {
        if(Revshare=="")
        {
            $("#RevshareEditBuyerErr").css('color','red');
            $("#RevshareEditBuyerErr").html('Please Enter Revshare Percent.');
            $("#RevshareEditBuyerErr").show();
            flag =0;
        }
    }
    if(PayoutCalculation=="FixedPrice")
    {
        if(FixedPrice=="" )
        {
            $("#FixedPriceEditBuyerErr").css('color','red');
            $("#FixedPriceEditBuyerErr").html('Please Enter Fixed Price.');
            $("#FixedPriceEditBuyerErr").show();
            flag =0;
        }
    }
    if(DailyCap=="")
    {
        $("#DailyCapEditBuyerErr").css('color','red');
        $("#DailyCapEditBuyerErr").html('Required.');
        $("#DailyCapEditBuyerErr").show();
        flag =0;
    }
    else if(DailyCap!="")
    {
        if(isNaN(DailyCap))
        {
            $("#DailyCapEditBuyerErr").css('color','red');
            $("#DailyCapEditBuyerErr").html('Daily Cap Values Should Be Numeric.');
            $("#DailyCapEditBuyerErr").show();
            flag =0;
        }
    }
    if(TotalCap=="")
    {
        $("#TotalCapEditBuyerErr").css('color','red');
        $("#TotalCapEditBuyerErr").html('Required.');
        $("#TotalCapEditBuyerErr").show();
        flag =0;
    }
    else if(TotalCap!="")
    {
        if(isNaN(TotalCap))
        {
            $("#TotalCapEditBuyerErr").css('color','red');
            $("#TotalCapEditBuyerErr").html('Total Cap Values Should Be Numeric.');
            $("#TotalCapEditBuyerErr").show();
            flag =0;
        }
    }
    if(CampaignType=="DirectPost")
    {
        if(MaxTimeOut=="")
        {
            $("#MaxTimeOutEditBuyerErr").css('color','red');
            $("#MaxTimeOutEditBuyerErr").html('Please Enter Max Time Out Values.');
            $("#MaxTimeOutEditBuyerErr").show();
            flag =0;
        }
        else if(MaxTimeOut!="")
        {
            if(isNaN(MaxTimeOut))
            {
                $("#MaxTimeOutEditBuyerErr").css('color','red');
                $("#MaxTimeOutEditBuyerErr").html('Max Time Out Values Should Be Numeric.');
                $("#MaxTimeOutEditBuyerErr").show();
                flag =0;
            }
        }
        if(MinTimeOut=="")
        {
            $("#MinTimeOutEditErr").css('color','red');
            $("#MinTimeOutEditErr").html('Please Enter Min Time Out Values.');
            $("#MinTimeOutEditErr").show();
            flag =0;
        }
        else if(MinTimeOut!="")
        {
            if(isNaN(MinTimeOut))
            {
                $("#MinTimeOutEditBuyerErr").css('color','red');
                $("#MinTimeOutEditBuyerErr").html('Min Time Out Values Should Be Numeric.');
                $("#MinTimeOutEditBuyerErr").show();
                flag =0;
            }
        }
    }
    if(CampaignType=="PingPost")
    {
        if(MaxPingTimeOut=="")
        {
            $("#MaxPingTimeOutEditBuyerErr").css('color','red');
            $("#MaxPingTimeOutEditBuyerErr").html('Please Enter Max Time Out Values.');
            $("#MaxPingTimeOutEditBuyerErr").show();
            flag =0;
        }
        else if(MaxPingTimeOut!="")
        {
            if(isNaN(MaxPingTimeOut))
            {
                $("#MaxPingTimeOutEditBuyerErr").css('color','red');
                $("#MaxPingTimeOutEditBuyerErr").html('Max Time Out Values Should Be Numeric.');
                $("#MaxPingTimeOutEditBuyerErr").show();
                flag =0;
            }
        }
        if(MaxPostTimeOut=="")
        {
            $("#MaxPostTimeOutEditBuyerErr").css('color','red');
            $("#MaxPostTimeOutEditBuyerErr").html('Please Enter Max Time Out Values.');
            $("#MaxPostTimeOutEditBuyerErr").show();
            flag =0;
        }
        else if(MaxPostTimeOut!="")
        {
            if(isNaN(MaxPostTimeOut))
            {
                $("#MaxPostTimeOutEditBuyerErr").css('color','red');
                $("#MaxPostTimeOutEditBuyerErr").html('Max Time Out Values Should Be Numeric.');
                $("#MaxPostTimeOutEditBuyerErr").show();
                flag =0;
            }
        } 
        if(MaxPingsPerMinute=="")
        {
            $("#MaxPingsPerMinuteEditBuyerErr").css('color','red');
            $("#MaxPingsPerMinuteEditBuyerErr").html('Please Enter Max Pings Per Minute Values.');
            $("#MaxPingsPerMinuteEditBuyerErr").show();
            flag =0;
        }
        else if(MaxPingsPerMinute!="")
        {
            if(isNaN(MaxPingsPerMinute))
            {
                $("#MaxPingsPerMinuteEditBuyerErr").css('color','red');
                $("#MaxPingsPerMinuteEditBuyerErr").html('Max Pings Per Minute Values Should Be Numeric.');
                $("#MaxPingsPerMinuteEditBuyerErr").show();
                flag =0;
            }
        }              
    }
    if(flag ==1)
    {        
        if(FormType=='Update')
        {
            $("#BuyerUpdateForm").submit();
        }
        else if(FormType=='Clone')
        {
            if(AdminLabel!='')
            {
                var IsBuyerNameExist = IsBuyerNameExistFunctionForClone();
                if(IsBuyerNameExist==true)
                {
                    $("#AdminLabelEditBuyerErr").css('color','red');
                    $("#AdminLabelEditBuyerErr").html('Same Admin Label Already Exist.');
                    $("#AdminLabelEditBuyerErr").show();
                    flag=0;
                }
                else
                {
                    $("#BuyerUpdateForm").submit();
                }
            }
        }  
    }
      
}
function IsBuyerNameExistFunctionForClone()
{
    var BuyerName       = $("#AdminLabelEditBuyer").val(); 
    var token = $("#_token").val();
    var url = $("#url").val();
    var DataStr = {_token : token,'BuyerName':BuyerName};
    var result;
    $.ajax({
            type:"POST",
            url :url+"/IsBuyerNameExist",
            data : DataStr,
            async : false,
            success:function(data)
            {
               result = data;
            }
    });
    return result;
}
function CopyAdminLableEditBuyer(){
    var AdminLabel= $("#AdminLabelEditBuyer").val();
    $("#PartnerLabelEditBuyer").val(AdminLabel);

    $("#AdminLabelEditBuyerErr").hide();
    $("#PartnerLabelEditBuyerErr").hide();
}

$('#filesEditBuyer').change(function (e) {
    var files = [];
    for (var i = 0; i < $(this)[0].files.length; i++) {
        files.push($(this)[0].files[i].name);
    }
    $(this).next('#fileLabelEditBuyer').html(files.join(', '));
});
function DeleteBuyerUploadedFile(BuyerID, ID)
{
    var token           = $("#_token").val();
    var url             = $("#url").val();
    var DataStr         = {'_token':token,'BuyerID':BuyerID,'ID':ID};
    $.ajax({
        type: "POST",
        url:  url+"/DeleteBuyerUploadedFile",
        data: DataStr,
        dataType: "json",
        success: function(data)
        {
            if(data.Status==1)
            {
                $("#UploadedFileTR_"+ID).remove();
                $("#UploadFileMessage").html(data.Message);
            } 
            else
            {
                $("#UploadFileMessage").html(data.Message);
            }        
        }
    });
}
function PayoutCalculationEditBuyerFunction(){
     var PayoutCalculation  = $("#PayoutCalculationEditBuyer").val();

     if(PayoutCalculation=="RevShare")
     {
        $("#RevshareInputEditBuyer").show();
        $("#FixedPriceInputEditBuyer").hide();
     }
     else if(PayoutCalculation=="FixedPrice")
     {
        $("#RevshareInputEditBuyer").hide();
        $("#FixedPriceInputEditBuyer").show();
     }
}

function GetBuyerCloneDetailsFromCompany(BuyerID)
{
    var token = $("#_token").val();
    var url = $("#url").val();
    
    var DataStr = {'_token': token, 'BuyerID': BuyerID};
    $.ajax({
        type: "POST",
        url: url + "/GetBuyerCloneDetailsFromCompany",
        data: DataStr,
        success: function (data)
        {
            $("#CloneBuyerDiv").html(data);     
            $("#CloneBuyerModal").modal('show');       
        }
    });
}
