$(document).ready(function(){
    GetVendorBuyerUsingCampaignID();
    VendorResponse();
    GetVendorResponseList(1);
});
$("#Campaign").change(function(){
    GetVendorBuyerUsingCampaignID();
    VendorResponse();
});
$("#Action").change(function(){
    VendorResponse();
});

function GetVendorBuyerUsingCampaignID()
{ 
    var token           = $("#_token").val();
    var url             = $("#url").val(); 

    var CampaignID      = $("#Campaign").val();     
    var DataStr         = {'_token':token,'CampaignID':CampaignID};
    $.ajax({
        type: "POST",
        url:  url+"/GetVendorBuyerUsingCampaignID",
        data: DataStr,
        dataType: 'json',
        success: function(data) 
        {
            $("#VendorCompany").html(data.VendorCompany);
            $("#Vendor").html(data.Vendor);
            if(data.CampaignType=='DirectPost')
            {
                $("#Action .pp").prop('disabled',true);
                $("#Action .dp").prop('disabled',false);
                $('#Action').val('DirectPost'); 
            }   
            else if(data.CampaignType=='PingPost') 
            {
                $("#Action .pp").prop('disabled',false);
                $("#Action .dp").prop('disabled',true);
                $('#Action').val('Ping'); 
            }    
        }
    }); 
}
function GetVendorUsingVendorCompany()
{
    var VendorCompanyID = $("#VendorCompany").val();
    var CampaignID      = $("#Campaign").val(); 
    var token = $("#_token").val();
    var url = $("#url").val();

    var DataStr = {'_token': token, 'VendorCompanyID': VendorCompanyID,'CampaignID':CampaignID};
    $.ajax({
        type: "POST",
        url: url + "/GetVendorUsingVendorCompany",
        data: DataStr,
        success: function (data)
        {
            $("#Vendor").html(data);
        }
    });
}
function VendorResponse() {
    var token           = $("#_token").val();
    var url             = $("#url").val();
    var CampaignID      = $("#Campaign").val();
    var Action          = $("#Action").val();

    var DataStr = {'_token':token,'CampaignID':CampaignID,'Action':Action};

    $.ajax({
        type: "POST",
        url:  url+"/VendorResponse",
        data: DataStr,
        success: function(data)
        {
           $("#Response").html(data);
        }
    });
}
function SearchVendorResponseList()
{
    GetVendorResponseList(1);
}
function Pagination(page)
{
    GetVendorResponseList(page);
}
function GetVendorResponseList(page) {
    
    var numofrecords    = $('#numofrecords').val();
    var loading=$("#loading").val();
    if(numofrecords==10)
    {
        $("#dataTable_wrapper").html('<center><img src="'+loading+'" style="width: 200px"><input type="hidden" name="numofrecords" id="numofrecords" value="10"></center>'); 
    }
    else
    {
        $("#dataTable_wrapper").html('<center><img src="'+loading+'" style="width: 200px"></center>'); 
    }

    var token           = $("#_token").val();
    var url             = $("#url").val();
    var CampaignID      = $("#Campaign").val();
    var VendorCompany   = $("#VendorCompany").val();
    var Vendor          = $("#Vendor").val();
    var Response        = $("#Response").val();
    var StartDate       = $("#StartDate").val();
    var EndDate         = $("#EndDate").val();
    var Sort            = $("#Sort").val();
    var Action          = $("#Action").val();
    
    var DataStr = {
            '_token':token,
            'CampaignID':CampaignID,
            'VendorCompany':VendorCompany,
            'Vendor':Vendor,
            'Response':Response,
            'Sort':Sort,
            'Action':Action,
            'StartDate':StartDate,
            'EndDate':EndDate,
            'numofrecords':numofrecords,
            'page':page
    };

    $.ajax({
        type: "POST",
        url:  url+"/GetVendorResponseList",
        data: DataStr,
        success: function(data)
        {
           $("#dataTable_wrapper").html(data);
        }
    });
}

