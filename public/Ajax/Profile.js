$("#ProfileBtn").click(function()
{
    var flag                =1;
    var CompanyName         = $("#CompanyName").val();  
    var CompanyAddress1     = $("#CompanyAddress1").val();  
    var CompanyAddress2     = $("#CompanyAddress2").val();  
    var CompanyCity         = $("#CompanyCity").val();  
    var CompanyStateCountry = $("#CompanyStateCountry").val();  
    var CompanyZipPostCode  = $("#CompanyZipPostCode").val();  
    var UserFirstName       = $("#UserFirstName").val();  
    var UserLastName        = $("#UserLastName").val();  
    var UserPassword        = $("#UserPassword").val();  
    var UserPasswordConfirm = $("#UserPasswordConfirm").val();

    var token           = $("#_token").val();
    var url             = $("#url").val();  

   /* if(CompanyName=="")
    {
        $("#CompanyNameErr").css('color','red');
        $("#CompanyNameErr").html('Please Enter Company Name.');
        $("#CompanyNameErr").show();
        flag =0;
    }*/
   /* else
    {
        if(IsSpecialCharacter(CompanyName))
        {
            $("#CompanyNameErr").css('color','red');
            $("#CompanyNameErr").html('Special Characters not allowed.');
            $("#CompanyNameErr").show();
            flag =0;
        }
    }*/
    /*if(CompanyAddress1=="")
    {
        $("#CompanyAddress1Err").css('color','red');
        $("#CompanyAddress1Err").html('Please Enter Company Address1.');
        $("#CompanyAddress1Err").show();
        flag =0;
    }
    if(CompanyAddress2=="")
    {
        $("#CompanyAddress2Err").css('color','red');
        $("#CompanyAddress2Err").html('Please Enter Company Address2.');
        $("#CompanyAddress2Err").show();
        flag =0;
    }
    if(CompanyCity=="")
    {
        $("#CompanyCityErr").css('color','red');
        $("#CompanyCityErr").html('Please Enter Company City.');
        $("#CompanyCityErr").show();
        flag =0;
    }
    else
    {
        if(IsSpecialCharacter(CompanyCity))
        {
            $("#CompanyCityErr").css('color','red');
            $("#CompanyCityErr").html('Special Characters Or Number not allowed.');
            $("#CompanyCityErr").show();
            flag =0;
        }
    }
    if(CompanyStateCountry=="")
    {
        $("#CompanyStateCountryErr").css('color','red');
        $("#CompanyStateCountryErr").html('Please Enter Company State/Country.');
        $("#CompanyStateCountryErr").show();
        flag =0;
    }
    if(CompanyZipPostCode=="")
    {
        $("#CompanyZipPostCodeErr").css('color','red');
        $("#CompanyZipPostCodeErr").html('Please Enter Company Zip Code.');
        $("#CompanyZipPostCodeErr").show();
        flag =0;
    }*/

    if(UserFirstName=="")
    {
        $("#UserFirstNameErr").css('color','red');
        $("#UserFirstNameErr").html('Please Enter First Name.');
        $("#UserFirstNameErr").show();
        flag =0;
    }
    else
    {
        if(IsSpecialCharacter(UserFirstName))
        {
            $("#UserFirstNameErr").css('color','red');
            $("#UserFirstNameErr").html('Special Characters Or Number not allowed.');
            $("#UserFirstNameErr").show();
            flag =0;
        }
    }

    if(UserLastName=="")
    {
        $("#UserLastNameErr").css('color','red');
        $("#UserLastNameErr").html('Please Enter Last Name.');
        $("#UserLastNameErr").show();
        flag =0;
    }
    else
    {
        if(IsSpecialCharacter(UserLastName))
        {
            $("#UserLastNameErr").css('color','red');
            $("#UserLastNameErr").html('Special Characters Or Number not allowed.');
            $("#UserLastNameErr").show();
            flag =0;
        }
    }

    if(UserPassword!="")
    {
        if(UserPasswordConfirm=="")
        {
            $("#UserPasswordConfirmErr").css('color','red');
            $("#UserPasswordConfirmErr").html('Please Enter Confirm Password.');
            $("#UserPasswordConfirmErr").show();
            flag =0;
        }
    }
    if(flag ==1)
    {        
        $("#ProfileForm").submit();
    }

});
function HideErr(id)
{
    $("#"+id).hide();
}
function ValidateEmail(email)
{
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function IsSpecialCharacter(yourInput)
{
    re = /[`0123456789~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi;
    var isSplChar = re.test(yourInput);
    return isSplChar;
}


