$(document).ready(function() {    
    GetAffiliateReport(1);
    GetVendorBuyerUsingCampaignID();
});
function Pagination(page)
{
    GetAffiliateReport(page);
}
function SearchAffiliateReport()
{
    GetAffiliateReport(1);
}
function GetAffiliateReport(page)
{
    var loading=$("#loading").val(); 
    $("#AffiliateReport").html('<center><div><img src="'+loading+'" style="width: 200px"></div></center>');
    var token           = $("#_token").val();
    var url             = $("#url").val(); 

    var UserID          = $("#User").val();   
    var CampaignID      = $("#Campaign").val();   
    var VendorCompanyID   = $("#VendorCompany").val();   
    var StartDate       = $("#StartDate").val();
    var EndDate         = $("#EndDate").val();
    var numofrecords    = $('#numofrecords').val();
    
    var DataStr         = {'_token':token,'page':page,'numofrecords':numofrecords,'UserID':UserID,'CampaignID':CampaignID,'StartDate':StartDate,'EndDate':EndDate,'VendorCompanyID':VendorCompanyID};
    $.ajax({
        type: "POST",
        url:  url+"/GetAffiliateReport",
        data: DataStr,
        success: function(data) 
        {
            $("#AffiliateReport").html(data);         
        }
    }); 
}
function GetVendorBuyerUsingCampaignID()
{ 
    var token           = $("#_token").val();
    var url             = $("#url").val(); 

    var CampaignID      = $("#Campaign").val();     
    var DataStr         = {'_token':token,'CampaignID':CampaignID};
    $.ajax({
        type: "POST",
        url:  url+"/GetVendorBuyerUsingCampaignID",
        data: DataStr,
        dataType: 'json',
        success: function(data) 
        {
            $("#VendorCompany").html(data.VendorCompany);      
        }
    }); 
}

function GetUserUsingCompanyID()
{ 
    var token           = $("#_token").val();
    var url             = $("#url").val(); 

    var VendorCompanyID      = $("#VendorCompany").val();     
    var CampaignID      = $("#Campaign").val();     
    var DataStr         = {'_token':token,'CampaignID':CampaignID,'VendorCompanyID':VendorCompanyID};
    $.ajax({
        type: "POST",
        url:  url+"/GetUserUsingCompanyID",
        data: DataStr,
        success: function(data) 
        {
            $("#User").html(data);      
        }
    }); 
}
