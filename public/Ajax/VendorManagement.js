$(document).ready(function() {   
    GetCompanyUsingShwowInactiveAndTest();
    GetVendorList(1);
    GetCampaignDetailsAdd();
});
function Pagination(page)
{
    GetVendorList(page);
}
function SearchVendorList()
{
    GetVendorList(1);
}
function GetVendorList(page)
{
    var numofrecords    = $('#numofrecords').val();
    var loading=$("#loading").val();
    if(numofrecords==10)
    {
        $("#dataTable_wrapper").html('<center><img src="'+loading+'" style="width: 200px"><input type="hidden" name="numofrecords" id="numofrecords" value="10"></center>'); 
    }
    else
    {
        $("#dataTable_wrapper").html('<center><img src="'+loading+'" style="width: 200px"></center>'); 
    }
    var token           = $("#_token").val();
    var url             = $("#url").val(); 
    var ShowInactive = '0';
    var ShowTest = '0';
    if ($("#ShowInactive").prop('checked') == true) {
        ShowInactive = '1';
    }
    if ($("#ShowTest").prop('checked') == true) {
        ShowTest = '1';
    }

    var CompanyID       = $("#Company").val();
    var Campaign        = $("#Campaign").val();
    var VendorID        = $("#VendorID").val();
    var VendorName      = $("#VendorName").val();
    var VendorStatus    = $("#VendorStatus").val();
    
    var DataStr         = {'_token':token,'page':page,'numofrecords':numofrecords,'Campaign':Campaign,'VendorName':VendorName,'CompanyID':CompanyID,'VendorID':VendorID,'VendorStatus':VendorStatus,'ShowInactive':ShowInactive,'ShowTest':ShowTest};
    $.ajax({
        type: "POST",
        url:  url+"/GetVendorList",
        data: DataStr,
        success: function(data) 
        {
            $("#dataTable_wrapper").html(data);         
        }
    }); 
}


$("#VendorSaveBtn").click(function(){

    var flag            =1;
    var PartnerLabel    = $("#PartnerLabel").val(); 

    var token           = $("#_token").val();
    var url             = $("#url").val();  

    if(PartnerLabel=="")
    {
        $("#PartnerLabelErr").css('color','red');
        $("#PartnerLabelErr").html('Please Enter Partner Label.');
        $("#PartnerLabelErr").show();
        flag =0;
    }
    if(flag ==1)
    {        
        $("#VendorSaveForm").submit();
    }
});

function HideErr(id)
{
    $("#"+id).hide();
}

function InsertTokenValue(token_value)
{     
    var $txt = jQuery("#TrackingPixelCode");
    var caretPos = $txt[0].selectionStart;
    var textAreaTxt = $txt.val();
    var txtToAdd = "[--" + token_value.value + "--]";
    $txt.val(textAreaTxt.substring(0, caretPos) + txtToAdd + textAreaTxt.substring(caretPos) );   
}

function GetPartnetLabel(VendorID)
{    
    var token           = $("#_token").val();
    var url             = $("#url").val();     
    var DataStr         = {'_token':token,'VendorID':VendorID};
    $.ajax({
        type: "POST",
        url:  url+"/GetPartnetLabel",
        data: DataStr,
        success: function(data) 
        {
            $("#PartnerLabel_"+VendorID).html(data);         
        }
    }); 
}

function SavePartnetLabel(Action,VendorID)
{
    var token           = $("#_token").val();
    var url             = $("#url").val();     
    var PartnerLabel    = $("#PartnerLabelText_"+VendorID).val();     
    var DataStr         = {'_token':token,'VendorID':VendorID,'Action':Action,'PartnerLabel':PartnerLabel};
    $.ajax({
        type: "POST",
        url:  url+"/SavePartnetLabel",
        data: DataStr,
        success: function(data) 
        {
            $("#PartnerLabel_"+VendorID).html(data);         
        }
    });
}

function GetCompanyUsingShwowInactiveAndTest()
{
    var token = $("#_token").val();
    var url = $("#url").val();
    var ShowInactive = '0';
    var ShowTest = '0';
    if ($("#ShowInactive").prop('checked') == true) {
        ShowInactive = '1';
    }
    if ($("#ShowTest").prop('checked') == true) {
        ShowTest = '1';
    }
    var DataStr = {'_token': token, 'ShowInactive': ShowInactive, 'ShowTest': ShowTest};
    $.ajax({
        type: "POST",
        url: url + "/GetCompanyUsingShwowInactiveAndTest",
        data: DataStr,
        success: function (data)
        {
            $("#Company").html(data);        
        }
    });
}

function GetCompanyUsingShwowInactiveAndTestEdit()
{
    var token = $("#_token").val();
    var url = $("#url").val();
    var ShowInactive = '0';
    var ShowTest = '0';
    if ($("#ShowInactiveEdit").prop('checked') == true) {
        ShowInactive = '1';
    }
    if ($("#ShowTestEdit").prop('checked') == true) {
        ShowTest = '1';
    }
    var DataStr = {'_token': token, 'ShowInactive': ShowInactive, 'ShowTest': ShowTest};
    $.ajax({
        type: "POST",
        url: url + "/GetCompanyUsingShwowInactiveAndTest2",
        data: DataStr,
        success: function (data)
        {
            $("#CompanyEdit").html(data);        
        }
    });
}
function GetCompanyUsingShwowInactiveAndTestAdd()
{
    var token = $("#_token").val();
    var url = $("#url").val();
    var ShowInactive = '0';
    var ShowTest = '0';
    if ($("#ShowInactiveAdd").prop('checked') == true) {
        ShowInactive = '1';
    }
    if ($("#ShowTestAdd").prop('checked') == true) {
        ShowTest = '1';
    }
    var DataStr = {'_token': token, 'ShowInactive': ShowInactive, 'ShowTest': ShowTest};
    $.ajax({
        type: "POST",
        url: url + "/GetCompanyUsingShwowInactiveAndTest2",
        data: DataStr,
        success: function (data)
        {
            $("#CompanyAdd").html(data);        
        }
    });
}
function ChangeVendorStatus(VID, Status)
{    
    var token           = $("#_token").val();
    var url             = $("#url").val();     
    var DataStr         = {'_token':token,'VID':VID,'Status':Status};
    $.ajax({
        type: "POST",
        url:  url+"/ChangeVendorStatus",
        data: DataStr,
        dataType: 'json',
        success: function(data) 
        {
            if(data.Status==1)
            {
                $("#VendorMessage").html(data.Msg); 
                $("#StatusHtml_"+VID).html(data.StatusHtml);                             
            }
            else
            {                
                $("#VendorMessage").html(data.Msg);
                $("#StatusHtml_"+VID).html(data.StatusHtml);                             
            }
            setInterval(function(){ $('#VendorMessage').fadeOut(); }, 5000);
        }
    });
}
function ChangeVendorTestMode(VID, Mode)
{    
    var token           = $("#_token").val();
    var url             = $("#url").val();     
    var DataStr         = {'_token':token,'VID':VID,'Mode':Mode};
    $.ajax({
        type: "POST",
        url:  url+"/ChangeVendorTestMode",
        data: DataStr,
        dataType: 'json',
        success: function(data) 
        {
            if(data.Status==1)
            {
                $("#VendorMessage").html(data.Msg);                         
                $("#ModeHtml_"+VID).html(data.ModeHtml);                         
            }
            else
            {                
                $("#VendorMessage").html(data.Msg);                         
                $("#ModeHtml_"+VID).html(data.ModeHtml);                         
            }
            setInterval(function(){ $('#VendorMessage').fadeOut(); }, 5000);
        }
    });
}

function CopyAdminLableAdd(){
    var AdminLabel= $("#AdminLabelAdd").val();
    $("#PartnerLabelAdd").val(AdminLabel);

    $("#AdminLabelAddErr").hide();
    $("#PartnerLabelAddErr").hide();
}
function CopyAdminLableEdit(){
    var AdminLabel= $("#AdminLabelEdit").val();
    $("#PartnerLabelEdit").val(AdminLabel);

    $("#AdminLabelEditErr").hide();
    $("#PartnerLabelEditErr").hide();
}
function PayoutCalculationChange()
{
     var PayoutCalculation  = $("#PayoutCalculationEdit").val();

     if(PayoutCalculation=="RevShare")
     {
        $("#RevsharePercentInputEdit").show();
        $("#FixedPriceInputEdit").hide();
        $("#TierPriceInputEdit").hide();
     }
     else if(PayoutCalculation=="MinPrice")
     {
        $("#RevsharePercentInputEdit").show();
        $("#FixedPriceInputEdit").hide();
        $("#TierPriceInputEdit").hide();
     }
     else if(PayoutCalculation=="TierPrice")
     {
       $("#RevsharePercentInputEdit").hide();
        $("#FixedPriceInputEdit").hide();
        $("#TierPriceInputEdit").show();
     }
     else if(PayoutCalculation=="FixedPrice")
     {
        $("#RevsharePercentInputEdit").hide();
        $("#FixedPriceInputEdit").show();
        $("#TierPriceInputEdit").hide();
     }
     else if(PayoutCalculation=="BucketPrice" || PayoutCalculation=="BucketSystemBySubID")
     {
        $("#RevsharePercentInputEdit").show();
        $("#FixedPriceInputEdit").show();
        $("#TierPriceInputEdit").hide();
     }
}
$("#PayoutCalculationAdd").change(function(){

     var PayoutCalculation  = $("#PayoutCalculationAdd").val();

     if(PayoutCalculation=="RevShare")
     {
        $("#RevsharePercentInputAdd").show();
        $("#FixedPriceInputAdd").hide();
        $("#TierPriceInputAdd").hide();
     }
     else if(PayoutCalculation=="MinPrice")
     {
        $("#RevsharePercentInputAdd").show();
        $("#FixedPriceInputAdd").hide();
        $("#TierPriceInputAdd").hide();
     }
     else if(PayoutCalculation=="TierPrice")
     {
       $("#RevsharePercentInputAdd").hide();
        $("#FixedPriceInputAdd").hide();
        $("#TierPriceInputAdd").show();
     }
     else if(PayoutCalculation=="FixedPrice")
     {
        $("#RevsharePercentInputAdd").hide();
        $("#FixedPriceInputAdd").show();
        $("#TierPriceInputAdd").hide();
     }
     else if(PayoutCalculation=="BucketPrice" || PayoutCalculation=="BucketSystemBySubID")
     {
        $("#RevsharePercentInputAdd").show();
        $("#FixedPriceInputAdd").show();
        $("#TierPriceInputAdd").hide();
     }
});
function SaveVendorDetails()
{
    var flag            = 1;
    var FormType        = $("#FormType").val();  
    var CampaignType    = $("#CampaignType").val();  
    var Campaign        = $("#CampaignEdit").val();  
    var Company         = $("#CompanyEdit").val();  
    var AdminLabel      = $("#AdminLabelEdit").val();  
    var PartnerLabel    = $("#PartnerLabelEdit").val();  
    var PayoutCalculation          = $("#PayoutCalculation").val();  
    var RevsharePercent = $("#RevshareEdit").val();  
    var FixedPrice      = $("#FixedPriceEdit").val();  
    var DailyCap        = $("#DailyCapEdit").val();  
    var TotalCap        = $("#TotalCapEdit").val();  
    var MaxTimeOut      = $("#MaxTimeOutEdit").val(); 
    var MaxPingTimeOut  = $("#MaxPingTimeOutEdit").val(); 
    var MaxPostTimeOut  = $("#MaxPostTimeOutEdit").val(); 
    var VendorStatus          = $("#VendorStatus").val();  
    var VendorTestMode        = $("#VendorTestMode").val();  
    var token           = $("#_token").val();
    var url             = $("#url").val();      

        if(Campaign=="")
        {
            $("#CampaignEditErr").css('color','red');
            $("#CampaignEditErr").html('Please Select Campaign.');
            $("#CampaignEditErr").show();
            flag =0;
        }
        if(Company=="")
        {
            $("#CompanyEditErr").css('color','red');
            $("#CompanyEditErr").html("Please Select Company.")
            $("#CompanyEditErr").show();
            flag =0;
        }
        if(PartnerLabel=="")
        {
            $("#PartnerLabelEditErr").css('color','red');
            $("#PartnerLabelEditErr").html('Please Enter Partner Label.');
            $("#PartnerLabelEditErr").show();
            flag =0;
        }
        if(AdminLabel=="")
        {
            $("#AdminLabelEditErr").css('color','red');
            $("#AdminLabelEditErr").html('Please Enter Admin Label.');
            $("#AdminLabelEditErr").show();
            flag =0;
        }               
        if(PayoutCalculation=="")
        {
            $("#PayoutCalculationErr").css('color','red');
            $("#PayoutCalculationErr").html('Please Enter Payout Calculation Type.');
            $("#PayoutCalculationErr").show();
            flag =0;
        }
        if(PayoutCalculation=="RevShare")
        {
            if(RevsharePercent=="")
            {
                $("#RevshareEditErr").css('color','red');
                $("#RevshareEditErr").html('Please Enter Revshare Percent.');
                $("#RevshareEditErr").show();
                flag =0;
            }
        }
        if(PayoutCalculation=="FixedPrice")
        {
            if(FixedPrice=="" )
            {
                $("#FixedPriceEditErr").css('color','red');
                $("#FixedPriceEditErr").html('Please Enter Fixed Price.');
                $("#FixedPriceEditErr").show();
                flag =0;
            }
        }
        if(DailyCap=="")
        {
            $("#DailyCapEditErr").css('color','red');
            $("#DailyCapEditErr").html('Required.');
            $("#DailyCapEditErr").show();
            flag =0;
        }
        if(TotalCap=="")
        {
            $("#TotalCapEditErr").css('color','red');
            $("#TotalCapEditErr").html('Required.');
            $("#TotalCapEditErr").show();
            flag =0;
        }
        if(CampaignType=="DirectPost")
        {
            if(MaxTimeOut=="")
            {
                $("#MaxTimeOutEditErr").css('color','red');
                $("#MaxTimeOutEditErr").html('Please Enter Max Time Out Values.');
                $("#MaxTimeOutEditErr").show();
                flag =0;
            }
            else if(MaxTimeOut!="")
            {
                if(isNaN(MaxTimeOut))
                {
                    $("#MaxTimeOutEditErr").css('color','red');
                    $("#MaxTimeOutEditErr").html('Max Time Out Values Should Be Numeric.');
                    $("#MaxTimeOutEditErr").show();
                    flag =0;
                }
            }
        }
        if(CampaignType=="PingPost")
        {
            if(MaxPingTimeOut=="")
            {
                $("#MaxPingTimeOutEditErr").css('color','red');
                $("#MaxPingTimeOutEditErr").html('Please Enter Max Time Out Values.');
                $("#MaxPingTimeOutEditErr").show();
                flag =0;
            }
            else if(MaxPingTimeOut!="")
            {
                if(isNaN(MaxPingTimeOut))
                {
                    $("#MaxPingTimeOutEditErr").css('color','red');
                    $("#MaxPingTimeOutEditErr").html('Max Time Out Values Should Be Numeric.');
                    $("#MaxPingTimeOutEditErr").show();
                    flag =0;
                }
            }
            if(MaxPostTimeOut=="")
            {
                $("#MaxPostTimeOutEditErr").css('color','red');
                $("#MaxPostTimeOutEditErr").html('Please Enter Max Time Out Values.');
                $("#MaxPostTimeOutEditErr").show();
                flag =0;
            }
            else if(MaxPostTimeOut!="")
            {
                if(isNaN(MaxPostTimeOut))
                {
                    $("#MaxPostTimeOutEditErr").css('color','red');
                    $("#MaxPostTimeOutEditErr").html('Max Time Out Values Should Be Numeric.');
                    $("#MaxPostTimeOutEditErr").show();
                    flag =0;
                }
            }              
        }
        if(flag ==1)
        {
            if(FormType=='Update')
            {
                $("#VendorUpdateForm").submit();
            }
            else if(FormType=='Clone')
            {
                if(AdminLabel!='')
                {
                    var IsVendorNameExist = IsVendorNameExistFunctionForClone();
                    if(IsVendorNameExist==true)
                    {
                        $("#AdminLabelEditErr").css('color','red');
                        $("#AdminLabelEditErr").html('Same Admin Label Already Exist.');
                        $("#AdminLabelEditErr").show();
                        flag=0;
                    }
                    else
                    {
                        $("#VendorUpdateForm").submit();
                    }
                }
            }            
        }
      
}

function GetCampaignDetailsEdit()
{
    var CampaignID = $("#CampaignEdit").val();
    var token = $("#_token").val();
    var url = $("#url").val();
    var DataStr = {_token : token,CampaignID:CampaignID};
    var result;
    $.ajax({
            type:"POST",
            url :url+"/GetCampaignDetails",
            data : DataStr,
            dataType : 'json',
            success:function(data)
            {
                if(data.CampaignType == 'DirectPost')
                { 
                    $("#CampaignTypeEdit").val(data.CampaignType);
                    $("#MaxTimeOutInputEdit").css('display','block');
                    $("#MaxPingTimeOutInputEdit").css('display','none');
                    $("#MaxPostTimeOutInputEdit").css('display','none');
                } 
                else if(data.CampaignType == 'PingPost')
                {
                    $("#CampaignTypeEdit").val(data.CampaignType);
                    $("#MaxTimeOutInputEdit").css('display','none');
                    $("#MaxPingTimeOutInputEdit").css('display','block');
                    $("#MaxPostTimeOutInputEdit").css('display','block');
                }
            }
    });
    return result;
}
function GetCampaignDetailsAdd()
{
    var CampaignID = $("#CampaignAdd").val();
    var token = $("#_token").val();
    var url = $("#url").val();
    var DataStr = {_token : token,CampaignID:CampaignID};
    var result;
    $.ajax({
            type:"POST",
            url :url+"/GetCampaignDetails",
            data : DataStr,
            dataType : 'json',
            success:function(data)
            {
                if(data.CampaignType == 'DirectPost')
                { 
                    $("#CampaignTypeAdd").val(data.CampaignType);
                    $("#MaxTimeOutInputAdd").css('display','block');
                    $("#MaxPingTimeOutInputAdd").css('display','none');
                    $("#MaxPostTimeOutInputAdd").css('display','none');
                } 
                else if(data.CampaignType == 'PingPost')
                {
                    $("#CampaignTypeAdd").val(data.CampaignType);
                    $("#MaxTimeOutInputAdd").css('display','none');
                    $("#MaxPingTimeOutInputAdd").css('display','block');
                    $("#MaxPostTimeOutInputAdd").css('display','block');
                }
            }
    });
    return result;
}
function SetSessionForCloneVendor(VendorID)
{
    var token           = $("#_token").val();
    var url             = $("#url").val();
   
    var DataStr         = {'_token':token,'VendorID':VendorID};
    $.ajax({
        type: "POST",
        url:  url+"/SetSessionForCloneVendor",
        data: DataStr,
        success: function(data) 
        {
            if(data==VendorID)
            {
                window.location = url+"/add-vendor";
            }   
            else
            {
                $("#ErrMsg").html('<div class="alert alert-danger alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Something Wrong! Please Try Again.</strong></div>');
                $("#ErrMsg").show();
            }     
        }
    });
}

 function updateTextBox(token_value)
{        
    var $txt = jQuery("#TrackingPixelCode");
    var caretPos = $txt[0].selectionStart;
    var textAreaTxt = $txt.val();
    var txtToAdd = "[--" + token_value.value + "--]";
    $txt.val(textAreaTxt.substring(0, caretPos) + txtToAdd + textAreaTxt.substring(caretPos) );
    //$('#tracking_pixel_code').append(token_value.value +"="+ "[--" + token_value.value + "--]");
}


function SendMailToAdminUser()
{

    var Company = $("#CompanyEdit").val();
    var Campaign = $("#CampaignEdit").val();
    var VendorID = $("#VendorID").val();
    var VendorApiKey = $("#VendorApiKey").val();
    var AdminLabel = $("#AdminLabelEdit").val();

    var token = $("#_token").val();
    var url = $("#url").val();
    var DataStr = {'_token': token,'Company':Company,'Campaign':Campaign,'VendorID':VendorID,'VendorApiKey':VendorApiKey,'AdminLabel':AdminLabel};

    $.ajax({
        type: "POST",
        url:url+ "/SendEmailToAdminUser",
        data: DataStr,
        success: function(response) 
        {
            $("#MessageEdit").html(response);
        }
     });
}
///////////////////////////////////
function AddVendorModal()
{
    $("#CampaignAddErr").hide();  
    $("#CompanyAddErr").hide();  
    $("#AdminLabelAddErr").hide();  
    $("#PartnerLabelAddErr").hide();  
    $("#PayoutCalculationAddErr").hide();  
    $("#RevshareAddErr").hide();  
    $("#FixedPriceAddErr").hide();  
    $("#DailyCapAddErr").hide();  
    $("#TotalCapAddErr").hide();  
    $("#MaxTimeOutAddErr").hide(); 
    $("#MaxPingTimeOutAddErr").hide(); 
    $("#MaxPostTimeOutAddErr").hide();
    
    $("#AddVendorModal").modal('show');
}
function GetVendorDetails(VendorID)
{
    var token = $("#_token").val();
    var url = $("#url").val();
    
    var DataStr = {'_token': token, 'VendorID': VendorID};
    $.ajax({
        type: "POST",
        url: url + "/GetVendorDetails",
        data: DataStr,
        success: function (data)
        {
            $("#EditVendorDiv").html(data);     
            $("#EditVendorModal").modal('show');       
        }
    });
}
function GetVendorCloneDetails(VendorID)
{
    var token = $("#_token").val();
    var url = $("#url").val();
    
    var DataStr = {'_token': token, 'VendorID': VendorID};
    $.ajax({
        type: "POST",
        url: url + "/GetVendorCloneDetails",
        data: DataStr,
        success: function (data)
        {
            $("#CloneVendorDiv").html(data);     
            $("#CloneVendorModal").modal('show');       
        }
    });
}
function AddVendorDetails()
{
    var flag            = 1;
    var CampaignType    = $("#CampaignType").val();  
    var Campaign        = $("#CampaignAdd").val();  
    var Company         = $("#CompanyAdd").val();  
    var AdminLabel      = $("#AdminLabelAdd").val();  
    var PartnerLabel    = $("#PartnerLabelAdd").val();  
    var PayoutCalculation          = $("#PayoutCalculationAdd").val();  
    var RevsharePercent = $("#RevshareAdd").val();  
    var FixedPrice      = $("#FixedPriceAdd").val();  
    var DailyCap        = $("#DailyCapAdd").val();  
    var TotalCap        = $("#TotalCapAdd").val();  
    var MaxTimeOut      = $("#MaxTimeOutAdd").val(); 
    var MaxPingTimeOut  = $("#MaxPingTimeOutAdd").val(); 
    var MaxPostTimeOut  = $("#MaxPostTimeOutAdd").val(); 
    var VendorStatus          = $("#VendorStatus").val();  
    var VendorTestMode        = $("#VendorTestMode").val();  
    var token           = $("#_token").val();
    var url             = $("#url").val();      

    if(Campaign=="")
    {
        $("#CampaignAddErr").css('color','red');
        $("#CampaignAddErr").html('Please Select Campaign.');
        $("#CampaignAddErr").show();
        flag =0;
    }
    if(Company=="")
    {
        $("#CompanyAddErr").css('color','red');
        $("#CompanyAddErr").html("Please Select Company.")
        $("#CompanyAddErr").show();
        flag =0;
    }
    if(PartnerLabel=="")
    {
        $("#PartnerLabelAddErr").css('color','red');
        $("#PartnerLabelAddErr").html('Please Enter Partner Label.');
        $("#PartnerLabelAddErr").show();
        flag =0;
    }
    if(AdminLabel=="")
    {
        $("#AdminLabelAddErr").css('color','red');
        $("#AdminLabelAddErr").html('Please Enter Admin Label.');
        $("#AdminLabelAddErr").show();
        flag =0;
    }               
    if(PayoutCalculation=="")
    {
        $("#PayoutCalculationErr").css('color','red');
        $("#PayoutCalculationErr").html('Please Enter Payout Calculation Type.');
        $("#PayoutCalculationErr").show();
        flag =0;
    }
    if(PayoutCalculation=="RevShare")
    {
        if(RevsharePercent=="")
        {
            $("#RevshareAddErr").css('color','red');
            $("#RevshareAddErr").html('Please Enter Revshare Percent.');
            $("#RevshareAddErr").show();
            flag =0;
        }
    }
    if(PayoutCalculation=="FixedPrice")
    {
        if(FixedPrice=="" )
        {
            $("#FixedPriceAddErr").css('color','red');
            $("#FixedPriceAddErr").html('Please Enter Fixed Price.');
            $("#FixedPriceAddErr").show();
            flag =0;
        }
    }
    if(DailyCap=="")
    {
        $("#DailyCapAddErr").css('color','red');
        $("#DailyCapAddErr").html('Required.');
        $("#DailyCapAddErr").show();
        flag =0;
    }
    if(TotalCap=="")
    {
        $("#TotalCapAddErr").css('color','red');
        $("#TotalCapAddErr").html('Required.');
        $("#TotalCapAddErr").show();
        flag =0;
    }
    if(CampaignType=="DirectPost")
    {
        if(MaxTimeOut=="")
        {
            $("#MaxTimeOutAddErr").css('color','red');
            $("#MaxTimeOutAddErr").html('Please Enter Max Time Out Values.');
            $("#MaxTimeOutAddErr").show();
            flag =0;
        }
        else if(MaxTimeOut!="")
        {
            if(isNaN(MaxTimeOut))
            {
                $("#MaxTimeOutAddErr").css('color','red');
                $("#MaxTimeOutAddErr").html('Max Time Out Values Should Be Numeric.');
                $("#MaxTimeOutAddErr").show();
                flag =0;
            }
        }
    }
    if(CampaignType=="PingPost")
    {
        if(MaxPingTimeOut=="")
        {
            $("#MaxPingTimeOutAddErr").css('color','red');
            $("#MaxPingTimeOutAddErr").html('Please Enter Max Time Out Values.');
            $("#MaxPingTimeOutAddErr").show();
            flag =0;
        }
        else if(MaxPingTimeOut!="")
        {
            if(isNaN(MaxPingTimeOut))
            {
                $("#MaxPingTimeOutAddErr").css('color','red');
                $("#MaxPingTimeOutAddErr").html('Max Time Out Values Should Be Numeric.');
                $("#MaxPingTimeOutAddErr").show();
                flag =0;
            }
        }
        if(MaxPostTimeOut=="")
        {
            $("#MaxPostTimeOutAddErr").css('color','red');
            $("#MaxPostTimeOutAddErr").html('Please Enter Max Time Out Values.');
            $("#MaxPostTimeOutAddErr").show();
            flag =0;
        }
        else if(MaxPostTimeOut!="")
        {
            if(isNaN(MaxPostTimeOut))
            {
                $("#MaxPostTimeOutAddErr").css('color','red');
                $("#MaxPostTimeOutAddErr").html('Max Time Out Values Should Be Numeric.');
                $("#MaxPostTimeOutAddErr").show();
                flag =0;
            }
        }              
    }
    if(flag ==1)
    {
        if(AdminLabel!='')
        {
            var IsVendorNameExist = IsVendorNameExistFunction();
            if(IsVendorNameExist==true)
            {
                $("#AdminLabelAddErr").css('color','red');
                $("#AdminLabelAddErr").html('Same Admin Label Already Exist.');
                $("#AdminLabelAddErr").show();
                flag=0;
            }
            else
            {
                $("#VendorAddForm").submit();
            }
        }
        
    }
      
}
function IsVendorNameExistFunction()
{
    var VendorName       = $("#AdminLabelAdd").val(); 
    var token = $("#_token").val();
    var url = $("#url").val();
    var DataStr = {_token : token,'VendorName':VendorName};
    var result;
    $.ajax({
            type:"POST",
            url :url+"/IsVendorNameExist",
            data : DataStr,
            async : false,
            success:function(data)
            {
               result = data;
            }
    });
    return result;
}
function IsVendorNameExistFunctionForClone()
{
    var VendorName       = $("#AdminLabelEdit").val(); 
    var token = $("#_token").val();
    var url = $("#url").val();
    var DataStr = {_token : token,'VendorName':VendorName};
    var result;
    $.ajax({
            type:"POST",
            url :url+"/IsVendorNameExist",
            data : DataStr,
            async : false,
            success:function(data)
            {
               result = data;
            }
    });
    return result;
}