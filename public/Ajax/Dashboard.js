function RedirectToLead(Campaign,Status,Where)
{
    var token           = $("#_token").val();
    var url             = $("#url").val(); 
    var DataStr         = {'_token':token,'Campaign':Campaign,'Status':Status,'Where':Where};
    $.ajax({
        type: "POST",
        url:  url+"/RedirectToLead",
        data: DataStr,
        dataType: 'json',
        success: function(data) 
        {
            console.log(data); 
            if(data.Status=='1')
            {
                window.location.href = url+"/"+data.URL;
            }   
            else
            {
                alert('Something Wrong Please Refresh Your Page.');
            }     
        }
    }); 
}


