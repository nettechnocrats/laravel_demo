$(document).ready(function() {    
    GetErrorLogList(1);
});
function Pagination(page)
{
    GetErrorLogList(page);
}
function GetErrorLogList(page)
{
    var numofrecords    = $('#numofrecords').val();
    var loading=$("#loading").val();
    if(numofrecords==10)
    {
        $("#dataTable_wrapper").html('<center><img src="'+loading+'" style="width: 200px"><input type="hidden" name="numofrecords" id="numofrecords" value="10"></center>'); 
    }
    else
    {
        $("#dataTable_wrapper").html('<center><img src="'+loading+'" style="width: 200px"></center>'); 
    }
    var token           = $("#_token").val();
    var url             = $("#url").val();   
    
    var DataStr         = {'_token':token,'page':page,'numofrecords':numofrecords};
    $.ajax({
        type: "POST",
        url:  url+"/GetErrorLogList",
        data: DataStr,
        success: function(data) 
        {
            $("#dataTable_wrapper").html(data);         
        }
    }); 
}
