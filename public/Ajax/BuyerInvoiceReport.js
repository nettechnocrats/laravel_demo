$(document).ready(function() {    
    GetBuyerInvoiceReport();
    GetVendorBuyerUsingCampaignID();
});
function SearchBuyerInvoiceReport()
{
    GetBuyerInvoiceReport();
}
function GetBuyerInvoiceReport()
{
    var loading=$("#loading").val();
    $("#dataTable").html('<center><img src="'+loading+'" style="width: 200px"></center>'); 

    var token           = $("#_token").val();
    var url             = $("#url").val(); 
    var ShowAll         = 0;
    if($('#ShowAll').prop("checked") == true){
        ShowAll=1;
    }
    var CampaignID      = $("#Campaign").val();
    var BuyerCompanyID  = $("#BuyerCompany").val();
    var VendorCompanyID = $("#VendorCompany").val();
    var VendorID        = $("#Vendor").val();   
    var Tiers   = $("#Tiers").val();   
    
    var StartDate       = $("#StartDate").val();
    var EndDate         = $("#EndDate").val();
    
    var DataStr         = {'_token':token,'ShowAll':ShowAll,'CampaignID':CampaignID,'StartDate':StartDate,'EndDate':EndDate,'VendorCompanyID':VendorCompanyID,'VendorID':VendorID,'BuyerCompanyID':BuyerCompanyID,'Tiers':Tiers};
    $.ajax({
        type: "POST",
        url:  url+"/GetBuyerInvoiceReport",
        data: DataStr,
        success: function(data) 
        {
            $("#dataTable").html(data);         
        }
    }); 
}
function GetVendorBuyerUsingCampaignID()
{ 
    var token           = $("#_token").val();
    var url             = $("#url").val(); 

    var CampaignID      = $("#Campaign").val();     
    var DataStr         = {'_token':token,'CampaignID':CampaignID};
    $.ajax({
        type: "POST",
        url:  url+"/GetVendorBuyerUsingCampaignID",
        data: DataStr,
        dataType: 'json',
        success: function(data) 
        {
            $("#BuyerCompany").html(data.BuyerCompany);
            $("#VendorCompany").html(data.VendorCompany);
            $("#Vendor").html(data.Vendor);       
        }
    }); 
}

function GetVendorUsingVendorCompany()
{
    var VendorCompanyID = $("#VendorCompany").val();
    var CampaignID      = $("#Campaign").val(); 
    var token = $("#_token").val();
    var url = $("#url").val();

    var DataStr = {'_token': token, 'VendorCompanyID': VendorCompanyID,'CampaignID':CampaignID};
    $.ajax({
        type: "POST",
        url: url + "/GetVendorUsingVendorCompany",
        data: DataStr,
        success: function (data)
        {
            $("#Vendor").html(data);
        }
    });
}

