$(document).ready(function(){
    GetVendorBuyerUsingCampaignID();
    BuyerResponse();
    GetBuyerResponseList(1);
});
$("#Campaign").change(function(){
    GetVendorBuyerUsingCampaignID();
    BuyerResponse();
});
$("#Action").change(function(){
    BuyerResponse();
});

function GetVendorBuyerUsingCampaignID()
{ 
    var token           = $("#_token").val();
    var url             = $("#url").val(); 

    var CampaignID      = $("#Campaign").val();     
    var DataStr         = {'_token':token,'CampaignID':CampaignID};
    $.ajax({
        type: "POST",
        url:  url+"/GetVendorBuyerUsingCampaignID",
        data: DataStr,
        dataType: 'json',
        success: function(data) 
        {
            $("#BuyerCompany").html(data.BuyerCompany);
            $("#Buyer").html(data.Buyer);
            if(data.CampaignType=='DirectPost')
            {
                $("#Action .pp").prop('disabled',true);
                $("#Action .dp").prop('disabled',false);
                $('#Action').val('DirectPost'); 
            }   
            else if(data.CampaignType=='PingPost') 
            {
                $("#Action .pp").prop('disabled',false);
                $("#Action .dp").prop('disabled',true);
                $('#Action').val('Ping'); 
            }    
        }
    }); 
}
function GetBuyerUsingVendorCompany()
{
    var BuyerCompanyID = $("#BuyerCompany").val();
    var CampaignID      = $("#Campaign").val(); 
    var token = $("#_token").val();
    var url = $("#url").val();

    var DataStr = {'_token': token, 'BuyerCompanyID': BuyerCompanyID,'CampaignID':CampaignID};
    $.ajax({
        type: "POST",
        url: url + "/GetBuyerUsingVendorCompany",
        data: DataStr,
        success: function (data)
        {
            $("#Buyer").html(data);
        }
    });
}
function BuyerResponse() {
    var token           = $("#_token").val();
    var url             = $("#url").val();
    var CampaignID      = $("#Campaign").val();
    var Action          = $("#Action").val();

    var DataStr = {'_token':token,'CampaignID':CampaignID,'Action':Action};

    $.ajax({
        type: "POST",
        url:  url+"/BuyerResponse",
        data: DataStr,
        success: function(data)
        {
           $("#Response").html(data);
        }
    });
}
function SearchBuyerResponseList()
{
    GetBuyerResponseList(1);
}
function Pagination(page)
{
    GetBuyerResponseList(page);
}
function GetBuyerResponseList(page) {
    
    var numofrecords    = $('#numofrecords').val();
    var loading=$("#loading").val();
    if(numofrecords==10)
    {
        $("#dataTable_wrapper").html('<center><img src="'+loading+'" style="width: 200px"><input type="hidden" name="numofrecords" id="numofrecords" value="10"></center>'); 
    }
    else
    {
        $("#dataTable_wrapper").html('<center><img src="'+loading+'" style="width: 200px"></center>'); 
    }

    var token           = $("#_token").val();
    var url             = $("#url").val();
    var CampaignID      = $("#Campaign").val();
    var BuyerCompany    = $("#BuyerCompany").val();
    var Buyer           = $("#Buyer").val();
    var Response        = $("#Response").val();
    var StartDate       = $("#StartDate").val();
    var EndDate         = $("#EndDate").val();
    var Sort            = $("#Sort").val();
    var Action          = $("#Action").val();
    
    var DataStr = {
            '_token':token,
            'CampaignID':CampaignID,
            'BuyerCompany':BuyerCompany,
            'Buyer':Buyer,
            'Response':Response,
            'Sort':Sort,
            'Action':Action,
            'StartDate':StartDate,
            'EndDate':EndDate,
            'numofrecords':numofrecords,
            'page':page
    };

    $.ajax({
        type: "POST",
        url:  url+"/GetBuyerResponseList",
        data: DataStr,
        success: function(data)
        {
           $("#dataTable_wrapper").html(data);
        }
    });
}

