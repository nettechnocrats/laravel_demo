$(document).ready(function() {    
    GetExportReport(1);
    IsExportCSVOrNot(1);
});
function Pagination(page)
{
    GetExportReport(page);
}
function SearchExportReport()
{
    GetExportReport(1);
    IsExportCSVOrNot(1);
}
function GetExportReport(page)
{
    var numofrecords    = $('#numofrecords').val();
    var loading=$("#loading").val();
    if(numofrecords==10)
    {
        $("#ExportReport").html('<center><img src="'+loading+'" style="width: 200px"><input type="hidden" name="numofrecords" id="numofrecords" value="10"></center>'); 
    }
    else
    {
        $("#ExportReport").html('<center><img src="'+loading+'" style="width: 200px"></center>'); 
    }
    var token           = $("#_token").val();
    var url             = $("#url").val(); 

    var CampaignID      = $("#Campaign").val();
    var StartDate       = $("#start").val();
    var EndDate         = $("#end").val();
    
    var DataStr         = {'_token':token,'page':page,'numofrecords':numofrecords,'CampaignID':CampaignID,'StartDate':StartDate,'EndDate':EndDate};
    $.ajax({
        type: "POST",
        url:  url+"/GetExportReport",
        data: DataStr,
        success: function(data) 
        {
            $("#ExportReport").html(data);          
        }
    }); 
}

function IsExportCSVOrNot(page)
{
    var loading=$("#loading").val();

    var token           = $("#_token").val();
    var url             = $("#url").val(); 

    var CampaignID      = $("#Campaign").val();
    var StartDate       = $("#start").val();
    var EndDate         = $("#end").val();

    var numofrecords    = $('#numofrecords').val();
    
    var DataStr         = {'_token':token,'page':page,'numofrecords':numofrecords,'CampaignID':CampaignID,'StartDate':StartDate,'EndDate':EndDate};
    $.ajax({
        type: "POST",
        url:  url+"/IsExportCSVOrNot",
        data: DataStr,
        success: function(data) 
        {                           
            $("#DownloadBtn").html(data);
        }
    }); 
}

function EmailExportReturnCSV(CampaignID,startDate,endDate,VendorCompany)
{
    
    var token           = $("#_token").val();
    var url             = $("#url").val(); 
    
    var DataStr         = {'_token':token,'CampaignID':CampaignID,'StartDate':startDate,'EndDate':endDate,'VendorCompany':VendorCompany};
    $.ajax({
        type: "POST",
        url:  url+"/EmailExportReturnCSV",
        data: DataStr,
        success: function(data) 
        {
            $("#ErrorMessage").html(data);       
        }
    }); 
}
