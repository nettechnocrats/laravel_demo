
function Pagination(page)
{
    GetCampaignList(page);
}
function SearchCampaignList()
{
    GetCampaignList(1);
}
function GetCampaignList(page)
{
    var numofrecords    = $('#numofrecords').val();
    var loading=$("#loading").val();
    if(numofrecords==10)
    {
        $("#dataTable_wrapper").html('<center><img src="'+loading+'" style="width: 200px"><input type="hidden" name="numofrecords" id="numofrecords" value="10"></center>'); 
    }
    else
    {
        $("#dataTable_wrapper").html('<center><img src="'+loading+'" style="width: 200px"></center>'); 
    }
    var token           = $("#_token").val();
    var url             = $("#url").val(); 

    var CampaignName    = $("#CampaignName").val();
    var CampaignType    = $("#CampaignType").val();
    var MaintenanceMode = $("#MaintenanceMode").val();
    
    var DataStr         = {'_token':token,'page':page,'numofrecords':numofrecords,'CampaignName':CampaignName,'CampaignType':CampaignType,'MaintenanceMode':MaintenanceMode};
    $.ajax({
        type: "POST",
        url:  url+"/GetCampaignList",
        data: DataStr,
        success: function(data) 
        {
            $("#dataTable_wrapper").html(data);         
        }
    }); 
}

function HideErr(id)
{
    $("#"+id).hide();
}

$("#CampaignAddBtn").click(function(){
    var flag=1;
    var CampaignName = $("#CampaignName").val();
    var CampaignType = $("#CampaignType").val();
   
    if(CampaignName=="")
    {
        $("#CampaignNameErr").css("color",'red');
        $("#CampaignNameErr").html("Please Enter Campaign Name.");
        $("#CampaignNameErr").show();
        flag=0;
    }
    if(CampaignType=="")
    {
        $("#CampaignTypeErr").css("color",'red');
        $("#CampaignTypeErr").html("Please Select Campaign Type.");
        $("#CampaignTypeErr").show();
        flag=0;
    }   
    
    if(flag==1)
    {
        $("#CampaignAddForm").submit();
    } 
});


$("#CampaignEditBtn").click(function(){
    var flag=1;
    var CampaignName = $("#CampaignName").val();
    var CampaignType = $("#CampaignType").val();
   
    if(CampaignName=="")
    {
        $("#CampaignNameErr").css("color",'red');
        $("#CampaignNameErr").html("Please Enter Campaign Name.");
        $("#CampaignNameErr").show();
        flag=0;
    }
    if(CampaignType=="")
    {
        $("#CampaignTypeErr").css("color",'red');
        $("#CampaignTypeErr").html("Please Select Campaign Type.");
        $("#CampaignTypeErr").show();
        flag=0;
    }   
    
    if(flag==1)
    {
        $("#CampaignEditForm").submit();
    } 
});
   
function ChangeMaintenanceMode(CID)
{
    var MaintenanceMode = '';
    if($("#MaintenanceMode_"+CID).prop("checked") == true){
        MaintenanceMode = '1';
    }
    else if($("#MaintenanceMode_"+CID).prop("checked") == false){
        MaintenanceMode = '0';
    }
    var token           = $("#_token").val();
    var url             = $("#url").val(); 

    
    var DataStr         = {'_token':token,'CID':CID,'MaintenanceMode':MaintenanceMode};
    $.ajax({
        type: "POST",
        url:  url+"/ChangeMaintenanceMode",
        data: DataStr,
        dataType: 'json',
        success: function(data) 
        {
            if(data.Status==1)
            {
                $("#ResponseMessage").html(data.Msg);
            }  
            else
            {
                $("#ResponseMessage").html(data.Msg);
                if(MaintenanceMode=='1')
                {
                    $$("#MaintenanceMode_"+CID).prop('checked', false);
                }
                else if(MaintenanceMode=='0')
                {
                    $("#MaintenanceMode_"+CID).prop('checked', true);
                }
            }
        }
    }); 
} 

function ChangeShowInDashboardMode(CID)
{
    var ShowInDashboard = '';
    if($("#ShowInDashboard_"+CID).prop("checked") == true){
        ShowInDashboard = '1';
    }
    else if($("#ShowInDashboard_"+CID).prop("checked") == false){
        ShowInDashboard = '0';
    }
    var token           = $("#_token").val();
    var url             = $("#url").val(); 

    
    var DataStr         = {'_token':token,'CID':CID,'ShowInDashboard':ShowInDashboard};
    $.ajax({
        type: "POST",
        url:  url+"/ChangeShowInDashboardMode",
        data: DataStr,
        dataType: 'json',
        success: function(data) 
        {
            if(data.Status==1)
            {
                $("#ResponseMessage").html(data.Msg);
            }  
            else
            {
                $("#ResponseMessage").html(data.Msg);
                if(MaintenanceMode=='1')
                {
                    $$("#ShowInDashboard_"+CID).prop('checked', false);
                }
                else if(MaintenanceMode=='0')
                {
                    $("#ShowInDashboard_"+CID).prop('checked', true);
                }
            }
        }
    }); 
}
