$(document).ready(function() {    
    GetBuyersUsingCampaignID();
    GetViewProcessedLeadReport(1);
});
function Pagination()
{
    var page = $("#Pagination").val();
    GetViewProcessedLeadReport(page);
}
function SearchViewProcessedLeadReport()
{   
    GetViewProcessedLeadReport(1);
}
function GetViewProcessedLeadReport(page)
{
    var numofrecords    = $('#numofrecords').val();
    var loading=$("#loading").val();
    if(numofrecords==10)
    {
        $("#Lead").html('<center><img src="'+loading+'" style="width: 200px"><input type="hidden" name="numofrecords" id="numofrecords" value="10"></center>'); 
    }
    else
    {
        $("#Lead").html('<center><img src="'+loading+'" style="width: 200px"></center>'); 
    }
    var token           = $("#_token").val();
    var url             = $("#url").val(); 

    var CampaignID      = $("#Campaign").val();
    var TierID          = $("#Tiers").val();
    var BuyerID         = $("#Buyer").val();
    var StartDate       = $("#StartDate").val();
    var EndDate         = $("#EndDate").val();
    var Redirected      = $("#Redirected").val();
    var Status          = $("#Status").val();
    var PingPostStatus  = $("#PingPostStatus").val();

    var SubID           = $("#SubID").val();
    var Email           = $("#Email").val();
    var LeadID          = $("#LeadID").val();
    
    var DataStr         = {'_token':token,'page':page,'numofrecords':numofrecords,'CampaignID':CampaignID,'StartDate':StartDate,'EndDate':EndDate,'BuyerID':BuyerID, 'Redirected':Redirected,'Status':Status,'PingPostStatus':PingPostStatus,'SubID':SubID,'Email':Email,'LeadID':LeadID};
    $.ajax({
        type: "POST",
        url:  url+"/GetViewPurchasedLeadReport",
        data: DataStr,
        success: function(data) 
        {
            $("#Lead").html(data);         
        }
    }); 
}

function GetBuyersUsingCampaignID()
{
    var CampaignID = $("#Campaign").val();
    var token = $("#_token").val();
    var url = $("#url").val();

    var DataStr = {'_token': token, 'CampaignID': CampaignID};
    $.ajax({
        type: "POST",
        url: url + "/GetBuyersUsingCampaignID",
        dataType: 'json',
        data: DataStr,
        success: function (data)
        {
            $("#Buyer").html(data.Buyer);
            if(data.CampaignType=="DirectPost")
            {
                $("#RedirectedDiv").show();
                $("#TiersDiv").show();
                $("#PingPostStatusDiv").hide();
                $("#SearchBtnDiv").removeClass('offset-md-2');
            }
            else
            {
                $("#RedirectedDiv").hide();
                $("#TiersDiv").hide();
                $("#PingPostStatusDiv").show();
                $("#SearchBtnDiv").addClass('offset-md-2');
            }
        }
    });
}

function OpenReturnModal(LeadID,CampaignID)
{
    $("#LeadID").html(LeadID);
    var token = $("#_token").val();
    var url = $("#url").val();

    var DataStr = {'_token': token, 'LeadID': LeadID, 'CampaignID': CampaignID};
    $.ajax({
        type: "POST",
        url: url + "/GetReturnModalUsingLeadID",
        data: DataStr,
        success: function (data)
        {
            $("#ReturnDiv").html(data);
            $("#myModal").modal('show');
        }
    });
}

function ValidateReturnCode(LeadID,CampaignID)
{   
    var flag            = 1;
    var ReturnCode      = $("#ReturnCode").val();  
    var Reason          = $("#Reason").val();  
    var token           = $("#_token").val();
    var url             = $("#url").val();  

    if(ReturnCode=="")
    {
        $("#ReturnCodeErr").css('color','red');
        $("#ReturnCodeErr").html('Please Select Return Code.');
        $("#ReturnCodeErr").show();
        flag =0;
    }
    if(Reason=="")
    {
        $("#ReasonErr").css('color','red');
        $("#ReasonErr").html('Please Enter Return Reason.');
        $("#ReasonErr").show();
        flag =0;
    }
    if(flag ==1)
    {        
        var DataStr = {'_token': token,'LeadID': LeadID,'CampaignID':CampaignID,'ReturnCode':ReturnCode,'Reason':Reason};
        $.ajax({
            type: "POST",
            url: url + "/AddReturnCode",
            data: DataStr,
            dataType: 'json',
            success: function (data)
            {
               if(data.LeadReturnStatus=='1')
               {   
                    $("#CloseBtn").trigger('click');
                    $("#ReturnCodeMsgg").html(data.Msg);
                    GetViewPurchasedLeadReport(1)
               }
               else
               {
                    $("#ReturnCodeMsg").html(data.Msg);
               }
            }
        });
    }
}

function HideErr(id)
{
    $("#"+id).hide();
}


function ActionTask(LeadID,CampaignID)
{
    var val = $("#ActionDrp").val();
    if(val=="ViewDetails")
    {
    $("#ViewDetails_"+LeadID).click();
    }
    else if(val=="Return")
    {
    OpenReturnModal(LeadID,CampaignID);
    }
}