$("#LoginBtn").click(function()
{
    var flag            =1;
    var email           = $("#email").val();  
    var password        = $("#password").val();  
    var token           = $("#_token").val();
    var url             = $("#url").val();  

    if(email=="")
    {
        $("#emailErr").css('color','red');
        $("#emailErr").html('Please Enter E-mail.');
        $("#emailErr").show();
        flag =0;
    }
    else if(email!="")
    {
        if(!validateEmail(email))
        {
            $("#emailErr").css('color','red');
            $("#emailErr").html('Please Enter Valid E-mail');
            $("#emailErr").show();
            flag=0;
        }
    }
    if(password=="")
    {
        $("#passwordErr").css('color','red');
        $("#passwordErr").html('Please Enter Password.');
        $("#passwordErr").show();
        flag =0;
    }
    if(flag ==1)
    {        
        $("#LoginForm").submit();
    }

});
function HideErr(id)
{
    $("#"+id).hide();
}
function validateEmail(email)
{
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}
