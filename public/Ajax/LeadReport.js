$(document).ready(function() {    
    GetVendorBuyerUsingCampaignID();
    GetLeadReport(1);
});
function Pagination()
{
    var page = $("#Pagination").val();
    GetLeadReport(page);
}
function SearchViewProcessedLeadReport()
{   
    GetLeadReport(1);
}
function GetLeadReport(page)
{
    var numofrecords    = $('#numofrecords').val();
    var loading=$("#loading").val();
    if(numofrecords==10)
    {
        $("#Lead").html('<center><img src="'+loading+'" style="width: 200px"><input type="hidden" name="numofrecords" id="numofrecords" value="10"></center>'); 
    }
    else
    {
        $("#Lead").html('<center><img src="'+loading+'" style="width: 200px"></center>'); 
    }

    var token           = $("#_token").val();
    var url             = $("#url").val(); 

    var CampaignID      = $("#Campaign").val();
    var TierID          = $("#Tiers").val();
    var StartDate       = $("#StartDate").val();
    var EndDate         = $("#EndDate").val();
    var Redirected      = $("#Redirected").val();
    var Status          = $("#Status").val();
    var PingPostStatus  = $("#PingPostStatus").val();


    var VendorCompanyID = $("#VendorCompany").val();
    var VendorID        = $("#Vendor").val();
    var BuyerCompanyID  = $("#BuyerCompany").val();
    var BuyerID         = $("#Buyer").val();

    var SubID           = $("#SubID").val();
    var Email           = $("#Email").val();
    var LeadID          = $("#LeadID").val();

    
    var DataStr         = {'_token':token,'page':page,'numofrecords':numofrecords,'CampaignID':CampaignID,'StartDate':StartDate,'EndDate':EndDate, 'Redirected':Redirected,'Status':Status,'PingPostStatus':PingPostStatus,'SubID':SubID,'Email':Email,'LeadID':LeadID,'VendorCompanyID':VendorCompanyID,'VendorID':VendorID,'BuyerCompanyID':BuyerCompanyID,'BuyerID':BuyerID};
    $.ajax({
        type: "POST",
        url:  url+"/GetLeadReport",
        data: DataStr,
        success: function(data) 
        {
            $("#Lead").html(data);         
        }
    }); 
}

function OpenReturnModal(LeadID,CampaignID)
{
    $("#LeadID").html(LeadID);
    var token = $("#_token").val();
    var url = $("#url").val();

    var DataStr = {'_token': token, 'LeadID': LeadID, 'CampaignID': CampaignID};
    $.ajax({
        type: "POST",
        url: url + "/GetReturnModalUsingLeadID",
        data: DataStr,
        success: function (data)
        {
            $("#ReturnDiv").html(data);
            $("#myModal").modal('show');
        }
    });
}

function ValidateReturnCode(LeadID,CampaignID)
{   
    var flag            = 1;
    var ReturnCode      = $("#ReturnCode").val();  
    var Reason          = $("#Reason").val();  
    var token           = $("#_token").val();
    var url             = $("#url").val();  

    if(ReturnCode=="")
    {
        $("#ReturnCodeErr").css('color','red');
        $("#ReturnCodeErr").html('Please Select Return Code.');
        $("#ReturnCodeErr").show();
        flag =0;
    }
    if(Reason=="")
    {
        $("#ReasonErr").css('color','red');
        $("#ReasonErr").html('Please Enter Return Reason.');
        $("#ReasonErr").show();
        flag =0;
    }
    if(flag ==1)
    {        
        var DataStr = {'_token': token,'LeadID': LeadID,'CampaignID':CampaignID,'ReturnCode':ReturnCode,'Reason':Reason};
        $.ajax({
            type: "POST",
            url: url + "/AddReturnCode",
            data: DataStr,
            dataType: 'json',
            success: function (data)
            {
               if(data.LeadReturnStatus=='1')
               {   
                    $("#CloseBtn").trigger('click');
                    $("#ReturnCodeMsgg").html(data.Msg);   
                    GetLeadReport(1);                 
               }
               else
               {
                    $("#ReturnCodeMsg").html(data.Msg);
               }

            }
        });
    }
}

function HideErr(id)
{
    $("#"+id).hide();
}


/////////////////
function GetVendorBuyerUsingCampaignID()
{
    var CampaignID = $("#Campaign").val();
    var token = $("#_token").val();
    var url = $("#url").val();

    var DataStr = {'_token': token, 'CampaignID': CampaignID};
    $.ajax({
        type: "POST",
        url: url + "/GetVendorBuyerUsingCampaignID",
        dataType: 'json',
        data: DataStr,
        success: function (data)
        {
            $("#VendorCompany").html(data.VendorCompany);
            $("#Vendor").html(data.Vendor);
            $("#BuyerCompany").html(data.BuyerCompany);
            $("#Buyer").html(data.Buyer);
            if(data.CampaignType=="DirectPost")
            {
                $("#RedirectedDiv").show();
                $("#TiersDiv").show();
                $("#PingPostStatusDiv").hide();
                //$("#SearchBtnDiv").removeClass('offset-md-2');
            }
            else
            {
                $("#RedirectedDiv").hide();
                $("#TiersDiv").hide();
                $("#PingPostStatusDiv").show();
                //$("#SearchBtnDiv").addClass('offset-md-2');
            }
        }
    });
}

function GetVendorUsingVendorCompany()
{
    var VendorCompanyID = $("#VendorCompany").val();
    var CampaignID = $("#Campaign").val();
    var token = $("#_token").val();
    var url = $("#url").val();

    var DataStr = {'_token': token, 'VendorCompanyID': VendorCompanyID, 'CampaignID': CampaignID};
    $.ajax({
        type: "POST",
        url: url + "/GetVendorUsingVendorCompany",
        data: DataStr,
        success: function (data)
        {
            $("#Vendor").html(data);
        }
    });
}
function GetBuyerUsingBuyerCompany()
{
    var BuyerCompanyID = $("#BuyerCompany").val();
    var CampaignID = $("#Campaign").val();
    var token = $("#_token").val();
    var url = $("#url").val();

    var DataStr = {'_token': token, 'BuyerCompanyID': BuyerCompanyID, 'CampaignID': CampaignID};
    $.ajax({
        type: "POST",
        url: url + "/GetBuyerUsingBuyerCompany",
        data: DataStr,
        success: function (data)
        {
            $("#Buyer").html(data);
        }
    });
}