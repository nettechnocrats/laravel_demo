$(document).ready(function() {    
     GetBuyerList(1);
     GetCompanyUsingShwowInactiveAndTest();
     GetCampaignDetails();
     GetCampaignDetailsAdd();
});
function Pagination(page)
{
    GetBuyerList(page);
}
function SearchVendorList()
{
    GetBuyerList(1);
}
function GetBuyerList(page)
{
    var numofrecords    = $('#numofrecords').val();
    var loading=$("#loading").val();
    if(numofrecords==10)
    { 
        $("#dataTable_wrapper").html('<center><img src="'+loading+'" style="width: 200px"><input type="hidden" name="numofrecords" id="numofrecords" value="10"></center>'); 
    }
    else
    {
        $("#dataTable_wrapper").html('<center><img src="'+loading+'" style="width: 200px"></center>'); 
    }
    var token           = $("#_token").val();
    var url             = $("#url").val(); 

    var ShowInactive = '0';
    var ShowTest = '0';
    if ($("#ShowInactive").prop('checked') == true) {
        ShowInactive = '1';
    }
    if ($("#ShowTest").prop('checked') == true) {
        ShowTest = '1';
    }
    
    var Company         = $("#Company").val();
    var Campaign        = $("#Campaign").val();
    var BuyerName       = $("#BuyerName").val();
    var Status          = $("#Status").val();
    var BuyerMode       = $("#BuyerMode").val();
    
    var DataStr         = {'_token':token,'page':page,'numofrecords':numofrecords,'Company':Company,'Campaign':Campaign,'BuyerName':BuyerName,'BuyerMode':BuyerMode,'Status':Status,'ShowInactive':ShowInactive,'ShowTest':ShowTest};
    $.ajax({
        type: "POST",
        url:  url+"/GetBuyerList",
        data: DataStr,
        success: function(data) 
        {
            $("#dataTable_wrapper").html(data);         
        }
    }); 
}


function HideErr(id)
{
    $("#"+id).hide();
}


function GetBuyerPartnetLabel(BuyerID)
{    
    var token           = $("#_token").val();
    var url             = $("#url").val();     
    var DataStr         = {'_token':token,'BuyerID':BuyerID};
    $.ajax({
        type: "POST",
        url:  url+"/GetBuyerPartnetLabel",
        data: DataStr,
        success: function(data) 
        {
            $("#PartnerLabel_"+BuyerID).html(data);         
        }
    }); 
}

function SaveBuyerPartnetLabel(Action,BuyerID)
{
    var token           = $("#_token").val();
    var url             = $("#url").val();     
    var PartnerLabel    = $("#PartnerLabelText_"+BuyerID).val();     
    var DataStr         = {'_token':token,'BuyerID':BuyerID,'Action':Action,'PartnerLabel':PartnerLabel};
    $.ajax({
        type: "POST",
        url:  url+"/SaveBuyerPartnetLabel",
        data: DataStr,
        success: function(data) 
        {
            $("#PartnerLabel_"+BuyerID).html(data);         
        }
    });
}
function ChangeBuyerStatus(BID, Status)
{    
    var token           = $("#_token").val();
    var url             = $("#url").val();     
    var DataStr         = {'_token':token,'BID':BID,'Status':Status};
    $.ajax({
        type: "POST",
        url:  url+"/ChangeBuyerStatus",
        data: DataStr,
        dataType: 'json',
        success: function(data) 
        {
            if(data.Status==1)
            {
                $("#BuyerMessage").html(data.Msg); 
                $("#StatusHtml_"+BID).html(data.StatusHtml);                             
            }
            else
            {                
                $("#BuyerMessage").html(data.Msg);
                $("#StatusHtml_"+BID).html(data.StatusHtml);                             
            }
            setInterval(function(){ $('#BuyerMessage').fadeOut(); }, 5000);
        }
    });
}
function ChangeBuyerTestMode(BID, Mode)
{    
    var token           = $("#_token").val();
    var url             = $("#url").val();     
    var DataStr         = {'_token':token,'BID':BID,'Mode':Mode};
    $.ajax({
        type: "POST",
        url:  url+"/ChangeBuyerTestMode",
        data: DataStr,
        dataType: 'json',
        success: function(data) 
        {
            if(data.Status==1)
            {
                $("#BuyerMessage").html(data.Msg);                         
                $("#ModeHtml_"+BID).html(data.ModeHtml);                         
            }
            else
            {                
                $("#BuyerMessage").html(data.Msg);                         
                $("#ModeHtml_"+BID).html(data.ModeHtml);                         
            }
            setInterval(function(){ $('#BuyerMessage').fadeOut(); }, 5000);
        }
    });
}
function GetCompanyUsingShwowInactiveAndTest()
{
    var token = $("#_token").val();
    var url = $("#url").val();
    var ShowInactive = '0';
    var ShowTest = '0';
    if ($("#ShowInactive").prop('checked') == true) {
        ShowInactive = '1';
    }
    if ($("#ShowTest").prop('checked') == true) {
        ShowTest = '1';
    }
    var DataStr = {'_token': token, 'ShowInactive': ShowInactive, 'ShowTest': ShowTest};
    $.ajax({
        type: "POST",
        url: url + "/GetCompanyUsingShwowInactiveAndTest2",
        data: DataStr,
        success: function (data)
        {
            $("#Company").html(data);        
        }
    });
}
$("#CopyAdminLable").click(function(){
    var AdminLabel= $("#AdminLabel").val();
    $("#PartnerLabel").val(AdminLabel);

    $("#AdminLabelErr").hide();
    $("#PartnerLabelErr").hide();
});

function CopyAdminLableAdd(){
    var AdminLabel= $("#AdminLabelAdd").val();
    $("#PartnerLabelAdd").val(AdminLabel);

    $("#AdminLabelAddErr").hide();
    $("#PartnerLabelAddErr").hide();
}
function CopyAdminLableEdit(){
    var AdminLabel= $("#AdminLabelEdit").val();
    $("#PartnerLabelEdit").val(AdminLabel);

    $("#AdminLabelEditErr").hide();
    $("#PartnerLabelEditErr").hide();
}
$("#PayoutCalculation").change(function(){
     var PayoutCalculation  = $("#PayoutCalculation").val();

     if(PayoutCalculation=="RevShare")
     {
        $("#RevsharePercentInput").show();
        $("#FixedPriceInput").hide();
     }
     else if(PayoutCalculation=="FixedPrice")
     {
        $("#RevsharePercentInput").hide();
        $("#FixedPriceInput").show();
     }
});
function PayoutCalculationAddFunction(){
     var PayoutCalculation  = $("#PayoutCalculationAdd").val();

     if(PayoutCalculation=="RevShare")
     {
        $("#RevshareInputAdd").show();
        $("#FixedPriceInputAdd").hide();
     }
     else if(PayoutCalculation=="FixedPrice")
     {
        $("#RevshareInputAdd").hide();
        $("#FixedPriceInputAdd").show();
     }
}
function PayoutCalculationEditFunction(){
     var PayoutCalculation  = $("#PayoutCalculationEdit").val();

     if(PayoutCalculation=="RevShare")
     {
        $("#RevshareInputEdit").show();
        $("#FixedPriceInputEdit").hide();
     }
     else if(PayoutCalculation=="FixedPrice")
     {
        $("#RevshareInputEdit").hide();
        $("#FixedPriceInputEdit").show();
     }
}
function SaveBuyerDetails()
{
    var flag            = 1;
    var FormType        = $("#FormType").val(); 
    var CampaignType    = $("#CampaignTypeEdit").val(); 
    var Company         = $("#CompanyEdit").val();  
    var AdminLabel      = $("#AdminLabelEdit").val();  
    var PartnerLabel    = $("#PartnerLabelEdit").val();  
    var IntegrationFileName    = $("#IntegrationFileNameEdit").val();  
    var PayoutCalculation      = $("#PayoutCalculationEdit").val();  
    var Revshare        = $("#RevshareEdit").val();  
    var FixedPrice      = $("#FixedPriceEdit").val();  
    var DailyCap        = $("#DailyCapEdit").val();  
    var TotalCap        = $("#TotalCapEdit").val();  
    var MaxTimeOut      = $("#MaxTimeOutEdit").val(); 
    var MinTimeOut      = $("#MinTimeOutEdit").val(); 
    var MaxPingTimeOut  = $("#MaxPingTimeOutEdit").val(); 
    var MaxPostTimeOut  = $("#MaxPostTimeOutEdit").val(); 
    var MaxPingsPerMinute  = $("#MaxPingsPerMinuteEdit").val(); 
           
    if(Company=="")
    {
        $("#CompanyEditErr").css('color','red');
        $("#CompanyEditErr").html("Please Select Company.")
        $("#CompanyEditErr").show();
        flag =0;
    }
    if(PartnerLabel=="")
    {
        $("#PartnerLabelEditErr").css('color','red');
        $("#PartnerLabelEditErr").html('Please Enter Partner Label.');
        $("#PartnerLabelEditErr").show();
        flag =0;
    }
    if(AdminLabel=="")
    {
        $("#AdminLabelEditErr").css('color','red');
        $("#AdminLabelEditErr").html('Please Enter Admin Label.');
        $("#AdminLabelEditErr").show();
        flag =0;
    }
    if(IntegrationFileName=="")
    {
        $("#IntegrationFileNameEditErr").css('color','red');
        $("#IntegrationFileNameEditErr").html('Please Enter Integration File Name.');
        $("#IntegrationFileNameEditErr").show();
        flag =0;
    }               
    if(PayoutCalculation=="")
    {
        $("#PayoutCalculationEditErr").css('color','red');
        $("#PayoutCalculationEditErr").html('Please Enter Payout Calculation Type.');
        $("#PayoutCalculationEditErr").show();
        flag =0;
    }
    if(PayoutCalculation=="Revshare")
    {
        if(Revshare=="")
        {
            $("#RevshareEditErr").css('color','red');
            $("#RevshareEditErr").html('Please Enter Revshare Percent.');
            $("#RevshareEditErr").show();
            flag =0;
        }
    }
    if(PayoutCalculation=="FixedPrice")
    {
        if(FixedPrice=="" )
        {
            $("#FixedPriceEditErr").css('color','red');
            $("#FixedPriceEditErr").html('Please Enter Fixed Price.');
            $("#FixedPriceEditErr").show();
            flag =0;
        }
    }
    if(DailyCap=="")
    {
        $("#DailyCapEditErr").css('color','red');
        $("#DailyCapEditErr").html('Required.');
        $("#DailyCapEditErr").show();
        flag =0;
    }
    else if(DailyCap!="")
    {
        if(isNaN(DailyCap))
        {
            $("#DailyCapEditErr").css('color','red');
            $("#DailyCapEditErr").html('Daily Cap Values Should Be Numeric.');
            $("#DailyCapEditErr").show();
            flag =0;
        }
    }
    if(TotalCap=="")
    {
        $("#TotalCapEditErr").css('color','red');
        $("#TotalCapEditErr").html('Required.');
        $("#TotalCapEditErr").show();
        flag =0;
    }
    else if(TotalCap!="")
    {
        if(isNaN(TotalCap))
        {
            $("#TotalCapEditErr").css('color','red');
            $("#TotalCapEditErr").html('Total Cap Values Should Be Numeric.');
            $("#TotalCapEditErr").show();
            flag =0;
        }
    }
    if(CampaignType=="DirectPost")
    {
        if(MaxTimeOut=="")
        {
            $("#MaxTimeOutEditErr").css('color','red');
            $("#MaxTimeOutEditErr").html('Please Enter Max Time Out Values.');
            $("#MaxTimeOutEditErr").show();
            flag =0;
        }
        else if(MaxTimeOut!="")
        {
            if(isNaN(MaxTimeOut))
            {
                $("#MaxTimeOutEditErr").css('color','red');
                $("#MaxTimeOutEditErr").html('Max Time Out Values Should Be Numeric.');
                $("#MaxTimeOutEditErr").show();
                flag =0;
            }
        }
        if(MinTimeOut=="")
        {
            $("#MinTimeOutEditErr").css('color','red');
            $("#MinTimeOutEditErr").html('Please Enter Min Time Out Values.');
            $("#MinTimeOutEditErr").show();
            flag =0;
        }
        else if(MinTimeOut!="")
        {
            if(isNaN(MinTimeOut))
            {
                $("#MinTimeOutEditErr").css('color','red');
                $("#MinTimeOutEditErr").html('Min Time Out Values Should Be Numeric.');
                $("#MinTimeOutEditErr").show();
                flag =0;
            }
        }
    }
    if(CampaignType=="PingPost")
    {
        if(MaxPingTimeOut=="")
        {
            $("#MaxPingTimeOutEditErr").css('color','red');
            $("#MaxPingTimeOutEditErr").html('Please Enter Max Time Out Values.');
            $("#MaxPingTimeOutEditErr").show();
            flag =0;
        }
        else if(MaxPingTimeOut!="")
        {
            if(isNaN(MaxPingTimeOut))
            {
                $("#MaxPingTimeOutEditErr").css('color','red');
                $("#MaxPingTimeOutEditErr").html('Max Time Out Values Should Be Numeric.');
                $("#MaxPingTimeOutEditErr").show();
                flag =0;
            }
        }
        if(MaxPostTimeOut=="")
        {
            $("#MaxPostTimeOutEditErr").css('color','red');
            $("#MaxPostTimeOutEditErr").html('Please Enter Max Time Out Values.');
            $("#MaxPostTimeOutEditErr").show();
            flag =0;
        }
        else if(MaxPostTimeOut!="")
        {
            if(isNaN(MaxPostTimeOut))
            {
                $("#MaxPostTimeOutEditErr").css('color','red');
                $("#MaxPostTimeOutEditErr").html('Max Time Out Values Should Be Numeric.');
                $("#MaxPostTimeOutEditErr").show();
                flag =0;
            }
        } 
        if(MaxPingsPerMinute=="")
        {
            $("#MaxPingsPerMinuteEditErr").css('color','red');
            $("#MaxPingsPerMinuteEditErr").html('Please Enter Max Pings Per Minute Values.');
            $("#MaxPingsPerMinuteEditErr").show();
            flag =0;
        }
        else if(MaxPingsPerMinute!="")
        {
            if(isNaN(MaxPingsPerMinute))
            {
                $("#MaxPingsPerMinuteEditErr").css('color','red');
                $("#MaxPingsPerMinuteEditErr").html('Max Pings Per Minute Values Should Be Numeric.');
                $("#MaxPingsPerMinuteEditErr").show();
                flag =0;
            }
        }              
    }
    if(flag ==1)
    {        
        if(FormType=='Update')
        {
            $("#BuyerUpdateForm").submit();
        }
        else if(FormType=='Clone')
        {
            if(AdminLabel!='')
            {
                var IsBuyerNameExist = IsBuyerNameExistFunctionForClone();
                if(IsBuyerNameExist==true)
                {
                    $("#AdminLabelEditErr").css('color','red');
                    $("#AdminLabelEditErr").html('Same Admin Label Already Exist.');
                    $("#AdminLabelEditErr").show();
                    flag=0;
                }
                else
                {
                    $("#BuyerUpdateForm").submit();
                }
            }
        }  
    }
      
}
function AddBuyerDetails()
{
    var flag            = 1;
    var CampaignType    = $("#CampaignTypeAdd").val(); 
    var Company         = $("#CompanyAdd").val();  
    var AdminLabel      = $("#AdminLabelAdd").val();  
    var PartnerLabel    = $("#PartnerLabelAdd").val();  
    var IntegrationFileName    = $("#IntegrationFileNameAdd").val();  
    var PayoutCalculation      = $("#PayoutCalculationAdd").val();  
    var Revshare        = $("#RevshareAdd").val();  
    var FixedPrice      = $("#FixedPriceAdd").val();  
    var DailyCap        = $("#DailyCapAdd").val();  
    var TotalCap        = $("#TotalCapAdd").val();  
    var MaxTimeOut      = $("#MaxTimeOutAdd").val(); 
    var MinTimeOut      = $("#MinTimeOutAdd").val(); 
    var MaxPingTimeOut  = $("#MaxPingTimeOutAdd").val(); 
    var MaxPostTimeOut  = $("#MaxPostTimeOutAdd").val(); 
    var MaxPingsPerMinute  = $("#MaxPingsPerMinuteAdd").val();  
    var token           = $("#_token").val();
    var url             = $("#url").val();      
   
    if(Company=="")
    {
        $("#CompanyAddErr").css('color','red');
        $("#CompanyAddErr").html("Please Select Company.")
        $("#CompanyAddErr").show();
        flag =0;
    }
    if(PartnerLabel=="")
    {
        $("#PartnerLabelAddErr").css('color','red');
        $("#PartnerLabelAddErr").html('Please Enter Partner Label.');
        $("#PartnerLabelAddErr").show();
        flag =0;
    }
    if(AdminLabel=="")
    {
        $("#AdminLabelAddErr").css('color','red');
        $("#AdminLabelAddErr").html('Please Enter Admin Label.');
        $("#AdminLabelAddErr").show();
        flag =0;
    }
    if(IntegrationFileName=="")
    {
        $("#IntegrationFileNameAddErr").css('color','red');
        $("#IntegrationFileNameAddErr").html('Please Enter Integration File Name.');
        $("#IntegrationFileNameAddErr").show();
        flag =0;
    }               
    if(PayoutCalculation=="")
    {
        $("#PayoutCalculationAddErr").css('color','red');
        $("#PayoutCalculationAddErr").html('Please Enter Payout Calculation Type.');
        $("#PayoutCalculationAddErr").show();
        flag =0;
    }
    if(PayoutCalculation=="Revshare")
    {
        if(Revshare=="")
        {
            $("#RevshareAddErr").css('color','red');
            $("#RevshareAddErr").html('Please Enter Revshare Percent.');
            $("#RevshareAddErr").show();
            flag =0;
        }
    }
    if(PayoutCalculation=="FixedPrice")
    {
        if(FixedPrice=="" )
        {
            $("#FixedPriceAddErr").css('color','red');
            $("#FixedPriceAddErr").html('Please Enter Fixed Price.');
            $("#FixedPriceAddErr").show();
            flag =0;
        }
    }
    if(DailyCap=="")
    {
        $("#DailyCapAddErr").css('color','red');
        $("#DailyCapAddErr").html('Required.');
        $("#DailyCapAddErr").show();
        flag =0;
    }
    else if(DailyCap!="")
    {
        if(isNaN(DailyCap))
        {
            $("#DailyCapAddErr").css('color','red');
            $("#DailyCapAddErr").html('Daily Cap Values Should Be Numeric.');
            $("#DailyCapAddErr").show();
            flag =0;
        }
    }
    if(TotalCap=="")
    {
        $("#TotalCapAddErr").css('color','red');
        $("#TotalCapAddErr").html('Required.');
        $("#TotalCapAddErr").show();
        flag =0;
    }
    else if(TotalCap!="")
    {
        if(isNaN(TotalCap))
        {
            $("#TotalCapAddErr").css('color','red');
            $("#TotalCapAddErr").html('Total Cap Values Should Be Numeric.');
            $("#TotalCapAddErr").show();
            flag =0;
        }
    }
    if(CampaignType=="DirectPost")
    {
        if(MaxTimeOut=="")
        {
            $("#MaxTimeOutAddErr").css('color','red');
            $("#MaxTimeOutAddErr").html('Please Enter Max Time Out Values.');
            $("#MaxTimeOutAddErr").show();
            flag =0;
        }
        else if(MaxTimeOut!="")
        {
            if(isNaN(MaxTimeOut))
            {
                $("#MaxTimeOutAddErr").css('color','red');
                $("#MaxTimeOutAddErr").html('Max Time Out Values Should Be Numeric.');
                $("#MaxTimeOutAddErr").show();
                flag =0;
            }
        }
        if(MinTimeOut=="")
        {
            $("#MinTimeOutAddErr").css('color','red');
            $("#MinTimeOutAddErr").html('Please Enter Min Time Out Values.');
            $("#MinTimeOutAddErr").show();
            flag =0;
        }
        else if(MinTimeOut!="")
        {
            if(isNaN(MinTimeOut))
            {
                $("#MinTimeOutAddErr").css('color','red');
                $("#MinTimeOutAddErr").html('Min Time Out Values Should Be Numeric.');
                $("#MinTimeOutAddErr").show();
                flag =0;
            }
        }
    }
    if(CampaignType=="PingPost")
    {
        if(MaxPingTimeOut=="")
        {
            $("#MaxPingTimeOutAddErr").css('color','red');
            $("#MaxPingTimeOutAddErr").html('Please Enter Max Time Out Values.');
            $("#MaxPingTimeOutAddErr").show();
            flag =0;
        }
        else if(MaxPingTimeOut!="")
        {
            if(isNaN(MaxPingTimeOut))
            {
                $("#MaxPingTimeOutAddErr").css('color','red');
                $("#MaxPingTimeOutAddErr").html('Max Time Out Values Should Be Numeric.');
                $("#MaxPingTimeOutAddErr").show();
                flag =0;
            }
        }
        if(MaxPostTimeOut=="")
        {
            $("#MaxPostTimeOutAddErr").css('color','red');
            $("#MaxPostTimeOutAddErr").html('Please Enter Max Time Out Values.');
            $("#MaxPostTimeOutAddErr").show();
            flag =0;
        }
        else if(MaxPostTimeOut!="")
        {
            if(isNaN(MaxPostTimeOut))
            {
                $("#MaxPostTimeOutAddErr").css('color','red');
                $("#MaxPostTimeOutAddErr").html('Max Time Out Values Should Be Numeric.');
                $("#MaxPostTimeOutAddErr").show();
                flag =0;
            }
        } 
        if(MaxPingsPerMinute=="")
        {
            $("#MaxPingsPerMinuteAddErr").css('color','red');
            $("#MaxPingsPerMinuteAddErr").html('Please Enter Max Pings Per Minute Values.');
            $("#MaxPingsPerMinuteAddErr").show();
            flag =0;
        }
        else if(MaxPingsPerMinute!="")
        {
            if(isNaN(MaxPingsPerMinute))
            {
                $("#MaxPingsPerMinuteAddErr").css('color','red');
                $("#MaxPingsPerMinuteAddErr").html('Max Pings Per Minute Values Should Be Numeric.');
                $("#MaxPingsPerMinuteAddErr").show();
                flag =0;
            }
        }              
    }
    if(flag ==1)
    {
        
        if(AdminLabel!='')
        {
            var IsBuyerNameExist = IsBuyerNameExistFunction();

            if(IsBuyerNameExist==true)
            {
                $("#AdminLabelAddErr").css('color','red');
                $("#AdminLabelAddErr").html('Same Admin Label Already Exist.');
                $("#AdminLabelAddErr").show();
                flag=0;
            }
            else
            {
                $("#BuyerAddForm").submit();
            }
        }
    }
      
}
$('.custom-file input').change(function (e) {
    var files = [];
    for (var i = 0; i < $(this)[0].files.length; i++) {
        files.push($(this)[0].files[i].name);
    }
    $(this).next('.custom-file-label').html(files.join(', '));
});

function DeleteBuyerUploadedFile(BuyerID, ID)
{
    var token           = $("#_token").val();
    var url             = $("#url").val();
    var DataStr         = {'_token':token,'BuyerID':BuyerID,'ID':ID};
    $.ajax({
        type: "POST",
        url:  url+"/DeleteBuyerUploadedFile",
        data: DataStr,
        dataType: "json",
        success: function(data)
        {
            if(data.Status==1)
            {
                $("#UploadedFileTR_"+ID).remove();
                $("#UploadFileMessage").html(data.Message);
            } 
            else
            {
                $("#UploadFileMessage").html(data.Message);
            }        
        }
    });
}

function SetSessionForCloneBuyer(BuyerID)
{
    var token           = $("#_token").val();
    var url             = $("#url").val();
   
    var DataStr         = {'_token':token,'BuyerID':BuyerID};
    $.ajax({
        type: "POST",
        url:  url+"/SetSessionForCloneBuyer",
        data: DataStr,
        success: function(data) 
        {
            if(data==BuyerID)
            {
                window.location = url+"/add-buyer";
            }   
            else
            {
                $("#ErrMsg").html('<div class="alert alert-danger alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Something Wrong! Please Try Again.</strong></div>');
                $("#ErrMsg").show();
            }     
        }
    });
}

function GetCampaignDetails()
{
    var CampaignID = $("#Campaign").val();
    var token = $("#_token").val();
    var url = $("#url").val();
    var DataStr = {_token : token,CampaignID:CampaignID};
    var result;
    $.ajax({
            type:"POST",
            url :url+"/GetCampaignDetails",
            data : DataStr,
            dataType : 'json',
            success:function(data)
            {
                if(data.CampaignType == 'DirectPost')
                { 
                    $("#CampaignType").val(data.CampaignType);
                    $("#BuyerTierIDInput").css('display','block');

                    $("#MaxTimeOutInput").css('display','block');
                    $("#MinTimeOutInput").css('display','block');

                    $("#MaxPingTimeOutInput").css('display','none');
                    $("#MaxPostTimeOutInput").css('display','none');
                    $("#MaxPingsPerMinuteInput").css('display','none');

                } 
                else if(data.CampaignType == 'PingPost')
                {
                    $("#CampaignType").val(data.CampaignType);
                    $("#BuyerTierIDInput").css('display','none');

                    $("#MaxTimeOutInput").css('display','none');
                    $("#MinTimeOutInput").css('display','none');

                    $("#MaxPingTimeOutInput").css('display','block');
                    $("#MaxPostTimeOutInput").css('display','block');
                    $("#MaxPingsPerMinuteInput").css('display','block');
                }

            }
    });
    return result;
}

//////////////////////////////
function GetCampaignDetailsAdd()
{
    var CampaignID = $("#CampaignAdd").val();
    var token = $("#_token").val();
    var url = $("#url").val();
    var DataStr = {_token : token,CampaignID:CampaignID};
    var result;
    $.ajax({
            type:"POST",
            url :url+"/GetCampaignDetails",
            data : DataStr,
            dataType : 'json',
            success:function(data)
            {
                if(data.CampaignType == 'DirectPost')
                { 
                    $("#CampaignTypeAdd").val(data.CampaignType);
                    $("#BuyerTierIDInputAdd").css('display','block');

                    $("#MaxTimeOutInputAdd").css('display','block');
                    $("#MinTimeOutInputAdd").css('display','block');

                    $("#MaxPingTimeOutInputAdd").css('display','none');
                    $("#MaxPostTimeOutInputAdd").css('display','none');
                    $("#MaxPingsPerMinuteInputAdd").css('display','none');

                    $("#PingTestURLInputAdd").css('display','none');
                    $("#PingLiveURLInputAdd").css('display','none');
                    $("#PostTestURLInputAdd").css('display','none');
                    $("#PostLiveURLInputAdd").css('display','none');
                    $("#DirectPostTestURLInputAdd").css('display','block');
                    $("#DirectPostLiveURLInputAdd").css('display','block');

                } 
                else if(data.CampaignType == 'PingPost')
                {
                    $("#CampaignTypeAdd").val(data.CampaignType);
                    $("#BuyerTierIDInputAdd").css('display','none');

                    $("#MaxTimeOutInputAdd").css('display','none');
                    $("#MinTimeOutInputAdd").css('display','none');

                    $("#MaxPingTimeOutInputAdd").css('display','block');
                    $("#MaxPostTimeOutInputAdd").css('display','block');
                    $("#MaxPingsPerMinuteInputAdd").css('display','block');

                    $("#PingTestURLInputAdd").css('display','block');
                    $("#PingLiveURLInputAdd").css('display','block');
                    $("#PostTestURLInputAdd").css('display','block');
                    $("#PostLiveURLInputAdd").css('display','block');
                    $("#DirectPostTestURLInputAdd").css('display','none');
                    $("#DirectPostLiveURLInputAdd").css('display','none');


                }

            }
    });
    return result;
}
function GetCampaignDetailsEdit()
{
    var CampaignID = $("#CampaignEdit").val();
    var token = $("#_token").val();
    var url = $("#url").val();
    var DataStr = {_token : token,CampaignID:CampaignID};
    var result;
    $.ajax({
            type:"POST",
            url :url+"/GetCampaignDetails",
            data : DataStr,
            dataType : 'json',
            success:function(data)
            {
                if(data.CampaignType == 'DirectPost')
                { 
                    $("#CampaignTypeEdit").val(data.CampaignType);
                    $("#BuyerTierIDInputEdit").css('display','block');

                    $("#MaxTimeOutInputEdit").css('display','block');
                    $("#MinTimeOutInputEdit").css('display','block');

                    $("#MaxPingTimeOutInputEdit").css('display','none');
                    $("#MaxPostTimeOutInputEdit").css('display','none');
                    $("#MaxPingsPerMinuteInputEdit").css('display','none');

                    $("#PingTestURLInputEdit").css('display','none');
                    $("#PingLiveURLInputEdit").css('display','none');
                    $("#PostTestURLInputEdit").css('display','none');
                    $("#PostLiveURLInputEdit").css('display','none');
                    $("#DirectPostTestURLInputEdit").css('display','block');
                    $("#DirectPostLiveURLInputEdit").css('display','block');

                } 
                else if(data.CampaignType == 'PingPost')
                {
                    $("#CampaignTypeEdit").val(data.CampaignType);
                    $("#BuyerTierIDInputEdit").css('display','none');

                    $("#MaxTimeOutInputEdit").css('display','none');
                    $("#MinTimeOutInputEdit").css('display','none');

                    $("#MaxPingTimeOutInputEdit").css('display','block');
                    $("#MaxPostTimeOutInputEdit").css('display','block');
                    $("#MaxPingsPerMinuteInputEdit").css('display','block');

                    $("#PingTestURLInputEdit").css('display','block');
                    $("#PingLiveURLInputEdit").css('display','block');
                    $("#PostTestURLInputEdit").css('display','block');
                    $("#PostLiveURLInputEdit").css('display','block');
                    $("#DirectPostTestURLInputEdit").css('display','none');
                    $("#DirectPostLiveURLInputEdit").css('display','none');


                }

            }
    });
    return result;
}
function IsBuyerNameExistFunction()
{
    var BuyerName       = $("#AdminLabelAdd").val(); 
    var token = $("#_token").val();
    var url = $("#url").val();
    var DataStr = {_token : token,'BuyerName':BuyerName};
    var result;
    $.ajax({
            type:"POST",
            url :url+"/IsBuyerNameExist",
            data : DataStr,
            async : false,
            success:function(data)
            {
               result = data;
            }
    });
    return result;
}
function GetCompanyUsingShwowInactiveAndTest2()
{
    var token = $("#_token").val();
    var url = $("#url").val();
    var ShowInactive = '0';
    var ShowTest = '0';
    if ($("#ShowInactiveAdd").prop('checked') == true) {
        ShowInactive = '1';
    }
    if ($("#ShowTestAdd").prop('checked') == true) {
        ShowTest = '1';
    }
    var DataStr = {'_token': token, 'ShowInactive': ShowInactive, 'ShowTest': ShowTest};
    $.ajax({
        type: "POST",
        url: url + "/GetCompanyUsingShwowInactiveAndTest2",
        data: DataStr,
        success: function (data)
        {
            $("#CompanyAdd").html(data);        
        }
    });
}
function GetCompanyUsingShwowInactiveAndTest3()
{
    var token = $("#_token").val();
    var url = $("#url").val();
    var ShowInactive = '0';
    var ShowTest = '0';
    if ($("#ShowInactiveEdit").prop('checked') == true) {
        ShowInactive = '1';
    }
    if ($("#ShowTestEdit").prop('checked') == true) {
        ShowTest = '1';
    }
    var DataStr = {'_token': token, 'ShowInactive': ShowInactive, 'ShowTest': ShowTest};
    $.ajax({
        type: "POST",
        url: url + "/GetCompanyUsingShwowInactiveAndTest2",
        data: DataStr,
        success: function (data)
        {
            $("#CompanyEdit").html(data);        
        }
    });
}
function AddBuyerModal()
{
    $("#CompanyAddErr").hide();  
    $("#AdminLabelAddErr").hide();  
    $("#PartnerLabelAddErr").hide();  
    $("#IntegrationFileNameAddErr").hide();  
    $("#PayoutCalculationAddErr").hide();  
    $("#RevshareAddErr").hide();  
    $("#FixedPriceAddErr").hide();  
    $("#DailyCapAddErr").hide();  
    $("#TotalCapAddErr").hide();  
    $("#MaxTimeOutAddErr").hide(); 
    $("#MinTimeOutAddErr").hide(); 
    $("#MaxPingTimeOutAddErr").hide(); 
    $("#MaxPostTimeOutAddErr").hide(); 
    $("#MaxPingsPerMinuteAddErr").hide(); 
    
    $("#AddBuyerModal").modal('show');
}
function GetBuyerDetails(BuyerID)
{
    var token = $("#_token").val();
    var url = $("#url").val();
    
    var DataStr = {'_token': token, 'BuyerID': BuyerID};
    $.ajax({
        type: "POST",
        url: url + "/GetBuyerDetails",
        data: DataStr,
        success: function (data)
        {
            $("#EditBuyerDiv").html(data);     
            $("#EditBuyerModal").modal('show');       
        }
    });
}

function IsBuyerNameExistFunctionForClone()
{
    var BuyerName       = $("#AdminLabelEdit").val(); 
    var token = $("#_token").val();
    var url = $("#url").val();
    var DataStr = {_token : token,'BuyerName':BuyerName};
    var result;
    $.ajax({
            type:"POST",
            url :url+"/IsBuyerNameExist",
            data : DataStr,
            async : false,
            success:function(data)
            {
               result = data;
            }
    });
    return result;
}
function GetBuyerCloneDetails(BuyerID)
{
    var token = $("#_token").val();
    var url = $("#url").val();
    
    var DataStr = {'_token': token, 'BuyerID': BuyerID};
    $.ajax({
        type: "POST",
        url: url + "/GetBuyerCloneDetails",
        data: DataStr,
        success: function (data)
        {
            $("#CloneBuyerDiv").html(data);     
            $("#CloneBuyerModal").modal('show');       
        }
    });
}
