function GetVendorBuyerUsingCampaignID()
{
    var CampaignID = $("#Campaign").val();
    ////////////////////
    var ShowInactiveVendorCompanyID = '0';
    if( $("#ShowInactiveVendorCompanyID").prop("checked") == true){
        ShowInactiveVendorCompanyID = '1';
    }
    var ShowTestVendorCompanyID = '0';
     if( $("#ShowTestVendorCompanyID").prop("checked") == true){
        ShowTestVendorCompanyID = '1';
    }
    /////////////////////
    var ShowInactiveBuyerCompanyID = '0';
    if( $("#ShowInactiveBuyerCompanyID").prop("checked") == true){
        ShowInactiveBuyerCompanyID = '1';
    }
    var ShowTestBuyerCompanyID = '0';
     if( $("#ShowTestBuyerCompanyID").prop("checked") == true){
        ShowTestBuyerCompanyID = '1';
    }
    /////////////////////
     var ShowInactiveVendor = '0';
    if( $("#ShowInactiveVendor").prop("checked") == true){
        ShowInactiveVendor = '1';
    }
    var ShowTestVendor = '0';
     if( $("#ShowTestVendor").prop("checked") == true){
        ShowTestVendor = '1';
    }
    /////////////////////
    var token = $("#_token").val();
    var url = $("#url").val();

    var DataStr = {'_token': token, 'CampaignID': CampaignID,'ShowInactiveVendorCompanyID': ShowInactiveVendorCompanyID,'ShowTestVendorCompanyID': ShowTestVendorCompanyID,'ShowInactiveBuyerCompanyID': ShowInactiveBuyerCompanyID,'ShowTestBuyerCompanyID': ShowTestBuyerCompanyID,'ShowInactiveVendor': ShowInactiveVendor,'ShowTestVendor': ShowTestVendor};
    $.ajax({
        type: "POST",
        url: url + "/GetVendorBuyerUsingCampaignIDAndShowInactiveAndTest",
        dataType: 'json',
        data: DataStr,
        success: function (data)
        {
            $("#VendorCompany").html(data.VendorCompany);
            $("#BuyerCompany").html(data.BuyerCompany);
            $("#Vendor").html(data.Vendor);
        }
    });
}
function GetVendorUsingVendorCompany()
{
    var VendorCompanyID = $("#VendorCompany").val();
    
    /////////////////////
     var ShowInactiveVendor = '0';
    if( $("#ShowInactiveVendor").prop("checked") == true){
        ShowInactiveVendor = '1';
    }
    var ShowTestVendor = '0';
     if( $("#ShowTestVendor").prop("checked") == true){
        ShowTestVendor = '1';
    }
    /////////////////////
    var CampaignID = $("#Campaign").val();
    var token = $("#_token").val();
    var url = $("#url").val();

    var DataStr = {'_token': token, 'VendorCompanyID': VendorCompanyID, 'CampaignID': CampaignID,'ShowInactiveVendor': ShowInactiveVendor,'ShowTestVendor': ShowTestVendor};
    $.ajax({
        type: "POST",
        url: url + "/GetVendorUsingVendorCompanyAndShowInactiveAndTest",
        data: DataStr,
        dataType: 'json',
        success: function (data)
        {
            $("#Vendor").html(data.Vendor);
        }
    });
}

function SearchMappingList()
{
    GetAllMapping();
}

function GetAllMapping()
{
    var token           = $("#_token").val();
    var url             = $("#url").val(); 
}