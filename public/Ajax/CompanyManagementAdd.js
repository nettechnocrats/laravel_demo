//First Tab
$("#CompanyToUserBtn").on("click", function() 
{
  CompanyDetails();
});
//Second Tab
$("#UserToCompanyBtn").on("click", function() 
{
  $("#collapseOne").addClass('show');
  $("#collapseTwo").removeClass('show'); 
  $("#collapseThree").removeClass('show');
  $("#collapseFour").removeClass('show'); 

  $("#user").removeClass('tabStyle');
  $("#user").addClass('tabStyleHiden');

  $("#comapny").removeClass('tabStyleHiden');
  $("#comapny").addClass('tabStyle');

  $("#UserFirstNameErr").hide();
  $("#UserEmailErr").hide();
  $("#PasswordErr").hide();
  $("#ConfirmPasswordErr").hide();
});
$("#UserToVendorBtn").on("click", function() 
{
    var response = UserDetails(); 
    if(response==1)
    {
        $("#collapseOne").removeClass('show');
        $("#collapseTwo").removeClass('show');
        $("#collapseThree").addClass('show');  
        $("#collapseFour").removeClass('show'); 

        $("#user").removeClass('tabStyle');
        $("#user").addClass('tabStyleHiden');

        $("#vendor").removeClass('tabStyleHiden');
        $("#vendor").addClass('tabStyle');
    } 
});
$("#UserToBuyerBtn").on("click", function() 
{
    var response = UserDetails(); 
    if(response==1)
    {
        $("#collapseOne").removeClass('show');
        $("#collapseTwo").removeClass('show');
        $("#collapseThree").removeClass('show');  
        $("#collapseFour").addClass('show'); 

        $("#user").removeClass('tabStyle');
        $("#user").addClass('tabStyleHiden');

        $("#buyer").removeClass('tabStyleHiden');
        $("#buyer").addClass('tabStyle');
     } 
});
//Third Tab
$("#VendorToBuyerBtn").on("click", function() 
{
    var response = VendorDetails(); 
    if(response==1)
    {
      $("#collapseOne").removeClass('show');
      $("#collapseTwo").removeClass('show');
      $("#collapseThree").removeClass('show');
      $("#collapseFour").addClass('show');  

      $("#vendor").removeClass('tabStyle');
      $("#vendor").addClass('tabStyleHiden');

      $("#buyer").removeClass('tabStyleHiden');
      $("#buyer").addClass('tabStyle');
    }
  
});
$("#VendorToUserBtn").on("click", function() 
{
  $("#collapseOne").removeClass('show');
  $("#collapseTwo").addClass('show');  
  $("#collapseThree").removeClass('show');
  $("#collapseFour").removeClass('show');

  $("#vendor").removeClass('tabStyle');
  $("#vendor").addClass('tabStyleHiden');

  $("#user").removeClass('tabStyleHiden');
  $("#user").addClass('tabStyle');
});
//Third Tab
$("#BuyerToVendorBtn").on("click", function() 
{
  $("#collapseOne").removeClass('show');
  $("#collapseTwo").removeClass('show');
  $("#collapseThree").addClass('show');  
  $("#collapseFour").removeClass('show');

  $("#buyer").removeClass('tabStyle');
  $("#buyer").addClass('tabStyleHiden');

  $("#vendor").removeClass('tabStyleHiden');
  $("#vendor").addClass('tabStyle');
});
$('#BuyerToUserBtn').on("click", function() 
{
  $("#collapseOne").removeClass('show');
  $("#collapseTwo").addClass('show');
  $("#collapseThree").removeClass('show');  
  $("#collapseFour").removeClass('show');

  $("#buyer").removeClass('tabStyle');
  $("#buyer").addClass('tabStyleHiden');

  $("#user").removeClass('tabStyleHiden');
  $("#user").addClass('tabStyle');
});
$("#AddCompanyBtnOnUser").on("click",function()
{
    var response = UserDetails(); 
    if(response==1)
    {
        $("#CompanyAddForm").submit();
    } 
});
$("#AddCompanyBtnOnVendor").on("click",function()
{
    var response = VendorDetails(); 
    if(response==1)
    {
        $("#CompanyAddForm").submit();
    }
});
$("#AddCompanyBtnOnBuyer").on("click",function()
{
    var response = BuyerDetails(); 
    if(response==1)
    {
        $("#CompanyAddForm").submit();
    }
});
function OpenVendor()
{
    if ($("#AddVendor").prop('checked') == true) 
    {
        $("#vendor").show();
    }
    else if ($("#AddVendor").prop('checked') == false) 
    {
        $("#vendor").hide();       
    }
    ShowHideNextSaveButton();
}
function OpenBuyer()
{
    if ($("#AddBuyer").prop('checked') == true) 
    {
        $("#buyer").show();
    }
    else if ($("#AddBuyer").prop('checked') == false) 
    {
        $("#buyer").hide();         
    }
    ShowHideNextSaveButton();
}

function ShowHideNextSaveButton()
{
    var VendorChecked = 0;
    if ($("#AddVendor").prop('checked') == true) 
    {
        VendorChecked = 1;
    }   
    var BuyerChecked = 0;
    if ($("#AddBuyer").prop('checked') == true) 
    {
       BuyerChecked = 1;
    }

    if(VendorChecked==1 && BuyerChecked==1)
    {
        $("#NextBtnOnUserToVendor").show();
        $("#NextBtnOnUserToBuyer").hide();
        $("#SaveBtnOnUser").hide();

        $("#BuyerToVendor").show();
        $("#BuyerToUser").hide();
        $("#SaveBtnOnVendor").hide();
        $("#NextBtnOnVendor").show();
    }
    if(VendorChecked==1 && BuyerChecked==0)
    {
        $("#NextBtnOnUserToVendor").show();
        $("#NextBtnOnUserToBuyer").hide();
        $("#SaveBtnOnUser").hide();

        $("#BuyerToVendor").hide();
        $("#BuyerToUser").hide();

        $("#SaveBtnOnVendor").show();
        $("#NextBtnOnVendor").hide();
    }
    if(VendorChecked==0 && BuyerChecked==1)
    {
        $("#NextBtnOnUserToVendor").hide();
        $("#NextBtnOnUserToBuyer").show();      
        $("#SaveBtnOnUser").hide();

        $("#BuyerToVendor").hide();
        $("#BuyerToUser").show();

        $("#SaveBtnOnBuyer").show();
        $("#NextBtnOnBuyer").hide();
    }
    if(VendorChecked==0 && BuyerChecked==0)
    {
        $("#NextBtnOnUserToVendor").hide();
        $("#NextBtnOnUserToBuyer").hide();      
        $("#SaveBtnOnUser").show();
    }
}
function IsCompanyNameExistFunction()
{
    var CompanyName = $("#CompanyName").val(); 
    var token       = $("#_token").val();
    var url         = $("#url").val();
    var DataStr = {_token : token,'CompanyName':CompanyName};
    var result;
    $.ajax({
            type:"POST",
            url :url+"/IsCompanyNameExist",
            data : DataStr,
            async : false,
            success:function(data)
            {
               result = data;
            }
    });
    return result;
}
function CompanyDetails()
{
    var flag                    = 1;
    var CompanyName             = $("#CompanyName").val();    
    var CompanyCountry          = $("#CompanyCountry").val();
    var CompanyZipPostCode      = $("#CompanyZipPostCode").val();
    
    if(CompanyName=="")
    {
        $("#CompanyNameErr").css("color",'red');
        $("#CompanyNameErr").html("Please Enter Company Name.");
        $("#CompanyNameErr").show();
        flag=0;
    }
    if(CompanyCountry=="")
    {
        $("#CompanyCountryErr").css("color",'red');
        $("#CompanyCountryErr").html("Please Enter Country.");
        $("#CompanyCountryErr").show();
        flag=0;
    }
    if(CompanyZipPostCode!="")
    {
        if(CompanyZipPostCode.length<4 || CompanyZipPostCode.length>10)
        {
            $("#CompanyZipPostCodeErr").css("color",'red');
            $("#CompanyZipPostCodeErr").html("Please Enter 5-10 Digit Valid Zip Code.");
            $("#CompanyZipPostCodeErr").show();
            flag=0;
        }
    }
    if(flag==1)
    {       
        if(CompanyName!="")
        {
            var IsCompanyNameExist = IsCompanyNameExistFunction();
            if(IsCompanyNameExist==true)
            {
                $("#CompanyNameErr").css('color','red');
                $("#CompanyNameErr").html('Same Company Name Already Exist.');
                $("#CompanyNameErr").show();
                flags=0;
            }
            else
            {
                $("#collapseOne").removeClass('show');
                $("#collapseTwo").addClass('show');  
                $("#collapseThree").removeClass('show');
                $("#collapseFour").removeClass('show');

                $("#comapny").removeClass('tabStyle');
                $("#comapny").addClass('tabStyleHiden');

                $("#user").removeClass('tabStyleHiden');
                $("#user").addClass('tabStyle');
            }
        } 
     
    }
}
function IsUserEmailExistFunction()
{
    var UserEmail       = $("#UserEmail").val(); 
    var token = $("#_token").val();
    var url = $("#url").val();
    var DataStr = {_token : token,UserEmail:UserEmail};
    var result;
    $.ajax({
            type:"POST",
            url :url+"/IsUserEmailExist",
            data : DataStr,
            async : false,
            success:function(data)
            {
               result = data;
            }
    });
    return result;
}
function UserDetails()
{
    var flags           = 1;
    var UserFirstName   = $("#UserFirstName").val();  
    var UserLastName    = $("#UserLastName").val();  
    var UserEmail       = $("#UserEmail").val();  
    var UserTel         = $("#UserTel").val();  
    var UserSkype       = $("#UserSkype").val();
    var UserRole        = $("#UserRole").val();
    var Password        = $("#Password").val();  
    var ConfirmPassword = $("#ConfirmPassword").val(); 
    var token           = $("#_token").val();
    var url             = $("#url").val();    

    if(UserFirstName=="")
    {
        $("#UserFirstNameErr").css('color','red');
        $("#UserFirstNameErr").html('Please Enter First Name.');
        $("#UserFirstNameErr").show();
        flags =0;
    }
    if(UserRole=="")
    {
        $("#UserRoleErr").css('color','red');
        $("#UserRoleErr").html('Please Select User Role.');
        $("#UserRoleErr").show();
        flags =0;
    }
    if(UserEmail=="")
    {
        $("#UserEmailErr").css('color','red');
        $("#UserEmailErr").html('Please Enter E-mail.');
        $("#UserEmailErr").show();
        flags =0;
    }
    else if(UserEmail!="")
    {
        if(!ValidateEmail(UserEmail))
        {
            $("#UserEmailErr").css('color','red');
            $("#UserEmailErr").html('Please Enter Valid E-mail');
            $("#UserEmailErr").show();
            flags=0;
        }
        
    }
    if(UserTel!="")
    {
        if(!ValidatePhone(UserTel))
        {
            $("#UserTelErr").css('color','red');
            $("#UserTelErr").html('Please Enter Valid Telephone');
            $("#UserTelErr").show();
            flags=0;
        }
    }    
    if (Password == "")
    {
        $("#PasswordErr").css("color", 'red');
        $("#PasswordErr").html("Please Enter Password.");
        $("#PasswordErr").show();
        flags = 0;
    }
    else
    {
        if (Password.length < 8)
        {
            $("#PasswordErr").css("color", 'red');
            $("#PasswordErr").html("Password Length Should More Then or Equeal To 8 Digit.");
            $("#PasswordErr").show();
            flags = 0;
        }
        else
        {
            if (!ValidatePwd(Password))
            {
                $("#PasswordErr").css("color", 'red');
                $("#PasswordErr").html("Password Should Contain at least 1 Alphabet, 1 Number and 1 Special Character.");
                $("#PasswordErr").show();
                flags = 0;
            }
        }
    }
    if (ConfirmPassword == "")
    {
        $("#ConfirmPasswordErr").css("color", 'red');
        $("#ConfirmPasswordErr").html("Please Enter Confirm Password.");
        $("#ConfirmPasswordErr").show();
        flags = 0;
    }
    if (Password != ConfirmPassword)
    {
        $("#ConfirmPasswordErr").css("color", 'red');
        $("#ConfirmPasswordErr").html("Confirm Password Not Match.");
        $("#ConfirmPasswordErr").show();
        flags = 0;
    }
    if(flags ==1)
    {   
        if(UserEmail!='')
        {
            var IsUserEmailExist = IsUserEmailExistFunction();
            if(IsUserEmailExist==true)
            {
                $("#UserEmailErr").css('color','red');
                $("#UserEmailErr").html('Same E-mail Already Exist.');
                $("#UserEmailErr").show();
                flags=0;
            }
            else
            {
                return flags;
            }
        }
        
    }
}

function MoveNext()
{
    var VendorChecked = 0;
    if ($("#AddVendor").prop('checked') == true) 
    {
        VendorChecked = 1;
    }   
    var BuyerChecked = 0;
    if ($("#AddBuyer").prop('checked') == true) 
    {
       BuyerChecked = 1;
    }
    if(VendorChecked==1 && BuyerChecked==1)
    {
        $("#collapseOne").removeClass('show');
        $("#collapseTwo").removeClass('show');
        $("#collapseThree").addClass('show');  
        $("#collapseFour").removeClass('show'); 

        $("#user").removeClass('tabStyle');
        $("#user").addClass('tabStyleHiden');

        $("#vendor").removeClass('tabStyleHiden');
        $("#vendor").addClass('tabStyle');
    }
    else if(VendorChecked==1 && BuyerChecked==0)
    {
        $("#collapseOne").removeClass('show');
        $("#collapseTwo").removeClass('show');
        $("#collapseThree").addClass('show');  
        $("#collapseFour").removeClass('show'); 

        $("#user").removeClass('tabStyle');
        $("#user").addClass('tabStyleHiden');

        $("#vendor").removeClass('tabStyleHiden');
        $("#vendor").addClass('tabStyle');
    }
    else if(VendorChecked==0 && BuyerChecked==1)
    {
        $("#collapseOne").removeClass('show');
        $("#collapseTwo").removeClass('show');
        $("#collapseThree").removeClass('show');  
        $("#collapseFour").addClass('show'); 

        $("#user").removeClass('tabStyle');
        $("#user").addClass('tabStyleHiden');

        $("#buyer").removeClass('tabStyleHiden');
        $("#buyer").addClass('tabStyle');
    }
    else
    {
       alert("Ready To Save Company Details.");
    }
}
function ValidateEmail(email)
{
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function ValidatePwd(password)
{
    return !!password.match(/^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%* #+=\(\)\^?&])[A-Za-z\d$@$!%* #+=\(\)\^?&]{8,}$/);
}

function ValidatePhone(tel) {
    var reg = /^[0-9-\s\+?\(\)]{10,20}$/;
    return reg.test(tel);
}
function GeneratePassword()
{  
    var token           = $("#_token").val();
    var url             = $("#url").val(); 
  
    var DataStr         = {'_token':token};
    $.ajax({
        type: "POST",
        url:  url+"/GeneratePassword",
        data: DataStr,
        success: function(pass) 
        {
            $("#Password").val(pass);         
            $("#ConfirmPassword").val(pass);

            $("#PasswordErr").hide();         
            $("#ConfirmPasswordErr").hide();         
        }
    }); 
}
function Pagination(page)
{
    GetCompanyList(page);
}
function SearchCompanyList()
{
    GetCompanyList(1);
}
function GetCompanyList(page)
{
    var numofrecords    = $('#numofrecords').val();
    var loading=$("#loading").val();
    if(numofrecords==10)
    {
        $("#dataTable_wrapper").html('<center><img src="'+loading+'" style="width: 200px"><input type="hidden" name="numofrecords" id="numofrecords" value="10"></center>'); 
    }
    else
    {
        $("#dataTable_wrapper").html('<center><img src="'+loading+'" style="width: 200px"></center>'); 
    }
    var token           = $("#_token").val();
    var url             = $("#url").val(); 

    var StartDate       = $("#StartDate").val();
    var EndDate         = $("#EndDate").val();
    var CompanyName     = $("#CompanyName").val();
    var CompanyTel      = $("#CompanyTel").val();
    var CompanyStatus   = $("#CompanyStatus").val();
    var CompanyTestMode = $("#CompanyTestMode").val();
    var MappedUser      = $("#MappedUser").val();
    var MappedBuyer     = $("#MappedBuyer").val();
    
    var DataStr         = {'_token':token,'page':page,'numofrecords':numofrecords,'CompanyName':CompanyName,'CompanyTel':CompanyTel,'CompanyStatus':CompanyStatus,'CompanyTestMode':CompanyTestMode,'MappedUser':MappedUser,'MappedBuyer':MappedBuyer,'StartDate':StartDate,'EndDate':EndDate};
    $.ajax({
        type: "POST",
        url:  url+"/GetCompanyList",
        data: DataStr,
        success: function(data) 
        {
            $("#dataTable_wrapper").html(data);         
        }
    }); 
}
function HideErr(id)
{
    $("#"+id).hide();
} 
function ChangeCompanyTestMode(CID,CompanyTestMode)
{   
    var token           = $("#_token").val();
    var url             = $("#url").val();     
    var DataStr         = {'_token':token,'CID':CID,'CompanyTestMode':CompanyTestMode};
    $.ajax({
        type: "POST",
        url:  url+"/ChangeCompanyTestMode",
        data: DataStr,
        dataType: 'json',
        success: function(data) 
        {
            if(data.Status==1)
            {
                $("#ResponseMessage").html(data.Msg);
                $("#ModeHtml_"+CID).html(data.ModeHtml);
            }  
            else
            {
                $("#ResponseMessage").html(data.Msg);
            }
        }
    }); 
} 
$("#CompanyZipPostCode").keyup(function(){
    var CompanyCountry      = $("#CompanyCountry").val();
    var CompanyZipPostCode  = $("#CompanyZipPostCode").val();
    var token           = $("#_token").val();
    var url             = $("#url").val();
    if(CompanyZipPostCode.length>=5 && CompanyCountry=="US")
    {
        var DataStr         = {'_token':token,'CompanyZipPostCode':CompanyZipPostCode};
        $.ajax({
            type: "POST",
            url:  url+"/GetCityZipCodeState",
            data: DataStr,
            dataType: "json",
            success: function(data)
            {
                if(data.Status==1)
                {
                    $("#CompanyStateCounty").val(data.state);
                    $("#CompanyCity").val(data.city);
                }
                else
                {
                    $("#CompanyStateCounty").val('');
                    $("#CompanyCity").val('');
                }
                
            }
        });
    }
});
$("#CompanyCountry").change(function(){
    var CompanyCountry        =$("#CompanyCountry").val();
    if(CompanyCountry!="US")
    {
        $("#StateDiv").hide();
        $("#CompanyZipPostCodeLable").text("Postal Code");
        $('#CompanyZipPostCode').attr("placeholder", "Postal Code");
    }
    else
    {
        $("#StateDiv").show();
        $("#CompanyZipPostCodeLable").text("Zip Code");
        $('#CompanyZipPostCode').attr("placeholder", "Zip Code");
    }
});
$("#CompanyEditBtn").click(function(){
    var flag                    = 1;
    var CompanyName             = $("#CompanyName").val();    
    var CompanyCountry          = $("#CompanyCountry").val();
    var CompanyZipPostCode      = $("#CompanyZipPostCode").val();

    if(CompanyName=="")
    {
        $("#CompanyNameErr").css("color",'red');
        $("#CompanyNameErr").html("Please Enter Company Name.");
        $("#CompanyNameErr").show();
        flag=0;
    }
    if(CompanyCountry=="")
    {
        $("#CompanyCountryErr").css("color",'red');
        $("#CompanyCountryErr").html("Please Enter Country.");
        $("#CompanyCountryErr").show();
        flag=0;
    }
    if(CompanyZipPostCode!="")
    {
        if(CompanyZipPostCode.length<4 || CompanyZipPostCode.length>10)
        {
            $("#CompanyZipPostCodeErr").css("color",'red');
            $("#CompanyZipPostCodeErr").html("Please Enter 5 Digit Valid Zip Code.");
            $("#CompanyZipPostCodeErr").show();
            flag=0;
        }
    }
    if(flag==1)
    {
        $("#CompanyEditForm").submit();
    }
});
$('.custom-file input').change(function (e) {
    var files = [];
    for (var i = 0; i < $(this)[0].files.length; i++) {
        files.push($(this)[0].files[i].name);
    }
    $(this).next('.custom-file-label').html(files.join(', '));
});
function DeleteCompanyUploadedFile(CompanyID, ID)
{
    var token           = $("#_token").val();
    var url             = $("#url").val();
    var DataStr         = {'_token':token,'CompanyID':CompanyID,'ID':ID};
    $.ajax({
        type: "POST",
        url:  url+"/DeleteCompanyUploadedFile",
        data: DataStr,
        dataType: "json",
        success: function(data)
        {
            if(data.Status==1)
            {
                $("#UploadedFileTR_"+ID).remove();
                $("#UploadFileMessage").html(data.Message);
            } 
            else
            {
                $("#UploadFileMessage").html(data.Message);
            }        
        }
    });
}
function ChangeCompanyStatus(CID,CompanyStatus)
{
    var token           = $("#_token").val();
    var url             = $("#url").val(); 
    
    var DataStr         = {'_token':token,'CID':CID,'CompanyStatus':CompanyStatus};
    $.ajax({
        type: "POST",
        url:  url+"/ChangeCompanyStatus",
        data: DataStr,
        dataType: 'json',
        success: function(data) 
        {
            if(data.Status==1)
            {
                $("#ResponseMessage").html(data.Msg);
                $("#StatusHtml_"+CID).html(data.StatusHtml);
            }  
            else
            {
                $("#ResponseMessage").html(data.Msg);
            }
        }
    }); 
}
///////////////Vendor///////////////////////
function IsVendorNameExistFunction()
{
    var VendorName       = $("#AdminLabelVendor").val(); 
    var token = $("#_token").val();
    var url = $("#url").val();
    var DataStr = {_token : token,'VendorName':VendorName};
    var result;
    $.ajax({
            type:"POST",
            url :url+"/IsVendorNameExist",
            data : DataStr,
            async : false,
            success:function(data)
            {
               result = data;
            }
    });
    return result;
}
function VendorDetails()
{
    var flag            = 1;
    var CampaignType    = $("#CampaignTypeVendor").val();  
    
    var Campaign        = $("#CampaignVendor").val(); 
    var AdminLabel      = $("#AdminLabelVendor").val();  
    var PartnerLabel    = $("#PartnerLabelVendor").val();  
    var PayoutCalculation          = $("#PayoutCalculationVendor").val();  
    var RevsharePercent = $("#RevshareVendor").val();  
    var FixedPrice      = $("#FixedPriceVendor").val();  
    var DailyCap        = $("#DailyCapVendor").val();  
    var TotalCap        = $("#TotalCapVendor").val();  
    var MaxTimeOut      = $("#MaxTimeOutVendor").val(); 
    var MaxPingTimeOut  = $("#MaxPingTimeOutVendor").val(); 
    var MaxPostTimeOut  = $("#MaxPostTimeOutVendor").val(); 
    var VendorStatus          = $("#VendorStatus").val();  
    var VendorTestMode        = $("#VendorTestMode").val();  
    var token           = $("#_token").val();
    var url             = $("#url").val();      
        if(CampaignVendor=="")
        {
            $("#CampaignVendorErr").css('color','red');
            $("#CampaignVendorErr").html('Please Select Campaign.');
            $("#CampaignErr").show();
            flag =0;
        }
        if(PartnerLabel=="")
        {
            $("#PartnerLabelVendorErr").css('color','red');
            $("#PartnerLabelVendorErr").html('Please Enter Partner Label.');
            $("#PartnerLabelVendorErr").show();
            flag =0;
        }
        if(AdminLabel=="")
        {
            $("#AdminLabelVendorErr").css('color','red');
            $("#AdminLabelVendorErr").html('Please Enter Admin Label.');
            $("#AdminLabelVendorErr").show();
            flag =0;
        } 
        if(PayoutCalculation=="RevShare")
        {
            if(RevsharePercent=="")
            {
                $("#RevshareVendorErr").css('color','red');
                $("#RevshareVendorErr").html('Please Enter Revshare Percent.');
                $("#RevshareVendorErr").show();
                flag =0;
            }
        }
        if(PayoutCalculation=="FixedPrice")
        {
            if(FixedPrice=="" )
            {
                $("#FixedPriceVendorErr").css('color','red');
                $("#FixedPriceVendorErr").html('Please Enter Fixed Price.');
                $("#FixedPriceVendorErr").show();
                flag =0;
            }
        }
        if(DailyCap=="")
        {
            $("#DailyCapVendorErr").css('color','red');
            $("#DailyCapVendorErr").html('Required.');
            $("#DailyCapVendorErr").show();
            flag =0;
        }
        if(TotalCap=="")
        {
            $("#TotalCapVendorErr").css('color','red');
            $("#TotalCapVendorErr").html('Required.');
            $("#TotalCapVendorErr").show();
            flag =0;
        }
        if(CampaignType=="DirectPost")
        {
            if(MaxTimeOut=="")
            {
                $("#MaxTimeOutVendorErr").css('color','red');
                $("#MaxTimeOutVendorErr").html('Please Enter Max Time Out Values.');
                $("#MaxTimeOutVendorErr").show();
                flag =0;
            }
            else if(MaxTimeOut!="")
            {
                if(isNaN(MaxTimeOut))
                {
                    $("#MaxTimeOutVendorErr").css('color','red');
                    $("#MaxTimeOutVendorErr").html('Max Time Out Values Should Be Numeric.');
                    $("#MaxTimeOutVendorErr").show();
                    flag =0;
                }
            }
        }
        if(CampaignType=="PingPost")
        {
            if(MaxPingTimeOut=="")
            {
                $("#MaxPingTimeOutVendorErr").css('color','red');
                $("#MaxPingTimeOutVendorErr").html('Please Enter Max Time Out Values.');
                $("#MaxPingTimeOutVendorErr").show();
                flag =0;
            }
            else if(MaxPingTimeOut!="")
            {
                if(isNaN(MaxPingTimeOut))
                {
                    $("#MaxPingTimeOutVendorErr").css('color','red');
                    $("#MaxPingTimeOutVendorErr").html('Max Time Out Values Should Be Numeric.');
                    $("#MaxPingTimeOutVendorErr").show();
                    flag =0;
                }
            }
            if(MaxPostTimeOut=="")
            {
                $("#MaxPostTimeOutVendorErr").css('color','red');
                $("#MaxPostTimeOutVendorErr").html('Please Enter Max Time Out Values.');
                $("#MaxPostTimeOutVendorErr").show();
                flag =0;
            }
            else if(MaxPostTimeOut!="")
            {
                if(isNaN(MaxPostTimeOut))
                {
                    $("#MaxPostTimeOutVendorErr").css('color','red');
                    $("#MaxPostTimeOutVendorErr").html('Max Time Out Values Should Be Numeric.');
                    $("#MaxPostTimeOutVendorErr").show();
                    flag =0;
                }
            }              
        }
        if(flag ==1)
        {
            if(AdminLabel!='')
            {
                var IsVendorNameExist = IsVendorNameExistFunction();
                if(IsVendorNameExist==true)
                {
                    $("#AdminLabelVendorErr").css('color','red');
                    $("#AdminLabelVendorErr").html('Same Admin Label Already Exist.');
                    $("#AdminLabelVendorErr").show();
                    flag=0;
                }
                else
                {
                    return flag;
                }
            }
        }      
}
$("#CopyAdminLableVendor").click(function(){
    var AdminLabel= $("#AdminLabelVendor").val();
    $("#PartnerLabelVendor").val(AdminLabel);

    $("#AdminLabelVendorErr").hide();
    $("#PartnerLabelVendorErr").hide();
});
$("#CopyAdminLableBuyer").click(function(){
    var AdminLabel= $("#AdminLabelBuyer").val();
    $("#PartnerLabelBuyer").val(AdminLabel);

    $("#AdminLabelBuyerErr").hide();
    $("#PartnerLabelBuyerErr").hide();
});
$("#PayoutCalculationVendor").change(function(){
     var PayoutCalculation  = $("#PayoutCalculationVendor").val();

     if(PayoutCalculation=="RevShare")
     {
        $("#RevsharePercentInputVendor").show();
        $("#FixedPriceInputVendor").hide();
        $("#TierPriceInputVendor").hide();
     }
     else if(PayoutCalculation=="MinPrice")
     {
        $("#RevsharePercentInputVendor").show();
        $("#FixedPriceInputVendor").hide();
        $("#TierPriceInputVendor").hide();
     }
     else if(PayoutCalculation=="TierPrice")
     {
       $("#RevsharePercentInputVendor").hide();
        $("#FixedPriceInputVendor").hide();
        $("#TierPriceInputVendor").show();
     }
     else if(PayoutCalculation=="FixedPrice")
     {
        $("#RevsharePercentInputVendor").hide();
        $("#FixedPriceInputVendor").show();
        $("#TierPriceInputVendor").hide();
     }
     else if(PayoutCalculation=="BucketPrice" || PayoutCalculation=="BucketSystemBySubID")
     {
        $("#RevsharePercentInputVendor").show();
        $("#FixedPriceInputVendor").show();
        $("#TierPriceInputVendor").hide();
     }
});

 function updateTextBox(token_value)
{        
    var $txt = jQuery("#TrackingPixelCode");
    var caretPos = $txt[0].selectionStart;
    var textAreaTxt = $txt.val();
    var txtToAdd = "[--" + token_value.value + "--]";
    $txt.val(textAreaTxt.substring(0, caretPos) + txtToAdd + textAreaTxt.substring(caretPos) );
    //$('#tracking_pixel_code').append(token_value.value +"="+ "[--" + token_value.value + "--]");
}

function GetCampaignDetailsVendor()
{
    var CampaignID = $("#CampaignVendor").val();
    var token = $("#_token").val();
    var url = $("#url").val();
    var DataStr = {_token : token,CampaignID:CampaignID};
    var result;
    $.ajax({
            type:"POST",
            url :url+"/GetCampaignDetails",
            data : DataStr,
            dataType : 'json',
            success:function(data)
            {
                $("#CampaignTypeVendor").val(data.CampaignType);
                if(data.CampaignType == 'DirectPost')
                { 
                    $("#MaxTimeOutInputVendor").css('display','block');
                    $("#MaxPingTimeOutInputVendor").css('display','none');
                    $("#MaxPostTimeOutInputVendor").css('display','none');
                } 
                else if(data.CampaignType == 'PingPost')
                {
                    $("#MaxTimeOutInputVendor").css('display','none');
                    $("#MaxPingTimeOutInputVendor").css('display','block');
                    $("#MaxPostTimeOutInputVendor").css('display','block');
                }
            }
    });
}
function GetCampaignDetailsBuyer()
{
    var CampaignID = $("#CampaignBuyer").val();
    var token = $("#_token").val();
    var url = $("#url").val();
    var DataStr = {_token : token,CampaignID:CampaignID};
    var result;
    $.ajax({
            type:"POST",
            url :url+"/GetCampaignDetails",
            data : DataStr,
            dataType : 'json',
            success:function(data)
            {
                $("#CampaignTypeBuyer").val(data.CampaignType);                
                if(data.CampaignType == 'DirectPost')
                { 
                    $("#BuyerTierIDInput").css('display','block');

                    $("#MaxTimeOutInputBuyer").css('display','block');
                    $("#MinTimeOutInputBuyer").css('display','block');

                    $("#MaxPingTimeOutInputBuyer").css('display','none');
                    $("#MaxPostTimeOutInputBuyer").css('display','none');
                    $("#MaxPingsPerMinuteInputBuyer").css('display','none');

                    $("#PingTestURLInput").css('display','none');
                    $("#PingLiveURLInput").css('display','none');
                    $("#PostTestURLInput").css('display','none');
                    $("#PostLiveURLInput").css('display','none');

                    $("#DirectPostTestURLInput").css('display','block');
                    $("#DirectPostLiveURLInput").css('display','block');
                } 
                else if(data.CampaignType == 'PingPost')
                {
                    $("#BuyerTierIDInput").css('display','none');

                    $("#MaxTimeOutInputBuyer").css('display','none');
                    $("#MinTimeOutInputBuyer").css('display','none');

                    $("#MaxPingTimeOutInputBuyer").css('display','block');
                    $("#MaxPostTimeOutInputBuyer").css('display','block');
                    $("#MaxPingsPerMinuteInputBuyer").css('display','block');

                    $("#PingTestURLInput").css('display','block');
                    $("#PingLiveURLInput").css('display','block');
                    $("#PostTestURLInput").css('display','block');
                    $("#PostLiveURLInput").css('display','block');

                    $("#DirectPostTestURLInput").css('display','none');
                    $("#DirectPostLiveURLInput").css('display','none');
                }
            }
    });
}
//////////////Buyer/////////////////////////
function IsBuyerNameExistFunction()
{
    var BuyerName       = $("#AdminLabelBuyer").val(); 
    var token = $("#_token").val();
    var url = $("#url").val();
    var DataStr = {_token : token,'BuyerName':BuyerName};
    var result;
    $.ajax({
            type:"POST",
            url :url+"/IsBuyerNameExist",
            data : DataStr,
            async : false,
            success:function(data)
            {
               result = data;
            }
    });
    return result;
}
function BuyerDetails()
{
    var flag            = 1;
    var CampaignType    = $("#CampaignTypeBuyer").val();
    var AdminLabel      = $("#AdminLabelBuyer").val();  
    var PartnerLabel    = $("#PartnerLabelBuyer").val();  
    var IntegrationFileName    = $("#IntegrationFileName").val();  
    var PayoutCalculation      = $("#PayoutCalculation").val();  
    var Revshare        = $("#RevshareBuyer").val();  
    var FixedPrice      = $("#FixedPriceBuyer").val();  
    var DailyCap        = $("#DailyCapBuyer").val();  
    var TotalCap        = $("#TotalCapBuyer").val();  
    var MaxTimeOut      = $("#MaxTimeOutBuyer").val(); 
    var MinTimeOut      = $("#MinTimeOutBuyer").val(); 
    var MaxPingTimeOut  = $("#MaxPingTimeOutBuyer").val(); 
    var MaxPostTimeOut  = $("#MaxPostTimeOutBuyer").val(); 
    var MaxPingsPerMinute  = $("#MaxPingsPerMinuteBuyer").val(); 
    var VendorStatus       = $("#VendorStatus").val();  
    var VendorTestMode     = $("#VendorTestMode").val();  
    var token           = $("#_token").val();
    var url             = $("#url").val();      
   
    if(PartnerLabel=="")
    {
        $("#PartnerLabelBuyerErr").css('color','red');
        $("#PartnerLabelBuyerErr").html('Please Enter Partner Label.');
        $("#PartnerLabelBuyerErr").show();
        flag =0;
    }
    if(AdminLabel=="")
    {
        $("#AdminLabelBuyerErr").css('color','red');
        $("#AdminLabelBuyerErr").html('Please Enter Admin Label.');
        $("#AdminLabelBuyerErr").show();
        flag =0;
    }
    if(IntegrationFileName=="")
    {
        $("#IntegrationFileNameErr").css('color','red');
        $("#IntegrationFileNameErr").html('Please Enter Integration File Name.');
        $("#IntegrationFileNameErr").show();
        flag =0;
    }               
    if(PayoutCalculation=="")
    {
        $("#PayoutCalculationBuyerErr").css('color','red');
        $("#PayoutCalculationBuyerErr").html('Please Enter Payout Calculation Type.');
        $("#PayoutCalculationBuyerErr").show();
        flag =0;
    }
    if(PayoutCalculation=="Revshare")
    {
        if(Revshare=="")
        {
            $("#RevshareBuyerErr").css('color','red');
            $("#RevshareBuyerErr").html('Please Enter Revshare Percent.');
            $("#RevshareBuyerErr").show();
            flag =0;
        }
    }
    if(PayoutCalculation=="FixedPrice")
    {
        if(FixedPrice=="" )
        {
            $("#FixedPriceBuyerErr").css('color','red');
            $("#FixedPriceBuyerErr").html('Please Enter Fixed Price.');
            $("#FixedPriceBuyerErr").show();
            flag =0;
        }
    }
    if(DailyCap=="")
    {
        $("#DailyCapBuyerErr").css('color','red');
        $("#DailyCapBuyerErr").html('Required.');
        $("#DailyCapBuyerErr").show();
        flag =0;
    }
    else if(DailyCap!="")
    {
        if(isNaN(DailyCap))
        {
            $("#DailyCapBuyerErr").css('color','red');
            $("#DailyCapBuyerErr").html('Daily Cap Values Should Be Numeric.');
            $("#DailyCapBuyerErr").show();
            flag =0;
        }
    }
    if(TotalCap=="")
    {
        $("#TotalCapBuyerErr").css('color','red');
        $("#TotalCapBuyerErr").html('Required.');
        $("#TotalCapBuyerErr").show();
        flag =0;
    }
    else if(TotalCap!="")
    {
        if(isNaN(TotalCap))
        {
            $("#TotalCapBuyerErr").css('color','red');
            $("#TotalCapBuyerErr").html('Total Cap Values Should Be Numeric.');
            $("#TotalCapBuyerErr").show();
            flag =0;
        }
    }
    if(CampaignType=="DirectPost")
    {
        if(MaxTimeOut=="")
        {
            $("#MaxTimeOutBuyerErr").css('color','red');
            $("#MaxTimeOutBuyerErr").html('Please Enter Max Time Out Values.');
            $("#MaxTimeOutBuyerErr").show();
            flag =0;
        }
        else if(MaxTimeOut!="")
        {
            if(isNaN(MaxTimeOut))
            {
                $("#MaxTimeOutBuyerErr").css('color','red');
                $("#MaxTimeOutBuyerErr").html('Max Time Out Values Should Be Numeric.');
                $("#MaxTimeOutBuyerErr").show();
                flag =0;
            }
        }
        if(MinTimeOut=="")
        {
            $("#MinTimeOutBuyerErr").css('color','red');
            $("#MinTimeOutBuyerErr").html('Please Enter Min Time Out Values.');
            $("#MinTimeOutBuyerErr").show();
            flag =0;
        }
        else if(MinTimeOut!="")
        {
            if(isNaN(MinTimeOut))
            {
                $("#MinTimeOutBuyerErr").css('color','red');
                $("#MinTimeOutBuyerErr").html('Min Time Out Values Should Be Numeric.');
                $("#MinTimeOutBuyerErr").show();
                flag =0;
            }
        }
    }
    if(CampaignType=="PingPost")
    {
        if(MaxPingTimeOut=="")
        {
            $("#MaxPingTimeOutBuyerErr").css('color','red');
            $("#MaxPingTimeOutBuyerErr").html('Please Enter Max Time Out Values.');
            $("#MaxPingTimeOutBuyerErr").show();
            flag =0;
        }
        else if(MaxPingTimeOut!="")
        {
            if(isNaN(MaxPingTimeOut))
            {
                $("#MaxPingTimeOutBuyerErr").css('color','red');
                $("#MaxPingTimeOutBuyerErr").html('Max Time Out Values Should Be Numeric.');
                $("#MaxPingTimeOutBuyerErr").show();
                flag =0;
            }
        }
        if(MaxPostTimeOut=="")
        {
            $("#MaxPostTimeOutBuyerErr").css('color','red');
            $("#MaxPostTimeOutBuyerErr").html('Please Enter Max Time Out Values.');
            $("#MaxPostTimeOutBuyerErr").show();
            flag =0;
        }
        else if(MaxPostTimeOut!="")
        {
            if(isNaN(MaxPostTimeOut))
            {
                $("#MaxPostTimeOutBuyerErr").css('color','red');
                $("#MaxPostTimeOutBuyerErr").html('Max Time Out Values Should Be Numeric.');
                $("#MaxPostTimeOutBuyerErr").show();
                flag =0;
            }
        } 
        if(MaxPingsPerMinute=="")
        {
            $("#MaxPingsPerMinuteBuyerErr").css('color','red');
            $("#MaxPingsPerMinuteBuyerErr").html('Please Enter Max Pings Per Minute Values.');
            $("#MaxPingsPerMinuteBuyerErr").show();
            flag =0;
        }
        else if(MaxPingsPerMinute!="")
        {
            if(isNaN(MaxPingsPerMinute))
            {
                $("#MaxPingsPerMinuteBuyerErr").css('color','red');
                $("#MaxPingsPerMinuteBuyerErr").html('Max Pings Per Minute Values Should Be Numeric.');
                $("#MaxPingsPerMinuteBuyerErr").show();
                flag =0;
            }
        }              
    }
    if(flag ==1)
    {               
        if(AdminLabel!='')
        {
            var IsBuyerNameExist = IsBuyerNameExistFunction();
            if(IsBuyerNameExist==true)
            {
                $("#AdminLabelBuyerErr").css('color','red');
                $("#AdminLabelBuyerErr").html('Same Admin Label Already Exist.');
                $("#AdminLabelBuyerErr").show();
                flag=0;
            }
            else
            {
                return flag;
            }
        }
    }      
}
$("#PayoutCalculationBuyer").change(function(){
     var PayoutCalculation  = $("#PayoutCalculationBuyer").val();

     if(PayoutCalculation=="RevShare")
     {
        $("#RevshareInputBuyer").show();
        $("#FixedPriceInputBuyer").hide();
     }
     else if(PayoutCalculation=="FixedPrice")
     {
        $("#RevshareInputBuyer").hide();
        $("#FixedPriceInputBuyer").show();
     }
});