$(document).ready(function() {    
    GetWebsiteVisitorReport(1);
    GetWebsiteVisitorChartAndSummary();
});
function Pagination(page)
{
    GetWebsiteVisitorReport(page);
}
function SearchWebsiteVisitorReport()
{
    GetWebsiteVisitorReport(1);
    GetWebsiteVisitorChartAndSummary();
}
function GetWebsiteVisitorReport(page)
{
    var loading=$("#loading").val();
    //$("#Lead").html('<table class="table table-striped table-bordered" style="background-color: white;"><tr><td colspan="13"><center><img src="'+loading+'" style="width: 200px"></center></td></tr></table>'); 

    var token           = $("#_token").val();
    var url             = $("#url").val(); 

    var StartDate       = $("#start").val();
    var EndDate         = $("#end").val();
    var VendorID        = $("#VendorID").val();
    var SubID           = $("#SubID").val();
    var IPAddress       = $("#IPAddress").val();
    var URL             = $("#URL").val();

    var numofrecords    = $('#numofrecords').val();
    
    var DataStr         = {'_token':token,'page':page,'numofrecords':numofrecords,'StartDate':StartDate,'EndDate':EndDate,'VendorID':VendorID,'SubID':SubID, 'IPAddress':IPAddress, 'URL':URL};
    $.ajax({
        type: "POST",
        url:  url+"/GetWebsiteVisitorReport",
        data: DataStr,
        success: function(data) 
        {
            $("#dataTable_wrapper").html(data);         
        }
    }); 
}
function GetWebsiteVisitorChartAndSummary()
{
    var token           = $("#_token").val();
    var url             = $("#url").val(); 

    var StartDate       = $("#start").val();
    var EndDate         = $("#end").val();
    var VendorID        = $("#VendorID").val();
    var SubID           = $("#SubID").val();
    var IPAddress       = $("#IPAddress").val();
    var URL             = $("#URL").val();
    
    var DataStr         = {'_token':token,'StartDate':StartDate,'EndDate':EndDate,'VendorID':VendorID,'SubID':SubID, 'IPAddress':IPAddress, 'URL':URL};
    $.ajax({
        type: "POST",
        url:  url+"/GetWebsiteVisitorChartAndSummary",
        data: DataStr,
        dataType: 'JSON',
        success: function(data) 
        {
            $("#Hits").html(data.TotalHits);
            $("#IPAddresss").html(data.UniqueIPs);
            DisplayChart(data.Dates,data.Hits,data.IPAddress);        
        }
    }); 
}


function DisplayChart(Dates,Hits,IPAddress)
{
    var DatesArray = Dates.split(',');
    var HitsArray = Hits.split(',');
    var IPAddressArray = IPAddress.split(',');
    
    var CountArray2 = new Array();
    var CountArray3 = new Array();
     
    for(var i=0; i<HitsArray.length; i++)
    {
        CountArray2.push(parseInt(HitsArray[i]));
    } 
    for(var i=0; i<IPAddressArray.length; i++)
    {
        CountArray3.push(parseInt(IPAddressArray[i]));
    } 

    var chart;
    chart = new Highcharts.Chart({
      chart: {
          renderTo: 'container',
          type: 'area'
      },
      title: {
          text: 'Website Visitors Chart'
      },
      xAxis: {
          categories: DatesArray
      },
      yAxis: {
          max: 150,
          min:0,
          maxPadding: 0.50,
          startOnTick: false,
          endOnTick: true,
          title: {
            enabled: true,
            text: '' 
          },
          labels: {
              formatter: function(){
                  return (Math.abs(this.value)) + '';
              }
          }
      },
      tooltip: {
          formatter: function() {
              return ''+
                  this.series.name +': '+ Math.abs(this.y);
          }
      },
      credits: {
          enabled: false
      },
      plotOptions: {
            series: {
                 events: {
                     legendItemClick: function(event) {
                         name = this.name.replace(' ', '');
                         this.visible ? this.chart.get(name).hide() : this.chart.get(name).show();
                     }
                 }
            }
      },
      series: [{
          id: 'error1',
          name: 'Hits',
          color:'#9ec6f1',
          data: CountArray2,
      }, {
          id:'error2',
          name: 'IP-Address',
          color:'#737172',
          data: CountArray3,
      },]
  }); 
}
