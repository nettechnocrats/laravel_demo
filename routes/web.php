
<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});
*/

Route::get('/', [ 'as' => 'admin.login', 'uses' => 'LoginController@Login' ]);
Route::get('/forgot-password', [ 'as' => 'admin.ForgotPassword', 'uses' => 'LoginController@ForgotPassword' ]);
Route::post('/AuthenticateLogin', [ 'as' => 'admin.AuthenticateLogin', 'uses' => 'LoginController@AuthenticateLogin' ]);
Route::get('/dashboard', [ 'as' => 'admin.Dashboard', 'uses' => 'DashboardController@Dashboard' ]);
Route::get('/dashboardTest', [ 'as' => 'admin.DashboardTest', 'uses' => 'DashboardController@DashboardTest' ]);
Route::get('/Logout', [ 'as' => 'admin.logout', 'uses' => 'LoginController@Logout' ]);

Route::get('/profile', [ 'as' => 'admin.Profile', 'uses' => 'DashboardController@Profile' ]);
Route::post('/ProfileUpdate', [ 'as' => 'admin.ProfileUpdate', 'uses' => 'DashboardController@ProfileUpdate' ]);
Route::post('/RedirectToLead', [ 'as' => 'admin.RedirectToLead', 'uses' => 'DashboardController@RedirectToLead' ]);

Route::get('/lead-report', [ 'as' => 'admin.LeadReportView', 'uses' => 'LeadReportController@LeadReportView' ]);
Route::post('/GetLeadReport', [ 'as' => 'admin.GetLeadReport', 'uses' => 'LeadReportController@GetLeadReport' ]);
Route::get('/lead-report-summary/{RequestID}', [ 'as' => 'admin.ViewLeadReportSummary', 'uses' => 'LeadReportController@ViewLeadReportSummary' ]);
Route::post('/GetReturnModalUsingLeadID', [ 'as' => 'admin.GetReturnModalUsingLeadID', 'uses' => 'LeadReportController@GetReturnModalUsingLeadID' ]);
Route::post('/AddReturnCode', [ 'as' => 'admin.AddReturnCode', 'uses' => 'LeadReportController@AddReturnCode' ]);

Route::post('/GetVendorBuyerUsingCampaignID', [ 'as' => 'admin.GetVendorBuyerUsingCampaignID', 'uses' => 'CommonController@GetVendorBuyerUsingCampaignID' ]);
Route::post('/GetVendorUsingVendorCompany', [ 'as' => 'admin.GetVendorUsingVendorCompany', 'uses' => 'CommonController@GetVendorUsingVendorCompany' ]);
Route::post('/GetBuyerUsingBuyerCompany', [ 'as' => 'admin.GetBuyerUsingBuyerCompany', 'uses' => 'CommonController@GetBuyerUsingBuyerCompany' ]);
Route::post('/GetUserUsingCompanyID', [ 'as' => 'admin.GetUserUsingCompanyID', 'uses' => 'CommonController@GetUserUsingCompanyID' ]);

Route::post('/GetVendorBuyerUsingCampaignIDAndShowInactiveAndTest', [ 'uses' => 'CommonController@GetVendorBuyerUsingCampaignIDAndShowInactiveAndTest' ]);
Route::post('/GetVendorUsingVendorCompanyAndShowInactiveAndTest', [ 'uses' => 'CommonController@GetVendorUsingVendorCompanyAndShowInactiveAndTest' ]);
Route::post('/GetCompanyUsingShwowInactiveAndTest', [ 'uses' => 'CommonController@GetCompanyUsingShwowInactiveAndTest' ]);
Route::post('/GetCompanyUsingShwowInactiveAndTest2', [ 'uses' => 'CommonController@GetCompanyUsingShwowInactiveAndTest2' ]);
Route::post('/GetCampaignDetails', [ 'uses' => 'CommonController@GetCampaignDetails' ]);

Route::get('/vendor-commission-report', [ 'as' => 'admin.VendorCommissionReportView', 'uses' => 'VendorCommissionReportController@VendorCommissionReportView' ]);
Route::post('/GetVendorCommissionReport', [ 'as' => 'admin.GetVendorCommissionReport', 'uses' => 'VendorCommissionReportController@GetVendorCommissionReport' ]);

Route::get('/buyer-invoice-report', [ 'as' => 'admin.BuyerInvoiceReportView', 'uses' => 'BuyerInvoiceReportController@BuyerInvoiceReportView' ]);
Route::post('/GetBuyerInvoiceReport', [ 'as' => 'admin.GetBuyerInvoiceReport', 'uses' => 'BuyerInvoiceReportController@GetBuyerInvoiceReport' ]);

Route::get('/affiliate-report', [ 'as' => 'admin.AffiliateReportView', 'uses' => 'AffiliateReportController@AffiliateReportView' ]);
Route::post('/GetAffiliateReport', [ 'as' => 'admin.GetAffiliateReport', 'uses' => 'AffiliateReportController@GetAffiliateReport' ]);


Route::get('/campaign-list', [ 'as' => 'admin.CampaignManagementView', 'uses' => 'CampaignManagementController@CampaignManagementView' ]);
Route::post('/GetCampaignList', [ 'as' => 'admin.GetCampaignList', 'uses' => 'CampaignManagementController@GetCampaignList' ]);
Route::get('/campaign-add', [ 'as' => 'admin.CampaignAddView', 'uses' => 'CampaignManagementController@CampaignAddView' ]);
Route::post('/AddCampaignDetails', [ 'as' => 'admin.AddCampaignDetails', 'uses' => 'CampaignManagementController@AddCampaignDetails' ]);
Route::get('/campaign-edit/{CampaignID}', [ 'as' => 'admin.CampaignEditView', 'uses' => 'CampaignManagementController@CampaignEditView' ]);
Route::post('/SaveCampaignDetails', [ 'as' => 'admin.SaveCampaignDetails', 'uses' => 'CampaignManagementController@SaveCampaignDetails' ]);
Route::post('/ChangeMaintenanceMode', [ 'as' => 'admin.ChangeMaintenanceMode', 'uses' => 'CampaignManagementController@ChangeMaintenanceMode' ]);
Route::post('/ChangeShowInDashboardMode', [ 'as' => 'admin.ChangeShowInDashboardMode', 'uses' => 'CampaignManagementController@ChangeShowInDashboardMode' ]);

Route::get('/company-list', [ 'as' => 'admin.CompanyManagementView', 'uses' => 'CompanyManagementController@CompanyManagementView' ]);
Route::post('/GetCompanyList', [ 'as' => 'admin.GetCompanyList', 'uses' => 'CompanyManagementController@GetCompanyList' ]);
Route::get('/company-add', [ 'as' => 'admin.CompanyAddView', 'uses' => 'CompanyManagementController@CompanyAddView' ]);
Route::post('/AddCompanyDetails', [ 'as' => 'admin.AddCompanyDetails', 'uses' => 'CompanyManagementController@AddCompanyDetails' ]);
Route::get('/company-edit/{CompanyID}', [ 'as' => 'admin.CompanyEditView', 'uses' => 'CompanyManagementController@CompanyEditView' ]);
Route::post('/SaveCompanyDetails', [ 'as' => 'admin.SaveCompanyDetails', 'uses' => 'CompanyManagementController@SaveCompanyDetails' ]);
Route::post('/ChangeCompanyStatus', [ 'as' => 'admin.ChangeCompanyStatus', 'uses' => 'CompanyManagementController@ChangeCompanyStatus' ]);
Route::post('/ChangeCompanyTestMode', [ 'as' => 'admin.ChangeCompanyTestMode', 'uses' => 'CompanyManagementController@ChangeCompanyTestMode' ]);
Route::post('/GetCityZipCodeState', ['as' => 'GetCityZipCodeState', 'uses' => 'CompanyManagementController@GetCityZipCodeState']);
Route::post('/DeleteCompanyUploadedFile', ['as' => 'DeleteCompanyUploadedFile', 'uses' => 'CompanyManagementController@DeleteCompanyUploadedFile']);
Route::post('/IsUserEmailExist', ['as' => 'IsUserEmailExist', 'uses' => 'CompanyManagementController@IsUserEmailExist']);
Route::post('/IsCompanyNameExist', ['as' => 'IsCompanyNameExist', 'uses' => 'CompanyManagementController@IsCompanyNameExist']);
Route::post('/IsVendorNameExist', ['as' => 'IsVendorNameExist', 'uses' => 'CompanyManagementController@IsVendorNameExist']);
Route::post('/IsBuyerNameExist', ['as' => 'IsBuyerNameExist', 'uses' => 'CompanyManagementController@IsBuyerNameExist']);
Route::post('/DeleteCompanyUploadedFile', ['as' => 'DeleteCompanyUploadedFile', 'uses' => 'CompanyManagementController@DeleteCompanyUploadedFile']);
Route::post('/AddUserDetailsFromCompany', ['as' => 'admin.AddUserDetailsFromCompany', 'uses' => 'CompanyManagementController@AddUserDetailsFromCompany']);

Route::post('/GetUserListFromCompany', ['uses' => 'CompanyManagementController@GetUserListFromCompany']);
Route::post('/GetUserDetailsFromCompany', ['uses' => 'CompanyManagementController@GetUserDetailsFromCompany']);

Route::post('/GetVendorListFromCompany', ['uses' => 'CompanyManagementController@GetVendorListFromCompany']);
Route::post('/GetVendorDetailsFromCompany', ['uses' => 'CompanyManagementController@GetVendorDetailsFromCompany']);
Route::post('/GetVendorCloneDetailsFromCompany', ['uses' => 'CompanyManagementController@GetVendorCloneDetailsFromCompany']);

Route::post('/GetBuyerListFromCompany', ['uses' => 'CompanyManagementController@GetBuyerListFromCompany']);
Route::post('/GetBuyerDetailsFromCompany', ['uses' => 'CompanyManagementController@GetBuyerDetailsFromCompany']);
Route::post('/GetBuyerCloneDetailsFromCompany', ['uses' => 'CompanyManagementController@GetBuyerCloneDetailsFromCompany']);


Route::get('/mapping-list', [ 'as' => 'admin.MappingList', 'uses' => 'VendorBuyerMappingController@MappingList' ]);
Route::post('/GetAllMapping', [ 'uses' => 'VendorBuyerMappingController@GetAllMapping' ]);
Route::get('/add-mapping', [ 'as' => 'AddMapping' , 'uses' => 'VendorBuyerMappingController@AddMapping']);
Route::post('/AddMappingDetails', [ 'uses' => 'VendorBuyerMappingController@AddMappingDetails' ]);
Route::get('/edit-mapping-details/{m_id}', [ 'as' => 'AddMappingDetails', 'uses' => 'VendorBuyerMappingController@EditMapping','as' => 'EditMapping' ]);
Route::post('/EditMappingDetails', [ 'as' => 'EditMappingDetails', 'uses' => 'VendorBuyerMappingController@EditMappingDetails' ]);


Route::get('/users-list', [ 'as' => 'admin.UserManagementView', 'uses' => 'UserManagementController@UserManagementView' ]);
Route::post('/GetUserList', [ 'as' => 'admin.GetUserList', 'uses' => 'UserManagementController@GetUserList' ]);
Route::get('/users-edit/{UserID}', [ 'as' => 'admin.UserEditView', 'uses' => 'UserManagementController@UserEditView' ]);
Route::post('/SaveUserDetails', [ 'as' => 'admin.SaveUserDetails', 'uses' => 'UserManagementController@SaveUserDetails' ]);
Route::post('/GeneratePassword', [ 'as' => 'admin.GeneratePassword', 'uses' => 'UserManagementController@GeneratePassword' ]);
Route::get('/users-add', [ 'as' => 'admin.UserAddView', 'uses' => 'UserManagementController@UserAddView' ]);
Route::post('/AddUserDetails', [ 'as' => 'admin.AddUserDetails', 'uses' => 'UserManagementController@AddUserDetails' ]);
Route::post('/ChangeUserStatus', [ 'as' => 'admin.ChangeUserStatus', 'uses' => 'UserManagementController@ChangeUserStatus' ]);
Route::post('/GetUserDetails', [ 'as' => 'admin.GetUserDetails', 'uses' => 'UserManagementController@GetUserDetails' ]);


Route::get('/vendor-list', [ 'as' => 'admin.VendorManagementView', 'uses' => 'VendorManagementController@VendorManagementView' ]);
Route::post('/GetVendorList', [ 'as' => 'admin.GetVendorList', 'uses' => 'VendorManagementController@GetVendorList' ]);
Route::post('/ChangeVendorStatus', [ 'as' => 'admin.ChangeVendorStatus', 'uses' => 'VendorManagementController@ChangeVendorStatus' ]);
Route::post('/ChangeVendorTestMode', [ 'as' => 'admin.ChangChangeVendorTestModeeVendorStatus', 'uses' => 'VendorManagementController@ChangeVendorTestMode' ]);
Route::get('/vendor-edit/{VendorID}', [ 'as' => 'admin.VendorEditView', 'uses' => 'VendorManagementController@VendorEditView' ]);
Route::post('/SaveVendorDetails', [ 'as' => 'admin.SaveVendorDetails', 'uses' => 'VendorManagementController@SaveVendorDetails' ]);
Route::post('/SetSessionForCloneVendor', [ 'as' => 'admin.SetSessionForCloneVendor', 'uses' => 'VendorManagementController@SetSessionForCloneVendor' ]);
Route::get('/add-vendor', [ 'as' => 'admin.VendorAddView', 'uses' => 'VendorManagementController@VendorAddView' ]);
Route::post('/AddVendorDetails', [ 'as' => 'admin.AddVendorDetails', 'uses' => 'VendorManagementController@AddVendorDetails' ]);
Route::post('/SendEmailToAdminUser', [ 'as' => 'admin.SendEmailToAdminUser', 'uses' => 'VendorManagementController@SendEmailToAdminUser' ]);
Route::post('/GetPartnetLabel', [ 'as' => 'admin.GetPartnetLabel', 'uses' => 'VendorManagementController@GetPartnetLabel' ]);
Route::post('/SavePartnetLabel', [ 'as' => 'admin.SavePartnetLabel', 'uses' => 'VendorManagementController@SavePartnetLabel' ]);
Route::post('/GetVendorDetails', [ 'as' => 'admin.GetVendorDetails', 'uses' => 'VendorManagementController@GetVendorDetails' ]);
Route::post('/GetVendorCloneDetails', [ 'as' => 'admin.GetVendorCloneDetails', 'uses' => 'VendorManagementController@GetVendorCloneDetails' ]);


Route::get('/buyer-list', [ 'as' => 'admin.BuyerManagementView', 'uses' => 'BuyerManagementController@BuyerManagementView' ]);
Route::post('/GetBuyerList', [ 'as' => 'admin.GetBuyerList', 'uses' => 'BuyerManagementController@GetBuyerList' ]);
Route::post('/ChangeBuyerStatus', [ 'as' => 'admin.ChangeBuyerStatus', 'uses' => 'BuyerManagementController@ChangeBuyerStatus' ]);
Route::post('/ChangeBuyerTestMode', [ 'as' => 'admin.ChangeBuyerTestMode', 'uses' => 'BuyerManagementController@ChangeBuyerTestMode' ]);
Route::get('/buyer-edit/{BuyerID}', [ 'as' => 'admin.BuyerEditView', 'uses' => 'BuyerManagementController@BuyerEditView' ]);
Route::post('/SaveBuyerDetails', [ 'as' => 'admin.SaveBuyerDetails', 'uses' => 'BuyerManagementController@SaveBuyerDetails' ]);
Route::post('/DeleteBuyerUploadedFile', [ 'as' => 'admin.DeleteBuyerUploadedFile', 'uses' => 'BuyerManagementController@DeleteBuyerUploadedFile' ]);
Route::post('/SetSessionForCloneBuyer', [ 'as' => 'admin.SetSessionForCloneBuyer', 'uses' => 'BuyerManagementController@SetSessionForCloneBuyer' ]);
Route::get('/add-buyer', [ 'as' => 'admin.BuyerAddView', 'uses' => 'BuyerManagementController@BuyerAddView' ]);
Route::post('/AddBuyerDetails', [ 'as' => 'admin.AddBuyerDetails', 'uses' => 'BuyerManagementController@AddBuyerDetails' ]);
Route::post('/GetBuyerPartnetLabel', [ 'as' => 'admin.GetBuyerPartnetLabel', 'uses' => 'BuyerManagementController@GetBuyerPartnetLabel' ]);
Route::post('/SaveBuyerPartnetLabel', [ 'as' => 'admin.SaveBuyerPartnetLabel', 'uses' => 'BuyerManagementController@SaveBuyerPartnetLabel' ]);
Route::post('/GetBuyerDetails', [ 'as' => 'admin.GetBuyerDetails', 'uses' => 'BuyerManagementController@GetBuyerDetails' ]);
Route::post('/GetBuyerCloneDetails', [ 'as' => 'admin.GetBuyerCloneDetails', 'uses' => 'BuyerManagementController@GetBuyerCloneDetails' ]);


Route::get('/error-log', [ 'as' => 'admin.ErrorLogView', 'uses' => 'ErrorLogController@ErrorLogView' ]);
Route::post('/GetErrorLogList', [ 'as' => 'admin.GetErrorLogList', 'uses' => 'ErrorLogController@GetErrorLogList' ]);


## Analyze vendor responses Tool ###
Route::get('/vendor-response-view', [ 'uses' => 'VendorResponseController@VendorResponseView','as' => 'admin.VendorResponseView' ]);
Route::post('/VendorResponse', [ 'uses' => 'VendorResponseController@VendorResponse','as' => 'VendorResponse' ]);
Route::post('/GetVendorResponseList', [ 'uses' => 'VendorResponseController@GetVendorResponseList','as' => 'GetVendorResponseList' ]);

### Analyze Buyer responses Tool ###
Route::get('/buyer-response-view', [ 'uses' => 'BuyerResponseController@BuyerResponseView','as' => 'admin.BuyerResponseView' ]);
Route::post('/BuyerResponse', [ 'uses' => 'BuyerResponseController@BuyerResponse','as' => 'BuyerResponse' ]);
Route::post('/GetBuyerResponseList', [ 'uses' => 'BuyerResponseController@GetBuyerResponseList','as' => 'GetBuyerResponseList' ]);

//////////////////////
Route::get('/sold-lead-report', [ 'as' => 'admin.ViewSoldLeadReportView', 'uses' => 'ViewProcessedLeadReportController@ViewSoldLeadReportView' ]);
Route::post('/GetViewSoldLeadReport', [ 'as' => 'admin.GetViewSoldLeadReport', 'uses' => 'ViewProcessedLeadReportController@GetViewSoldLeadReport' ]);
Route::post('/GetVendorsUsingCampaignID', [ 'as' => 'admin.GetVendorsUsingCampaignID', 'uses' => 'ViewProcessedLeadReportController@GetVendorsUsingCampaignID' ]);

Route::get('/purchased-lead-report', [ 'as' => 'admin.ViewPurchasedLeadReportView', 'uses' => 'ViewProcessedLeadReportController@ViewPurchasedLeadReportView' ]);
Route::post('/GetViewPurchasedLeadReport', [ 'as' => 'admin.GetViewPurchasedLeadReport', 'uses' => 'ViewProcessedLeadReportController@GetViewPurchasedLeadReport' ]);
Route::post('/GetBuyersUsingCampaignID', [ 'as' => 'admin.GetBuyersUsingCampaignID', 'uses' => 'ViewProcessedLeadReportController@GetBuyersUsingCampaignID' ]);


Route::get('/purchased-report-summary/{LeadID}/{CampaignID}', [ 'as' => 'admin.ViewPurchasedLeadReportSummary', 'uses' => 'ViewProcessedLeadReportController@ViewPurchasedLeadReportSummary' ]);




Route::get('/website-visitor-report', [ 'as' => 'admin.WebsiteVisitorReportView', 'uses' => 'WebsiteVisitorReportController@WebsiteVisitorReportView' ]);
Route::post('/GetWebsiteVisitorReport', [ 'as' => 'admin.GetWebsiteVisitorReport', 'uses' => 'WebsiteVisitorReportController@GetWebsiteVisitorReport' ]);
Route::post('/GetWebsiteVisitorChartAndSummary', [ 'as' => 'admin.GetWebsiteVisitorChartAndSummary', 'uses' => 'WebsiteVisitorReportController@GetWebsiteVisitorChartAndSummary' ]);

Route::get('/import-returns', [ 'as' => 'admin.ImportReturnsView', 'uses' => 'ReturnsController@ImportReturnsView' ]);
Route::post('/GetReturnCodeList', [ 'as' => 'admin.GetReturnCodeList', 'uses' => 'ReturnsController@GetReturnCodeList' ]);
Route::post('/ImportReturnsProcess', [ 'as' => 'admin.ImportReturnsProcess', 'uses' => 'ReturnsController@ImportReturnsProcess' ]);
Route::post('/UploadedFilesDetails', [ 'as' => 'admin.UploadedFilesDetails', 'uses' => 'ReturnsController@UploadedFilesDetails' ]);
Route::get('/DownloadUploadedReturns/{FileNo}', [ 'as' => 'admin.DownloadUploadedReturns' ,'uses' => 'ReturnsController@DownloadUploadedReturns']);

Route::get('/export-returns', [ 'as' => 'admin.ExportReturnsView', 'uses' => 'ReturnsController@ExportReturnsView' ]);
Route::post('/GetExportReport', [ 'as' => 'admin.GetExportReport' ,'uses' => 'ReturnsController@GetExportReport']);
Route::post('/IsExportCSVOrNot', ['as' => 'admin.IsExportCSVOrNot' , 'uses' => 'ReturnsController@IsExportCSVOrNot']);
Route::post('/EmailExportReturnCSV', ['as' => 'admin.EmailExportReturnCSV' , 'uses' => 'ReturnsController@EmailExportReturnCSV']);
Route::get('/ExportReturns/{CampaignID}/{startDate}/{endDate}/{VendorCID}', [ 'as' => 'admin.ExportReturns', 'uses' => 'ReturnsController@ExportReturns']);


Route::get('/view-specs/{CampaignID}/{v_id}', [ 'as' => 'admin.ViewSpecs','uses' => 'ViewSpecsController@ViewSpecs' ]);

Route::get('/GeneratePDF/{CampaignID}', [ 'uses' => 'ViewSpecsController@GeneratePDF', 'as' => 'GeneratePDF']);
Route::post('GeneratePDF/{CampaignID}', [ 'uses' => 'ViewSpecsController@GeneratePDF', 'as' => 'GeneratePDF' ]);
