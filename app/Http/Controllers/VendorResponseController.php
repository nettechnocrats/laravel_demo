<?php
namespace App\Http\Controllers;
use Route;
use Mail;
use Auth, Hash;
use Validator;
use DB;
use Session;
use Redirect;
use Excel;
use PDF;
use Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Pagination;
use App\Http\Models\VendorResponseModel;
use App\Http\Models\MasterConfigModel;

class VendorResponseController extends Controller
{
	public function __construct(Request $request)
	{	
		$this->Pagination 			= new Pagination();
		$this->VRModel 				= new VendorResponseModel();
	}

	public function VendorResponseView(Request $request)
	{
		$CampaignDropDown = $this->VRModel->CampaignDropDown();
		$StartDate    				= date("Y-m-d");
        $EndDate    				= date("Y-m-d");
		$Data['CampaignDropDown']   = $CampaignDropDown;
		$Data['StartDate']   		= $StartDate;
		$Data['EndDate']   			= $EndDate;
		$Data['Menu']   			= "Report";
        $Data['Title']  			= "Admin | VendorResponseReport";
	    return view('VendorResponseView')->with($Data);
	}
	public function VendorResponse(Request $request) 
	{
    	$data 		= $request->all();
    	$CampaignID = $data['CampaignID'];
    	$Action 	= $data['Action'];

    	$VendorResponse = $this->VRModel->VendorResponse($CampaignID,$Action);
    	?>
		<option value="">Select Response</option>
		<?php
    	foreach($VendorResponse as $vr)
		{	

			if($Action == 'DirectPost') {
				?>
					<option value="<?php echo $vr->DirectPostResponse; ?>"><?php echo $vr->DirectPostResponse; ?></option>
				<?php
			} else if($Action == "Ping") {
				?>
					<option value="<?php echo $vr->PingResponse; ?>"><?php echo $vr->PingResponse; ?></option>
				<?php
			} else if($Action == "Post" || $Action == "PingPost") {
				$Select = "PostResponse";
				?>
					<option value="<?php echo $vr->PostResponse; ?>"><?php echo $vr->PostResponse; ?></option>
				<?php
			}
		}
		exit();
    }
	public function GetVendorResponseList(Request $request)
	{
		$Data = $request->all();

        $NumOfRecords = $Data['numofrecords'];
        
        $page = $Data['page'];
        $cur_page = $page;

        $Limitpage = $page - 1;
        $Start = $Limitpage * $NumOfRecords;
        $CampaignID      = $Data["CampaignID"];
    	$VendorCompany   = $Data["VendorCompany"];
    	$Vendor          = $Data["Vendor"];
    	$Response        = $Data["Response"];
    	$StartDate       = $Data["StartDate"];
    	$EndDate         = $Data["EndDate"];
    	$Sort            = $Data["Sort"];
    	$Action          = $Data["Action"];
        
        $Search = array('Start' 		=> $Start,
			            'NumOfRecords' 	=> $NumOfRecords,
			            'CampaignID' 	=> $CampaignID,
			            'VendorCompany' => $VendorCompany,
			            'Vendor' 		=> $Vendor,
			            'Response' 		=> $Response,
			            'StartDate' 	=> $StartDate,
			            'EndDate' 		=> $EndDate,
			            'Sort' 			=> $Sort,
			            'Action' 		=> $Action
			            );
        $GetVendorResponseList 		= $this->VRModel->GetVendorResponseList($Search);
       
        $AllVendorResponseData 		= $GetVendorResponseList['Res'];
        $VendorResponseCount 		= $GetVendorResponseList['Count'];

        $Pagination = $this->Pagination->Links($NumOfRecords, $VendorResponseCount, $page);
        ?>
		<div id="dataTable_wrapper" class="dataTables_wrapper dt-bootstrap4">
			<div class="row">
				<div class="heading text-center">
				  <h2>Vendor Response Report</h2>
				</div>
				<div class="table-responsive">
				     	<table class="table table-bordered tableFont" id="dataTable" width="100%">
						    <thead>
						        <tr role="row">
						            <th style="color: #c94142;" scope="col">Row</th>
						            <th style="color: #c94142;" scope="col">Campaign Name</th>
						            <th style="color: #c94142;" scope="col">Vendor Name</th>
						            <th style="color: #c94142;" scope="col">Response</th>
						            <th style="color: #c94142;" scope="col">Message</th>
						            <th style="color: #c94142;" scope="col">Count</th>
						        </tr>
						    </thead>
						    <tbody>
				     		<?php
					     	if(!empty($AllVendorResponseData ))
					     	{
					     		
					     		$i = $Start + 1;
					     		foreach($AllVendorResponseData as $vld)
					     		{
								     ?>				       
							        <tr>
							           	<td ><?=$i?></td>
							           	<td><?php echo $vld->CampaignName; ?></td>
							           	<td><?php echo $vld->VendorName; ?></td>
							           	<td><?php echo $vld->Response; ?></td>
							           	<td><?php echo $vld->Message; ?></td>							           	
							           	<td><?php echo $vld->Count; ?></td>							           	
							        </tr>
				     				<?php 
				     				$i++;
				     			}
				     		} 
				     		else 
				     		{
				     			?>
						     	<tr >
						           <td colspan="6" style="color: #05aefd;">Record Not Found.</td>
						           </td>
						        </tr>
				     			<?php 
				     		} 
				     	?>
				     </tbody>
				  </table>
				</div>
			</div>
		<?php if(!empty($AllVendorResponseData ))
		{ ?>
			<div class="row mt-3">			
				<div class="col-md-6 col-sm-6 col-6 noPaddd">
					<div class="dataTables_length mCenter" id="dataTable_length">
						<label>Records/Pages
							<select id="numofrecords" onchange="GetVendorResponseList(1);" name="dataTable_length" aria-controls="dataTable" class="custom-select custom-select-sm form-control form-control-sm">
							<option value="10" <?php if($NumOfRecords=='10'){ echo 'selected'; } ?> >10</option>
							<option value="20" <?php if($NumOfRecords=='20'){ echo 'selected'; } ?> >20</option>
							<option value="50" <?php if($NumOfRecords=='50'){ echo 'selected'; } ?> >50</option>
							<option value="100" <?php if($NumOfRecords=='100'){ echo 'selected'; } ?> >100</option>
							<option value="150" <?php if($NumOfRecords=='150'){ echo 'selected'; } ?> >150</option>
							<option value="200" <?php if($NumOfRecords=='200'){ echo 'selected'; } ?> >200</option>
							</select>
						</label>
					</div>
				</div>
				<div class="col-md-6 col-sm-6 col-6">
					<div class="dataTables_length mCenter" id="dataTable_length" style="text-align: right;">
						<label>
							<?php echo $Pagination; ?>
						</label>
					</div>
				</div>
			</div>
      	<?php
      	}
      	else
      	{
      		?>
      		<input type="hidden" name="numofrecords" id="numofrecords" value='10'>
      		<?php
      	}
       	exit();
	}
}
