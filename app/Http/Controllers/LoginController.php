<?php
namespace App\Http\Controllers;
use Route;
use Mail;
use Auth, Hash;
use Validator;
use Session;
use Redirect;
use DB;
use Crypt;
use Illuminate\Http\Request;
use App\Http\Models\LoginModel;
use Illuminate\Routing\ResponseFactory;
use App\Http\Models\MasterConfigModel;
use App\Http\Controllers\Controller;

class LoginController extends Controller {

	public function __construct(Request $request)
	{
		$this->LoginModel = new LoginModel();
		$this->MasterConfigModel = new MasterConfigModel();
	}
	public function Login(Request $request)
	{		
		$Data['Menu'] 	= "Login";
		$Data['Title'] 	= "Admin | Login";
		return View('Login')->with($Data);
	}
	public function AuthenticateLogin(Request $request)
	{		
		$Data 		= $request->all();
		$Email 		= $Data['email'];
		$Passowrd 	= $Data['password'];
		
		$CheckUserExist = $this->LoginModel->CheckUserExist($Email,$Passowrd);

		if (empty($CheckUserExist)) 
		{
			Session::flash('message', 'Wrong Email/Password.'); 
	        Session::flash('alert-class', 'alert-danger'); 
	        return Redirect::route('admin.login');
		}
		$UserPassword = $CheckUserExist->UserPassword;
		if (md5($Passowrd)!==$UserPassword) 
		{
			Session::flash('message', 'Wrong Password.'); 
	        Session::flash('alert-class', 'alert-danger'); 
	        return Redirect::route('admin.login');
		}
		$UserStatus = $CheckUserExist->UserStatus;
		if ($UserStatus == 0) 
		{			
			$AdminEmail = $this->MasterConfigModel->getAdminEmail();
			Session::flash('message', 'Sorry, your account is currently disabled. Please contact '. $AdminEmail.' for assistance.'); 
	        Session::flash('alert-class', 'alert-danger'); 
	        return Redirect::route('admin.login');
		}

		if($request->has('remember'))
		{
			setcookie ("username",$request->email,time()+ (86400 * 14));
			setcookie ("password",$request->password,time()+ (86400 * 14));
			setcookie ("remember",1,time()+ (86400 * 14));			
		}
		else
		{
			setcookie ("username","");
			setcookie ("password","");
			setcookie ("remember","");
		}

		Session::put('user_id', $CheckUserExist->UserID);
		Session::put('user_firstname', $CheckUserExist->UserFirstName);
		Session::put('user_lastname', $CheckUserExist->UserLastName);
		Session::put('user_role', $CheckUserExist->UserRole);
		//Session::put('user_type', $userType);
		Session::put('ShowAPISpecs', $CheckUserExist->ShowAPISpecs);
		Session::put('ShowAffLinks', $CheckUserExist->ShowAffLinks);
		Session::put('user_companyID', $CheckUserExist->CompanyID);
		//Session::put('user_DataProcessingCompany', $getDPC->DataProcessingCompany);

		return Redirect::route('admin.Dashboard');
	}

	public function Logout()
	{		
		session()->regenerate();
		Session::put('user_id', '');
		Session::put('user_firstname', '');
		Session::put('user_lastname', '');
		Session::put('user_role', '');
		//Session::put('user_type', '');
		//Session::put('user_companyID', '');
		//Session::put('SignupProcess', '');
		Session::put('ShowAPISpecs', '');
		Session::put('ShowAffLinks', '');
		Session::put('user_companyID', '');
		//Session::put('user_DataProcessingCompany', '');
		$sucess =  'Logout successfully.';

		Session::flash('message', 'Logout successfully.'); 
        Session::flash('alert-class', 'alert-success'); 
        return Redirect::route('admin.login');
	}

	public function ForgotPassword(Request $request)
	{		
		$Data['Menu'] 	= "Login";
		$Data['Title'] 	= "Admin | Forgot Password";
		return View('ForgotPassword')->with($Data);
	}
}
