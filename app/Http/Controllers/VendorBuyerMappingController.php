<?php
namespace App\Http\Controllers;

use Route;
use Mail;
use Auth, Hash;
use Validator;
use DB;
use Session;
use Excel;
use Redirect;
use Illuminate\Http\Request;
use App\Http\Models\Dashboard;
use App\Http\Controllers\Pagination;
use App\Http\Models\VendorBuyerMappingModel;
use App\Http\Models\MasterConfigModel;

class VendorBuyerMappingController extends Controller
{
	/*
	 * Display Revenue Till Date
	 *
	 * @param NULL
	 * @return to dashboard
	 */
	public function __construct(Request $request)
	{
		$this->Pagination = new Pagination();
		$this->MasterConfigModel = new MasterConfigModel();
		$this->VBMModel = new VendorBuyerMappingModel();
	}
	public function MappingList(Request $request)
	{
		if(!$request->session()->has('user_id'))
        {
            return redirect()->action('LoginController@view');
        }
        $userRole = Session::get('user_role');
        $companyID = Session::get('user_companyID');
        $userID = Session::get('user_id');

        $Data['CampaignDropDown'] = $this->MasterConfigModel->getCampaignList();
		$BuyerTierList = $this->VBMModel->BuyerTierList();

		$Data['Menu']   		= "SuperAdmin";
    	$Data['Title']  		= "Admin | Vendor/Buyer Mapping";

		return view('MappingList')->with($Data);
	}

	/*public function GetAllMapping(Request $request)
	{
		$data   = $request->all();

		$userRole = Session::get('user_role');
        $companyID = Session::get('user_companyID');
        $userID = Session::get('user_id');

		$Searcher=array('vendor_company_id'=>$data['vendor_company_id'],
						'vendor_id'=>$data['vendor_id'],
						'ShowInactiveVendor'=>$data['ShowInactiveVendor'],
						'ShowTestVendor'=>$data['ShowTestVendor'],
						'sort_order_clause'=>$data['sort_order_clause'],
						'Campaign'=>$data['campaign'],
						'buyer_id'=>$data['BuyerID']
						);

		$GetAllMapping=$this->VBMModel->GetAllMapping($Searcher);

		$GetTierBG=$this->VBMModel->GetTierBG();
		$GetTierC=$this->VBMModel->GetTierC();
		if(!empty($GetAllMapping))
		{
			$VendorID=$data['vendor_id'];
			$Display="readonly";
			if( $VendorID!="")
			{
				$Display="";
			}

			$bgColor = 'white';
			$tier = '';

			foreach($GetAllMapping as $m)
			{
				$BuyerIDLabel="";
				if($m->BuyerStatus==1)
				{
					$BuyerIDLabel = "<b><font color='green'> BID:". $m->BuyerID."</font></b>";
				}
				else if($m->BuyerStatus==0)
				{
					$BuyerIDLabel = "<b><font color='red'> BID:". $m->BuyerID."</font></b>";
				}
				$BuyerCompanyIDLabel="";
				if($m->CompanyStatus==1)
				{
					$BuyerCompanyIDLabel = "<b><font color='green'> CID:". $m->BuyerCompanyID."</font></b>";
				}
				else if($m->CompanyStatus==0)
				{
					$BuyerCompanyIDLabel = "<b><font color='red'> CID:". $m->BuyerCompanyID."</font></b>";
				}

				$BuyerLabel="";
				if($m->BuyerStatus==0 && $m->CompanyStatus==0)
				{
					$BuyerLabel = "<del>". $m->BuyerAdminLabel."</del>";
				}
				else if($m->BuyerStatus==1 && $m->CompanyStatus==1)
				{
					$BuyerLabel = $m->BuyerAdminLabel ;
				}
				else if($m->BuyerStatus==1 && $m->CompanyStatus==0)
				{
					$BuyerLabel = "<del>". $m->BuyerAdminLabel."</del>";
				}
				else if($m->BuyerStatus==0 && $m->CompanyStatus==1)
				{
					$BuyerLabel = "<strike>". $m->BuyerAdminLabel."</strike>";
				}

				if ( $m->Tier != $tier ) {
					$tier = $m->Tier;
					$bgColor = ( $bgColor == 'white' ) ? 'lightgray' : 'white';
				}

				?>
				<!-- style="background-color:<?php //echo $GetTierBG[$m->Tier]; ?>;" -->
				<tr id="MappingRowID_<?php echo $m->ID; ?>" bgcolor="<?php echo $bgColor; ?>">
					<td><?php echo $m->VendorAdminLabel; ?> (VID:<?php echo $m->VendorID; ?> / CID: <?php echo $m->VendorCompanyID; ?>)</td>
					<td><?php echo $m->Tier; ?></td>
					<td></td>
					<td><?php echo $BuyerLabel." ( ".$BuyerIDLabel." / ".$BuyerCompanyIDLabel." ) "; ?></td>
					<td><?php echo $m->FixedPrice; ?></td>
					<td>
						<input type="text" class="form-control"
						value="<?php echo $m->BuyerSortOrder; ?>"
						name="SortOrders[]" onkeypress="return isNumberKey(event)" style="width:60px;"
						<?php echo $Display; ?>>
						<input type="hidden"
						value="<?php echo $m->ID; ?>"
						name="MappingIDs[]" >
						<input type="hidden"
						value="<?php echo $m->BuyerID; ?>"
						name="BuyerIDs[]" >
					</td>
					<td id="StatusDiv_<?php echo $m->ID; ?>">
						<?php
						if($m->MappingStatus=='0')
						{
							?>
								<a href="javascript:void(0);" class="btn btn-danger"
									onclick="ChangeMappingStatus('<?php echo $m->ID; ?>',1);">
									<i class="fa fa-square-o" aria-hidden="true"></i>
								</a>
							<?php
						}
						else if($m->MappingStatus=='1')
						{
							?>
								<a href="javascript:void(0);" class="btn btn-success"
									 onclick="ChangeMappingStatus('<?php echo $m->ID; ?>',0);">
									<i class="fa fa-check-square" aria-hidden="true"></i>
								</a>
							<?php
						}
						?>
					</td>
	                <td class="text-center">
	                    <input type="checkbox" class="custom_check" id="DeleteMapping_<?php echo $m->ID; ?>" name="DeleteMapping[]"
	                    	value="<?php echo $m->ID; ?>" onclick="ShowDeleteMapping();">
	                    <label for="DeleteMapping_<?php echo $m->ID; ?>"></label>
	                </td>
				</tr>
				<?php
			}
		}
		else
		{
			?>
				<tr>
					<td colspan="8">
						Data Not Found!
	                </td>
				</tr>
			<?php
		}
		exit();
	}
	public function AddMapping(Request $request)
	{
		$userRole = Session::get('user_role');
        $companyID = Session::get('user_companyID');
        $userID = Session::get('user_id');

		$VendorCompanyList=$this->VBMModel->VendorCompanyList();
		$BuyerCompanyList=$this->VBMModel->BuyerCompanyList();

		$VendorList=$this->VBMModel->VendorListByCompanyID($companyID,$userRole);
		$BuyerList=$this->VBMModel->BuyerListByCompanyID($companyID,$userRole);
		$BuyerTierList=$this->VBMModel->BuyerTierList();

		return view('addMapping')->with(array('VendorList'=>$VendorList,
												'BuyerList'=>$BuyerList,
												'VendorCompanyList'=>$VendorCompanyList,
												'BuyerCompanyList'=>$BuyerCompanyList,
												'BuyerTierList'=>$BuyerTierList,
												'userRole'=>$userRole));
	}
	public function AddMappingDetails(Request $request)
	{
		$data   = $request->all();

		$Details=array('VendorID'=>$data['vendor_id'],
						'Tier'=>$data['tier'],
						'BuyerID'=>$data['buyer_id']
						);

		$Add=$this->VBMModel->AddMappingDetails($Details);
		if($Add)
		{
			Session::flash('message', 'Vendor/Buyer Mapping Details Has been Added.');
	        Session::flash('alert-class', 'alert-success');
	        return redirect()->action('VendorBuyerMappingController@AddMapping');
		}
		else
      	{
          Session::flash('message', 'OOPS! Something Wrong Please Try Again.');
          Session::flash('alert-class', 'alert-danger');
          return redirect()->action('VendorBuyerMappingController@AddMapping');
      	}
	}
	public function EditMapping(Request $request, $m_id)
	{
		$ID= base64_decode($m_id);

		$userRole = Session::get('user_role');
        $companyID = Session::get('user_companyID');
        $userID = Session::get('user_id');

		$VendorList=$this->VBMModel->VendorListByCompanyID($companyID,$userRole);
		$BuyerList=$this->VBMModel->BuyerListByCompanyID($companyID,$userRole);
		$BuyerTierList=$this->VBMModel->BuyerTierList();

		$MappingDetails=$this->VBMModel->MappingDetails($ID);

		return view('editMapping')->with(array('VendorList'=>$VendorList,
												'BuyerList'=>$BuyerList,
												'BuyerTierList'=>$BuyerTierList,
												'MappingDetails'=>$MappingDetails,
												'userRole'=>$userRole));
	}
	public function EditMappingDetails(Request $request)
	{
      	$data   = $request->all();
		$ID=$data['mapping_id'];

		$Details=array('VendorID'=>$data['vendor'],
						'Tier'=>$data['tier'],
						'BuyerID'=>$data['buyer'],
						'BuyerSortOrder'=>$data['buyer_sort_order'],
						'MappingStatus'=>$data['status']
						);

		$Add=$this->VBMModel->EditMappingDetails($ID,$Details);
		if($Add)
		{
			Session::flash('message', 'Vendor/Buyer Mapping Details Has been Updated.');
	        Session::flash('alert-class', 'alert-success');
	        return Redirect::route('EditMapping',base64_encode($ID));
		}
		else
      	{
           Session::flash('message', 'OOPS! Something Wrong Please Try Again.');
           Session::flash('alert-class', 'alert-danger');
           return Redirect::route('EditMapping',base64_encode($ID));
      	}
	}
	public function GetVendorListByVendorCompany(Request $request)
	{
		$data   = $request->all();
		$vendor_company_id 	= $data['vendor_company_id'];
		$ShowInactiveVendor = $data['ShowInactiveVendor'];
		$ShowTestVendor 	= $data['ShowTestVendor'];

		$ShowInactiveVendorList = $data['ShowInactiveVendorList'];
		$ShowTestVendorList 	= $data['ShowTestVendorList'];
		$CampaignID 			= $data['CampaignID'];

		$VendorList=$this->VBMModel->VendorList($CampaignID,$vendor_company_id,$ShowInactiveVendor,$ShowTestVendor,$ShowInactiveVendorList,$ShowTestVendorList);
		?>
		<option value="">Select Vendor</option>
		<?php
		foreach($VendorList as $val)
		{
			?>
			<option value="<?php echo $val->VendorID; ?>"><?php echo $val->AdminLabel; ?> (VID:<?php echo $val->VendorID; ?> / CID:<?php echo $val->CompanyID; ?>)</option>
			<?php
		}
		exit();
	}
	public function GetBuyerListByBuyerCompany(Request $request)
	{
		$data   = $request->all();
		$buyer_company_id 	= $data['buyer_company_id'];
		$ShowInactiveBuyer 	= $data['ShowInactiveBuyer'];
		$ShowTestBuyer 		= $data['ShowTestBuyer'];

		$ShowInactiveBuyerList = $data['ShowInactiveBuyerList'];
		$ShowTestBuyerList 	= $data['ShowTestBuyerList'];

		$BuyerList=$this->VBMModel->BuyerList($buyer_company_id,$ShowInactiveBuyer,$ShowTestBuyer,$ShowInactiveBuyerList,$ShowTestBuyerList);
		?>
		<option value="">Select Buyer</option>
		<?php
		foreach($BuyerList as $val)
		{
			?>
			<option value="<?php echo $val->BuyerID; ?>"><?php echo $val->AdminLabel; ?> (BID:<?php echo $val->BuyerID; ?> / CID:<?php echo $val->CompanyID; ?>)</option>
			<?php
		}
		exit();
	}
	public function GetSelectedVendorCompany(Request $request)
	{
		$data   = $request->all();
		$ShowInactive = $data['ShowInactiveVendor'];
		$ShowTest = $data['ShowTestVendor'];
		$CampaignID = $data['CampaignID'];

		$GetSelectedVendorCompany=$this->VBMModel->GetSelectedVendorCompany($CampaignID,$ShowInactive,$ShowTest);
		?>
		<option value="All">All Vendor Company</option>
		<?php
		foreach($GetSelectedVendorCompany as $key=>$val)
		{
			?>
			<option value="<?php echo $key; ?>"><?php echo $val; ?> (CID:<?php echo $key; ?>)</option>
			<?php
		}
		exit();
	}
	public function GetSelectedBuyerCompany(Request $request)
	{
		$data   = $request->all();
		$ShowInactive = $data['ShowInactiveBuyer'];
		$ShowTest = $data['ShowTestBuyer'];
		$CampaignID = $data['CampaignID'];
		$GetSelectedBuyerCompany=$this->VBMModel->GetSelectedBuyerCompany($CampaignID,$ShowInactive,$ShowTest);
		?>
		<option value="All">All Buyer Company</option>
		<?php
		foreach($GetSelectedBuyerCompany as $key=>$val)
		{
			?>
			<option value="<?php echo $key; ?>"><?php echo $val; ?> (CID:<?php echo $key; ?>)</option>
			<?php
		}
		exit();
	}
	public function UpdatSortOrdersForSelectedMapping(Request $request)
	{
		$data   	= $request->all();
		$SortOrders = $data['SortOrders'];
		$MappingIDs = $data['MappingIDs'];
		$UpdatSortOrders=$this->VBMModel->UpdatSortOrdersForSelectedMapping($SortOrders,$MappingIDs);
		if($UpdatSortOrders)
		{
			echo 1;
		}
		else
		{
			echo 0;
		}
		exit();
	}
	public function SortWithFixedPrice(Request $request)
	{
		$data   	= $request->all();
		$VendorID 	= $data['VendorID'];
		//$TierID 	= $data['TierID'];
		$Sort=$this->VBMModel->SortWithFixedPrice($VendorID);
		if($Sort)
		{
			echo 1;
		}
		else
		{
			echo 0;
		}
		exit();
	}
	public function ChangeMappingStatus(Request $request)
	{
		$data   	= $request->all();
		$MappingID 	= $data['MappingID'];
		$Status 	= $data['Status'];
		$update=$this->VBMModel->ChangeMappingStatus($MappingID,$Status);
		if($update)
		{
			if($Status=='0')
			{
				?>
					<a href="javascript:void(0);" class="btn btn-danger"
						onclick="ChangeMappingStatus('<?php echo $MappingID; ?>',1);">
						<i class="fa fa-square-o" aria-hidden="true"></i>
					</a>
				<?php
			}
			else if($Status=='1')
			{
				?>
					<a href="javascript:void(0);" class="btn btn-success"
						 onclick="ChangeMappingStatus('<?php echo $MappingID; ?>',0);">
						<i class="fa fa-check-square" aria-hidden="true"></i>
					</a>
				<?php
			}
		}
		else
		{
			echo 0;
		}
		exit();
	}
	public function DeleteVendorBuyerMappingRow(Request $request)
	{
		$data   	= $request->all();
		$MappingID 	= $data['MappingID'];
		$Del=$this->VBMModel->DeleteVendorBuyerMappingRow($MappingID);
		if($Del)
		{
			echo 1;
		}
		else
		{
			echo 0;
		}
		exit();
	}
	////phase 2
	public function GetTiersList(Request $request)
	{
		$data   	= $request->all();
		$VendorID 	= $data['VendorID'];
		$TiersList=$this->VBMModel->GetTiersList($VendorID);
		foreach($TiersList as $val) { ?>
		<option value="<?php echo $val->Tier; ?>"><?php echo $val->Tier; ?></option>
		<?php }
		exit();
	}
	public function AddNewMappingModal(Request $request)
	{
		$data   			= $request->all();
		$VendorID 			= $data['VendorID'];
		$CampaignID 		= $data['CampaignID'];
		$BuyerCompanyList	= $this->VBMModel->BuyerCompanyList($CampaignID);
		//$BuyerList 			= $this->VBMModel->AllBuyerListForModal($CampaignID);
		$TiersList 			= $this->VBMModel->GetAllTiersList();

		?>
		<div class="row">
			<div class="col-md-12">
				<span id="ErrMsgModal"></span>
			</div>
			<!-- <form action="<?php echo route('SaveNewMultipleMapping'); ?>" name="SaveMappingForm" id="SaveMappingForm" method="POST"> -->
				<input type="hidden" name="_token" id="_token" value="<?php echo csrf_token(); ?>">
				<input type="hidden" name="VendorID" id="VendorID" value='<?php echo $VendorID; ?>'>
				<div class="col-md-12">
					<label>Tiers</label>
	                <select class="form-control" id="tier_id" name="tier_id" onchange="GetBuyerListByBuyerCompanyForModal(),hideErr('tier_idErr');">
	                    <?php foreach($TiersList as $val) { ?>
						<option value="<?php echo $val->TierID; ?>"
							><?php echo $val->TierID; ?></option>
						<?php } ?>
	                </select>
	                <span id="tier_idErr"></span>
				</div>
				<div class="col-md-12">
					<label>Buyer Company</label>
			        <select class="form-control" id="buyer_company_id" name="buyer_company_id"
			        		onchange="GetBuyerListByBuyerCompanyForModal(),hideErr('buyer_company_idErr');">
			        	<option value="All">All Buyer Company</option>
			            <?php foreach($BuyerCompanyList as $key=>$val) {?>
			            <option value="<?php echo $key; ?>"><?php echo $val; ?> (CID:<?php echo $key; ?>)</option>
			            <?php } ?>
			        </select>
			        <input type="checkbox" id="ShowInactiveBuyerForModal" onclick="GetBuyerCompanyClauseForModal();">
			        <label for="ShowInactiveBuyerForModal">Show Inactive</label>
			        <input type="checkbox" id="ShowTestBuyerForModal" onclick="GetBuyerCompanyClauseForModal();">
			        <label for="ShowTestBuyerForModal">Show Test</label>
			        <br>
			        <span id="buyer_company_idErr"></span>
				</div>
				<div class="col-md-12">
					<label>Buyer</label>
	                <select class="form-control multipleSelect" id="buyer_id"  name="buyer_id[]" onchange="hideErr('buyer_idErr');"
	                			multiple size='10'>
	                    
	                </select>
	                <input type="checkbox" id="ShowAll" onclick="GetBuyerListByBuyerCompanyForModal();">
			        <label for="ShowAll">Show All</label>
	                <span id="buyer_idErr"></span>
				</div>
				<div class="col-md-3">
					<label>&nbsp;</label>
					<a href="javascript:void(0);" class="form-control btn btn-primary" id="SaveMappingBtn" onclick="ValidateMappingForm();">Save</a>
				</div>
			<!-- </form> -->
		</div>
		<?php
		exit();
	}
	public function GetBuyerListByBuyerCompanyForModal(Request $request)
	{
		$data   			= $request->all();
		$buyer_company_id 	= $data['buyer_company_id'];
		$ShowInactive 		= $data['ShowInactiveBuyer'];
		$ShowTest 			= $data['ShowTestBuyer'];
		$tier_id 			= $data['tier_id'];
		$ShowAll 			= $data['ShowAll'];
		$CampaignID 		= $data['CampaignID'];
		$TierPayout=$this->VBMModel->GetTierPayout($tier_id);
		$BuyerList=$this->VBMModel->BuyerListForModal($CampaignID,$buyer_company_id,$ShowInactive,$ShowTest);
		foreach($BuyerList as $val)
		{
			if($ShowAll=='0')
			{
				if($val->FixedPrice>=$TierPayout)
				{
					?>
					<option value="<?php echo $val->BuyerID; ?>">
							<?php echo $val->AdminLabel; ?> (BID:<?php echo $val->BuyerID; ?> / CID:<?php echo $val->CompanyID; ?>)
					</option>
					<?php
				}
			}
			else
			{
				?>
				<option value="<?php echo $val->BuyerID; ?>">
						<?php echo $val->AdminLabel; ?> (BID:<?php echo $val->BuyerID; ?> / CID:<?php echo $val->CompanyID; ?>)
				</option>
				<?php
			}
		}
		exit();
	}
	public function SaveNewMultipleMapping(Request $request)
	{
		$data   = $request->all();
		$VendorID 	= $data['VendorID'];
		$TierID 	= $data['TierID'];
		$BuyerIDs 	= $data['BuyerIDs'];
		$BuyerIDs	= array_values(array_filter($BuyerIDs));
		$Save=$this->VBMModel->SaveNewMultipleMapping($VendorID,$TierID,$BuyerIDs);
		if($Save)
		{
			echo 1;
		}
		else
      	{
           echo 0;
      	}
		exit();
	}
	public function GetVendorListByVendorCompanyForModal(Request $request)
	{
		$data   = $request->all();
		$vendor_company_id 	= $data['vendor_company_id'];
		$vendor_id 			= $data['vendor_id'];
		$ShowInactiveVendor = $data['ShowInactiveVendor'];
		$ShowTestVendor 	= $data['ShowTestVendor'];
		$CampaignID			= $data['campaign'];

		$VendorList=$this->VBMModel->VendorListForModal($vendor_company_id,$ShowInactiveVendor,$ShowTestVendor,$CampaignID);

		foreach($VendorList as $val)
		{
			if($vendor_id!=$val->VendorID)
			{
			?>
				<option value="<?php echo $val->VendorID; ?>">
					<?php echo $val->AdminLabel; ?> (VID:<?php echo $val->VendorID; ?> / CID:<?php echo $val->CompanyID; ?>)
				</option>
			<?php
			}
		}
		exit();
	}
	public function GetSelectedVendorCompanyForModal(Request $request)
	{
		$data   		= $request->all();
		$ShowInactive 	= $data['ShowInactiveVendor'];
		$ShowTest 		= $data['ShowTestVendor'];
		$CampaignID		= $data['campaign'];
		$GetSelectedVendorCompany=$this->VBMModel->GetSelectedVendorCompany2($ShowInactive,$ShowTest,$CampaignID);
		?>
		<option value="">Select Vendor Company</option>
		<?php
		foreach($GetSelectedVendorCompany as $key=>$val)
		{
			?>
			<option value="<?php echo $key; ?>"><?php echo $val; ?> (CID:<?php echo $key; ?>)</option>
			<?php
		}
		exit();
	}
	public function CloneMappingModal(Request $request)
	{
		$data   			= $request->all();
		$VendorCompanyID 	= $data['VendorCompanyID'];
		$VendorID 			= $data['VendorID'];
		$ShowInactiveVendor = $data['ShowInactiveVendor'];
		$ShowTestVendor 	= $data['ShowTestVendor'];
		$CampaignID			= $data['campaign'];

		$VendorCompanyList=$this->VBMModel->GetSelectedVendorCompany2($ShowInactiveVendor,$ShowTestVendor,$CampaignID);
		$VendorList=$this->VBMModel->VendorListForModal($VendorCompanyID,$ShowInactiveVendor,$ShowTestVendor,$CampaignID);

		$ShowInactiveStatus="";
		$ShowTestStatus="";
		if($ShowInactiveVendor==1)
		{
			$ShowInactiveStatus="checked";
		}
		if($ShowTestVendor==1)
		{
			$ShowTestStatus="checked";
		}
		?>
		<div class="row">
			<div class="col-md-12">
				<span id="ErrMsgConeModal"></span>
			</div>
			<input type="hidden" id="SourceVendorID" name="SourceVendorID" value='<?php echo $VendorID; ?>'>
			<div class="col-md-12">
				<label>Vendor Company</label>
                <select class="form-control" id="vendor_company_id_for_modal" onchange="GetVendorListByVendorCompanyForModal();">
                	<option value="">Select Vendor Company</option>
                    <?php foreach($VendorCompanyList as $key=>$val) { ?>
                    <option value="<?php echo $key; ?>"
                    <?php if($VendorCompanyID==$key){ echo "selected"; }?>>
                    <?php echo $val; ?> (CID:<?php echo $key; ?>)
                	</option>
                    <?php } ?>
                </select>
                <input type="checkbox" id="ShowInactiveVendorForModal" onclick="GetVendorCompanyClauseForModal();"
                	<?php echo $ShowInactiveStatus; ?> >
                <label for="ShowInactiveVendorForModal">Show Inactive</label>
                <input type="checkbox" id="ShowTestVendorForModal" onclick="GetVendorCompanyClauseForModal();"
                	<?php echo $ShowTestStatus; ?> >
                <label for="ShowTestVendorForModal">Show Test</label><br>
                <span id="vendor_company_id_for_modalErr"></span>
			</div>
			<div class="col-md-12">
				<label>Vendor</label>
		        <select class="form-control multipleSelectForModal" id="vendor_id_for_modal" multiple size='10' onchange="hideErr('vendor_id_for_modalErr');">
			        <?php
				        foreach($VendorList as $val)
						{
							if($VendorID!=$val->VendorID)
							{
								?>
								<option value="<?php echo $val->VendorID; ?>">
									<?php echo $val->AdminLabel; ?> (VID:<?php echo $val->VendorID; ?> / CID:<?php echo $val->CompanyID; ?>)
								</option>
								<?php
							}
						}
			        ?>
		        </select><br>
		        <span id="vendor_id_for_modalErr"></span>
			</div>

			<div class="col-md-12">
				<div class="form-group">
                    <a href="javascript:void(0);" class="btn btn-primary" id="CloneMappingAddBtn" onclick="ValidateCloneMappingForm();">Save</a>
                </div>
			</div>
		</div>
		<?php
		exit();
	}
	public function SaveMultipleCloneMapping(Request $request)
	{
		$data   = $request->all();
		$TargerVendorIDs 	= $data['TargerVendorIDs'];
		$SourceVendorID 	= $data['SourceVendorID'];
		$TargerVendorIDs	= array_values(array_filter($TargerVendorIDs));

		$Save=$this->VBMModel->SaveMultipleCloneMapping($TargerVendorIDs,$SourceVendorID);
		if($Save)
		{
			echo 1;
		}
		else
      	{
           echo 0;
      	}
		exit();
	}
	public function DeleteMultipleMappings(Request $request)
	{
		$data   = $request->all();
		$MappingIDs 	= $data['MappingIDs'];
		$MappingIDs		= array_values(array_filter($MappingIDs));
		$Del=$this->VBMModel->DeleteMultipleMappings($MappingIDs);
		if($Del)
		{
			echo 1;
		}
		else
      	{
           echo 0;
      	}
		exit();
	}
	public function GetAllBuyerList(Request $request)
	{
		$data = $request->all();
		$ShowInactive = $data['ShowInactiveBuyer'];
		$ShowTest = $data['ShowTestBuyer'];
		$CampaignID = $data['CampaignID'];

		$BuyerList = $this->VBMModel->GetAllBuyerList($CampaignID,$ShowInactive,$ShowTest);
		?>
		  <option value="">Select Buyer Company</option>
		<?php
		foreach ($BuyerList as $key => $value) { 
			?>
				<option value="<?php echo $value->CompanyID; ?>">
					<?php echo $value->CompanyName; ?> (CID:<?php echo $value->CompanyID;?>) 
				</option>
			<?php
		}
	}
	public function GetMappedVendor(Request $request) {
		$data = $request->all();
		$CampaignID = $data['CampaignID'];
		$VendorCompanyID="";
		if($data['VendorCompany']!="") {
			$VendorCompanyID = $data['VendorCompany'];
		}
		$BuyerID="";
		if($data['Buyer']!="") {
			$BuyerID = $data['Buyer'];
		}
		$VendorList = $this->VBMModel->GetMappedVendor($CampaignID,$VendorCompanyID,$BuyerID);
		?>
		<option value="">Select Vendor</option>
		<?php
		foreach ($VendorList as $key => $value) { 
			?>
				<option value="<?php echo $value->VendorID; ?>"><?php echo $value->AdminLabel; ?> (VID:<?php echo $value->VendorID; ?> / CID:<?php echo $value->CompanyID; ?>)</option>
			<?php
		}
	}*/
}