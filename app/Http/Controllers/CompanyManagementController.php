<?php
namespace App\Http\Controllers;
use Route;
use Mail;
use Auth, Hash;
use Validator;
use DB;
use Session;
use Excel;
use PDF;
use Response;
use Redirect;
use Illuminate\Http\Request;
use App\Http\Controllers\Pagination;
use App\Http\Models\CompanyManagementModel;
use App\Http\Models\CommonModel;
use App\Http\Models\UserManagementModel;
use App\Http\Models\VendorManagementModel;
use App\Http\Models\BuyerManagementModel;
use App\Http\Models\MasterConfigModel;

class CompanyManagementController extends Controller
{
	public function __construct(Request $request)
	{		
		$this->Pagination 		= new Pagination();
		$this->MasterConfigModel= new MasterConfigModel();
		$this->CMModel 			= new CompanyManagementModel();
		$this->CModel 			= new CommonModel();
		$this->UMModel 			= new UserManagementModel();
		$this->VMModel 			= new VendorManagementModel();
		$this->BMModel 			= new BuyerManagementModel();
	}
	public function CompanyManagementView(Request $request)
	{
		if(!$request->session()->has('user_id'))
		{
		    return redirect()->action('LoginController@Login');
		}
		$StartDate  = date("Y-m-01");
        $EndDate    = date("Y-m-t");
		$Data['StartDate']   	= $StartDate;
		$Data['EndDate']   		= $EndDate;
		$Data['Menu']   		= "SuperAdmin";
        $Data['Title']  		= "Admin | CompanyManagement";
		return view('CompanyManagementView')->with($Data);
	}   
	public function GetCompanyList(Request $request)
	{
		$Data = $request->all();

        $UserRole 	= Session::get('user_role');
		$CompanyID 	= Session::get('user_companyID');
		$UserID 	= Session::get('user_id');

        $NumOfRecords = $Data['numofrecords'];
        $page = $Data['page'];
        $cur_page = $page;

        $Limitpage = $page - 1;
        $Start = $Limitpage * $NumOfRecords;

        $StartDate 			= $Data['StartDate'];
        $EndDate 			= $Data['EndDate'];
        $CompanyName 		= $Data['CompanyName'];
        $CompanyTel 		= $Data['CompanyTel'];
        $CompanyTestMode	= $Data['CompanyTestMode'];
        $CompanyStatus		= $Data['CompanyStatus'];
        $MappedUser			= $Data['MappedUser'];
        $MappedBuyer		= $Data['MappedBuyer'];
        $Search 			= array('CompanyName' 	=> $CompanyName,
					            'CompanyTel' 		=> $CompanyTel,
					            'CompanyStatus' 	=> $CompanyStatus,
					            'CompanyTestMode' 	=> $CompanyTestMode,
					            'MappedUser' 		=> $MappedUser,
					            'MappedBuyer' 		=> $MappedBuyer,
					            'StartDate' 		=> $StartDate,
					            'EndDate' 			=> $EndDate,
					            'Start' 			=> $Start,
					            'NumOfRecords' 		=> $NumOfRecords
					            );
        $GetCompanyList 	= $this->CMModel->GetCompanyList($Search);       
        $AllCompanyList 	= $GetCompanyList['Res'];
        $CompanyCount 		= $GetCompanyList['Count'];        

        $Pagination = $this->Pagination->Links($NumOfRecords, $CompanyCount, $page);
        
      ?>
		<div class="row mt-3">
          	<div class="table-responsive">
		  		<table class="table table-bordered tableFont txtL nTabl">
                    <thead class="thead-dark">
						<tr>
							<th scope="col" style="padding: 0.684rem; font-size: 0.8rem;">Company Name</th>
							<th scope="col" style="padding: 0.684rem; font-size: 0.8rem;">Sign-up Date</th>
							<th scope="col" style="padding: 0.684rem; font-size: 0.8rem;">Company Telephone</th>
							<th scope="col" style="padding: 0.684rem; font-size: 0.8rem;">Status</th>
							<th scope="col" style="padding: 0.684rem; font-size: 0.8rem;">Company Mode</th>
							<!-- <th scope="col" style="padding: 0.684rem; font-size: 0.8rem;">Total User Mapped</th>
							<th scope="col" style="padding: 0.684rem; font-size: 0.8rem;">Total Buyer Mapped</th> -->
							<th scope="col" style="padding: 0.684rem; font-size: 0.8rem;">Action</th>
						</tr>
                    </thead>
			    	<tbody>
				    <?php 
				     	if(!empty($AllCompanyList ))
				     	{
				     		$i = $Start + 1;
				     		foreach($AllCompanyList as $cl)
				     		{
							     ?>				       
						        <tr>
						           	<td><?php echo $cl->CompanyName." (CID:".$cl->CompanyID.")"; ?></td>
						           	<td><?php echo $cl->CreatedDateTime; ?></td>
						           	<td><?php echo $cl->CompanyTel; ?></td>
						           	<td id="StatusHtml_<?php echo $cl->CompanyID; ?>">
						           		<?php 
						           		if ($cl->CompanyStatus == '1') 
						           		{
		                                   ?>
		                                   	<div class="custom-control custom-checkbox noCush greenCh">
				                              <input type="checkbox" checked="true" 
				                              	class="custom-control-input noCush-input green-input" 
				                              	id="customCheck<?php echo $cl->CompanyID; ?>" 
				                              	onclick="ChangeCompanyStatus('<?php echo $cl->CompanyID; ?>',0);">
				                              <label class="custom-control-label noCush-label green-label mb-3" 
				                              	for="customCheck<?php echo $cl->CompanyID; ?>"  
				                              	></label>
				                            </div> 
		                                   <?php
		                                } 
		                                else if ($cl->CompanyStatus == '0') 
		                                {
		                                    ?>
		                                   	<div class="custom-control custom-checkbox noCush">
				                              <input type="checkbox" 
				                              class="custom-control-input noCush-input" 
				                              id="customCheck<?php echo $cl->CompanyID; ?>" 
				                              onclick="ChangeCompanyStatus('<?php echo $cl->CompanyID; ?>',1);" >
				                              <label class="custom-control-label noCush-label mb-3" 
				                              	for="customCheck<?php echo $cl->CompanyID; ?>" 
				                              	></label>
				                            </div> 
		                                   <?php
		                                }
						           		?>
						           	</td>
						           	<td id="ModeHtml_<?php echo $cl->CompanyID ?>">
						           		<?php 
						           		if ($cl->CompanyTestMode == '1') 
						           		{
		                                   ?>
		                                   	<a href="javascript:void(0);" 
		                                   		class="btn btn-danger cfonnT cbtnPadd noBoradi"
		                                   		onclick="ChangeCompanyTestMode('<?php echo $cl->CompanyID; ?>',0);">
												<i class="fas fa-square sqFont" aria-hidden="true"></i> 
												TestMode
											</a>  
		                                   <?php

		                                } 
		                                else if ($cl->CompanyTestMode == '0') 
		                                {
		                                    ?>
		                                   	<a href="javascript:void(0);" 
		                                   		class="btn btn-success cfonnT cbtnPadd noBoradi"
		                                   		onclick="ChangeCompanyTestMode('<?php echo $cl->CompanyID; ?>',1);">
												<i class="fas fa-check-square sqFont" aria-hidden="true"></i> 
												LiveMode
											</a>  
		                                   <?php
		                                }
						           		?>
						           	</td>							           	
						           	<!-- <td style="text-align: center;" >0</td>
						           	<td style="text-align: center;" >0</td> -->
						           	<td  style="text-align: center;">
						           		<ul class="btnList">						              	
											<li>
											<a href="<?php echo route('admin.CompanyEditView',array(base64_encode($cl->CompanyID))) ?>">
												<button type="button" title="edit" class="btn btn-info cfonnT cbtnPadd noBoradi">
													<i class="fas fa-pencil-alt fnnPadd"></i>Company
												</button>
											</a>
											</li>
										</ul>
						           	</td>
						        </tr>
			     				<?php 
			     				$i++;
			     			}
			     		} 
			     		else 
			     		{
			     			?>
					     	<tr >
					           <td colspan="7" style="color: #c94542;text-align:center;">Record Not Found.</td>
					           </td>
					        </tr>
			     			<?php 
			     		} 
			     	?>
			     	</tbody>
				  </table>
				</div>
			</div>
		<?php if(!empty($AllCompanyList ))
		{ ?>
			<div class="row mt-3">			
				<div class="col-md-6 col-sm-6 col-12">
					<div class="dataTables_length mCenter" id="dataTable_length">
						<label>Records/Pages
							<select id="numofrecords" onchange="SearchUserList();" name="dataTable_length" aria-controls="dataTable" class="custom-select custom-select-sm form-control form-control-sm">
							<option value="10" <?php if($NumOfRecords=='10'){ echo 'selected'; } ?> >10</option>
							<option value="20" <?php if($NumOfRecords=='20'){ echo 'selected'; } ?> >20</option>
							<option value="50" <?php if($NumOfRecords=='50'){ echo 'selected'; } ?> >50</option>
							<option value="100" <?php if($NumOfRecords=='100'){ echo 'selected'; } ?> >100</option>
							<option value="150" <?php if($NumOfRecords=='150'){ echo 'selected'; } ?> >150</option>
							<option value="200" <?php if($NumOfRecords=='200'){ echo 'selected'; } ?> >200</option>
							</select>
						</label>
					</div>
				</div>
				<div class="col-md-6 col-sm-6 col-12 grid-margin stretch-card">
					<div class="card noBg border-0">
					    <nav>
					      <?php echo $Pagination; ?>
					    </nav>
					</div>
				</div>
			</div>
      	<?php
      	}
      	else
      	{
      		?>
      		<input type="hidden" name="numofrecords" id="numofrecords" value='10'>
      		<?php
      	}
       	exit();
	}
	public function CompanyAddView(Request $request)
	{
		if(!$request->session()->has('user_id'))
		{
		    return redirect()->action('LoginController@Login');
		}
		$UserRole 	= Session::get('user_role');
		$CompanyID 	= Session::get('user_companyID');
		$UserID 	= Session::get('user_id');
		$Data['StatesList']   		= $this->CMModel->StatesList();
		$Data['CountryList']  	   	= $this->CMModel->CountryList();
		$Data['CampaignDropDown'] 	= $this->MasterConfigModel->getCampaignList();
		$Data['Menu']   = "SuperAdmin";
        $Data['Title']  = "Admin | CompanyManagement";
        
		return view('CompanyAddView')->with($Data);
	}
	public function AddCompanyDetails(Request $request)
	{
		$Data 		= $request->all();
		
		$CompanyDetails['CompanyName'] = $Data['CompanyName'];
		$CompanyTel='';
		if(isset($Data['CompanyTel']) && $Data['CompanyTel']!='')
		{
			$CompanyDetails['CompanyTel'] = $Data['CompanyTel'];
		}
		$CompanyWebSite='';
		if(isset($Data['CompanyWebSite']) && $Data['CompanyWebSite']!='')
		{
			$CompanyDetails['CompanyWebSite'] = $Data['CompanyWebSite'];
		}
		$CompanyAddress1='';
		if(isset($Data['CompanyAddress1']) && $Data['CompanyAddress1']!='')
		{
			$CompanyDetails['CompanyAddress1'] = $Data['CompanyAddress1'];
		}
		$CompanyAddress2='';
		if(isset($Data['CompanyAddress2']) && $Data['CompanyAddress2']!='')
		{
			$CompanyDetails['CompanyAddress2'] = $Data['CompanyAddress2'];
		}
		$CompanyAddress3='';
		if(isset($Data['CompanyAddress3']) && $Data['CompanyAddress3']!='')
		{
			$CompanyDetails['CompanyAddress3'] = $Data['CompanyAddress3'];
		}		
		$CompanyCurrency='';
		if(isset($Data['CompanyCurrency']) && $Data['CompanyCurrency']!='')
		{
			$CompanyDetails['CompanyCurrency'] = $Data['CompanyCurrency'];
		}
		$CompanyNotes='';
		if(isset($Data['CompanyNotes']) && $Data['CompanyNotes']!='')
		{
			$CompanyDetails['CompanyNotes'] = $Data['CompanyNotes'];
		}
		
		$CompanyCity="";
		if(isset($Data['CompanyCity']) && $Data['CompanyCity']!="")
		{
			$CompanyDetails['CompanyCity'] = $Data['CompanyCity'];
		}
		$CompanyStateCountry="";
		if(isset($Data['CompanyStateCounty']) && $Data['CompanyStateCounty']!="")
		{
			$CompanyDetails['CompanyStateCounty'] = $Data['CompanyStateCounty'];
		}		
		$CompanyDetails['CompanyCountry'] = $Data['CompanyCountry'];
		$CompanyStatus = '0';
		if(isset($Data['CompanyStatus']) && $Data['CompanyStatus']=="on" )
		{
			$CompanyDetails['CompanyStatus'] = '1';
		}
		$CompanyTestMode = '1';
		if(isset($Data['CompanyTestMode']) && $Data['CompanyTestMode']=="on" )
		{
			$CompanyDetails['CompanyTestMode'] = '0';
		}
		$CompanyZipPostCode="";
		if(isset($Data['CompanyZipPostCode']) && $Data['CompanyZipPostCode']!="")
		{
			$CompanyDetails['CompanyZipPostCode'] = $Data['CompanyZipPostCode'];
		}
		
        $UploadedFiles = "";
		$UploadedFilesArray=array();
		if(isset($Data['Companyfiles']) && $Data['Companyfiles'][0]!="")
        {
        	$FilesArray  = $Data['Companyfiles'];
        	$j=1;
	        foreach($FilesArray as $file)
	        {         
	            $filepath 	= 'uploads';
	            $filename 	= $file->getClientOriginalName();
				$file->move($filepath, $filename);
	            $SampleFiles=array('id'=>$j,'filename'=>$filename,'filepath'=>'/uploads/'.$filename);
	            $j++;
	            array_push($UploadedFilesArray,$SampleFiles); 	            
	        }
        } 
        if(!empty($UploadedFilesArray))
        {
        	$UploadedFiles =  json_encode($UploadedFilesArray);        	
        }
        $CompanyDetails['UploadedFiles'] = $UploadedFiles;
        $CompanyDetails['CreatedDateTime'] = date('Y-m-d H:i:s',time());
        ////////////////////User Details///////////////////////////////
        $UserDetails['UserRole']   		= $Data['UserRole'];
		$UserDetails['UserFirstName']   = $Data['UserFirstName'];
		$UserDetails['UserLastName'] = '';
		if(isset($Data['UserLastName']) && $Data['UserLastName']!='')
		{
			$UserDetails['UserLastName']   	= $Data['UserLastName'];			
		}
		$UserDetails['UserEmail']   	= $Data['UserEmail'];
		$UserDetails['UserTel'] = '';
		if(isset($Data['UserTel']) && $Data['UserTel']!='')
		{
			$UserDetails['UserTel']   	= $Data['UserTel'];			
		}
		$UserDetails['UserSkype'] = '';
		if(isset($Data['UserSkype']) && $Data['UserSkype']!='')
		{
			$UserDetails['UserSkype']   	= $Data['UserSkype'];			
		}
		$UserDetails['UserSkype'] = '';
		if(isset($Data['UserNotes']) && $Data['UserNotes']!='')
		{
			$UserDetails['UserNotes']   	= $Data['UserNotes'];			
		}		
		$UserDetails['UserStatus']   	= '0';			
		if(isset($Data['UserStatus']) && $Data['UserStatus']=='on')
		{
			$UserDetails['UserStatus']   	= '1';			
		}

		$UserDetails['ShowAPISpecs']   	= '0';			
		if(isset($Data['ShowAPISpecs']) && $Data['ShowAPISpecs']=='on')
		{
			$UserDetails['ShowAPISpecs']   	= '1';			
		}

		$UserDetails['ShowAffLinks']   	= '0';			
		if(isset($Data['ShowAffLinks']) && $Data['ShowAffLinks']=='on')
		{
			$UserDetails['ShowAffLinks']   	= '1';			
		}

		if(isset($Data['Password']) && $Data['Password']=='on')
		{
			$UserDetails['UserPassword']   	= md5($Data['Password']);
		}
		/*if(isset($Data['SendCredentialsMail']) && $Data['SendCredentialsMail']=='on')
		{
			$this->SendCredentialsMail($Data);
		}*/
		//////////////////Vendor Details////////////////
		$VendorDetails=array();
		if(isset($Data['AddVendor']) && $Data['AddVendor']=='on')
		{
			$CampaignID     				= $Data['CampaignVendor'];
			$VendorDetails['CampaignID']    = $CampaignID;
			$VendorDetails['AdminLabel']    = $Data['AdminLabelVendor'];
			$VendorDetails['PartnerLabel']  = $Data['PartnerLabelVendor'];
			$CampaignDetails = $this->CModel->GetCampaignDetails($CampaignID);
			if($CampaignDetails->CampaignType=='DirectPost')
			{
				$VendorDetails['MaxTimeOut']   = $Data['MaxTimeOutVendor'];
			}
			else if($CampaignDetails->CampaignType=='PingPost')
			{
				$VendorDetails['MaxPingTimeOut']   = $Data['MaxPingTimeOutVendor'];
				$VendorDetails['MaxPostTimeOut']   = $Data['MaxPostTimeOutVendor'];
			}
			$VendorDetails['PayoutCalculation']   = $Data['PayoutCalculationVendor'];
			if($Data['PayoutCalculationVendor']=='Revshare' || $Data['PayoutCalculationVendor']=='MinPrice')
			{
				$VendorDetails['RevsharePercent']   = $Data['RevshareVendor'];
			}
			else if($Data['PayoutCalculationVendor']=='TierPrice')
			{
				$VendorDetails['RevsharePercent']   = $Data['TierPriceVendor'];
			}
			else if($Data['PayoutCalculationVendor']=='FixedPrice')
			{
				$VendorDetails['FixedPrice']   = $Data['FixedPriceVendor'];	
			}
			else if($Data['PayoutCalculationVendor']=='MinPrice')
			{
				$VendorDetails['RevsharePercent']   = $Data['RevshareVendor'];	
			}
			else if($Data['PayoutCalculationVendor']=='BucketPrice' || $Data['PayoutCalculationVendor'] = "BucketSystemBySubID")
			{
				$VendorDetails['RevsharePercent'] = $Data['RevshareVendor'];	
				$VendorDetails['FixedPrice']   	= $Data['FixedPriceVendor'];	
			}

			$VendorDetails['DailyCap'] 	= $Data['DailyCapVendor'];
			$VendorDetails['TotalCap'] 	= $Data['TotalCapVendor'];
			$VendorDetails['TrackingPixelType'] 	= $Data['TrackingPixelType'];
			$VendorDetails['TrackingPixelStatus'] = 0;
			if(isset($Data['TrackingPixelStatus']) && $Data['TrackingPixelStatus']=='on')
			{
				$VendorDetails['TrackingPixelStatus'] = 1;
			}
			$VendorDetails['VendorStatus'] = 0;
			if(isset($Data['VendorStatus']) && $Data['VendorStatus']=='on')
			{
				$VendorDetails['VendorStatus'] = 1;
			}
			$VendorDetails['VendorTestMode'] = 0;
			if(isset($Data['VendorTestMode']) && $Data['VendorTestMode']=='on')
			{
				$VendorDetails['VendorTestMode'] = 1;
			}				
			$VendorDetails['TrackingPixelCode']   = $Data['TrackingPixelCode'];
			$VendorDetails['VendorNotes']   		= html_entity_decode($Data['VendorNotes']);
		}
		///////////////////////////Buyer Details
		$BuyerDetails=array();
		if(isset($Data['AddBuyer']) && $Data['AddBuyer']=='on')
		{
			$CampaignID     				= $Data['CampaignBuyer'];
			$BuyerDetails['CampaignID']     = $CampaignID;
			$BuyerDetails['AdminLabel']     = $Data['AdminLabelBuyer'];
			$BuyerDetails['PartnerLabel']   	= $Data['PartnerLabelBuyer'];
			$BuyerDetails['IntegrationFileName'] = $Data['IntegrationFileName'];
			$CampaignDetails = $this->CModel->GetCampaignDetails($CampaignID);
			$BuyerDetails['PingTestURL']   = '';
			$BuyerDetails['PingLiveURL']   = '';
			$BuyerDetails['PostTestURL']   = '';
			$BuyerDetails['PostLiveURL']   = '';
			if($CampaignDetails->CampaignType=='DirectPost')
			{
				$BuyerDetails['BuyerTierID']  = $Data['BuyerTierID'];
				$BuyerDetails['MaxTimeOut']   = $Data['MaxTimeOutBuyer'];
				$BuyerDetails['MinTimeOut']   = $Data['MinTimeOutBuyer'];
				$BuyerDetails['PostTestURL']  = '';
				$BuyerDetails['PostLiveURL']  = '';
				if(isset($Data['PostTestURL']) && $Data['PostTestURL']!='')
				{					
					$BuyerDetails['PostTestURL']   = $Data['PostTestURL'];
				}
				if(isset($Data['PostLiveURL']) && $Data['PostLiveURL']!='')
				{					
					$BuyerDetails['PostLiveURL']   = $Data['PostLiveURL'];
				}
			}
			else if($CampaignDetails->CampaignType=='PingPost')
			{
				$BuyerDetails['BuyerTierID']  = '';
				$BuyerDetails['MaxPingTimeOut']   = $Data['MaxPingTimeOutBuyer'];
				$BuyerDetails['MaxPostTimeOut']   = $Data['MaxPostTimeOutBuyer'];
				$BuyerDetails['MaxPingsPerMinute']= $Data['MaxPingsPerMinuteBuyer'];
				
				$BuyerDetails['PingTestURL']  = '';
				$BuyerDetails['PingLiveURL']  = '';
				$BuyerDetails['PostLiveURL']  = '';
				$BuyerDetails['PostLiveURL']  = '';
				if(isset($Data['PingTestURL']) && $Data['PingTestURL']!='')
				{					
					$BuyerDetails['PingTestURL']   = $Data['PingTestURL'];
				}
				if(isset($Data['PingLiveURL']) && $Data['PingLiveURL']!='')
				{					
					$BuyerDetails['PingLiveURL']   = $Data['PingLiveURL'];
				}
				if(isset($Data['PostTestURL']) && $Data['PostTestURL']!='')
				{					
					$BuyerDetails['PostTestURL']   = $Data['PostTestURL'];
				}
				if(isset($Data['PostLiveURL']) && $Data['PostLiveURL']!='')
				{					
					$BuyerDetails['PostLiveURL']   = $Data['PostLiveURL'];
				}
			}
			$BuyerDetails['PayoutCalculation']   = $Data['PayoutCalculationBuyer'];
			if($Data['PayoutCalculationBuyer']=='Revshare')
			{
				$BuyerDetails['FixedPrice']   = $Data['Revshare'];
			}
			else if($Data['PayoutCalculationBuyer']=='FixedPrice')
			{
				$BuyerDetails['FixedPrice']   = $Data['FixedPrice'];	
			}
			$BuyerDetails['DailyCap'] 	= $Data['DailyCapBuyer'];
			$BuyerDetails['TotalCap'] 	= $Data['TotalCapBuyer'];
			
			$BuyerDetails['BuyerStatus'] = 0;
			if(isset($Data['BuyerStatus']) && $Data['BuyerStatus']=='on')
			{
				$BuyerDetails['BuyerStatus'] = 1;
			}
			$BuyerDetails['BuyerTestMode'] = 0;
			if(isset($Data['BuyerTestMode']) && $Data['BuyerTestMode']=='on')
			{
				$BuyerDetails['BuyerTestMode'] = 1;
			}	
			$BuyerDetails['RequestMethod'] 	= $Data['RequestMethod'];
			$BuyerDetails['RequestHeader'] 	= $Data['RequestHeader'];
			$BuyerDetails['Parameter1'] 	= '';
			if(isset($Data['Parameter1']) && $Data['Parameter1']!='')
			{					
				$BuyerDetails['Parameter1']   = $Data['Parameter1'];
			}
			$BuyerDetails['Parameter2'] 	= '';
			if(isset($Data['Parameter2']) && $Data['Parameter2']!='')
			{					
				$BuyerDetails['Parameter2']   = $Data['Parameter2'];
			}
			$BuyerDetails['Parameter3'] 	= '';
			if(isset($Data['Parameter3']) && $Data['Parameter3']!='')
			{					
				$BuyerDetails['Parameter3']   = $Data['Parameter3'];
			}
			$BuyerDetails['Parameter4'] 	= '';
			if(isset($Data['Parameter4']) && $Data['Parameter4']!='')
			{					
				$BuyerDetails['Parameter4']   = $Data['Parameter4'];
			}
			$BuyerDetails['BuyerNotes']   	= html_entity_decode($Data['BuyerNotes']);
			$UploadedFiles = "";		
			$UploadedFilesArray=array();
			if(!empty($Data['Buyerfiles']) && $Data['Buyerfiles'][0]!="")
	        {
	        	$FilesArray  = $Data['Buyerfiles'];
	        	$j = 0;
		        foreach($FilesArray as $file)
		        {         
		            $filepath 	= 'uploads';
		            $filename 	= $file->getClientOriginalName();
					$file->move($filepath, $filename);
		            $SampleFiles=array('id'=>$j,'filename'=>$filename,'filepath'=>'/uploads/'.$filename);
		            $j++;
		            array_push($UploadedFilesArray,$SampleFiles); 	            
		        }
	        }  
	        if(!empty($UploadedFilesArray))
	        {
	        	$UploadedFiles = json_encode($UploadedFilesArray);
	        }
	        $BuyerDetails['UploadedFiles'] 	= $UploadedFiles;
		}
		$Details['CompanyDetails']=$CompanyDetails;
		$Details['UserDetails']=$UserDetails;
		$Details['VendorDetails']=$VendorDetails;
		$Details['BuyerDetails']=$BuyerDetails;
		$AddCompanyDetails = $this->CMModel->AddCompanyDetails($Details);
		if($AddCompanyDetails)
		{
			Session::flash('message', 'Company Details Has been Updated.');
            Session::flash('alert-class', 'alert-success');
            return Redirect::route('admin.CompanyManagementView');
		}
		else
		{
			Session::flash('message', 'OOPS! Something Wrong Please Try Again.');
            Session::flash('alert-class', 'alert-danger');
            return Redirect::route('admin.CompanyManagementView');
		}
	}

	public function CompanyEditView(Request $request, $CompanyID)
	{
		if(!$request->session()->has('user_id'))
		{
		    return redirect()->action('LoginController@Login');
		}
		$CompanyID  			= base64_decode($CompanyID);
		/// Company
		$Data['CompanyDetails'] = $this->CMModel->GetCompanyDetail($CompanyID);		
		$Data['StatesList']   	= $this->CMModel->StatesList();
		$Data['CountryList']  	= $this->CMModel->CountryList();

		/// User
		$Data['UserRole'] 		= $this->UMModel->GetUserRoles();
		$Data['CompanyList'] 	= $this->UMModel->CompanyList();
		// Vendor
		$Data['CampaignDropDown'] = $this->MasterConfigModel->getCampaignList();

		$Data['Menu']   		= "SuperAdmin";
        $Data['Title']  		= "Admin | CompanyManagement";
        /*echo "<pre>";
        print_r($Data);
        exit();*/
		return view('CompanyEditView')->with($Data);
	}
	public function SaveCompanyDetails(Request $request)
	{
		$Data 							= $request->all();		
		$CompanyID 						= $Data['CompanyID'];
		$CompanyDetails['CompanyName']  = $Data['CompanyName'];
		$CompanyTel='';
		if(isset($Data['CompanyTel']) && $Data['CompanyTel']!='')
		{
			$CompanyDetails['CompanyTel'] = $Data['CompanyTel'];
		}
		$CompanyWebSite='';
		if(isset($Data['CompanyWebSite']) && $Data['CompanyWebSite']!='')
		{
			$CompanyDetails['CompanyWebSite'] = $Data['CompanyWebSite'];
		}
		$CompanyAddress1='';
		if(isset($Data['CompanyAddress1']) && $Data['CompanyAddress1']!='')
		{
			$CompanyDetails['CompanyAddress1'] = $Data['CompanyAddress1'];
		}
		$CompanyAddress2='';
		if(isset($Data['CompanyAddress2']) && $Data['CompanyAddress2']!='')
		{
			$CompanyDetails['CompanyAddress2'] = $Data['CompanyAddress2'];
		}
		$CompanyAddress3='';
		if(isset($Data['CompanyAddress3']) && $Data['CompanyAddress3']!='')
		{
			$CompanyDetails['CompanyAddress3'] = $Data['CompanyAddress3'];
		}		
		$CompanyCurrency='';
		if(isset($Data['CompanyCurrency']) && $Data['CompanyCurrency']!='')
		{
			$CompanyDetails['CompanyCurrency'] = $Data['CompanyCurrency'];
		}
		$CompanyNotes='';
		if(isset($Data['CompanyNotes']) && $Data['CompanyNotes']!='')
		{
			$CompanyDetails['CompanyNotes'] = $Data['CompanyNotes'];
		}
		
		$CompanyCity="";
		if(isset($Data['CompanyCity']) && $Data['CompanyCity']!="")
		{
			$CompanyDetails['CompanyCity'] = $Data['CompanyCity'];
		}
		$CompanyStateCountry="";
		if(isset($Data['CompanyStateCounty']) && $Data['CompanyStateCounty']!="")
		{
			$CompanyDetails['CompanyStateCounty'] = $Data['CompanyStateCounty'];
		}		
		$CompanyDetails['CompanyCountry'] = $Data['CompanyCountry'];
		$CompanyStatus = '0';
		if(isset($Data['CompanyStatus']) && $Data['CompanyStatus']=="on" )
		{
			$CompanyDetails['CompanyStatus'] = '1';
		}
		$CompanyTestMode = '1';
		if(isset($Data['CompanyTestMode']) && $Data['CompanyTestMode']=="on" )
		{
			$CompanyDetails['CompanyTestMode'] = '0';
		}
		$CompanyZipPostCode="";
		if(isset($Data['CompanyZipPostCode']) && $Data['CompanyZipPostCode']!="")
		{
			$CompanyDetails['CompanyZipPostCode'] = $Data['CompanyZipPostCode'];
		}
					
		
		$UploadedFiles = "";		
		$UploadedFilesArray=array();		
		$OldUploadedFilesArray=array();		
		if(isset($Data['OldUploadedFiles']))
		{
			$i=0;
			foreach($Data['OldUploadedFiles'] as $val)
			{         
				$SampleFiles=array('id'=>$i,'filename'=>$val,'filepath'=>'/uploads/'.$val);
				$i++;
				array_push($OldUploadedFilesArray,$SampleFiles); 	            
			}
		}
       	
       	if(isset($Data['Companyfiles']) && $Data['Companyfiles'][0]!="")
        {
        	$FilesArray  = $Data['Companyfiles'];
        	$j=count($OldUploadedFilesArray);
	        foreach($FilesArray as $file)
	        {         
	            $filepath 	= 'uploads';
	            $filename 	= $file->getClientOriginalName();
				$file->move($filepath, $filename);
	            $SampleFiles=array('id'=>$j,'filename'=>$filename,'filepath'=>'/uploads/'.$filename);
	            $j++;
	            array_push($UploadedFilesArray,$SampleFiles); 	            
	        }
        } 
        if(!empty($UploadedFilesArray))
        {
        	$UploadedFiles =  json_encode($UploadedFilesArray);        	
        }
        
        if(!empty($OldUploadedFilesArray) && !empty($UploadedFilesArray))
        {
        	$UploadedFiles = array_merge($OldUploadedFilesArray,$UploadedFilesArray);
        	$UploadedFiles = json_encode($UploadedFiles);
        }
        else if(empty($OldUploadedFilesArray) && !empty($UploadedFilesArray))
        {
        	$UploadedFiles = json_encode($UploadedFilesArray);
        }
        else if(!empty($OldUploadedFilesArray) && empty($UploadedFilesArray))
        {
        	$UploadedFiles = json_encode($OldUploadedFilesArray);
        }

        $CompanyDetails['UploadedFiles'] = $UploadedFiles;
		
		$SaveCompanyDetails = $this->CMModel->SaveCompanyDetails($CompanyID,$CompanyDetails);
		if($SaveCompanyDetails)
		{
			Session::flash('message', 'Company Details Has been Updated.');
            Session::flash('alert-class', 'alert-success');
            return Redirect::route('admin.CompanyEditView', base64_encode($CompanyID));
		}
		else
		{
			Session::flash('message', 'OOPS! Something Wrong Please Try Again.');
            Session::flash('alert-class', 'alert-danger');
            return Redirect::route('admin.CompanyEditView', base64_encode($CompanyID));
		}
	}
	public function AddUserDetailsFromCompany(Request $request)
	{
		$Data 					= $request->all();
		$CompanyID 				= $Data['CompanyID'];
		$Details['UserRole']   	= $CompanyID;
		$Details['CompanyID']   = $Data['CompanyID'];
		if(isset($Data['UserStatus']) && $Data['UserStatus']=='on')
		{
			$Details['UserStatus']   	= '1';			
		}
		$Details['UserFirstName']   = $Data['UserFirstName'];
		$Details['UserLastName']	= '';
		if($Data['UserLastName']!='')
		{
			$Details['UserLastName']   	= $Data['UserLastName'];			
		}
		$Details['UserEmail']   	= $Data['UserEmail'];
		$Details['UserPassword']   	= md5($Data['Password']);
		$Details['UserTel']   		= $Data['UserTel'];
		$Details['UserSkype']	= '';
		if($Data['UserSkype']!='')
		{
			$Details['UserSkype']   	= $Data['UserSkype'];			
		}
		
		$SaveUserDetails = $this->UMModel->AddUserDetails($Details);
		if($SaveUserDetails)
		{
			Session::flash('message', 'New User Details Has been Added.');
            Session::flash('alert-class', 'alert-success');
            return Redirect::route('admin.CompanyEditView', base64_encode($CompanyID));
		}
		else
		{
			Session::flash('message', 'OOPS! Something Wrong Please Try Again.');
            Session::flash('alert-class', 'alert-danger');
            return Redirect::route('admin.CompanyEditView', base64_encode($CompanyID));
		}
	}
	public function SendNewCredentialsMail($Details)
    {
		$MarketingEmail = $this->MasterConfigModel->getMarketingEmail();     
        $SiteName 		= $this->MasterConfigModel->getSiteName();
        $DashboardDomain= $this->MasterConfigModel->getDashboardDomain();
        
        $to     = $Details['UserEmail'];
        $subject = "Welcome To ".$SiteName;
        $headers = "From: <".$MarketingEmail.">";
        $message='';
        $message.="Thank you for signing up with us!\n\n";

        $message.="To login to your dashboard, please visit https://".$DashboardDomain."\n\n";

        $message.="Username: ".$Details['UserEmail']."\n";
        $message.="Password: ".$Details['Password']."\n\n";
        $message.="NOTE: For security purposes, you will be asked to change your password upon your initial login.\n\n";

        $message.="Once you have logged in, you will be able to view and edit various details within your account.\n\n";

        $message.="PUBLISHERS: we will need to review your details and approve your account. Please reply to this email and tell us if you will be:\n";
        $message.=" a) sending traffic to our websites (i.e. as an affiliate),\n";
        $message.=" b) generating leads on your own websites and integrating with our API, or\n";
        $message.=" c) have data that you want to upload to our List Management service.\n\n";

        $message.="ADVERTISERS: please send us your Agreement and integration specs so we can setup your buyer accounts.\n\n";

        $message.="If you have any questions, simply email me - I'm here to help!\n\n";

        $message.="Kind regards,\n";
        $message.= $SiteName." Team\n";
        mail($to,$subject,$message,$headers);
    }
	public function ChangeCompanyTestMode(Request $request)
	{
		$Data 		= $request->all();
		$Response 	=array();
		$CompanyID   			    	= $Data['CID'];
		$Details['CompanyTestMode'] 	= $Data['CompanyTestMode'];
		$ChangeCompanyTestMode = $this->CMModel->ChangeCompanyTestMode($CompanyID,$Details);
		if($ChangeCompanyTestMode)
		{
			$Massage = '<div class="alert alert-success alert-dismissible fade show" role="alert">
						  Company Mode Has Been Changed.
						  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
						    <span aria-hidden="true">&times;</span>
						  </button>
						</div>';
			$Response['Status'] = 1;
			$Response['Msg'] = $Massage;
		}
		else
		{
			$Massage = '<div class="alert alert-danger alert-dismissible fade show" role="alert">
						  Something Wrong Please Try Again.
						  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
						    <span aria-hidden="true">&times;</span>
						  </button>
						</div>';
			$Response['Status'] = 0;
			$Response['Msg'] = $Massage;
		} 
		if ($Data['CompanyTestMode']=='1') 
   		{
           $Mode = '<a href="javascript:void(0);" 
                   		class="btn btn-danger cfonnT cbtnPadd noBoradi"
                   		onclick="ChangeCompanyTestMode('.$CompanyID.',0);">
						<i class="fas fa-square sqFont" aria-hidden="true"></i> 
						TestMode
					</a> ';
			$Response['ModeHtml'] = $Mode;

        } 
        else
        {
            $Mode = '<a href="javascript:void(0);" 
                   		class="btn btn-success cfonnT cbtnPadd noBoradi"
                   		onclick="ChangeCompanyTestMode('.$CompanyID.',1);">
						<i class="fas fa-check-square sqFont" aria-hidden="true"></i> 
						LiveMode
					</a> ';
			$Response['ModeHtml'] = $Mode;
        }
		echo json_encode($Response);
		exit();
	}
	public function ChangeCompanyStatus(Request $request)
	{
		$Data 		= $request->all();
		$Response 	=array();
		$CompanyID   			    	= $Data['CID'];
		$Details['CompanyStatus'] 		= $Data['CompanyStatus'];
		$ChangeCompanyStatus = $this->CMModel->ChangeCompanyStatus($CompanyID,$Details);
		if($ChangeCompanyStatus)
		{
			$Massage = '<div class="alert alert-success alert-dismissible fade show" role="alert">
						  Company Status Has Been Changed.
						  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
						    <span aria-hidden="true">&times;</span>
						  </button>
						</div>';
			$Response['Status'] = 1;
			$Response['Msg'] = $Massage;
		}
		else
		{
			$Massage = '<div class="alert alert-danger alert-dismissible fade show" role="alert">
						  Something Wrong Please Try Again.
						  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
						    <span aria-hidden="true">&times;</span>
						  </button>
						</div>';
			$Response['Status'] = 0;
			$Response['Msg'] = $Massage;
		}

		if ($Data['CompanyStatus']=='1') 
   		{
           	$Status = '<div class="custom-control custom-checkbox noCush greenCh">
			              <input type="checkbox" 
			              	class="custom-control-input noCush-input green-input" 
			              	id="customCheck'.$CompanyID.'"  
			              	onclick="ChangeCompanyStatus('.$CompanyID.',0);">
			              <label class="custom-control-label noCush-label green-label mb-3" 
			              	for="customCheck'.$CompanyID.'"   
			              	></label>
			            </div> ';
			$Response['StatusHtml'] = $Status;
        } 
        else 
        {
        	$Status = '<div class="custom-control custom-checkbox noCush greenCh">
		              <input type="checkbox" checked="true"
		              	class="custom-control-input noCush-input green-input" 
		              	id="customCheck'.$CompanyID.'" 
		              	onclick="ChangeCompanyStatus('.$CompanyID.',1);">
		              <label class="custom-control-label noCush-label green-label mb-3" 
		              	for="customCheck'.$CompanyID.'"  
		              	></label>
		            </div> ';
			$Response['StatusHtml'] = $Status;
        }
		echo json_encode($Response);
		exit();
	}	
	public function GetCityZipCodeState(Request $request)
	{
		$data   			= $request->all();
		$CompanyZipPostCode = $data['CompanyZipPostCode'];
		$Details 			= $this->CMModel->GetStateDetails($CompanyZipPostCode);
		if(!empty($Details))
		{
			$Result['state'] = $Details->State;
			$Result['city'] = $Details->City;
			$Result['Status'] = 1;
		}
		else
		{	
			$Result['state'] = '';
			$Result['city'] = '';
			$Result['Status'] = 0;
		}
		
		echo json_encode($Result);
		exit();
	}
	public function DeleteCompanyUploadedFile(Request $request)
	{
		$data   = $request->all();	

		$CompanyID 	= $data['CompanyID'];
		$ID 		= $data['ID'];
		$CompanyDetails=$this->CMModel->GetCompanyDetail($CompanyID);
		$Array = $CompanyDetails->UploadedFiles;
		$UploadedFileArray = json_decode($Array);
		$NewUploadedFileArray =array();
		
		$i=0;
		foreach($UploadedFileArray as $ufa)
		{
			if($ufa->id!=$ID)
			{
				$Sample=array("id"=>$i,
							"filename"=>$ufa->filename,
							"filepath"=>'/uploads/'.$ufa->filename);
				array_push($NewUploadedFileArray,$Sample); 	
				$i++;
			}			
		}
		$UploadedFileJson = json_encode($NewUploadedFileArray);
		
		$Details=array('UploadedFiles'=>$UploadedFileJson);

		$Update=$this->CMModel->SaveCompanyDetails($CompanyID,$Details);
		$Response=array();
		if($Update)
		{
			$Message = '<div class="alert alert-success alert-dismissible fade show" role="alert">
						  File Deleted Successfully!.
						  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
						    <span aria-hidden="true">&times;</span>
						  </button>
						</div>';
			$Response['Status'] = 1; 
			$Response['Message'] = $Message; 
		}
		else
		{
			$Message = '<div class="alert alert-danger alert-dismissible fade show" role="alert">
						  OOPS! Something Worng. Please Try Again.
						  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
						    <span aria-hidden="true">&times;</span>
						  </button>
						</div>';
			$Response['Status'] = 1;
			$Response['Message'] = $Message; 
		}

		echo json_encode($Response);
		exit();		
	}

	public function IsUserEmailExist(Request $request)
	{
		$data   			= $request->all();
		$UserEmail 			= $data['UserEmail'];
		$Details 			= $this->CMModel->IsUserEmailExist($UserEmail);
		if($Details>0)
		{
			echo true;
		}
		else
		{	
			echo false;
		}		
		exit();
	}
	public function IsCompanyNameExist(Request $request)
	{
		$data   			= $request->all();
		$CompanyName 			= $data['CompanyName'];
		$Details 			= $this->CMModel->IsCompanyNameExist($CompanyName);
		if($Details>0)
		{
			echo true;
		}
		else
		{	
			echo false;
		}		
		exit();
	}
	public function IsBuyerNameExist(Request $request)
	{
		$data   			= $request->all();
		$BuyerName 			= $data['BuyerName'];
		$Details 			= $this->CMModel->IsBuyerNameExist($BuyerName);
		if($Details>0)
		{
			echo true;
		}
		else
		{	
			echo false;
		}		
		exit();
	}
	public function IsVendorNameExist(Request $request)
	{
		$data   			= $request->all();
		$VendorName 		= $data['VendorName'];
		$Details 			= $this->CMModel->IsVendorNameExist($VendorName);
		if($Details>0)
		{
			echo true;
		}
		else
		{	
			echo false;
		}	
		exit();
	}
	/////////////////////User List//////////////////////
	public function GetUserListFromCompany(Request $request)
	{
		$Data = $request->all();

        $NumOfRecords = $Data['numofrecords'];
        $page = $Data['page'];
        $cur_page = $page;

        $Limitpage = $page - 1;
        $Start = $Limitpage * $NumOfRecords;

        $CompanyID 	= $Data['CompanyID'];
        $FirstName 	= $Data['FirstName'];
        $LastName 	= $Data['LastName'];
        $Email 		= $Data['Email'];
        $Phone 		= $Data['Phone'];
        $UserStatus = $Data['UserStatus'];
        $UserRole 	= $Data['UserRole'];
        $Search 	= array('CompanyID' => $CompanyID,
				            'FirstName' => $FirstName,
				            'LastName' 	=> $LastName,
				            'Email' 	=> $Email,
				            'Phone' 	=> $Phone,
				            'UserStatus'=> $UserStatus,
				            'UserRole' 	=> $UserRole,
				            'CompanyID' => $CompanyID,
				            'Start' 	=> $Start,
				            'NumOfRecords' 	=> $NumOfRecords
				            );
        $GetUserList 	= $this->UMModel->GetUserList($Search);
       
        $AllUserList 	= $GetUserList['Res'];
        $UserCount 		= $GetUserList['Count'];        

        $Pagination = $this->Pagination->LinksForUser($NumOfRecords, $UserCount, $page);
      ?>
		<div class="row mt-3">
          	<div class="table-responsive">
		  		<table class="table table-bordered tableFont txtL nTabl">
                    <thead class="thead-dark">
						<tr>
							<th scope="col" style="padding: 0.684rem; font-size: 0.8rem;">Company</th>
							<th scope="col" style="padding: 0.684rem; font-size: 0.8rem;">First Name</th>
							<th scope="col" style="padding: 0.684rem; font-size: 0.8rem;">Last Name</th>
							<th scope="col" style="padding: 0.684rem; font-size: 0.8rem;">Email</th>
							<th scope="col" style="padding: 0.684rem; font-size: 0.8rem;">Telephone</th>
							<th scope="col" style="padding: 0.684rem; font-size: 0.8rem;">User Role</th>
							<th scope="col" style="padding: 0.684rem; font-size: 0.8rem;">Status</th>
							<th scope="col" style="padding: 0.684rem; font-size: 0.8rem;">Action</th>
						</tr>
                    </thead>
			    	<tbody>
				    <?php 
				     	if(!empty($AllUserList ))
				     	{
				     		$i = $Start + 1;
				     		foreach($AllUserList as $ul)
				     		{
							     ?>				       
						        <tr>
						           	<td><?php echo $ul->CompanyName; ?></td>
						           	<td><?php echo $ul->UserFirstName; ?></td>
						           	<td><?php echo $ul->UserLastName; ?></td>
						           	<td><?php echo $ul->UserEmail.' (UID:'.$ul->UserID.')'; ?></td>
						           	<td><?php echo $ul->UserTel; ?></td>							           	
						           	<td>
						           		<?php 

						           		if ($ul->UserRole == '2') {
		                                    echo "Admin";
		                                } else if ($ul->UserRole == '3') {
		                                    echo "User";
		                                }
		                                else if ($ul->UserRole == '1') {
		                                    echo "SuperAdmin";
		                                }
						           		?>

						           	</td>						           	
						           	<td  style="text-align: center;">
						           		<?php 

						           		if ($ul->UserStatus == '1') 
						           		{
		                                   ?>
		                                   	<div class="custom-control custom-checkbox noCush greenCh">
				                              <input type="checkbox" checked="true" 
				                              	class="custom-control-input noCush-input green-input" 
				                              	id="customCheck<?php echo $ul->UserID; ?>" 
				                              	onclick="ChangeUserStatus('<?php echo $ul->UserID; ?>',0);">
				                              <label class="custom-control-label noCush-label green-label mb-3" 
				                              	for="customCheck<?php echo $ul->UserID; ?>"  
				                              	></label>
				                            </div> 
		                                   <?php
		                                } 
		                                else if ($ul->UserStatus == '0') 
		                                {
		                                    ?>
		                                   	<div class="custom-control custom-checkbox noCush">
				                              <input type="checkbox" 
				                              class="custom-control-input noCush-input" 
				                              id="customCheck<?php echo $ul->UserID; ?>" 
				                              onclick="ChangeUserStatus('<?php echo $ul->UserID; ?>',1);" >
				                              <label class="custom-control-label noCush-label mb-3" 
				                              	for="customCheck<?php echo $ul->UserID; ?>" 
				                              	></label>
				                            </div> 
		                                   <?php
		                                }
						           		?>


						              	
						           	</td>
						           	<td  style="text-align: center;">
						              	<!-- <a href="<?php echo route('admin.UserEditView',array(base64_encode($ul->UserID))) ?>">
			                              <i class="fas fa-edit fa-lg editPadd" title="edit"></i>
			                            </a> -->
			                            
			                            <a href="javascript:void(0);" onclick="GetUserDetailsFromCompany('<?php echo $ul->UserID; ?>');">
			                              <i class="fas fa-edit fa-lg editPadd" title="edit"></i>
			                            </a>
						           	</td>
						        </tr>
			     				<?php 
			     				$i++;
			     			}
			     		} 
			     		else 
			     		{
			     			?>
					     	<tr >
					           <td colspan="7" style="color: #c94542;text-align:center;">Record Not Found.</td>
					           </td>
					        </tr>
			     			<?php 
			     		} 
			     	?>
			     	</tbody>
				  </table>
				</div>
			</div>
		<?php if(!empty($AllUserList ))
		{ ?>
			<div class="row mt-3">			
				<div class="col-md-6 col-sm-6 col-12">
					<div class="dataTables_length mCenter" id="dataTable_length">
						<label>Records/Pages
							<select id="numofrecordsUser" onchange="SearchUserList();" name="dataTable_length" aria-controls="dataTable" class="custom-select custom-select-sm form-control form-control-sm">
							<option value="10" <?php if($NumOfRecords=='10'){ echo 'selected'; } ?> >10</option>
							<option value="20" <?php if($NumOfRecords=='20'){ echo 'selected'; } ?> >20</option>
							<option value="50" <?php if($NumOfRecords=='50'){ echo 'selected'; } ?> >50</option>
							<option value="100" <?php if($NumOfRecords=='100'){ echo 'selected'; } ?> >100</option>
							<option value="150" <?php if($NumOfRecords=='150'){ echo 'selected'; } ?> >150</option>
							<option value="200" <?php if($NumOfRecords=='200'){ echo 'selected'; } ?> >200</option>
							</select>
						</label>
					</div>
				</div>
				<div class="col-md-6 col-sm-6 col-12 grid-margin stretch-card">
					<div class="card noBg border-0">
					    <nav>
					      <?php echo $Pagination; ?>
					    </nav>
					</div>
				</div>
			</div>
      	<?php
      	}
      	else
      	{
      		?>
      		<input type="hidden" name="numofrecords" id="numofrecordsUser" value='10'>
      		<?php
      	}
       	exit();
	}
	public function GetUserDetailsFromCompany(Request $request)
	{
		$Data 			= $request->all();
		$UserID  		= $Data['UserID'];
		$CompanyList 	= $this->UMModel->CompanyList();
		$UserRole 		= $this->UMModel->GetUserRoles();
		$UserDetails 	= $this->UMModel->UserDetails($UserID);
		?>
		<form action="<?php echo route('admin.SaveUserDetails'); ?>" name="UserSaveForm" id="UserSaveForm" method="POST">
	        <div class="row">
	        <input type="hidden" name="UserID" id="UserID" value="<?php echo $UserDetails->UserID; ?>">
	        <input type="hidden" name="CompanyID" id="CompanyID" value="<?php echo $UserDetails->CompanyID; ?>">
	        <input type="hidden" name="_token" id="_token" value="<?php echo  csrf_token(); ?>">
	        <input type="hidden" name="From" id="From" value="Company">
	          <div class="col-md-1 form-group tadmPadd labFontt">
	              <label for="exampleInputEmail1">UserID:</label>
	              <input disabled type="text" onkeypress="HideErr('UserIDErr');" value="<?=$UserDetails->UserID;?>"  class="form-control txtPadd" placeholder="UserID">
	              <span id="UserIDErr" class="errordisp"></span>
	           </div> 
	            <div class="col-md-3 form-group tadmPadd labFontt">
	              <label for="exampleInputEmail1">First Name:</label>
	              <input type="text" onkeypress="HideErr('UserFirstNameErr');" value="<?=$UserDetails->UserFirstName;?>" id="UserFirstName" name="UserFirstName" class="form-control txtPadd" placeholder="First Name">
	              <span id="UserFirstNameErr" class="errordisp"></span>
	           </div>
	           <div class="col-md-3 form-group tadmPadd labFontt">
	              <label for="exampleInputEmail1">Last Name:</label>
	              <input type="text" onkeypress="HideErr('UserLastNameErr');" value="<?=$UserDetails->UserLastName;?>" id="UserLastName" name="UserLastName" class="form-control txtPadd" placeholder="Last Name">
	              <span id="UserLastNameErr" class="errordisp"></span>
	           </div> 
	           <div class="col-md-3 form-group tadmPadd labFontt">
	              <label for="exampleInputEmail1">Email:</label>
	              <input type="email" onkeypress="HideErr('UserEmailErr');" value="<?=$UserDetails->UserEmail;?>" id="UserEmail" name="UserEmail" class="form-control camH" id="exampleInputEmail1" placeholder="Email">
	              <span id="UserEmailErr" class="errordisp"></span>
	           </div>
	            <div class="col-md-2 form-group tadmPadd labFontt">
	              <label for="exampleInputEmail1">Telephone:</label>
	              <input type="text" onkeypress="HideErr('UserTelErr');" value="<?=$UserDetails->UserTel;?>" id="UserTel" name="UserTel" class="form-control txtPadd" placeholder="Telephone">
	              <span id="UserTelErr" class="errordisp"></span>
	           </div>                              
	        </div>
	        <div class="row">	          
	          
	           <div class="col-md-3 form-group tadmPadd labFontt">
	              <label for="exampleInputEmail1">Skype ID:</label>
	              <input type="text" onkeypress="HideErr('UserSkypeErr');" value="<?=$UserDetails->UserSkype;?>" id="UserSkype" name="UserSkype" class="form-control txtPadd" placeholder="Skype ID">
	              <span id="UserSkypeErr" class="errordisp"></span>
	           </div>
	           <div class="col-md-2 form-group tadmPadd labFontt">
	            	<label for="exampleInputEmail1">User Role:</label>
	              <select class="form-control fonT" id="UserRoleEdit" name="UserRole" onchange="HideErr('UserRoleEditErr');">
	                <option value="">Select User Role</option>
	                  <?php foreach($UserRole as $Key=>$Value){ ?>
	                  <option value="<?=$Key;?>"
	                  <?php if($Key==$UserDetails->UserRole){ echo "selected";} ?>><?=$Value;?></option>
	                  <?php } ?>
	              </select>
	              <span id="UserRoleEditErr" class="errordisp"></span>
	           </div>
	           <?php if($UserDetails->UserStatus=='1'){?>
	            <div class="col-md-2 form-group tadmPadd labFontt">
	              <label for="exampleInputEmail1">User Status:</label>
	              <div class="onoffswitch4">
	                <input type="checkbox" name="UserStatus" class="onoffswitch4-checkbox" id="UserStatusEdit" 
	                  checked>
	                <label class="onoffswitch4-label" for="UserStatusEdit">
	                    <span class="onoffswitch4-inner"></span>
	                    <span class="onoffswitch4-switch"></span>
	                </label>
	              </div>
	            </div> 
	            <?php } else { ?>
	             <div class="col-md-2 form-group tadmPadd labFontt">
	              <label for="exampleInputEmail1">User Status:</label>
	              <div class="onoffswitch4">
	                <input type="checkbox" name="UserStatus" class="onoffswitch4-checkbox" id="UserStatusEdit" 
	                  >
	                <label class="onoffswitch4-label" for="UserStatusEdit">
	                    <span class="onoffswitch4-inner"></span>
	                    <span class="onoffswitch4-switch"></span>
	                </label>
	              </div>
	            </div> 
	            <?php } ?> 
	            <?php if($UserDetails->ShowAPISpecs=='1'){?>                                
	            <div class="col-md-2 form-group tadmPadd labFontt">
	              <label for="exampleInputEmail1">Show Specs:</label>
	              <div class="onoffswitch6">
	                <input type="checkbox" class="onoffswitch6-checkbox" 
	                id="ShowAPISpecs" name="ShowAPISpecs" checked>
	                <label class="onoffswitch6-label" for="ShowAPISpecs">
	                    <span class="onoffswitch6-inner"></span>
	                    <span class="onoffswitch6-switch"></span>
	                </label>
	              </div>
	            </div>
	            <?php } else { ?>
	            <div class="col-md-2 form-group tadmPadd labFontt">
	              <label for="exampleInputEmail1">Show Specs:</label>
	              <div class="onoffswitch6">
	                <input type="checkbox" class="onoffswitch6-checkbox" 
	                id="ShowAPISpecs" name="ShowAPISpecs" >
	                <label class="onoffswitch6-label" for="ShowAPISpecs">
	                    <span class="onoffswitch6-inner"></span>
	                    <span class="onoffswitch6-switch"></span>
	                </label>
	              </div>
	            </div>
	            <?php } ?>
	            <?php if($UserDetails->ShowAffLinks=='1'){?>                                 
	            <div class="col-md-2 form-group tadmPadd labFontt">
	              <label for="exampleInputEmail1">Show Affiliate Links:</label>
	              <div class="onoffswitch7">
	                <input type="checkbox" class="onoffswitch7-checkbox" 
	                id="ShowAffLinks" name="ShowAffLinks" checked>
	                <label class="onoffswitch7-label" for="ShowAffLinks">
	                    <span class="onoffswitch7-inner"></span>
	                    <span class="onoffswitch7-switch"></span>
	                </label>
	              </div>
	            </div>
	            <?php } else { ?>
	            <div class="col-md-2 form-group tadmPadd labFontt">
	              <label for="exampleInputEmail1">Show Affiliate Links:</label>
	              <div class="onoffswitch7">
	                <input type="checkbox" class="onoffswitch7-checkbox"
	                id="ShowAffLinks" name="ShowAffLinks">
	                <label class="onoffswitch7-label" for="ShowAffLinks">
	                    <span class="onoffswitch7-inner"></span>
	                    <span class="onoffswitch7-switch"></span>
	                </label>
	              </div>
	            </div>
	            <?php } ?> 
	        </div>
	        <div class="row" >
	            <div class="col-md-4 form-group tadmPadd labFonsize">
		          <div class="form-check form-check-flat form-check-primary adComp adComp-primary mt-4">
		             <label class="form-check-label adComp-label labLh">
		             <input type="checkbox" class="form-check-input chOp" 
		             	id="ChangePassword" name="ChangePassword" onclick="ChangePasswordShowHide();">
		             Do You Want To Change Password? 
		             </label>
		          </div>
	            </div> 
	            <div class="col-md-3 form-group tadmPadd labFontt" id="PasswordDiv" style="display:none;">
	              <label for="exampleInputPassword1">Password:</label>
	              <input type="text" onkeypress="HideErr('PasswordErr');" class="form-control camH" id="Password" name="Password" placeholder="Password">
	              <span id="PasswordErr" class="errordisp"></span>
	           </div>
	           <div class="col-md-3 form-group tadmPadd labFontt" id="ConfirmPasswordDiv" style="display:none;">
	              <label for="exampleInputPassword1">Confirm Password:</label>
	              <input type="text" onkeypress="HideErr('ConfirmPasswordErr');" class="form-control camH" id="ConfirmPassword" name="ConfirmPassword" placeholder="Confirm Password">
	              <span id="ConfirmPasswordErr" class="errordisp"></span>
	           </div>
	           <div class="col-md-2 form-group tadmPadd labFontt" id="GeneratePasswordDiv" style="display:none;">
	              <label class="gP" for="exampleInputPassword1">Generate Password:</label>
	              <button type="button" onclick="GeneratePassword();" class="btn btn-success fonT noBoradi cbtnPadd sercFontt gpBg">Generate Password</button>
	           </div>                                                           
	        </div>
	         <div class="row" >
	         	<div class="col-md-6 form-group mb-0 labFontt">
	              <label for="exampleFormControlTextarea1">User Notes:</label>
	              <textarea class="form-control" id="UserNotes" name="UserNotes" rows="3"><?=$UserDetails->UserNotes;?></textarea>
	            </div>
	           	<div class="col-md-6 form-group tadmPadd labFonsize">
	              <div class="form-check form-check-flat form-check-primary adComp adComp-primary mt-4">
	                 <label class="form-check-label adComp-label labLh">
	                 <input type="checkbox" class="form-check-input chOp" id="SendCredentials" name="SendCredentials">
	                  Would you like to email credentials to the user?
	                 </label>
	              </div>
	           	</div>            
	        </div>
	        <div class="row btnFlo">
	           <button type="button" id="UserSaveBtn" class="btn btn-success btn-fw noBoradi mr-2" onclick="SaveUserDetails();" >Update</button>
	           <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
	        </div>
	     </form>
		<?php
	}
	/////////////////////Vendors List//////////////////////
	public function GetVendorListFromCompany(Request $request)
	{
		$Data = $request->all();

        $NumOfRecords = $Data['numofrecords'];
        $page = $Data['page'];
        $cur_page = $page;

        $Limitpage = $page - 1;
        $Start = $Limitpage * $NumOfRecords;

        $CompanyID 		= $Data['CompanyID'];
        $CampaignID 	= $Data['Campaign'];
        $VendorID 		= $Data['VendorID'];
        $VendorName 	= $Data['VendorName'];
        $VendorStatus 	= $Data['VendorStatus'];
        $ShowInactive 	= $Data['ShowInactive'];
        $ShowTest 		= $Data['ShowTest'];
      
        $Search 	= array('CompanyID' 	=> $CompanyID,
        					'CampaignID' 	=> $CampaignID,
				            'VendorID' 		=> $VendorID,
				            'VendorName' 	=> $VendorName,
				            'VendorStatus' 	=> $VendorStatus,
				            'ShowInactive' 	=> $ShowInactive,
				            'ShowTest' 		=> $ShowTest,
				            'Start' 		=> $Start,
				            'NumOfRecords' 	=> $NumOfRecords
				            );
        $GetVendorList 	= $this->VMModel->GetVendorList($Search);
       
        $AllVendorList 	= $GetVendorList['Res'];
        $UserCount 		= $GetVendorList['Count'];        

        $Pagination = $this->Pagination->Links($NumOfRecords, $UserCount, $page);
        $CampaignDetails = $this->CModel->GetCampaignDetails($CampaignID);

        $HasSpecs = $CampaignDetails->HasSpecs;
      ?>
		<div class="row mt-0">
          	<div class="table-responsive">
		  		<table class="table table-bordered tableFont txtL nTabl">
                    <thead class="thead-dark">
						<tr>
							<th scope="col" style="padding: 0.684rem; font-size: 0.8rem;">Company Name</th>
							<th scope="col" style="padding: 0.684rem; font-size: 0.8rem;">Campaign</th>
							<th scope="col" style="padding: 0.684rem; font-size: 0.8rem;">Admin Label</th>
							<th scope="col" style="padding: 0.684rem; font-size: 0.8rem;">Daily Cap</th>
							<th scope="col" style="padding: 0.684rem; font-size: 0.8rem;">Total Cap</th>
							<?php if($CampaignDetails->CampaignType=="DirectPost"){ ?>
							<th scope="col" style="padding: 0.684rem; font-size: 0.8rem;">Max Time Out</th>
							<?php } else { ?>
							<th scope="col" style="padding: 0.684rem; font-size: 0.8rem;">Max Ping Time Out</th>
							<th scope="col" style="padding: 0.684rem; font-size: 0.8rem;">Max Post Time Out</th>
							<?php }?>
							<th scope="col" style="padding: 0.684rem; font-size: 0.8rem;">Status</th>
							<th scope="col" style="padding: 0.684rem; font-size: 0.8rem;">Vendor Mode</th>
							<th scope="col" style="padding: 0.684rem; font-size: 0.8rem;">Action</th>
						</tr>
                    </thead>
			    	<tbody>
				    <?php 
				     	if(!empty($AllVendorList ))
				     	{
				     		foreach($AllVendorList as $p)
				     		{
							     ?>				       
						        <tr>
						           	<td><?php echo $p->CompanyName; ?></td>
						           	<td><?php echo $p->CampaignName; ?></td>
						           	<td><?php echo $p->AdminLabel ." (VID: ".$p->VendorID.")"; ?></td>
						           	<td><?php echo $p->DailyCap; ?></td>
						           	<td><?php echo $p->TotalCap; ?></td>
						           	<?php if($CampaignDetails->CampaignType=="DirectPost"){ ?>
									<td><?php echo $p->MaxTimeOut; ?></td>
									<?php } else { ?>
									<td><?php echo $p->MaxPingTimeOut; ?></td>
									<td><?php echo $p->MaxPostTimeOut; ?></td>
									<?php }?>
						           	<td id="StatusHtml_<?php echo $p->VendorID; ?>">
						           		<?php 
						           		if ($p->VendorStatus == '1') 
						           		{
		                                   ?>
		                                   	<div class="custom-control custom-checkbox noCush greenCh">
				                              <input type="checkbox" checked="true"
				                              	class="custom-control-input noCush-input green-input" 
				                              	id="customCheck<?php echo $p->VendorID; ?>" 
				                              	onclick="ChangeVendorStatus('<?php echo $p->VendorID; ?>',0);">
				                              <label class="custom-control-label noCush-label green-label mb-3" 
				                              	for="customCheck<?php echo $p->VendorID; ?>"  
				                              	></label>
				                            </div> 
		                                   <?php
		                                } 
		                                else if ($p->VendorStatus == '0') 
		                                {
		                                    ?>
		                                   	<div class="custom-control custom-checkbox noCush">
				                              <input type="checkbox"  
				                              class="custom-control-input noCush-input" 
				                              id="customCheck<?php echo $p->VendorID; ?>" 
				                              onclick="ChangeVendorStatus('<?php echo $p->VendorID; ?>',1);" >
				                              <label class="custom-control-label noCush-label mb-3" 
				                              	for="customCheck<?php echo $p->VendorID; ?>" 
				                              	></label>
				                            </div> 
		                                   <?php
		                                }
						           		?>
						           	</td>
						           	<td id="ModeHtml_<?php echo $p->VendorID; ?>">
						           		<?php 
						           		if ($p->VendorTestMode == '1') 
						           		{
		                                   ?>
		                                   	<a href="javascript:void(0);" 
		                                   		class="btn btn-success cfonnT cbtnPadd noBoradi"
		                                   		onclick="ChangeVendorTestMode('<?php echo $p->VendorID; ?>',0);">
												<i class="fas fa-check-square sqFont" aria-hidden="true"></i> 
												LiveMode
											</a>  
		                                   <?php

		                                } 
		                                else if ($p->VendorTestMode == '0') 
		                                {
		                                    ?>
		                                   	<a href="javascript:void(0);" 
		                                   		class="btn btn-danger cfonnT cbtnPadd noBoradi"
		                                   		onclick="ChangeVendorTestMode('<?php echo $p->VendorID; ?>',1);">
												<i class="fas fa-square sqFont" aria-hidden="true"></i> 
												TestMode
											</a>  
		                                   <?php
		                                }
						           		?>
						           	</td>
						           	<td>
						           		<ul class="btnList">
                                          <li>
                                            <a href="javascript:void(0);" onclick="GetVendorDetailsFromCompany('<?php echo $p->VendorID; ?>');">
				                              	<button type="button" title="edit" 
	                                        		class="btn btn-info cfonnT cbtnPadd noBoradi">
	                                        		<i class="fas fa-pencil-alt fnnPadd"></i>Edit
                                        		</button>
				                            </a>
                                          </li>
                                          <li>
                                            <a href="javascript:void(0);" onclick="GetVendorCloneDetailsFromCompany('<?php echo $p->VendorID; ?>');">
				                              	<button type="button" title="add" 
                                            		class="btn btn-info cfonnT cbtnPadd noBoradi bgClrred">
                                            		<i class="fas fa-plus fnnPadd"></i>Clone
                                            	</button>
				                            </a>
                                          </li>
                                          <?php if($HasSpecs=='1'){ ?>
                                          <li>
	                                        <a href="<?php echo route('admin.ViewSpecs',['CampaignID'=>$CampaignID,'v_id'=>base64_encode($p->VendorID)]); ?>"
                                            	target="_blank">
                                            	<button type="button" title="add" 
	                                            	class="btn btn-success cfonnT cbtnPadd noBoradi">
	                                            	<i class="fas fa-eye fnnPadd"></i>Specs
	                                            </button>
	                                        </a>
                                          </li>
                                          <?php } ?>
                                        </ul>
						           	</td>
						        </tr>
			     				<?php 
			     			}
			     		} 
			     		else 
			     		{
			     			?>
					     	<tr >
					           <td colspan="10" style="color: #c94542;text-align:center;">Record Not Found.</td>
					           </td>
					        </tr>
			     			<?php 
			     		} 
			     	?>
			     	</tbody>
				  </table>
				</div>
			</div>
		<?php if(!empty($AllVendorList ))
		{ ?>
			<div class="row mt-3">			
				<div class="col-md-6 col-sm-6 col-12">
					<div class="dataTables_length mCenter" id="dataTable_length">
						<label>Records/Pages
							<select id="numofrecordsVendor" onchange="SearchVendorList();" name="dataTable_length" aria-controls="dataTable" class="custom-select custom-select-sm form-control form-control-sm">
							<option value="10" <?php if($NumOfRecords=='10'){ echo 'selected'; } ?> >10</option>
							<option value="20" <?php if($NumOfRecords=='20'){ echo 'selected'; } ?> >20</option>
							<option value="50" <?php if($NumOfRecords=='50'){ echo 'selected'; } ?> >50</option>
							<option value="100" <?php if($NumOfRecords=='100'){ echo 'selected'; } ?> >100</option>
							<option value="150" <?php if($NumOfRecords=='150'){ echo 'selected'; } ?> >150</option>
							<option value="200" <?php if($NumOfRecords=='200'){ echo 'selected'; } ?> >200</option>
							</select>
						</label>
					</div>
				</div>
				<div class="col-md-6 col-sm-6 col-12 grid-margin stretch-card">
					<div class="card noBg border-0">
					    <nav>
					      <?php echo $Pagination; ?>
					    </nav>
					</div>
				</div>
			</div>
      	<?php
      	}
      	else
      	{
      		?>
      		<input type="hidden" name="numofrecords" id="numofrecordsVendor" value='10'>
      		<?php
      	}
       	exit();
	}
	public function GetVendorDetailsFromCompany(Request $request)
	{
		$Data 				= $request->all();
		$VendorID  			= $Data['VendorID'];
		$VendorDetails 		= $this->VMModel->VendorDetails($VendorID);
		$CompanyID 			= $VendorDetails->CompanyID;
		$CompanyDetails		= $this->VMModel->CompanyDetails($CompanyID);
		$CompanyTestMode 	= $CompanyDetails->CompanyTestMode;
		$CompanyStatus 		= $CompanyDetails->CompanyStatus;
		$CompanyList 		= $this->VMModel->CompanyListForEdit($CompanyTestMode,$CompanyStatus);

		$VendorDetails 		= $VendorDetails;
		$CampaignDropDown 	= $this->MasterConfigModel->getCampaignList();
		$CampaignID 		= $VendorDetails->CampaignID;
		$CampaignDetails 	= $this->CModel->GetCampaignDetails($CampaignID);

		$ShowInactive="checked";
		$ShowTest="";
		if($CompanyStatus==1)
		{
			$ShowInactive="";
		}
		if($CompanyTestMode==1)
		{
			$ShowTest="checked";
		}
		?>
		<form action="<?php echo route('admin.SaveVendorDetails');?>" name="VendorUpdateForm" id="VendorUpdateForm" method="POST">
      	<input type="hidden" name="FormType" id="FormType" value="Update">
      	<input type="hidden" name="From" id="From" value="Company">
      	<input type="hidden" name="VendorID" id="VendorID" value="<?php echo $VendorDetails->VendorID; ?>">
      	<input type="hidden" name="Company" id="Company" value="<?php echo $VendorDetails->CompanyID; ?>">
      	<input type="hidden" name="VendorApiKey" id="VendorApiKey" value="<?php echo $VendorDetails->APIKey; ?>">
      	<input type="hidden" name="_token" id="_token" value="<?php echo  csrf_token(); ?>">
      	<input type="hidden" name="CampaignTypeEdit" id="CampaignTypeEdit" value="<?php echo $CampaignDetails->CampaignType; ?>">
			<div class="row">
				<div class="col-md-12" id="MessageEdit">
				
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 form-group  tadmPadd labFontt">
				    <label for="exampleFormControlSelect1">Campaign:</label>
				    <select class="form-control fonT" id="CampaignEditVendor" name="Campaign" 
				        onchange="GetCampaignDetailsEdit(),HideErr('CompanyErr');">
				        <?php foreach($CampaignDropDown as $cdd){ ?>
				        <option value="<?php echo $cdd->CampaignID; ?>"
				          <?php if($cdd->CampaignID==$VendorDetails->CampaignID){echo "selected";}?>>
				          	<?php echo $cdd->CampaignName; ?></option>
				        <?php } ?>  
				    </select>
				    <span id="CampaignEditErr"></span>
				</div>
				<div class="col-md-3 form-group tadmPadd labFontt">
				  <label for="exampleFormControlSelect1">Vendor ID:</label>
				  <input type="text" disabled class="form-control txtPadd" 
				  value="<?php echo $VendorDetails->VendorID; ?>" placeholder="">
				</div>
				<div class="col-md-5 form-group  tadmPadd labFontt">
				  <label for="exampleFormControlSelect1">Vendor API-KEY:</label>
				  <input type="text" disabled class="form-control txtPadd" 
				    value="<?php echo $VendorDetails->APIKey; ?>" placeholder="">
				</div>
			</div> 
			<div class="row">				
				<div class="col-md-3 form-group tadmPadd labFontt">
				  <label for="exampleInputEmail1">Admin Label:</label>
				    <input type="text" class="form-control txtPadd" 
				    value="<?php echo $VendorDetails->AdminLabel; ?>" id="AdminLabelEditVendor" name="AdminLabel"
				    onkeypress="HideErr('AdminLabelEditVendorErr');">
				    <span id="AdminLabelEditVendorErr"></span>
				</div>
				<div class="col-md-2 form-group tadmPadd btnCen">
				  <button type="button" id="CopyAdminLableEditVendor" onclick="CopyAdminLableEditVendorLabel();" class="btn btn-success noBoradi mr-2">&gt;&gt;</button>
				</div>
				<div class="col-md-3 form-group tadmPadd labFontt">
				  <label for="exampleInputEmail1">Partner Label:</label>
				    <input type="text" class="form-control txtPadd" 
				      value="<?php echo $VendorDetails->PartnerLabel; ?>" id="PartnerLabelEditVendor" name="PartnerLabel"
				      onkeypress="HideErr('PartnerLabelEditVendorErr');">
				    <span id="PartnerLabelEditVendorErr"></span>
				</div>
			</div>
			<div class="row">
				<div class="col-md-3 form-group mb-0 tadmPadd labFontt">
				  <label for="PayoutCalculation">Payout Calculation:</label>
				    <select class="form-control fonT" id="PayoutCalculationEditVendor" onchange="PayoutCalculationChange();" name="PayoutCalculation" >
				      <option value="RevShare" <?php if($VendorDetails->PayoutCalculation=="RevShare"){ echo "selected"; } ?>>RevShare</option>
				      <option value="FixedPrice" <?php if($VendorDetails->PayoutCalculation=="FixedPrice"){ echo "selected"; } ?>>Fixed Price</option>
				      <option value="TierPrice" <?php if($VendorDetails->PayoutCalculation=="TierPrice"){ echo "selected"; } ?>>Tier Price</option>
				      <option value="MinPrice" <?php if($VendorDetails->PayoutCalculation=="MinPrice"){ echo "selected"; } ?>>Min Price</option>
				      <option value="BucketPrice" <?php if($VendorDetails->PayoutCalculation=="BucketPrice"){ echo "selected"; } ?>>Bucket Price</option>
				      <option value="BucketSystemBySubID" <?php if($VendorDetails->PayoutCalculation=="BucketSystemBySubID"){ echo "selected"; } ?>>Bucket System By SubID</option>
				    </select>
				</div>
				<?php 
				$PayoutCalculation = $VendorDetails->PayoutCalculation;
				if($PayoutCalculation=='FixedPrice')
				{ 
				     $PayoutCalculationFixedPrice="style='display:block'";
				     $PayoutCalculationTierPrice="style='display:none'";
				     $PayoutCalculationRevShare="style='display:none'";
				} 
				else if($PayoutCalculation=='RevShare')
				{
				     $PayoutCalculationRevShare="style='display:block'";
				     $PayoutCalculationTierPrice="style='display:none'";
				     $PayoutCalculationFixedPrice="style='display:none'";
				}
				 else if($PayoutCalculation=='TierPrice')
				{
				     $PayoutCalculationRevShare="style='display:none'";
				     $PayoutCalculationTierPrice="style='display:block'";
				     $PayoutCalculationFixedPrice="style='display:none'";
				}
				 else if($PayoutCalculation=='MinPrice' )
				{
				     $PayoutCalculationRevShare="style='display:block'";
				     $PayoutCalculationTierPrice="style='display:none'";
				     $PayoutCalculationFixedPrice="style='display:none'";
				}
				 else if($PayoutCalculation=='BucketSystemBySubID' || $PayoutCalculation=='BucketPrice')
				{
				     $PayoutCalculationRevShare="style='display:block'";
				     $PayoutCalculationTierPrice="style='display:none'";
				     $PayoutCalculationFixedPrice="style='display:block'";
				}
				?>
				<div class="col-md-2 form-group tadmPadd labFontt" <?php echo $PayoutCalculationRevShare ?>  
				    id="RevsharePercentInputEditVendor">
				  <label for="exampleInputEmail1">Revshare: </label>
				  <input type="text" class="form-control txtPadd" value="<?php echo $VendorDetails->RevsharePercent; ?>" 
				  placeholder="70" id="RevshareEditVendor" name="Revshare" onkeypress="HideErr('RevshareEditvErr');">
				  <span id="RevshareEditVendorErr"></span>
				</div>
				<div class="col-md-2 form-group tadmPadd labFontt" <?php echo $PayoutCalculationFixedPrice ?> 
				    id="FixedPriceInputEditVendor">
				  <label for="exampleInputEmail1">Fixed Price:</label>
				  <input type="text" class="form-control txtPadd" value="<?php echo $VendorDetails->FixedPrice; ?>" 
				  placeholder="70" id="FixedPriceEditVendor" name="FixedPrice" onkeypress="HideErr('FixedPriceEditVendorErr');">
				  <span id="FixedPriceEditVendorErr"></span>
				</div>
				<div class="col-md-5 form-group tadmPadd labFontt" <?php echo $PayoutCalculationTierPrice ?> 
				    id="TierPriceInputEditVendor">
				  <label for="exampleInputEmail1">Revshare Percent (for when Tier Payout is $0.00):</label>
				  <input type="text" class="form-control txtPadd" value="<?php echo $VendorDetails->RevsharePercent; ?>"
				   placeholder="70" id="TierPriceEditVendor" name="TierPrice" onkeypress="HideErr('TierPriceEditVendorErr');">
				  <span id="TierPriceEditVendorErr"></span>
				</div>
				<div class="col-md-2 form-group tadmPadd labFontt">
				  <label for="exampleInputEmail1">Daily Cap:</label>
				    <input type="text"  value="<?php echo $VendorDetails->DailyCap; ?>" 
				      id="DailyCapEditVendor" name="DailyCap" class="form-control txtPadd" placeholder="Daily Cap"
				      onkeypress="HideErr('DailyCapEditvErr');">
				    <span id="DailyCapEditVendorErr"></span>
				</div>
				<div class="col-md-2 form-group tadmPadd labFontt">
				  <label for="exampleInputEmail1">Total Cap:</label>
				    <input type="text" value="<?php echo $VendorDetails->TotalCap; ?>" 
				      id="TotalCapEditVendor" name="TotalCap" class="form-control txtPadd" placeholder="Total Cap"
				      onkeypress="HideErr('TotalCapEditVendorErr');">
				    <span id="TotalCapEditVendorErr"></span>
				</div>
			</div>
			<div class="row">				
				<?php if($VendorDetails->VendorStatus=='1'){ ?>
				<div class="col-md-2 form-group tadmPadd labFontt">
				  <label for="exampleInputEmail1">Vendor Status:</label>
				    <div class="onoffswitch8">
				      <input type="checkbox" name="VendorStatus" class="onoffswitch8-checkbox" 
				      id="VendorStatusEdit" checked="">
				      <label class="onoffswitch8-label" for="VendorStatusEdit">
				        <span class="onoffswitch8-inner"></span>
				        <span class="onoffswitch8-switch"></span>
				      </label>
				    </div>
				</div>
				<?php } else {?>
				<div class="col-md-2 form-group tadmPadd labFontt">
				  <label for="exampleInputEmail1">Vendor Status:</label>
				    <div class="onoffswitch8">
				      <input type="checkbox" name="VendorStatus" class="onoffswitch8-checkbox" 
				      id="VendorStatusEdit">
				      <label class="onoffswitch8-label" for="VendorStatusEdit">
				        <span class="onoffswitch8-inner"></span>
				        <span class="onoffswitch8-switch"></span>
				      </label>
				    </div>
				</div>
				<?php }?>
				<?php if($VendorDetails->VendorTestMode=='1'){ ?>
				<div class="col-md-2 form-group tadmPadd labFontt">
				  <label for="exampleInputEmail1">Vendor Mode?</label>
				    <div class="onoffswitch9">
				      <input type="checkbox" name="VendorTestMode" class="onoffswitch9-checkbox"
				         id="VendorTestMode" checked="">
				      <label class="onoffswitch9-label" for="VendorTestMode">
				        <span class="onoffswitch9-inner"></span>
				        <span class="onoffswitch9-switch"></span>
				      </label>
				    </div>
				</div>
				<?php } else {?>
				<div class="col-md-2 form-group tadmPadd labFontt">
				  <label for="exampleInputEmail1">Vendor Mode?</label>
				    <div class="onoffswitch9">
				      <input type="checkbox" name="VendorTestMode" class="onoffswitch9-checkbox" 
				        id="VendorTestMode" >
				      <label class="onoffswitch9-label" for="VendorTestMode">
				        <span class="onoffswitch9-inner"></span>
				        <span class="onoffswitch9-switch"></span>
				      </label>
				    </div>
				</div>
				<?php }?>				
				
				<?php 
				if($CampaignDetails->CampaignType=="DirectPost"){                           
				  $MaxTimeOutInput = "style='display:block;'";
				  $MaxPingTimeOutInput = "style='display:none;'";
				  $MaxPostTimeOutInput = "style='display:none;'";
				}else if($CampaignDetails->CampaignType=="PingPost"){
				  $MaxTimeOutInput = "style='display:none;'";
				  $MaxPingTimeOutInput = "style='display:block;'";
				  $MaxPostTimeOutInput = "style='display:block;'";
				}
				?>
				 <div class="col-md-2 form-group tadmPadd labFontt" id="MaxTimeOutInputEditVendor" <?php echo $MaxTimeOutInput; ?>>
				  <label for="exampleInputEmail1">Max Time Out:</label>
				    <input type="text" class="form-control txtPadd" placeholder="0" 
				    id="MaxTimeOutEditVendor" name="MaxTimeOut" value="<?php echo $VendorDetails->MaxTimeOut; ?>" onkeypress="HideErr('MaxTimeOutEditVendorErr');">
				    <span id="MaxTimeOutEditVendorErr"></span>
				</div>
				<div class="col-md-2 form-group tadmPadd labFontt" id="MaxPingTimeOutInputEditVendor" <?php echo $MaxPingTimeOutInput; ?>>
				  <label for="exampleInputEmail1">Max Ping Time Out:</label>
				    <input type="text" class="form-control txtPadd" placeholder="0" 
				    id="MaxPingTimeOutEditVendor" name="MaxPingTimeOut" value="<?php echo $VendorDetails->MaxPingTimeOut; ?>" onkeypress="HideErr('MaxPingTimeOutEditVendorErr');">
				    <span id="MaxPingTimeOutEditVendorErr"></span>
				</div>
				<div class="col-md-2 form-group tadmPadd labFontt" id="MaxPostTimeOutInputEditVendor" <?php echo $MaxPostTimeOutInput; ?>>
				  <label for="exampleInputEmail1">Max Post Time Out:</label>
				    <input type="text" class="form-control txtPadd" placeholder="0" 
				    id="MaxPostTimeOutEditVendor" name="MaxPostTimeOut" value="<?php echo $VendorDetails->MaxPostTimeOut; ?>" onkeypress="HideErr('MaxPostTimeOutEditVendorErr');">
				    <span id="MaxPostTimeOutEditVendorErr"></span>
				</div> 
				<?php if($VendorDetails->TrackingPixelStatus=='1'){ ?>
				<div class="col-md-2 form-group tadmPadd labFontt">
				  <label for="exampleInputEmail1">Tracking Pixel Status?</label>
				    <div class="onoffswitch10">
				      <input type="checkbox" name="TrackingPixelStatus" class="onoffswitch10-checkbox"
				       id="TrackingPixelStatusEditVendor" checked="">
				      <label class="onoffswitch10-label" for="TrackingPixelStatusEditVendor">
				        <span class="onoffswitch10-inner"></span>
				        <span class="onoffswitch10-switch"></span>
				      </label>
				    </div>
				</div>
				<?php } else {?>
				<div class="col-md-2 form-group tadmPadd labFontt">
				  <label for="exampleInputEmail1">Tracking Pixel Status?</label>
				    <div class="onoffswitch10">
				      <input type="checkbox" name="TrackingPixelStatus" class="onoffswitch10-checkbox"
				       id="TrackingPixelStatusEditVendor" >
				      <label class="onoffswitch10-label" for="TrackingPixelStatusEditVendor">
				        <span class="onoffswitch10-inner"></span>
				        <span class="onoffswitch10-switch"></span>
				      </label>
				    </div>
				</div>
				<?php }?>
			</div>
			<div class="row">
				<div class="col-md-2 form-group  tadmPadd labFontt">
				  <label for="exampleFormControlSelect1">Tracking Pixel Type:</label>
				    <select class="form-control fonT" id="TrackingPixelType" name="TrackingPixelType">
				      <option value="Pixel" <?php if($VendorDetails->TrackingPixelType=='Pixel'){echo "selected";}?>>Pixel</option>
				      <option value="PostBack" <?php if($VendorDetails->TrackingPixelType=='PostBack'){echo "selected";}?>>Post Back</option>
				    </select>
				</div>
				<div class="col-md-2 form-group labFontt">
				  <label for="exampleFormControlSelect1">Insert Token:</label>
				    <select class="form-control" id="insert_Token_typeVendor" name="insert_Token_type" onchange="updateTextBoxEditVendor(this)">
				      <option value="aid">aid</option>
				      <option value="SubID">SubID</option>
				      <option value="TestMode">TestMode</option>
				      <option value="TransactionID">TransactionID</option>
				      <option value="VendorLeadPrice">VendorLeadPrice</option>
				  </select>
				</div>
				<div class="col-md-5 form-group labFontt">
				  <label for="exampleFormControlTextarea1">Tracking Pixel Code:</label>
				  <textarea class="form-control" id="TrackingPixelCodeEditVendor" name="TrackingPixelCode" rows="3"><?php echo $VendorDetails->TrackingPixelCode; ?></textarea>
				</div>				
			</div>
	        <div class="row">
	        	<div class="col-md-5 form-group  labFontt">
				  <label for="exampleFormControlTextarea1">Vendor Notes:</label>
				  <textarea class="form-control" id="VendorNotes" name="VendorNotes" rows="3"><?php echo $VendorDetails->VendorNotes; ?></textarea>
				</div>
	        </div>
		    <div class="row btnFlo">
		      	<div class="col-md-12 form-group mb-0 labFontt">
		       		<button type="button" class="btn btn-warning btn-fw noBoradi mr-2" onclick="SendMailToAdminUser();">Send E-Mail To Admin User.</button>
		        	<button type="button" class="btn btn-success btn-fw noBoradi mr-2" onclick="SaveVendorDetails();">Update</button>
		        	<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
		      	</div>
		    </div>
		</form>
		<?php		
	}
	public function GetVendorCloneDetailsFromCompany(Request $request)
	{
		$Data 				= $request->all();
		$VendorID  			= $Data['VendorID'];
		$VendorDetails 		= $this->VMModel->VendorDetails($VendorID);
		$CompanyID 			= $VendorDetails->CompanyID;
		$CompanyDetails		= $this->VMModel->CompanyDetails($CompanyID);
		$CompanyTestMode 	= $CompanyDetails->CompanyTestMode;
		$CompanyStatus 		= $CompanyDetails->CompanyStatus;
		$CompanyList 		= $this->VMModel->CompanyListForEdit($CompanyTestMode,$CompanyStatus);

		$VendorDetails 		= $VendorDetails;
		$CampaignDropDown 	= $this->MasterConfigModel->getCampaignList();
		$CampaignID 		= $VendorDetails->CampaignID;
		$CampaignDetails 	= $this->CModel->GetCampaignDetails($CampaignID);

		$ShowInactive="checked";
		$ShowTest="";
		if($CompanyStatus==1)
		{
			$ShowInactive="";
		}
		if($CompanyTestMode==1)
		{
			$ShowTest="checked";
		}
		?>
		<form action="<?php echo route('admin.AddVendorDetails');?>" name="VendorUpdateForm" id="VendorUpdateForm" method="POST">
      	<input type="hidden" name="FormType" id="FormType" value="Clone">
      	<input type="hidden" name="From" id="From" value="Company">
      	<input type="hidden" name="Company" id="Company" value="<?php echo $VendorDetails->CompanyID; ?>">
      	<input type="hidden" name="_token" id="_token" value="<?php echo  csrf_token(); ?>">
      	<input type="hidden" name="CampaignTypeEdit" id="CampaignTypeEdit" value="<?php echo $CampaignDetails->CampaignType; ?>">
			<div class="row">
				<div class="col-md-12" id="MessageEdit">
				
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 form-group  tadmPadd labFontt">
				    <label for="exampleFormControlSelect1">Campaign:</label>
				    <select class="form-control fonT" id="CampaignEditVendor" name="Campaign" 
				        onchange="GetCampaignDetailsEdit(),HideErr('CompanyErr');">
				        <?php foreach($CampaignDropDown as $cdd){ ?>
				        <option value="<?php echo $cdd->CampaignID; ?>"
				          <?php if($cdd->CampaignID==$VendorDetails->CampaignID){echo "selected";}?>>
				          	<?php echo $cdd->CampaignName; ?></option>
				        <?php } ?>  
				    </select>
				    <span id="CampaignEditErr"></span>
				</div>
				<div class="col-md-3 form-group tadmPadd labFontt">
				  <label for="exampleInputEmail1">Admin Label:</label>
				    <input type="text" class="form-control txtPadd" 
				    value="<?php echo $VendorDetails->AdminLabel; ?>" id="AdminLabelEditVendor" name="AdminLabel"
				    onkeypress="HideErr('AdminLabelEditVendorErr');">
				    <span id="AdminLabelEditVendorErr"></span>
				</div>
				<div class="col-md-2 form-group tadmPadd btnCen">
				  <button type="button" id="CopyAdminLableEditVendor" onclick="CopyAdminLableEditVendorLabel();" class="btn btn-success noBoradi mr-2">&gt;&gt;</button>
				</div>
				<div class="col-md-3 form-group tadmPadd labFontt">
				  <label for="exampleInputEmail1">Partner Label:</label>
				    <input type="text" class="form-control txtPadd" 
				      value="<?php echo $VendorDetails->PartnerLabel; ?>" id="PartnerLabelEditVendor" name="PartnerLabel"
				      onkeypress="HideErr('PartnerLabelEditVendorErr');">
				    <span id="PartnerLabelEditVendorErr"></span>
				</div>
			</div> 
			<div class="row">
				<div class="col-md-3 form-group mb-0 tadmPadd labFontt">
				  <label for="PayoutCalculation">Payout Calculation:</label>
				    <select class="form-control fonT" id="PayoutCalculationEditVendor" onchange="PayoutCalculationChange();" name="PayoutCalculation" >
				      <option value="RevShare" <?php if($VendorDetails->PayoutCalculation=="RevShare"){ echo "selected"; } ?>>RevShare</option>
				      <option value="FixedPrice" <?php if($VendorDetails->PayoutCalculation=="FixedPrice"){ echo "selected"; } ?>>Fixed Price</option>
				      <option value="TierPrice" <?php if($VendorDetails->PayoutCalculation=="TierPrice"){ echo "selected"; } ?>>Tier Price</option>
				      <option value="MinPrice" <?php if($VendorDetails->PayoutCalculation=="MinPrice"){ echo "selected"; } ?>>Min Price</option>
				      <option value="BucketPrice" <?php if($VendorDetails->PayoutCalculation=="BucketPrice"){ echo "selected"; } ?>>Bucket Price</option>
				      <option value="BucketSystemBySubID" <?php if($VendorDetails->PayoutCalculation=="BucketSystemBySubID"){ echo "selected"; } ?>>Bucket System By SubID</option>
				    </select>
				</div>
				<?php 
				$PayoutCalculation = $VendorDetails->PayoutCalculation;
				if($PayoutCalculation=='FixedPrice')
				{ 
				     $PayoutCalculationFixedPrice="style='display:block'";
				     $PayoutCalculationTierPrice="style='display:none'";
				     $PayoutCalculationRevShare="style='display:none'";
				} 
				else if($PayoutCalculation=='RevShare')
				{
				     $PayoutCalculationRevShare="style='display:block'";
				     $PayoutCalculationTierPrice="style='display:none'";
				     $PayoutCalculationFixedPrice="style='display:none'";
				}
				 else if($PayoutCalculation=='TierPrice')
				{
				     $PayoutCalculationRevShare="style='display:none'";
				     $PayoutCalculationTierPrice="style='display:block'";
				     $PayoutCalculationFixedPrice="style='display:none'";
				}
				 else if($PayoutCalculation=='MinPrice' )
				{
				     $PayoutCalculationRevShare="style='display:block'";
				     $PayoutCalculationTierPrice="style='display:none'";
				     $PayoutCalculationFixedPrice="style='display:none'";
				}
				 else if($PayoutCalculation=='BucketSystemBySubID' || $PayoutCalculation=='BucketPrice')
				{
				     $PayoutCalculationRevShare="style='display:block'";
				     $PayoutCalculationTierPrice="style='display:none'";
				     $PayoutCalculationFixedPrice="style='display:block'";
				}
				?>
				<div class="col-md-2 form-group tadmPadd labFontt" <?php echo $PayoutCalculationRevShare ?>  
				    id="RevsharePercentInputEditVendor">
				  <label for="exampleInputEmail1">Revshare: </label>
				  <input type="text" class="form-control txtPadd" value="<?php echo $VendorDetails->RevsharePercent; ?>" 
				  placeholder="70" id="RevshareEditVendor" name="Revshare" onkeypress="HideErr('RevshareEditvErr');">
				  <span id="RevshareEditVendorErr"></span>
				</div>
				<div class="col-md-2 form-group tadmPadd labFontt" <?php echo $PayoutCalculationFixedPrice ?> 
				    id="FixedPriceInputEditVendor">
				  <label for="exampleInputEmail1">Fixed Price:</label>
				  <input type="text" class="form-control txtPadd" value="<?php echo $VendorDetails->FixedPrice; ?>" 
				  placeholder="70" id="FixedPriceEditVendor" name="FixedPrice" onkeypress="HideErr('FixedPriceEditVendorErr');">
				  <span id="FixedPriceEditVendorErr"></span>
				</div>
				<div class="col-md-5 form-group tadmPadd labFontt" <?php echo $PayoutCalculationTierPrice ?> 
				    id="TierPriceInputEditVendor">
				  <label for="exampleInputEmail1">Revshare Percent (for when Tier Payout is $0.00):</label>
				  <input type="text" class="form-control txtPadd" value="<?php echo $VendorDetails->RevsharePercent; ?>"
				   placeholder="70" id="TierPriceEditVendor" name="TierPrice" onkeypress="HideErr('TierPriceEditVendorErr');">
				  <span id="TierPriceEditVendorErr"></span>
				</div>
				<div class="col-md-2 form-group tadmPadd labFontt">
				  <label for="exampleInputEmail1">Daily Cap:</label>
				    <input type="text"  value="<?php echo $VendorDetails->DailyCap; ?>" 
				      id="DailyCapEditVendor" name="DailyCap" class="form-control txtPadd" placeholder="Daily Cap"
				      onkeypress="HideErr('DailyCapEditvErr');">
				    <span id="DailyCapEditVendorErr"></span>
				</div>
				<div class="col-md-2 form-group tadmPadd labFontt">
				  <label for="exampleInputEmail1">Total Cap:</label>
				    <input type="text" value="<?php echo $VendorDetails->TotalCap; ?>" 
				      id="TotalCapEditVendor" name="TotalCap" class="form-control txtPadd" placeholder="Total Cap"
				      onkeypress="HideErr('TotalCapEditVendorErr');">
				    <span id="TotalCapEditVendorErr"></span>
				</div>
			</div>
			<div class="row">				
				<?php if($VendorDetails->VendorStatus=='1'){ ?>
				<div class="col-md-2 form-group tadmPadd labFontt">
				  <label for="exampleInputEmail1">Vendor Status:</label>
				    <div class="onoffswitch8">
				      <input type="checkbox" name="VendorStatus" class="onoffswitch8-checkbox" 
				      id="VendorStatusEdit" checked="">
				      <label class="onoffswitch8-label" for="VendorStatusEdit">
				        <span class="onoffswitch8-inner"></span>
				        <span class="onoffswitch8-switch"></span>
				      </label>
				    </div>
				</div>
				<?php } else {?>
				<div class="col-md-2 form-group tadmPadd labFontt">
				  <label for="exampleInputEmail1">Vendor Status:</label>
				    <div class="onoffswitch8">
				      <input type="checkbox" name="VendorStatus" class="onoffswitch8-checkbox" 
				      id="VendorStatusEdit">
				      <label class="onoffswitch8-label" for="VendorStatusEdit">
				        <span class="onoffswitch8-inner"></span>
				        <span class="onoffswitch8-switch"></span>
				      </label>
				    </div>
				</div>
				<?php }?>
				<?php if($VendorDetails->VendorTestMode=='1'){ ?>
				<div class="col-md-2 form-group tadmPadd labFontt">
				  <label for="exampleInputEmail1">Vendor Mode?</label>
				    <div class="onoffswitch9">
				      <input type="checkbox" name="VendorTestMode" class="onoffswitch9-checkbox"
				         id="VendorTestMode" checked="">
				      <label class="onoffswitch9-label" for="VendorTestMode">
				        <span class="onoffswitch9-inner"></span>
				        <span class="onoffswitch9-switch"></span>
				      </label>
				    </div>
				</div>
				<?php } else {?>
				<div class="col-md-2 form-group tadmPadd labFontt">
				  <label for="exampleInputEmail1">Vendor Mode?</label>
				    <div class="onoffswitch9">
				      <input type="checkbox" name="VendorTestMode" class="onoffswitch9-checkbox" 
				        id="VendorTestMode" >
				      <label class="onoffswitch9-label" for="VendorTestMode">
				        <span class="onoffswitch9-inner"></span>
				        <span class="onoffswitch9-switch"></span>
				      </label>
				    </div>
				</div>
				<?php }?>				
				
				<?php 
				if($CampaignDetails->CampaignType=="DirectPost"){                           
				  $MaxTimeOutInput = "style='display:block;'";
				  $MaxPingTimeOutInput = "style='display:none;'";
				  $MaxPostTimeOutInput = "style='display:none;'";
				}else if($CampaignDetails->CampaignType=="PingPost"){
				  $MaxTimeOutInput = "style='display:none;'";
				  $MaxPingTimeOutInput = "style='display:block;'";
				  $MaxPostTimeOutInput = "style='display:block;'";
				}
				?>
				 <div class="col-md-2 form-group tadmPadd labFontt" id="MaxTimeOutInputEditVendor" <?php echo $MaxTimeOutInput; ?>>
				  <label for="exampleInputEmail1">Max Time Out:</label>
				    <input type="text" class="form-control txtPadd" placeholder="0" 
				    id="MaxTimeOutEditVendor" name="MaxTimeOut" value="<?php echo $VendorDetails->MaxTimeOut; ?>" onkeypress="HideErr('MaxTimeOutEditVendorErr');">
				    <span id="MaxTimeOutEditVendorErr"></span>
				</div>
				<div class="col-md-2 form-group tadmPadd labFontt" id="MaxPingTimeOutInputEditVendor" <?php echo $MaxPingTimeOutInput; ?>>
				  <label for="exampleInputEmail1">Max Ping Time Out:</label>
				    <input type="text" class="form-control txtPadd" placeholder="0" 
				    id="MaxPingTimeOutEditVendor" name="MaxPingTimeOut" value="<?php echo $VendorDetails->MaxPingTimeOut; ?>" onkeypress="HideErr('MaxPingTimeOutEditVendorErr');">
				    <span id="MaxPingTimeOutEditVendorErr"></span>
				</div>
				<div class="col-md-2 form-group tadmPadd labFontt" id="MaxPostTimeOutInputEditVendor" <?php echo $MaxPostTimeOutInput; ?>>
				  <label for="exampleInputEmail1">Max Post Time Out:</label>
				    <input type="text" class="form-control txtPadd" placeholder="0" 
				    id="MaxPostTimeOutEditVendor" name="MaxPostTimeOut" value="<?php echo $VendorDetails->MaxPostTimeOut; ?>" onkeypress="HideErr('MaxPostTimeOutEditVendorErr');">
				    <span id="MaxPostTimeOutEditVendorErr"></span>
				</div> 
				<?php if($VendorDetails->TrackingPixelStatus=='1'){ ?>
				<div class="col-md-2 form-group tadmPadd labFontt">
				  <label for="exampleInputEmail1">Tracking Pixel Status?</label>
				    <div class="onoffswitch10">
				      <input type="checkbox" name="TrackingPixelStatus" class="onoffswitch10-checkbox"
				       id="TrackingPixelStatusEditVendor" checked="">
				      <label class="onoffswitch10-label" for="TrackingPixelStatusEditVendor">
				        <span class="onoffswitch10-inner"></span>
				        <span class="onoffswitch10-switch"></span>
				      </label>
				    </div>
				</div>
				<?php } else {?>
				<div class="col-md-2 form-group tadmPadd labFontt">
				  <label for="exampleInputEmail1">Tracking Pixel Status?</label>
				    <div class="onoffswitch10">
				      <input type="checkbox" name="TrackingPixelStatus" class="onoffswitch10-checkbox"
				       id="TrackingPixelStatusEditVendor" >
				      <label class="onoffswitch10-label" for="TrackingPixelStatusEditVendor">
				        <span class="onoffswitch10-inner"></span>
				        <span class="onoffswitch10-switch"></span>
				      </label>
				    </div>
				</div>
				<?php }?>
			</div>
			<div class="row">
				<div class="col-md-2 form-group  tadmPadd labFontt">
				  <label for="exampleFormControlSelect1">Tracking Pixel Type:</label>
				    <select class="form-control fonT" id="TrackingPixelType" name="TrackingPixelType">
				      <option value="Pixel" <?php if($VendorDetails->TrackingPixelType=='Pixel'){echo "selected";}?>>Pixel</option>
				      <option value="PostBack" <?php if($VendorDetails->TrackingPixelType=='PostBack'){echo "selected";}?>>Post Back</option>
				    </select>
				</div>
				<div class="col-md-2 form-group labFontt">
				  <label for="exampleFormControlSelect1">Insert Token:</label>
				    <select class="form-control" id="insert_Token_typeVendor" name="insert_Token_type" onchange="updateTextBoxEditVendor(this)">
				      <option value="aid">aid</option>
				      <option value="SubID">SubID</option>
				      <option value="TestMode">TestMode</option>
				      <option value="TransactionID">TransactionID</option>
				      <option value="VendorLeadPrice">VendorLeadPrice</option>
				  </select>
				</div>
				<div class="col-md-5 form-group labFontt">
				  <label for="exampleFormControlTextarea1">Tracking Pixel Code:</label>
				  <textarea class="form-control" id="TrackingPixelCodeEditVendor" name="TrackingPixelCode" rows="3"><?php echo $VendorDetails->TrackingPixelCode; ?></textarea>
				</div>				
			</div>
	        <div class="row">
	        	<div class="col-md-5 form-group  labFontt">
				  <label for="exampleFormControlTextarea1">Vendor Notes:</label>
				  <textarea class="form-control" id="VendorNotes" name="VendorNotes" rows="3"><?php echo $VendorDetails->VendorNotes; ?></textarea>
				</div>
	        </div>
		    <div class="row btnFlo">
		      	<div class="col-md-12 form-group mb-0 labFontt">
		       		<button type="button" class="btn btn-success btn-fw noBoradi mr-2" onclick="SaveVendorDetails();">Add</button>
		        	<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
		      	</div>
		    </div>
		</form>
		<?php		
	}
	/////////////////////Buyer List//////////////////////
	public function GetBuyerListFromCompany(Request $request)
	{
		$Data = $request->all();

        $NumOfRecords = $Data['numofrecords'];
        $page = $Data['page'];
        $cur_page = $page;

        $Limitpage = $page - 1;
        $Start = $Limitpage * $NumOfRecords;

        $ShowInactive 	= $Data['ShowInactive'];
        $ShowTest 	= $Data['ShowTest'];
        $CompanyID 	= $Data['Company'];
        $CampaignID = $Data['Campaign'];
        $BuyerName 	= $Data['BuyerName'];
        $Status 	= $Data['Status'];
        $BuyerMode 	= $Data['BuyerMode'];
      
        $Search 	= array('CampaignID' 	=> $CampaignID,
				            'CompanyID' 	=> $CompanyID,
				            'ShowInactive' 	=> $ShowInactive,
				            'ShowTest' 		=> $ShowTest,
				            'BuyerName' 	=> $BuyerName,
				            'Status' 		=> $Status,
				            'BuyerMode' 	=> $BuyerMode,
				            'Start' 		=> $Start,
				            'NumOfRecords' 	=> $NumOfRecords
				            );
        $GetBuyerList 	= $this->BMModel->GetBuyerList($Search);
       	
        $AllBuyerList 	= $GetBuyerList['Res'];
        $UserCount 		= $GetBuyerList['Count'];        

        $Pagination = $this->Pagination->Links($NumOfRecords, $UserCount, $page);
        $CampaignDetails = $this->CModel->GetCampaignDetails($CampaignID);
      	?>
		<div class="row mt-0">
          	<div class="table-responsive">
		  		<table class="table table-bordered tableFont txtL nTabl">
                    <thead class="thead-dark">
						<tr>
							<th scope="col" style="padding: 0.684rem; font-size: 0.8rem;">Company Name</th>
							<th scope="col" style="padding: 0.684rem; font-size: 0.8rem;">Campaign</th>
							<th scope="col" style="padding: 0.684rem; font-size: 0.8rem;">Buyer Name</th>
							<th scope="col" style="padding: 0.684rem; font-size: 0.8rem;">Payout Calculation</th>
							<th scope="col" style="padding: 0.684rem; font-size: 0.8rem;">Fixed Price</th>
							<th scope="col" style="padding: 0.684rem; font-size: 0.8rem;">Daily Cap</th>
							<th scope="col" style="padding: 0.684rem; font-size: 0.8rem;">Total Cap</th>
							<?php if($CampaignDetails->CampaignType=="DirectPost"){ ?>
							<th scope="col" style="padding: 0.684rem; font-size: 0.8rem;">Max Time Out</th>
							<th scope="col" style="padding: 0.684rem; font-size: 0.8rem;">Min Time Out</th>
							<?php } else { ?>
							<th scope="col" style="padding: 0.684rem; font-size: 0.8rem;">Max Ping Time Out</th>
							<th scope="col" style="padding: 0.684rem; font-size: 0.8rem;">Max Post Time Out</th>
							<?php }?>
							
							<th scope="col" style="padding: 0.684rem; font-size: 0.8rem;">Status</th>
							<th scope="col" style="padding: 0.684rem; font-size: 0.8rem;">Buyer Mode</th>
							<th scope="col" style="padding: 0.684rem; font-size: 0.8rem;">Action</th>
						</tr>
                    </thead>
			    	<tbody>
				    <?php 
				     	if(!empty($AllBuyerList ))
				     	{
				     		foreach($AllBuyerList as $p)
				     		{
							     ?>				       
						        <tr >
						           	<td><?php echo $p->CompanyName; ?>	</td>
						           	<td><?php echo $p->CampaignName; ?>	</td>
						           	<td><?php echo $p->AdminLabel.' (BID: '.$p->BuyerID.')'; ?>	</td>
						           	<td><?php echo $p->PayoutCalculation; ?></td>
						           	<td><?php echo $p->FixedPrice; ?></td>
						           	<td><?php echo $p->DailyCap; ?></td>
						           	<td><?php echo $p->TotalCap; ?></td>
						           	<?php if($CampaignDetails->CampaignType=="DirectPost"){ ?>
									<td><?php echo $p->MaxTimeOut; ?></td>
									<td><?php echo $p->MinTimeOut; ?></td>
									<?php } else { ?>
									<td><?php echo $p->MaxPingTimeOut; ?></td>
									<td><?php echo $p->MaxPostTimeOut; ?></td>
									<?php }?>
						           	<td id="StatusHtmlBuyer_<?php echo $p->BuyerID; ?>">
						           		<?php 
						           		if ($p->BuyerStatus == '1') 
						           		{
		                                   ?>
		                                   	<div class="custom-control custom-checkbox noCush greenCh">
				                              <input type="checkbox"  checked="true"
				                              	class="custom-control-input noCush-input green-input" 
				                              	id="customCheck<?php echo $p->BuyerID; ?>" 
				                              	onclick="ChangeBuyerStatus(<?php echo $p->BuyerID; ?>,0);">
				                              <label class="custom-control-label noCush-label green-label mb-3" 
				                              	for="customCheck<?php echo $p->BuyerID; ?>"  
				                              	></label>
				                            </div> 
		                                   <?php
		                                } 
		                                else if ($p->BuyerStatus == '0') 
		                                {
		                                    ?>
		                                   	<div class="custom-control custom-checkbox noCush">
				                              <input type="checkbox" 
				                              class="custom-control-input noCush-input" 
				                              id="customCheck<?php echo $p->BuyerID; ?>" 
				                              onclick="ChangeBuyerStatus(<?php echo $p->BuyerID; ?>,1);" >
				                              <label class="custom-control-label noCush-label mb-3" 
				                              	for="customCheck<?php echo $p->BuyerID; ?>" 
				                              	></label>
				                            </div> 
		                                   <?php
		                                }
						           		?>
						           	</td>
						           	<td id="ModeHtmlBuyer_<?php echo $p->BuyerID; ?>">
						           		<?php 
						           		if ($p->BuyerTestMode == '1') 
						           		{
		                                   ?>
		                                   	<a href="javascript:void(0);" 
		                                   		class="btn btn-danger cfonnT cbtnPadd noBoradi"
		                                   		onclick="ChangeBuyerTestMode('<?php echo $p->BuyerID; ?>',0);">
												<i class="fas fa-check-square sqFont" aria-hidden="true"></i> 
												TestMode
											</a> 
		                                   <?php
		                                } 
		                                else if ($p->BuyerTestMode == '0') 
		                                {
		                                    ?>
		                                   	<a href="javascript:void(0);" 
		                                   		class="btn btn-success cfonnT cbtnPadd noBoradi"
		                                   		onclick="ChangeBuyerTestMode('<?php echo $p->BuyerID; ?>',1);">
												<i class="fas fa-square sqFont" aria-hidden="true"></i> 
												LiveMode
											</a>  
		                                   <?php
		                                }
						           		?>
						           	</td>
						           	<td>
						           		<ul class="btnList">
                                          <li>
                                            <a href="javascript:void(0);" onclick="GetBuyerDetailsFromCompany('<?php echo $p->BuyerID; ?>');">
				                              	<button type="button" title="edit" 
	                                        		class="btn btn-info cfonnT cbtnPadd noBoradi">
	                                        		<i class="fas fa-pencil-alt fnnPadd"></i>Edit
                                        		</button>
				                            </a>
                                          </li>
                                          <li>
                                            <a href="javascript:void(0);" onclick="GetBuyerCloneDetailsFromCompany('<?php echo $p->BuyerID; ?>');">
				                              	<button type="button" title="add" 
                                            		class="btn btn-info cfonnT cbtnPadd noBoradi bgClrred">
                                            		<i class="fas fa-plus fnnPadd"></i>Clone
                                            	</button>
				                            </a>
                                          </li>
                                        </ul>
						           	</td>
						        </tr>
			     				<?php 
			     			}
			     		} 
			     		else 
			     		{
			     			?>
					     	<tr >
					           <td colspan="10" style="color: #c94542;text-align:center;">Record Not Found.</td>
					           </td>
					        </tr>
			     			<?php 
			     		} 
			     	?>
			     	</tbody>
				  </table>
				</div>
			</div>
		<?php if(!empty($AllBuyerList ))
		{ ?>
			<div class="row mt-3">			
				<div class="col-md-6 col-sm-6 col-12">
					<div class="dataTables_length mCenter" id="dataTable_length">
						<label>Records/Pages
							<select id="numofrecordsBuyer" onchange="SearchVendorList();" name="dataTable_length" aria-controls="dataTable" class="custom-select custom-select-sm form-control form-control-sm">
							<option value="10" <?php if($NumOfRecords=='10'){ echo 'selected'; } ?> >10</option>
							<option value="20" <?php if($NumOfRecords=='20'){ echo 'selected'; } ?> >20</option>
							<option value="50" <?php if($NumOfRecords=='50'){ echo 'selected'; } ?> >50</option>
							<option value="100" <?php if($NumOfRecords=='100'){ echo 'selected'; } ?> >100</option>
							<option value="150" <?php if($NumOfRecords=='150'){ echo 'selected'; } ?> >150</option>
							<option value="200" <?php if($NumOfRecords=='200'){ echo 'selected'; } ?> >200</option>
							</select>
						</label>
					</div>
				</div>
				<div class="col-md-6 col-sm-6 col-12 grid-margin stretch-card">
					<div class="card noBg border-0">
					    <nav>
					      <?php echo $Pagination; ?>
					    </nav>
					</div>
				</div>
			</div>
      	<?php
      	}
      	else
      	{
      		?>
      		<input type="hidden" name="numofrecords" id="numofrecordsBuyer" value='10'>
      		<?php
      	}
       	exit();
	}

	public function GetBuyerDetailsFromCompany(Request $request)
	{
		$Data   = $request->all();	

		$BuyerID 			= $Data['BuyerID'];
		$BuyerDetails 		= $this->BMModel->BuyerDetails($BuyerID);
		$BuyerDetails 		= $BuyerDetails;
		$CampaignDropDown 	= $this->MasterConfigModel->getCampaignList();
		$CampaignID 		= $BuyerDetails->CampaignID;
		$CampaignDetails 	= $this->CModel->GetCampaignDetails($CampaignID);

		$CompanyID 			= $BuyerDetails->CompanyID;
		$CompanyDetails		= $this->VMModel->CompanyDetails($CompanyID);

		$CompanyTestMode 	= $CompanyDetails->CompanyTestMode;
		$CompanyStatus 		= $CompanyDetails->CompanyStatus;

		$CompanyList		= $this->VMModel->CompanyListForEdit($CompanyTestMode,$CompanyStatus);

		
		$CompanyTestMode 	= $CompanyDetails->CompanyTestMode;
		$CompanyStatus 		= $CompanyDetails->CompanyStatus;
		$ShowInactive="checked";
		$ShowTest="";
		if($CompanyStatus==1)
		{
			$ShowInactive="";
		}
		if($CompanyTestMode==1)
		{
			$ShowTest="checked";
		}
		?>
		<form action="<?php echo route('admin.SaveBuyerDetails'); ?>" name="BuyerUpdateForm" 
	      id="BuyerUpdateForm" method="POST" enctype="multipart/form-data">
	      <input type="hidden" name="BuyerID" id="BuyerID" value="<?php echo $BuyerDetails->BuyerID; ?>">
	      <input type="hidden" name="Company" id="CompanyID" value="<?php echo $BuyerDetails->CompanyID; ?>">
	      <input type="hidden" name="_token" id="_token" value="<?php echo csrf_token(); ?>">	      
	      <input type="hidden" name="FormType" id="FormTypeEditBuyer" value="Update">
	      <input type="hidden" name="From" id="From" value="Company">
	      <div class="row">
	        <div class="col-md-2 form-group mb-0 tadmPadd labFontt">
	          <label for="exampleFormControlSelect1">Buyer ID:</label>
	          <input type="text" disabled class="form-control txtPadd" 
	          value="<?php echo $BuyerDetails->BuyerID; ?>" placeholder="">
	        </div>                          
	        <div class="col-md-3 form-group mb-0 tadmPadd labFontt">
	            <label for="exampleFormControlSelect1">Campaign:</label>
	            <select class="form-control fonT" id="CampaignEditBuyer" name="CampaignID"  
	                onchange="GetCampaignDetailsEditBuyer(),HideErr('CompanyEditErr');">
	                <?php foreach($CampaignDropDown as $cdd) {?>
	                <option value="<?php echo $cdd->CampaignID; ?>"
	                  <?php if($cdd->CampaignID==$BuyerDetails->CampaignID){echo "selected";}?>
	                  ><?php echo $cdd->CampaignName; ?></option>
	                <?php }?>
	            </select>
	            <span id="CampaignEditErr"></span>
	        </div>
	        <div class="col-md-2 form-group tadmPadd labFontt">
	          <label for="exampleInputEmail1">Admin Label:</label>
	            <input type="text" class="form-control txtPadd" 
	            value="<?php echo $BuyerDetails->AdminLabel; ?>" id="AdminLabelEditBuyer" name="AdminLabel"
	            onkeypress="HideErr('AdminLabelEditBuyerErr');">
	             <span id="AdminLabelEditBuyerErr"></span>
	        </div>
	        <div class="col-md-1 form-group tadmPadd btnCen">
	          <button type="button" id="CopyAdminLable" onclick="CopyAdminLableEditBuyer();" class="btn btn-success noBoradi mr-2">&gt;&gt;</button>
	        </div>
	        <div class="col-md-2 form-group tadmPadd labFontt">
	          <label for="exampleInputEmail1">Partner Label:</label>
	            <input type="text" class="form-control txtPadd" 
	              value="<?php echo $BuyerDetails->PartnerLabel; ?>" id="PartnerLabelEditBuyer" name="PartnerLabel"
	              onkeypress="HideErr('PartnerLabelEditBuyerErr');">
	               <span id="PartnerLabelEditBuyerErr"></span>
	        </div>
	       </div>
	      <div class="row">	
	      	<?php 
	      	$BuyerTierID = "style='display:none;'";
	        if($CampaignDetails->CampaignType=="DirectPost"){                           
	          $BuyerTierID = "style='display:block;'";
	        }
	        ?>
	        <div class="col-md-2 form-group tadmPadd labFontt" id="BuyerTierIDInputEditBuyer" <?php echo $BuyerTierID; ?>>
	            <label for="exampleInputEmail1">Buyer Tier ID:</label>
	              <input type="text" onkeypress="HideErr('BuyerTierIDEditBuyerErr');" value="<?php echo $BuyerDetails->BuyerTierID; ?>" 
	                id="BuyerTierIDEditBuyer" name="BuyerTierID" class="form-control txtPadd" placeholder="Buyer Tier ID">
	              <span id="BuyerTierIDEditBuyerErr"></span>
	          </div>        
	        <div class="col-md-2 form-group tadmPadd labFontt">
	          <label for="exampleInputEmail1">Integration File Name:</label>
	            <input type="text"value="<?php echo $BuyerDetails->IntegrationFileName; ?>" 
	              id="IntegrationFileNameEditBuyer" name="IntegrationFileName" class="form-control txtPadd" placeholder="Integration File Name"
	              onkeypress="HideErr('IntegrationFileNameEditBuyerErr');">
	            <span id="IntegrationFileNameEditBuyerErr"></span>
	        </div>
	        <div class="col-md-2 form-group mb-0 tadmPadd labFontt">
	          <label for="PayoutCalculation">Payout Calculation:</label>
	            <select class="form-control fonT" id="PayoutCalculationEditBuyer" name="PayoutCalculation" 
	            	onchange="PayoutCalculationEditBuyerFunction();">
	              <option value="RevShare" <?php if($BuyerDetails->PayoutCalculation=="RevShare"){ echo "selected"; } ?>>RevShare</option>
	              <option value="FixedPrice" <?php if($BuyerDetails->PayoutCalculation=="FixedPrice"){ echo "selected"; } ?>>Fixed Price</option>
	            </select>
	        </div>
	        <?php 
	        $PayoutCalculation = $BuyerDetails->PayoutCalculation;
	        if($PayoutCalculation=='FixedPrice')
	        { 
	             $PayoutCalculationFixedPrice="style='display:block'";
	             $PayoutCalculationRevShare="style='display:none'";
	        } 
	        else if($PayoutCalculation=='RevShare')
	        {
	             $PayoutCalculationRevShare="style='display:block'";
	             $PayoutCalculationFixedPrice="style='display:none'";
	        }
	        ?>
	        <div class="col-md-2 form-group tadmPadd labFontt" id="RevshareInputEditBuyer" <?php echo $PayoutCalculationRevShare ?>  
	            id="RevsharePercentInput">
	          <label for="exampleInputEmail1">RevShare: </label>
	          <input type="text" class="form-control txtPadd" value="<?php echo $BuyerDetails->FixedPrice; ?>" 
	          placeholder="70" id="RevshareEditBuyer" name="Revshare" onkeypress="HideErr('RevshareEditBuyerErr');">
	          <span id="RevshareEditBuyerErr"></span>
	        </div>
	        <div class="col-md-2 form-group tadmPadd labFontt" id="FixedPriceInputEditBuyer" <?php echo $PayoutCalculationFixedPrice ?> 
	            id="FixedPriceInput">
	          <label for="exampleInputEmail1">Fixed Price:</label>
	          <input type="text" class="form-control txtPadd" value="<?php echo $BuyerDetails->FixedPrice; ?>" 
	          placeholder="70" id="FixedPriceEditBuyer" name="FixedPrice" onkeypress="HideErr('FixedPriceEditBuyerErr');">
	          <span id="FixedPriceEditBuyerErr"></span>
	        </div>
	        <div class="col-md-2 form-group tadmPadd labFontt">
	          <label for="exampleInputEmail1">Daily Cap:</label>
	            <input type="text" value="<?php echo $BuyerDetails->DailyCap; ?>" 
	              id="DailyCapEditBuyer" name="DailyCap" class="form-control txtPadd" placeholder="Daily Cap"
	              onkeypress="HideErr('DailyCapEditBuyerErr');">
	            <span id="DailyCapEditBuyerErr"></span>
	        </div>
	        <div class="col-md-2 form-group tadmPadd labFontt">
	          <label for="exampleInputEmail1">Total Cap:</label>
	            <input type="text" value="<?php echo $BuyerDetails->TotalCap; ?>" 
	              id="TotalCapEditBuyer" name="TotalCap" class="form-control txtPadd" placeholder="Total Cap"
	              onkeypress="HideErr('TotalCapEditBuyerErr');">
	            <span id="TotalCapEditBuyerErr"></span>
	        </div>
	      </div>                         
	      <div class="row">        
	        
	        <?php if($BuyerDetails->BuyerStatus=='1'){ ?>
	        <div class="col-md-2 form-group tadmPadd labFontt">
	          <label for="exampleInputEmail1">Buyer Status:</label>
	            <div class="onoffswitch8">
	              <input type="checkbox" name="BuyerStatus" class="onoffswitch8-checkbox" 
	              id="BuyerStatusEditEditBuyer" checked="">
	              <label class="onoffswitch8-label" for="BuyerStatusEditEditBuyer">
	                <span class="onoffswitch8-inner"></span>
	                <span class="onoffswitch8-switch"></span>
	              </label>
	            </div>
	        </div>
	        <?php } else {?>
	        <div class="col-md-2 form-group tadmPadd labFontt">
	          <label for="exampleInputEmail1">Buyer Status:</label>
	            <div class="onoffswitch8">
	              <input type="checkbox" name="BuyerStatus" class="onoffswitch8-checkbox" 
	              id="BuyerStatusEditEditBuyer">
	              <label class="onoffswitch8-label" for="BuyerStatusEditEditBuyer">
	                <span class="onoffswitch8-inner"></span>
	                <span class="onoffswitch8-switch"></span>
	              </label>
	            </div>
	        </div>
	        <?php }?>
	        <?php if($BuyerDetails->BuyerTestMode=='1'){ ?>
	        <div class="col-md-2 form-group tadmPadd labFontt">
	          <label for="exampleInputEmail1">Buyer Mode?</label>
	            <div class="onoffswitch9">
	              <input type="checkbox" name="BuyerTestMode" class="onoffswitch9-checkbox"
	                 id="BuyerTestModeEditBuyer" >
	              <label class="onoffswitch9-label" for="BuyerTestModeEditBuyer">
	                <span class="onoffswitch9-inner"></span>
	                <span class="onoffswitch9-switch"></span>
	              </label>
	            </div>
	        </div>
	        <?php } else {?>
	        <div class="col-md-2 form-group tadmPadd labFontt">
	          <label for="exampleInputEmail1">Buyer Mode?</label>
	            <div class="onoffswitch9">
	              <input type="checkbox" name="BuyerTestMode" class="onoffswitch9-checkbox" 
	                id="BuyerTestModeEditBuyer" checked="">
	              <label class="onoffswitch9-label" for="BuyerTestModeEditBuyer">
	                <span class="onoffswitch9-inner"></span>
	                <span class="onoffswitch9-switch"></span>
	              </label>
	            </div>
	        </div>
	        <?php }?>
	         <?php if($BuyerDetails->EWSStatus=='1'){ ?>
	        <div class="col-md-2 form-group tadmPadd labFontt">
              <label for="exampleInputEmail1">EWS Status</label>
                <div class="onoffswitch8">
                  <input type="checkbox" name="EWSStatus" class="onoffswitch8-checkbox"
                     id="EWSStatusAdd" checked="">
                  <label class="onoffswitch8-label" for="EWSStatusAdd">
                    <span class="onoffswitch8-inner"></span>
                    <span class="onoffswitch8-switch"></span>
                  </label>
                </div>
            </div>
	        <?php } else {?>
	        <div class="col-md-2 form-group tadmPadd labFontt">
              <label for="exampleInputEmail1">EWS Status</label>
                <div class="onoffswitch8">
                  <input type="checkbox" name="EWSStatus" class="onoffswitch8-checkbox"
                     id="EWSStatusAdd" >
                  <label class="onoffswitch8-label" for="EWSStatusAdd">
                    <span class="onoffswitch8-inner"></span>
                    <span class="onoffswitch8-switch"></span>
                  </label>
                </div>
            </div>
	        <?php }?>
	        <?php 
	        if($CampaignDetails->CampaignType=="DirectPost"){                           
	          $MaxTimeOutInput = "style='display:block;'";
	          $MinTimeOutInput = "style='display:block;'";
	          $MaxPingTimeOutInput = "style='display:none;'";
	          $MaxPostTimeOutInput = "style='display:none;'";
	          $MaxPingsPerMinuteInput = "style='display:none;'";
	        }else if($CampaignDetails->CampaignType=="PingPost"){
	          $MaxTimeOutInput = "style='display:none;'";
	          $MinTimeOutInput = "style='display:none;'";
	          $MaxPingTimeOutInput = "style='display:block;'";
	          $MaxPostTimeOutInput = "style='display:block;'";
	          $MaxPingsPerMinuteInput = "style='display:block;'";
	        }
	        ?>
	        <div class="col-md-2 form-group tadmPadd labFontt" id="MaxTimeOutInputEditBuyer" <?php echo $MaxTimeOutInput; ?>>
	          <label for="exampleInputEmail1">Max Time Out:</label>
	            <input type="text" class="form-control txtPadd" placeholder="0" 
	            id="MaxTimeOutEditBuyer" name="MaxTimeOut" value="<?php echo $BuyerDetails->MaxTimeOut; ?>" onkeypress="HideErr('MaxTimeOutEditBuyerErr');">
	            <span id="MaxTimeOutEditBuyerErr"></span>
	        </div>
	        <div class="col-md-2 form-group tadmPadd labFontt" id="MinTimeOutInputEditBuyer" <?php echo $MinTimeOutInput; ?>>
	          <label for="exampleInputEmail1">Min Time Out:</label>
	            <input type="text" class="form-control txtPadd" placeholder="0" 
	            id="MinTimeOutEditBuyer" name="MinTimeOut" value="<?php echo $BuyerDetails->MinTimeOut; ?>" onkeypress="HideErr('MinTimeOutEditBuyerErr');">
	            <span id="MinTimeOutEditBuyerErr"></span>
	        </div>

	        <div class="col-md-2 form-group tadmPadd labFontt" id="MaxPingTimeOutInputEditBuyer" <?php echo $MaxPingTimeOutInput; ?>>
	          <label for="exampleInputEmail1">Max Ping Time Out:</label>
	            <input type="text" class="form-control txtPadd" placeholder="0" 
	            id="MaxPingTimeOutEditBuyer" name="MaxPingTimeOut" value="<?php echo $BuyerDetails->MaxPingTimeOut; ?>" onkeypress="HideErr('MaxPingTimeOutEditBuyerErr');">
	            <span id="MaxPingTimeOutEditBuyerErr"></span>
	        </div>
	        <div class="col-md-2 form-group tadmPadd labFontt" id="MaxPostTimeOutInputEditBuyer" <?php echo $MaxPostTimeOutInput; ?>>
	          <label for="exampleInputEmail1">Max Post Time Out:</label>
	            <input type="text" class="form-control txtPadd" placeholder="0" 
	            id="MaxPostTimeOutEditBuyer" name="MaxPostTimeOut" value="<?php echo $BuyerDetails->MaxPostTimeOut; ?>" onkeypress="HideErr('MaxPostTimeOutEditBuyerErr');">
	            <span id="MaxPostTimeOutEditBuyerErr"></span>
	        </div>
	        <div class="col-md-2 form-group tadmPadd labFontt" id="MaxPingsPerMinuteInputEditBuyer" <?php echo $MaxPingsPerMinuteInput; ?>>
	          <label for="exampleInputEmail1">Max Pings Per Minute:</label>
	            <input type="text" class="form-control txtPadd" placeholder="0" 
	            id="MaxPingsPerMinuteEditBuyer" name="MaxPingsPerMinute" value="<?php echo $BuyerDetails->MaxPingsPerMinute; ?>" onkeypress="HideErr('MaxPingsPerMinuteEditBuyerErr');">
	            <span id="MaxPingsPerMinuteEditBuyerErr"></span>
	        </div>
	      </div>                             
	      <div class="row">
	      	<?php 
	        if($CampaignDetails->CampaignType=="DirectPost"){                           
	          $PingTestURLInputEditBuyer = "style='display:none;'";
	          $PingLiveURLInputEditBuyer = "style='display:none;'";
	          $PostTestURLInputEditBuyer = "style='display:none;'";
	          $PostLiveURLInputEditBuyer = "style='display:none;'";
	          $DirectPostTestURLInputEditBuyer = "style='display:block;'";
	          $DirectPostLiveURLInputEditBuyer = "style='display:block;'";
	        }else if($CampaignDetails->CampaignType=="PingPost"){
	          $PingTestURLInputEditBuyer = "style='display:block;'";
	          $PingLiveURLInputEditBuyer = "style='display:block;'";
	          $PostTestURLInputEditBuyer = "style='display:block;'";
	          $PostLiveURLInputEditBuyer = "style='display:block;'";
	          $DirectPostTestURLInputEditBuyer = "style='display:none;'";
	          $DirectPostLiveURLInputEditBuyer = "style='display:none;'";
	        }
	        ?>
	          <div class="col-md-6 form-group tadmPadd labFontt" id="PingTestURLInputEditBuyer" <?php echo $PingTestURLInputEditBuyer; ?>>
	            <label for="exampleInputEmail1">Ping Test URL:</label>
	              <input type="text" class="form-control txtPadd" 
	              value="<?php echo $BuyerDetails->PingTestURL; ?>" id="PingTestURLEditBuyer" name="PingTestURL"
	              onkeypress="HideErr('PingTestURLEditBuyerErr');">
	               <span id="PingTestURLEditBuyerErr"></span>
	          </div>
	          <div class="col-md-6 form-group tadmPadd labFontt" id="PingLiveURLInputEditBuyer" <?php echo $PingLiveURLInputEditBuyer; ?>>
	            <label for="exampleInputEmail1">Ping Live URL:</label>
	              <input type="text" class="form-control txtPadd" 
	                value="<?php echo $BuyerDetails->PingLiveURL; ?>" id="PingLiveURLEditBuyer" name="PingLiveURL"
	                onkeypress="HideErr('PingLiveURLEditBuyerErr');">
	                 <span id="PingLiveURLEditBuyerErr"></span>
	          </div>
	          <div class="col-md-6 form-group tadmPadd labFontt" id="PostTestURLInputEditBuyer" <?php echo $PostTestURLInputEditBuyer; ?>>
	            <label for="exampleInputEmail1">Post Test URL:</label>
	              <input type="text" class="form-control txtPadd" 
	              value="<?php echo $BuyerDetails->PostTestURL; ?>" id="PostTestURLEditBuyer" name="PostTestURL"
	              onkeypress="HideErr('PostTestURLEditBuyerErr');">
	               <span id="PostTestURLEditBuyerErr"></span>
	          </div>
	          <div class="col-md-6 form-group tadmPadd labFontt" id="PostLiveURLInputEditBuyer" <?php echo $PostLiveURLInputEditBuyer; ?>>
	            <label for="exampleInputEmail1">Post Live URL:</label>
	              <input type="text" class="form-control txtPadd" 
	                value="<?php echo $BuyerDetails->PostLiveURL; ?>" id="PostLiveURLEditBuyer" name="PostLiveURL"
	                onkeypress="HideErr('PostLiveURLEditBuyerErr');">
	                 <span id="PostLiveURLEditBuyerErr"></span>
	          </div>

	          <div class="col-md-6 form-group tadmPadd labFontt" id="DirectPostTestURLInputEditBuyer" <?php echo $DirectPostTestURLInputEditBuyer; ?>>
	            <label for="exampleInputEmail1">DirectPost Test URL:</label>
	              <input type="text" class="form-control txtPadd" 
	              value="<?php echo $BuyerDetails->PostTestURL; ?>" id="DirectPostTestURLEditBuyer" name="DirectPostTestURL"
	              onkeypress="HideErr('DirectPostTestURLEditBuyerErr');">
	               <span id="DirectPostTestURLEditBuyerErr"></span>
	          </div>
	          <div class="col-md-6 form-group tadmPadd labFontt" id="DirectPostLiveURLInputEditBuyer" <?php echo $DirectPostLiveURLInputEditBuyer; ?>>
	            <label for="exampleInputEmail1">DirectPost Live URL:</label>
	              <input type="text" class="form-control txtPadd" 
	                value="<?php echo $BuyerDetails->PostLiveURL; ?>" id="DirectPostLiveURLEditBuyer" name="DirectPostLiveURL"
	                onkeypress="HideErr('DirectPostLiveURLEditBuyerErr');">
	                 <span id="DirectPostLiveURLEditBuyerErr"></span>
	          </div>

	      </div>
	      <div class="row">
	        <div class="col-md-2 form-group mb-0 tadmPadd labFontt">
	            <label for="exampleFormControlSelect1">Request Method:</label>
	            <select class="form-control fonT" id="RequestMethodEditBuyer" name="RequestMethod" >
	                <option value="1" <?php if($BuyerDetails->RequestMethod=='1'){echo "selected";}?>>POST</option>
	                <option value="2" <?php if($BuyerDetails->RequestMethod=='2'){echo "selected";}?>>GET</option>
	                <option value="3" <?php if($BuyerDetails->RequestMethod=='3'){echo "selected";}?>>XmlinPost</option>
	                <option value="4" <?php if($BuyerDetails->RequestMethod=='4'){echo "selected";}?>>XmlPost</option>
	                <option value="5" <?php if($BuyerDetails->RequestMethod=='5'){echo "selected";}?>>InlineXML</option>
	            </select>
	            <span id="RequestMethodEditBuyerErr"></span>
	        </div>
	        <div class="col-md-2 form-group mb-0 tadmPadd labFontt">
	            <label for="exampleFormControlSelect1">Request Header:</label>
	            <select class="form-control fonT" id="RequestHeaderEditBuyer" name="RequestHeader" >
	                <option value="1" <?php if($BuyerDetails->RequestHeader=='1'){echo "selected";}?>>Content-Type: application/x-www-form-urlencoded</option>
	                <option value="2" <?php if($BuyerDetails->RequestHeader=='2'){echo "selected";}?>>Content-Type:text/xml; charset=utf-8 </option>
	                <option value="3" <?php if($BuyerDetails->RequestHeader=='3'){echo "selected";}?>>Content-type: application/xml </option>
	                <option value="4" <?php if($BuyerDetails->RequestHeader=='4'){echo "selected";}?>>Content-type: application/json</option>
	            </select>
	            <span id="RequestHeaderEditBuyerErr"></span>
	        </div>
	        <div class="col-md-2 form-group tadmPadd labFontt">
	          <label for="exampleInputEmail1">Parameter1:</label>
	            <input type="text" class="form-control txtPadd" 
	            value="<?php echo $BuyerDetails->Parameter1; ?>" id="Parameter1" name="Parameter1"
	            onkeypress="HideErr('Parameter1Err');">
	             <span id="Parameter1Err"></span>
	        </div>
	        <div class="col-md-2 form-group tadmPadd labFontt">
	          <label for="exampleInputEmail1">Parameter2:</label>
	            <input type="text" class="form-control txtPadd" 
	            value="<?php echo $BuyerDetails->Parameter2; ?>" id="Parameter2EditBuyer" name="Parameter2"
	            onkeypress="HideErr('Parameter2EditBuyerErr');">
	             <span id="Parameter2EditBuyerErr"></span>
	        </div>
	        <div class="col-md-2 form-group tadmPadd labFontt">
	          <label for="exampleInputEmail1">Parameter3:</label>
	            <input type="text" class="form-control txtPadd" 
	            value="<?php echo $BuyerDetails->Parameter3; ?>" id="Parameter3EditBuyer" name="Parameter3"
	            onkeypress="HideErr('Parameter3EditBuyerErr');">
	             <span id="Parameter3EditBuyerErr"></span>
	        </div>
	        <div class="col-md-2 form-group tadmPadd labFontt">
	          <label for="exampleInputEmail1">Parameter4:</label>
	            <input type="text" class="form-control txtPadd" 
	            value="<?php echo $BuyerDetails->Parameter4; ?>" id="Parameter4EditBuyer" name="Parameter4"
	            onkeypress="HideErr('Parameter4EditBuyerErr');">
	             <span id="Parameter4EditBuyerErr"></span>
	        </div>
	      </div>
	       <div class="row">
	        <div class="col-md-4 form-group labFontt tablabFontt">
	          <label>File: (You Can Select Multiple Files)</label>
	          <div class="custom-file">
	            <input type="file" multiple class="custom-file-input" id="filesEditBuyer" name="files[]" multiple>
	            <label class="custom-file-label fileLabel" for="files" id="fileLabelEditBuyer">No File Chosen</label>
	          </div>
	        </div>
	        <?php
	        if($BuyerDetails->UploadedFiles!='')
	        {
	          ?>
	          <div class="col-md-4 form-group mb-0 labFontt">
	            <span id="UploadFileMessage"></span>
	          <label for="exampleFormControlTextarea1">Uploaded Files:</label>
	            <table class="table">
	              <thead>
	                <tr>
	                  <th>Uploaded File</th>
	                  <th>Action</th>
	                </tr>
	              </thead>
	              <tbody>
	              <?php
	              $UploadedFiles = json_decode($BuyerDetails->UploadedFiles);
	              foreach($UploadedFiles as $upf)
	              {
	                ?>
	                <tr id="UploadedFileTR_<?php echo $upf->id; ?>">
	                  <td>
	                    <a href="<?php echo  asset('uploads').'/'.$upf->filename; ?>" 
	                      target="_blank"><?php echo $upf->filename; ?></a>
	                  </td>
	                  <td>
	                    <a class="btn btn-danger btn-sm" href="javascript:void(0);" 
	                        onclick="DeleteBuyerUploadedFile('<?php echo $BuyerDetails->BuyerID; ?>','<?php echo $upf->id; ?>');">
	                        Delete
	                    </a>
	                    <input type="hidden" name="OldUploadedFiles[]" value="<?php echo $upf->filename; ?>">
	                  </td>
	                </tr>
	                <?php
	              }
	              ?>
	              </tbody>
	            </table>
	          </div>
	          <?php
	        } 
	        ?>
	        <div class="col-md-4 form-group mb-0 labFontt">
	          <label for="exampleFormControlTextarea1">Buyer Notes:</label>
	          <textarea class="form-control" id="BuyerNotesBuyer" name="BuyerNotes" rows="3"><?php echo  $BuyerDetails->BuyerNotes; ?></textarea>
	        </div>
	      	</div>
	      	<div class="row btnFlo">
	            <div class="col-md-12 form-group mb-0 labFontt">
	              <button type="button" class="btn btn-success btn-fw noBoradi mr-2" onclick="SaveBuyerDetails();">Update</button>
	              <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
	            </div>
	        </div>
	    </form>
		<?php
	}
	public function GetBuyerCloneDetailsFromCompany(Request $request)
	{
		$Data   = $request->all();	

		$BuyerID 			= $Data['BuyerID'];
		$BuyerDetails 		= $this->BMModel->BuyerDetails($BuyerID);
		$BuyerDetails 		= $BuyerDetails;
		$CampaignDropDown 	= $this->MasterConfigModel->getCampaignList();
		$CampaignID 		= $BuyerDetails->CampaignID;
		$CampaignDetails 	= $this->CModel->GetCampaignDetails($CampaignID);

		$CompanyID 			= $BuyerDetails->CompanyID;
		$CompanyDetails		= $this->VMModel->CompanyDetails($CompanyID);

		$CompanyTestMode 	= $CompanyDetails->CompanyTestMode;
		$CompanyStatus 		= $CompanyDetails->CompanyStatus;

		$CompanyList		= $this->VMModel->CompanyListForEdit($CompanyTestMode,$CompanyStatus);

		
		$CompanyTestMode 	= $CompanyDetails->CompanyTestMode;
		$CompanyStatus 		= $CompanyDetails->CompanyStatus;
		$ShowInactive="checked";
		$ShowTest="";
		if($CompanyStatus==1)
		{
			$ShowInactive="";
		}
		if($CompanyTestMode==1)
		{
			$ShowTest="checked";
		}
		?>
		<form action="<?php echo route('admin.AddBuyerDetails'); ?>" name="BuyerUpdateForm" 
	      id="BuyerUpdateForm" method="POST" enctype="multipart/form-data">
	      <input type="hidden" name="Company" id="CompanyID" value="<?php echo $BuyerDetails->CompanyID; ?>">
	      <input type="hidden" name="_token" id="_token" value="<?php echo csrf_token(); ?>">	      
	      <input type="hidden" name="FormType" id="FormTypeEditBuyer" value="Clone">
	      <input type="hidden" name="From" id="From" value="Company">
	      <div class="row">
	        <div class="col-md-2 form-group mb-0 tadmPadd labFontt">
	          <label for="exampleFormControlSelect1">Buyer ID:</label>
	          <input type="text" disabled class="form-control txtPadd" 
	          value="<?php echo $BuyerDetails->BuyerID; ?>" placeholder="">
	        </div>                          
	        <div class="col-md-3 form-group mb-0 tadmPadd labFontt">
	            <label for="exampleFormControlSelect1">Campaign:</label>
	            <select class="form-control fonT" id="CampaignEditBuyer" name="Campaign"  
	                onchange="GetCampaignDetailsEditBuyer(),HideErr('CompanyEditErr');">
	                <?php foreach($CampaignDropDown as $cdd) {?>
	                <option value="<?php echo $cdd->CampaignID; ?>"
	                  <?php if($cdd->CampaignID==$BuyerDetails->CampaignID){echo "selected";}?>
	                  ><?php echo $cdd->CampaignName; ?></option>
	                <?php }?>
	            </select>
	            <span id="CampaignEditErr"></span>
	        </div>
	        <div class="col-md-2 form-group tadmPadd labFontt">
	          <label for="exampleInputEmail1">Admin Label:</label>
	            <input type="text" class="form-control txtPadd" 
	            value="<?php echo $BuyerDetails->AdminLabel; ?>" id="AdminLabelEditBuyer" name="AdminLabel"
	            onkeypress="HideErr('AdminLabelEditBuyerErr');">
	             <span id="AdminLabelEditBuyerErr"></span>
	        </div>
	        <div class="col-md-1 form-group tadmPadd btnCen">
	          <button type="button" id="CopyAdminLable" onclick="CopyAdminLableEditBuyer();" class="btn btn-success noBoradi mr-2">&gt;&gt;</button>
	        </div>
	        <div class="col-md-2 form-group tadmPadd labFontt">
	          <label for="exampleInputEmail1">Partner Label:</label>
	            <input type="text" class="form-control txtPadd" 
	              value="<?php echo $BuyerDetails->PartnerLabel; ?>" id="PartnerLabelEditBuyer" name="PartnerLabel"
	              onkeypress="HideErr('PartnerLabelEditBuyerErr');">
	               <span id="PartnerLabelEditBuyerErr"></span>
	        </div>
	       </div>
	      <div class="row">	
	      	<?php 
	      	$BuyerTierID = "style='display:none;'";
	        if($CampaignDetails->CampaignType=="DirectPost"){                           
	          $BuyerTierID = "style='display:block;'";
	        }
	        ?>
	        <div class="col-md-2 form-group tadmPadd labFontt" id="BuyerTierIDInputEditBuyer" <?php echo $BuyerTierID; ?>>
	            <label for="exampleInputEmail1">Buyer Tier ID:</label>
	              <input type="text" onkeypress="HideErr('BuyerTierIDEditBuyerErr');" value="<?php echo $BuyerDetails->BuyerTierID; ?>" 
	                id="BuyerTierIDEditBuyer" name="BuyerTierID" class="form-control txtPadd" placeholder="Buyer Tier ID">
	              <span id="BuyerTierIDEditBuyerErr"></span>
	          </div>        
	        <div class="col-md-2 form-group tadmPadd labFontt">
	          <label for="exampleInputEmail1">Integration File Name:</label>
	            <input type="text"value="<?php echo $BuyerDetails->IntegrationFileName; ?>" 
	              id="IntegrationFileNameEditBuyer" name="IntegrationFileName" class="form-control txtPadd" placeholder="Integration File Name"
	              onkeypress="HideErr('IntegrationFileNameEditBuyerErr');">
	            <span id="IntegrationFileNameEditBuyerErr"></span>
	        </div>
	        <div class="col-md-2 form-group mb-0 tadmPadd labFontt">
	          <label for="PayoutCalculation">Payout Calculation:</label>
	            <select class="form-control fonT" id="PayoutCalculationEditBuyer" name="PayoutCalculation" 
	            	onchange="PayoutCalculationEditBuyerFunction();">
	              <option value="RevShare" <?php if($BuyerDetails->PayoutCalculation=="RevShare"){ echo "selected"; } ?>>RevShare</option>
	              <option value="FixedPrice" <?php if($BuyerDetails->PayoutCalculation=="FixedPrice"){ echo "selected"; } ?>>Fixed Price</option>
	            </select>
	        </div>
	        <?php 
	        $PayoutCalculation = $BuyerDetails->PayoutCalculation;
	        if($PayoutCalculation=='FixedPrice')
	        { 
	             $PayoutCalculationFixedPrice="style='display:block'";
	             $PayoutCalculationRevShare="style='display:none'";
	        } 
	        else if($PayoutCalculation=='RevShare')
	        {
	             $PayoutCalculationRevShare="style='display:block'";
	             $PayoutCalculationFixedPrice="style='display:none'";
	        }
	        ?>
	        <div class="col-md-2 form-group tadmPadd labFontt" id="RevshareInputEditBuyer" <?php echo $PayoutCalculationRevShare ?>  
	            id="RevsharePercentInput">
	          <label for="exampleInputEmail1">RevShare: </label>
	          <input type="text" class="form-control txtPadd" value="<?php echo $BuyerDetails->FixedPrice; ?>" 
	          placeholder="70" id="RevshareEditBuyer" name="Revshare" onkeypress="HideErr('RevshareEditBuyerErr');">
	          <span id="RevshareEditBuyerErr"></span>
	        </div>
	        <div class="col-md-2 form-group tadmPadd labFontt" id="FixedPriceInputEditBuyer" <?php echo $PayoutCalculationFixedPrice ?> 
	            id="FixedPriceInput">
	          <label for="exampleInputEmail1">Fixed Price:</label>
	          <input type="text" class="form-control txtPadd" value="<?php echo $BuyerDetails->FixedPrice; ?>" 
	          placeholder="70" id="FixedPriceEditBuyer" name="FixedPrice" onkeypress="HideErr('FixedPriceEditBuyerErr');">
	          <span id="FixedPriceEditBuyerErr"></span>
	        </div>
	        <div class="col-md-2 form-group tadmPadd labFontt">
	          <label for="exampleInputEmail1">Daily Cap:</label>
	            <input type="text" value="<?php echo $BuyerDetails->DailyCap; ?>" 
	              id="DailyCapEditBuyer" name="DailyCap" class="form-control txtPadd" placeholder="Daily Cap"
	              onkeypress="HideErr('DailyCapEditBuyerErr');">
	            <span id="DailyCapEditBuyerErr"></span>
	        </div>
	        <div class="col-md-2 form-group tadmPadd labFontt">
	          <label for="exampleInputEmail1">Total Cap:</label>
	            <input type="text" value="<?php echo $BuyerDetails->TotalCap; ?>" 
	              id="TotalCapEditBuyer" name="TotalCap" class="form-control txtPadd" placeholder="Total Cap"
	              onkeypress="HideErr('TotalCapEditBuyerErr');">
	            <span id="TotalCapEditBuyerErr"></span>
	        </div>
	      </div>                         
	      <div class="row">        
	        
	        <?php if($BuyerDetails->BuyerStatus=='1'){ ?>
	        <div class="col-md-2 form-group tadmPadd labFontt">
	          <label for="exampleInputEmail1">Buyer Status:</label>
	            <div class="onoffswitch8">
	              <input type="checkbox" name="BuyerStatus" class="onoffswitch8-checkbox" 
	              id="BuyerStatusEditEditBuyer" checked="">
	              <label class="onoffswitch8-label" for="BuyerStatusEditEditBuyer">
	                <span class="onoffswitch8-inner"></span>
	                <span class="onoffswitch8-switch"></span>
	              </label>
	            </div>
	        </div>
	        <?php } else {?>
	        <div class="col-md-2 form-group tadmPadd labFontt">
	          <label for="exampleInputEmail1">Buyer Status:</label>
	            <div class="onoffswitch8">
	              <input type="checkbox" name="BuyerStatus" class="onoffswitch8-checkbox" 
	              id="BuyerStatusEditEditBuyer">
	              <label class="onoffswitch8-label" for="BuyerStatusEditEditBuyer">
	                <span class="onoffswitch8-inner"></span>
	                <span class="onoffswitch8-switch"></span>
	              </label>
	            </div>
	        </div>
	        <?php }?>
	        <?php if($BuyerDetails->BuyerTestMode=='1'){ ?>
	        <div class="col-md-2 form-group tadmPadd labFontt">
	          <label for="exampleInputEmail1">Buyer Mode?</label>
	            <div class="onoffswitch9">
	              <input type="checkbox" name="BuyerTestMode" class="onoffswitch9-checkbox"
	                 id="BuyerTestModeEditBuyer" >
	              <label class="onoffswitch9-label" for="BuyerTestModeEditBuyer">
	                <span class="onoffswitch9-inner"></span>
	                <span class="onoffswitch9-switch"></span>
	              </label>
	            </div>
	        </div>
	        <?php } else {?>
	        <div class="col-md-2 form-group tadmPadd labFontt">
	          <label for="exampleInputEmail1">Buyer Mode?</label>
	            <div class="onoffswitch9">
	              <input type="checkbox" name="BuyerTestMode" class="onoffswitch9-checkbox" 
	                id="BuyerTestModeEditBuyer" checked="">
	              <label class="onoffswitch9-label" for="BuyerTestModeEditBuyer">
	                <span class="onoffswitch9-inner"></span>
	                <span class="onoffswitch9-switch"></span>
	              </label>
	            </div>
	        </div>
	        <?php }?>
	         <?php if($BuyerDetails->EWSStatus=='1'){ ?>
	        <div class="col-md-2 form-group tadmPadd labFontt">
              <label for="exampleInputEmail1">EWS Status</label>
                <div class="onoffswitch8">
                  <input type="checkbox" name="EWSStatus" class="onoffswitch8-checkbox"
                     id="EWSStatusAdd" checked="">
                  <label class="onoffswitch8-label" for="EWSStatusAdd">
                    <span class="onoffswitch8-inner"></span>
                    <span class="onoffswitch8-switch"></span>
                  </label>
                </div>
            </div>
	        <?php } else {?>
	        <div class="col-md-2 form-group tadmPadd labFontt">
              <label for="exampleInputEmail1">EWS Status</label>
                <div class="onoffswitch8">
                  <input type="checkbox" name="EWSStatus" class="onoffswitch8-checkbox"
                     id="EWSStatusAdd" >
                  <label class="onoffswitch8-label" for="EWSStatusAdd">
                    <span class="onoffswitch8-inner"></span>
                    <span class="onoffswitch8-switch"></span>
                  </label>
                </div>
            </div>
	        <?php }?>
	        <?php 
	        if($CampaignDetails->CampaignType=="DirectPost"){                           
	          $MaxTimeOutInput = "style='display:block;'";
	          $MinTimeOutInput = "style='display:block;'";
	          $MaxPingTimeOutInput = "style='display:none;'";
	          $MaxPostTimeOutInput = "style='display:none;'";
	          $MaxPingsPerMinuteInput = "style='display:none;'";
	        }else if($CampaignDetails->CampaignType=="PingPost"){
	          $MaxTimeOutInput = "style='display:none;'";
	          $MinTimeOutInput = "style='display:none;'";
	          $MaxPingTimeOutInput = "style='display:block;'";
	          $MaxPostTimeOutInput = "style='display:block;'";
	          $MaxPingsPerMinuteInput = "style='display:block;'";
	        }
	        ?>
	        <div class="col-md-2 form-group tadmPadd labFontt" id="MaxTimeOutInputEditBuyer" <?php echo $MaxTimeOutInput; ?>>
	          <label for="exampleInputEmail1">Max Time Out:</label>
	            <input type="text" class="form-control txtPadd" placeholder="0" 
	            id="MaxTimeOutEditBuyer" name="MaxTimeOut" value="<?php echo $BuyerDetails->MaxTimeOut; ?>" onkeypress="HideErr('MaxTimeOutEditBuyerErr');">
	            <span id="MaxTimeOutEditBuyerErr"></span>
	        </div>
	        <div class="col-md-2 form-group tadmPadd labFontt" id="MinTimeOutInputEditBuyer" <?php echo $MinTimeOutInput; ?>>
	          <label for="exampleInputEmail1">Min Time Out:</label>
	            <input type="text" class="form-control txtPadd" placeholder="0" 
	            id="MinTimeOutEditBuyer" name="MinTimeOut" value="<?php echo $BuyerDetails->MinTimeOut; ?>" onkeypress="HideErr('MinTimeOutEditBuyerErr');">
	            <span id="MinTimeOutEditBuyerErr"></span>
	        </div>

	        <div class="col-md-2 form-group tadmPadd labFontt" id="MaxPingTimeOutInputEditBuyer" <?php echo $MaxPingTimeOutInput; ?>>
	          <label for="exampleInputEmail1">Max Ping Time Out:</label>
	            <input type="text" class="form-control txtPadd" placeholder="0" 
	            id="MaxPingTimeOutEditBuyer" name="MaxPingTimeOut" value="<?php echo $BuyerDetails->MaxPingTimeOut; ?>" onkeypress="HideErr('MaxPingTimeOutEditBuyerErr');">
	            <span id="MaxPingTimeOutEditBuyerErr"></span>
	        </div>
	        <div class="col-md-2 form-group tadmPadd labFontt" id="MaxPostTimeOutInputEditBuyer" <?php echo $MaxPostTimeOutInput; ?>>
	          <label for="exampleInputEmail1">Max Post Time Out:</label>
	            <input type="text" class="form-control txtPadd" placeholder="0" 
	            id="MaxPostTimeOutEditBuyer" name="MaxPostTimeOut" value="<?php echo $BuyerDetails->MaxPostTimeOut; ?>" onkeypress="HideErr('MaxPostTimeOutEditBuyerErr');">
	            <span id="MaxPostTimeOutEditBuyerErr"></span>
	        </div>
	        <div class="col-md-2 form-group tadmPadd labFontt" id="MaxPingsPerMinuteInputEditBuyer" <?php echo $MaxPingsPerMinuteInput; ?>>
	          <label for="exampleInputEmail1">Max Pings Per Minute:</label>
	            <input type="text" class="form-control txtPadd" placeholder="0" 
	            id="MaxPingsPerMinuteEditBuyer" name="MaxPingsPerMinute" value="<?php echo $BuyerDetails->MaxPingsPerMinute; ?>" onkeypress="HideErr('MaxPingsPerMinuteEditBuyerErr');">
	            <span id="MaxPingsPerMinuteEditBuyerErr"></span>
	        </div>
	      </div>                             
	      <div class="row">
	      	<?php 
	        if($CampaignDetails->CampaignType=="DirectPost"){                           
	          $PingTestURLInputEditBuyer = "style='display:none;'";
	          $PingLiveURLInputEditBuyer = "style='display:none;'";
	          $PostTestURLInputEditBuyer = "style='display:none;'";
	          $PostLiveURLInputEditBuyer = "style='display:none;'";
	          $DirectPostTestURLInputEditBuyer = "style='display:block;'";
	          $DirectPostLiveURLInputEditBuyer = "style='display:block;'";
	        }else if($CampaignDetails->CampaignType=="PingPost"){
	          $PingTestURLInputEditBuyer = "style='display:block;'";
	          $PingLiveURLInputEditBuyer = "style='display:block;'";
	          $PostTestURLInputEditBuyer = "style='display:block;'";
	          $PostLiveURLInputEditBuyer = "style='display:block;'";
	          $DirectPostTestURLInputEditBuyer = "style='display:none;'";
	          $DirectPostLiveURLInputEditBuyer = "style='display:none;'";
	        }
	        ?>
	          <div class="col-md-6 form-group tadmPadd labFontt" id="PingTestURLInputEditBuyer" <?php echo $PingTestURLInputEditBuyer; ?>>
	            <label for="exampleInputEmail1">Ping Test URL:</label>
	              <input type="text" class="form-control txtPadd" 
	              value="<?php echo $BuyerDetails->PingTestURL; ?>" id="PingTestURLEditBuyer" name="PingTestURL"
	              onkeypress="HideErr('PingTestURLEditBuyerErr');">
	               <span id="PingTestURLEditBuyerErr"></span>
	          </div>
	          <div class="col-md-6 form-group tadmPadd labFontt" id="PingLiveURLInputEditBuyer" <?php echo $PingLiveURLInputEditBuyer; ?>>
	            <label for="exampleInputEmail1">Ping Live URL:</label>
	              <input type="text" class="form-control txtPadd" 
	                value="<?php echo $BuyerDetails->PingLiveURL; ?>" id="PingLiveURLEditBuyer" name="PingLiveURL"
	                onkeypress="HideErr('PingLiveURLEditBuyerErr');">
	                 <span id="PingLiveURLEditBuyerErr"></span>
	          </div>
	          <div class="col-md-6 form-group tadmPadd labFontt" id="PostTestURLInputEditBuyer" <?php echo $PostTestURLInputEditBuyer; ?>>
	            <label for="exampleInputEmail1">Post Test URL:</label>
	              <input type="text" class="form-control txtPadd" 
	              value="<?php echo $BuyerDetails->PostTestURL; ?>" id="PostTestURLEditBuyer" name="PostTestURL"
	              onkeypress="HideErr('PostTestURLEditBuyerErr');">
	               <span id="PostTestURLEditBuyerErr"></span>
	          </div>
	          <div class="col-md-6 form-group tadmPadd labFontt" id="PostLiveURLInputEditBuyer" <?php echo $PostLiveURLInputEditBuyer; ?>>
	            <label for="exampleInputEmail1">Post Live URL:</label>
	              <input type="text" class="form-control txtPadd" 
	                value="<?php echo $BuyerDetails->PostLiveURL; ?>" id="PostLiveURLEditBuyer" name="PostLiveURL"
	                onkeypress="HideErr('PostLiveURLEditBuyerErr');">
	                 <span id="PostLiveURLEditBuyerErr"></span>
	          </div>

	          <div class="col-md-6 form-group tadmPadd labFontt" id="DirectPostTestURLInputEditBuyer" <?php echo $DirectPostTestURLInputEditBuyer; ?>>
	            <label for="exampleInputEmail1">DirectPost Test URL:</label>
	              <input type="text" class="form-control txtPadd" 
	              value="<?php echo $BuyerDetails->PostTestURL; ?>" id="DirectPostTestURLEditBuyer" name="DirectPostTestURL"
	              onkeypress="HideErr('DirectPostTestURLEditBuyerErr');">
	               <span id="DirectPostTestURLEditBuyerErr"></span>
	          </div>
	          <div class="col-md-6 form-group tadmPadd labFontt" id="DirectPostLiveURLInputEditBuyer" <?php echo $DirectPostLiveURLInputEditBuyer; ?>>
	            <label for="exampleInputEmail1">DirectPost Live URL:</label>
	              <input type="text" class="form-control txtPadd" 
	                value="<?php echo $BuyerDetails->PostLiveURL; ?>" id="DirectPostLiveURLEditBuyer" name="DirectPostLiveURL"
	                onkeypress="HideErr('DirectPostLiveURLEditBuyerErr');">
	                 <span id="DirectPostLiveURLEditBuyerErr"></span>
	          </div>

	      </div>
	      <div class="row">
	        <div class="col-md-2 form-group mb-0 tadmPadd labFontt">
	            <label for="exampleFormControlSelect1">Request Method:</label>
	            <select class="form-control fonT" id="RequestMethodEditBuyer" name="RequestMethod" >
	                <option value="1" <?php if($BuyerDetails->RequestMethod=='1'){echo "selected";}?>>POST</option>
	                <option value="2" <?php if($BuyerDetails->RequestMethod=='2'){echo "selected";}?>>GET</option>
	                <option value="3" <?php if($BuyerDetails->RequestMethod=='3'){echo "selected";}?>>XmlinPost</option>
	                <option value="4" <?php if($BuyerDetails->RequestMethod=='4'){echo "selected";}?>>XmlPost</option>
	                <option value="5" <?php if($BuyerDetails->RequestMethod=='5'){echo "selected";}?>>InlineXML</option>
	            </select>
	            <span id="RequestMethodEditBuyerErr"></span>
	        </div>
	        <div class="col-md-2 form-group mb-0 tadmPadd labFontt">
	            <label for="exampleFormControlSelect1">Request Header:</label>
	            <select class="form-control fonT" id="RequestHeaderEditBuyer" name="RequestHeader" >
	                <option value="1" <?php if($BuyerDetails->RequestHeader=='1'){echo "selected";}?>>Content-Type: application/x-www-form-urlencoded</option>
	                <option value="2" <?php if($BuyerDetails->RequestHeader=='2'){echo "selected";}?>>Content-Type:text/xml; charset=utf-8 </option>
	                <option value="3" <?php if($BuyerDetails->RequestHeader=='3'){echo "selected";}?>>Content-type: application/xml </option>
	                <option value="4" <?php if($BuyerDetails->RequestHeader=='4'){echo "selected";}?>>Content-type: application/json</option>
	            </select>
	            <span id="RequestHeaderEditBuyerErr"></span>
	        </div>
	        <div class="col-md-2 form-group tadmPadd labFontt">
	          <label for="exampleInputEmail1">Parameter1:</label>
	            <input type="text" class="form-control txtPadd" 
	            value="<?php echo $BuyerDetails->Parameter1; ?>" id="Parameter1" name="Parameter1"
	            onkeypress="HideErr('Parameter1Err');">
	             <span id="Parameter1Err"></span>
	        </div>
	        <div class="col-md-2 form-group tadmPadd labFontt">
	          <label for="exampleInputEmail1">Parameter2:</label>
	            <input type="text" class="form-control txtPadd" 
	            value="<?php echo $BuyerDetails->Parameter2; ?>" id="Parameter2EditBuyer" name="Parameter2"
	            onkeypress="HideErr('Parameter2EditBuyerErr');">
	             <span id="Parameter2EditBuyerErr"></span>
	        </div>
	        <div class="col-md-2 form-group tadmPadd labFontt">
	          <label for="exampleInputEmail1">Parameter3:</label>
	            <input type="text" class="form-control txtPadd" 
	            value="<?php echo $BuyerDetails->Parameter3; ?>" id="Parameter3EditBuyer" name="Parameter3"
	            onkeypress="HideErr('Parameter3EditBuyerErr');">
	             <span id="Parameter3EditBuyerErr"></span>
	        </div>
	        <div class="col-md-2 form-group tadmPadd labFontt">
	          <label for="exampleInputEmail1">Parameter4:</label>
	            <input type="text" class="form-control txtPadd" 
	            value="<?php echo $BuyerDetails->Parameter4; ?>" id="Parameter4EditBuyer" name="Parameter4"
	            onkeypress="HideErr('Parameter4EditBuyerErr');">
	             <span id="Parameter4EditBuyerErr"></span>
	        </div>
	      </div>
	       <div class="row">
	        <div class="col-md-4 form-group labFontt tablabFontt">
	          <label>File: (You Can Select Multiple Files)</label>
	          <div class="custom-file">
	            <input type="file" multiple class="custom-file-input" id="filesEditBuyer" name="files[]" multiple>
	            <label class="custom-file-label fileLabel" for="files" id="fileLabelEditBuyer">No File Chosen</label>
	          </div>
	        </div>
	        <?php
	        if($BuyerDetails->UploadedFiles!='')
	        {
	          ?>
	          <div class="col-md-4 form-group mb-0 labFontt">
	            <span id="UploadFileMessage"></span>
	          <label for="exampleFormControlTextarea1">Uploaded Files:</label>
	            <table class="table">
	              <thead>
	                <tr>
	                  <th>Uploaded File</th>
	                  <th>Action</th>
	                </tr>
	              </thead>
	              <tbody>
	              <?php
	              $UploadedFiles = json_decode($BuyerDetails->UploadedFiles);
	              foreach($UploadedFiles as $upf)
	              {
	                ?>
	                <tr id="UploadedFileTR_<?php echo $upf->id; ?>">
	                  <td>
	                    <a href="<?php echo  asset('uploads').'/'.$upf->filename; ?>" 
	                      target="_blank"><?php echo $upf->filename; ?></a>
	                  </td>
	                  <td>
	                    <a class="btn btn-danger btn-sm" href="javascript:void(0);" 
	                        onclick="DeleteBuyerUploadedFile('<?php echo $BuyerDetails->BuyerID; ?>','<?php echo $upf->id; ?>');">
	                        Delete
	                    </a>
	                    <input type="hidden" name="OldUploadedFiles[]" value="<?php echo $upf->filename; ?>">
	                  </td>
	                </tr>
	                <?php
	              }
	              ?>
	              </tbody>
	            </table>
	          </div>
	          <?php
	        } 
	        ?>
	        <div class="col-md-4 form-group mb-0 labFontt">
	          <label for="exampleFormControlTextarea1">Buyer Notes:</label>
	          <textarea class="form-control" id="BuyerNotesBuyer" name="BuyerNotes" rows="3"><?php echo  $BuyerDetails->BuyerNotes; ?></textarea>
	        </div>
	      	</div>
	      	<div class="row btnFlo">
	            <div class="col-md-12 form-group mb-0 labFontt">
	              <button type="button" class="btn btn-success btn-fw noBoradi mr-2" onclick="SaveBuyerDetails();">Update</button>
	              <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
	            </div>
	        </div>
	    </form>
		<?php
	}
}
