<?php
namespace App\Http\Controllers;
use Route;
use Mail;
use Auth, Hash;
use Validator;
use DB;
use Session;
use Redirect;
use Excel;
use PDF;
use Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Pagination;
use App\Http\Models\ErrorLogModel;
use App\Http\Models\MasterConfigModel;

class ErrorLogController extends Controller
{
	public function __construct(Request $request)
	{	
		$this->Pagination 			= new Pagination();
		$this->MasterConfigModel 	= new MasterConfigModel();
		$this->ErrorLogModel 		= new ErrorLogModel();
	}

	public function ErrorLogView(Request $request)
	{
		$Data['Menu']   = "Report";
        $Data['Title']  = "Admin | ErrorLog";
	    return view('ErrorLogView')->with($Data);
	}
	public function GetErrorLogList(Request $request)
	{
		$Data = $request->all();

        $NumOfRecords = $Data['numofrecords'];
        
        $page = $Data['page'];
        $cur_page = $page;

        $Limitpage = $page - 1;
        $Start = $Limitpage * $NumOfRecords;

        
        $Search = array('Start' 		=> $Start,
			            'NumOfRecords' 	=> $NumOfRecords);
        $AllErrorLog 		= $this->ErrorLogModel->GetErrorLogList($Search);
       
        $AllErrorLogData 	= $AllErrorLog['Res'];
        $ErrorLogCount 		= $AllErrorLog['Count'];

        $Pagination = $this->Pagination->Links($NumOfRecords, $ErrorLogCount, $page);
        ?>
		<div id="dataTable_wrapper" class="dataTables_wrapper dt-bootstrap4">
			<div class="row">
				<div class="heading text-center">
				  <h2>Error Logs</h2>
				</div>
				<div class="table-responsive">
				  <table class="table table-bordered tableFont" id="dataTable" width="100%">
				     <thead>
				        <tr role="row">
				            <th style="color: #c94142;" scope="col">Row</th>
				            <th style="color: #c94142;" scope="col">Date-Time</th>
				            <th style="color: #c94142;" scope="col">File</th>
				            <th style="color: #c94142;" scope="col">Line</th>
				            <th style="color: #c94142;" scope="col">Erroe</th>
				        </tr>
				    </thead>
				    <tbody>
					    <?php 
					     	if(!empty($AllErrorLogData ))
					     	{
					     		$i = $Start + 1;
					     		foreach($AllErrorLogData as $vld)
					     		{
								     ?>				       
							        <tr>
							           	<td ><?=$i?></td>
							           	<td><?php echo $vld->DateTime; ?></td>
							           	<td><?php echo $vld->File; ?></td>
							           	<td><?php echo $vld->Line; ?></td>
							           	<td><?php echo $vld->Error; ?></td>							           	
							        </tr>
				     				<?php 
				     				$i++;
				     			}
				     		} 
				     		else 
				     		{
				     			?>
						     	<tr >
						           <td colspan="4" style="color: #05aefd;">Record Not Found.</td>
						           </td>
						        </tr>
				     			<?php 
				     		} 
				     	?>
				     </tbody>
				  </table>
				</div>
			</div>
		<?php if(!empty($AllErrorLogData ))
		{ ?>
			<div class="row mt-3">			
				<div class="col-md-6 col-sm-6 col-6 noPaddd">
					<div class="dataTables_length mCenter" id="dataTable_length">
						<label>Records/Pages
							<select id="numofrecords" onchange="GetErrorLogList(1);" name="dataTable_length" aria-controls="dataTable" class="custom-select custom-select-sm form-control form-control-sm">
							<option value="10" <?php if($NumOfRecords=='10'){ echo 'selected'; } ?> >10</option>
							<option value="20" <?php if($NumOfRecords=='20'){ echo 'selected'; } ?> >20</option>
							<option value="50" <?php if($NumOfRecords=='50'){ echo 'selected'; } ?> >50</option>
							<option value="100" <?php if($NumOfRecords=='100'){ echo 'selected'; } ?> >100</option>
							<option value="150" <?php if($NumOfRecords=='150'){ echo 'selected'; } ?> >150</option>
							<option value="200" <?php if($NumOfRecords=='200'){ echo 'selected'; } ?> >200</option>
							</select>
						</label>
					</div>
				</div>
				<div class="col-md-6 col-sm-6 col-6">
					<div class="dataTables_length mCenter" id="dataTable_length" style="text-align: right;">
						<label>
							<?php echo $Pagination; ?>
						</label>
					</div>
				</div>
			</div>
      	<?php
      	}
      	else
      	{
      		?>
      		<input type="hidden" name="numofrecords" id="numofrecords" value='10'>
      		<?php
      	}
       	exit();
	}
}
