<?php
namespace App\Http\Controllers;
use Route;
use Mail;
use Auth, Hash;
use Validator;
use DB;
use Session;
use Redirect;
use Excel;
use PDF;
use Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Pagination;
use App\Http\Models\DashboardModel;
use App\Http\Models\MasterConfigModel;

class DashboardController extends Controller
{
	public function __construct(Request $request)
	{	
		$this->Pagination 			= new Pagination();
		$this->MasterConfigModel 	= new MasterConfigModel();
		$this->DashboardModel 		= new DashboardModel();
	}

	public function Dashboard(Request $request)
	{
		if(!$request->session()->has('user_id'))
		{
		    return redirect()->action('LoginController@Login');
		}
		$CampaignList = $this->MasterConfigModel->getCampaignList();
		$DashboardVendorReport = $this->DashboardModel->DashboardVendorReport();
		$DashboardBuyerReport = $this->DashboardModel->DashboardBuyerReport();
		$HasVendor = $this->MasterConfigModel->HasVendor();
		$HasBuyer = $this->MasterConfigModel->HasBuyer();
		
		$Data['CampaignList'] 			= $CampaignList;
		$Data['DashboardVendorReport'] 	= $DashboardVendorReport;
		$Data['DashboardBuyerReport'] 	= $DashboardBuyerReport;
		$Data['HasVendor'] 	= $HasVendor;
		$Data['HasBuyer'] 	= $HasBuyer;
		$Data['Menu'] 	= "Dashboard";
		$Data['Title'] 	= "Admin | Dashboard";
		return view('Dashboard')->with($Data);
	}
	/*public function DashboardTest(Request $request)
	{
		if(!$request->session()->has('user_id'))
		{
		    return redirect()->action('LoginController@Login');
		}
		$CampaignList = $this->MasterConfigModel->getCampaignList();
		$DashboardVendorReport = $this->DashboardModel->DashboardVendorReportTest();
		$DashboardBuyerReport = $this->DashboardModel->DashboardBuyerReportTest();
		$HasVendor = $this->MasterConfigModel->HasVendor();
		$HasBuyer = $this->MasterConfigModel->HasBuyer();
		
		$Data['CampaignList'] 			= $CampaignList;
		$Data['DashboardVendorReport'] 	= $DashboardVendorReport;
		$Data['DashboardBuyerReport'] 	= $DashboardBuyerReport;
		$Data['HasVendor'] 	= $HasVendor;
		$Data['HasBuyer'] 	= $HasBuyer;
		$Data['Menu'] 	= "Dashboard";
		$Data['Title'] 	= "Admin | Dashboard";
		return view('DashboardTest')->with($Data);
	}*/
	public function Profile(Request $request)
	{
		if(!$request->session()->has('user_id'))
		{
		    return redirect()->action('LoginController@Login');
		}
		$GetUserInfo = $this->DashboardModel->GetUserInfo();
		$Data['GetUserInfo'] = $GetUserInfo; 
		$Data['Menu'] 	= "Dashboard";
		$Data['Title'] 	= "Admin | Profile";
		return view('Profile')->with($Data);
	}  
	public function ProfileUpdate(Request $request)
	{
		$Data = $request->all();
		
		$ProfileUpdate = $this->DashboardModel->ProfileUpdate($Data);
		if($ProfileUpdate)
		{
			Session::put('user_firstname', '');
			Session::put('user_lastname', '');

			Session::put('user_firstname', $Data['UserFirstName']);
			Session::put('user_lastname',  $Data['UserLastName']);

			Session::flash('message', 'Profile has been updated successfully'); 
	        Session::flash('alert-class', 'alert-success'); 
	        return Redirect::route('admin.Profile');
		}
		else
		{
			Session::flash('message', 'Something Wrong Please try Again'); 
	        Session::flash('alert-class', 'alert-danger'); 
	        return Redirect::route('admin.Profile');
		}
        
	}    

	public function RedirectToLead(Request $request)
	{
		$Data = $request->all();
		$Campaign 	= str_replace("#"," ",$Data['Campaign']);

		$CampaignDetails = $this->DashboardModel->CampaignDetails($Campaign);

		$Status 	= $Data['Status'];
		$Where 		= $Data['Where'];
		Session::put('CampaignID', $CampaignDetails->CampaignID);
		Session::put('Status', $Status);
		Session::save();

		if($request->session()->has('CampaignID') && $request->session()->has('Status'))
		{
			$Response['Status'] = 1;
			if($Where=='Sold')
			{
				$Response['URL'] = 'sold-lead-report';
			}
			else if($Where=='Purchased')
			{
				$Response['URL'] = 'purchased-lead-report';	
			}
			else
			{
				$Response['URL'] = '';	
			}
		}
		else
		{
			$Response['Status'] = 0;
			$Response['URL'] = '';
		}
		echo json_encode($Response);
		exit();
	}
}
