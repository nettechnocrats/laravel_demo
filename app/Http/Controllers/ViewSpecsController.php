<?php
namespace App\Http\Controllers;
use Route;
use Mail;
use Auth, Hash;
use Validator;
use DB;
use Session;
use Excel;
use PDF;
use Response;
use Redirect;
use Illuminate\Http\Request;
use App\Http\Controllers\Pagination;
use App\Http\Models\ViewSpecsModel;
use App\Http\Models\MasterConfigModel;

class ViewSpecsController extends Controller
{
	public function __construct(Request $request)
	{		
		$this->Pagination 		= new Pagination();
		$this->MasterConfigModel= new MasterConfigModel();
		$this->VSModel 			= new ViewSpecsModel();
	}

	public function ViewSpecs(Request $request,$CampaignID,$v_id)
	{
		if(!$request->session()->has('user_id'))
		{
		    return redirect()->action('LoginController@Login');
		}
		
		


        $userRole = Session::get('user_role');
        $CompanyID = Session::get('user_companyID');
        $userID = Session::get('user_id');
        $v_id = base64_decode($v_id);

        $VendorDetails 		= $this->VSModel->VendorDetailsForSpecs($v_id);
        $CampaignDetails 	= $this->VSModel->GetCampaignDetails($CampaignID);
        $CampaignSpecsDetails 	= $this->VSModel->CampaignSpecsDetails($CampaignID);
		$CompanyDetails 	= $this->VSModel->GetCompanyDetails($VendorDetails->CompanyID);
		$UserID 			= $userID;
		$UserDetails 		= $this->VSModel->UserDetails($VendorDetails->UserID);
		$IPAddress 			= $request->getClientIp(true);
		$UserAgent 			= $request->server('HTTP_USER_AGENT');

		$Data['CampaignID']   	 = $CampaignID;
		$Data['VendorDetails']   = $VendorDetails;
		$Data['CampaignDetails'] = $CampaignDetails;
		$Data['CampaignSpecsDetails'] = $CampaignSpecsDetails;
		$Data['CompanyDetails']  = $CompanyDetails;
		$Data['UserDetails']   	 = $UserDetails;
		$Data['IPAddress']   	 = $IPAddress;
		$Data['UserAgent']   	 = $UserAgent;
		$Data['UserID']   	 	 = $UserID;
		$Data['Menu']   		 = "ViewSpecs";
        $Data['Title']  		 = "Admin | ViewSpecs";
       
		return view('ViewSpecs')->with($Data);
	}

	public function GeneratePDF(Request $request, $CampaignID) 
	{				
		/*$CampaignSpecsDetails = $this->VSModel->GetCampaignSpecsDetails($CampaignID);
    	
    	$FileName = $CampaignSpecsDetails->CampaignName;
    	$FileName = str_replace(" ","",$FileName);
    	$FileName = $FileName.'.pdf';   
  		
  		$Specs = $CampaignSpecsDetails->Specs;
    	$PDF = PDF::loadHTML($Specs);
		$PDF->download("$FileName");*/

	}

}
