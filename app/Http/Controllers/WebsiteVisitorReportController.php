<?php
namespace App\Http\Controllers;
use Route;
use Mail;
use Auth, Hash;
use Validator;
use DB;
use Session;
use Redirect;
use Excel;
use PDF;
use Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Pagination;
use App\Http\Models\WebsiteVisitorReportModel;
use App\Http\Models\MasterConfigModel;

class WebsiteVisitorReportController extends Controller
{
	public function __construct(Request $request)
	{	
		$this->Pagination 			= new Pagination();
		$this->MasterConfigModel 	= new MasterConfigModel();
		$this->WVRModel 			= new WebsiteVisitorReportModel();
	}

	public function WebsiteVisitorReportView(Request $request)
	{
		if(!$request->session()->has('user_id'))
		{
		    return redirect()->action('LoginController@Login');
		}
		$Data['VendorList'] = $this->WVRModel->GetVendorList();
		$Data['Menu']   = "Report";
    	$Data['Title']  = "Admin | WebsiteVisitorReport";
		return view('WebsiteVisitorReportView')->with($Data);
	}
	public function GetWebsiteVisitorReport(Request $request)
	{
		$Data = $request->all();
		$UserRole 	= Session::get('user_role');
		$CompanyID 	= Session::get('user_companyID');
		$UserID 	= Session::get('user_id');

        $NumOfRecords = $Data['numofrecords'];
        $page = $Data['page'];
        $cur_page = $page;

        $Limitpage = $page - 1;
        $Start = $Limitpage * $NumOfRecords;

		$StartDate 	= date("Y-m-d",strtotime($Data['StartDate'])).' 00:00:00';
		$EndDate 	= date("Y-m-d",strtotime($Data['EndDate'])).' 23:59:59';
        $VendorID 	= $Data['VendorID'];
        $SubID 		= $Data['SubID'];
        $IPAddress 	= $Data['IPAddress'];
        $URL 		= $Data['URL'];

        $Search = array('CompanyID' 	=> $CompanyID,
        				'URL' 			=> $URL,
        				'IPAddress' 	=> $IPAddress,
			            'StartDate' 	=> $StartDate,
			            'EndDate' 		=> $EndDate,
			            'VendorID' 		=> $VendorID,
			            'SubID' 		=> $SubID,
			            'Start' 		=> $Start,
			            'NumOfRecords' 	=> $NumOfRecords);

        $GetWebsiteVisitorReport = $this->WVRModel->GetWebsiteVisitorReport($Search);

        $AllVisitorData 	= $GetWebsiteVisitorReport['Res'];
        $VisitorCount 		= $GetWebsiteVisitorReport['Count'];
       
        $Pagination = $this->Pagination->Links($NumOfRecords, $VisitorCount, $page);

        if(!empty($AllVisitorData))
        {
        	?>
        	<div class="row">
                <div class="col-sm-12">
                    <div class="heading text-center">
                        <h2>Website Visitors List</h2>
                    </div>
                    <div class="table-responsive">                        
                        <table class="table table-bordered tableFont" id="dataTable">
                            <thead>
                                <tr role="row">
                                    <th scope="col">Date & Time</th>
                                    <th scope="col">URL</th>
                                    <th scope="col">IP Address</th>
                                    <th scope="col">User Agent</th>
                                    <th scope="col">Vendor </th>
                                    <th scope="col">Sub ID</th>
                                    <th scope="col">Referrer</th>
                                </tr>
                            </thead>
                            <tbody>
                            	<?php foreach($AllVisitorData as $avd){?>
                                <tr>
                                    <td><?php echo $avd->DateTime; ?></td>
                                    <td><a href="<?php echo $avd->URL; ?>" target="_blank"><?php echo $avd->URL; ?></a></td>
                                    <td><?php echo $avd->IPAddress; ?></td>
                                    <td><?php echo $avd->UserAgent; ?></td>
                                    <td><?php echo $avd->VendorID; ?></td>
                                    <td><?php echo $avd->SubID; ?></td>
                                    <td><a href="<?php echo $avd->Referrer; ?>" target="_blank"><?php echo $avd->Referrer; ?></a></td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
           	<div class="row mt-3">			
				<div class="col-md-6 col-sm-6 col-12">
					<div class="dataTables_length mCenter" id="dataTable_length">
						<label>Records/Pages
							<select id="numofrecords" onchange="SearchWebsiteVisitorReport();" name="dataTable_length" aria-controls="dataTable" class="custom-select custom-select-sm form-control form-control-sm">
							<option value="10" <?php if($NumOfRecords=='10'){ echo 'selected'; } ?> >10</option>
							<option value="20" <?php if($NumOfRecords=='20'){ echo 'selected'; } ?> >20</option>
							<option value="50" <?php if($NumOfRecords=='50'){ echo 'selected'; } ?> >50</option>
							<option value="100" <?php if($NumOfRecords=='100'){ echo 'selected'; } ?> >100</option>
							<option value="150" <?php if($NumOfRecords=='150'){ echo 'selected'; } ?> >150</option>
							<option value="200" <?php if($NumOfRecords=='200'){ echo 'selected'; } ?> >200</option>
							</select>
						</label>
					</div>
				</div>
				<div class="col-md-6 col-sm-6 col-12 grid-margin stretch-card">
					<div class="card noBg border-0">
					    <nav>
					      <?php echo $Pagination; ?>
					    </nav>
					</div>
				</div>
			</div>
        	<?php
        }
        else
        {
        	?>
        	<div class="row">
	            <div class="col-sm-12">
	                <div class="heading text-center">
	                    <h2>Website Visitors List</h2>
	                </div>
	                <div class="table-responsive">	                    
	                    <table class="table table-bordered tableFont" id="dataTable">
	                        <thead>
	                            <tr role="row">
	                                <th scope="col">Rows</th>
	                                <th scope="col">Date & Time</th>
	                                <th cope="col">Vendor Name</th>
	                                <th scope="col">Vendor ID</th>
	                                <th scope="col">Sub ID</th>
	                                <th scope="col">URL</th>
	                                <th scope="col">IP Address</th>
	                            </tr>
	                        </thead>
	                        <tbody>
	                            <tr>
	                                <td colspan="7" style="color:red;text-align:center;">No Record Found</td>
	                            </tr>
	                        </tbody>
	                    </table>
	                </div>
	            </div>
	        </div>
	        <input type="hidden" name="numofrecords" id="numofrecords" value='10'>
        	<?php
        }

	}    
	public function GetWebsiteVisitorChartAndSummary(Request $request)
	{
		$Data = $request->all();
		$UserRole 	= Session::get('user_role');
		$CompanyID 	= Session::get('user_companyID');
		$UserID 	= Session::get('user_id');
        

		$StartDate 	= date("Y-m-d",strtotime($Data['StartDate'])).' 00:00:00';
		$EndDate 	= date("Y-m-d",strtotime($Data['EndDate'])).' 23:59:59';
        $VendorID 	= $Data['VendorID'];
        $SubID 		= $Data['SubID'];
        $IPAddress 	= $Data['IPAddress'];
        $URL 		= $Data['URL'];

        $Search = array('CompanyID' 	=> $CompanyID,
        				'URL' 			=> $URL,
        				'IPAddress' 	=> $IPAddress,
			            'StartDate' 	=> $StartDate,
			            'EndDate' 		=> $EndDate,
			            'VendorID' 		=> $VendorID,
			            'SubID' 		=> $SubID);
        $GetWebsiteVisitorReport= $this->WVRModel->GetWebsiteVisitorReportCount($Search);
       	$ChartData 				= $this->WVRModel->GetChartDataForWebsiteVisitor($Search);

        $TotalHits 		= $GetWebsiteVisitorReport['Count'];
        $VisitorData 	= $GetWebsiteVisitorReport['Res'];

        $UniqueIPs 	= 0;
        $UniqueIP 		= array();
        if(!empty($VisitorData))
        { 
        	$IPArray=array();
        	foreach($VisitorData as $avd)
	        {
	        	$IPArray[]=$avd->IPAddress;
	       	}
	       	$UniqueIP = array_unique($IPArray);
        }        
       	$UniqueIPs = count($UniqueIP);

       	$Response['TotalHits'] = $TotalHits;
       	$Response['UniqueIPs'] = $UniqueIPs;

		$Date=array();
		$Hit=array();
		$IPAddress=array();
		if(!empty($ChartData))
		{
			foreach ($ChartData as $cd)
			{
				if($StartDate==$EndDate)
				{
					$str = "'".$cd->Hour."'";
				}
				else
				{
					$str = "'".$cd->Date."'";
				}
				array_push($Date, $str);
				array_push($Hit, $cd->Hits);
				array_push($IPAddress, $cd->IPAddress);
			}
		}
		$Dates = implode(',', $Date);
		$Hits = implode(',', $Hit);
		$IPAddress = implode(',', $IPAddress);
		$Response['Dates'] = $Dates;
       	$Response['Hits'] = $Hits;
       	$Response['IPAddress'] = $IPAddress;

       	echo json_encode($Response);
		exit();
	} 
}
