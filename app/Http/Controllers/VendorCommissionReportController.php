<?php
namespace App\Http\Controllers;
use Route;
use Mail;
use Auth, Hash;
use Validator;
use DB;
use Session;
use Excel;
use PDF;
use Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Pagination;
use App\Http\Models\VendorCommissionReportModel;
use App\Http\Models\MasterConfigModel;

class VendorCommissionReportController extends Controller
{
	public function __construct(Request $request)
	{		
		$this->Pagination 		= new Pagination();
		$this->MasterConfigModel= new MasterConfigModel();
		$this->VCRModel 		= new VendorCommissionReportModel();
	}

	public function VendorCommissionReportView(Request $request)
	{
		if(!$request->session()->has('user_id'))
		{
		    return redirect()->action('LoginController@Login');
		}
		$UserRole 	= Session::get('user_role');
		$CompanyID 	= Session::get('user_companyID');
		$UserID 	= Session::get('user_id');

		$StartDate 	= date("Y-m-d");
		$EndDate 	= date("Y-m-d");

		$CampaignDropDown 	= $this->MasterConfigModel->getCampaignList();
		$Data =array('CampaignDropDown' => $CampaignDropDown,
                     'StartDate'		=> $StartDate,
                     'EndDate' 			=> $EndDate);
    $Data['Menu']   = "Report";
    $Data['Title']  = "Admin | VendorCommissionReport";
		return view('VendorCommissionReportView')->with($Data);
	}   
	public function GetVendorCommissionReport(Request $request)
	{
		  $Data = $request->all();

      $UserRole 	= Session::get('user_role');
		  $CompanyID 	= Session::get('user_companyID');
		  $UserID 	= Session::get('user_id');

		  $StartDate 	= date("Y-m-d",strtotime($Data['StartDate']))." 00:00:00";
      $EndDate 	  = date("Y-m-d",strtotime($Data['EndDate']))." 23:59:59";
      $CampaignID = $Data['CampaignID'];
      $VendorCompanyID  = $Data['VendorCompanyID'];
      $BuyerCompanyID   = $Data['BuyerCompanyID'];
      $BuyerID          = $Data['BuyerID'];

		  $Search = array(
			      'StartDate' => $StartDate,
            'EndDate' 	=> $EndDate,
            'CompanyID' => $CompanyID,
            'CampaignID'=> $CampaignID,
            'VendorCompanyID'=> $VendorCompanyID,
            'BuyerCompanyID'=> $BuyerCompanyID,
            'BuyerID'=> $BuyerID,
            'UserRole' 	=> $UserRole,
            'UserID' 	=> $UserID);
        
        $Campaigns 				     = $this->VCRModel->GetCampaignDetails($CampaignID);
        $GetCommissionDetails  = $this->VCRModel->GetVendorCommissionReport($Search);
        if (!empty($GetCommissionDetails)) 
        {
          $Total = 0;
          $SubTotal = 0;
          $CompanyArr = array();
          ?>
          <thead>
            <tr class="table_thead">
              <th style="color:#c94142 ;font-weight: 700;" scope="col">Rows</th>
              <th style="color:#c94142 ;font-weight: 700;" scope="col">Company Name</th>
              <th style="color:#c94142 ;font-weight: 700;" scope="col">Vendor Label</th>                    
              <?php 
               	if ($Campaigns->CampaignType == "DirectPost") { ?>
              	  <th style="color:#c94142 ;font-weight: 700;" scope="col">Tier Name</th>
              	<?php } ?>
              <th style="color:#c94142 ;font-weight: 700;" scope="col">Leads In</th>
              <th style="color:#c94142 ;font-weight: 700;" scope="col">Accepted</th>
              <th style="color:#c94142 ;font-weight: 700;" scope="col">Acceptance Ratio</th>
              <?php 
              if ($Campaigns->CampaignType == "DirectPost") { ?>
              <th style="color:#c94142 ;font-weight: 700;" scope="col">Redirected</th>
              <th style="color:#c94142 ;font-weight: 700;" scope="col">Redirect Ratio</th>
              <?php } ?>
              <th style="color:#c94142 ;font-weight: 700;" scope="col">Purchased Amount</th>      
              <th style="color:#c94142 ;font-weight: 700;" scope="col">Leads Returned</th>
              <th style="color:#c94142 ;font-weight: 700;" scope="col">Returned Amount</th>
              <th style="color:#c94142 ;font-weight: 700;" scope="col">Total</th>
            </tr>
          </thead>
          <tbody>
          	<?php
          	$SubTotal = 0;
            $CountComp_arr = array();
            $CoutSubTotal_arr = array();
            foreach ($GetCommissionDetails as $CountName) 
            {
            	array_push($CountComp_arr, $CountName->CompanyName);
              if (array_key_exists($CountName->CompanyName, $CoutSubTotal_arr)) {
              	$LastVal = $CoutSubTotal_arr[$CountName->CompanyName] + $CountName->Commission;
                $CoutSubTotal_arr[$CountName->CompanyName] = $LastVal;
              } else {
              	$CoutSubTotal_arr[$CountName->CompanyName] = $CountName->Commission;
              }
            }
            $LeadsIn = 0;
            $LeadsAccepted = 0;
            $Redirected = 0;
            $CountRows = 0;
            $TotalCount = 0;
            $row = 1;
            $PurchasedAmountCount = 0;
            $LeadsReturnedCount = 0;
            $ReturnedAmountCount = 0;
            foreach ($GetCommissionDetails as $key => $gcd) 
            {
              $CountRows++;
              $CompanyArr[] = $gcd->CompanyName;
              $Total = $Total + $gcd->Commission;
              ?>
              <tr>
	            	<td><?php echo $row++; ?></td>
	              <?php
	              $counts = array_count_values($CompanyArr);
	              if ($counts[$gcd->CompanyName] == 1) 
	              {
	              	$CheckCompany = array_count_values($CountComp_arr);
	                echo "<td style='vertical-align: middle;' rowspan=" . $CheckCompany[$gcd->CompanyName] . ">" . $gcd->CompanyName . "</td>";
	              }
	              ?>
	              <td><?php echo $gcd->AdminLabel; ?></td>
	              <?php if ($Campaigns->CampaignType == "DirectPost") { ?>
	              <td><?php echo $gcd->Tier; ?></td>
	              <?php } ?>
	              <td><?php echo $gcd->LeadsIn; $LeadsIn+=$gcd->LeadsIn; ?></td>
	              <td><?php echo $gcd->LeadsAccepted; $LeadsAccepted+=$gcd->LeadsAccepted; ?></td>
	              <td><?php echo number_format($gcd->AcceptPercent, 2)."%"; ?></td>
	              <?php if ($Campaigns->CampaignType == "DirectPost") { ?>
		              <td><?php echo $gcd->Redirected; $Redirected+=$gcd->Redirected; ?></td>
		              <td>
		              	<?php if ($gcd->RedirectPercent == NULL || $gcd->RedirectPercent == 0) {
		                	echo "0.00%";
		                  } else {
		                  	echo number_format($gcd->RedirectPercent, 2)."%";
		                  }
		                  ?>
	                </td>
	              <?php } ?>      
	              <td><?php echo '$' . number_format($gcd->SoldAmount, 2); $PurchasedAmountCount+=$gcd->SoldAmount;?></td>      
	              <td><?php echo $gcd->LeadsReturned; $LeadsReturnedCount+=$gcd->LeadsReturned;?></td>
	              <td><?php echo '$'.number_format($gcd->ReturnedPrice,2); $ReturnedAmountCount+=$gcd->ReturnedPrice;?></td>
	              <td><?php echo '$' . number_format($gcd->Commission, 2); ?></td>
	            </tr>
              <?php 
              if ($CountRows == $CheckCompany[$gcd->CompanyName]) 
              {
              	$CountRows = 0; ?>    
                <tr style="font-weight: bold;">
                	<td></td>
                  <?php
                  if ($Campaigns->CampaignType == "DirectPost") {
                  	$span = 3;
                  } else{    	
                  	$span = 2;
                  }
                  ?>
                  <td colspan="<?php echo $span; ?>">Sub Total</td>
                	<td><?php echo $LeadsIn; ?></td>
                  <td><?php echo $LeadsAccepted; ?></td>
                  <?php $AcceptanceRatio = (($LeadsAccepted / $LeadsIn) * 100);
                  $cpan = 0;
	                //if($Campaigns->CampaignType != "DirectPost") $cpan = 3;
	                ?>
                  <td ><?php echo number_format($AcceptanceRatio, 2) . "%"; ?></td>
                  <?php 
                  if ($Campaigns->CampaignType == "DirectPost") 
                  { ?>              
	                  <td><?php echo $Redirected; ?></td>
	                  <?php $RedirectRatio = ($Redirected) ? (($Redirected / $LeadsAccepted) * 100) : 0; ?>
	                  <td><?php echo number_format($RedirectRatio, 2) . "%"; ?></td>
	                  <td><?php echo '$' . number_format($PurchasedAmountCount, 2);?></td>
	                  <td><?php echo $LeadsReturnedCount;?></td>
	                  <td><?php echo '$' . number_format($ReturnedAmountCount, 2);?></td>
                  <?php
                  }
                  $counts = array_count_values($CompanyArr);
                  $CheckCompany = array_count_values($CountComp_arr);
                  echo "<td style='vertical-align: bottom;'>$" . number_format($CoutSubTotal_arr[$gcd->CompanyName], 2) . "</td>";
                  $TotalCount+=$CoutSubTotal_arr[$gcd->CompanyName];
                  ?>
                </tr>
                <?php
                $LeadsIn = 0;
                $LeadsAccepted = 0;
                $Redirected = 0;
                $PurchasedAmountCount = 0;
                $LeadsReturnedCount = 0;
                $ReturnedAmountCount = 0;
            	}
        		}
                    
                    $colspan = 12;
                    if ($Campaigns->CampaignType != "DirectPost") 
                    {
                        $colspan = 9;
                    }
                    
                    ?>
                <?php
                $baseURL = url()->current();
                $stripcolor = 'background-color: #ca3b44; text-align: right; color: white;';
                if (strpos($baseURL, 'tapgage.net') !== false) 
                {
                    $stripcolor = 'background-color: #0A64B8; text-align: right; color: white;';
                }
                ?>
                <tr style="background: #c94142;">
                    <td colspan="<?php echo $colspan; ?>" style="text-align: right;color:#fff ;font-weight: 700; font-size: 0.8rem; padding-top: 0.484rem; padding-bottom: 0.484rem;"><b>Grand Total</b></td>
                    <td style='color: #fff; background: #c94142; font-weight: 700;'><?php echo '$' . number_format($TotalCount, 2); ?></td>
                </tr>
                <?php
            } 
            else 
            {
                ?>
                <tr >
                    <td colspan="5" style="color:#c94142 ;font-weight: 700;">No Data Found</td>
                </tr>
                <?php
            }
        
	}

}
