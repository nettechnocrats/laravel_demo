<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Session;

class Pagination extends Controller
{   
	public function Links($numofrecords, $count, $page)
	{
		$per_page = $numofrecords;
		$previous_btn = true;
		$next_btn = true;
		$first_btn = true;
		$last_btn = true;
		$start = $page * $per_page;
		$cur_page = $page;
		$msg = "";
		
		$no_of_paginations = ceil($count / $per_page);

		if($count>0)
		{
			if ($cur_page >= 7) {
				$start_loop = $cur_page - 3;
				if ($no_of_paginations > $cur_page + 3)
				$end_loop = $cur_page + 3;
				else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 6) {
	    			$start_loop = $no_of_paginations - 6;
	    			$end_loop = $no_of_paginations;
				} else {
	    			$end_loop = $no_of_paginations;
				}
			} else {
				$start_loop = 1;
				if ($no_of_paginations > 7)
	    			$end_loop = 7;
				else
	    			$end_loop = $no_of_paginations;
			}
			$msg .= "<ul class='pagination d-flex justify-content-end pagination-danger newPagi'>";
			if ($first_btn && $cur_page > 1) {
					$msg .= "<li p='1' class='page-item'> <a class='page-link' href='javascript:void(0)' onclick='Pagination(1)'><i class='fa fa-chevron-left'></i><i class='fa fa-chevron-left'></i></a> </li>";
			} else if ($first_btn) {
					$msg .= "<li p='1' class='inactive'> <a class='page-link'><i class='fa fa-chevron-left'></i><i class='fa fa-chevron-left'></i></a> </li>";
			}
			if ($previous_btn && $cur_page > 1) {
				$pre = $cur_page - 1;
				$msg .= "<li p='$pre' class='page-item'> <a class='page-link' onclick='Pagination($pre)' href='javascript:void(0)'><i class='fa fa-chevron-left'></i></a> </li>";
			} else if ($previous_btn) {
				$msg .= "<li class='inactive'> <a class='page-link'><i class='fa fa-chevron-left'></i></a> </li>";
			}
			for ($i = $start_loop; $i <= $end_loop; $i++) {
				if ($cur_page == $i)
	    			$msg .= "<li p='$i' class='page-item active'><a class='page-link' href='javascript:void(0)'>{$i}</a></li>";
				else
	    			$msg .= "<li p='$i' class='page-item'><a class='page-link' onclick='Pagination($i)' href='javascript:void(0)'>{$i}</a></li>";
				}
			    if ($next_btn && $cur_page < $no_of_paginations) {
			        $nex = $cur_page + 1;
			        $msg .= "<li p='$nex' class='page-item'> <a class='page-link' href='javascript:void(0)' onclick='Pagination($nex)'><i class='fa fa-chevron-right'></i></a> </li>";
			    } else if ($next_btn) {
			        $msg .= "<li class='inactive'> <a class='page-link'><i class='fa fa-chevron-right'></i></a> </li>";
			    }

				if ($last_btn && $cur_page < $no_of_paginations) {
					$msg .= "<li p='$no_of_paginations' class='page-item'> <a class='page-link' onclick='Pagination($no_of_paginations)' href='javascript:void(0)'><i class='fa fa-chevron-right'></i><i class='fa fa-chevron-right'></i></a> </li>";
				} else if ($last_btn) {
					$msg .= "<li p='$no_of_paginations' class='inactive'> <a class='page-link'><i class='fa fa-chevron-right'></i><i class='fa fa-chevron-right'></i></a> </li>";
				}
				$msg .= "</ul>";
				return $msg;
		}
	}
	public function DropDowns($numofrecords, $count, $page)
	{
		$per_page = $numofrecords;
		$msg ='';
		if($count>0)
		{
			$no_of_paginations = ceil($count / $per_page);
			$start_loop = 1 ;
			$end_loop = $no_of_paginations ;
			$msg .= "<select id='Pagination' onchange='Pagination();'> ";
			
			for ($i = $start_loop; $i <= $end_loop; $i++) 
			{
				$selected = '';
				if($page==$i){
					$selected = 'selected';
				}
				$msg .= "<option value='{$i}' ".$selected." >{$i}</option>";
			}
			$msg .= "</select>";
			return $msg;
		}
		else
		{
			return '<div class="col-md-12 text-center" style="color:red"><strong>No Record Found</strong></div>';
		}
	}

	public function LinksForUser($numofrecords, $count, $page)
	{
		$per_page = $numofrecords;
		$previous_btn = true;
		$next_btn = true;
		$first_btn = true;
		$last_btn = true;
		$start = $page * $per_page;
		$cur_page = $page;
		$msg = "";
		
		$no_of_paginations = ceil($count / $per_page);

		if($count>0)
		{
			if ($cur_page >= 7) {
				$start_loop = $cur_page - 3;
				if ($no_of_paginations > $cur_page + 3)
				$end_loop = $cur_page + 3;
				else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 6) {
	    			$start_loop = $no_of_paginations - 6;
	    			$end_loop = $no_of_paginations;
				} else {
	    			$end_loop = $no_of_paginations;
				}
			} else {
				$start_loop = 1;
				if ($no_of_paginations > 7)
	    			$end_loop = 7;
				else
	    			$end_loop = $no_of_paginations;
			}
			$msg .= "<ul class='pagination d-flex justify-content-end pagination-danger newPagi'>";
			if ($first_btn && $cur_page > 1) {
					$msg .= "<li p='1' class='page-item'> <a class='page-link' href='javascript:void(0)' onclick='PaginationUser(1)'><i class='fa fa-chevron-left'></i><i class='fa fa-chevron-left'></i></a> </li>";
			} else if ($first_btn) {
					$msg .= "<li p='1' class='inactive'> <a class='page-link'><i class='fa fa-chevron-left'></i><i class='fa fa-chevron-left'></i></a> </li>";
			}
			if ($previous_btn && $cur_page > 1) {
				$pre = $cur_page - 1;
				$msg .= "<li p='$pre' class='page-item'> <a class='page-link' onclick='PaginationUser($pre)' href='javascript:void(0)'><i class='fa fa-chevron-left'></i></a> </li>";
			} else if ($previous_btn) {
				$msg .= "<li class='inactive'> <a class='page-link'><i class='fa fa-chevron-left'></i></a> </li>";
			}
			for ($i = $start_loop; $i <= $end_loop; $i++) {
				if ($cur_page == $i)
	    			$msg .= "<li p='$i' class='page-item active'><a class='page-link' href='javascript:void(0)'>{$i}</a></li>";
				else
	    			$msg .= "<li p='$i' class='page-item'><a class='page-link' onclick='PaginationUser($i)' href='javascript:void(0)'>{$i}</a></li>";
				}
			    if ($next_btn && $cur_page < $no_of_paginations) {
			        $nex = $cur_page + 1;
			        $msg .= "<li p='$nex' class='page-item'> <a class='page-link' href='javascript:void(0)' onclick='PaginationUser($nex)'><i class='fa fa-chevron-right'></i></a> </li>";
			    } else if ($next_btn) {
			        $msg .= "<li class='inactive'> <a class='page-link'><i class='fa fa-chevron-right'></i></a> </li>";
			    }

				if ($last_btn && $cur_page < $no_of_paginations) {
					$msg .= "<li p='$no_of_paginations' class='page-item'> <a class='page-link' onclick='PaginationUser($no_of_paginations)' href='javascript:void(0)'><i class='fa fa-chevron-right'></i><i class='fa fa-chevron-right'></i></a> </li>";
				} else if ($last_btn) {
					$msg .= "<li p='$no_of_paginations' class='inactive'> <a class='page-link'><i class='fa fa-chevron-right'></i><i class='fa fa-chevron-right'></i></a> </li>";
				}
				$msg .= "</ul>";
				return $msg;
		}
	}
	public function LinksForVendor($numofrecords, $count, $page)
	{
		$per_page = $numofrecords;
		$previous_btn = true;
		$next_btn = true;
		$first_btn = true;
		$last_btn = true;
		$start = $page * $per_page;
		$cur_page = $page;
		$msg = "";
		
		$no_of_paginations = ceil($count / $per_page);

		if($count>0)
		{
			if ($cur_page >= 7) {
				$start_loop = $cur_page - 3;
				if ($no_of_paginations > $cur_page + 3)
				$end_loop = $cur_page + 3;
				else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 6) {
	    			$start_loop = $no_of_paginations - 6;
	    			$end_loop = $no_of_paginations;
				} else {
	    			$end_loop = $no_of_paginations;
				}
			} else {
				$start_loop = 1;
				if ($no_of_paginations > 7)
	    			$end_loop = 7;
				else
	    			$end_loop = $no_of_paginations;
			}
			$msg .= "<ul class='pagination d-flex justify-content-end pagination-danger newPagi'>";
			if ($first_btn && $cur_page > 1) {
					$msg .= "<li p='1' class='page-item'> <a class='page-link' href='javascript:void(0)' onclick='PaginationVendor(1)'><i class='fa fa-chevron-left'></i><i class='fa fa-chevron-left'></i></a> </li>";
			} else if ($first_btn) {
					$msg .= "<li p='1' class='inactive'> <a class='page-link'><i class='fa fa-chevron-left'></i><i class='fa fa-chevron-left'></i></a> </li>";
			}
			if ($previous_btn && $cur_page > 1) {
				$pre = $cur_page - 1;
				$msg .= "<li p='$pre' class='page-item'> <a class='page-link' onclick='PaginationVendor($pre)' href='javascript:void(0)'><i class='fa fa-chevron-left'></i></a> </li>";
			} else if ($previous_btn) {
				$msg .= "<li class='inactive'> <a class='page-link'><i class='fa fa-chevron-left'></i></a> </li>";
			}
			for ($i = $start_loop; $i <= $end_loop; $i++) {
				if ($cur_page == $i)
	    			$msg .= "<li p='$i' class='page-item active'><a class='page-link' href='javascript:void(0)'>{$i}</a></li>";
				else
	    			$msg .= "<li p='$i' class='page-item'><a class='page-link' onclick='PaginationVendor($i)' href='javascript:void(0)'>{$i}</a></li>";
				}
			    if ($next_btn && $cur_page < $no_of_paginations) {
			        $nex = $cur_page + 1;
			        $msg .= "<li p='$nex' class='page-item'> <a class='page-link' href='javascript:void(0)' onclick='PaginationVendor($nex)'><i class='fa fa-chevron-right'></i></a> </li>";
			    } else if ($next_btn) {
			        $msg .= "<li class='inactive'> <a class='page-link'><i class='fa fa-chevron-right'></i></a> </li>";
			    }

				if ($last_btn && $cur_page < $no_of_paginations) {
					$msg .= "<li p='$no_of_paginations' class='page-item'> <a class='page-link' onclick='PaginationVendor($no_of_paginations)' href='javascript:void(0)'><i class='fa fa-chevron-right'></i><i class='fa fa-chevron-right'></i></a> </li>";
				} else if ($last_btn) {
					$msg .= "<li p='$no_of_paginations' class='inactive'> <a class='page-link'><i class='fa fa-chevron-right'></i><i class='fa fa-chevron-right'></i></a> </li>";
				}
				$msg .= "</ul>";
				return $msg;
		}
	}
	public function LinksForBuyer($numofrecords, $count, $page)
	{
		$per_page = $numofrecords;
		$previous_btn = true;
		$next_btn = true;
		$first_btn = true;
		$last_btn = true;
		$start = $page * $per_page;
		$cur_page = $page;
		$msg = "";
		
		$no_of_paginations = ceil($count / $per_page);

		if($count>0)
		{
			if ($cur_page >= 7) {
				$start_loop = $cur_page - 3;
				if ($no_of_paginations > $cur_page + 3)
				$end_loop = $cur_page + 3;
				else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 6) {
	    			$start_loop = $no_of_paginations - 6;
	    			$end_loop = $no_of_paginations;
				} else {
	    			$end_loop = $no_of_paginations;
				}
			} else {
				$start_loop = 1;
				if ($no_of_paginations > 7)
	    			$end_loop = 7;
				else
	    			$end_loop = $no_of_paginations;
			}
			$msg .= "<ul class='pagination d-flex justify-content-end pagination-danger newPagi'>";
			if ($first_btn && $cur_page > 1) {
					$msg .= "<li p='1' class='page-item'> <a class='page-link' href='javascript:void(0)' onclick='PaginationBuyer(1)'><i class='fa fa-chevron-left'></i><i class='fa fa-chevron-left'></i></a> </li>";
			} else if ($first_btn) {
					$msg .= "<li p='1' class='inactive'> <a class='page-link'><i class='fa fa-chevron-left'></i><i class='fa fa-chevron-left'></i></a> </li>";
			}
			if ($previous_btn && $cur_page > 1) {
				$pre = $cur_page - 1;
				$msg .= "<li p='$pre' class='page-item'> <a class='page-link' onclick='PaginationBuyer($pre)' href='javascript:void(0)'><i class='fa fa-chevron-left'></i></a> </li>";
			} else if ($previous_btn) {
				$msg .= "<li class='inactive'> <a class='page-link'><i class='fa fa-chevron-left'></i></a> </li>";
			}
			for ($i = $start_loop; $i <= $end_loop; $i++) {
				if ($cur_page == $i)
	    			$msg .= "<li p='$i' class='page-item active'><a class='page-link' href='javascript:void(0)'>{$i}</a></li>";
				else
	    			$msg .= "<li p='$i' class='page-item'><a class='page-link' onclick='PaginationBuyer($i)' href='javascript:void(0)'>{$i}</a></li>";
				}
			    if ($next_btn && $cur_page < $no_of_paginations) {
			        $nex = $cur_page + 1;
			        $msg .= "<li p='$nex' class='page-item'> <a class='page-link' href='javascript:void(0)' onclick='PaginationBuyer($nex)'><i class='fa fa-chevron-right'></i></a> </li>";
			    } else if ($next_btn) {
			        $msg .= "<li class='inactive'> <a class='page-link'><i class='fa fa-chevron-right'></i></a> </li>";
			    }

				if ($last_btn && $cur_page < $no_of_paginations) {
					$msg .= "<li p='$no_of_paginations' class='page-item'> <a class='page-link' onclick='PaginationBuyer($no_of_paginations)' href='javascript:void(0)'><i class='fa fa-chevron-right'></i><i class='fa fa-chevron-right'></i></a> </li>";
				} else if ($last_btn) {
					$msg .= "<li p='$no_of_paginations' class='inactive'> <a class='page-link'><i class='fa fa-chevron-right'></i><i class='fa fa-chevron-right'></i></a> </li>";
				}
				$msg .= "</ul>";
				return $msg;
		}
	}
}
?>
