<?php
namespace App\Http\Controllers;
use Route;
use Mail;
use Auth, Hash;
use Validator;
use DB;
use Session;
use Excel;
use PDF;
use Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Pagination;
use App\Http\Models\CommonModel;
use App\Http\Models\MasterConfigModel;

class CommonController extends Controller
{
	public function __construct(Request $request)
	{		
		$this->Pagination 		= new Pagination();
		$this->MasterConfigModel= new MasterConfigModel();
		$this->CModel 			= new CommonModel();
	}
 
	public function GetVendorBuyerUsingCampaignID(Request $request) 
    {
        $data = $request->all();
        $CompanyID  = Session::get('user_companyID'); 
        $CampaignID = $data['CampaignID'];

        $GetCampaignDetails = $this->CModel->GetCampaignDetails($CampaignID);
        ////////////Vendor Company//////////////////
        $GetVendorCompany = $this->CModel->GetVendorCompany($CampaignID);  
        $VendorCompanyDropDown = '<option value="All">All Vendor Company</option>';
        foreach ($GetVendorCompany as $vc) {
            $VendorCompanyDropDown.='<option value=' . $vc->CompanyID . '>' . $vc->CompanyName . '(' . $vc->CompanyID . ')</option>';
        }  
        ////////////Vendor//////////////////
        $Vendor = $this->CModel->GetVendorsUsingCampaignID($CampaignID);       
        $VendorDropDown = '<option value="All">All Vendor</option>';
        foreach ($Vendor as $vdd) {
            $VendorDropDown.='<option value=' . $vdd->VendorID . '>' . $vdd->AdminLabel . '(' . $vdd->VendorID . ')</option>';
        }
        ////////////Buyer Company/////////////////
        $GetBuyerCompany = $this->CModel->GetBuyerCompany($CampaignID);      
        $BuyerCompanyDropDown = '<option value="All">All Buyer Company</option>';
        foreach ($GetBuyerCompany as $bc) {
            $BuyerCompanyDropDown.='<option value=' . $bc->CompanyID . '>' . $bc->CompanyName. '(' . $bc->CompanyID . ')</option>';
        } 
        ////////////Buyer //////////////////      
        $Buyer = $this->CModel->GetBuyersUsingCampaignID($CampaignID);       
        $BuyerDropDown = '<option value="All">All Buyer</option>';
        foreach ($Buyer as $vdd) {
            $BuyerDropDown.='<option value=' . $vdd->BuyerID . '>' . $vdd->AdminLabel . '(' . $vdd->BuyerID . ')</option>';
        }

        $DataArray = array('Vendor' => $Vendor,'CampaignType' => $GetCampaignDetails->CampaignType);
        $DataArray['CampaignType']  = $GetCampaignDetails->CampaignType;
        $DataArray['VendorCompany'] = $VendorCompanyDropDown;
        $DataArray['Vendor'] 		= $VendorDropDown;
        $DataArray['BuyerCompany'] 	= $BuyerCompanyDropDown;
        $DataArray['Buyer'] 		= $BuyerDropDown;
        echo json_encode($DataArray);
        exit();
    }
    
    public function GetVendorBuyerUsingCampaignIDAndShowInactiveAndTest(Request $request) 
    {
        $data = $request->all();
        $CampaignID = $data['CampaignID'];

        $ShowInactiveVendorCompanyID= $data['ShowInactiveVendorCompanyID'];
        $ShowTestVendorCompanyID    = $data['ShowTestVendorCompanyID'];

        $ShowInactiveBuyerCompanyID= $data['ShowInactiveBuyerCompanyID'];
        $ShowTestBuyerCompanyID    = $data['ShowTestBuyerCompanyID'];
        
        $ShowInactiveVendor        = $data['ShowInactiveVendor'];
        $ShowTestVendor            = $data['ShowTestVendor'];

        $GetCampaignDetails = $this->CModel->GetCampaignDetails($CampaignID);
        ////////////Vendor Company//////////////////
        $GetVendorCompany = $this->CModel->GetVendorCompanyUsingShowInactiveAndTest($ShowInactiveVendorCompanyID,$ShowTestVendorCompanyID,$CampaignID);  

        $VendorCompanyDropDown = '<option value="All">All Vendor Company</option>';
        foreach ($GetVendorCompany as $vc) {
            $VendorCompanyDropDown.='<option value=' . $vc->CompanyID . '>' . $vc->CompanyName . '(' . $vc->CompanyID . ')</option>';
        }  
        ////////////Vendor//////////////////
        $Vendor = $this->CModel->GetVendorsUsingCampaignIDAndShowInactiveAndTest($ShowInactiveVendor,$ShowTestVendor,$CampaignID);       
        $VendorDropDown = '<option value="All">All Vendor</option>';
        foreach ($Vendor as $vdd) {
            $VendorDropDown.='<option value=' . $vdd->VendorID . '>' . $vdd->AdminLabel . '(' . $vdd->VendorID . ')</option>';
        }
        ////////////Buyer Company/////////////////
        $GetBuyerCompany = $this->CModel->GetBuyerCompanyUsingShowInactiveAndTest($ShowInactiveBuyerCompanyID,$ShowTestBuyerCompanyID,$CampaignID);      
        $BuyerCompanyDropDown = '<option value="All">All Buyer Company</option>';
        foreach ($GetBuyerCompany as $bc) {
            $BuyerCompanyDropDown.='<option value=' . $bc->CompanyID . '>' . $bc->CompanyName. '(' . $bc->CompanyID . ')</option>';
        }
        
        $DataArray['VendorCompany'] = $VendorCompanyDropDown;
        $DataArray['Vendor']        = $VendorDropDown;
        $DataArray['BuyerCompany']  = $BuyerCompanyDropDown;
        echo json_encode($DataArray);
        exit();
    }
    public function GetVendorUsingVendorCompany(Request $request) 
    {
        $data = $request->all();
        $VendorCompanyID = $data['VendorCompanyID'];  
        $CampaignID = $data['CampaignID'];  
        ////////////Vendor//////////////////
        $Vendor = $this->CModel->GetVendorUsingVendorCompany($VendorCompanyID,$CampaignID);       
        $VendorDropDown = '<option value="All">All Vendor</option>';
        foreach ($Vendor as $vdd) {
            $VendorDropDown.='<option value=' . $vdd->VendorID . '>' . $vdd->AdminLabel . '(' . $vdd->VendorID . ')</option>';
        }
        echo $VendorDropDown;
        exit();
    }
    public function GetBuyerUsingBuyerCompany(Request $request) 
    {
        $data = $request->all();
        $BuyerCompanyID = $data['BuyerCompanyID']; 
        $CampaignID = $data['CampaignID'];  
      
        $Buyer = $this->CModel->GetBuyerUsingBuyerCompany($BuyerCompanyID,$CampaignID);       
        $BuyerDropDown = '<option value="All">All Buyer</option>';
        foreach ($Buyer as $vdd) {
            $BuyerDropDown.='<option value=' . $vdd->BuyerID . '>' . $vdd->AdminLabel . '(' . $vdd->BuyerID . ')</option>';
        }
        echo $BuyerDropDown;
        exit();
    }  
     
    public function GetUserUsingCompanyID(Request $request) 
    {
        $data = $request->all();
        $VendorCompanyID = $data['VendorCompanyID'];   
        $CampaignID = $data['CampaignID'];
        $User = $this->CModel->GetUserUsingCompanyID($VendorCompanyID,$CampaignID);       
        $UserDropDown = '<option value="All">All User</option>';
        foreach ($User as $u) {
            $UserDropDown.='<option value=' . $u->UserID . '>' . $u->UserFirstName .  $u->UserLastName . '</option>';
        }
        echo $UserDropDown;
        exit();
    } 

    public function GetVendorUsingVendorCompanyAndShowInactiveAndTest(Request $request) 
    {
        $data = $request->all();
        $CampaignID = $data['CampaignID'];

        $ShowInactiveVendor        = $data['ShowInactiveVendor'];
        $ShowTestVendor            = $data['ShowTestVendor'];

        $GetCampaignDetails = $this->CModel->GetCampaignDetails($CampaignID);
        ////////////Vendor//////////////////
        $Vendor = $this->CModel->GetVendorsUsingCampaignIDAndShowInactiveAndTest($ShowInactiveVendor,$ShowTestVendor,$CampaignID);       
        $VendorDropDown = '<option value="All">All Vendor</option>';
        foreach ($Vendor as $vdd) {
            $VendorDropDown.='<option value=' . $vdd->VendorID . '>' . $vdd->AdminLabel . '(' . $vdd->VendorID . ')</option>';
        }
        
        $DataArray['Vendor']        = $VendorDropDown;
        echo json_encode($DataArray);
        exit();
    }

    public function GetCompanyUsingShwowInactiveAndTest(Request $request)
    {
        $Data           = $request->all();
        $ShowInactive   = $Data['ShowInactive'];
        $ShowTest       = $Data['ShowTest'];

        $GetCompany = $this->CModel->GetCompanyUsingShwowInactiveAndTest($ShowInactive, $ShowTest);
        ?>
        <option value="All">All Company</option>
        <?php
        foreach ($GetCompany as $key => $val) 
        {
            ?>
            <option value="<?php echo $key; ?>" ><?php echo $val; ?></option>
            <?php
        }
        exit();
    }
    public function GetCompanyUsingShwowInactiveAndTest2(Request $request)
    {
        $Data           = $request->all();
        $ShowInactive   = $Data['ShowInactive'];
        $ShowTest       = $Data['ShowTest'];

        $GetCompany = $this->CModel->GetCompanyUsingShwowInactiveAndTest($ShowInactive, $ShowTest);
        ?>
        <option value="">Select Company</option>
        <?php
        foreach ($GetCompany as $key => $val) 
        {
            ?>
            <option value="<?php echo $key; ?>" ><?php echo $val; ?></option>
            <?php
        }
        exit();
    }

    public function GetCampaignDetails(Request $request)
    {
        $data = $request->all();
        $CampaignDetails=array();
        $CampaignID = $data['CampaignID'];
        $GetCampaignDetails = $this->CModel->GetCampaignDetails($CampaignID);
        if(!empty($GetCampaignDetails))
        {
            $CampaignDetails['Status']        = 1;
            $CampaignDetails['CampaignType']  = $GetCampaignDetails->CampaignType;
        }
        else
        {
            $CampaignDetails['Status']        = 0;
        }
        echo json_encode($CampaignDetails);
        exit();
    }
}

