<?php
namespace App\Http\Controllers;
use Route;
use Mail;
use Auth, Hash;
use Validator;
use DB;
use Session;
use Excel;
use PDF;
use Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Pagination;
use App\Http\Models\LeadReportModel;
use App\Http\Models\CommonModel;
use App\Http\Models\MasterConfigModel;

class LeadReportController extends Controller
{
	public function __construct(Request $request)
	{		
		$this->Pagination 		= new Pagination();
		$this->MasterConfigModel= new MasterConfigModel();
		$this->LRModel 			= new LeadReportModel();
		$this->CModel 			= new CommonModel();
	}

	public function LeadReportView(Request $request)
	{
		if(!$request->session()->has('user_id'))
		{
		    return redirect()->action('LoginController@Login');
		}
		
		$UserRole 	= Session::get('user_role');
		$CompanyID 	= Session::get('user_companyID');
		$UserID 	= Session::get('user_id');

		$StartDate 	= date("Y-m-d");
		$EndDate 	= date("Y-m-d");

		$CampaignDropDown 	= $this->MasterConfigModel->getCampaignList();
		$TiersDropDown 		= $this->CModel->TiersDropDown();

		$SessionCampaignID = '';
		$SessionStatus = '';
		
		if (Session::has('CampaignID') && Session::has('Status')) {
		   	$SessionCampaignID = Session::get('CampaignID');
		   	$SessionStatus 	  = Session::get('Status');
		   	Session::forget('CampaignID');
    		Session::forget('Status');
		}
		$Data['SessionCampaignID'] 	= $SessionCampaignID;
		$Data['SessionStatus'] 		= $SessionStatus;

		$Data['CampaignDropDown'] 	= $CampaignDropDown;
		$Data['TiersDropDown'] 		= $TiersDropDown;
		$Data['StartDate'] 	= $StartDate;
		$Data['EndDate'] 	= $EndDate;
	
		$Data['Menu'] 	= "LeadReport";
		$Data['Title'] 	= "Admin | Lead Report";
		return view('LeadReportView')->with($Data);
	}	
    public function GetLeadReport(Request $request)
	{
		$Data = $request->all();

        $UserRole 	= Session::get('user_role');
		$CompanyID 	= Session::get('user_companyID');
		$UserID 	= Session::get('user_id');

        $NumOfRecords = $Data['numofrecords'];
        //$NumOfRecords = 10;
        $page = $Data['page'];
        $cur_page = $page;

        $Limitpage = $page - 1;
        $Start = $Limitpage * $NumOfRecords;

        $CampaignID = $Data['CampaignID'];
        $VendorCompanyID 	= $Data['VendorCompanyID'];
        $VendorID 			= $Data['VendorID'];
        $BuyerCompanyID 	= $Data['BuyerCompanyID'];
        $BuyerID 			= $Data['BuyerID'];
        $StartDate 	= $Data['StartDate'];
        $EndDate 	= $Data['EndDate'];
        $Status 	= $Data['Status'];
        $PingPostStatus 	= $Data['PingPostStatus'];
        $Redirected = $Data['Redirected'];

        $SubID 		= $Data['SubID'];        
        $Email 		= $Data['Email'];
        $LeadID 	= $Data['LeadID'];
        
        $Search = array('CompanyID' 	=> $CompanyID,
			            'CampaignID' 	=> $CampaignID,
			            'StartDate' 	=> $StartDate,
			            'EndDate' 		=> $EndDate,
			            'VendorCompanyID'	=> $VendorCompanyID,
			            'VendorID' 			=> $VendorID,
			            'BuyerCompanyID'	=> $BuyerCompanyID,
			            'BuyerID' 			=> $BuyerID,
			            'Status' 		=> $Status,
			            'PingPostStatus'=> $PingPostStatus,
			            'Redirected' 	=> $Redirected,
			            'SubID' 		=> $SubID,
			            'Email' 		=> $Email,
			            'LeadID' 		=> $LeadID,
			            'Start' 		=> $Start,
			            'NumOfRecords' 	=> $NumOfRecords);
        $AllLead 		= $this->LRModel->GetAllLeadReport($Search);
       
        $AllLeadData 	= $AllLead['Res'];
        $LeadCount 		= $AllLead['Count'];
        $LeadData 		= $this->LRModel->GetSummaryLeadsReport($Search);
       
        echo $this->GetLeadReportSummary($CampaignID, $LeadData);

        $CampaignDetails = $this->CModel->GetCampaignDetails($CampaignID);

        $Pagination = $this->Pagination->DropDowns($NumOfRecords, $LeadCount, $page);
      	?>
		<div id="dataTable_wrapper" class="dataTables_wrapper dt-bootstrap4">
			<div class="row">
				<div class="heading text-center">
				  <h2>Leads List</h2>
				</div>
				<div class="table-responsive">
				  <table class="table table-bordered tableFont" id="dataTable" width="100%">
				     <thead>
				        <tr role="row">
				            <th style="color: #c94142;" scope="col">Row</th>
				            <th style="color: #c94142;" scope="col">Lead ID</th>
				            <th style="color: #c94142;" scope="col">Date Time</th>
				            <th style="color: #c94142;" scope="col">Vendor</th>
				            <th style="color: #c94142;" scope="col">Email</th>
				            <th style="color: #c94142;" scope="col">Zip Code</th>
				            <th style="color: #c94142;" scope="col">Vendor Price</th>
				            <th style="color: #c94142;" scope="col">Buyer</th>
				            <th style="color: #c94142;" scope="col">Buyer Price</th>
				            <th style="color: #c94142;" scope="col" id="DynamicLabel">
		                        <?php
		                        if ($CampaignDetails->CampaignType == 'DirectPost') {
		                            echo "Redirect/Date-Time";
		                        } else {
		                            echo "Action";
		                        }
		                        ?>                    
		                    </th>
				            <th style="color: #c94142;" scope="col">Time Total</th>
				            <th style="color: #c94142;" scope="col">Status</th>
				            <th style="color: #c94142;" scope="col">Action</th>
				        </tr>
				    </thead>
				    <tbody>
					    <?php 
					     	if(!empty($AllLeadData ))
					     	{
					     		$i = $Start + 1;
					     		foreach($AllLeadData as $vld)
					     		{
								     ?>				       
							        <tr>
							           	<td ><?=$i?></td>
							           	<td style="color: #0580ad; font-weight: 700;">
							           		<a target="_blank" 
							           			href="<?php echo route('admin.ViewLeadReportSummary',array($vld->ID)) ?>">
							           			<?php echo $vld->LeadID; ?>
							           		</a>
							           	</td>
							           	<td>
							           		<?php
			                                if ($vld->Action == 'DirectPost') {
			                                    echo $vld->DirectPostDate;
			                                } else if ($vld->Action == 'Ping') {
			                                    echo $vld->PingDate;
			                                } else if ($vld->Action == 'Post') {
			                                    echo $vld->PostDate;
			                                }
			                                ?>
							           	</td>
							           	<td><?php echo $vld->VendorName; ?> (VID: <?php echo $vld->VendorID; ?> / CID: <?php echo $vld->VendorCompanyID; ?>) </td>
							           	<td><?php echo $vld->Email; ?></td>
							           	<td><?php echo $vld->Zip; ?></td>
							           	<td>
							           	<?php echo "$" . number_format($vld->VendorLeadPrice, 2); 
							           	if(isset($vld->ReturnedVendorPrice)) {
			                              echo " ($".number_format($vld->ReturnedVendorPrice, 2).")"; 
			                            }							           		
							           	?>
							           	</td>
							           	<td><?php echo $vld->BuyerName; ?> (BID: <?php echo $vld->BuyerID; ?> / CID: <?php echo $vld->BuyerCompanyID; ?>) </td>
							           	<td>
							           	<?php echo "$" . number_format($vld->BuyerLeadPrice, 2); 
							           	if(isset($vld->ReturnedBuyerPrice)) {
			                              echo " ($".number_format($vld->ReturnedBuyerPrice, 2).")"; 
			                            }
							           	?>
							           	</td>
							           	<td>
							           		<?php
				                                if ($vld->Action == 'DirectPost') {
				                                    $redirect = ($vld->IsRedirected == '1') ? 'Yes' : 'No';
				                                    if ($redirect == "Yes") {
				                                        echo $vld->IsRedirectedTime;
				                                    } else {
				                                        echo "No.";
				                                    }
				                                } else {
				                                    echo $vld->Action;
				                                }
			                                ?>
							           	</td>
							           	<td>
							           		<?php
			                                if ($vld->Action == 'DirectPost') {
			                                    echo number_format($vld->DirectPostTime, 3) . 's';
			                                } else if ($vld->Action == 'Ping') {
			                                    echo number_format($vld->PingTime, 3) . 's';
			                                } else if ($vld->Action == 'Post') {
			                                    echo number_format($vld->PostTime, 3) . 's';
			                                }
			                                ?>
							           	</td>
							           	<td>
							           		<?php
			                                if ($vld->Action == 'DirectPost') {
			                                    $LeadStatus = $vld->DirectPostResponse;
			                                } else if ($vld->Action == 'Ping') {
			                                    $LeadStatus = $vld->PingResponse;
			                                } else if ($vld->Action == 'Post') {
			                                    $LeadStatus = $vld->PostResponse;
			                                }


			                                if ($LeadStatus == 'Lead Rejected' || $LeadStatus == 'Buyer Bid On Lead' || $LeadStatus == 'Buyer Rejected Lead') {
			                                    ?>
			                                    <span  class="fonnT noBoradi exPadd lnarWidth btnPadd leadRejBtn">Rejected</span >
			                                    <?php
			                                } else if ($LeadStatus == 'Buyer Accepted Lead') {
			                                    ?>
			                                    <span  class="fonnT noBoradi exPadd lnarWidth mb-1 leadRejBtn leadAccpBtn">Accepted</span >
			                                    <?php
			                                } else if ($LeadStatus == 'Lead Returned') {
			                                    ?>
			                                    <span  class="fonnT noBoradi lnarWidth btnPadd rexPadd leadRejBtn leadRettrnBtn">  Returned </span >
			                                    <?php
			                                }else if ($LeadStatus == 'No Buyers Mapped') {
			                                    ?>
			                                    <span  class="fonnT btnPadd noBoradi exPadd lnarWidth leadRejBtn leadNoBuyBtn">No Buyer</span >
			                                    <?php
			                                }
			                                ?>
							           	</td>
							            <td>
							              	<div class="dropdown">
											  <button class="btn btn-secondary dropdown-toggle actBtnClr noBoradi mb-1" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
											  </button>
											  <div class="dropdown-menu drpDwnMarg noBoradi" aria-labelledby="dropdownMenuButton">
											    
						           				<a class="dropdown-item drpDwnItem" target="_blank" 
								           			href="<?php echo route('admin.ViewLeadReportSummary',array($vld->ID)) ?>">
								           			View Details
								           		</a>
								           		<?php if ($LeadStatus == 'Buyer Accepted Lead' || $LeadStatus == 'Buyer Purchased Lead') {?>			                                    
						              			<a class="dropdown-item drpDwnItem" href="javascript:void(0);"
						              			onclick="OpenReturnModal('<?php echo $vld->LeadID; ?>','<?php echo $CampaignID; ?>');">
						              				Return
							              		</a>
							              		<?php } ?>
											  </div>
											</div>
							           	</td>
							        </tr>
				     				<?php 
				     				$i++;
				     			}
				     		} 
				     		else 
				     		{
				     			?>
						     	<tr >
						           <td colspan="11" style="color: #05aefd;">Record Not Found.</td>
						           </td>
						        </tr>
				     			<?php 
				     		} 
				     	?>
				     </tbody>
				  </table>
				</div>
			</div>
		<?php if(!empty($AllLeadData ))
		{ ?>
			<div class="row mt-3">			
				<div class="col-md-6 col-sm-6 col-6 noPaddd">
					<div class="dataTables_length mCenter" id="dataTable_length">
						<label>Records/Pages
							<select id="numofrecords" onchange="SearchViewProcessedLeadReport();" name="dataTable_length" aria-controls="dataTable" class="custom-select custom-select-sm form-control form-control-sm">
							<option value="10" <?php if($NumOfRecords=='10'){ echo 'selected'; } ?> >10</option>
							<option value="20" <?php if($NumOfRecords=='20'){ echo 'selected'; } ?> >20</option>
							<option value="50" <?php if($NumOfRecords=='50'){ echo 'selected'; } ?> >50</option>
							<option value="100" <?php if($NumOfRecords=='100'){ echo 'selected'; } ?> >100</option>
							<option value="150" <?php if($NumOfRecords=='150'){ echo 'selected'; } ?> >150</option>
							<option value="200" <?php if($NumOfRecords=='200'){ echo 'selected'; } ?> >200</option>
							</select>
						</label>
					</div>
				</div>
				<div class="col-md-6 col-sm-6 col-6">
					<div class="dataTables_length mCenter" id="dataTable_length" style="text-align: right;">
						<label>Jump On Page
							<?php echo $Pagination; ?>
						</label>
					</div>
				</div>
			</div>
      	<?php
      	}
      	else
      	{
      		?>
      		<input type="hidden" name="numofrecords" id="numofrecords" value='10'>
      		<?php
      	}
       	exit();
	}
	public function GetLeadReportSummary($CampaignID,$LeadData) 
	{
	 	$Accepted 		= 0;
	 	$Rejected 		= 0;
	 	$Returned 		= 0;
	 	$Redirected 	= 0;
	 	$Total 			= 0;	
	 	$RedirectRatio 	= 0;
	 	$TotalVendorPrice 		= 0.00;
	 	$TotalBuyerPrice 		= 0.00;
		$ReturnedVendorPrice 	= 0.00;
		$ReturnedBuyerPrice 	= 0.00;
		$TotalProfit 			= 0.00;
		$TotalReturnedProfit 	= 0.00;
	 	if(!empty($LeadData))
	 	{
	 		if($LeadData->Accepted!='')
	 		{
	 			$Accepted 	= $LeadData->Accepted;	 			
	 		}
	 		if($LeadData->Rejected!='')
	 		{
	 			$Rejected 	= $LeadData->Rejected; 			
	 		}
	 		if($LeadData->Returned!='')
	 		{
	 			$Returned 	= $LeadData->Returned;	 			
	 		}
	 		if(isset($LeadData->Redirected))
		 	{
		 		if($LeadData->Redirected!='')
		 		{
		 			$Redirected = $LeadData->Redirected;	 			
		 		}	
		 	}
	 			 	
		 	$Total 		= $LeadData->Total;	 	
		 	$RedirectRatio = (($Redirected) && ($LeadData->Accepted)) ? (($Redirected / $LeadData->Accepted) * 100) : 0;
		 	
		 	if($LeadData->TotalVendorPrice!='')
	 		{
	 			$TotalVendorPrice 		= $LeadData->TotalVendorPrice;			
	 		}
		 	
		 	if(isset($LeadData->ReturnedVendorPrice))
		 	{
		 		$ReturnedVendorPrice 	= $LeadData->ReturnedVendorPrice;
		 	}

		 	if($LeadData->TotalBuyerPrice!='')
	 		{
	 			$TotalBuyerPrice 		= $LeadData->TotalBuyerPrice;			
	 		}
		 	
		 	if(isset($LeadData->ReturnedBuyerPrice))
		 	{
		 		$ReturnedBuyerPrice 	= $LeadData->ReturnedBuyerPrice;
		 	}

		 	$TotalProfit = $TotalBuyerPrice-$TotalVendorPrice;
		 	if(isset($LeadData->ReturnedVendorPrice))
		 	{		 		
		 		$TotalReturnedProfit = $ReturnedBuyerPrice-$ReturnedVendorPrice;
		 	}
	 	}
	 	

	 	?>
	 	<div class="row">
	        <div class="hBox">
	           <div class="heading text-center">
	              <h2>Leads Summary</h2>
	           </div>
	           <div class="flex-nowrap">
	              <div class="table-responsive">
	                 <table class="table table-bordered newTableTh">
	                    <thead>
	                       <tr>
	                          <th scope="col">Accepted</th>
	                          <th scope="col">Rejected</th>
	                          <th scope="col">Returned</th>
	                          <?php if ($CampaignID == '1') { ?>
	                          <th scope="col">Redirected</th>
	                          <th scope="col">Redirected (%)</th>
	                          <?php } ?>
	                          <th scope="col">Total</th>
	                          <th scope="col">Total Vendor Price</th>
	                          <th scope="col">Total Buyer Price</th>
	                          <th scope="col">Profit</th>
	                       </tr>
	                    </thead>
	                    <tbody>
	                       <tr>
	                          <th scope="row"><?php echo $Accepted; ?></th>
	                          <th><?php echo $Rejected; ?></th>
	                          <th><?php echo $Returned; ?></th>
	                          <?php if ($CampaignID == '1') { ?>
	                          <th><?php echo $Redirected; ?></th>
	                          <th><?php echo number_format($RedirectRatio,2); ?>%</th>
	                          <?php } ?>
	                          <th><?php echo $Total; ?></th>
	                          <th>$<?php echo number_format($TotalVendorPrice,2); 
	                          if(isset($LeadData->ReturnedVendorPrice)) {
	                              echo " ($".number_format($ReturnedVendorPrice, 2).")"; 
	                            }
	                          ?></th>
	                          <th>$<?php echo number_format($TotalBuyerPrice,2); 
	                           if(isset($LeadData->ReturnedBuyerPrice)) {
	                              echo " ($".number_format($ReturnedBuyerPrice, 2).")"; 
	                            }
	                          ?></th>
	                          <th>$<?php echo number_format($TotalProfit,2); 
	                           if(isset($LeadData->ReturnedBuyerPrice)) {
	                              echo " ($".number_format($TotalReturnedProfit, 2).")"; 
	                            }
	                          ?></th>
	                       </tr>
	                    </tbody>
	                 </table>
	              </div>
	           </div>
	        </div>
	    </div>
	 	<?php
    }
    public function ViewLeadReportSummary(Request $request,$RequestID)
    {	
    	$userRole = Session::get('user_role');
        $companyID = Session::get('user_companyID');
        $userID = Session::get('user_id');
        $LeadInboundDetails = $this->LRModel->LeadInboundDetails($RequestID);
       	        
        $PostedData = array();
        $TotalTime = 0;
        $LeadID = 0;
        if (!empty($LeadInboundDetails)) 
        {
       		$CampaignID 	= $LeadInboundDetails->CampaignID;
       		$LeadID 		= $LeadInboundDetails->LeadID;
            $CampaignDetails= $this->LRModel->GetCampaignDetails($CampaignID);
            $TotalTime 		= $this->LRModel->GetTotalTime($LeadID, $CampaignID);
            $BuyerAttemptedList = $this->LRModel->BuyerAttemptedList($LeadID,$CampaignID);
            if ($LeadInboundDetails->Action == "DirectPost") 
            {
                $PostedData = $this->XMLToArray($LeadInboundDetails->DirectPostData);
            } 
            elseif ($LeadInboundDetails->Action == "Ping") 
            {
                $PostedData = $this->XMLToArray($LeadInboundDetails->PingData);
            } 
            elseif ($LeadInboundDetails->Action == "Post ") 
            {
                $PostedData = $this->XMLToArray($LeadInboundDetails->PingData);
            }
           
        	return view('ViewLeadReportSummary')->with(array('LeadInboundDetails' => $LeadInboundDetails,
										                    'BuyerAttemptedList' => $BuyerAttemptedList,
										                    'PostedData' => $PostedData,
										                    'CampaignDetails' => $CampaignDetails,
										                    'TotalTime' => $TotalTime,
										                    'LeadID' => $LeadID,
										                    'Menu' => "Report",
										                    'Title' => "Admin | LeadReport"));
        }
        else
        {
        	return redirect('/dashboard');
        }
    }
    public function XMLToArray($XMLString) 
    {
        $SSN = $this->GetStringBetween($XMLString, '<SSN>', '</SSN>');
        $BankRoutingNumber = $this->GetStringBetween($XMLString, '<BankRoutingNumber>', '</BankRoutingNumber>');
        $BankAccountNumber = $this->GetStringBetween($XMLString, '<BankAccountNumber>', '</BankAccountNumber>');
        $Array = array('SSN' => $SSN,
            'BankRoutingNumber' => $BankRoutingNumber,
            'BankAccountNumber' => $BankAccountNumber);
        return $Array;
    }
    public function GetStringBetween($string, $start, $end) {
        $string = ' ' . $string;
        $ini = strpos($string, $start);
        if ($ini == 0)
            return '';
        $ini += strlen($start);
        $len = strpos($string, $end, $ini) - $ini;
        return substr($string, $ini, $len);
    }
     public function GetReturnModalUsingLeadID(Request $request) 
    {
        $data 			= $request->all();
        $LeadID 		= $data['LeadID'];
        $CampaignID 	= $data['CampaignID'];
        $ReturnCode 	= $this->MasterConfigModel->getReturnCode();
        $GetReturnModalUsingLeadID = $this->LRModel->GetReturnModalUsingLeadID($LeadID,$CampaignID);

        ?>
        
    	<div class="row" id="ReturnCodeMsg">

    	</div>
        <div class="row">
			<div class="col-md-6 form-group tadmPadd labFontt">
				<label for="exampleInputEmail1">Lead Id:</label>
				<input type="email" class="form-control camH" disabled 
					value="<?php echo $GetReturnModalUsingLeadID->LeadID; ?>">
				<span id="UserEmailErr"></span>
			</div>
			<div class="col-md-6 form-group tadmPadd labFontt">
				<label for="exampleInputEmail1">Email:</label>
				<input type="email" class="form-control camH" disabled
					value="<?php echo $GetReturnModalUsingLeadID->Email; ?>">
				<span id="UserEmailErr"></span>
			</div>
			<div class="col-md-6 form-group tadmPadd labFontt">
				<label for="exampleInputEmail1">Vendor:</label>
				<input type="email" class="form-control camH" disabled
					value="<?php echo $GetReturnModalUsingLeadID->VendorName; ?>">
				<span id="UserEmailErr"></span>
			</div>
			<div class="col-md-6 form-group tadmPadd labFontt">
				<label for="exampleInputEmail1">Buyer:</label>
				<input type="email" class="form-control camH" disabled
					value="<?php echo $GetReturnModalUsingLeadID->BuyerName; ?>">
				<span id="UserEmailErr"></span>
			</div>
			<div class="col-md-6 form-group tadmPadd labFontt">
				<label for="ReturnCode">Return Code:</label>
				<select type="email"  onchange="HideErr('ReturnCodeErr');" class="form-control camH" id="ReturnCode" name="ReturnCode" placeholder="Email">
					<option value="">Select Return Code</option>
					<?php foreach($ReturnCode as $rc){?>
					<option value="<?php echo $rc->ReturnCode; ?>"><?php echo $rc->ReturnReason; ?></option>
					<?php } ?>
				</select>
				<span id="ReturnCodeErr"></span>
			</div>
			<div class="col-md-6 form-group tadmPadd labFontt resOn">
				<label for="exampleInputEmail1">Reason:</label>
				<textarea onkeypress="HideErr('ReasonErr');" class="form-control" cols="5" id="Reason" name="Reason" placeholder="Reason"></textarea>
				<span id="ReasonErr"></span>
			</div>
			<div class="col-md-6 offset-md-6 form-group tadmPadd labFontt clretBtn">
				<button type="button" class="btn btn-default" data-dismiss="modal" id="CloseBtn" >Close</button>
      			<button type="button" class="btn btn-primary retBtn" id="ReturnBtn" 
      				onclick="ValidateReturnCode('<?php echo $LeadID; ?>','<?php echo $CampaignID; ?>');">Return</button>
			</div>
        </div>
        <?php
        exit();
    }

    public function AddReturnCode(Request $request)
    {
    	$data 			= $request->all();
    	$CampaignID 	= $data['CampaignID'];
    	$LeadID 		= $data['LeadID'];
        $ReturnCode 	= $data['ReturnCode'];
        $ReturnMessage 	= $data['Reason'];
        $LeadData = $this->LRModel->GetReturnModalUsingLeadID($LeadID,$CampaignID);
        
        if(!empty($LeadData))
        {
        	$Details = array(
                'CampaignID' => $CampaignID,
                'CompanyID' => $LeadData->CompanyID,
                'LeadID' => $LeadID,
                'VendorID' => $LeadData->VendorID,
                'VendorLeadPrice' => $LeadData->VendorLeadPrice,
                'BuyerID' => $LeadData->BuyerID,
                'BuyerLeadPrice' => $LeadData->BuyerLeadPrice,
                'VendorRefPayout' => $LeadData->VendorRefPayout,
                'BuyerRefPayout' => $LeadData->BuyerRefPayout,
                'ReturnCode' => $ReturnCode,
                'ReturnMessage' => $ReturnMessage
            );

            $Add = $this->LRModel->AddReturnCode($Details);

            if(isset($LeadData->ReturnAPIURL) && $LeadData->ReturnAPIURL !=""){
                $ReturnAPIURL = $LeadData->ReturnAPIURL;
            } else {
                $ReturnAPIURL = "";
            }
            
            $res = "";
            if(!empty($ReturnAPIURL) && $ReturnAPIURL != "") {
                $fields = http_build_query($Details);
                $url = $ReturnAPIURL.'?'.$fields;
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL,$url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $response = curl_exec($ch);
                $res = json_decode($response);
            } 

            if ($Add) 
            {
                if($res!="" && $res->status=="1") 
                {
                    $response = array(
                        "LeadReturnStatus" => "1",
                        "AutoLeadReturnStatus" => '1',
                        "Msg"=>'<div class="alert alert-success">
                                    <strong>Success!</strong> Lead Returned Successfully! 
                                </div>'
                    );
                } 
                else 
                {
                    $response = array(
                        "LeadReturnStatus" => "1",
                        "AutoLeadReturnStatus" => '0',
                        "Msg"=>'<div class="alert alert-success">
                                    <strong>Success!</strong> Lead Returned Successfully!
                                </div>'
                    );
                }  
            } 
            else 
            {
            	$response = array(
                    "LeadReturnStatus" => "0",
                    "AutoLeadReturnStatus" => '0',
                    "Msg"=>'<div class="alert alert-warning">
                                <strong>Sorry!</strong> Please Try Again.
                            </div>'
                );
            }
        }
        else
        {
			$response = array(
                "LeadReturnStatus" => "0",
                "AutoLeadReturnStatus" => '0',
                "Msg"=>'<div class="alert alert-warning">
                            <strong>Sorry!</strong> No Lead Found.
                        </div>'
 			);
        }
        echo json_encode($response);
        exit();
    }
	/*public function GetViewSoldLeadReport(Request $request)
	{
		$Data = $request->all();

        $UserRole 	= Session::get('user_role');
		$CompanyID 	= Session::get('user_companyID');
		$UserID 	= Session::get('user_id');

        $NumOfRecords = $Data['numofrecords'];
        $page = $Data['page'];
        $cur_page = $page;

        $Limitpage = $page - 1;
        $Start = $Limitpage * $NumOfRecords;

        $CampaignID = $Data['CampaignID'];
        $VendorID 	= $Data['VendorID'];
        $StartDate 	= $Data['StartDate'];
        $EndDate 	= $Data['EndDate'];
        $Status 	= $Data['Status'];
        $PingPostStatus 	= $Data['PingPostStatus'];
        $Redirected = $Data['Redirected'];

        $SubID 		= $Data['SubID'];        
        $Email 		= $Data['Email'];
        $LeadID 	= $Data['LeadID'];
        
        $Search = array('CompanyID' 	=> $CompanyID,
			            'CampaignID' 	=> $CampaignID,
			            'StartDate' 	=> $StartDate,
			            'EndDate' 		=> $EndDate,
			            'VendorID' 		=> $VendorID,
			            'Status' 		=> $Status,
			            'PingPostStatus'=> $PingPostStatus,
			            'Redirected' 	=> $Redirected,
			            'SubID' 		=> $SubID,
			            'Email' 		=> $Email,
			            'LeadID' 		=> $LeadID,
			            'Start' 		=> $Start,
			            'NumOfRecords' 	=> $NumOfRecords);
        $AllLead 		= $this->VPLRModel->GetAllViewSoldLeadsAdmin($Search);
       
        $AllLeadData 	= $AllLead['Res'];
        $LeadCount 		= $AllLead['Count'];
        $LeadData 		= $this->VPLRModel->GetSummaryViewSoldLeadsAdmin($Search);
       
        echo $this->GetLeadSoldReportSummary($CampaignID, $LeadData);

        $CampaignDetails = $this->VPLRModel->GetCampaignDetails($CampaignID);

        $Pagination = $this->Pagination->DropDowns($NumOfRecords, $LeadCount, $page);
      ?>
		<div id="dataTable_wrapper" class="dataTables_wrapper dt-bootstrap4">
			<div class="row">
				<div class="heading text-center">
				  <h2>Leads List</h2>
				</div>
				<div class="table-responsive">
				  <table class="table table-bordered tableFont" id="dataTable" width="100%">
				     <thead>
				        <tr role="row">
				            <th style="color: #c94142;" scope="col">Row</th>
				            <th style="color: #c94142;" scope="col">Lead ID</th>
				            <th style="color: #c94142;" scope="col">Date Time</th>
				            <th style="color: #c94142;" scope="col">Vendor</th>
				            <th style="color: #c94142;" scope="col">Email</th>
				            <th style="color: #c94142;" scope="col">Zip Code</th>
				            <th style="color: #c94142;" scope="col">Payout</th>
				            <th style="color: #c94142;" scope="col" id="DynamicLabel">
		                        <?php
		                        if ($CampaignDetails->CampaignType == 'DirectPost') {
		                            echo "Redirect/Date-Time";
		                        } else {
		                            echo "Action";
		                        }
		                        ?>                    
		                    </th>
				            <th style="color: #c94142;" scope="col">Time Total</th>
				            <th style="color: #c94142;" scope="col">Status</th>
				            <th style="color: #c94142;" scope="col">Action</th>
				        </tr>
				    </thead>
				    <tbody>
					    <?php 
					     	if(!empty($AllLeadData ))
					     	{
					     		$i = $Start + 1;
					     		foreach($AllLeadData as $vld)
					     		{
								     ?>				       
							        <tr>
							           	<td ><?=$i?></td>
							           	<td style="color: #0580ad; font-weight: 700;">
							           		<a target="_blank" 
							           			href="<?php echo route('admin.ViewSoldLeadReportSummary',array($vld->ID)) ?>">
							           			<?php echo $vld->LeadID; ?>
							           		</a>
							           	</td>
							           	<td>
							           		<?php
			                                if ($vld->Action == 'DirectPost') {
			                                    echo $vld->DirectPostDate;
			                                } else if ($vld->Action == 'Ping') {
			                                    echo $vld->PingDate;
			                                } else if ($vld->Action == 'Post') {
			                                    echo $vld->PostDate;
			                                }
			                                ?>
							           	</td>
							           	<td><?php echo $vld->VendorName; ?> (VID: <?php echo $vld->VendorID; ?> / CID: <?php echo $vld->CompanyID; ?>) </td>
							           	<td><?php echo $vld->Email; ?></td>
							           	<td><?php echo $vld->Zip; ?></td>
							           	<td><?php echo "$" . number_format($vld->VendorLeadPrice, 2); ?></td>
							           	<td>
							           		<?php
				                                if ($vld->Action == 'DirectPost') {
				                                    $redirect = ($vld->IsRedirected == '1') ? 'Yes' : 'No';
				                                    if ($redirect == "Yes") {
				                                        echo $vld->IsRedirectedTime;
				                                    } else {
				                                        echo "No.";
				                                    }
				                                } else {
				                                    echo $vld->Action;
				                                }
			                                ?>
							           	</td>
							           	<td>
							           		<?php
			                                if ($vld->Action == 'DirectPost') {
			                                    echo number_format($vld->DirectPostTime, 3) . 's';
			                                } else if ($vld->Action == 'Ping') {
			                                    echo number_format($vld->PingTime, 3) . 's';
			                                } else if ($vld->Action == 'Post') {
			                                    echo number_format($vld->PostTime, 3) . 's';
			                                }
			                                ?>
							           	</td>
							           	<td>
							           		<?php
			                                if ($vld->Action == 'DirectPost') {
			                                    $LeadStatus = $vld->DirectPostResponse;
			                                } else if ($vld->Action == 'Ping') {
			                                    $LeadStatus = $vld->PingResponse;
			                                } else if ($vld->Action == 'Post') {
			                                    $LeadStatus = $vld->PostResponse;
			                                }


			                                if ($LeadStatus == 'Lead Rejected' || $LeadStatus == 'Buyer Bid On Lead' || $LeadStatus == 'Buyer Rejected Lead') {
			                                    ?>
			                                    <span  class="fonnT noBoradi exPadd lnarWidth btnPadd leadRejBtn">Rejected</span >
			                                    <?php
			                                } else if ($LeadStatus == 'Buyer Accepted Lead') {
			                                    ?>
			                                    <span  class="fonnT noBoradi exPadd lnarWidth mb-1 leadRejBtn leadAccpBtn">Accepted</span >
			                                    <?php if($vld->Action=='Post' || $vld->Action=='DirectPost'){?>
			                                    	<!-- <button type="button" class="btn btn-info fonnT noBoradi lnarWidth btnPadd rexPadd" 
			                                    		onclick="OpenReturnModal('<?php //echo $vld->LeadID; ?>','<?php //echo $vld->CampaignID; ?>');"> <i class="fa fa-reply"></i> Return </button> -->
			                                    <?php }?>
			                                    <?php
			                                } else if ($LeadStatus == 'Lead Returned') {
			                                    ?>
			                                    <span  class="fonnT noBoradi lnarWidth btnPadd rexPadd leadRejBtn leadRettrnBtn">  Returned </span >
			                                    <?php
			                                }else if ($LeadStatus == 'No Buyers Mapped') {
			                                    ?>
			                                    <span  class="fonnT btnPadd noBoradi exPadd lnarWidth leadRejBtn leadNoBuyBtn">No Buyer</span >
			                                    <?php
			                                }
			                                ?>
							           	</td>
							            <td>
							              	<div class="dropdown">
											  <button class="btn btn-secondary dropdown-toggle actBtnClr noBoradi mb-1" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
											  </button>
											  <div class="dropdown-menu drpDwnMarg noBoradi" aria-labelledby="dropdownMenuButton">
											    
						           				<a class="dropdown-item drpDwnItem" target="_blank" 
								           			href="<?php echo route('admin.ViewSoldLeadReportSummary',array($vld->ID)) ?>">
								           			View Details
								           		</a>

											  </div>
											</div>
							           	</td>
							        </tr>
				     				<?php 
				     				$i++;
				     			}
				     		} 
				     		else 
				     		{
				     			?>
						     	<tr >
						           <td colspan="11" style="color: #05aefd;">Record Not Found.</td>
						           </td>
						        </tr>
				     			<?php 
				     		} 
				     	?>
				     </tbody>
				  </table>
				</div>
			</div>
		<?php if(!empty($AllLeadData ))
		{ ?>
			<div class="row mt-3">			
				<div class="col-md-6 col-sm-6 col-6 noPaddd">
					<div class="dataTables_length mCenter" id="dataTable_length">
						<label>Records/Pages
							<select id="numofrecords" onchange="SearchViewProcessedLeadReport();" name="dataTable_length" aria-controls="dataTable" class="custom-select custom-select-sm form-control form-control-sm">
							<option value="10" <?php if($NumOfRecords=='10'){ echo 'selected'; } ?> >10</option>
							<option value="20" <?php if($NumOfRecords=='20'){ echo 'selected'; } ?> >20</option>
							<option value="50" <?php if($NumOfRecords=='50'){ echo 'selected'; } ?> >50</option>
							<option value="100" <?php if($NumOfRecords=='100'){ echo 'selected'; } ?> >100</option>
							<option value="150" <?php if($NumOfRecords=='150'){ echo 'selected'; } ?> >150</option>
							<option value="200" <?php if($NumOfRecords=='200'){ echo 'selected'; } ?> >200</option>
							</select>
						</label>
					</div>
				</div>
				<div class="col-md-6 col-sm-6 col-6">
					<div class="dataTables_length mCenter" id="dataTable_length" style="text-align: right;">
						<label>Jump On Page
							<?php echo $Pagination; ?>
						</label>
					</div>
				</div>
				<!-- <div class="col-md-6 col-sm-6 col-12 grid-margin stretch-card">
					<div class="card noBg border-0">
					    <nav>
					      <?php echo $Pagination; ?>
					    </nav>
					</div>
				</div> -->
			</div>
      	<?php
      	}
      	else
      	{
      		?>
      		<input type="hidden" name="numofrecords" id="numofrecords" value='10'>
      		<?php
      	}
       	exit();
	}

	public function GetLeadSoldReportSummary($CampaignID,$LeadData) 
	{
	 	$Accepted 		= 0;
	 	$Rejected 		= 0;
	 	$Returned 		= 0;
	 	$Redirected 	= 0;
	 	$Total 			= 0;	
	 	$RedirectRatio 	= 0;
	 	$TotalVendorPrice 		= 0.00;
		$ReturnedVendorPrice 	= 0.00;
	 	if(!empty($LeadData))
	 	{
	 		if($LeadData->Accepted!='')
	 		{
	 			$Accepted 	= $LeadData->Accepted;	 			
	 		}
	 		if($LeadData->Rejected!='')
	 		{
	 			$Rejected 	= $LeadData->Rejected; 			
	 		}
	 		if($LeadData->Returned!='')
	 		{
	 			$Returned 	= $LeadData->Returned;	 			
	 		}
	 		if(isset($LeadData->Redirected))
		 	{
		 		if($LeadData->Redirected!='')
		 		{
		 			$Redirected = $LeadData->Redirected;	 			
		 		}	
		 	}
	 			 	
		 	$Total 		= $LeadData->Total;	 	
		 	$RedirectRatio = (($Redirected) && ($LeadData->Accepted)) ? (($Redirected / $LeadData->Accepted) * 100) : 0;
		 	if($LeadData->TotalVendorPrice!='')
	 		{
	 			$TotalVendorPrice 		= $LeadData->TotalVendorPrice;			
	 		}
		 	
		 	if(isset($LeadData->ReturnedVendorPrice))
		 	{
		 		$TotalVendorPrice 		= $LeadData->ReturnedVendorPrice;
		 	}
		 	//$ReturnedVendorPrice 	= $LeadData->ReturnedVendorPrice;
	 	}
	 	

	 	?>
	 	<div class="row">
	        <div class="hBox">
	           <div class="heading text-center">
	              <h2>Leads Summary</h2>
	           </div>
	           <div class="flex-nowrap">
	              <div class="table-responsive">
	                 <table class="table table-bordered newTableTh">
	                    <thead>
	                       <tr>
	                          <th scope="col">Accepted</th>
	                          <th scope="col">Rejected</th>
	                          <th scope="col">Returned</th>
	                          <?php if ($CampaignID == '1') { ?>
	                          <th scope="col">Redirected</th>
	                          <th scope="col">Redirected (%)</th>
	                          <?php } ?>
	                          <th scope="col">Total</th>
	                          <th scope="col">Total Payout</th>
	                       </tr>
	                    </thead>
	                    <tbody>
	                       <tr>
	                          <th scope="row"><?php echo $Accepted; ?></th>
	                          <th><?php echo $Rejected; ?></th>
	                          <th><?php echo $Returned; ?></th>
	                          <?php if ($CampaignID == '1') { ?>
	                          <th><?php echo $Redirected; ?></th>
	                          <th><?php echo number_format($RedirectRatio,2); ?>%</th>
	                          <?php } ?>
	                          <th><?php echo $Total; ?></th>
	                          <th>$<?php echo number_format($TotalVendorPrice,2); ?></th>
	                       </tr>
	                    </tbody>
	                 </table>
	              </div>
	           </div>
	        </div>
	    </div>
	 	<?php
    }

    public function ViewSoldLeadReportSummary(Request $request,$RequestID)
    {	
    	$userRole = Session::get('user_role');
        $companyID = Session::get('user_companyID');
        $userID = Session::get('user_id');
        $LeadInboundDetails = $this->VPLRModel->LeadInboundDetails($RequestID);
       	
        $BuyerAttemptedDetails = array();
        $LeadDetails = array();
        $PostedData = array();
        $PostPostedData = array();
        $TotalTime = 0;
        $LeadID = 0;
        if (!empty($LeadInboundDetails)) 
        {
            if ($LeadInboundDetails->LeadID != 0) 
            {
                $LeadID 		= $LeadInboundDetails->LeadID;
                $CampaignID 	= $LeadInboundDetails->CampaignID;
                $BuyerAttemptedDetails = $this->VPLRModel->BuyerAttemptedDetails($LeadID, $CampaignID);
                $LeadDetails 	= $this->VPLRModel->getLeadDetails($LeadID, $CampaignID);
                $TotalTime 		= $this->VPLRModel->GetTotalTime($LeadID, $CampaignID);
                $CampaignDetails = $this->VPLRModel->GetCampaignDetails($CampaignID);
            }
            if ($LeadInboundDetails->Action == "DirectPost") 
            {
                $PostedData = $this->XMLToArray($LeadInboundDetails->DirectPostData);
            } 
            elseif ($LeadInboundDetails->Action == "Ping") 
            {
                $PostedData = $this->XMLToArray($LeadInboundDetails->PingData);
            } 
            elseif ($LeadInboundDetails->Action == "Post ") 
            {
                $PostedData = $this->XMLToArray($LeadInboundDetails->PingData);
                $PostPostedData = $this->XMLToArray($LeadInboundDetails->PostData);
            }

            $AllBuyer = $this->VPLRModel->getBuyerCampaign();
            
        	return view('ViewSoldLeadReportSummary')->with(array('LeadInboundDetails' => $LeadInboundDetails,
												                    'BuyerAttemptedDetails' => $BuyerAttemptedDetails,
												                    'LeazdDetails' => $LeadDetails,
												                    'AllBuyer' => $AllBuyer,
												                    'TotalTime' => $TotalTime,
												                    'LeadID' => $LeadID,
												                    'PostedData' => $PostedData,
												                    'PostPostedData'=>$PostPostedData,
												                    'userRole' => $userRole,
												                    'CampaignDetails' => $CampaignDetails,
												                    'Menu' => "Report",
												                    'Title' => "Admin | LeadReport"));
        }
        else
        {
        	return redirect('/dashboard');
        }
    }

    public function XMLToArray($XMLString) 
    {
        $SSN = $this->GetStringBetween($XMLString, '<SSN>', '</SSN>');
        $BankRoutingNumber = $this->GetStringBetween($XMLString, '<BankRoutingNumber>', '</BankRoutingNumber>');
        $BankAccountNumber = $this->GetStringBetween($XMLString, '<BankAccountNumber>', '</BankAccountNumber>');
        $Array = array('SSN' => $SSN,
            'BankRoutingNumber' => $BankRoutingNumber,
            'BankAccountNumber' => $BankAccountNumber);
        return $Array;
    }
    public function GetStringBetween($string, $start, $end) {
        $string = ' ' . $string;
        $ini = strpos($string, $start);
        if ($ini == 0)
            return '';
        $ini += strlen($start);
        $len = strpos($string, $end, $ini) - $ini;
        return substr($string, $ini, $len);
    }

    public function GetVendorsUsingCampaignID(Request $request) 
    {
        $data = $request->all();
        $CompanyID  = Session::get('user_companyID'); 
        $CampaignID = $data['CampaignID'];
        $GetCampaignDetails = $this->VPLRModel->GetCampaignDetails($CampaignID);
        $vendorDropDown = $this->VPLRModel->GetVendorsUsingCampaignID($CompanyID, $CampaignID);
       
        $Vendor = '<option value="All">All Vendor</option>';
        foreach ($vendorDropDown as $vdd) {
            $Vendor.='<option value=' . $vdd->VendorID . '>' . $vdd->PartnerLabel . '(' . $vdd->VendorID . ')</option>';
        }

        $DataArray = array('Vendor' => $Vendor,'CampaignType' => $GetCampaignDetails->CampaignType);
        echo json_encode($DataArray);
        exit();
    }

    public function GetBuyersUsingCampaignID(Request $request) 
    {
        $data = $request->all();
        $CompanyID  = Session::get('user_companyID'); 
        $CampaignID = $data['CampaignID'];
        $GetCampaignDetails = $this->VPLRModel->GetCampaignDetails($CampaignID);
        $buyerDropDown = $this->VPLRModel->GetBuyersUsingCampaignID($CompanyID, $CampaignID);
       
        $Buyer = '<option value="All">All Buyer</option>';
        foreach ($buyerDropDown as $vdd) {
            $Buyer.='<option value=' . $vdd->BuyerID . '>' . $vdd->PartnerLabel . '(' . $vdd->BuyerID . ')</option>';
        }

        $DataArray = array('Buyer' => $Buyer,'CampaignType' => $GetCampaignDetails->CampaignType);
        echo json_encode($DataArray);
        exit();
    }
    public function GetReturnModalUsingLeadID(Request $request) 
    {
        $data 			= $request->all();
        $LeadID 		= $data['LeadID'];
        $CampaignID 	= $data['CampaignID'];
        $ReturnCode 	= $this->MasterConfigModel->getReturnCode();
        $GetReturnModalUsingLeadID = $this->VPLRModel->GetReturnModalUsingLeadID($LeadID,$CampaignID);

        ?>
        
    	<div class="row" id="ReturnCodeMsg">

    	</div>
        <div class="row">
			<div class="col-md-6 form-group tadmPadd labFontt">
				<label for="exampleInputEmail1">Lead Id:</label>
				<input type="email" class="form-control camH" disabled 
					value="<?php echo $GetReturnModalUsingLeadID->LeadID; ?>">
				<span id="UserEmailErr"></span>
			</div>
			<div class="col-md-6 form-group tadmPadd labFontt">
				<label for="exampleInputEmail1">Email:</label>
				<input type="email" class="form-control camH" disabled
					value="<?php echo $GetReturnModalUsingLeadID->Email; ?>">
				<span id="UserEmailErr"></span>
			</div>
			<div class="col-md-6 form-group tadmPadd labFontt">
				<label for="exampleInputEmail1">Vendor:</label>
				<input type="email" class="form-control camH" disabled
					value="<?php echo $GetReturnModalUsingLeadID->VendorName; ?>">
				<span id="UserEmailErr"></span>
			</div>
			<div class="col-md-6 form-group tadmPadd labFontt">
				<label for="exampleInputEmail1">Buyer:</label>
				<input type="email" class="form-control camH" disabled
					value="<?php echo $GetReturnModalUsingLeadID->BuyerName; ?>">
				<span id="UserEmailErr"></span>
			</div>
			<div class="col-md-6 form-group tadmPadd labFontt">
				<label for="ReturnCode">Return Code:</label>
				<select type="email"  onchange="HideErr('ReturnCodeErr');" class="form-control camH" id="ReturnCode" name="ReturnCode" placeholder="Email">
					<option value="">Select Return Code</option>
					<?php foreach($ReturnCode as $rc){?>
					<option value="<?php echo $rc->ReturnCode; ?>"><?php echo $rc->ReturnReason; ?></option>
					<?php } ?>
				</select>
				<span id="ReturnCodeErr"></span>
			</div>
			<div class="col-md-6 form-group tadmPadd labFontt resOn">
				<label for="exampleInputEmail1">Reason:</label>
				<textarea onkeypress="HideErr('ReasonErr');" class="form-control" cols="5" id="Reason" name="Reason" placeholder="Reason"></textarea>
				<span id="ReasonErr"></span>
			</div>
			<div class="col-md-6 offset-md-6 form-group tadmPadd labFontt clretBtn">
				<button type="button" class="btn btn-default" data-dismiss="modal" id="CloseBtn" >Close</button>
      			<button type="button" class="btn btn-primary retBtn" id="ReturnBtn" 
      				onclick="ValidateReturnCode('<?php echo $LeadID; ?>','<?php echo $CampaignID; ?>');">Return</button>
			</div>
        </div>
        <?php
        exit();
    }

    public function AddReturnCode(Request $request)
    {
    	$data 			= $request->all();
    	$CampaignID 	= $data['CampaignID'];
    	$LeadID 		= $data['LeadID'];
        $ReturnCode 	= $data['ReturnCode'];
        $ReturnMessage 	= $data['Reason'];
        $LeadData = $this->VPLRModel->GetReturnModalUsingLeadID($LeadID,$CampaignID);
        
        if(!empty($LeadData))
        {
        	$Details = array(
                'CampaignID' => $CampaignID,
                'CompanyID' => $LeadData->CompanyID,
                'LeadID' => $LeadID,
                'VendorID' => $LeadData->VendorID,
                'VendorLeadPrice' => $LeadData->VendorLeadPrice,
                'BuyerID' => $LeadData->BuyerID,
                'BuyerLeadPrice' => $LeadData->BuyerLeadPrice,
                'VendorRefPayout' => $LeadData->VendorRefPayout,
                'BuyerRefPayout' => $LeadData->BuyerRefPayout,
                'ReturnCode' => $ReturnCode,
                'ReturnMessage' => $ReturnMessage
            );

            $Add = $this->VPLRModel->AddReturnCode($Details);

            if(isset($LeadData->ReturnAPIURL) && $LeadData->ReturnAPIURL !=""){
                $ReturnAPIURL = $LeadData->ReturnAPIURL;
            } else {
                $ReturnAPIURL = "";
            }
            
            $res = "";
            if(!empty($ReturnAPIURL) && $ReturnAPIURL != "") {
                $fields = http_build_query($Details);
                $url = $ReturnAPIURL.'?'.$fields;
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL,$url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $response = curl_exec($ch);
                $res = json_decode($response);
            } 

            if ($Add) 
            {
                if($res!="" && $res->status=="1") 
                {
                    $response = array(
                        "LeadReturnStatus" => "1",
                        "AutoLeadReturnStatus" => '1',
                        "Msg"=>'<div class="alert alert-success">
                                    <strong>Success!</strong> Lead Returned Successfully! 
                                </div>'
                    );
                } 
                else 
                {
                    $response = array(
                        "LeadReturnStatus" => "1",
                        "AutoLeadReturnStatus" => '0',
                        "Msg"=>'<div class="alert alert-success">
                                    <strong>Success!</strong> Lead Returned Successfully!
                                </div>'
                    );
                }  
            } 
            else 
            {
            	$response = array(
                    "LeadReturnStatus" => "0",
                    "AutoLeadReturnStatus" => '0',
                    "Msg"=>'<div class="alert alert-warning">
                                <strong>Sorry!</strong> Please Try Again.
                            </div>'
                );
            }
        }
        else
        {
			$response = array(
                "LeadReturnStatus" => "0",
                "AutoLeadReturnStatus" => '0',
                "Msg"=>'<div class="alert alert-warning">
                            <strong>Sorry!</strong> No Lead Found.
                        </div>'
 			);
        }
        echo json_encode($response);
        exit();
    }
    /////////////////////////////////////////////////////////////////////////////////
    public function ViewPurchasedLeadReportView(Request $request)
	{
		if(!$request->session()->has('user_id'))
		{
		    return redirect()->action('LoginController@Login');
		}
		
		$UserRole 	= Session::get('user_role');
		$CompanyID 	= Session::get('user_companyID');
		$UserID 	= Session::get('user_id');

		$StartDate 	= date("Y-m-d");
		$EndDate 	= date("Y-m-d");

		$CampaignDropDown 	= $this->MasterConfigModel->getCampaignList();
		$TiersDropDown 		= $this->VPLRModel->TiersDropDown();
		$BuyerDropDown 	= $this->VPLRModel->BuyerDropDown($CompanyID);

		$SessionCampaignID = '';
		$SessionStatus = '';
		
		if (Session::has('CampaignID') && Session::has('Status')) {
		   	$SessionCampaignID = Session::get('CampaignID');
		   	$SessionStatus 	  = Session::get('Status');
		   	Session::forget('CampaignID');
    		Session::forget('Status');
		}
		$Data['SessionCampaignID'] 	= $SessionCampaignID;
		$Data['SessionStatus'] 		= $SessionStatus;

		$Data['CampaignDropDown'] 	= $CampaignDropDown;
		$Data['TiersDropDown'] 	= $TiersDropDown;
		$Data['BuyerDropDown'] 	= $BuyerDropDown;
		$Data['StartDate'] 	= $StartDate;
		$Data['EndDate'] 	= $EndDate;
	
		$Data['Menu'] 	= "Report";
		$Data['Title'] 	= "Admin | LeadReport";
		return view('ViewPurchasedLeadReportView')->with($Data);
	}   
	public function GetViewPurchasedLeadReport(Request $request)
	{
		$Data = $request->all();

        $UserRole 	= Session::get('user_role');
		$CompanyID 	= Session::get('user_companyID');
		$UserID 	= Session::get('user_id');

        $NumOfRecords = $Data['numofrecords'];
        $page = $Data['page'];
        $cur_page = $page;

        $Limitpage = $page - 1;
        $Start = $Limitpage * $NumOfRecords;

        $CampaignID = $Data['CampaignID'];
        $BuyerID 	= $Data['BuyerID'];
        $StartDate 	= $Data['StartDate'];
        $EndDate 	= $Data['EndDate'];
        $Status 	= $Data['Status'];
        $PingPostStatus 	= $Data['PingPostStatus'];
        $Redirected = $Data['Redirected'];

        $SubID 		= $Data['SubID'];        
        $Email 		= $Data['Email'];
        $LeadID 	= $Data['LeadID'];
        
        $Search = array('CompanyID' 	=> $CompanyID,
			            'CampaignID' 	=> $CampaignID,
			            'StartDate' 	=> $StartDate,
			            'EndDate' 		=> $EndDate,
			            'BuyerID' 		=> $BuyerID,
			            'Status' 		=> $Status,
			            'PingPostStatus'=> $PingPostStatus,
			            'Redirected' 	=> $Redirected,
			            'SubID' 		=> $SubID,
			            'Email' 		=> $Email,
			            'LeadID' 		=> $LeadID,
			            'Start' 		=> $Start,
			            'NumOfRecords' 	=> $NumOfRecords);
        $AllLead 		= $this->VPLRModel->GetAllViewPurchasedLeadsAdmin($Search);
       
        $AllLeadData 	= $AllLead['Res'];
        $LeadCount 		= $AllLead['Count'];
       	$LeadData 		= $this->VPLRModel->GetSummaryViewPurchasedLeadsAdmin($Search);
       
        echo $this->GetLeadPurchasedReportSummary($CampaignID, $LeadData,$PingPostStatus);

        $CampaignDetails = $this->VPLRModel->GetCampaignDetails($CampaignID);

        $Pagination = $this->Pagination->DropDowns($NumOfRecords, $LeadCount, $page);
      ?>
		<div id="dataTable_wrapper" class="dataTables_wrapper dt-bootstrap4">
			<div class="row">
				<div class="heading text-center">
				  <h2>Leads List</h2>
				</div>
				<div class="table-responsive">
				  <table class="table table-bordered tableFont" id="dataTable" width="100%">
				     <thead>
				        <tr role="row">
				            <th style="color: #c94142;" scope="col">Row</th>
				            <th style="color: #c94142;" scope="col">Lead ID</th>
				            <th style="color: #c94142;" scope="col">Date Time</th>
				            <th style="color: #c94142;" scope="col">Buyer</th>
				            <th style="color: #c94142;" scope="col">Email</th>
				            <th style="color: #c94142;" scope="col">Zip Code</th>
				            <th style="color: #c94142;" scope="col">Buyer Price</th>
				            <th style="color: #c94142;" scope="col" id="DynamicLabel">
		                        <?php
		                        if ($CampaignDetails->CampaignType == 'DirectPost') {
		                            echo "Redirect/Date-Time";
		                        } else {
		                            echo "Action";
		                        }
		                        ?>                    
		                    </th>
				            <th style="color: #c94142;" scope="col">Time Total</th>
				            <th style="color: #c94142;" scope="col">Status</th>
				            <th style="color: #c94142;" scope="col">Action</th>
				        </tr>
				    </thead>
				    <tbody>
					    <?php 
					     	if(!empty($AllLeadData ))
					     	{
					     		$i = $Start + 1;
					     		foreach($AllLeadData as $vld)
					     		{
								     ?>				       
							        <tr>
							           	<td ><?=$i?></td>
							           	<td style="color: #0580ad; font-weight: 700;">
							           		<a target="_blank" 
							           			href="<?php echo route('admin.ViewPurchasedLeadReportSummary',array($vld->LeadID,$CampaignID)) ?>">
							           			<?php echo $vld->LeadID; ?>
							           		</a>
							           	</td>
							           	<td>
							           		<?php
			                                if ($vld->Action == 'DirectPost') {
			                                    echo $vld->DirectPostDate;
			                                } else if ($vld->Action == 'Ping') {
			                                    echo $vld->PingDate;
			                                } else if ($vld->Action == 'Post') {
			                                    echo $vld->PostDate;
			                                }
			                                ?>
							           	</td>
							           	<td><?php echo $vld->BuyerName; ?> (BID: <?php echo $vld->BuyerID; ?> / CID: <?php echo $vld->CompanyID; ?>) </td>
							           	<td><?php echo $vld->Email; ?></td>
							           	<td><?php echo $vld->Zip; ?></td>
							           	<td><?php echo "$" . number_format($vld->BuyerLeadPrice, 2); ?></td>
							           	<td>
							           		<?php
				                                if ($vld->Action == 'DirectPost') {
				                                    $redirect = ($vld->IsRedirected == '1') ? 'Yes' : 'No';
				                                    if ($redirect == "Yes") {
				                                        echo $vld->IsRedirectedTime;
				                                    } else {
				                                        echo "No.";
				                                    }
				                                } else {
				                                    echo $vld->Action;
				                                }
			                                ?>
							           	</td>
							           	<td>
							           		<?php
			                                if ($vld->Action == 'DirectPost') {
			                                    echo number_format($vld->DirectPostTime, 3) . 's';
			                                } else if ($vld->Action == 'Ping') {
			                                    echo number_format($vld->PingTime, 3) . 's';
			                                } else if ($vld->Action == 'Post') {
			                                    echo number_format($vld->PostTime, 3) . 's';
			                                }
			                                ?>
							           	</td>
							           	<td>
							           		<?php
			                                if ($vld->Action == 'DirectPost') {
			                                    $LeadStatus = $vld->DirectPostResponse;
			                                } else if ($vld->Action == 'Ping') {
			                                    $LeadStatus = $vld->PingResponse;
			                                } else if ($vld->Action == 'Post') {
			                                    $LeadStatus = $vld->PostResponse;
			                                }


			                                if ($LeadStatus == 'Lead Rejected' || $LeadStatus == 'Buyer Bid On Lead') {
			                                    ?>
			                                    <span  class="fonnT noBoradi exPadd lnarWidth btnPadd leadRejBtn">Rejected</span >
			                                    <?php
			                                } else if ($LeadStatus == 'Buyer Accepted Lead' || $LeadStatus == 'Buyer Purchased Lead') {
			                                    ?>
			                                    <span  class="fonnT noBoradi exPadd lnarWidth mb-1 leadRejBtn leadAccpBtn">Accepted</span >
			                                    <?php if($vld->Action=='Post' || $vld->Action=='DirectPost'){?>
			                                    	<!-- <button type="button" class="btn btn-info fonnT noBoradi lnarWidth btnPadd rexPadd" 
			                                    		onclick="OpenReturnModal('<?php //echo $vld->LeadID; ?>','<?php //echo $vld->CampaignID; ?>');"> <i class="fa fa-reply"></i> Return </button> -->
			                                    <?php }?>
			                                    <?php
			                                } else if ($LeadStatus == 'Lead Returned') {
			                                    ?>
			                                    <span  class="fonnT noBoradi lnarWidth btnPadd rexPadd leadRejBtn leadRettrnBtn">  Returned </span >
			                                    <?php
			                                }else if ($LeadStatus == 'No Buyers Mapped') {
			                                    ?>
			                                    <span  class="fonnT btnPadd noBoradi exPadd lnarWidth leadRejBtn leadNoBuyBtn">No Buyer</span >
			                                    <?php
			                                }
			                                ?>
							           	</td>
							           	<td>
							              	<div class="dropdown">
											  <button class="btn btn-secondary dropdown-toggle actBtnClr noBoradi mb-1" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
											  </button>
											  <div class="dropdown-menu drpDwnMarg noBoradi" aria-labelledby="dropdownMenuButton">
											    
						           				<a class="dropdown-item drpDwnItem" target="_blank" 
								           			href="<?php echo route('admin.ViewPurchasedLeadReportSummary',array($vld->LeadID,$CampaignID)) ?>">
								           			View Details
								           		</a>

											    <?php if ($LeadStatus == 'Buyer Accepted Lead' || $LeadStatus == 'Buyer Purchased Lead') {?>			                                    
						              			<a class="dropdown-item drpDwnItem" href="javascript:void(0);"
						              			onclick="OpenReturnModal('<?php echo $vld->LeadID; ?>','<?php echo $CampaignID; ?>');">
						              				Return
							              		</a>

							              		<?php } ?>
											  </div>
											</div>
							           	</td>	
							        </tr>
				     				<?php 
				     				$i++;
				     			}
				     		} 
				     		else 
				     		{
				     			?>
						     	<tr >
						           <td colspan="11" style="color: #05aefd;">Record Not Found.</td>
						           </td>
						        </tr>
				     			<?php 
				     		} 
				     	?>
				     </tbody>
				  </table>
				</div>
			</div>
		<?php if(!empty($AllLeadData ))
		{ ?>
			<div class="row mt-3">			
				<div class="col-md-6 col-sm-6 col-6 noPaddd">
					<div class="dataTables_length mCenter" id="dataTable_length">
						<label>Records/Pages
							<select id="numofrecords" onchange="SearchViewProcessedLeadReport();" name="dataTable_length" aria-controls="dataTable" class="custom-select custom-select-sm form-control form-control-sm">
							<option value="10" <?php if($NumOfRecords=='10'){ echo 'selected'; } ?> >10</option>
							<option value="20" <?php if($NumOfRecords=='20'){ echo 'selected'; } ?> >20</option>
							<option value="50" <?php if($NumOfRecords=='50'){ echo 'selected'; } ?> >50</option>
							<option value="100" <?php if($NumOfRecords=='100'){ echo 'selected'; } ?> >100</option>
							<option value="150" <?php if($NumOfRecords=='150'){ echo 'selected'; } ?> >150</option>
							<option value="200" <?php if($NumOfRecords=='200'){ echo 'selected'; } ?> >200</option>
							</select>
						</label>
					</div>
				</div>
				<div class="col-md-6 col-sm-6 col-6">
					<div class="dataTables_length mCenter" id="dataTable_length" style="text-align: right;">
						<label>Jump On Page
							<?php echo $Pagination; ?>
						</label>
					</div>
				</div>
			</div>
      	<?php
      	}
      	else
      	{
      		?>
      		<input type="hidden" name="numofrecords" id="numofrecords" value='10'>
      		<?php
      	}
       	exit();
	}

	public function GetLeadPurchasedReportSummary($CampaignID,$LeadData,$PingPostStatus) 
	{
	 	$Accepted 		= 0;
	 	$Rejected 		= 0;
	 	$Returned 		= 0;
	 	$Redirected 	= 0;
	 	$Total 			= 0;	
	 	$RedirectRatio 	= 0;
	 	$TotalBuyerPrice 		= 0.00;
		$ReturnedBuyerPrice 	= 0.00;
	 	if(!empty($LeadData))
	 	{
	 		if($LeadData->Accepted!='')
	 		{
	 			$Accepted 	= $LeadData->Accepted;	 			
	 		}
	 			
	 		if($LeadData->Returned!='')
	 		{
	 			$Returned 	= $LeadData->Returned;	 			
	 		}


	 		if(isset($LeadData->Redirected))
		 	{
		 		if($LeadData->Redirected!='')
		 		{
		 			$Redirected = $LeadData->Redirected;	 			
		 		}	
		 	}
	 			 	
		 	$Total 		= $LeadData->Total;	 

		 	$Rejected 	= $Total - ($Accepted + $Returned);

		 	$RedirectRatio = (($Redirected) && ($LeadData->Accepted)) ? (($Redirected / $LeadData->Accepted) * 100) : 0;
		 	if($LeadData->TotalBuyerPrice!='')
	 		{
	 			$TotalBuyerPrice 		= $LeadData->TotalBuyerPrice;			
	 		}
		 	
		 	if(isset($LeadData->ReturnedBuyerPrice))
		 	{
		 		$TotalBuyerPrice 		= $LeadData->TotalBuyerPrice;
		 	}
	 	}
	 	

	 	?>
	 	<div class="row">
	        <div class="hBox">
	           <div class="heading text-center">
	              <h2>Leads Summary</h2>
	           </div>
	           <div class="flex-nowrap">
	              <div class="table-responsive">
	                 <table class="table table-bordered newTableTh">
	                    <thead>
	                       <tr>
	                          <th scope="col">Accepted</th>
	                          <th scope="col">Rejected</th>
	                          <th scope="col">Returned</th>
	                          <?php if ($CampaignID == '1') { ?>
	                          <th scope="col">Redirected</th>
	                          <th scope="col">Redirected (%)</th>
	                          <?php } ?>
	                          <th scope="col">Total</th>
	                          <th scope="col">Total Buyer Price</th>
	                       </tr>
	                    </thead>
	                    <tbody>
	                       <tr>
	                          <th scope="row"><?php echo $Accepted; ?></th>
	                          <th>
	                          	<?php 
	                          	if($PingPostStatus==''){
	                          		echo $Total-($Accepted+$Returned);
	                          	}
	                          	else{
	                          		echo $Rejected;
	                          	}
	                          	?>
	                          </th>
	                          <th><?php echo $Returned; ?></th>
	                          <?php if ($CampaignID == '1') { ?>
	                          <th><?php echo $Redirected; ?></th>
	                          <th><?php echo number_format($RedirectRatio,2); ?>%</th>
	                          <?php } ?>
	                          <th><?php echo $Total; ?></th>
	                          <th>$<?php echo number_format($TotalBuyerPrice,2); ?></th>
	                       </tr>
	                    </tbody>
	                 </table>
	              </div>
	           </div>
	        </div>
	    </div>
	 	<?php
    }

    public function ViewPurchasedLeadReportSummary(Request $request,$LeadID,$CampaignID)
    {	
    	$userRole = Session::get('user_role');
        $companyID = Session::get('user_companyID');
        $userID = Session::get('user_id');
        $CampaignDetails = $this->VPLRModel->GetCampaignDetails($CampaignID);
        $BuyerAttemptedList = $this->VPLRModel->BuyerAttemptedList($LeadID,$CampaignID);
        if (!empty($BuyerAttemptedList)) 
        {			
			$Data['CampaignDetails'] 	= $CampaignDetails;
			$Data['BuyerAttemptedList'] 	= $BuyerAttemptedList;
			$Data['Menu'] 	= "Report";
			$Data['Title'] 	= "Admin | LeadReport";
			return view('ViewPurchasedLeadReportSummary')->with($Data);
        }
        else
        {
        	return redirect('/dashboard');
        }
    }*/
}

