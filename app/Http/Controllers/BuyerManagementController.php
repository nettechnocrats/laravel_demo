<?php
namespace App\Http\Controllers;
use Route;
use Mail;
use Auth, Hash;
use Validator;
use DB;
use Session;
use Excel;
use PDF;
use Response;
use Redirect;
use Illuminate\Http\Request;
use App\Http\Controllers\Pagination;
use App\Http\Models\BuyerManagementModel;
use App\Http\Models\VendorManagementModel;
use App\Http\Models\MasterConfigModel;
use App\Http\Models\CommonModel;

class BuyerManagementController extends Controller
{
	public function __construct(Request $request)
	{		
		$this->Pagination 		= new Pagination();
		$this->MasterConfigModel= new MasterConfigModel();
		$this->BMModel 			= new BuyerManagementModel();
		$this->VMModel 			= new VendorManagementModel();
		$this->CModel 			= new CommonModel();
	}

	public function BuyerManagementView(Request $request)
	{
		if(!$request->session()->has('user_id'))
		{
		    return redirect()->action('LoginController@Login');
		}
		$Data['CompanyList'] 		= $this->VMModel->CompanyList();
		$Data['CampaignDropDown'] 	= $this->MasterConfigModel->getCampaignList();
		$Data['Menu']   = "Account";
        $Data['Title']  = "Admin | BuyerManagement";
		return view('BuyerManagementView')->with($Data);
	}

	public function GetBuyerList(Request $request)
	{
		$Data = $request->all();

        $NumOfRecords = $Data['numofrecords'];
        $page = $Data['page'];
        $cur_page = $page;

        $Limitpage = $page - 1;
        $Start = $Limitpage * $NumOfRecords;

        $ShowInactive 	= $Data['ShowInactive'];
        $ShowTest 	= $Data['ShowTest'];
        $CompanyID 	= $Data['Company'];
        $CampaignID = $Data['Campaign'];
        $BuyerName 	= $Data['BuyerName'];
        $Status 	= $Data['Status'];
        $BuyerMode 	= $Data['BuyerMode'];
      
        $Search 	= array('CampaignID' 	=> $CampaignID,
				            'CompanyID' 	=> $CompanyID,
				            'ShowInactive' 	=> $ShowInactive,
				            'ShowTest' 		=> $ShowTest,
				            'BuyerName' 	=> $BuyerName,
				            'Status' 		=> $Status,
				            'BuyerMode' 	=> $BuyerMode,
				            'Start' 		=> $Start,
				            'NumOfRecords' 	=> $NumOfRecords
				            );
        $GetBuyerList 	= $this->BMModel->GetBuyerList($Search);
       	
        $AllBuyerList 	= $GetBuyerList['Res'];
        $UserCount 		= $GetBuyerList['Count'];        

        $Pagination = $this->Pagination->Links($NumOfRecords, $UserCount, $page);
        $CampaignDetails = $this->CModel->GetCampaignDetails($CampaignID);
      ?>
		<div class="row mt-0">
          	<div class="table-responsive">
		  		<table class="table table-bordered tableFont txtL nTabl">
                    <thead class="thead-dark">
						<tr>
							<th scope="col" style="padding: 0.684rem; font-size: 0.8rem;">Company Name</th>
							<th scope="col" style="padding: 0.684rem; font-size: 0.8rem;">Campaign</th>
							<th scope="col" style="padding: 0.684rem; font-size: 0.8rem;">Buyer Name</th>
							<th scope="col" style="padding: 0.684rem; font-size: 0.8rem;">Payout Calculation</th>
							<th scope="col" style="padding: 0.684rem; font-size: 0.8rem;">Fixed Price</th>
							<th scope="col" style="padding: 0.684rem; font-size: 0.8rem;">Daily Cap</th>
							<th scope="col" style="padding: 0.684rem; font-size: 0.8rem;">Total Cap</th>
							<?php if($CampaignDetails->CampaignType=="DirectPost"){ ?>
							<th scope="col" style="padding: 0.684rem; font-size: 0.8rem;">Max Time Out</th>
							<th scope="col" style="padding: 0.684rem; font-size: 0.8rem;">Min Time Out</th>
							<?php } else { ?>
							<th scope="col" style="padding: 0.684rem; font-size: 0.8rem;">Max Ping Time Out</th>
							<th scope="col" style="padding: 0.684rem; font-size: 0.8rem;">Max Post Time Out</th>
							<?php }?>
							
							<th scope="col" style="padding: 0.684rem; font-size: 0.8rem;">Status</th>
							<th scope="col" style="padding: 0.684rem; font-size: 0.8rem;">Buyer Mode</th>
							<th scope="col" style="padding: 0.684rem; font-size: 0.8rem;">Action</th>
						</tr>
                    </thead>
			    	<tbody>
				    <?php 
				     	if(!empty($AllBuyerList ))
				     	{
				     		foreach($AllBuyerList as $p)
				     		{
							     ?>				       
						        <tr >
						           	<td><?php echo $p->CompanyName; ?>	</td>
						           	<td><?php echo $p->CampaignName; ?>	</td>
						           	<td><?php echo $p->AdminLabel.' (BID: '.$p->BuyerID.')'; ?>	</td>
						           	<td><?php echo $p->PayoutCalculation; ?></td>
						           	<td><?php echo $p->FixedPrice; ?></td>
						           	<td><?php echo $p->DailyCap; ?></td>
						           	<td><?php echo $p->TotalCap; ?></td>
						           	<?php if($CampaignDetails->CampaignType=="DirectPost"){ ?>
									<td><?php echo $p->MaxTimeOut; ?></td>
									<td><?php echo $p->MinTimeOut; ?></td>
									<?php } else { ?>
									<td><?php echo $p->MaxPingTimeOut; ?></td>
									<td><?php echo $p->MaxPostTimeOut; ?></td>
									<?php }?>
						           	<td id="StatusHtml_<?php echo $p->BuyerID; ?>">
						           		<?php 
						           		if ($p->BuyerStatus == '1') 
						           		{
		                                   ?>
		                                   	<div class="custom-control custom-checkbox noCush greenCh">
				                              <input type="checkbox"  checked="true"
				                              	class="custom-control-input noCush-input green-input" 
				                              	id="customCheck<?php echo $p->BuyerID; ?>" 
				                              	onclick="ChangeBuyerStatus('<?php echo $p->BuyerID; ?>',0);">
				                              <label class="custom-control-label noCush-label green-label mb-3" 
				                              	for="customCheck<?php echo $p->BuyerID; ?>"  
				                              	></label>
				                            </div> 
		                                   <?php
		                                } 
		                                else if ($p->BuyerStatus == '0') 
		                                {
		                                    ?>
		                                   	<div class="custom-control custom-checkbox noCush">
				                              <input type="checkbox" 
				                              class="custom-control-input noCush-input" 
				                              id="customCheck<?php echo $p->BuyerID; ?>" 
				                              onclick="ChangeBuyerStatus('<?php echo $p->BuyerID; ?>',1);" >
				                              <label class="custom-control-label noCush-label mb-3" 
				                              	for="customCheck<?php echo $p->BuyerID; ?>" 
				                              	></label>
				                            </div> 
		                                   <?php
		                                }
						           		?>
						           	</td>
						           	<td id="ModeHtml_<?php echo $p->BuyerID; ?>">
						           		<?php 
						           		if ($p->BuyerTestMode == '1') 
						           		{
		                                   ?>
		                                   	<a href="javascript:void(0);" 
		                                   		class="btn btn-danger cfonnT cbtnPadd noBoradi"
		                                   		onclick="ChangeBuyerTestMode('<?php echo $p->BuyerID; ?>',0);">
												<i class="fas fa-check-square sqFont" aria-hidden="true"></i> 
												TestMode
											</a> 
		                                   <?php
		                                } 
		                                else if ($p->BuyerTestMode == '0') 
		                                {
		                                    ?>
		                                   	<a href="javascript:void(0);" 
		                                   		class="btn btn-success cfonnT cbtnPadd noBoradi"
		                                   		onclick="ChangeBuyerTestMode('<?php echo $p->BuyerID; ?>',1);">
												<i class="fas fa-square sqFont" aria-hidden="true"></i> 
												LiveMode
											</a>  
		                                   <?php
		                                }
						           		?>
						           	</td>
						           	<td>
						           		<ul class="btnList">
                                          <li>
                                           <!--  <a href="<?php echo route('admin.BuyerEditView',array(base64_encode($p->BuyerID))); ?>">
                                            	<button type="button" title="edit" 
                                            		class="btn btn-info cfonnT cbtnPadd noBoradi">
                                            		<i class="fas fa-pencil-alt fnnPadd"></i>Edit
                                            	</button>
                                            </a> -->
                                            <a href="javascript:void(0);" onclick="GetBuyerDetails('<?php echo $p->BuyerID; ?>');">
				                              	<button type="button" title="edit" 
	                                        		class="btn btn-info cfonnT cbtnPadd noBoradi">
	                                        		<i class="fas fa-pencil-alt fnnPadd"></i>Edit
                                        		</button>
				                            </a>
                                          </li>
                                          <li>
                                           <!--  <a href="javascript:void(0);" 
                                            	onclick="SetSessionForCloneBuyer(<?php //echo $p->BuyerID; ?>);">
                                            	<button type="button" title="add" 
                                            		class="btn btn-info cfonnT cbtnPadd noBoradi bgClrred">
                                            		<i class="fas fa-plus fnnPadd"></i>Clone
                                            	</button>
                                            </a> -->

                                            <a href="javascript:void(0);" onclick="GetBuyerCloneDetails('<?php echo $p->BuyerID; ?>');">
				                              	<button type="button" title="add" 
                                            		class="btn btn-info cfonnT cbtnPadd noBoradi bgClrred">
                                            		<i class="fas fa-plus fnnPadd"></i>Clone
                                            	</button>
				                            </a>
                                          </li>
                                        </ul>
						           	</td>
						        </tr>
			     				<?php 
			     			}
			     		} 
			     		else 
			     		{
			     			?>
					     	<tr >
					           <td colspan="10" style="color: #c94542;text-align:center;">Record Not Found.</td>
					           </td>
					        </tr>
			     			<?php 
			     		} 
			     	?>
			     	</tbody>
				  </table>
				</div>
			</div>
		<?php if(!empty($AllBuyerList ))
		{ ?>
			<div class="row mt-3">			
				<div class="col-md-6 col-sm-6 col-12">
					<div class="dataTables_length mCenter" id="dataTable_length">
						<label>Records/Pages
							<select id="numofrecords" onchange="SearchVendorList();" name="dataTable_length" aria-controls="dataTable" class="custom-select custom-select-sm form-control form-control-sm">
							<option value="10" <?php if($NumOfRecords=='10'){ echo 'selected'; } ?> >10</option>
							<option value="20" <?php if($NumOfRecords=='20'){ echo 'selected'; } ?> >20</option>
							<option value="50" <?php if($NumOfRecords=='50'){ echo 'selected'; } ?> >50</option>
							<option value="100" <?php if($NumOfRecords=='100'){ echo 'selected'; } ?> >100</option>
							<option value="150" <?php if($NumOfRecords=='150'){ echo 'selected'; } ?> >150</option>
							<option value="200" <?php if($NumOfRecords=='200'){ echo 'selected'; } ?> >200</option>
							</select>
						</label>
					</div>
				</div>
				<div class="col-md-6 col-sm-6 col-12 grid-margin stretch-card">
					<div class="card noBg border-0">
					    <nav>
					      <?php echo $Pagination; ?>
					    </nav>
					</div>
				</div>
			</div>
      	<?php
      	}
      	else
      	{
      		?>
      		<input type="hidden" name="numofrecords" id="numofrecords" value='10'>
      		<?php
      	}
       	exit();
	}

	public function ChangeBuyerStatus(Request $request)
	{
		$Data 		= $request->all();
		$Response 	=array();
		$BuyerID   	 	   = $Data['BID'];
		$Details['BuyerStatus'] = $Data['Status'];
		$ChangeBuyerStatus = $this->BMModel->SaveBuyerDetails($BuyerID,$Details);
		if($ChangeBuyerStatus)
		{
			$Massage = '<div class="alert alert-success alert-dismissible fade show" role="alert">
						  Buyer Status Has Been Changed.
						  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
						    <span aria-hidden="true">&times;</span>
						  </button>
						</div>';
			$Response['Status'] = 1;
			$Response['Msg'] = $Massage;
		}
		else
		{
			$Massage = '<div class="alert alert-danger alert-dismissible fade show" role="alert">
						  Something Wrong Please Try Again.
						  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
						    <span aria-hidden="true">&times;</span>
						  </button>
						</div>';
			$Response['Status'] = 0;
			$Response['Msg'] = 1;
		}
		
		if ($Data['Status']=='1') 
   		{
            $Status = '	<div class="custom-control custom-checkbox noCush">
                          <input type="checkbox" checked="true"
                          class="custom-control-input noCush-input" 
                          id="customCheck'.$BuyerID.'"
                          onclick="ChangeBuyerStatus('.$BuyerID.',0);" >
                          <label class="custom-control-label noCush-label mb-3" 
                          	for="customCheck'.$BuyerID.'; ?>" 
                          	></label>
                        </div>';
			$Response['StatusHtml'] = $Status;
        } 
        else 
        {
            $Status = '<div class="custom-control custom-checkbox noCush">
                          <input type="checkbox" 
                          class="custom-control-input noCush-input" 
                          id="customCheck'.$BuyerID.'" 
                          onclick="ChangeBuyerStatus('.$BuyerID.',1);" >
                          <label class="custom-control-label noCush-label mb-3" 
                          	for="customCheck'.$BuyerID.'; ?>" 
                          	></label>
                        </div>';
			$Response['StatusHtml'] = $Status;
        }
		echo json_encode($Response);
		exit();
	}
	public function ChangeBuyerTestMode(Request $request)
	{
		$Data 		= $request->all();
		$Response 	= array();
		$BuyerID   	 	   = $Data['BID'];
		$Details['BuyerTestMode'] = $Data['Mode'];
		$ChangeBuyerTestMode = $this->BMModel->SaveBuyerDetails($BuyerID,$Details);
		if($ChangeBuyerTestMode)
		{
			$Massage = '<div class="alert alert-success alert-dismissible fade show" role="alert">
						  Buyer Mode Has Been Changed.
						  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
						    <span aria-hidden="true">&times;</span>
						  </button>
						</div>';
			$Response['Status'] = 1;
			$Response['Msg'] = $Massage;
		}
		else
		{
			$Massage = '<div class="alert alert-danger alert-dismissible fade show" role="alert">
						  Something Wrong Please Try Again.
						  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
						    <span aria-hidden="true">&times;</span>
						  </button>
						</div>';
			$Response['Status'] = 0;
			$Response['Msg'] = 1;
		}
		if($Data['Mode']=='1')
		{
			$Mode = '<a href="javascript:void(0);" 
                   		class="btn btn-danger cfonnT cbtnPadd noBoradi"
                   		onclick="ChangeBuyerTestMode('.$BuyerID.',0);">
						<i class="fas fa-check-square sqFont" aria-hidden="true"></i> 
						TesTMode
					</a> ';
			$Response['ModeHtml'] = $Mode;
		}
		else
		{
			$Mode = '<a href="javascript:void(0);" 
                   		class="btn btn-success cfonnT cbtnPadd noBoradi"
                   		onclick="ChangeBuyerTestMode('.$BuyerID.',1);">
						<i class="fas fa-square sqFont" aria-hidden="true"></i> 
						LiveMode
					</a> ';
			$Response['ModeHtml'] = $Mode;
		}
		echo json_encode($Response);
		exit();
	}

	public function SetSessionForCloneBuyer(Request $request)
	{
		$data   		= $request->all();
		$BuyerID 		= $data['BuyerID'];
		$ShowInactive 	= '1';
		$ShowTest 		= '0';
		$Details=$this->BMModel->BuyerDetails($BuyerID);
		if(!empty($Details))
		{
			$CompanyID = $Details->CompanyID;
			$CampaignID = $Details->CampaignID;
			$CampaignDetails = $this->CModel->GetCampaignDetails($CampaignID);
			$ShowActiveOrTest = $this->CModel->GetCompanyDetails($CompanyID);

			if(!empty($ShowActiveOrTest))
			{
				$ShowInactive 	= $ShowActiveOrTest->CompanyStatus; //1
				$ShowTest 		= $ShowActiveOrTest->CompanyTestMode; //0
			}
			$BuyerDetails=array('BuyerID' => $Details->BuyerID,
							    'CompanyID' => $Details->CompanyID,
							    'CampaignID' => $Details->CampaignID,
							    'CampaignType' => $CampaignDetails->CampaignType,
							    'BuyerTierID' => $Details->BuyerTierID,
							    'AdminLabel' => $Details->AdminLabel,
							    'PartnerLabel' => $Details->PartnerLabel,
							    'IntegrationFileName' => $Details->IntegrationFileName,
							    'PayoutCalculation' => $Details->PayoutCalculation,
							    'FixedPrice' => $Details->FixedPrice,
							    'BuyerStatus' => $Details->BuyerStatus,
							    'BuyerTestMode' => $Details->BuyerTestMode,
							    'DailyCap' => $Details->DailyCap,
							    'TotalCap' => $Details->TotalCap,
							    'PingTestURL' => $Details->PingTestURL,
							    'PingLiveURL' => $Details->PingLiveURL,
							    'PostTestURL' => $Details->PostTestURL,
							    'PostLiveURL' => $Details->PostLiveURL,
							    'RequestMethod' => $Details->RequestMethod,
							    'RequestHeader' => $Details->RequestHeader,
							    'MaxTimeOut' => $Details->MaxTimeOut,
							    'MinTimeOut' => $Details->MinTimeOut,
							    'MaxPingTimeOut'=> $Details->MaxPingTimeOut,
								'MaxPostTimeOut'=> $Details->MaxPostTimeOut,
								'MaxPingsPerMinute'=>$Details->MaxPingsPerMinute,
							    'Parameter1' => $Details->Parameter1,
							    'Parameter2' => $Details->Parameter2,
							    'Parameter3' => $Details->Parameter3,
							    'Parameter4' => $Details->Parameter4,
							    'UploadedFiles' => $Details->UploadedFiles,
							    'BuyerNotes' =>$Details->BuyerNotes,
							    'ShowTest' =>$ShowTest,
							    'ShowInactive' =>$ShowInactive,
								'IfClone' => "Yes");
			$request->session()->put('BuyerDetails', $BuyerDetails);
			$request->session()->save();
			echo $BuyerID;
		}
		else
		{
			echo 0;
		}
		exit();
	}

	public function BuyerEditView(Request $request,$BuyerID)
	{
		if(!$request->session()->has('user_id'))
		{
		    return redirect()->action('LoginController@Login');
		}
		$BuyerID 					= base64_decode($BuyerID);
		$Data['CompanyList'] 		= $this->VMModel->CompanyList();
		$BuyerDetails 				= $this->BMModel->BuyerDetails($BuyerID);
		$Data['BuyerDetails'] 		= $BuyerDetails;
		$Data['CampaignDropDown'] 	= $this->MasterConfigModel->getCampaignList();
		$CampaignID 				= $BuyerDetails->CampaignID;
		$Data['CampaignDetails'] 	= $this->CModel->GetCampaignDetails($CampaignID);
		$Data['Menu']   = "Account";
        $Data['Title']  = "Admin | BuyerManagement";
        
		return view('BuyerEditView')->with($Data);
	}
	public function SaveBuyerDetails(Request $request)
	{
		$Data 							= $request->all();
		
		$CampaignID     				= $Data['CampaignID'];
		$CompanyID      				= $Data['Company'];
		$BuyerID      					= $Data['BuyerID'];
		$Details['CampaignID']      	= $Data['CampaignID'];
		$Details['CompanyID']      		= $Data['Company'];
		$Details['AdminLabel']      	= $Data['AdminLabel'];
		$Details['PartnerLabel']      	= $Data['PartnerLabel'];
		$Details['IntegrationFileName'] = $Data['IntegrationFileName'];
		$CampaignDetails = $this->CModel->GetCampaignDetails($CampaignID);
		if($CampaignDetails->CampaignType=='DirectPost')
		{
			$Details['BuyerTierID']  = $Data['BuyerTierID'];

			$Details['MaxTimeOut']   = $Data['MaxTimeOut'];
			$Details['MinTimeOut']   = $Data['MinTimeOut'];

			$Details['PostTestURL']   = $Data['DirectPostTestURL'];
			$Details['PostLiveURL']   = $Data['DirectPostLiveURL'];
		}
		else if($CampaignDetails->CampaignType=='PingPost')
		{
			$Details['BuyerTierID']  = '';

			$Details['MaxPingTimeOut']   = $Data['MaxPingTimeOut'];
			$Details['MaxPostTimeOut']   = $Data['MaxPostTimeOut'];
			$Details['MaxPingsPerMinute']= $Data['MaxPingsPerMinute'];

			$Details['PingTestURL']   = $Data['PingTestURL'];
			$Details['PingLiveURL']   = $Data['PingLiveURL'];

			$Details['PostTestURL']   = $Data['PostTestURL'];
			$Details['PostLiveURL']   = $Data['PostLiveURL'];
		}
		$Details['PayoutCalculation']   = $Data['PayoutCalculation'];
		if($Data['PayoutCalculation']=='Revshare')
		{
			$Details['FixedPrice']   = $Data['Revshare'];
		}
		else if($Data['PayoutCalculation']=='FixedPrice')
		{
			$Details['FixedPrice']   = $Data['FixedPrice'];	
		}

		$Details['DailyCap'] 	= $Data['DailyCap'];
		$Details['TotalCap'] 	= $Data['TotalCap'];
		
		$Details['BuyerStatus'] = 0;
		if(isset($Data['BuyerStatus']) && $Data['BuyerStatus']=='on')
		{
			$Details['BuyerStatus'] = 1;
		}
		$Details['BuyerTestMode'] = 1;
		if(isset($Data['BuyerTestMode']) && $Data['BuyerTestMode']=='on')
		{
			$Details['BuyerTestMode'] = 0;
		}	
		$Details['EWSStatus'] = 0;
		if(isset($Data['EWSStatus']) && $Data['EWSStatus']=='on')
		{
			$Details['EWSStatus'] = 1;
		}
		$Details['RequestMethod'] 	= $Data['RequestMethod'];
		$Details['RequestHeader'] 	= $Data['RequestHeader'];

		$Details['Parameter1'] 	= $Data['Parameter1'];
		$Details['Parameter2'] 	= $Data['Parameter2'];
		$Details['Parameter3'] 	= $Data['Parameter3'];
		$Details['Parameter4'] 	= $Data['Parameter4'];

		$Details['BuyerNotes']   		= html_entity_decode($Data['BuyerNotes']);

		$UploadedFiles = "";		
		$OldUploadedFilesArray=array();		
		if(isset($Data['OldUploadedFiles']))
		{
			$i=0;
			foreach($Data['OldUploadedFiles'] as $val)
			{         
				$SampleFiles=array('id'=>$i,'filename'=>$val,'filepath'=>'/uploads/'.$val);
				$i++;
				array_push($OldUploadedFilesArray,$SampleFiles); 	            
			}
		}
        
		$UploadedFilesArray=array();
		if(!empty($Data['files']))
        {
        	$FilesArray  = $Data['files'];
        	$j = count($OldUploadedFilesArray);
	        foreach($FilesArray as $file)
	        {         
	            $filepath 	= 'uploads';
	            $filename 	= $file->getClientOriginalName();
				$file->move($filepath, $filename);
	            $SampleFiles=array('id'=>$j,'filename'=>$filename,'filepath'=>'/uploads/'.$filename);
	            $j++;
	            array_push($UploadedFilesArray,$SampleFiles); 	            
	        }
        }        
        if(!empty($OldUploadedFilesArray) && !empty($UploadedFilesArray))
        {
        	$UploadedFiles = array_merge($OldUploadedFilesArray,$UploadedFilesArray);
        	$UploadedFiles = json_encode($UploadedFiles);
        }
        else if(empty($OldUploadedFilesArray) && !empty($UploadedFilesArray))
        {
        	$UploadedFiles = json_encode($UploadedFilesArray);
        }
        else if(!empty($OldUploadedFilesArray) && empty($UploadedFilesArray))
        {
        	$UploadedFiles = json_encode($OldUploadedFilesArray);
        }

        $Details['UploadedFiles'] 	= $UploadedFiles;
		/*echo "<pre>";
		print_r($Data);
		print_r($Details);
		exit();*/
		$SaveBuyerDetails = $this->BMModel->SaveBuyerDetails($BuyerID,$Details);
		if($SaveBuyerDetails)
		{
			Session::flash('message', 'Buyer Details Has been Added.');
            Session::flash('alert-class', 'alert-success');
            if(isset($Data['From']) && $Data['From']=='Company')
            {
            	return Redirect::route('admin.CompanyEditView',base64_encode($CompanyID));      
            }
            else
            {
            	return Redirect::route('admin.BuyerManagementView');            	
            }
		}
		else
		{
			Session::flash('message', 'OOPS! Something Wrong Please Try Again.');
            Session::flash('alert-class', 'alert-danger');
            if(isset($Data['From']) && $Data['From']=='Company')
            {
            	return Redirect::route('admin.CompanyEditView',base64_encode($CompanyID));      
            }
            else
            {
            	return Redirect::route('admin.BuyerManagementView');            	
            }
		}
	}

	public function BuyerAddView(Request $request)
	{
		if(!$request->session()->has('user_id'))
		{
		    return redirect()->action('LoginController@Login');
		}
		
		$Data['CompanyList'] 		= $this->VMModel->CompanyList();		
		$Data['CampaignDropDown'] 	= $this->MasterConfigModel->getCampaignList();
		$Data['Menu']   = "Account";
        $Data['Title']  = "Admin | BuyerManagement";
        
		return view('BuyerAddView')->with($Data);
	}
	public function AddBuyerDetails(Request $request)
	{
		$Data 							= $request->all();
		$CampaignID     				= $Data['Campaign'];
		$CompanyID     					= $Data['Company'];
		$Details['CampaignID']      	= $CampaignID;
		$Details['CompanyID']      		= $Data['Company'];
		$Details['AdminLabel']      	= $Data['AdminLabel'];
		$Details['PartnerLabel']      	= $Data['PartnerLabel'];
		$Details['IntegrationFileName'] = $Data['IntegrationFileName'];
		$CampaignDetails = $this->CModel->GetCampaignDetails($CampaignID);
	
		
		if($CampaignDetails->CampaignType=='DirectPost')
		{
			$BuyerTierID  = '';
			if(isset($Data['BuyerTierID']) && $Data['BuyerTierID']!='')
			{
				$Details['BuyerTierID']   = $Data['BuyerTierID'];
			}
			$Details['MaxTimeOut']   = $Data['MaxTimeOut'];
			$Details['MinTimeOut']   = $Data['MinTimeOut'];
			$Details['PostTestURL']   = $Data['DirectPostTestURL'];
			$Details['PostLiveURL']   = $Data['DirectPostLiveURL'];
			
			$Details['PostTestURL']   = '';
			$Details['PostLiveURL']   = '';
			if(isset($Data['PostTestURL']) && $Data['PingTestURL']!='')
			{
				$Details['PostTestURL']   = $Data['DirectPostTestURL'];
			}
			if(isset($Data['PostTestURL']) && $Data['PingTestURL']!='')
			{
				$Details['PostLiveURL']   = $Data['DirectPostLiveURL'];
			}
		}
		else if($CampaignDetails->CampaignType=='PingPost')
		{
			$BuyerTierID  = '';
			
			$Details['MaxPingTimeOut']   = $Data['MaxPingTimeOut'];
			$Details['MaxPostTimeOut']   = $Data['MaxPostTimeOut'];
			$Details['MaxPingsPerMinute']= $Data['MaxPingsPerMinute'];
			$Details['PingTestURL']   = '';
			$Details['PingLiveURL']   = '';
			$Details['PostTestURL']   = '';
			$Details['PostLiveURL']   = '';
			if(isset($Data['PingTestURL']) && $Data['PingTestURL']!='')
			{
				$Details['PingTestURL']   = $Data['PingTestURL'];
			}
			if(isset($Data['PingLiveURL']) && $Data['PingTestURL']!='')
			{
				$Details['PingLiveURL']   = $Data['PingLiveURL'];
			}
			if(isset($Data['PostTestURL']) && $Data['PingTestURL']!='')
			{
				$Details['PostTestURL']   = $Data['PostTestURL'];
			}
			if(isset($Data['PostTestURL']) && $Data['PingTestURL']!='')
			{
				$Details['PostLiveURL']   = $Data['PostLiveURL'];
			}
		}
		$Details['BuyerTierID']  = $BuyerTierID;
		$Details['PayoutCalculation']   = $Data['PayoutCalculation'];
		if($Data['PayoutCalculation']=='Revshare')
		{
			$Details['FixedPrice']   = $Data['Revshare'];
		}
		else if($Data['PayoutCalculation']=='FixedPrice')
		{
			$Details['FixedPrice']   = $Data['FixedPrice'];	
		}
		$Details['DailyCap'] 	= $Data['DailyCap'];
		$Details['TotalCap'] 	= $Data['TotalCap'];
		
		$Details['BuyerStatus'] = 0;
		if(isset($Data['BuyerStatus']) && $Data['BuyerStatus']=='on')
		{
			$Details['BuyerStatus'] = 1;
		}
		$Details['BuyerTestMode'] = 1;
		if(isset($Data['BuyerTestMode']) && $Data['BuyerTestMode']=='on')
		{
			$Details['BuyerTestMode'] = 0;
		}
		$Details['EWSStatus'] = 0;
		if(isset($Data['EWSStatus']) && $Data['EWSStatus']=='on')
		{
			$Details['EWSStatus'] = 1;
		}	
		$Details['RequestMethod'] 	= $Data['RequestMethod'];
		$Details['RequestHeader'] 	= $Data['RequestHeader'];
		if(isset($Data['Parameter1']) && $Data['Parameter1']!='')
		{
			$Details['Parameter1']   = $Data['Parameter1'];
		}
		if(isset($Data['Parameter2']) && $Data['Parameter2']!='')
		{
			$Details['Parameter2']   = $Data['Parameter2'];
		}
		if(isset($Data['Parameter3']) && $Data['Parameter3']!='')
		{
			$Details['Parameter3']   = $Data['Parameter3'];
		}
		if(isset($Data['Parameter4']) && $Data['Parameter4']!='')
		{
			$Details['Parameter4']   = $Data['Parameter4'];
		}
		$Details['BuyerNotes']   		= html_entity_decode($Data['BuyerNotes']);
		$UploadedFiles = "";		
		$UploadedFilesArray=array();
		if(!empty($Data['filesAdd']))
        {
        	$FilesArray  = $Data['filesAdd'];
        	$j = 0;
	        foreach($FilesArray as $file)
	        {         
	            $filepath 	= 'uploads';
	            $filename 	= $file->getClientOriginalName();
				$file->move($filepath, $filename);
	            $SampleFiles=array('id'=>$j,'filename'=>$filename,'filepath'=>'/uploads/'.$filename);
	            $j++;
	            array_push($UploadedFilesArray,$SampleFiles); 	            
	        }
        }  
        if(!empty($UploadedFilesArray))
        {
        	$UploadedFiles = json_encode($UploadedFilesArray);
        }
        $Details['UploadedFiles'] 	= $UploadedFiles;
		/*echo "<pre>";
		print_r($Details);
		exit();*/
		$AddBuyerDetails = $this->BMModel->AddBuyerDetails($Details);
		if($AddBuyerDetails)
		{
			Session::flash('message', 'Buyer Details Has been Added.');
            Session::flash('alert-class', 'alert-success');
            if(isset($Data['From']) && $Data['From']=='Company')
            {
            	return Redirect::route('admin.CompanyEditView',base64_encode($CompanyID));      
            }
            else
            {
            	return Redirect::route('admin.BuyerManagementView');            	
            }
		}
		else
		{
			Session::flash('message', 'OOPS! Something Wrong Please Try Again.');
            Session::flash('alert-class', 'alert-danger');
            if(isset($Data['From']) && $Data['From']=='Company')
            {
            	return Redirect::route('admin.CompanyEditView',base64_encode($CompanyID));      
            }
            else
            {
            	return Redirect::route('admin.BuyerManagementView');            	
            }
		}
	}

	public function GetBuyerPartnetLabel(Request $request)
	{
		$Data 		= $request->all();			
		$BuyerID  	= $Data['BuyerID'];
		$GetPartnerLabel = $this->BMModel->BuyerDetails($BuyerID);
		?>
		<td class="VendClas">
       		<input id="PartnerLabelText_<?php echo $GetPartnerLabel->BuyerID; ?>" type="text" value="<?php echo $GetPartnerLabel->PartnerLabel; ?>" class="form-control txtPadd inFl" placeholder=""> 
       		<div class="vbDiv">
	       		<img class="saveIcon" 
	       			src="<?php echo asset('Admin/images/check.png'); ?>" 
	   				title="save" alt="save-icon" onclick="SaveBuyerPartnetLabel('Save','<?php echo $GetPartnerLabel->BuyerID; ?>');">
	       		<img class="canIcon" 
	       			src="<?php echo asset('Admin/images/cancel.png'); ?>" 
	       			title="cancel" alt="cancel-icon" onclick="SaveBuyerPartnetLabel('Cancel','<?php echo $GetPartnerLabel->BuyerID; ?>');">
       		</div>
       </td>
       	<td><?php echo $GetPartnerLabel->BuyerID; ?></td>
       	<td><?php echo $GetPartnerLabel->PayoutCalculation; ?></td>
       	<td><?php echo $GetPartnerLabel->FixedPrice; ?></td>
       	<td><?php echo $GetPartnerLabel->DailyCap; ?></td>
       	<td><?php echo $GetPartnerLabel->TotalCap; ?></td>
       	<td><?php echo $GetPartnerLabel->MaxTimeOut; ?></td>
       	<td><?php echo $GetPartnerLabel->MinTimeOut; ?></td>
       	<td>
       		<?php if($GetPartnerLabel->BuyerStatus=='1'){ ?>
       		<div class="custom-control custom-checkbox noCush greenCh">
              <input type="checkbox" checked="true" disabled="" class="custom-control-input noCush-input green-input" id="customCheck298">
              <label class="custom-control-label noCush-label green-label mb-3" for="customCheck298" onclick="StatusAlert();"></label>
            </div>
       		<?php } else { ?>
       		<div class="custom-control custom-checkbox noCush">
              <input type="checkbox" disabled="" class="custom-control-input noCush-input" id="customCheck207">
              <label class="custom-control-label noCush-label mb-3" for="customCheck207" onclick="StatusAlert();"></label>
            </div>
       		<?php }?>
       	</td>
       	<td>
       		<?php if($GetPartnerLabel->BuyerTestMode=='0'){ ?>
       		<span class="leadAccpBtn cfonnT cbtnPadd noBoradi">
       			<i class="fas fa-check-square sqFont" aria-hidden="true"></i> Live Mode
            </span>
       		<?php } else { ?>
       		<span class="leadRejBtn cfonnT cbtnPadd noBoradi">
       			<i class="fas fa-check-square sqFont" aria-hidden="true"></i> Test Mode
            </span>
       		<?php }?>
       	</td>
		<?php
		exit();
	}
	public function SaveBuyerPartnetLabel(Request $request)
	{
		$Data 		= $request->all();			
		$BuyerID  	= $Data['BuyerID'];
		$PartnerLabel  	= $Data['PartnerLabel'];
		$Action  	= $Data['Action'];		
		if($Action=='Save')
		{
			$Details['PartnerLabel'] = $PartnerLabel;
			$SavePartnetLabel 	= $this->BMModel->SaveBuyerDetails($BuyerID,$Details);
			$GetPartnerLabel 	= $this->BMModel->BuyerDetails($BuyerID);
			?>
			<td onclick="GetBuyerPartnetLabel('<?php echo $GetPartnerLabel->BuyerID; ?>');" class="VendClas">
           		<?php echo $GetPartnerLabel->PartnerLabel; ?>
           		<img class="queIcon" 
           			src="https://dashboardv2-dev.revjolt.net/Admin/images/help.png" 
           			title="Double Click To Change The Buyer Name." alt="save-icon">
           	</td>
	       	<td><?php echo $GetPartnerLabel->BuyerID; ?></td>
	       	<td><?php echo $GetPartnerLabel->PayoutCalculation; ?></td>
	       	<td><?php echo $GetPartnerLabel->FixedPrice; ?></td>
	       	<td><?php echo $GetPartnerLabel->DailyCap; ?></td>
	       	<td><?php echo $GetPartnerLabel->TotalCap; ?></td>
	       	<td><?php echo $GetPartnerLabel->MaxTimeOut; ?></td>
	       	<td><?php echo $GetPartnerLabel->MinTimeOut; ?></td>
	       	<td>
	       		<?php if($GetPartnerLabel->BuyerStatus=='1'){ ?>
	       		<div class="custom-control custom-checkbox noCush greenCh">
	              <input type="checkbox" checked="true" disabled="" class="custom-control-input noCush-input green-input" id="customCheck298">
	              <label class="custom-control-label noCush-label green-label mb-3" for="customCheck298" onclick="StatusAlert();"></label>
	            </div>
	       		<?php } else { ?>
	       		<div class="custom-control custom-checkbox noCush">
	              <input type="checkbox" disabled="" class="custom-control-input noCush-input" id="customCheck207">
	              <label class="custom-control-label noCush-label mb-3" for="customCheck207" onclick="StatusAlert();"></label>
	            </div>
	       		<?php }?>
	       	</td>
	       	<td>
	       		<?php if($GetPartnerLabel->BuyerTestMode=='0'){ ?>
	       		<span class="leadAccpBtn cfonnT cbtnPadd noBoradi">
	       			<i class="fas fa-check-square sqFont" aria-hidden="true"></i> Live Mode
	            </span>
	       		<?php } else { ?>
	       		<span class="leadRejBtn cfonnT cbtnPadd noBoradi">
	       			<i class="fas fa-check-square sqFont" aria-hidden="true"></i> Test Mode
	            </span>
	       		<?php }?>
	       	</td>
			<?php
		}
		else
		{
			$GetPartnerLabel = $this->BMModel->BuyerDetails($BuyerID);
			?>
			<td onclick="GetBuyerPartnetLabel('<?php echo $GetPartnerLabel->BuyerID; ?>');" class="VendClas">
           		<?php echo $GetPartnerLabel->PartnerLabel; ?>
           		<img class="queIcon" 
           			src="https://dashboardv2-dev.revjolt.net/Admin/images/help.png" 
           			title="Double Click To Change The Buyer Name." alt="save-icon">
           	</td>
	       	<td><?php echo $GetPartnerLabel->BuyerID; ?></td>
	       	<td><?php echo $GetPartnerLabel->PayoutCalculation; ?></td>
	       	<td><?php echo $GetPartnerLabel->FixedPrice; ?></td>
	       	<td><?php echo $GetPartnerLabel->DailyCap; ?></td>
	       	<td><?php echo $GetPartnerLabel->TotalCap; ?></td>
	       	<td><?php echo $GetPartnerLabel->MaxTimeOut; ?></td>
	       	<td><?php echo $GetPartnerLabel->MinTimeOut; ?></td>
	       	<td>
	       		<?php if($GetPartnerLabel->BuyerStatus=='1'){ ?>
	       		<div class="custom-control custom-checkbox noCush greenCh">
	              <input type="checkbox" checked="true" disabled="" class="custom-control-input noCush-input green-input" id="customCheck298">
	              <label class="custom-control-label noCush-label green-label mb-3" for="customCheck298" onclick="StatusAlert();"></label>
	            </div>
	       		<?php } else { ?>
	       		<div class="custom-control custom-checkbox noCush">
	              <input type="checkbox" disabled="" class="custom-control-input noCush-input" id="customCheck207">
	              <label class="custom-control-label noCush-label mb-3" for="customCheck207" onclick="StatusAlert();"></label>
	            </div>
	       		<?php }?>
	       	</td>
	       	<td>
	       		<?php if($GetPartnerLabel->BuyerTestMode=='0'){ ?>
	       		<span class="leadAccpBtn cfonnT cbtnPadd noBoradi">
	       			<i class="fas fa-check-square sqFont" aria-hidden="true"></i> Live Mode
	            </span>
	       		<?php } else { ?>
	       		<span class="leadRejBtn cfonnT cbtnPadd noBoradi">
	       			<i class="fas fa-check-square sqFont" aria-hidden="true"></i> Test Mode
	            </span>
	       		<?php }?>
	       	</td>
			<?php
		}
		exit();
	}

	public function DeleteBuyerUploadedFile(Request $request)
	{
		$data   = $request->all();	

		$BuyerID 	= $data['BuyerID'];
		$ID 		= $data['ID'];
		$BuyerDetails=$this->BMModel->BuyerDetails($BuyerID);
		$Array = $BuyerDetails->UploadedFiles;
		$UploadedFileArray = json_decode($Array);
		$NewUploadedFileArray =array();		
		$i=0;
		foreach($UploadedFileArray as $ufa)
		{
			if($ufa->id!=$ID)
			{
				$Sample=array("id"=>$i,
							"filename"=>$ufa->filename,
							"filepath"=>'/uploads/'.$ufa->filename);
				array_push($NewUploadedFileArray,$Sample); 	
				$i++;
			}			
		}
		$UploadedFileJson='';
		if(!empty($NewUploadedFileArray))
		{
			$UploadedFileJson = json_encode($NewUploadedFileArray);			
		}
		
		$Details=array('UploadedFiles'=>$UploadedFileJson);

		$Update=$this->BMModel->SaveBuyerDetails($BuyerID,$Details);
		$Response=array();
		if($Update)
		{
			$Message = '<div class="alert alert-success alert-dismissible fade show" role="alert">
						  File Deleted Successfully!.
						  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
						    <span aria-hidden="true">&times;</span>
						  </button>
						</div>';
			$Response['Status'] = 1; 
			$Response['Message'] = $Message; 
		}
		else
		{
			$Message = '<div class="alert alert-danger alert-dismissible fade show" role="alert">
						  OOPS! Something Worng. Please Try Again.
						  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
						    <span aria-hidden="true">&times;</span>
						  </button>
						</div>';
			$Response['Status'] = 1;
			$Response['Message'] = $Message; 
		}

		echo json_encode($Response);
		exit();		
	}

	/////////////////////
	public function GetBuyerDetails(Request $request)
	{
		$Data   = $request->all();	

		$BuyerID 			= $Data['BuyerID'];
		$BuyerDetails 		= $this->BMModel->BuyerDetails($BuyerID);
		$BuyerDetails 		= $BuyerDetails;
		$CampaignDropDown 	= $this->MasterConfigModel->getCampaignList();
		$CampaignID 		= $BuyerDetails->CampaignID;
		$CampaignDetails 	= $this->CModel->GetCampaignDetails($CampaignID);

		$CompanyID 			= $BuyerDetails->CompanyID;
		$CompanyDetails		= $this->VMModel->CompanyDetails($CompanyID);

		$CompanyTestMode 	= $CompanyDetails->CompanyTestMode;
		$CompanyStatus 		= $CompanyDetails->CompanyStatus;

		$CompanyList		= $this->VMModel->CompanyListForEdit($CompanyTestMode,$CompanyStatus);

		
		$CompanyTestMode 	= $CompanyDetails->CompanyTestMode;
		$CompanyStatus 		= $CompanyDetails->CompanyStatus;
		$ShowInactive="checked";
		$ShowTest="";
		if($CompanyStatus==1)
		{
			$ShowInactive="";
		}
		if($CompanyTestMode==1)
		{
			$ShowTest="checked";
		}
		?>
		<form action="<?php echo route('admin.SaveBuyerDetails'); ?>" name="BuyerUpdateForm" 
	      id="BuyerUpdateForm" method="POST" enctype="multipart/form-data">
	      <input type="hidden" name="BuyerID" id="BuyerID" value="<?php echo $BuyerDetails->BuyerID; ?>">
	      <input type="hidden" name="_token" id="_token" value="<?php echo csrf_token(); ?>">
	      <input type="hidden" name="CampaignType" id="CampaignTypeEdit" value="<?php echo $CampaignDetails->CampaignType; ?>">
	      <input type="hidden" name="CampaignID" id="CampaignIDEdit" value="<?php echo $CampaignDetails->CampaignID; ?>">
	      <input type="hidden" name="FormType" id="FormType" value="Update">
	      <div class="row">
	        <div class="col-md-2 form-group mb-0 tadmPadd labFontt">
	          <label for="exampleFormControlSelect1">Buyer ID:</label>
	          <input type="text" disabled class="form-control txtPadd" 
	          value="<?php echo $BuyerDetails->BuyerID; ?>" placeholder="">
	        </div>                          
	        <div class="col-md-3 form-group mb-0 tadmPadd labFontt">
	            <label for="exampleFormControlSelect1">Campaign:</label>
	            <select class="form-control fonT" id="CampaignEdit" name="Campaign"  
	                onchange="GetCampaignDetailsEdit(),HideErr('CompanyEditErr');">
	                <?php foreach($CampaignDropDown as $cdd) {?>
	                <option value="<?php echo $cdd->CampaignID; ?>"
	                  <?php if($cdd->CampaignID==$BuyerDetails->CampaignID){echo "selected";}?>
	                  ><?php echo $cdd->CampaignName; ?></option>
	                <?php }?>
	            </select>
	            <span id="CampaignEditErr"></span>
	        </div>
	        <div class="col-md-4 form-group mb-0 tadmPadd labFontt">
	          <label for="exampleFormControlSelect1">Company:</label>
	            <select class="form-control fonT" id="CompanyEdit" name="Company"
	              onchange="HideErr('CompanyEditErr');">
	              <option value="">Select Company</option>
	              <?php foreach($CompanyList as $cl){?>
	              <option value="<?php echo $cl->CompanyID; ?>"
	              <?php if($cl->CompanyID==$BuyerDetails->CompanyID){echo "selected";}?>
	              ><?php echo $cl->CompanyName; ?></option>
	              <?php }?>                                
	            </select>
	            <span id="CompanyEditErr"></span>
	            <div class="form-check form-check-flat form-check-primary adComp adComp-primary disPlay tabdisPlay tMrg">
		            <label class="form-check-label adComp-label">
		            <input type="checkbox" class="form-check-input" id="ShowInactiveEdit"
		            onclick="GetCompanyUsingShwowInactiveAndTest3();" <?php echo $ShowInactive; ?>>
		              Show Inactive
		            <i class="input-helper"></i></label>
		          </div>
		          <div class="form-check form-check-flat form-check-primary adComp adComp-primary disPlay tabdisPlay tMrg">
		            <label class="form-check-label adComp-label">
		            <input type="checkbox" class="form-check-input" id="ShowTestEdit"
		            onclick="GetCompanyUsingShwowInactiveAndTest3();" <?php echo $ShowTest; ?>>
		              Show Test
		            <i class="input-helper"></i></label>
		          </div>
	        </div>
	        
	       </div>
	      <div class="row">
	        <div class="col-md-2 form-group tadmPadd labFontt">
	          <label for="exampleInputEmail1">Admin Label:</label>
	            <input type="text" class="form-control txtPadd" 
	            value="<?php echo $BuyerDetails->AdminLabel; ?>" id="AdminLabelEdit" name="AdminLabel"
	            onkeypress="HideErr('AdminLabelEditErr');">
	             <span id="AdminLabelEditErr"></span>
	        </div>
	        <div class="col-md-1 form-group tadmPadd btnCen">
	          <button type="button" id="CopyAdminLable" onclick="CopyAdminLableEdit();" class="btn btn-success noBoradi mr-2">&gt;&gt;</button>
	        </div>
	        <div class="col-md-2 form-group tadmPadd labFontt">
	          <label for="exampleInputEmail1">Partner Label:</label>
	            <input type="text" class="form-control txtPadd" 
	              value="<?php echo $BuyerDetails->PartnerLabel; ?>" id="PartnerLabelEdit" name="PartnerLabel"
	              onkeypress="HideErr('PartnerLabelEditErr');">
	               <span id="PartnerLabelEditErr"></span>
	        </div>
	        <div class="col-md-2 form-group tadmPadd labFontt">
	          <label for="exampleInputEmail1">Integration File Name:</label>
	            <input type="text"value="<?php echo $BuyerDetails->IntegrationFileName; ?>" 
	              id="IntegrationFileNameEdit" name="IntegrationFileName" class="form-control txtPadd" placeholder="Integration File Name"
	              onkeypress="HideErr('IntegrationFileNameEditErr');">
	            <span id="IntegrationFileNameEditErr"></span>
	        </div>
	        <div class="col-md-2 form-group mb-0 tadmPadd labFontt">
	          <label for="PayoutCalculation">Payout Calculation:</label>
	            <select class="form-control fonT" id="PayoutCalculationEdit" name="PayoutCalculation" 
	            	onchange="PayoutCalculationEditFunction();">
	              <option value="RevShare" <?php if($BuyerDetails->PayoutCalculation=="RevShare"){ echo "selected"; } ?>>RevShare</option>
	              <option value="FixedPrice" <?php if($BuyerDetails->PayoutCalculation=="FixedPrice"){ echo "selected"; } ?>>Fixed Price</option>
	            </select>
	        </div>
	        <?php 
	        $PayoutCalculation = $BuyerDetails->PayoutCalculation;
	        if($PayoutCalculation=='FixedPrice')
	        { 
	             $PayoutCalculationFixedPrice="style='display:block'";
	             $PayoutCalculationRevShare="style='display:none'";
	        } 
	        else if($PayoutCalculation=='RevShare')
	        {
	             $PayoutCalculationRevShare="style='display:block'";
	             $PayoutCalculationFixedPrice="style='display:none'";
	        }
	        ?>
	        <div class="col-md-2 form-group tadmPadd labFontt" id="RevshareInputEdit" <?php echo $PayoutCalculationRevShare ?>  
	            id="RevsharePercentInput">
	          <label for="exampleInputEmail1">RevShare: </label>
	          <input type="text" class="form-control txtPadd" value="<?php echo $BuyerDetails->FixedPrice; ?>" 
	          placeholder="70" id="RevshareEdit" name="Revshare" onkeypress="HideErr('RevshareEditErr');">
	          <span id="RevshareEditErr"></span>
	        </div>
	        <div class="col-md-2 form-group tadmPadd labFontt" id="FixedPriceInputEdit" <?php echo $PayoutCalculationFixedPrice ?> 
	            id="FixedPriceInput">
	          <label for="exampleInputEmail1">Fixed Price:</label>
	          <input type="text" class="form-control txtPadd" value="<?php echo $BuyerDetails->FixedPrice; ?>" 
	          placeholder="70" id="FixedPriceEdit" name="FixedPrice" onkeypress="HideErr('FixedPriceEditErr');">
	          <span id="FixedPriceEditErr"></span>
	        </div>
	      </div>                         
	      <div class="row">
	        <?php 
	        if($CampaignDetails->CampaignType=="DirectPost"){                           
	          ?>
	          <div class="col-md-2 form-group tadmPadd labFontt">
	            <label for="exampleInputEmail1">Buyer Tier ID:</label>
	              <input type="text" onkeypress="HideErr('BuyerTierIDErr');" value="<?php echo $BuyerDetails->BuyerTierID; ?>" 
	                id="BuyerTierID" name="BuyerTierID" class="form-control txtPadd" placeholder="Buyer Tier ID">
	              <span id="BuyerTierIDErr"></span>
	          </div>
	          <?php
	        }
	        ?>
	        
	        <div class="col-md-1 form-group tadmPadd labFontt">
	          <label for="exampleInputEmail1">Daily Cap:</label>
	            <input type="text" value="<?php echo $BuyerDetails->DailyCap; ?>" 
	              id="DailyCapEdit" name="DailyCap" class="form-control txtPadd" placeholder="Daily Cap"
	              onkeypress="HideErr('DailyCapEditErr');">
	            <span id="DailyCapEditErr"></span>
	        </div>
	        <div class="col-md-1 form-group tadmPadd labFontt">
	          <label for="exampleInputEmail1">Total Cap:</label>
	            <input type="text" value="<?php echo $BuyerDetails->TotalCap; ?>" 
	              id="TotalCapEdit" name="TotalCap" class="form-control txtPadd" placeholder="Total Cap"
	              onkeypress="HideErr('TotalCapEditErr');">
	            <span id="TotalCapEditErr"></span>
	        </div>
	        <?php if($BuyerDetails->BuyerStatus=='1'){ ?>
	        <div class="col-md-2 form-group tadmPadd labFontt">
	          <label for="exampleInputEmail1">Buyer Status:</label>
	            <div class="onoffswitch8">
	              <input type="checkbox" name="BuyerStatus" class="onoffswitch8-checkbox" 
	              id="BuyerStatusEdit" checked="">
	              <label class="onoffswitch8-label" for="BuyerStatusEdit">
	                <span class="onoffswitch8-inner"></span>
	                <span class="onoffswitch8-switch"></span>
	              </label>
	            </div>
	        </div>
	        <?php } else {?>
	        <div class="col-md-2 form-group tadmPadd labFontt">
	          <label for="exampleInputEmail1">Buyer Status:</label>
	            <div class="onoffswitch8">
	              <input type="checkbox" name="BuyerStatus" class="onoffswitch8-checkbox" 
	              id="BuyerStatusEdit">
	              <label class="onoffswitch8-label" for="BuyerStatusEdit">
	                <span class="onoffswitch8-inner"></span>
	                <span class="onoffswitch8-switch"></span>
	              </label>
	            </div>
	        </div>
	        <?php }?>
	        <?php if($BuyerDetails->BuyerTestMode=='1'){ ?>
	        <div class="col-md-2 form-group tadmPadd labFontt">
	          <label for="exampleInputEmail1">Buyer Mode?</label>
	            <div class="onoffswitch9">
	              <input type="checkbox" name="BuyerTestMode" class="onoffswitch9-checkbox"
	                 id="BuyerTestMode" >
	              <label class="onoffswitch9-label" for="BuyerTestMode">
	                <span class="onoffswitch9-inner"></span>
	                <span class="onoffswitch9-switch"></span>
	              </label>
	            </div>
	        </div>
	        <?php } else {?>
	        <div class="col-md-2 form-group tadmPadd labFontt">
	          <label for="exampleInputEmail1">Buyer Mode?</label>
	            <div class="onoffswitch9">
	              <input type="checkbox" name="BuyerTestMode" class="onoffswitch9-checkbox" 
	                id="BuyerTestMode" checked="">
	              <label class="onoffswitch9-label" for="BuyerTestMode">
	                <span class="onoffswitch9-inner"></span>
	                <span class="onoffswitch9-switch"></span>
	              </label>
	            </div>
	        </div>
	        <?php }?>
	        <?php if($BuyerDetails->EWSStatus=='1'){ ?>
	        <div class="col-md-2 form-group tadmPadd labFontt">
              <label for="exampleInputEmail1">EWS Status</label>
                <div class="onoffswitch8">
                  <input type="checkbox" name="EWSStatus" class="onoffswitch8-checkbox"
                     id="EWSStatusAddBuyer" checked="">
                  <label class="onoffswitch8-label" for="EWSStatusAddBuyer">
                    <span class="onoffswitch8-inner"></span>
                    <span class="onoffswitch8-switch"></span>
                  </label>
                </div>
            </div>
	        <?php } else {?>
	        <div class="col-md-2 form-group tadmPadd labFontt">
              <label for="exampleInputEmail1">EWS Status</label>
                <div class="onoffswitch8">
                  <input type="checkbox" name="EWSStatus" class="onoffswitch8-checkbox"
                     id="EWSStatusAdd" >
                  <label class="onoffswitch8-label" for="EWSStatusAdd">
                    <span class="onoffswitch8-inner"></span>
                    <span class="onoffswitch8-switch"></span>
                  </label>
                </div>
            </div>
	        <?php }?>

	        <?php 
	        if($CampaignDetails->CampaignType=="DirectPost"){                           
	          $MaxTimeOutInput = "style='display:block;'";
	          $MinTimeOutInput = "style='display:block;'";
	          $MaxPingTimeOutInput = "style='display:none;'";
	          $MaxPostTimeOutInput = "style='display:none;'";
	          $MaxPingsPerMinuteInput = "style='display:none;'";
	        }else if($CampaignDetails->CampaignType=="PingPost"){
	          $MaxTimeOutInput = "style='display:none;'";
	          $MinTimeOutInput = "style='display:none;'";
	          $MaxPingTimeOutInput = "style='display:block;'";
	          $MaxPostTimeOutInput = "style='display:block;'";
	          $MaxPingsPerMinuteInput = "style='display:block;'";
	        }
	        ?>
	        <div class="col-md-2 form-group tadmPadd labFontt" id="MaxTimeOutInputEdit" <?php echo $MaxTimeOutInput; ?>>
	          <label for="exampleInputEmail1">Max Time Out:</label>
	            <input type="text" class="form-control txtPadd" placeholder="0" 
	            id="MaxTimeOutEdit" name="MaxTimeOut" value="<?php echo $BuyerDetails->MaxTimeOut; ?>" onkeypress="HideErr('MaxTimeOutEditErr');">
	            <span id="MaxTimeOutEditErr"></span>
	        </div>
	        <div class="col-md-2 form-group tadmPadd labFontt" id="MinTimeOutInputEdit" <?php echo $MinTimeOutInput; ?>>
	          <label for="exampleInputEmail1">Min Time Out:</label>
	            <input type="text" class="form-control txtPadd" placeholder="0" 
	            id="MinTimeOutEdit" name="MinTimeOut" value="<?php echo $BuyerDetails->MinTimeOut; ?>" onkeypress="HideErr('MinTimeOutEditErr');">
	            <span id="MinTimeOutEditErr"></span>
	        </div>

	        <div class="col-md-2 form-group tadmPadd labFontt" id="MaxPingTimeOutInputEdit" <?php echo $MaxPingTimeOutInput; ?>>
	          <label for="exampleInputEmail1">Max Ping Time Out:</label>
	            <input type="text" class="form-control txtPadd" placeholder="0" 
	            id="MaxPingTimeOutEdit" name="MaxPingTimeOut" value="<?php echo $BuyerDetails->MaxPingTimeOut; ?>" onkeypress="HideErr('MaxPingTimeOutEditErr');">
	            <span id="MaxPingTimeOutEditErr"></span>
	        </div>
	        <div class="col-md-2 form-group tadmPadd labFontt" id="MaxPostTimeOutInputEdit" <?php echo $MaxPostTimeOutInput; ?>>
	          <label for="exampleInputEmail1">Max Post Time Out:</label>
	            <input type="text" class="form-control txtPadd" placeholder="0" 
	            id="MaxPostTimeOutEdit" name="MaxPostTimeOut" value="<?php echo $BuyerDetails->MaxPostTimeOut; ?>" onkeypress="HideErr('MaxPostTimeOutEditErr');">
	            <span id="MaxPostTimeOutEditErr"></span>
	        </div>
	        <div class="col-md-2 form-group tadmPadd labFontt" id="MaxPingsPerMinuteInputEdit" <?php echo $MaxPingsPerMinuteInput; ?>>
	          <label for="exampleInputEmail1">Max Pings Per Minute:</label>
	            <input type="text" class="form-control txtPadd" placeholder="0" 
	            id="MaxPingsPerMinuteEdit" name="MaxPingsPerMinute" value="<?php echo $BuyerDetails->MaxPingsPerMinute; ?>" onkeypress="HideErr('MaxPingsPerMinuteEditErr');">
	            <span id="MaxPingsPerMinuteEditErr"></span>
	        </div>
	      </div>                             
	      <div class="row">
	       		<?php 
		        if($CampaignDetails->CampaignType=="DirectPost"){                           
		          $PingTestURLInputEdit = "style='display:none;'";
		          $PingLiveURLInputEdit = "style='display:none;'";
		          $PostTestURLInputEdit = "style='display:none;'";
		          $PostLiveURLInputEdit = "style='display:none;'";
		          $DirectPostTestURLInputEdit = "style='display:block;'";
		          $DirectPostLiveURLInputEdit = "style='display:block;'";
		        }else if($CampaignDetails->CampaignType=="PingPost"){
		          $PingTestURLInputEdit = "style='display:block;'";
		          $PingLiveURLInputEdit = "style='display:block;'";
		          $PostTestURLInputEdit = "style='display:block;'";
		          $PostLiveURLInputEdit = "style='display:block;'";
		          $DirectPostTestURLInputEdit = "style='display:none;'";
		          $DirectPostLiveURLInputEdit = "style='display:none;'";
		        }
		        ?>
	          <div class="col-md-6 form-group tadmPadd labFontt" id="PingTestURLInputEdit" <?php echo $PingTestURLInputEdit; ?>>
	            <label for="exampleInputEmail1">Ping Test URL:</label>
	              <input type="text" class="form-control txtPadd" 
	              value="<?php echo $BuyerDetails->PingTestURL; ?>" id="PingTestURL" name="PingTestURL"
	              onkeypress="HideErr('PingTestURLErr');">
	               <span id="PingTestURLErr"></span>
	          </div>
	          <div class="col-md-6 form-group tadmPadd labFontt" id="PingLiveURLInputEdit" <?php echo $PingLiveURLInputEdit; ?>>
	            <label for="exampleInputEmail1">Ping Live URL:</label>
	              <input type="text" class="form-control txtPadd" 
	                value="<?php echo $BuyerDetails->PingLiveURL; ?>" id="PingLiveURL" name="PingLiveURL"
	                onkeypress="HideErr('PingLiveURLErr');">
	                 <span id="PingLiveURLErr"></span>
	          </div>
	          <div class="col-md-6 form-group tadmPadd labFontt" id="PostTestURLInputEdit" <?php echo $PostTestURLInputEdit; ?>>
	            <label for="exampleInputEmail1">Post Test URL:</label>
	              <input type="text" class="form-control txtPadd" 
	              value="<?php echo $BuyerDetails->PostTestURL; ?>" id="PostTestURL" name="PostTestURL"
	              onkeypress="HideErr('PostTestURLErr');">
	               <span id="PostTestURLErr"></span>
	          </div>
	          <div class="col-md-6 form-group tadmPadd labFontt" id="PingLiveURLInputEdit" <?php echo $PingLiveURLInputEdit; ?>>
	            <label for="exampleInputEmail1">Post Live URL:</label>
	              <input type="text" class="form-control txtPadd" 
	                value="<?php echo $BuyerDetails->PostLiveURL; ?>" id="PostLiveURL" name="PostLiveURL"
	                onkeypress="HideErr('PostLiveURLErr');">
	                 <span id="PostLiveURLErr"></span>
	          </div>
	         
	          <div class="col-md-6 form-group tadmPadd labFontt" id="DirectPostTestURLInputEdit" <?php echo $DirectPostTestURLInputEdit; ?>>
	            <label for="exampleInputEmail1">DirectPost Test URL:</label>
	              <input type="text" class="form-control txtPadd" 
	              value="<?php echo $BuyerDetails->PostTestURL; ?>" id="DirectPostTestURL" name="DirectPostTestURL"
	              onkeypress="HideErr('DirectPostTestURLErr');">
	               <span id="DirectPostTestURLErr"></span>
	          </div>
	          <div class="col-md-6 form-group tadmPadd labFontt" id="DirectPostLiveURLInputEdit" <?php echo $DirectPostLiveURLInputEdit; ?>>
	            <label for="exampleInputEmail1">DirectPost Live URL:</label>
	              <input type="text" class="form-control txtPadd" 
	                value="<?php echo $BuyerDetails->PostLiveURL; ?>" id="DirectPostLiveURL" name="DirectPostLiveURL"
	                onkeypress="HideErr('DirectPostLiveURLErr');">
	                 <span id="DirectPostLiveURLErr"></span>
	          </div>
	         
	      </div>
	      <div class="row">
	        <div class="col-md-2 form-group mb-0 tadmPadd labFontt">
	            <label for="exampleFormControlSelect1">Request Method:</label>
	            <select class="form-control fonT" id="RequestMethod" name="RequestMethod" >
	                <option value="1" <?php if($BuyerDetails->RequestMethod=='1'){echo "selected";}?>>POST</option>
	                <option value="2" <?php if($BuyerDetails->RequestMethod=='2'){echo "selected";}?>>GET</option>
	                <option value="3" <?php if($BuyerDetails->RequestMethod=='3'){echo "selected";}?>>XmlinPost</option>
	                <option value="4" <?php if($BuyerDetails->RequestMethod=='4'){echo "selected";}?>>XmlPost</option>
	                <option value="5" <?php if($BuyerDetails->RequestMethod=='5'){echo "selected";}?>>InlineXML</option>
	            </select>
	            <span id="RequestMethodErr"></span>
	        </div>
	        <div class="col-md-2 form-group mb-0 tadmPadd labFontt">
	            <label for="exampleFormControlSelect1">Request Header:</label>
	            <select class="form-control fonT" id="RequestHeader" name="RequestHeader" >
	                <option value="1" <?php if($BuyerDetails->RequestHeader=='1'){echo "selected";}?>>Content-Type: application/x-www-form-urlencoded</option>
	                <option value="2" <?php if($BuyerDetails->RequestHeader=='2'){echo "selected";}?>>Content-Type:text/xml; charset=utf-8 </option>
	                <option value="3" <?php if($BuyerDetails->RequestHeader=='3'){echo "selected";}?>>Content-type: application/xml </option>
	                <option value="4" <?php if($BuyerDetails->RequestHeader=='4'){echo "selected";}?>>Content-type: application/json</option>
	            </select>
	            <span id="RequestHeaderErr"></span>
	        </div>
	        <div class="col-md-2 form-group tadmPadd labFontt">
	          <label for="exampleInputEmail1">Parameter1:</label>
	            <input type="text" class="form-control txtPadd" 
	            value="<?php echo $BuyerDetails->Parameter1; ?>" id="Parameter1" name="Parameter1"
	            onkeypress="HideErr('Parameter1Err');">
	             <span id="Parameter1Err"></span>
	        </div>
	        <div class="col-md-2 form-group tadmPadd labFontt">
	          <label for="exampleInputEmail1">Parameter2:</label>
	            <input type="text" class="form-control txtPadd" 
	            value="<?php echo $BuyerDetails->Parameter2; ?>" id="Parameter2" name="Parameter2"
	            onkeypress="HideErr('Parameter2Err');">
	             <span id="Parameter2Err"></span>
	        </div>
	        <div class="col-md-2 form-group tadmPadd labFontt">
	          <label for="exampleInputEmail1">Parameter3:</label>
	            <input type="text" class="form-control txtPadd" 
	            value="<?php echo $BuyerDetails->Parameter3; ?>" id="Parameter3" name="Parameter3"
	            onkeypress="HideErr('Parameter3Err');">
	             <span id="Parameter3Err"></span>
	        </div>
	        <div class="col-md-2 form-group tadmPadd labFontt">
	          <label for="exampleInputEmail1">Parameter4:</label>
	            <input type="text" class="form-control txtPadd" 
	            value="<?php echo $BuyerDetails->Parameter4; ?>" id="Parameter4" name="Parameter4"
	            onkeypress="HideErr('Parameter4Err');">
	             <span id="Parameter4Err"></span>
	        </div>
	      </div>
	       <div class="row">
	        <div class="col-md-4 form-group labFontt tablabFontt">
	          <label>File: (You Can Select Multiple Files)</label>
	          <div class="custom-file">
	            <input type="file" multiple class="custom-file-input" id="files" name="filesEdit[]" multiple>
	            <label class="custom-file-label fileLabel" for="files">No File Chosen</label>
	          </div>
	        </div>
	        <?php
	        if($BuyerDetails->UploadedFiles!='')
	        {
	          ?>
	          <div class="col-md-4 form-group mb-0 labFontt">
	            <span id="UploadFileMessage"></span>
	          <label for="exampleFormControlTextarea1">Uploaded Files:</label>
	            <table class="table">
	              <thead>
	                <tr>
	                  <th>Uploaded File</th>
	                  <th>Action</th>
	                </tr>
	              </thead>
	              <tbody>
	              <?php
	              $UploadedFiles = json_decode($BuyerDetails->UploadedFiles);
	              foreach($UploadedFiles as $upf)
	              {
	                ?>
	                <tr id="UploadedFileTR_<?php echo $upf->id; ?>">
	                  <td>
	                    <a href="<?php echo  asset('uploads').'/'.$upf->filename; ?>" 
	                      target="_blank"><?php echo $upf->filename; ?></a>
	                  </td>
	                  <td>
	                    <a class="btn btn-danger btn-sm" href="javascript:void(0);" 
	                        onclick="DeleteBuyerUploadedFile('<?php echo $BuyerDetails->BuyerID; ?>','<?php echo $upf->id; ?>');">
	                        Delete
	                    </a>
	                    <input type="hidden" name="OldUploadedFiles[]" value="<?php echo $upf->filename; ?>">
	                  </td>
	                </tr>
	                <?php
	              }
	              ?>
	              </tbody>
	            </table>
	          </div>
	          <?php
	        } 
	        ?>
	        <div class="col-md-4 form-group mb-0 labFontt">
	          <label for="exampleFormControlTextarea1">Buyer Notes:</label>
	          <textarea class="form-control" id="BuyerNotes" name="BuyerNotes" rows="3"><?php echo  $BuyerDetails->BuyerNotes; ?></textarea>
	        </div>
	      	</div>
	      	<div class="row btnFlo">
	            <div class="col-md-12 form-group mb-0 labFontt">
	              <button type="button" class="btn btn-success btn-fw noBoradi mr-2" onclick="SaveBuyerDetails();">Update</button>
	              <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
	            </div>
	        </div>
	    </form>
		<?php
	}
	public function GetBuyerCloneDetails(Request $request)
	{
		$Data   = $request->all();	

		$BuyerID 			= $Data['BuyerID'];
		$BuyerDetails 		= $this->BMModel->BuyerDetails($BuyerID);
		$BuyerDetails 		= $BuyerDetails;
		$CampaignDropDown 	= $this->MasterConfigModel->getCampaignList();
		$CampaignID 		= $BuyerDetails->CampaignID;
		$CampaignDetails 	= $this->CModel->GetCampaignDetails($CampaignID);

		$CompanyID 			= $BuyerDetails->CompanyID;
		$CompanyDetails		= $this->VMModel->CompanyDetails($CompanyID);

		$CompanyTestMode 	= $CompanyDetails->CompanyTestMode;
		$CompanyStatus 		= $CompanyDetails->CompanyStatus;

		$CompanyList		= $this->VMModel->CompanyListForEdit($CompanyTestMode,$CompanyStatus);

		
		$CompanyTestMode 	= $CompanyDetails->CompanyTestMode;
		$CompanyStatus 		= $CompanyDetails->CompanyStatus;
		$ShowInactive="checked";
		$ShowTest="";
		if($CompanyStatus==1)
		{
			$ShowInactive="";
		}
		if($CompanyTestMode==1)
		{
			$ShowTest="checked";
		}
		?>
		<form action="<?php echo route('admin.AddBuyerDetails'); ?>" name="BuyerUpdateForm" 
	      id="BuyerUpdateForm" method="POST" enctype="multipart/form-data">
	      <input type="hidden" name="_token" id="_token" value="<?php echo csrf_token(); ?>">
	      <input type="hidden" name="CampaignType" id="CampaignTypeEdit" value="<?php echo $CampaignDetails->CampaignType; ?>">
	      <input type="hidden" name="FormType" id="FormType" value="Clone">
	      <div class="row">                         
	        <div class="col-md-3 form-group mb-0 tadmPadd labFontt">
	            <label for="exampleFormControlSelect1">Campaign:</label>
	            <select class="form-control fonT" id="CampaignEdit" name="Campaign"  
	                onchange="GetCampaignDetailsEdit(),HideErr('CompanyEditErr');">
	                <?php foreach($CampaignDropDown as $cdd) {?>
	                <option value="<?php echo $cdd->CampaignID; ?>"
	                  <?php if($cdd->CampaignID==$BuyerDetails->CampaignID){echo "selected";}?>
	                  ><?php echo $cdd->CampaignName; ?></option>
	                <?php }?>
	            </select>
	            <span id="CampaignEditErr"></span>
	        </div>
	        <div class="col-md-4 form-group mb-0 tadmPadd labFontt">
	          <label for="exampleFormControlSelect1">Company:</label>
	            <select class="form-control fonT" id="CompanyEdit" name="Company"
	              onchange="HideErr('CompanyEditErr');">
	              <option value="">Select Company</option>
	              <?php foreach($CompanyList as $cl){?>
	              <option value="<?php echo $cl->CompanyID; ?>"
	              <?php if($cl->CompanyID==$BuyerDetails->CompanyID){echo "selected";}?>
	              ><?php echo $cl->CompanyName; ?></option>
	              <?php }?>                                
	            </select>
	            <span id="CompanyEditErr"></span>
		          <div class="form-check form-check-flat form-check-primary adComp adComp-primary disPlay tabdisPlay tMrg">
		            <label class="form-check-label adComp-label">
		            <input type="checkbox" class="form-check-input" id="ShowInactiveEdit"
		            onclick="GetCompanyUsingShwowInactiveAndTest3();" <?php echo $ShowInactive; ?>>
		              Show Inactive
		            <i class="input-helper"></i></label>
		          </div>
		          <div class="form-check form-check-flat form-check-primary adComp adComp-primary disPlay tabdisPlay tMrg">
		            <label class="form-check-label adComp-label">
		            <input type="checkbox" class="form-check-input" id="ShowTestEdit"
		            onclick="GetCompanyUsingShwowInactiveAndTest3();" <?php echo $ShowTest; ?>>
		              Show Test
		            <i class="input-helper"></i></label>
		          </div>
	        </div>
	        <div class="col-md-2 form-group tadmPadd labFontt">
	          <label for="exampleInputEmail1">Admin Label:</label>
	            <input type="text" class="form-control txtPadd" 
	            value="<?php echo $BuyerDetails->AdminLabel; ?>" id="AdminLabelEdit" name="AdminLabel"
	            onkeypress="HideErr('AdminLabelEditErr');">
	             <span id="AdminLabelEditErr"></span>
	        </div>
	        <div class="col-md-1 form-group tadmPadd btnCen">
	          <button type="button" id="CopyAdminLable" onclick="CopyAdminLableEdit();" class="btn btn-success noBoradi mr-2">&gt;&gt;</button>
	        </div>
	        <div class="col-md-2 form-group tadmPadd labFontt">
	          <label for="exampleInputEmail1">Partner Label:</label>
	            <input type="text" class="form-control txtPadd" 
	              value="<?php echo $BuyerDetails->PartnerLabel; ?>" id="PartnerLabelEdit" name="PartnerLabel"
	              onkeypress="HideErr('PartnerLabelEditErr');">
	               <span id="PartnerLabelEditErr"></span>
	        </div>
	       </div>
	      <div class="row">	       
	        <div class="col-md-2 form-group tadmPadd labFontt">
	          <label for="exampleInputEmail1">Integration File Name:</label>
	            <input type="text"value="<?php echo $BuyerDetails->IntegrationFileName; ?>" 
	              id="IntegrationFileNameEdit" name="IntegrationFileName" class="form-control txtPadd" placeholder="Integration File Name"
	              onkeypress="HideErr('IntegrationFileNameEditErr');">
	            <span id="IntegrationFileNameEditErr"></span>
	        </div>
	        <div class="col-md-2 form-group mb-0 tadmPadd labFontt">
	          <label for="PayoutCalculation">Payout Calculation:</label>
	            <select class="form-control fonT" id="PayoutCalculationEdit" name="PayoutCalculation" 
	            	onchange="PayoutCalculationEditFunction();">
	              <option value="RevShare" <?php if($BuyerDetails->PayoutCalculation=="RevShare"){ echo "selected"; } ?>>RevShare</option>
	              <option value="FixedPrice" <?php if($BuyerDetails->PayoutCalculation=="FixedPrice"){ echo "selected"; } ?>>Fixed Price</option>
	            </select>
	        </div>
	        <?php 
	        $PayoutCalculation = $BuyerDetails->PayoutCalculation;
	        if($PayoutCalculation=='FixedPrice')
	        { 
	             $PayoutCalculationFixedPrice="style='display:block'";
	             $PayoutCalculationRevShare="style='display:none'";
	        } 
	        else if($PayoutCalculation=='RevShare')
	        {
	             $PayoutCalculationRevShare="style='display:block'";
	             $PayoutCalculationFixedPrice="style='display:none'";
	        }
	        ?>
	        <div class="col-md-2 form-group tadmPadd labFontt" id="RevshareInputEdit" <?php echo $PayoutCalculationRevShare ?>  
	            id="RevsharePercentInput">
	          <label for="exampleInputEmail1">RevShare: </label>
	          <input type="text" class="form-control txtPadd" value="<?php echo $BuyerDetails->FixedPrice; ?>" 
	          placeholder="70" id="RevshareEdit" name="Revshare" onkeypress="HideErr('RevshareEditErr');">
	          <span id="RevshareEditErr"></span>
	        </div>
	        <div class="col-md-2 form-group tadmPadd labFontt" id="FixedPriceInputEdit" <?php echo $PayoutCalculationFixedPrice ?> 
	            id="FixedPriceInput">
	          <label for="exampleInputEmail1">Fixed Price:</label>
	          <input type="text" class="form-control txtPadd" value="<?php echo $BuyerDetails->FixedPrice; ?>" 
	          placeholder="70" id="FixedPriceEdit" name="FixedPrice" onkeypress="HideErr('FixedPriceEditErr');">
	          <span id="FixedPriceEditErr"></span>
	        </div>
	      </div>                         
	      <div class="row">
	        <?php 
	        if($CampaignDetails->CampaignType=="DirectPost"){                           
	          ?>
	          <div class="col-md-2 form-group tadmPadd labFontt">
	            <label for="exampleInputEmail1">Buyer Tier ID:</label>
	              <input type="text" onkeypress="HideErr('BuyerTierIDErr');" value="<?php echo $BuyerDetails->BuyerTierID; ?>" 
	                id="BuyerTierID" name="BuyerTierID" class="form-control txtPadd" placeholder="Buyer Tier ID">
	              <span id="BuyerTierIDErr"></span>
	          </div>
	          <?php
	        }
	        ?>
	        
	        <div class="col-md-1 form-group tadmPadd labFontt">
	          <label for="exampleInputEmail1">Daily Cap:</label>
	            <input type="text" value="<?php echo $BuyerDetails->DailyCap; ?>" 
	              id="DailyCapEdit" name="DailyCap" class="form-control txtPadd" placeholder="Daily Cap"
	              onkeypress="HideErr('DailyCapEditErr');">
	            <span id="DailyCapEditErr"></span>
	        </div>
	        <div class="col-md-1 form-group tadmPadd labFontt">
	          <label for="exampleInputEmail1">Total Cap:</label>
	            <input type="text" value="<?php echo $BuyerDetails->TotalCap; ?>" 
	              id="TotalCapEdit" name="TotalCap" class="form-control txtPadd" placeholder="Total Cap"
	              onkeypress="HideErr('TotalCapEditErr');">
	            <span id="TotalCapEditErr"></span>
	        </div>
	        <?php if($BuyerDetails->BuyerStatus=='1'){ ?>
	        <div class="col-md-2 form-group tadmPadd labFontt">
	          <label for="exampleInputEmail1">Buyer Status:</label>
	            <div class="onoffswitch8">
	              <input type="checkbox" name="BuyerStatus" class="onoffswitch8-checkbox" 
	              id="BuyerStatusEdit" checked="">
	              <label class="onoffswitch8-label" for="BuyerStatusEdit">
	                <span class="onoffswitch8-inner"></span>
	                <span class="onoffswitch8-switch"></span>
	              </label>
	            </div>
	        </div>
	        <?php } else {?>
	        <div class="col-md-2 form-group tadmPadd labFontt">
	          <label for="exampleInputEmail1">Buyer Status:</label>
	            <div class="onoffswitch8">
	              <input type="checkbox" name="BuyerStatus" class="onoffswitch8-checkbox" 
	              id="BuyerStatusEdit">
	              <label class="onoffswitch8-label" for="BuyerStatusEdit">
	                <span class="onoffswitch8-inner"></span>
	                <span class="onoffswitch8-switch"></span>
	              </label>
	            </div>
	        </div>
	        <?php }?>
	        <?php if($BuyerDetails->BuyerTestMode=='1'){ ?>
	        <div class="col-md-2 form-group tadmPadd labFontt">
	          <label for="exampleInputEmail1">Buyer Mode?</label>
	            <div class="onoffswitch9">
	              <input type="checkbox" name="BuyerTestMode" class="onoffswitch9-checkbox"
	                 id="BuyerTestMode" >
	              <label class="onoffswitch9-label" for="BuyerTestMode">
	                <span class="onoffswitch9-inner"></span>
	                <span class="onoffswitch9-switch"></span>
	              </label>
	            </div>
	        </div>
	        <?php } else {?>
	        <div class="col-md-2 form-group tadmPadd labFontt">
	          <label for="exampleInputEmail1">Buyer Mode?</label>
	            <div class="onoffswitch9">
	              <input type="checkbox" name="BuyerTestMode" class="onoffswitch9-checkbox" 
	                id="BuyerTestMode" checked="">
	              <label class="onoffswitch9-label" for="BuyerTestMode">
	                <span class="onoffswitch9-inner"></span>
	                <span class="onoffswitch9-switch"></span>
	              </label>
	            </div>
	        </div>
	        <?php }?>
	        <?php if($BuyerDetails->EWSStatus=='1'){ ?>
	        <div class="col-md-2 form-group tadmPadd labFontt">
              <label for="exampleInputEmail1">EWS Status</label>
                <div class="onoffswitch8">
                  <input type="checkbox" name="EWSStatus" class="onoffswitch8-checkbox"
                     id="EWSStatusCloneBuyer" checked="">
                  <label class="onoffswitch8-label" for="EWSStatusCloneBuyer">
                    <span class="onoffswitch8-inner"></span>
                    <span class="onoffswitch8-switch"></span>
                  </label>
                </div>
            </div>
	        <?php } else {?>
	        <div class="col-md-2 form-group tadmPadd labFontt">
              <label for="exampleInputEmail1">EWS Status</label>
                <div class="onoffswitch8">
                  <input type="checkbox" name="EWSStatus" class="onoffswitch8-checkbox"
                     id="EWSStatusAdd" >
                  <label class="onoffswitch8-label" for="EWSStatusAdd">
                    <span class="onoffswitch8-inner"></span>
                    <span class="onoffswitch8-switch"></span>
                  </label>
                </div>
            </div>
	        <?php }?>
	        <?php 
	        if($CampaignDetails->CampaignType=="DirectPost"){                           
	          $MaxTimeOutInput = "style='display:block;'";
	          $MinTimeOutInput = "style='display:block;'";
	          $MaxPingTimeOutInput = "style='display:none;'";
	          $MaxPostTimeOutInput = "style='display:none;'";
	          $MaxPingsPerMinuteInput = "style='display:none;'";
	        }else if($CampaignDetails->CampaignType=="PingPost"){
	          $MaxTimeOutInput = "style='display:none;'";
	          $MinTimeOutInput = "style='display:none;'";
	          $MaxPingTimeOutInput = "style='display:block;'";
	          $MaxPostTimeOutInput = "style='display:block;'";
	          $MaxPingsPerMinuteInput = "style='display:block;'";
	        }
	        ?>
	        <div class="col-md-2 form-group tadmPadd labFontt" id="MaxTimeOutInputEdit" <?php echo $MaxTimeOutInput; ?>>
	          <label for="exampleInputEmail1">Max Time Out:</label>
	            <input type="text" class="form-control txtPadd" placeholder="0" 
	            id="MaxTimeOutEdit" name="MaxTimeOut" value="<?php echo $BuyerDetails->MaxTimeOut; ?>" onkeypress="HideErr('MaxTimeOutEditErr');">
	            <span id="MaxTimeOutEditErr"></span>
	        </div>
	        <div class="col-md-2 form-group tadmPadd labFontt" id="MinTimeOutInputEdit" <?php echo $MinTimeOutInput; ?>>
	          <label for="exampleInputEmail1">Min Time Out:</label>
	            <input type="text" class="form-control txtPadd" placeholder="0" 
	            id="MinTimeOutEdit" name="MinTimeOut" value="<?php echo $BuyerDetails->MinTimeOut; ?>" onkeypress="HideErr('MinTimeOutEditErr');">
	            <span id="MinTimeOutEditErr"></span>
	        </div>

	        <div class="col-md-2 form-group tadmPadd labFontt" id="MaxPingTimeOutInputEdit" <?php echo $MaxPingTimeOutInput; ?>>
	          <label for="exampleInputEmail1">Max Ping Time Out:</label>
	            <input type="text" class="form-control txtPadd" placeholder="0" 
	            id="MaxPingTimeOutEdit" name="MaxPingTimeOut" value="<?php echo $BuyerDetails->MaxPingTimeOut; ?>" onkeypress="HideErr('MaxPingTimeOutEditErr');">
	            <span id="MaxPingTimeOutEditErr"></span>
	        </div>
	        <div class="col-md-2 form-group tadmPadd labFontt" id="MaxPostTimeOutInputEdit" <?php echo $MaxPostTimeOutInput; ?>>
	          <label for="exampleInputEmail1">Max Post Time Out:</label>
	            <input type="text" class="form-control txtPadd" placeholder="0" 
	            id="MaxPostTimeOutEdit" name="MaxPostTimeOut" value="<?php echo $BuyerDetails->MaxPostTimeOut; ?>" onkeypress="HideErr('MaxPostTimeOutEditErr');">
	            <span id="MaxPostTimeOutEditErr"></span>
	        </div>
	        <div class="col-md-2 form-group tadmPadd labFontt" id="MaxPingsPerMinuteInputEdit" <?php echo $MaxPingsPerMinuteInput; ?>>
	          <label for="exampleInputEmail1">Max Pings Per Minute:</label>
	            <input type="text" class="form-control txtPadd" placeholder="0" 
	            id="MaxPingsPerMinuteEdit" name="MaxPingsPerMinute" value="<?php echo $BuyerDetails->MaxPingsPerMinute; ?>" onkeypress="HideErr('MaxPingsPerMinuteEditErr');">
	            <span id="MaxPingsPerMinuteEditErr"></span>
	        </div>
	      </div>                             
	      <div class="row">
	       		<?php 
		        if($CampaignDetails->CampaignType=="DirectPost"){                           
		          $PingTestURLInputEdit = "style='display:none;'";
		          $PingLiveURLInputEdit = "style='display:none;'";
		          $PostTestURLInputEdit = "style='display:none;'";
		          $PostLiveURLInputEdit = "style='display:none;'";
		          $DirectPostTestURLInputEdit = "style='display:block;'";
		          $DirectPostLiveURLInputEdit = "style='display:block;'";
		        }else if($CampaignDetails->CampaignType=="PingPost"){
		          $PingTestURLInputEdit = "style='display:block;'";
		          $PingLiveURLInputEdit = "style='display:block;'";
		          $PostTestURLInputEdit = "style='display:block;'";
		          $PostLiveURLInputEdit = "style='display:block;'";
		          $DirectPostTestURLInputEdit = "style='display:none;'";
		          $DirectPostLiveURLInputEdit = "style='display:none;'";
		        }
		        ?>
	          <div class="col-md-6 form-group tadmPadd labFontt" id="PingTestURLInputEdit" <?php echo $PingTestURLInputEdit; ?>>
	            <label for="exampleInputEmail1">Ping Test URL:</label>
	              <input type="text" class="form-control txtPadd" 
	              value="<?php echo $BuyerDetails->PingTestURL; ?>" id="PingTestURL" name="PingTestURL"
	              onkeypress="HideErr('PingTestURLErr');">
	               <span id="PingTestURLErr"></span>
	          </div>
	          <div class="col-md-6 form-group tadmPadd labFontt" id="PingLiveURLInputEdit" <?php echo $PingLiveURLInputEdit; ?>>
	            <label for="exampleInputEmail1">Ping Live URL:</label>
	              <input type="text" class="form-control txtPadd" 
	                value="<?php echo $BuyerDetails->PingLiveURL; ?>" id="PingLiveURL" name="PingLiveURL"
	                onkeypress="HideErr('PingLiveURLErr');">
	                 <span id="PingLiveURLErr"></span>
	          </div>
	          <div class="col-md-6 form-group tadmPadd labFontt" id="PostTestURLInputEdit" <?php echo $PostTestURLInputEdit; ?>>
	            <label for="exampleInputEmail1">Post Test URL:</label>
	              <input type="text" class="form-control txtPadd" 
	              value="<?php echo $BuyerDetails->PostTestURL; ?>" id="PostTestURL" name="PostTestURL"
	              onkeypress="HideErr('PostTestURLErr');">
	               <span id="PostTestURLErr"></span>
	          </div>
	          <div class="col-md-6 form-group tadmPadd labFontt" id="PingLiveURLInputEdit" <?php echo $PingLiveURLInputEdit; ?>>
	            <label for="exampleInputEmail1">Post Live URL:</label>
	              <input type="text" class="form-control txtPadd" 
	                value="<?php echo $BuyerDetails->PostLiveURL; ?>" id="PostLiveURL" name="PostLiveURL"
	                onkeypress="HideErr('PostLiveURLErr');">
	                 <span id="PostLiveURLErr"></span>
	          </div>
	         
	          <div class="col-md-6 form-group tadmPadd labFontt" id="DirectPostTestURLInputEdit" <?php echo $DirectPostTestURLInputEdit; ?>>
	            <label for="exampleInputEmail1">DirectPost Test URL:</label>
	              <input type="text" class="form-control txtPadd" 
	              value="<?php echo $BuyerDetails->PostTestURL; ?>" id="DirectPostTestURL" name="DirectPostTestURL"
	              onkeypress="HideErr('DirectPostTestURLErr');">
	               <span id="DirectPostTestURLErr"></span>
	          </div>
	          <div class="col-md-6 form-group tadmPadd labFontt" id="DirectPostLiveURLInputEdit" <?php echo $DirectPostLiveURLInputEdit; ?>>
	            <label for="exampleInputEmail1">DirectPost Live URL:</label>
	              <input type="text" class="form-control txtPadd" 
	                value="<?php echo $BuyerDetails->PostLiveURL; ?>" id="DirectPostLiveURL" name="DirectPostLiveURL"
	                onkeypress="HideErr('DirectPostLiveURLErr');">
	                 <span id="DirectPostLiveURLErr"></span>
	          </div>
	         
	      </div>
	      <div class="row">
	        <div class="col-md-2 form-group mb-0 tadmPadd labFontt">
	            <label for="exampleFormControlSelect1">Request Method:</label>
	            <select class="form-control fonT" id="RequestMethod" name="RequestMethod" >
	                <option value="1" <?php if($BuyerDetails->RequestMethod=='1'){echo "selected";}?>>POST</option>
	                <option value="2" <?php if($BuyerDetails->RequestMethod=='2'){echo "selected";}?>>GET</option>
	                <option value="3" <?php if($BuyerDetails->RequestMethod=='3'){echo "selected";}?>>XmlinPost</option>
	                <option value="4" <?php if($BuyerDetails->RequestMethod=='4'){echo "selected";}?>>XmlPost</option>
	                <option value="5" <?php if($BuyerDetails->RequestMethod=='5'){echo "selected";}?>>InlineXML</option>
	            </select>
	            <span id="RequestMethodErr"></span>
	        </div>
	        <div class="col-md-2 form-group mb-0 tadmPadd labFontt">
	            <label for="exampleFormControlSelect1">Request Header:</label>
	            <select class="form-control fonT" id="RequestHeader" name="RequestHeader" >
	                <option value="1" <?php if($BuyerDetails->RequestHeader=='1'){echo "selected";}?>>Content-Type: application/x-www-form-urlencoded</option>
	                <option value="2" <?php if($BuyerDetails->RequestHeader=='2'){echo "selected";}?>>Content-Type:text/xml; charset=utf-8 </option>
	                <option value="3" <?php if($BuyerDetails->RequestHeader=='3'){echo "selected";}?>>Content-type: application/xml </option>
	                <option value="4" <?php if($BuyerDetails->RequestHeader=='4'){echo "selected";}?>>Content-type: application/json</option>
	            </select>
	            <span id="RequestHeaderErr"></span>
	        </div>
	        <div class="col-md-2 form-group tadmPadd labFontt">
	          <label for="exampleInputEmail1">Parameter1:</label>
	            <input type="text" class="form-control txtPadd" 
	            value="<?php echo $BuyerDetails->Parameter1; ?>" id="Parameter1" name="Parameter1"
	            onkeypress="HideErr('Parameter1Err');">
	             <span id="Parameter1Err"></span>
	        </div>
	        <div class="col-md-2 form-group tadmPadd labFontt">
	          <label for="exampleInputEmail1">Parameter2:</label>
	            <input type="text" class="form-control txtPadd" 
	            value="<?php echo $BuyerDetails->Parameter2; ?>" id="Parameter2" name="Parameter2"
	            onkeypress="HideErr('Parameter2Err');">
	             <span id="Parameter2Err"></span>
	        </div>
	        <div class="col-md-2 form-group tadmPadd labFontt">
	          <label for="exampleInputEmail1">Parameter3:</label>
	            <input type="text" class="form-control txtPadd" 
	            value="<?php echo $BuyerDetails->Parameter3; ?>" id="Parameter3" name="Parameter3"
	            onkeypress="HideErr('Parameter3Err');">
	             <span id="Parameter3Err"></span>
	        </div>
	        <div class="col-md-2 form-group tadmPadd labFontt">
	          <label for="exampleInputEmail1">Parameter4:</label>
	            <input type="text" class="form-control txtPadd" 
	            value="<?php echo $BuyerDetails->Parameter4; ?>" id="Parameter4" name="Parameter4"
	            onkeypress="HideErr('Parameter4Err');">
	             <span id="Parameter4Err"></span>
	        </div>
	      </div>
	       <div class="row">
	        <div class="col-md-4 form-group labFontt tablabFontt">
	          <label>File: (You Can Select Multiple Files)</label>
	          <div class="custom-file">
	            <input type="file" multiple class="custom-file-input" id="files" name="filesEdit[]" multiple>
	            <label class="custom-file-label fileLabel" for="files">No File Chosen</label>
	          </div>
	        </div>
	        <div class="col-md-4 form-group mb-0 labFontt">
	          <label for="exampleFormControlTextarea1">Buyer Notes:</label>
	          <textarea class="form-control" id="BuyerNotes" name="BuyerNotes" rows="3"><?php echo  $BuyerDetails->BuyerNotes; ?></textarea>
	        </div>
	      	</div>
	      	<div class="row btnFlo">
	            <div class="col-md-12 form-group mb-0 labFontt">
	              <button type="button" class="btn btn-success btn-fw noBoradi mr-2" onclick="SaveBuyerDetails();">Update</button>
	              <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
	            </div>
	        </div>
	    </form>
		<?php
	}
}
