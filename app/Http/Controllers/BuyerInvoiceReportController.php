<?php
namespace App\Http\Controllers;
use Route;
use Mail;
use Auth, Hash;
use Validator;
use DB;
use Session;
use Excel;
use PDF;
use Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Pagination;
use App\Http\Models\CommonModel;
use App\Http\Models\BuyerInvoiceReportModel;
use App\Http\Models\MasterConfigModel;

class BuyerInvoiceReportController extends Controller
{
	public function __construct(Request $request)
	{		
		$this->Pagination 		= new Pagination();
		$this->MasterConfigModel= new MasterConfigModel();
        $this->BIRModel           = new BuyerInvoiceReportModel();
		$this->CModel 		    = new CommonModel();
	}

	public function BuyerInvoiceReportView(Request $request)
	{
		if(!$request->session()->has('user_id'))
		{
		    return redirect()->action('LoginController@Login');
		}
		$UserRole 	= Session::get('user_role');
		$CompanyID 	= Session::get('user_companyID');
		$UserID 	= Session::get('user_id');

		$StartDate    = date("Y-m-d");
        $EndDate    = date("Y-m-d");

        $CampaignDropDown   = $this->MasterConfigModel->getCampaignList();
		$Tiers 	            = $this->CModel->TiersDropDown();
		$Data =array('CampaignDropDown' => $CampaignDropDown,
                    'Tiers'         => $Tiers,
                    'StartDate'		=> $StartDate,
                    'EndDate' 		=> $EndDate);
        $Data['Menu']   = "Report";
        $Data['Title']  = "Admin | BuyerInvoiceReport";

		return view('BuyerInvoiceReportView')->with($Data);
	}   
	public function GetBuyerInvoiceReport(Request $request)
	{
    $Data = $request->all();

    $UserRole   = Session::get('user_role');
    $CompanyID  = Session::get('user_companyID');
    $UserID 	= Session::get('user_id');

    $StartDate 	= date("Y-m-d",strtotime($Data['StartDate']))." 00:00:00";
    $EndDate 	= date("Y-m-d",strtotime($Data['EndDate']))." 23:59:59";

    $CampaignID         = $Data['CampaignID'];
    $BuyerCompanyID     = $Data['BuyerCompanyID'];
    $VendorCompanyID    = $Data['VendorCompanyID'];
    $VendorID           = $Data['VendorID'];
    $Tiers              = $Data['Tiers'];
    $ShowAll            = $Data['ShowAll'];

    $Search = array(
	        'StartDate' => $StartDate,
            'EndDate'   => $EndDate,
            'ShowAll' 	=> $ShowAll,
            'CampaignID'=> $CampaignID,
            'BuyerCompanyID' => $BuyerCompanyID,
            'VendorCompanyID' => $VendorCompanyID,
            'VendorID' => $VendorID,
            'Tiers' => $Tiers,
            'UserRole' 	=> $UserRole,
            'UserID' 	  => $UserID);
    
    $CampaignDetails 		= $this->BIRModel->GetCampaignDetails($CampaignID);
    $GetInvoiceDetails 	= $this->BIRModel->GetBuyerInvoiceReport($Search);
    if(!empty($GetInvoiceDetails))
    {       
        $Total=0;     
        $SubTotal=0;     
        $CompanyArr = array();
        ?>
        
        <thead>
            <tr class="table_thead">
                <th style="color:#c94142 ;font-weight: 700;" scope="col">Rows</th>
                <th style="color:#c94142 ;font-weight: 700;" scope="col">Company Name</th>
                <th style="color:#c94142 ;font-weight: 700;" scope="col">Buyer Label</th>                    
                <th style="color:#c94142 ;font-weight: 700;" scope="col">Attempted</th>
                <th style="color:#c94142 ;font-weight: 700;" scope="col">Accepted</th>
                <th style="color:#c94142 ;font-weight: 700;" scope="col">Acceptance Ratio</th>
                <?php 
                if($CampaignDetails->CampaignType=="DirectPost")
                {
                    echo "<th style='color:#c94142 ;font-weight: 700;' scope='col'>Redirected</th>";                
                    echo "<th style='color:#c94142 ;font-weight: 700;' scope='col'>Redirect Ratio</th>";
                }
                ?>
                <th style="color:#c94142 ;font-weight: 700;" scope="col">Sold Amount</th>
                <th style="color:#c94142 ;font-weight: 700;" scope="col">Leads Returned</th>
                <th style="color:#c94142 ;font-weight: 700;" scope="col">Returned Amount</th>
                <th style="color:#c94142 ;font-weight: 700;" scope="col">EPC</th>
                <th style="color:#c94142 ;font-weight: 700;" scope="col">Total</th>
                
            </tr>
        </thead>
        <tbody>
        <?php 
        $SubTotal=0;
        $CountComp_arr = array();
        $CoutSubTotal_arr = array();
        foreach ($GetInvoiceDetails as $CountName) 
        {
            array_push($CountComp_arr, $CountName->CompanyName);
            if(array_key_exists($CountName->CompanyName,$CoutSubTotal_arr))
            {
                $LastVal = $CoutSubTotal_arr[$CountName->CompanyName] + $CountName->Total;
                $CoutSubTotal_arr[$CountName->CompanyName]  = $LastVal ;
            }
            else
            {
                $CoutSubTotal_arr[$CountName->CompanyName] = $CountName->Total;
            }
        }
        $countRows = 0;
        $Attempted = 0;
        $Accepted = 0;
        $TotalCount =0;
        $Redirected = 0;
        $row = 1;
        $PurchasedAmountCount = 0;
        $LeadsReturnedCount = 0;
        $ReturnedAmountCount = 0;
        $EPCCount = 0;
        foreach($GetInvoiceDetails as $key=>$gid)
        {
                $countRows++;
                $CompanyArr[] = $gid->CompanyName;
                $Total = $Total+$gid->Total;
                ?>
                <tr>
                    <td><?php echo $row++; ?></td>
                    
                        <?php 
                        $counts = array_count_values($CompanyArr);
                        if($counts[$gid->CompanyName]==1)
                        {
                            $CheckCompany = array_count_values($CountComp_arr);
                            echo "<td style='vertical-align: middle;' rowspan=".$CheckCompany[$gid->CompanyName].">".$gid->CompanyName. "(CID: ".$gid->CompanyID.")</td>";
                        } 
                        ?>
                    
                    <td><?php echo $gid->AdminLabel."(BID: ".$gid->BuyerID.")"; ?></td>
                    <td><?php echo $gid->Attempted; $Attempted+=$gid->Attempted;?></td>
                    <td><?php echo $gid->Accepted; $Accepted+=$gid->Accepted; ?></td>
                    <td>
                        <?php 
                            echo number_format($gid->AcceptPercent,2)."%"; 
                        ?>
                    </td>
                    <?php 
                    if($CampaignDetails->CampaignType=="DirectPost")
                    {
                        echo "<td>".$gid->Redirected."</td>";
                        $Redirected+=$gid->Redirected;
                    }
                    ?>
                    <?php 
                    if($CampaignDetails->CampaignType=="DirectPost")
                    {
                        if($gid->RedirectRate!="" && $gid->RedirectRate!="NULl")
                        {
                            echo "<td>".number_format($gid->RedirectRate,2)."%</td>";
                        }
                        else
                        {
                           echo "<td>0.000%</td>"; 
                        }
                    }
                    ?>
                      
                    <td><?php echo '$'.number_format($gid->SoldAmount,2); $PurchasedAmountCount+=$gid->SoldAmount; ?></td>
                    <td><?php echo $gid->LeadsReturned; $LeadsReturnedCount+=$gid->LeadsReturned;?></td>
                    <td><?php echo '$'.number_format($gid->ReturnedPrice,2); $ReturnedAmountCount+=$gid->ReturnedPrice;?></td>

                    <td><?php echo number_format(($gid->SoldAmount/$gid->Attempted),2); 
                        $EPCCount+=$gid->EPC;?></td>                  
                    
                    <td><?php echo '$'.number_format($gid->Total,2); ?></td>
                </tr>
                <?php
                if($countRows == $CheckCompany[$gid->CompanyName])
                {
                    $countRows = 0;
                    ?>
                    <tr style="font-weight: bold;">
                        <td></td>
                        <td colspan="1">Sub Total</td>
                        <td></td>
                        <td><?php echo $Attempted; ?></td>
                        <td><?php echo $Accepted;  ?></td>
                        <td>
                            <?php 
                                $AcceptanceRatio = (($Accepted/$Attempted)*100);
                                echo number_format($AcceptanceRatio,2)."%"; 
                            ?>
                        </td>
                        <?php 
                        if($CampaignDetails->CampaignType=="DirectPost")
                        {?>
                            <td><?php echo $Redirected;  ?></td>
                        <?php 
                        }
                            $RedirectRatio = ($Redirected) ? (($Redirected/$Accepted)*100) : 0;
                        ?>
                        <?php if($CampaignDetails->CampaignType =="DirectPost") { ?>
                        <td><?php echo number_format($RedirectRatio,2)."%"; ?></td>
                        <?php }?>
                         <td><?php echo '$' . number_format($PurchasedAmountCount, 2);?></td>
                        <td><?php echo $LeadsReturnedCount;?></td>
                        <td><?php echo '$' . number_format($ReturnedAmountCount, 2);?></td>
                       
                        <td><?php echo number_format(($PurchasedAmountCount/$Attempted),2);?></td>
                       
                        <?php 
                            $counts = array_count_values($CompanyArr);
                             $CheckCompany = array_count_values($CountComp_arr);
                                echo "<td>$".number_format($CoutSubTotal_arr[$gid->CompanyName],2)."</td>";
                            $TotalCount += $CoutSubTotal_arr[$gid->CompanyName];
                        ?>
                    </tr>
                    <?php 
                    $Attempted =0;
                    $Accepted = 0;
                    $Redirected = 0;
                    $PurchasedAmountCount = 0;
                    $LeadsReturnedCount = 0;
                    $ReturnedAmountCount = 0;  
                }
        }
        ?>
        </tbody>
        <?php 
        $colspan=11;
        if($CampaignDetails->CampaignType=="DirectPost")
        {
            $colspan=13;
        }  
        ?>
        <?php 
            $baseURL = url()->current(); 
            $stripcolor = 'background-color: #ca3b44; text-align: right; color: white;';
            if (strpos($baseURL, 'tapgage.net') !== false) {
                $stripcolor = 'background-color: #0A64B8; text-align: right; color: white;';
             }
        ?>
        <tr style="<?php echo $stripcolor; ?>">
            <td colspan="<?php echo $colspan-1; ?>" style="text-align: right;color:#fff ;font-weight: 700; font-size: 0.8rem; padding-top: 0.484rem; padding-bottom: 0.484rem;"><b>Grand Total</b></td>
            <td style="color: #fff; background: #c94142; font-weight: 700;"><?php echo '$'.number_format($TotalCount,2); ?></td>
        </tr>
        <?php
    }
    else
    {
        ?>
        <tbody>
            <tr>
                <td colspan="7">No Data Found</td>
            </tr>
        </tbody>
        <?php
    }
	}

}
