<?php
namespace App\Http\Controllers;
use Route;
use Mail;
use Auth, Hash;
use Validator;
use DB;
use Session;
use Excel;
use PDF;
use Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Pagination;
use App\Http\Models\AffiliateReportModel;
use App\Http\Models\MasterConfigModel;

class AffiliateReportController extends Controller
{
	public function __construct(Request $request)
	{		
		$this->Pagination 		= new Pagination();
		$this->MasterConfigModel= new MasterConfigModel();
		$this->ARModel 			= new AffiliateReportModel();
	}

	public function AffiliateReportView(Request $request)
	{
		if(!$request->session()->has('user_id'))
		{
		    return redirect()->action('LoginController@Login');
		}
		$UserRole 	= Session::get('user_role');
		$CompanyID 	= Session::get('user_companyID');
		$UserID 	= Session::get('user_id');

		$StartDate 	= date("Y-m-d");
		$EndDate 	= date("Y-m-d");

		$CampaignDropDown 	= $this->MasterConfigModel->getCampaignList();
		$UserDropDown 		= $this->ARModel->UserDropDownWithCompanyID($CompanyID);
		$Data =array('CampaignDropDown' => $CampaignDropDown,
					 'UserDropDown' 	=> $UserDropDown,
                     'StartDate'		=> $StartDate,
                     'EndDate' 			=> $EndDate);
        $Data['Menu']   = "Report";
        $Data['Title']  = "Admin | AffiliateReport";
		return view('AffiliateReportView')->with($Data);
	}   
	public function GetAffiliateReport(Request $request)
	{
		$Data = $request->all();

		$StartDate 	= date("Y-m-d",strtotime($Data['StartDate']))." 00:00:00";
        $EndDate 	= date("Y-m-d",strtotime($Data['EndDate']))." 23:59:59";
        $VendorCompanyID = $Data['VendorCompanyID'];
        $CampaignID = $Data['CampaignID'];
        $UserID 	= $Data['UserID'];

		$Search = array(
			'StartDate' => $StartDate,
            'EndDate' 	=> $EndDate,
            'CampaignID'=> $CampaignID,
            'VendorCompanyID' => $VendorCompanyID,
            'UserID' 	=> $UserID);
        $GetCommissionSummary = $this->ARModel->GetCommissionSummary($Search);
        $GetVendorCommission = $this->ARModel->GetVendorCommissionReport($Search);
        $GetBuyerCommission = $this->ARModel->GetBuyerCommissionReport($Search);

       	?>
        <div class="col-md-12">
            <div class="hBox">
                <div class="heading text-center">
                    <h2>Commission Summary</h2>
                </div>
                <div class="table-responsive" id="Vendor">
                    <table class="table table-bordered tableFont tabFonnt newT">
                        <thead>
                            <tr>
                                <th scope="col" style="font-size: 0.8rem;">Gross Revenue</th>
                                <th scope="col" style="font-size: 0.8rem;">Gross COGS </th>
                                <th scope="col" style="font-size: 0.8rem;">Gross Profit</th>
                                <th scope="col" style="font-size: 0.8rem;">Total Returned Revenue</th>
                                <th scope="col" style="font-size: 0.8rem;">Total Returned COGS</th>
                                <th scope="col" style="font-size: 0.8rem;">Net Revenue</th>
                                <th scope="col" style="font-size: 0.8rem;">Net COGS</th>
                                <th scope="col" style="font-size: 0.8rem;">Net Profit</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr style="background: #c94142;">  
                             <td class="no-border-right tblRows">
                                <?php 
                                    $GrossRevenue = $GetCommissionSummary[0]->GrossRevenue;
                                    echo "$" . number_format($GrossRevenue, 2);
                                ?>
                             </td>
                             <td class="no-border-right tblRows">
                                <?php
                                    $GrossCOGS = $GetCommissionSummary[0]->GrossCOGS; 
                                    echo "$" . number_format($GrossCOGS, 2);
                                ?>
                             </td>
                             <td class="no-border-right tblRows">
                                <?php
                                    $GrossProfit = ($GrossRevenue - $GrossCOGS);
                                    echo "$" . number_format($GrossProfit, 2);
                                ?>
                             </td>
                             <td class="no-border-right tblRows">
                                <?php 
                                    $TotalReturnedRevenue = $GetCommissionSummary[0]->TotalReturnedRevenue;
                                    echo "$" . number_format($TotalReturnedRevenue, 2);
                                ?>
                             </td>
                             <td class="no-border-right tblRows">
                                <?php 
                                    $TotalReturnedCOGS = $GetCommissionSummary[0]->TotalReturnedCOGS;
                                    echo "$" . number_format($TotalReturnedCOGS, 2);
                                ?>
                             </td>
                             <td class="no-border-right tblRows">
                                <?php
                                    $NetRevenue = ($GrossRevenue - $TotalReturnedRevenue);
                                    echo "$" . number_format($NetRevenue, 2);
                                ?>
                             </td>
                             <td class="no-border-right tblRows">
                                <?php
                                    $NetCOGS = ($GrossCOGS - $TotalReturnedCOGS);
                                    echo "$" . number_format($NetCOGS, 2);
                                ?>
                             </td>
                             <td class="no-border-right tblRows">
                                <?php
                                    $NetProfit = ($NetRevenue - $NetCOGS);
                                    echo "$" . number_format($NetProfit, 2);
                                ?>
                             </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
       	<div class="col-md-6">
            <div class="hBox">
                <div class="heading text-center">
                    <h2>Vendor Commission</h2>
                </div>
                <div class="table-responsive" id="Vendor">
                    <table class="table table-bordered tableFont tabFonnt newT">
                        <thead>
                            <tr>
                                <th scope="col" style="font-size: 0.8rem;">Company</th>
                                <th scope="col" style="font-size: 0.8rem;">User Name</th>
                                <th scope="col" style="font-size: 0.8rem;">No. of Leads</th>
                                <th scope="col" style="font-size: 0.8rem;">Commission</th>
                            </tr>
                        </thead>
                        <?php if(!empty($GetVendorCommission)){ ?>
                        <tbody>
                        	<?php 
                        		$TotalVendorRefPayout=0;
                        		foreach($GetVendorCommission as $gvc){
                        		$TotalVendorRefPayout = $TotalVendorRefPayout +$gvc->VendorRefPayout;
                        		?>
                            <tr>
                                <td><?php echo $gvc->CompanyName; ?></td>
								<td><?php echo $gvc->UserName; ?></td>
								<td><?php echo $gvc->NumLeads; ?></td>
					            <td ><?php echo '$'.number_format($gvc->VendorRefPayout,2); ?></td>
                            </tr>
                            <?php } ?>
                        </tbody>
                        <tbody>
                            <tr style="background: #c94142;">
                                <td colspan="2" class="no-border-right"></td>
                                <td style="width: 35%; color:#fff ;font-weight: 700; font-size: 0.8rem; padding-top: 0.484rem; padding-bottom: 0.484rem;">Total Commissions</td>
                                <td style="color: #fff; background: #c94142; font-weight: 700;">$<?php echo number_format($TotalVendorRefPayout,2); ?></td>
                            </tr>
                        </tbody>
                        <?php } else{?>
                        <tbody>
                            <tr style="background: #c94142;">
                                <td colspan="2" class="no-border-right"></td>
                                <td style="width: 35%; color:#fff ;font-weight: 700; font-size: 0.8rem; padding-top: 0.484rem; padding-bottom: 0.484rem;">Total Commissions</td>
                                <td style="color: #fff; background: #c94142; font-weight: 700;">$0.00</td>
                            </tr>
                        </tbody>
                        <?php }?>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="hBox">
                <div class="heading text-center">
                    <h2>Buyer Commission</h2>
                </div>
                <div class="table-responsive" id="Buyer">
                    <table class="table table-bordered tabFonnt tableFont newT">
                        <thead>
                            <tr>
                                <th scope="col" style="font-size: 0.8rem;">Company</th>
                                <th scope="col" style="font-size: 0.8rem;">User Name</th>
                                <th scope="col" style="font-size: 0.8rem;">No. of Leads</th>
                                <th scope="col" style="font-size: 0.8rem;">Commission</th>
                            </tr>
                        </thead>
                        <?php if(!empty($GetBuyerCommission)){ ?>
                        <tbody>
                            <?php 
                            	$TotalBuyerRefPayout=0;
                            	foreach($GetBuyerCommission as $gbc){
                            	$TotalBuyerRefPayout = $TotalBuyerRefPayout +$gbc->BuyerRefPayout;
                            	?>
                            <tr>
                                <td><?php echo $gbc->CompanyName; ?></td>
								<td><?php echo $gbc->UserName; ?></td>
								<td><?php echo $gbc->NumLeads; ?></td>
					            <td ><?php echo '$'.number_format($gbc->BuyerRefPayout,2); ?></td>
                            </tr>
                            <?php } ?>
                        </tbody>
                        <tbody>
                            <tr style="background: #c94142;">
                                <td colspan="2" class="no-border-right"></td>
                                <td style="width: 35%; color:#fff ;font-weight: 700; font-size: 0.8rem; padding-top: 0.484rem; padding-bottom: 0.484rem;">Total Commissions</td>
                                <td style="color: #fff; background: #c94142; font-weight: 700;">$<?php echo number_format($TotalBuyerRefPayout,2); ?></td>
                            </tr>
                        </tbody>
                        <?php } else{?>
                        <tbody>
                            <tr style="background: #c94142;">
                                <td colspan="2" class="no-border-right"></td>
                                <td style="width: 35%; color:#fff ;font-weight: 700; font-size: 0.8rem; padding-top: 0.484rem; padding-bottom: 0.484rem;">Total Commissions</td>
                                <td style="color: #fff; background: #c94142; font-weight: 700;">$0.00</td>
                            </tr>
                        </tbody>
                        <?php }?>
                    </table>
                </div>
            </div>
        </div>
       	<?php
       	exit();
	}

}
