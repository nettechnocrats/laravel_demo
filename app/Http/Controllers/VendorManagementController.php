<?php
namespace App\Http\Controllers;
use Route;
use Mail;
use Auth, Hash;
use Validator;
use DB;
use Session;
use Excel;
use PDF;
use Response;
use Redirect;
use Illuminate\Http\Request;
use App\Http\Controllers\Pagination;
use App\Http\Models\VendorManagementModel;
use App\Http\Models\CommonModel;
use App\Http\Models\MasterConfigModel;

class VendorManagementController extends Controller
{
	public function __construct(Request $request)
	{		
		$this->Pagination 		= new Pagination();
		$this->MasterConfigModel= new MasterConfigModel();
		$this->VMModel 			= new VendorManagementModel();
		$this->CModel 			= new CommonModel();
	}

	public function VendorManagementView(Request $request)
	{
		if(!$request->session()->has('user_id'))
		{
		    return redirect()->action('LoginController@Login');
		}
		$Data['CampaignDropDown'] = $this->MasterConfigModel->getCampaignList();
		$Data['CompanyList'] 	= $this->VMModel->CompanyList();
		$Data['Menu']   = "Account";
        $Data['Title']  = "Admin | VendorManagement";
		return view('VendorManagementView')->with($Data);
	}

	public function GetVendorList(Request $request)
	{
		$Data = $request->all();

        $NumOfRecords = $Data['numofrecords'];
        $page = $Data['page'];
        $cur_page = $page;

        $Limitpage = $page - 1;
        $Start = $Limitpage * $NumOfRecords;

        $CompanyID 		= $Data['CompanyID'];
        $CampaignID 	= $Data['Campaign'];
        $VendorID 		= $Data['VendorID'];
        $VendorName 	= $Data['VendorName'];
        $VendorStatus 	= $Data['VendorStatus'];
        $ShowInactive 	= $Data['ShowInactive'];
        $ShowTest 		= $Data['ShowTest'];
      
        $Search 	= array('CompanyID' 	=> $CompanyID,
        					'CampaignID' 	=> $CampaignID,
				            'VendorID' 		=> $VendorID,
				            'VendorName' 	=> $VendorName,
				            'VendorStatus' 	=> $VendorStatus,
				            'ShowInactive' 	=> $ShowInactive,
				            'ShowTest' 		=> $ShowTest,
				            'Start' 		=> $Start,
				            'NumOfRecords' 	=> $NumOfRecords
				            );
        $GetVendorList 	= $this->VMModel->GetVendorList($Search);
       
        $AllVendorList 	= $GetVendorList['Res'];
        $UserCount 		= $GetVendorList['Count'];        

        $Pagination = $this->Pagination->Links($NumOfRecords, $UserCount, $page);
        $CampaignDetails = $this->CModel->GetCampaignDetails($CampaignID);

        $HasSpecs = $CampaignDetails->HasSpecs;
      ?>
		<div class="row mt-0">
          	<div class="table-responsive">
		  		<table class="table table-bordered tableFont txtL nTabl">
                    <thead class="thead-dark">
						<tr>
							<th scope="col" style="padding: 0.684rem; font-size: 0.8rem;">Company Name</th>
							<th scope="col" style="padding: 0.684rem; font-size: 0.8rem;">Campaign</th>
							<th scope="col" style="padding: 0.684rem; font-size: 0.8rem;">Admin Label</th>
							<th scope="col" style="padding: 0.684rem; font-size: 0.8rem;">Daily Cap</th>
							<th scope="col" style="padding: 0.684rem; font-size: 0.8rem;">Total Cap</th>
							<?php if($CampaignDetails->CampaignType=="DirectPost"){ ?>
							<th scope="col" style="padding: 0.684rem; font-size: 0.8rem;">Max Time Out</th>
							<?php } else { ?>
							<th scope="col" style="padding: 0.684rem; font-size: 0.8rem;">Max Ping Time Out</th>
							<th scope="col" style="padding: 0.684rem; font-size: 0.8rem;">Max Post Time Out</th>
							<?php }?>
							<th scope="col" style="padding: 0.684rem; font-size: 0.8rem;">Status</th>
							<th scope="col" style="padding: 0.684rem; font-size: 0.8rem;">Vendor Mode</th>
							<th scope="col" style="padding: 0.684rem; font-size: 0.8rem;">Action</th>
						</tr>
                    </thead>
			    	<tbody>
				    <?php 
				     	if(!empty($AllVendorList ))
				     	{
				     		foreach($AllVendorList as $p)
				     		{
							     ?>				       
						        <tr>
						           	<td><?php echo $p->CompanyName; ?></td>
						           	<td><?php echo $p->CampaignName; ?></td>
						           	<td><?php echo $p->AdminLabel ." (VID: ".$p->VendorID.")"; ?></td>
						           	<td><?php echo $p->DailyCap; ?></td>
						           	<td><?php echo $p->TotalCap; ?></td>
						           	<?php if($CampaignDetails->CampaignType=="DirectPost"){ ?>
									<td><?php echo $p->MaxTimeOut; ?></td>
									<?php } else { ?>
									<td><?php echo $p->MaxPingTimeOut; ?></td>
									<td><?php echo $p->MaxPostTimeOut; ?></td>
									<?php }?>
						           	<td id="StatusHtml_<?php echo $p->VendorID; ?>">
						           		<?php 
						           		if ($p->VendorStatus == '1') 
						           		{
		                                   ?>
		                                   	<div class="custom-control custom-checkbox noCush greenCh">
				                              <input type="checkbox" checked="true"
				                              	class="custom-control-input noCush-input green-input" 
				                              	id="customCheck<?php echo $p->VendorID; ?>" 
				                              	onclick="ChangeVendorStatus('<?php echo $p->VendorID; ?>',0);">
				                              <label class="custom-control-label noCush-label green-label mb-3" 
				                              	for="customCheck<?php echo $p->VendorID; ?>"  
				                              	></label>
				                            </div> 
		                                   <?php
		                                } 
		                                else if ($p->VendorStatus == '0') 
		                                {
		                                    ?>
		                                   	<div class="custom-control custom-checkbox noCush">
				                              <input type="checkbox"  
				                              class="custom-control-input noCush-input" 
				                              id="customCheck<?php echo $p->VendorID; ?>" 
				                              onclick="ChangeVendorStatus('<?php echo $p->VendorID; ?>',1);" >
				                              <label class="custom-control-label noCush-label mb-3" 
				                              	for="customCheck<?php echo $p->VendorID; ?>" 
				                              	></label>
				                            </div> 
		                                   <?php
		                                }
						           		?>
						           	</td>
						           	<td id="ModeHtml_<?php echo $p->VendorID; ?>">
						           		<?php 
						           		if ($p->VendorTestMode == '1') 
						           		{
		                                   ?>
		                                   	<a href="javascript:void(0);" 
		                                   		class="btn btn-success cfonnT cbtnPadd noBoradi"
		                                   		onclick="ChangeVendorTestMode('<?php echo $p->VendorID; ?>',0);">
												<i class="fas fa-check-square sqFont" aria-hidden="true"></i> 
												LiveMode
											</a>  
		                                   <?php

		                                } 
		                                else if ($p->VendorTestMode == '0') 
		                                {
		                                    ?>
		                                   	<a href="javascript:void(0);" 
		                                   		class="btn btn-danger cfonnT cbtnPadd noBoradi"
		                                   		onclick="ChangeVendorTestMode('<?php echo $p->VendorID; ?>',1);">
												<i class="fas fa-square sqFont" aria-hidden="true"></i> 
												TestMode
											</a>  
		                                   <?php
		                                }
						           		?>
						           	</td>
						           	<td>
						           		<ul class="btnList">
                                          <li>
                                            <!-- <a href="<?php //echo route('admin.VendorEditView',array(base64_encode($p->VendorID))); ?>">
                                            	<button type="button" title="edit" 
                                            		class="btn btn-info cfonnT cbtnPadd noBoradi">
                                            		<i class="fas fa-pencil-alt fnnPadd"></i>Edit
                                            	</button>
                                            </a> -->
                                            <a href="javascript:void(0);" onclick="GetVendorDetails('<?php echo $p->VendorID; ?>');">
				                              	<button type="button" title="edit" 
	                                        		class="btn btn-info cfonnT cbtnPadd noBoradi">
	                                        		<i class="fas fa-pencil-alt fnnPadd"></i>Edit
                                        		</button>
				                            </a>
                                          </li>
                                          <li>
                                            <!-- <a href="javascript:void(0);" 
                                            	onclick="SetSessionForCloneVendor(<?php echo $p->VendorID; ?>);">
                                            	<button type="button" title="add" 
                                            		class="btn btn-info cfonnT cbtnPadd noBoradi bgClrred">
                                            		<i class="fas fa-plus fnnPadd"></i>Clone
                                            	</button>
                                            </a> -->
                                            <a href="javascript:void(0);" onclick="GetVendorCloneDetails('<?php echo $p->VendorID; ?>');">
				                              	<button type="button" title="add" 
                                            		class="btn btn-info cfonnT cbtnPadd noBoradi bgClrred">
                                            		<i class="fas fa-plus fnnPadd"></i>Clone
                                            	</button>
				                            </a>
                                          </li>
                                          <?php if($HasSpecs=='1'){ ?>
                                          <li>
                                            <a href="<?php echo 'view-specs/'.$CampaignID.'/'.base64_encode($p->VendorID); ?>"
                                            	target="_blank">
                                            	<button type="button" title="add" 
	                                            	class="btn btn-success cfonnT cbtnPadd noBoradi">
	                                            	<i class="fas fa-eye fnnPadd"></i>Specs
	                                            </button>
	                                        </a>
                                          </li>
                                          <?php } ?>
                                        </ul>
						           	</td>
						        </tr>
			     				<?php 
			     			}
			     		} 
			     		else 
			     		{
			     			?>
					     	<tr >
					           <td colspan="10" style="color: #c94542;text-align:center;">Record Not Found.</td>
					           </td>
					        </tr>
			     			<?php 
			     		} 
			     	?>
			     	</tbody>
				  </table>
				</div>
			</div>
		<?php if(!empty($AllVendorList ))
		{ ?>
			<div class="row mt-3">			
				<div class="col-md-6 col-sm-6 col-12">
					<div class="dataTables_length mCenter" id="dataTable_length">
						<label>Records/Pages
							<select id="numofrecords" onchange="SearchVendorList();" name="dataTable_length" aria-controls="dataTable" class="custom-select custom-select-sm form-control form-control-sm">
							<option value="10" <?php if($NumOfRecords=='10'){ echo 'selected'; } ?> >10</option>
							<option value="20" <?php if($NumOfRecords=='20'){ echo 'selected'; } ?> >20</option>
							<option value="50" <?php if($NumOfRecords=='50'){ echo 'selected'; } ?> >50</option>
							<option value="100" <?php if($NumOfRecords=='100'){ echo 'selected'; } ?> >100</option>
							<option value="150" <?php if($NumOfRecords=='150'){ echo 'selected'; } ?> >150</option>
							<option value="200" <?php if($NumOfRecords=='200'){ echo 'selected'; } ?> >200</option>
							</select>
						</label>
					</div>
				</div>
				<div class="col-md-6 col-sm-6 col-12 grid-margin stretch-card">
					<div class="card noBg border-0">
					    <nav>
					      <?php echo $Pagination; ?>
					    </nav>
					</div>
				</div>
			</div>
      	<?php
      	}
      	else
      	{
      		?>
      		<input type="hidden" name="numofrecords" id="numofrecords" value='10'>
      		<?php
      	}
       	exit();
	}
	public function ChangeVendorStatus(Request $request)
	{
		$Data 		= $request->all();
		$Response 	=array();
		$VendorID   	 	   = $Data['VID'];
		$Details['VendorStatus'] = $Data['Status'];
		$ChangeVendorStatus = $this->VMModel->SaveVendorDetails($VendorID,$Details);
		if($ChangeVendorStatus)
		{
			$Massage = '<div class="alert alert-success alert-dismissible fade show" role="alert">
						  Vendor Status Has Been Changed.
						  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
						    <span aria-hidden="true">&times;</span>
						  </button>
						</div>';
			$Response['Status'] = 1;
			$Response['Msg'] = $Massage;
		}
		else
		{
			$Massage = '<div class="alert alert-danger alert-dismissible fade show" role="alert">
						  Something Wrong Please Try Again.
						  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
						    <span aria-hidden="true">&times;</span>
						  </button>
						</div>';
			$Response['Status'] = 0;
			$Response['Msg'] = 1;
		}
        if ($Data['Status']=='1') 
   		{
           	$Status = '<div class="custom-control custom-checkbox noCush greenCh">
			              <input type="checkbox"   checked="true"
			              	class="custom-control-input noCush-input green-input" 
			              	id="customCheck'.$VendorID.'"  
			              	onclick="ChangeVendorStatus('.$VendorID.',0);">
			              <label class="custom-control-label noCush-label green-label mb-3" 
			              	for="customCheck'.$VendorID.'"   
			              	></label>
			            </div> ';
			$Response['StatusHtml'] = $Status;
        } 
        else 
        {
        	$Status = '<div class="custom-control custom-checkbox noCush greenCh">
		              <input type="checkbox"
		              	class="custom-control-input noCush-input green-input" 
		              	id="customCheck'.$VendorID.'" 
		              	onclick="ChangeVendorStatus('.$VendorID.',1);">
		              <label class="custom-control-label noCush-label green-label mb-3" 
		              	for="customCheck'.$VendorID.'"  
		              	></label>
		            </div> ';
			$Response['StatusHtml'] = $Status;
        }

		echo json_encode($Response);
		exit();
	}
	public function ChangeVendorTestMode(Request $request)
	{
		$Data 		= $request->all();
		$Response 	=array();
		$VendorID   	 	   = $Data['VID'];
		$Details['VendorTestMode'] = $Data['Mode'];
		$ChangeVendorTestMode = $this->VMModel->SaveVendorDetails($VendorID,$Details);
		if($ChangeVendorTestMode)
		{
			$Massage = '<div class="alert alert-success alert-dismissible fade show" role="alert">
						  Vendor Mode Has Been Changed.
						  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
						    <span aria-hidden="true">&times;</span>
						  </button>
						</div>';
			$Response['Status'] = 1;
			$Response['Msg'] = $Massage;
		}
		else
		{
			$Massage = '<div class="alert alert-danger alert-dismissible fade show" role="alert">
						  Something Wrong Please Try Again.
						  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
						    <span aria-hidden="true">&times;</span>
						  </button>
						</div>';
			$Response['Status'] = 0;
			$Response['Msg'] = 1;
		} 
   		if ($Data['Mode']=='1') 
   		{
           $Mode = '<a href="javascript:void(0);" 
                   		class="btn btn-success cfonnT cbtnPadd noBoradi"
                   		onclick="ChangeVendorTestMode('.$VendorID.',0);">
						<i class="fas fa-check-square sqFont" aria-hidden="true"></i> 
						LiveMode
					</a> ';
			$Response['ModeHtml'] = $Mode;

        } 
        else
        {
            $Mode = '<a href="javascript:void(0);" 
                   		class="btn btn-danger cfonnT cbtnPadd noBoradi"
                   		onclick="ChangeVendorTestMode('.$VendorID.',1);">
						<i class="fas fa-square sqFont" aria-hidden="true"></i> 
						TestMode
					</a> ';
			$Response['ModeHtml'] = $Mode;
        }
		echo json_encode($Response);
		exit();
	}
	public function VendorAddView(Request $request)
	{
		if(!$request->session()->has('user_id'))
		{
		    return redirect()->action('LoginController@Login');
		}
		
		$Data['CompanyList'] 		= $this->VMModel->CompanyList();		
		$Data['CampaignDropDown'] 	= $this->MasterConfigModel->getCampaignList();
		$Data['Menu']   = "Account";
        $Data['Title']  = "Admin | VendorManagement";
        
		return view('VendorAddView')->with($Data);
	}
	public function AddVendorDetails(Request $request)
	{
		$Data 							= $request->all();
		$CampaignID     				= $Data['Campaign'];
		$CompanyID      				= $Data['Company'];
		$Details['CampaignID']      	= $CampaignID;
		$Details['CompanyID']      		= $CompanyID;
		$Details['AdminLabel']      	= $Data['AdminLabel'];
		$Details['PartnerLabel']      	= $Data['PartnerLabel'];
		$CampaignDetails = $this->CModel->GetCampaignDetails($CampaignID);
		if($CampaignDetails->CampaignType=='DirectPost')
		{
			$Details['MaxTimeOut']   = $Data['MaxTimeOut'];
		}
		else if($CampaignDetails->CampaignType=='PingPost')
		{
			$Details['MaxPingTimeOut']   = $Data['MaxPingTimeOut'];
			$Details['MaxPostTimeOut']   = $Data['MaxPostTimeOut'];
		}
		$Details['PayoutCalculation']   = $Data['PayoutCalculation'];
		if($Data['PayoutCalculation']=='Revshare' || $Data['PayoutCalculation']=='MinPrice')
		{
			$Details['RevsharePercent']   = $Data['Revshare'];
		}
		else if($Data['PayoutCalculation']=='TierPrice')
		{
			$Details['RevsharePercent']   = $Data['TierPrice'];
		}
		else if($Data['PayoutCalculation']=='FixedPrice')
		{
			$Details['FixedPrice']   = $Data['FixedPrice'];	
		}
		else if($Data['PayoutCalculation']=='MinPrice')
		{
			$Details['RevsharePercent']   = $Data['Revshare'];	
		}
		else if($Data['PayoutCalculation']=='BucketPrice' || $Data['PayoutCalculation'] = "BucketSystemBySubID")
		{
			$Details['RevsharePercent'] = $Data['Revshare'];	
			$Details['FixedPrice']   	= $Data['FixedPrice'];	
		}

		$Details['DailyCap'] 	= $Data['DailyCap'];
		$Details['TotalCap'] 	= $Data['TotalCap'];
		$Details['TrackingPixelType'] 	= $Data['TrackingPixelType'];
		$Details['TrackingPixelStatus'] = 0;
		if(isset($Data['TrackingPixelStatus']) && $Data['TrackingPixelStatus']=='on')
		{
			$Details['TrackingPixelStatus'] = 1;
		}
		$Details['VendorStatus'] = 0;
		if(isset($Data['VendorStatus']) && $Data['VendorStatus']=='on')
		{
			$Details['VendorStatus'] = 1;
		}
		$Details['VendorTestMode'] = 0;
		if(isset($Data['VendorTestMode']) && $Data['VendorTestMode']=='on')
		{
			$Details['VendorTestMode'] = 1;
		}				
		$Details['TrackingPixelCode']   = $Data['TrackingPixelCode'];
		$Details['VendorNotes']   		= html_entity_decode($Data['VendorNotes']);
		$VendorAPIKey =  md5($Data['Company'].$Data['Campaign'].time());
		$Details['APIKey']   = $VendorAPIKey;
		$AddVendorDetails = $this->VMModel->AddVendorDetails($Details);
		if($AddVendorDetails)
		{
			Session::flash('message', 'Vendor Details Has been Added.');
            Session::flash('alert-class', 'alert-success');
            if(isset($Data['From']) && $Data['From']=='Company')
            {
            	return Redirect::route('admin.CompanyEditView',base64_encode($CompanyID));      
            }
            else
            {
            	return Redirect::route('admin.VendorManagementView');            	
            }
		}
		else
		{
			Session::flash('message', 'OOPS! Something Wrong Please Try Again.');
            Session::flash('alert-class', 'alert-danger');
            if(isset($Data['From']) && $Data['From']=='Company')
            {
            	return Redirect::route('admin.CompanyEditView',base64_encode($CompanyID));      
            }
            else
            {
            	return Redirect::route('admin.VendorManagementView');            	
            }
		}
	}
	public function VendorEditView(Request $request,$VendorID)
	{
		if(!$request->session()->has('user_id'))
		{
		    return redirect()->action('LoginController@Login');
		}
		$VendorID = base64_decode($VendorID);
		$Data['CompanyList'] 		= $this->VMModel->CompanyList();
		$VendorDetails 				= $this->VMModel->VendorDetails($VendorID);
		$Data['VendorDetails'] 		= $VendorDetails;
		$Data['CampaignDropDown'] 	= $this->MasterConfigModel->getCampaignList();
		$CampaignID 				= $VendorDetails->CampaignID;
		$Data['CampaignDetails'] 	= $this->CModel->GetCampaignDetails($CampaignID);
		$Data['Menu']   = "Account";
        $Data['Title']  = "Admin | VendorManagement";
        
		return view('VendorEditView')->with($Data);
	} 

	public function SaveVendorDetails(Request $request)
	{
		$Data 							= $request->all();					
		$VendorID   					= $Data['VendorID'];
		$CompanyID      				= $Data['Company'];
		$Details['CampaignID']      	= $Data['Campaign'];
		$Details['CompanyID']      		= $Data['Company'];
		$Details['AdminLabel']      	= $Data['AdminLabel'];
		$Details['PartnerLabel']      	= $Data['PartnerLabel'];
		if($Data['CampaignTypeEdit']=='DirectPost')
		{
			$Details['MaxTimeOut']   = $Data['MaxTimeOut'];
		}
		else if($Data['CampaignTypeEdit']=='PingPost')
		{
			$Details['MaxPingTimeOut']   = $Data['MaxPingTimeOut'];
			$Details['MaxPostTimeOut']   = $Data['MaxPostTimeOut'];
		}
		$Details['PayoutCalculation']   = $Data['PayoutCalculation'];
		if($Data['PayoutCalculation']=='Revshare' || $Data['PayoutCalculation']=='MinPrice')
		{
			$Details['RevsharePercent']   = $Data['Revshare'];
		}
		else if($Data['PayoutCalculation']=='TierPrice')
		{
			$Details['RevsharePercent']   = $Data['TierPrice'];
		}
		else if($Data['PayoutCalculation']=='FixedPrice')
		{
			$Details['FixedPrice']   = $Data['FixedPrice'];	
		}
		else if($Data['PayoutCalculation']=='MinPrice')
		{
			$Details['RevsharePercent']   = $Data['Revshare'];	
		}
		else if($Data['PayoutCalculation']=='BucketPrice' || $Data['PayoutCalculation'] = "BucketSystemBySubID")
		{
			$Details['RevsharePercent'] = $Data['Revshare'];	
			$Details['FixedPrice']   	= $Data['FixedPrice'];	
		}

		$Details['DailyCap'] 	= $Data['DailyCap'];
		$Details['TotalCap'] 	= $Data['TotalCap'];
		$Details['TrackingPixelType'] 	= $Data['TrackingPixelType'];
		$Details['TrackingPixelStatus'] = 0;
		if(isset($Data['TrackingPixelStatus']) && $Data['TrackingPixelStatus']=='on')
		{
			$Details['TrackingPixelStatus'] = 1;
		}
		$Details['VendorStatus'] = 0;
		if(isset($Data['VendorStatus']) && $Data['VendorStatus']=='on')
		{
			$Details['VendorStatus'] = 1;
		}
		$Details['VendorTestMode'] = 0;
		if(isset($Data['VendorTestMode']) && $Data['VendorTestMode']=='on')
		{
			$Details['VendorTestMode'] = 1;
		}				
		$Details['TrackingPixelCode']   = $Data['TrackingPixelCode'];
		$Details['VendorNotes']   		= html_entity_decode($Data['VendorNotes']);
		/*echo "<pre>";
		print_r($Data);
		print_r($Details);
		exit();*/
		$SaveVendorDetails = $this->VMModel->SaveVendorDetails($VendorID,$Details);
		if($SaveVendorDetails)
		{
			Session::flash('message', 'Vendor Details Has been Updated.');
            Session::flash('alert-class', 'alert-success');
            if(isset($Data['From']) && $Data['From']=='Company')
            {
            	return Redirect::route('admin.CompanyEditView',base64_encode($CompanyID));      
            }
            else
            {
            	return Redirect::route('admin.VendorManagementView');            	
            }
		}
		else
		{
			Session::flash('message', 'OOPS! Something Wrong Please Try Again.');
            Session::flash('alert-class', 'alert-danger');
            if(isset($Data['From']) && $Data['From']=='Company')
            {
            	return Redirect::route('admin.CompanyEditView',base64_encode($CompanyID));      
            }
            else
            {
            	return Redirect::route('admin.VendorManagementView');            	
            }
		}
	}

	public function SetSessionForCloneVendor(Request $request)
	{
		$Data 		= $request->all();			
		$VendorID  	= $Data['VendorID'];
		$Details 	= $this->VMModel->VendorDetails($VendorID);
		if(!empty($Details))
		{
			$CompanyID = $Details->CompanyID;
			$CampaignID = $Details->CampaignID;
			$CampaignDetails = $this->CModel->GetCampaignDetails($CampaignID);
			$ShowActiveOrTest = $this->CModel->GetCompanyDetails($CompanyID);
			if(!empty($ShowActiveOrTest))
			{
				$ShowInactive 	= $ShowActiveOrTest->CompanyStatus;
				$ShowTest 		= $ShowActiveOrTest->CompanyTestMode;
			}
			$VendorDetails=array('VendorID' => $Details->VendorID,
							    'CompanyID' => $Details->CompanyID,
							    'CampaignID' => $Details->CampaignID,
							    'CampaignType' => $CampaignDetails->CampaignType,
							    'UserID' => $Details->UserID,
							    'AdminLabel' => $Details->AdminLabel,
							    'PartnerLabel' => $Details->PartnerLabel,
							    'PayoutCalculation' => $Details->PayoutCalculation,
							    'RevsharePercent' => $Details->RevsharePercent,
							    'FixedPrice' => $Details->FixedPrice,
							    'VendorStatus' => $Details->VendorStatus,
							    'VendorTestMode' => $Details->VendorTestMode,
							    'TrackingPixelType'=> $Details->TrackingPixelType,
								'TrackingPixelCode'=> $Details->TrackingPixelCode,
								'TrackingPixelStatus'=> $Details->TrackingPixelStatus,
							    'DailyCap' => $Details->DailyCap,
							    'TotalCap' => $Details->TotalCap,
							    'VendorNotes' => $Details->VendorNotes,
							    'ShowTest' => $ShowTest,
							    'ShowInactive' => $ShowInactive,
    							'IfClone' => "Yes",
    							'MaxTimeOut'=>$Details->MaxTimeOut,
    							'MaxPingTimeOut'=>$Details->MaxPingTimeOut,
    							'MaxPostTimeOut'=>$Details->MaxPostTimeOut,
    							'ReturnAPIURL'=>$Details->ReturnAPIURL
    						);

			$request->session()->put('VendorDetails', $VendorDetails);
			$request->session()->save();
			echo $VendorID;
		}
		else
		{
			echo 0;
		}
		exit();
	}
	public function GetPartnetLabel(Request $request)
	{
		$Data 		= $request->all();			
		$VendorID  	= $Data['VendorID'];
		$GetPartnerLabel = $this->VMModel->VendorDetails($VendorID);
		?>
		<td class="VendClas">
			
       		<input id="PartnerLabelText_<?php echo $GetPartnerLabel->VendorID; ?>" type="text" value="<?php echo $GetPartnerLabel->PartnerLabel; ?>" class="form-control txtPadd inFl" placeholder=""> 
       		
       		<div class="vbDiv">
	       		<img class="saveIcon" 
	       			src="<?php echo asset('Admin/images/check.png'); ?>" 
	   				title="save" alt="save-icon" onclick="SavePartnetLabel('Save','<?php echo $GetPartnerLabel->VendorID; ?>');">
	       		<img class="canIcon" 
	       			src="<?php echo asset('Admin/images/cancel.png'); ?>" 
	       			title="cancel" alt="cancel-icon" onclick="SavePartnetLabel('Cancel','<?php echo $GetPartnerLabel->VendorID; ?>');">
       		</div>
       	</td>
       	<td><?php echo $GetPartnerLabel->CampaignName; ?></td>
       	<td><?php echo $GetPartnerLabel->VendorID; ?></td>
       	<td><?php echo $GetPartnerLabel->APIKey; ?></td>
		<?php
		exit();
	}

	public function SavePartnetLabel(Request $request)
	{
		$Data 		= $request->all();			
		$VendorID  	= $Data['VendorID'];
		$PartnerLabel  	= $Data['PartnerLabel'];
		$Action  	= $Data['Action'];		
		if($Action=='Save')
		{
			$Details['PartnerLabel'] = $PartnerLabel;
			$SavePartnetLabel 	= $this->VMModel->SaveVendorDetails($VendorID,$Details);
			$GetPartnerLabel 	= $this->VMModel->VendorDetails($VendorID);
			?>
			<td onclick="GetPartnetLabel('<?php echo $VendorID; ?>');" class="VendClas">
				<?php echo $GetPartnerLabel->PartnerLabel; ?>
				<img class="queIcon" 
           			src="https://dashboardv2-dev.revjolt.net/Admin/images/help.png" 
           			title="Double Click To Change The Vendor Name." alt="save-icon">
			</td>
	       	<td><?php echo $GetPartnerLabel->CampaignName; ?></td>
	       	<td><?php echo $GetPartnerLabel->VendorID; ?></td>
	       	<td><?php echo $GetPartnerLabel->APIKey; ?></td>
			<?php
		}
		else
		{
			$GetPartnerLabel = $this->VMModel->VendorDetails($VendorID);
			?>
			<td onclick="GetPartnetLabel('<?php echo $VendorID; ?>');" class="VendClas" >
				<?php echo $GetPartnerLabel->PartnerLabel; ?>
				<img class="queIcon" 
           			src="https://dashboardv2-dev.revjolt.net/Admin/images/help.png" 
           			title="Double Click To Change The Vendor Name." alt="save-icon">
			</td>
	       	<td><?php echo $GetPartnerLabel->CampaignName; ?></td>
	       	<td><?php echo $GetPartnerLabel->VendorID; ?></td>
	       	<td><?php echo $GetPartnerLabel->APIKey; ?></td>
			<?php
		}
		exit();
	}
	public function SendEmailToAdminUser(Request $request)
	{		
		$data   	= $request->all();
		$Company 	= $data['Company'];
		$VendorID 	= $data['VendorID'];
		$Campaign 	= $data['Campaign'];
		$VendorAPIKey = $data['VendorApiKey'];
		$AdminLabel = $data['AdminLabel'];
	
		$MarketingEmail = $this->MasterConfigModel->getMarketingEmail();
        
       	$SiteName= $this->MasterConfigModel->getSiteName();
     
        $DashboardDomain = $this->MasterConfigModel->getDashboardDomain();
      
        $CompanyNameArray = DB::table('partner_companies')
                ->select('CompanyName')
                ->where('CompanyID',$Company)
                ->get()
                ->first(); 
        $CompanyName = $CompanyNameArray->CompanyName;
        $VendorIDArray = DB::table('partner_vendor_campaigns')
                ->select('VendorID')
                ->where('AdminLabel',$AdminLabel)
                ->get()
                ->first(); 
           
        $CompanyAdmins = DB::table('partner_users')
                ->select('UserEmail')
                ->where('CompanyID',$Company)
                ->where('UserRole',DB::raw("2"))
                ->where('UserStatus','=','1')
                ->get()
                ->toArray();
        $result = 0;
        foreach ($CompanyAdmins as $value) 
       	{
			//$to     = $value->UserEmail;	
            $subject = "Welcome To ".$SiteName;
            $headers = "From: <".$MarketingEmail.">";
            $message='';
            $message.="Your new vendor account has been setup!\n\n";
            $message.="Company: ".$CompanyName."(".$Company.")\n";
           	$message.="VendorID: ".$VendorID."\n";
            $message.="APIKey: ".$VendorAPIKey."\n\n";
            $message.="To view your Specs, please visit https://".$DashboardDomain."/view-specs//MQ==\n\n";
            $message.="If you are not currently logged in, you will need to use your email and password. If you cannot remember your password, click the <a href=\"".$DashboardDomain."/logout#\">'Forgot your password?'</a> link.\n\n";
            $message.="Kind regards,\n";
            $message.= $SiteName." Team\n";
            //$result = mail('mrigank.shekhar012gmail.com',$subject,$message,$headers);	            
            $result = mail('honey@nettechnocrats.com',$subject,$message,$headers);	            
        }	
        $Response='';    
        if ($result == 1) 
        {
           $Response='<div class="alert alert-success alert-dismissible fade show" role="alert">
					  Email Sent Successfully.
					  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
					    <span aria-hidden="true">&times;</span>
					  </button>
					</div>';
        }
        else
        {
        	 $Response='<div class="alert alert-danger alert-dismissible fade show" role="alert">
					  Something Wrong Please Try Again.
					  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
					    <span aria-hidden="true">&times;</span>
					  </button>
					</div>';
        }
        echo $Response;
        exit();
	}
	/////////////////////
	public function GetVendorDetails(Request $request)
	{
		$Data 			= $request->all();
		$VendorID  		= $Data['VendorID'];
		$VendorDetails 		= $this->VMModel->VendorDetails($VendorID);
		$CompanyID 			= $VendorDetails->CompanyID;
		$CompanyDetails		= $this->VMModel->CompanyDetails($CompanyID);
		$CompanyTestMode 	= $CompanyDetails->CompanyTestMode;
		$CompanyStatus 		= $CompanyDetails->CompanyStatus;
		$CompanyList 		= $this->VMModel->CompanyListForEdit($CompanyTestMode,$CompanyStatus);

		$VendorDetails 		= $VendorDetails;
		$CampaignDropDown 	= $this->MasterConfigModel->getCampaignList();
		$CampaignID 		= $VendorDetails->CampaignID;
		$CampaignDetails 	= $this->CModel->GetCampaignDetails($CampaignID);

		$ShowInactive="checked";
		$ShowTest="";
		if($CompanyStatus==1)
		{
			$ShowInactive="";
		}
		if($CompanyTestMode==1)
		{
			$ShowTest="checked";
		}
		?>
		<form action="<?php echo route('admin.SaveVendorDetails');?>" name="VendorUpdateForm" id="VendorUpdateForm" method="POST">
      	<input type="hidden" name="FormType" id="FormType" value="Update">
      	<input type="hidden" name="VendorID" id="VendorID" value="<?php echo $VendorDetails->VendorID; ?>">
      	<input type="hidden" name="VendorApiKey" id="VendorApiKey" value="<?php echo $VendorDetails->APIKey; ?>">
      	<input type="hidden" name="_token" id="_token" value="<?php echo  csrf_token(); ?>">
      	<input type="hidden" name="CampaignTypeEdit" id="CampaignTypeEdit" value="<?php echo $CampaignDetails->CampaignType; ?>">
			<div class="row">
				<div class="col-md-12" id="MessageEdit">
				
				</div>
			</div>
			<div class="row">
				<div class="col-md-3 form-group mb-0 tadmPadd labFontt">
				  <label for="exampleFormControlSelect1">Vendor ID:</label>
				  <input type="text" disabled class="form-control txtPadd" 
				  value="<?php echo $VendorDetails->VendorID; ?>" placeholder="">
				</div>
				<div class="col-md-5 form-group mb-0 tadmPadd labFontt">
				  <label for="exampleFormControlSelect1">Vendor API-KEY:</label>
				  <input type="text" disabled class="form-control txtPadd" 
				    value="<?php echo $VendorDetails->APIKey; ?>" placeholder="">
				</div>
				<div class="col-md-4 form-group mb-0 tadmPadd labFontt">
				  <label for="exampleFormControlSelect1">Company:</label>
				    <select class="form-control fonT" id="CompanyEdit" name="Company"
				      onchange="HideErr('CompanyErr');">
				      <option value="">Select Company</option>
				      <?php foreach($CompanyList as $cl){ ?>
				      <option value="<?php echo $cl->CompanyID; ?>"
				      <?php if($cl->CompanyID==$VendorDetails->CompanyID){echo "selected";}?>>
				      	<?php echo $cl->CompanyName; ?></option>
				      <?php } ?>                                 
				    </select>
				    <span id="CompanyErr"></span>
				     <div class="form-check form-check-flat form-check-primary adComp adComp-primary disPlay tabdisPlay tMrg">
					    <label class="form-check-label adComp-label">
					    <input type="checkbox" class="form-check-input" id="ShowInactiveEdit" <?php echo $ShowInactive; ?>
					    onclick="GetCompanyUsingShwowInactiveAndTestEdit();">
					      Show Inactive
					    <i class="input-helper"></i></label>
					  </div>
					  <div class="form-check form-check-flat form-check-primary adComp adComp-primary disPlay tabdisPlay tMrg">
					    <label class="form-check-label adComp-label">
					    <input type="checkbox" class="form-check-input" id="ShowTestEdit" <?php echo $ShowTest; ?>
					    onclick="GetCompanyUsingShwowInactiveAndTestEdit();">
					      Show Test
					    <i class="input-helper"></i></label>
					  </div>
				</div>
			</div> 
			<div class="row">
				<div class="col-md-4 form-group mb-0 tadmPadd labFontt">
				    <label for="exampleFormControlSelect1">Campaign:</label>
				    <select class="form-control fonT" id="CampaignEdit" name="Campaign" 
				        onchange="GetCampaignDetailsEdit(),HideErr('CompanyErr');">
				        <?php foreach($CampaignDropDown as $cdd){ ?>
				        <option value="<?php echo $cdd->CampaignID; ?>"
				          <?php if($cdd->CampaignID==$VendorDetails->CampaignID){echo "selected";}?>>
				          	<?php echo $cdd->CampaignName; ?></option>
				        <?php } ?>  
				    </select>
				    <span id="CampaignEditErr"></span>
				</div>
				<div class="col-md-3 form-group tadmPadd labFontt">
				  <label for="exampleInputEmail1">Admin Label:</label>
				    <input type="text" class="form-control txtPadd" 
				    value="<?php echo $VendorDetails->AdminLabel; ?>" id="AdminLabelEdit" name="AdminLabel"
				    onkeypress="HideErr('AdminLabelEditErr');">
				    <span id="AdminLabelEditErr"></span>
				</div>
				<div class="col-md-1 form-group tadmPadd btnCen">
				  <button type="button" id="CopyAdminLable" onclick="CopyAdminLableEdit();" class="btn btn-success noBoradi mr-2">&gt;&gt;</button>
				</div>
				<div class="col-md-3 form-group tadmPadd labFontt">
				  <label for="exampleInputEmail1">Partner Label:</label>
				    <input type="text" class="form-control txtPadd" 
				      value="<?php echo $VendorDetails->PartnerLabel; ?>" id="PartnerLabelEdit" name="PartnerLabel"
				      onkeypress="HideErr('PartnerLabelEditErr');">
				    <span id="PartnerLabelEditErr"></span>
				</div>
			</div>
			<div class="row">
				<div class="col-md-3 form-group mb-0 tadmPadd labFontt">
				  <label for="PayoutCalculation">Payout Calculation:</label>
				    <select class="form-control fonT" id="PayoutCalculationEdit" name="PayoutCalculation" onchange="PayoutCalculationChange();">
				      <option value="RevShare" <?php if($VendorDetails->PayoutCalculation=="RevShare"){ echo "selected"; } ?>>RevShare</option>
				      <option value="FixedPrice" <?php if($VendorDetails->PayoutCalculation=="FixedPrice"){ echo "selected"; } ?>>Fixed Price</option>
				      <option value="TierPrice" <?php if($VendorDetails->PayoutCalculation=="TierPrice"){ echo "selected"; } ?>>Tier Price</option>
				      <option value="MinPrice" <?php if($VendorDetails->PayoutCalculation=="MinPrice"){ echo "selected"; } ?>>Min Price</option>
				      <option value="BucketPrice" <?php if($VendorDetails->PayoutCalculation=="BucketPrice"){ echo "selected"; } ?>>Bucket Price</option>
				      <option value="BucketSystemBySubID" <?php if($VendorDetails->PayoutCalculation=="BucketSystemBySubID"){ echo "selected"; } ?>>Bucket System By SubID</option>
				    </select>
				</div>
				<?php 
				$PayoutCalculation = $VendorDetails->PayoutCalculation;
				if($PayoutCalculation=='FixedPrice')
				{ 
				     $PayoutCalculationFixedPrice="style='display:block'";
				     $PayoutCalculationTierPrice="style='display:none'";
				     $PayoutCalculationRevShare="style='display:none'";
				} 
				else if($PayoutCalculation=='RevShare')
				{
				     $PayoutCalculationRevShare="style='display:block'";
				     $PayoutCalculationTierPrice="style='display:none'";
				     $PayoutCalculationFixedPrice="style='display:none'";
				}
				 else if($PayoutCalculation=='TierPrice')
				{
				     $PayoutCalculationRevShare="style='display:none'";
				     $PayoutCalculationTierPrice="style='display:block'";
				     $PayoutCalculationFixedPrice="style='display:none'";
				}
				 else if($PayoutCalculation=='MinPrice' )
				{
				     $PayoutCalculationRevShare="style='display:block'";
				     $PayoutCalculationTierPrice="style='display:none'";
				     $PayoutCalculationFixedPrice="style='display:none'";
				}
				 else if($PayoutCalculation=='BucketSystemBySubID' || $PayoutCalculation=='BucketPrice')
				{
				     $PayoutCalculationRevShare="style='display:block'";
				     $PayoutCalculationTierPrice="style='display:none'";
				     $PayoutCalculationFixedPrice="style='display:block'";
				}
				?>
				<div class="col-md-2 form-group tadmPadd labFontt" <?php echo $PayoutCalculationRevShare ?>  
				    id="RevsharePercentInputEdit">
				  <label for="exampleInputEmail1">Revshare: </label>
				  <input type="text" class="form-control txtPadd" value="<?php echo $VendorDetails->RevsharePercent; ?>" 
				  placeholder="70" id="RevshareEdit" name="Revshare" onkeypress="HideErr('RevshareEditErr');">
				  <span id="RevshareEditErr"></span>
				</div>
				<div class="col-md-2 form-group tadmPadd labFontt" <?php echo $PayoutCalculationFixedPrice ?> 
				    id="FixedPriceInputEdit">
				  <label for="exampleInputEmail1">Fixed Price:</label>
				  <input type="text" class="form-control txtPadd" value="<?php echo $VendorDetails->FixedPrice; ?>" 
				  placeholder="70" id="FixedPriceEdit" name="FixedPrice" onkeypress="HideErr('FixedPriceEditErr');">
				  <span id="FixedPriceEditErr"></span>
				</div>
				<div class="col-md-5 form-group tadmPadd labFontt" <?php echo $PayoutCalculationTierPrice ?> 
				    id="TierPriceInputEdit">
				  <label for="exampleInputEmail1">Revshare Percent (for when Tier Payout is $0.00):</label>
				  <input type="text" class="form-control txtPadd" value="<?php echo $VendorDetails->RevsharePercent; ?>"
				   placeholder="70" id="TierPriceEdit" name="TierPrice" onkeypress="HideErr('TierPriceEditErr');">
				  <span id="TierPriceEditErr"></span>
				</div>
				<div class="col-md-2 form-group tadmPadd labFontt">
				  <label for="exampleInputEmail1">Daily Cap:</label>
				    <input type="text"  value="<?php echo $VendorDetails->DailyCap; ?>" 
				      id="DailyCapEdit" name="DailyCap" class="form-control txtPadd" placeholder="Daily Cap"
				      onkeypress="HideErr('DailyCapEditErr');">
				    <span id="DailyCapEditErr"></span>
				</div>
				<div class="col-md-2 form-group tadmPadd labFontt">
				  <label for="exampleInputEmail1">Total Cap:</label>
				    <input type="text" value="<?php echo $VendorDetails->TotalCap; ?>" 
				      id="TotalCapEdit" name="TotalCap" class="form-control txtPadd" placeholder="Total Cap"
				      onkeypress="HideErr('TotalCapEditErr');">
				    <span id="TotalCapEditErr"></span>
				</div>
			</div>
			<div class="row">				
				<?php if($VendorDetails->VendorStatus=='1'){ ?>
				<div class="col-md-2 form-group tadmPadd labFontt">
				  <label for="exampleInputEmail1">Vendor Status:</label>
				    <div class="onoffswitch8">
				      <input type="checkbox" name="VendorStatus" class="onoffswitch8-checkbox" 
				      id="VendorStatusEdit" checked="">
				      <label class="onoffswitch8-label" for="VendorStatusEdit">
				        <span class="onoffswitch8-inner"></span>
				        <span class="onoffswitch8-switch"></span>
				      </label>
				    </div>
				</div>
				<?php } else {?>
				<div class="col-md-2 form-group tadmPadd labFontt">
				  <label for="exampleInputEmail1">Vendor Status:</label>
				    <div class="onoffswitch8">
				      <input type="checkbox" name="VendorStatus" class="onoffswitch8-checkbox" 
				      id="VendorStatusEdit">
				      <label class="onoffswitch8-label" for="VendorStatusEdit">
				        <span class="onoffswitch8-inner"></span>
				        <span class="onoffswitch8-switch"></span>
				      </label>
				    </div>
				</div>
				<?php }?>
				<?php if($VendorDetails->VendorTestMode=='1'){ ?>
				<div class="col-md-2 form-group tadmPadd labFontt">
				  <label for="exampleInputEmail1">Vendor Mode?</label>
				    <div class="onoffswitch9">
				      <input type="checkbox" name="VendorTestMode" class="onoffswitch9-checkbox"
				         id="VendorTestMode" checked="">
				      <label class="onoffswitch9-label" for="VendorTestMode">
				        <span class="onoffswitch9-inner"></span>
				        <span class="onoffswitch9-switch"></span>
				      </label>
				    </div>
				</div>
				<?php } else {?>
				<div class="col-md-2 form-group tadmPadd labFontt">
				  <label for="exampleInputEmail1">Vendor Mode?</label>
				    <div class="onoffswitch9">
				      <input type="checkbox" name="VendorTestMode" class="onoffswitch9-checkbox" 
				        id="VendorTestMode" >
				      <label class="onoffswitch9-label" for="VendorTestMode">
				        <span class="onoffswitch9-inner"></span>
				        <span class="onoffswitch9-switch"></span>
				      </label>
				    </div>
				</div>
				<?php }?>
				<div class="col-md-2 form-group mb-0 tadmPadd labFontt">
				  <label for="exampleFormControlSelect1">Tracking Pixel Type:</label>
				    <select class="form-control fonT" id="TrackingPixelType" name="TrackingPixelType">
				      <option value="Pixel" <?php if($VendorDetails->TrackingPixelType=='Pixel'){echo "selected";}?>>Pixel</option>
				      <option value="PostBack" <?php if($VendorDetails->TrackingPixelType=='PostBack'){echo "selected";}?>>Post Back</option>
				    </select>
				</div>
				<?php if($VendorDetails->TrackingPixelStatus=='1'){ ?>
				<div class="col-md-2 form-group tadmPadd labFontt">
				  <label for="exampleInputEmail1">Tracking Pixel Status?</label>
				    <div class="onoffswitch10">
				      <input type="checkbox" name="TrackingPixelStatus" class="onoffswitch10-checkbox"
				       id="TrackingPixelStatus" checked="">
				      <label class="onoffswitch10-label" for="TrackingPixelStatus">
				        <span class="onoffswitch10-inner"></span>
				        <span class="onoffswitch10-switch"></span>
				      </label>
				    </div>
				</div>
				<?php } else {?>
				<div class="col-md-2 form-group tadmPadd labFontt">
				  <label for="exampleInputEmail1">Tracking Pixel Status?</label>
				    <div class="onoffswitch10">
				      <input type="checkbox" name="TrackingPixelStatus" class="onoffswitch10-checkbox"
				       id="TrackingPixelStatus" >
				      <label class="onoffswitch10-label" for="TrackingPixelStatus">
				        <span class="onoffswitch10-inner"></span>
				        <span class="onoffswitch10-switch"></span>
				      </label>
				    </div>
				</div>
				<?php }?>
				<?php 
				if($CampaignDetails->CampaignType=="DirectPost"){                           
				  $MaxTimeOutInput = "style='display:block;'";
				  $MaxPingTimeOutInput = "style='display:none;'";
				  $MaxPostTimeOutInput = "style='display:none;'";
				}else if($CampaignDetails->CampaignType=="PingPost"){
				  $MaxTimeOutInput = "style='display:none;'";
				  $MaxPingTimeOutInput = "style='display:block;'";
				  $MaxPostTimeOutInput = "style='display:block;'";
				}
				?>
				 <div class="col-md-2 form-group tadmPadd labFontt" id="MaxTimeOutInputEdit" <?php echo $MaxTimeOutInput; ?>>
				  <label for="exampleInputEmail1">Max Time Out:</label>
				    <input type="text" class="form-control txtPadd" placeholder="0" 
				    id="MaxTimeOutEdit" name="MaxTimeOut" value="<?php echo $VendorDetails->MaxTimeOut; ?>" onkeypress="HideErr('MaxTimeOutEditErr');">
				    <span id="MaxTimeOutEditErr"></span>
				</div>
				<div class="col-md-2 form-group tadmPadd labFontt" id="MaxPingTimeOutInputEdit" <?php echo $MaxPingTimeOutInput; ?>>
				  <label for="exampleInputEmail1">Max Ping Time Out:</label>
				    <input type="text" class="form-control txtPadd" placeholder="0" 
				    id="MaxPingTimeOutEdit" name="MaxPingTimeOut" value="<?php echo $VendorDetails->MaxPingTimeOut; ?>" onkeypress="HideErr('MaxPingTimeOutEditErr');">
				    <span id="MaxPingTimeOutEditErr"></span>
				</div>
				<div class="col-md-2 form-group tadmPadd labFontt" id="MaxPostTimeOutInputEdit" <?php echo $MaxPostTimeOutInput; ?>>
				  <label for="exampleInputEmail1">Max Post Time Out:</label>
				    <input type="text" class="form-control txtPadd" placeholder="0" 
				    id="MaxPostTimeOutEdit" name="MaxPostTimeOut" value="<?php echo $VendorDetails->MaxPostTimeOut; ?>" onkeypress="HideErr('MaxPostTimeOutEditErr');">
				    <span id="MaxPostTimeOutEditErr"></span>
				</div> 
			</div>
			<div class="row">
				<div class="col-md-2 form-group mb-0 labFontt">
				  <label for="exampleFormControlSelect1">Insert Token:</label>
				    <select class="form-control" id="insert_Token_type" name="insert_Token_type" onchange="updateTextBox(this)">
				      <option value="aid">aid</option>
				      <option value="SubID">SubID</option>
				      <option value="TestMode">TestMode</option>
				      <option value="TransactionID">TransactionID</option>
				      <option value="VendorLeadPrice">VendorLeadPrice</option>
				  </select>
				</div>
				<div class="col-md-5 form-group mb-0 labFontt">
				  <label for="exampleFormControlTextarea1">Tracking Pixel Code:</label>
				  <textarea class="form-control" id="TrackingPixelCode" name="TrackingPixelCode" rows="3"><?php echo $VendorDetails->TrackingPixelCode; ?></textarea>
				</div>
				<div class="col-md-5 form-group mb-0 labFontt">
				  <label for="exampleFormControlTextarea1">Vendor Notes:</label>
				  <textarea class="form-control" id="VendorNotes" name="VendorNotes" rows="3"><?php echo $VendorDetails->VendorNotes; ?></textarea>
				</div>
			</div>
	        <div class="row">
	        </div>
		    <div class="row">
		      	<div class="col-md-12 form-group mb-0 labFontt">
		       		<button type="button" class="btn btn-warning btn-fw noBoradi mr-2" onclick="SendMailToAdminUser();">Send E-Mail To Admin User.</button>
		        	<button type="button" class="btn btn-success btn-fw noBoradi mr-2" onclick="SaveVendorDetails();">Update</button>
		        	<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
		      	</div>
		    </div>
		</form>
		<?php		
	}
	/////////////////////
	public function GetVendorCloneDetails(Request $request)
	{
		$Data 			= $request->all();
		$VendorID  		= $Data['VendorID'];
		$VendorDetails 		= $this->VMModel->VendorDetails($VendorID);
		$CompanyID 			= $VendorDetails->CompanyID;
		$CompanyDetails		= $this->VMModel->CompanyDetails($CompanyID);
		$CompanyTestMode 	= $CompanyDetails->CompanyTestMode;
		$CompanyStatus 		= $CompanyDetails->CompanyStatus;
		$CompanyList 		= $this->VMModel->CompanyListForEdit($CompanyTestMode,$CompanyStatus);

		$VendorDetails 		= $VendorDetails;
		$CampaignDropDown 	= $this->MasterConfigModel->getCampaignList();
		$CampaignID 		= $VendorDetails->CampaignID;
		$CampaignDetails 	= $this->CModel->GetCampaignDetails($CampaignID);

		$ShowInactive="checked";
		$ShowTest="";
		if($CompanyStatus==1)
		{
			$ShowInactive="";
		}
		if($CompanyTestMode==1)
		{
			$ShowTest="checked";
		}
		?>
		<form action="<?php echo route('admin.AddVendorDetails');?>" name="VendorUpdateForm" id="VendorUpdateForm" method="POST">
      	
      	<input type="hidden" name="FormType" id="FormType" value="Clone">
      	<input type="hidden" name="_token" id="_token" value="<?php echo  csrf_token(); ?>">
      	<input type="hidden" name="CampaignTypeEdit" id="CampaignTypeEdit" value="<?php echo $CampaignDetails->CampaignType; ?>">
			<div class="row">
				<div class="col-md-12" id="MessageEdit">
				
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 form-group mb-0 tadmPadd labFontt">
				  <label for="exampleFormControlSelect1">Company:</label>
				    <select class="form-control fonT" id="CompanyEdit" name="Company"
				      onchange="HideErr('CompanyErr');">
				      <option value="">Select Company</option>
				      <?php foreach($CompanyList as $cl){ ?>
				      <option value="<?php echo $cl->CompanyID; ?>"
				      <?php if($cl->CompanyID==$VendorDetails->CompanyID){echo "selected";}?>>
				      	<?php echo $cl->CompanyName; ?></option>
				      <?php } ?>                                 
				    </select>
				    <span id="CompanyErr"></span>
				     <div class="form-check form-check-flat form-check-primary adComp adComp-primary disPlay tabdisPlay tMrg">
					    <label class="form-check-label adComp-label">
					    <input type="checkbox" class="form-check-input" id="ShowInactiveEdit" <?php echo $ShowInactive; ?>
					    onclick="GetCompanyUsingShwowInactiveAndTestEdit();">
					      Show Inactive
					    <i class="input-helper"></i></label>
					  </div>
					  <div class="form-check form-check-flat form-check-primary adComp adComp-primary disPlay tabdisPlay tMrg">
					    <label class="form-check-label adComp-label">
					    <input type="checkbox" class="form-check-input" id="ShowTestEdit" <?php echo $ShowTest; ?>
					    onclick="GetCompanyUsingShwowInactiveAndTestEdit();">
					      Show Test
					    <i class="input-helper"></i></label>
					  </div>
				</div>
				<div class="col-md-3 form-group mb-0 tadmPadd labFontt">
				    <label for="exampleFormControlSelect1">Campaign:</label>
				    <select class="form-control fonT" id="CampaignEdit" name="Campaign" 
				        onchange="GetCampaignDetails(),HideErr('CompanyErr');">
				        <?php foreach($CampaignDropDown as $cdd){ ?>
				        <option value="<?php echo $cdd->CampaignID; ?>"
				          <?php if($cdd->CampaignID==$VendorDetails->CampaignID){echo "selected";}?>>
				          	<?php echo $cdd->CampaignName; ?></option>
				        <?php } ?>  
				    </select>
				    <span id="CampaignEditErr"></span>
				</div>
				<div class="col-md-2 form-group tadmPadd labFontt">
				  <label for="exampleInputEmail1">Admin Label:</label>
				    <input type="text" class="form-control txtPadd" 
				    value="<?php echo $VendorDetails->AdminLabel; ?>" id="AdminLabelEdit" name="AdminLabel"
				    onkeypress="HideErr('AdminLabelEditErr');">
				    <span id="AdminLabelEditErr"></span>
				</div>
				<div class="col-md-1 form-group tadmPadd btnCen">
				  <button type="button" id="CopyAdminLable" onclick="CopyAdminLableEdit();" class="btn btn-success noBoradi mr-2">&gt;&gt;</button>
				</div>
				<div class="col-md-2 form-group tadmPadd labFontt">
				  <label for="exampleInputEmail1">Partner Label:</label>
				    <input type="text" class="form-control txtPadd" 
				      value="<?php echo $VendorDetails->PartnerLabel; ?>" id="PartnerLabelEdit" name="PartnerLabel"
				      onkeypress="HideErr('PartnerLabelEditErr');">
				    <span id="PartnerLabelEditErr"></span>
				</div>
			</div> 
			<div class="row">
				<div class="col-md-3 form-group mb-0 tadmPadd labFontt">
				  <label for="PayoutCalculation">Payout Calculation:</label>
				    <select class="form-control fonT" id="PayoutCalculationEdit" name="PayoutCalculation" onchange="PayoutCalculationChange();">
				      <option value="RevShare" <?php if($VendorDetails->PayoutCalculation=="RevShare"){ echo "selected"; } ?>>RevShare</option>
				      <option value="FixedPrice" <?php if($VendorDetails->PayoutCalculation=="FixedPrice"){ echo "selected"; } ?>>Fixed Price</option>
				      <option value="TierPrice" <?php if($VendorDetails->PayoutCalculation=="TierPrice"){ echo "selected"; } ?>>Tier Price</option>
				      <option value="MinPrice" <?php if($VendorDetails->PayoutCalculation=="MinPrice"){ echo "selected"; } ?>>Min Price</option>
				      <option value="BucketPrice" <?php if($VendorDetails->PayoutCalculation=="BucketPrice"){ echo "selected"; } ?>>Bucket Price</option>
				      <option value="BucketSystemBySubID" <?php if($VendorDetails->PayoutCalculation=="BucketSystemBySubID"){ echo "selected"; } ?>>Bucket System By SubID</option>
				    </select>
				</div>
				<?php 
				$PayoutCalculation = $VendorDetails->PayoutCalculation;
				if($PayoutCalculation=='FixedPrice')
				{ 
				     $PayoutCalculationFixedPrice="style='display:block'";
				     $PayoutCalculationTierPrice="style='display:none'";
				     $PayoutCalculationRevShare="style='display:none'";
				} 
				else if($PayoutCalculation=='RevShare')
				{
				     $PayoutCalculationRevShare="style='display:block'";
				     $PayoutCalculationTierPrice="style='display:none'";
				     $PayoutCalculationFixedPrice="style='display:none'";
				}
				 else if($PayoutCalculation=='TierPrice')
				{
				     $PayoutCalculationRevShare="style='display:none'";
				     $PayoutCalculationTierPrice="style='display:block'";
				     $PayoutCalculationFixedPrice="style='display:none'";
				}
				 else if($PayoutCalculation=='MinPrice' )
				{
				     $PayoutCalculationRevShare="style='display:block'";
				     $PayoutCalculationTierPrice="style='display:none'";
				     $PayoutCalculationFixedPrice="style='display:none'";
				}
				 else if($PayoutCalculation=='BucketSystemBySubID' || $PayoutCalculation=='BucketPrice')
				{
				     $PayoutCalculationRevShare="style='display:block'";
				     $PayoutCalculationTierPrice="style='display:none'";
				     $PayoutCalculationFixedPrice="style='display:block'";
				}
				?>
				<div class="col-md-2 form-group tadmPadd labFontt" <?php echo $PayoutCalculationRevShare ?>  
				    id="RevsharePercentInputEdit">
				  <label for="exampleInputEmail1">Revshare: </label>
				  <input type="text" class="form-control txtPadd" value="<?php echo $VendorDetails->RevsharePercent; ?>" 
				  placeholder="70" id="RevshareEdit" name="Revshare" onkeypress="HideErr('RevshareEditErr');">
				  <span id="RevshareEditErr"></span>
				</div>
				<div class="col-md-2 form-group tadmPadd labFontt" <?php echo $PayoutCalculationFixedPrice ?> 
				    id="FixedPriceInputEdit">
				  <label for="exampleInputEmail1">Fixed Price:</label>
				  <input type="text" class="form-control txtPadd" value="<?php echo $VendorDetails->FixedPrice; ?>" 
				  placeholder="70" id="FixedPriceEdit" name="FixedPrice" onkeypress="HideErr('FixedPriceEditErr');">
				  <span id="FixedPriceEditErr"></span>
				</div>
				<div class="col-md-5 form-group tadmPadd labFontt" <?php echo $PayoutCalculationTierPrice ?> 
				    id="TierPriceInputEdit">
				  <label for="exampleInputEmail1">Revshare Percent (for when Tier Payout is $0.00):</label>
				  <input type="text" class="form-control txtPadd" value="<?php echo $VendorDetails->RevsharePercent; ?>"
				   placeholder="70" id="TierPriceEdit" name="TierPrice" onkeypress="HideErr('TierPriceEditErr');">
				  <span id="TierPriceEditErr"></span>
				</div>
				<div class="col-md-2 form-group tadmPadd labFontt">
				  <label for="exampleInputEmail1">Daily Cap:</label>
				    <input type="text"  value="<?php echo $VendorDetails->DailyCap; ?>" 
				      id="DailyCapEdit" name="DailyCap" class="form-control txtPadd" placeholder="Daily Cap"
				      onkeypress="HideErr('DailyCapEditErr');">
				    <span id="DailyCapEditErr"></span>
				</div>
				<div class="col-md-2 form-group tadmPadd labFontt">
				  <label for="exampleInputEmail1">Total Cap:</label>
				    <input type="text" value="<?php echo $VendorDetails->TotalCap; ?>" 
				      id="TotalCapEdit" name="TotalCap" class="form-control txtPadd" placeholder="Total Cap"
				      onkeypress="HideErr('TotalCapEditErr');">
				    <span id="TotalCapEditErr"></span>
				</div>
			</div>
			<div class="row">				
				<?php if($VendorDetails->VendorStatus=='1'){ ?>
				<div class="col-md-2 form-group tadmPadd labFontt">
				  <label for="exampleInputEmail1">Vendor Status:</label>
				    <div class="onoffswitch8">
				      <input type="checkbox" name="VendorStatus" class="onoffswitch8-checkbox" 
				      id="VendorStatusEdit" checked="">
				      <label class="onoffswitch8-label" for="VendorStatusEdit">
				        <span class="onoffswitch8-inner"></span>
				        <span class="onoffswitch8-switch"></span>
				      </label>
				    </div>
				</div>
				<?php } else {?>
				<div class="col-md-2 form-group tadmPadd labFontt">
				  <label for="exampleInputEmail1">Vendor Status:</label>
				    <div class="onoffswitch8">
				      <input type="checkbox" name="VendorStatus" class="onoffswitch8-checkbox" 
				      id="VendorStatusEdit">
				      <label class="onoffswitch8-label" for="VendorStatusEdit">
				        <span class="onoffswitch8-inner"></span>
				        <span class="onoffswitch8-switch"></span>
				      </label>
				    </div>
				</div>
				<?php }?>
				<?php if($VendorDetails->VendorTestMode=='1'){ ?>
				<div class="col-md-2 form-group tadmPadd labFontt">
				  <label for="exampleInputEmail1">Vendor Mode?</label>
				    <div class="onoffswitch9">
				      <input type="checkbox" name="VendorTestMode" class="onoffswitch9-checkbox"
				         id="VendorTestMode" checked="">
				      <label class="onoffswitch9-label" for="VendorTestMode">
				        <span class="onoffswitch9-inner"></span>
				        <span class="onoffswitch9-switch"></span>
				      </label>
				    </div>
				</div>
				<?php } else {?>
				<div class="col-md-2 form-group tadmPadd labFontt">
				  <label for="exampleInputEmail1">Vendor Mode?</label>
				    <div class="onoffswitch9">
				      <input type="checkbox" name="VendorTestMode" class="onoffswitch9-checkbox" 
				        id="VendorTestMode" >
				      <label class="onoffswitch9-label" for="VendorTestMode">
				        <span class="onoffswitch9-inner"></span>
				        <span class="onoffswitch9-switch"></span>
				      </label>
				    </div>
				</div>
				<?php }?>
				<div class="col-md-2 form-group mb-0 tadmPadd labFontt">
				  <label for="exampleFormControlSelect1">Tracking Pixel Type:</label>
				    <select class="form-control fonT" id="TrackingPixelType" name="TrackingPixelType">
				      <option value="Pixel" <?php if($VendorDetails->TrackingPixelType=='Pixel'){echo "selected";}?>>Pixel</option>
				      <option value="PostBack" <?php if($VendorDetails->TrackingPixelType=='PostBack'){echo "selected";}?>>Post Back</option>
				    </select>
				</div>
				<?php if($VendorDetails->TrackingPixelStatus=='1'){ ?>
				<div class="col-md-2 form-group tadmPadd labFontt">
				  <label for="exampleInputEmail1">Tracking Pixel Status?</label>
				    <div class="onoffswitch10">
				      <input type="checkbox" name="TrackingPixelStatus" class="onoffswitch10-checkbox"
				       id="TrackingPixelStatus" checked="">
				      <label class="onoffswitch10-label" for="TrackingPixelStatus">
				        <span class="onoffswitch10-inner"></span>
				        <span class="onoffswitch10-switch"></span>
				      </label>
				    </div>
				</div>
				<?php } else {?>
				<div class="col-md-2 form-group tadmPadd labFontt">
				  <label for="exampleInputEmail1">Tracking Pixel Status?</label>
				    <div class="onoffswitch10">
				      <input type="checkbox" name="TrackingPixelStatus" class="onoffswitch10-checkbox"
				       id="TrackingPixelStatus" >
				      <label class="onoffswitch10-label" for="TrackingPixelStatus">
				        <span class="onoffswitch10-inner"></span>
				        <span class="onoffswitch10-switch"></span>
				      </label>
				    </div>
				</div>
				<?php }?>
				<?php 
				if($CampaignDetails->CampaignType=="DirectPost"){                           
				  $MaxTimeOutInput = "style='display:block;'";
				  $MaxPingTimeOutInput = "style='display:none;'";
				  $MaxPostTimeOutInput = "style='display:none;'";
				}else if($CampaignDetails->CampaignType=="PingPost"){
				  $MaxTimeOutInput = "style='display:none;'";
				  $MaxPingTimeOutInput = "style='display:block;'";
				  $MaxPostTimeOutInput = "style='display:block;'";
				}
				?>
				 <div class="col-md-2 form-group tadmPadd labFontt" id="MaxTimeOutInputEdit" <?php echo $MaxTimeOutInput; ?>>
				  <label for="exampleInputEmail1">Max Time Out:</label>
				    <input type="text" class="form-control txtPadd" placeholder="0" 
				    id="MaxTimeOutEdit" name="MaxTimeOut" value="<?php echo $VendorDetails->MaxTimeOut; ?>" onkeypress="HideErr('MaxTimeOutEditErr');">
				    <span id="MaxTimeOutEditErr"></span>
				</div>
				<div class="col-md-2 form-group tadmPadd labFontt" id="MaxPingTimeOutInputEdit" <?php echo $MaxPingTimeOutInput; ?>>
				  <label for="exampleInputEmail1">Max Ping Time Out:</label>
				    <input type="text" class="form-control txtPadd" placeholder="0" 
				    id="MaxPingTimeOutEdit" name="MaxPingTimeOut" value="<?php echo $VendorDetails->MaxPingTimeOut; ?>" onkeypress="HideErr('MaxPingTimeOutEditErr');">
				    <span id="MaxPingTimeOutEditErr"></span>
				</div>
				<div class="col-md-2 form-group tadmPadd labFontt" id="MaxPostTimeOutInputEdit" <?php echo $MaxPostTimeOutInput; ?>>
				  <label for="exampleInputEmail1">Max Post Time Out:</label>
				    <input type="text" class="form-control txtPadd" placeholder="0" 
				    id="MaxPostTimeOutEdit" name="MaxPostTimeOut" value="<?php echo $VendorDetails->MaxPostTimeOut; ?>" onkeypress="HideErr('MaxPostTimeOutEditErr');">
				    <span id="MaxPostTimeOutEditErr"></span>
				</div> 
			</div>
			<div class="row">
				<div class="col-md-2 form-group mb-0 labFontt">
				  <label for="exampleFormControlSelect1">Insert Token:</label>
				    <select class="form-control" id="insert_Token_type" name="insert_Token_type" onchange="updateTextBox(this)">
				      <option value="aid">aid</option>
				      <option value="SubID">SubID</option>
				      <option value="TestMode">TestMode</option>
				      <option value="TransactionID">TransactionID</option>
				      <option value="VendorLeadPrice">VendorLeadPrice</option>
				  </select>
				</div>
				<div class="col-md-5 form-group mb-0 labFontt">
				  <label for="exampleFormControlTextarea1">Tracking Pixel Code:</label>
				  <textarea class="form-control" id="TrackingPixelCode" name="TrackingPixelCode" rows="3"><?php echo $VendorDetails->TrackingPixelCode; ?></textarea>
				</div>
				<div class="col-md-5 form-group mb-0 labFontt">
				  <label for="exampleFormControlTextarea1">Vendor Notes:</label>
				  <textarea class="form-control" id="VendorNotes" name="VendorNotes" rows="3"><?php echo $VendorDetails->VendorNotes; ?></textarea>
				</div>
			</div>
	        <div class="row">
	        </div>
		    <div class="row btnFlo">
		      	<div class="col-md-12 form-group mb-0 labFontt">
		       		<button type="button" class="btn btn-success btn-fw noBoradi mr-2" onclick="SaveVendorDetails();">Save</button>
		        	<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
		      	</div>
		    </div>
		</form>
		<?php		
	}	
}



