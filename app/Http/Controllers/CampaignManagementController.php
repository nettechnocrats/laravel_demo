<?php
namespace App\Http\Controllers;
use Route;
use Mail;
use Auth, Hash;
use Validator;
use DB;
use Session;
use Excel;
use PDF;
use Response;
use Redirect;
use Illuminate\Http\Request;
use App\Http\Controllers\Pagination;
use App\Http\Models\CampaignManagementModel;
use App\Http\Models\MasterConfigModel;

class CampaignManagementController extends Controller
{
	public function __construct(Request $request)
	{		
		$this->Pagination 		= new Pagination();
		$this->MasterConfigModel= new MasterConfigModel();
		$this->CMModel 			= new CampaignManagementModel();
	}

	public function CampaignManagementView(Request $request)
	{
		if(!$request->session()->has('user_id'))
		{
		    return redirect()->action('LoginController@Login');
		}
		$Data['CampaignType'] 	= $this->CMModel->GetCampaignType();
		$Data['Menu']   		= "SuperAdmin";
        $Data['Title']  		= "Admin | CampaignManagement";
		return view('CampaignManagementView')->with($Data);
	}   
	public function GetCampaignList(Request $request)
	{
		$Data = $request->all();

        $UserRole 	= Session::get('user_role');
		$CompanyID 	= Session::get('user_companyID');
		$UserID 	= Session::get('user_id');

        $NumOfRecords = $Data['numofrecords'];
        $page = $Data['page'];
        $cur_page = $page;

        $Limitpage = $page - 1;
        $Start = $Limitpage * $NumOfRecords;

        $CampaignName 		= $Data['CampaignName'];
        $CampaignType 		= $Data['CampaignType'];
        $MaintenanceMode	= $Data['MaintenanceMode'];
        $Search 			= array('CampaignName' => $CampaignName,
				            'CampaignType' 	=> $CampaignType,
				            'MaintenanceMode' 	=> $MaintenanceMode,
				            'Start' 	=> $Start,
				            'NumOfRecords' 	=> $NumOfRecords
				            );
        $GetCampaignList 	= $this->CMModel->GetCampaignList($Search);       
        $AllCampaignList 	= $GetCampaignList['Res'];
        $CampaignCount 		= $GetCampaignList['Count'];        

        $Pagination = $this->Pagination->Links($NumOfRecords, $CampaignCount, $page);
        
      ?>
		<div class="row mt-3">
          	<div class="table-responsive">
		  		<table class="table table-bordered tableFont txtL nTabl">
                    <thead class="thead-dark">
						<tr>
							<th scope="col" style="padding: 0.684rem; font-size: 0.8rem;">Campaign Name</th>
							<th scope="col" style="padding: 0.684rem; font-size: 0.8rem;">Type</th>
							<th scope="col" style="padding: 0.684rem; font-size: 0.8rem;">Has Tiers?</th>
							<th scope="col" style="padding: 0.684rem; font-size: 0.8rem;">Maintenance Mode</th>
							<th scope="col" style="padding: 0.684rem; font-size: 0.8rem;">Show In Dashboard</th>
							<th scope="col" style="padding: 0.684rem; font-size: 0.8rem;">Action</th>
						</tr>
                    </thead>
			    	<tbody>
				    <?php 
				     	if(!empty($AllCampaignList ))
				     	{
				     		$i = $Start + 1;
				     		foreach($AllCampaignList as $cl)
				     		{
							     ?>				       
						        <tr>
						           	<td><?php echo $cl->CampaignName." (CID:".$cl->CampaignID.")"; ?></td>
						           	<td><?php echo $cl->CampaignType; ?></td>
						           	<td>
						           	<?php
						           	if($cl->HasTiers=='1'){
						           		echo "Yes";
						           	} else {
						           		echo "No";
						           	}
						           	?>
						            </td>						           	
						           	<td style="text-align: center;" id="MaintenanceModeTR_<?php echo $cl->CampaignID; ?>">
						           		<?php 
						           		if($cl->MaintenanceMode=='0')
						           		{
						           			?>
						           			<label class="switch swiCamplist"  >
		                                    	<input type="checkbox" id="MaintenanceMode_<?php echo $cl->CampaignID; ?>"		                                    	
						           				onclick="ChangeMaintenanceMode('<?php echo $cl->CampaignID; ?>');">
		                                    	<span class="slider round"></span>
		                                    	<span class="absolute-no">NO</span>
		                                    </label>
						           			<?php
						           		}
						           		else
						           		{
						           			?>
						           			<label class="switch swiCamplist">
		                                    	<input type="checkbox" checked id="MaintenanceMode_<?php echo $cl->CampaignID; ?>"
		                                    	onclick="ChangeMaintenanceMode('<?php echo $cl->CampaignID; ?>');">
		                                    	<span class="slider round"></span>
		                                    	<span class="absolute-no">NO</span>
		                                    </label>						           			
						           			<?php
						           		}
						           		?>	                                    
	                                </td>							           	
						           	<td style="text-align: center;" id="ShowInDashboardTR_<?php echo $cl->CampaignID; ?>">
						           	<?php 
						           		if($cl->ShowInDashboard=='1')
						           		{
						           			?>
						           			<div class="custom-control custom-checkbox noCush">
	                                        	<input type="checkbox" checked class="custom-control-input noCush-input" 
	                                        	id="ShowInDashboard_<?php echo $cl->CampaignID; ?>" name="example2"
	                                        	onclick="ChangeShowInDashboardMode('<?php echo $cl->CampaignID; ?>');">
	                                        	<label class="custom-control-label noCush-label mb-3" for="ShowInDashboard_<?php echo $cl->CampaignID; ?>"></label>
	                                      	</div>
						           			<?php
						           		}
						           		else
						           		{
						           			?>
						           			<div class="custom-control custom-checkbox noCush">
	                                        	<input type="checkbox" class="custom-control-input noCush-input" 
	                                        	id="ShowInDashboard_<?php echo $cl->CampaignID; ?>" name="example2"
	                                        	onclick="ChangeShowInDashboardMode('<?php echo $cl->CampaignID; ?>');">
	                                        	<label class="custom-control-label noCush-label mb-3" for="ShowInDashboard_<?php echo $cl->CampaignID; ?>"></label>
	                                      	</div>						           			
						           			<?php
						           		}
						           		?>	                                    
                                      		                                    
	                                </td>
						           	<td  style="text-align: center;">
						              	<a href="<?php echo route('admin.CampaignEditView',array(base64_encode($cl->CampaignID))) ?>">
			                              <i class="fas fa-edit fa-lg editPadd" title="edit"></i>
			                            </a>
						           	</td>
						        </tr>
			     				<?php 
			     				$i++;
			     			}
			     		} 
			     		else 
			     		{
			     			?>
					     	<tr >
					           <td colspan="7" style="color: #c94542;text-align:center;">Record Not Found.</td>
					           </td>
					        </tr>
			     			<?php 
			     		} 
			     	?>
			     	</tbody>
				  </table>
				</div>
			</div>
		<?php if(!empty($AllCampaignList ))
		{ ?>
			<div class="row mt-3">			
				<div class="col-md-6 col-sm-6 col-12">
					<div class="dataTables_length mCenter" id="dataTable_length">
						<label>Records/Pages
							<select id="numofrecords" onchange="SearchUserList();" name="dataTable_length" aria-controls="dataTable" class="custom-select custom-select-sm form-control form-control-sm">
							<option value="10" <?php if($NumOfRecords=='10'){ echo 'selected'; } ?> >10</option>
							<option value="20" <?php if($NumOfRecords=='20'){ echo 'selected'; } ?> >20</option>
							<option value="50" <?php if($NumOfRecords=='50'){ echo 'selected'; } ?> >50</option>
							<option value="100" <?php if($NumOfRecords=='100'){ echo 'selected'; } ?> >100</option>
							<option value="150" <?php if($NumOfRecords=='150'){ echo 'selected'; } ?> >150</option>
							<option value="200" <?php if($NumOfRecords=='200'){ echo 'selected'; } ?> >200</option>
							</select>
						</label>
					</div>
				</div>
				<div class="col-md-6 col-sm-6 col-12 grid-margin stretch-card">
					<div class="card noBg border-0">
					    <nav>
					      <?php echo $Pagination; ?>
					    </nav>
					</div>
				</div>
			</div>
      	<?php
      	}
      	else
      	{
      		?>
      		<input type="hidden" name="numofrecords" id="numofrecords" value='10'>
      		<?php
      	}
       	exit();
	}
	public function CampaignAddView(Request $request)
	{
		if(!$request->session()->has('user_id'))
		{
		    return redirect()->action('LoginController@Login');
		}
		$UserRole 	= Session::get('user_role');
		$CompanyID 	= Session::get('user_companyID');
		$UserID 	= Session::get('user_id');
		$Data['CampaignType'] 	= $this->CMModel->GetCampaignType();
		$Data['Menu']   = "SuperAdmin";
        $Data['Title']  = "Admin | CampaignManagement";
        
		return view('CampaignAddView')->with($Data);
	}
	public function AddCampaignDetails(Request $request)
	{
		$Data 		= $request->all();
		$UserRole 	= Session::get('user_role');
		$CompanyID 	= Session::get('user_companyID');
		$UserID 	= Session::get('user_id');
			
		$Details['CampaignName'] 	= $Data['CampaignName'];
		$Details['TablePrefix'] 	= strtolower(str_replace(' ','_', $Data['CampaignName']));
        $Details['CampaignType'] 	= $Data['CampaignType'];
        $Details['MaintenanceMode']	= '0';
        $Details['HasTiers']		= '0';
        $Details['ShowInDashboard']	= '0';
        $Details['HasSpecs']		= '0';
        $Specs						= $Data['Specs'];
        if(isset($Data['MaintenanceMode']))
        {
        	$Details['MaintenanceMode'] = '1';        	
        }
        if(isset($Data['HasTiers']) )
        {
        	$Details['HasTiers'] = '1';        	
        }
        if(isset($Data['ShowInDashboard']))
        {
        	$Details['ShowInDashboard'] = '1';        	
        }
        if(isset($Data['HasSpecs']))
        {
        	$Details['HasSpecs'] = '1';        	
        }
		$AddCampaignDetails = $this->CMModel->AddCampaignDetails($Details,$Specs);
		if($AddCampaignDetails)
		{
			Session::flash('message', 'Campaign Details Has been Added.');
            Session::flash('alert-class', 'alert-success');
            return Redirect::route('admin.CampaignManagementView');
		}
		else
		{
			Session::flash('message', 'OOPS! Something Wrong Please Try Again.');
            Session::flash('alert-class', 'alert-danger');
            return Redirect::route('admin.CampaignAddView');
		}
	}
	public function CampaignEditView(Request $request, $CampaignID)
	{
		if(!$request->session()->has('user_id'))
		{
		    return redirect()->action('LoginController@Login');
		}
		$CampaignID  = base64_decode($CampaignID);
		$Data['CampaignDetails'] = $this->CMModel->GetCampaignDetail($CampaignID);
		$Data['CampaignSpecs'] 	= $this->CMModel->GetCampaignSpecs($CampaignID);
		$Data['CampaignType'] 	= $this->CMModel->GetCampaignType();
		$Data['Menu']   = "SuperAdmin";
        $Data['Title']  = "Admin | CampaignManagement";
        
		return view('CampaignEditView')->with($Data);
	}

	public function SaveCampaignDetails(Request $request)
	{
		$Data = $request->all();		
		$CampaignID   			    = $Data['CampaignID'];
		$Details['CampaignName'] 	= $Data['CampaignName'];
        $Details['CampaignType'] 	= $Data['CampaignType'];
        $Details['MaintenanceMode']	= '0';
        $Details['HasTiers']		= '0';
        $Details['ShowInDashboard']	= '0';
        $Details['HasSpecs']		= '0';
        $Specs						= $Data['Specs'];
        if(isset($Data['MaintenanceMode']))
        {
        	$Details['MaintenanceMode'] = '1';        	
        }
        if(isset($Data['HasTiers']) )
        {
        	$Details['HasTiers'] = '1';        	
        }
        if(isset($Data['ShowInDashboard']))
        {
        	$Details['ShowInDashboard'] = '1';        	
        }
        if(isset($Data['HasSpecs']))
        {
        	$Details['HasSpecs'] = '1';        	
        }
		$SaveCampaignDetails = $this->CMModel->SaveCampaignDetails($CampaignID,$Details,$Specs);
		if($SaveCampaignDetails)
		{
			Session::flash('message', 'Campaign Details Has been Updated.');
            Session::flash('alert-class', 'alert-success');
            return Redirect::route('admin.CampaignEditView', base64_encode($CampaignID));
		}
		else
		{
			Session::flash('message', 'OOPS! Something Wrong Please Try Again.');
            Session::flash('alert-class', 'alert-danger');
            return Redirect::route('admin.CampaignEditView', base64_encode($CampaignID));
		}
	}

	public function ChangeMaintenanceMode(Request $request)
	{
		$Data 		= $request->all();
		$Response 	=array();
		$CampaignID   			    	= $Data['CID'];
		$Details['MaintenanceMode'] 	= $Data['MaintenanceMode'];
		$ChangeMaintenanceMode = $this->CMModel->ChangeMaintenanceMode($CampaignID,$Details);
		if($ChangeMaintenanceMode)
		{
			$Massage = '<div class="alert alert-success alert-dismissible fade show" role="alert">
						  Maintenance Mode Has Been Chnaged.
						  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
						    <span aria-hidden="true">&times;</span>
						  </button>
						</div>';
			$Response['Status'] = 1;
			$Response['Msg'] = $Massage;
		}
		else
		{
			$Massage = '<div class="alert alert-danger alert-dismissible fade show" role="alert">
						  Something Wrong Please Try Again.
						  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
						    <span aria-hidden="true">&times;</span>
						  </button>
						</div>';
			$Response['Status'] = 0;
			$Response['Msg'] = 1;
		}
		echo json_encode($Response);
		exit();
	}

	public function ChangeShowInDashboardMode(Request $request)
	{
		$Data 		= $request->all();
		$Response 	=array();
		$CampaignID   			    	= $Data['CID'];
		$Details['ShowInDashboard'] 	= $Data['ShowInDashboard'];
		$ChangeShowInDashboardMode = $this->CMModel->ChangeShowInDashboardMode($CampaignID,$Details);
		if($ChangeShowInDashboardMode)
		{
			$Massage = '<div class="alert alert-success alert-dismissible fade show" role="alert">
						  Show In Dashboard Status Has Been Chnaged.
						  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
						    <span aria-hidden="true">&times;</span>
						  </button>
						</div>';
			$Response['Status'] = 1;
			$Response['Msg'] = $Massage;
		}
		else
		{
			$Massage = '<div class="alert alert-danger alert-dismissible fade show" role="alert">
						  Something Wrong Please Try Again.
						  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
						    <span aria-hidden="true">&times;</span>
						  </button>
						</div>';
			$Response['Status'] = 0;
			$Response['Msg'] = 1;
		}
		echo json_encode($Response);
		exit();
	}
	

}
