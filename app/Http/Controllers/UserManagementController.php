<?php
namespace App\Http\Controllers;
use Route;
use Mail;
use Auth, Hash;
use Validator;
use DB;
use Session;
use Excel;
use PDF;
use Response;
use Redirect;
use Illuminate\Http\Request;
use App\Http\Controllers\Pagination;
use App\Http\Models\UserManagementModel;
use App\Http\Models\MasterConfigModel;

class UserManagementController extends Controller
{
	public function __construct(Request $request)
	{		
		$this->Pagination 		= new Pagination();
		$this->MasterConfigModel= new MasterConfigModel();
		$this->UMModel 			= new UserManagementModel();
	}

	public function UserManagementView(Request $request)
	{
		if(!$request->session()->has('user_id'))
		{
		    return redirect()->action('LoginController@Login');
		}
		$Data['UserRole'] = $this->UMModel->GetUserRoles();
		$Data['CompanyList'] = $this->UMModel->CompanyList();
		$Data['Menu']   = "Account";
        $Data['Title']  = "Admin | UserManagement";
		return view('UserManagementView')->with($Data);
	}   
	public function GetUserList(Request $request)
	{
		$Data = $request->all();

        $UserRole 	= Session::get('user_role');
		$CompanyID 	= Session::get('user_companyID');
		$UserID 	= Session::get('user_id');

        $NumOfRecords = $Data['numofrecords'];
        $page = $Data['page'];
        $cur_page = $page;

        $Limitpage = $page - 1;
        $Start = $Limitpage * $NumOfRecords;

        $CompanyID 	= $Data['CompanyID'];
        $FirstName 	= $Data['FirstName'];
        $LastName 	= $Data['LastName'];
        $Email 		= $Data['Email'];
        $Phone 		= $Data['Phone'];
        $UserStatus = $Data['UserStatus'];
        $UserRole 	= $Data['UserRole'];
        $Search 	= array('CompanyID' => $CompanyID,
				            'FirstName' => $FirstName,
				            'LastName' 	=> $LastName,
				            'Email' 	=> $Email,
				            'Phone' 	=> $Phone,
				            'UserStatus'=> $UserStatus,
				            'UserRole' 	=> $UserRole,
				            'CompanyID' => $CompanyID,
				            'Start' 	=> $Start,
				            'NumOfRecords' 	=> $NumOfRecords
				            );
        $GetUserList 	= $this->UMModel->GetUserList($Search);
       
        $AllUserList 	= $GetUserList['Res'];
        $UserCount 		= $GetUserList['Count'];        

        $Pagination = $this->Pagination->Links($NumOfRecords, $UserCount, $page);
      ?>
		<div class="row mt-3">
          	<div class="table-responsive">
		  		<table class="table table-bordered tableFont txtL nTabl">
                    <thead class="thead-dark">
						<tr>
							<th scope="col" style="padding: 0.684rem; font-size: 0.8rem;">Company</th>
							<th scope="col" style="padding: 0.684rem; font-size: 0.8rem;">First Name</th>
							<th scope="col" style="padding: 0.684rem; font-size: 0.8rem;">Last Name</th>
							<th scope="col" style="padding: 0.684rem; font-size: 0.8rem;">Email</th>
							<th scope="col" style="padding: 0.684rem; font-size: 0.8rem;">Telephone</th>
							<th scope="col" style="padding: 0.684rem; font-size: 0.8rem;">User Role</th>
							<th scope="col" style="padding: 0.684rem; font-size: 0.8rem;">Status</th>
							<th scope="col" style="padding: 0.684rem; font-size: 0.8rem;">Action</th>
						</tr>
                    </thead>
			    	<tbody>
				    <?php 
				     	if(!empty($AllUserList ))
				     	{
				     		$i = $Start + 1;
				     		foreach($AllUserList as $ul)
				     		{
							     ?>				       
						        <tr>
						           	<td><?php echo $ul->CompanyName; ?></td>
						           	<td><?php echo $ul->UserFirstName; ?></td>
						           	<td><?php echo $ul->UserLastName; ?></td>
						           	<td><?php echo $ul->UserEmail.' (UID:'.$ul->UserID.')'; ?></td>
						           	<td><?php echo $ul->UserTel; ?></td>							           	
						           	<td>
						           		<?php 

						           		if ($ul->UserRole == '2') {
		                                    echo "Admin";
		                                } else if ($ul->UserRole == '3') {
		                                    echo "User";
		                                }
		                                else if ($ul->UserRole == '1') {
		                                    echo "SuperAdmin";
		                                }
						           		?>

						           	</td>						           	
						           	<td  style="text-align: center;">
						           		<?php 

						           		if ($ul->UserStatus == '1') 
						           		{
		                                   ?>
		                                   	<div class="custom-control custom-checkbox noCush greenCh">
				                              <input type="checkbox" checked="true" 
				                              	class="custom-control-input noCush-input green-input" 
				                              	id="customCheck<?php echo $ul->UserID; ?>" 
				                              	onclick="ChangeUserStatus('<?php echo $ul->UserID; ?>',0);">
				                              <label class="custom-control-label noCush-label green-label mb-3" 
				                              	for="customCheck<?php echo $ul->UserID; ?>"  
				                              	></label>
				                            </div> 
		                                   <?php
		                                } 
		                                else if ($ul->UserStatus == '0') 
		                                {
		                                    ?>
		                                   	<div class="custom-control custom-checkbox noCush">
				                              <input type="checkbox" 
				                              class="custom-control-input noCush-input" 
				                              id="customCheck<?php echo $ul->UserID; ?>" 
				                              onclick="ChangeUserStatus('<?php echo $ul->UserID; ?>',1);" >
				                              <label class="custom-control-label noCush-label mb-3" 
				                              	for="customCheck<?php echo $ul->UserID; ?>" 
				                              	></label>
				                            </div> 
		                                   <?php
		                                }
						           		?>


						              	
						           	</td>
						           	<td  style="text-align: center;">
						              	<!-- <a href="<?php echo route('admin.UserEditView',array(base64_encode($ul->UserID))) ?>">
			                              <i class="fas fa-edit fa-lg editPadd" title="edit"></i>
			                            </a> -->
			                            
			                            <a href="javascript:void(0);" onclick="GetUserDetails('<?php echo $ul->UserID; ?>');">
			                              <i class="fas fa-edit fa-lg editPadd" title="edit"></i>
			                            </a>
						           	</td>
						        </tr>
			     				<?php 
			     				$i++;
			     			}
			     		} 
			     		else 
			     		{
			     			?>
					     	<tr >
					           <td colspan="7" style="color: #c94542;text-align:center;">Record Not Found.</td>
					           </td>
					        </tr>
			     			<?php 
			     		} 
			     	?>
			     	</tbody>
				  </table>
				</div>
			</div>
		<?php if(!empty($AllUserList ))
		{ ?>
			<div class="row mt-3">			
				<div class="col-md-6 col-sm-6 col-12">
					<div class="dataTables_length mCenter" id="dataTable_length">
						<label>Records/Pages
							<select id="numofrecords" onchange="SearchUserList();" name="dataTable_length" aria-controls="dataTable" class="custom-select custom-select-sm form-control form-control-sm">
							<option value="10" <?php if($NumOfRecords=='10'){ echo 'selected'; } ?> >10</option>
							<option value="20" <?php if($NumOfRecords=='20'){ echo 'selected'; } ?> >20</option>
							<option value="50" <?php if($NumOfRecords=='50'){ echo 'selected'; } ?> >50</option>
							<option value="100" <?php if($NumOfRecords=='100'){ echo 'selected'; } ?> >100</option>
							<option value="150" <?php if($NumOfRecords=='150'){ echo 'selected'; } ?> >150</option>
							<option value="200" <?php if($NumOfRecords=='200'){ echo 'selected'; } ?> >200</option>
							</select>
						</label>
					</div>
				</div>
				<div class="col-md-6 col-sm-6 col-12 grid-margin stretch-card">
					<div class="card noBg border-0">
					    <nav>
					      <?php echo $Pagination; ?>
					    </nav>
					</div>
				</div>
			</div>
      	<?php
      	}
      	else
      	{
      		?>
      		<input type="hidden" name="numofrecords" id="numofrecords" value='10'>
      		<?php
      	}
       	exit();
	}

	public function UserEditView(Request $request, $UserID)
	{
		if(!$request->session()->has('user_id'))
		{
		    return redirect()->action('LoginController@Login');
		}
		$UserID  = base64_decode($UserID);
		$Data['CompanyList'] = $this->UMModel->CompanyList();
		$Data['UserDetails'] = $this->UMModel->UserDetails($UserID);
		$Data['UserRole'] = $this->UMModel->GetUserRoles();
		$Data['Menu']   = "Account";
        $Data['Title']  = "Admin | UserManagement";
        
		return view('UserEditView')->with($Data);
	}

	public function SaveUserDetails(Request $request)
	{
		$Data = $request->all();
		
		$UserID   					= $Data['UserID'];
		$CompanyID   				= $Data['CompanyID'];
		$Details['CompanyID']   	= $Data['CompanyID'];
		$Details['UserRole']   		= $Data['UserRole'];
		$Details['UserFirstName']   = $Data['UserFirstName'];
		$Details['UserLastName']   	= $Data['UserLastName'];
		$Details['UserEmail']   	= $Data['UserEmail'];
		$Details['UserTel']   		= $Data['UserTel'];
		$Details['UserSkype']   	= $Data['UserSkype'];
		$Details['UserNotes']   	= $Data['UserNotes'];
		$Details['UserStatus']   	= '0';			
		if(isset($Data['UserStatus']) && $Data['UserStatus']=='on')
		{
			$Details['UserStatus']   	= '1';			
		}

		$Details['ShowAPISpecs']   	= '0';			
		if(isset($Data['ShowAPISpecs']) && $Data['ShowAPISpecs']=='on')
		{
			$Details['ShowAPISpecs']   	= '1';			
		}

		$Details['ShowAffLinks']   	= '0';			
		if(isset($Data['ShowAffLinks']) && $Data['ShowAffLinks']=='on')
		{
			$Details['ShowAffLinks']   	= '1';			
		}

		if(isset($Data['ChangePassword']) && $Data['ChangePassword']=='on')
		{
			$Details['UserPassword']   	= md5($Data['Password']);
		}
		if(isset($Data['SendCredentials']) && $Data['SendCredentials']=='on')
		{
			$this->SendCredentialsMail($Data);
		}
		$SaveUserDetails = $this->UMModel->SaveUserDetails($UserID,$Details);
		if($SaveUserDetails)
		{
			Session::flash('message', 'User Details Has been Updated.');
            Session::flash('alert-class', 'alert-success');
            if(isset($Data['From']) && $Data['From']=='Company')
            {
            	return Redirect::route('admin.CompanyEditView',base64_encode($CompanyID));      
            }
            else
            {
            	return Redirect::route('admin.UserManagementView');            	
            }
		}
		else
		{
			Session::flash('message', 'OOPS! Something Wrong Please Try Again.');
            Session::flash('alert-class', 'alert-danger');
            if(isset($Data['From']) && $Data['From']=='Company')
            {
            	return Redirect::route('admin.CompanyEditView',base64_encode($CompanyID));      
            }
            else
            {
            	return Redirect::route('admin.UserManagementView');            	
            }
		}
	}

	public function SendCredentialsMail($Data)
	{
		$MarketingEmail 	= $this->MasterConfigModel->getMarketingEmail();        
        $SiteName 			= $this->MasterConfigModel->getSiteName();
        $DashboardDomain 	= $this->MasterConfigModel->getDashboardDomain();
        if(isset($Data['Password']) && $Data['Password'] != '')   
        {
            $Password = $Data['Password'];
        }
        else 
        {
        	$Password = "******";
        }
        	
        $to     = $Data['UserEmail'];
	    $subject = "Welcome To ".$SiteName;
	    $headers = "From: <".$MarketingEmail.">";
	    $message='';
	    $message.="Please find the Updated Details!\n\n";

	    $message.="To login to your dashboard, please visit https://".$DashboardDomain."\n\n";

	    $message.="Username: ".$Data['UserEmail']."\n";
	    $message.="Password: ".$Password."\n\n";
	    $message.="NOTE: For security purposes, you will be asked to change your password upon your initial login.\n\n";

	    $message.="Once you have logged in, you will be able to view and edit various details within your account.\n\n";

	    $message.="PUBLISHERS: we will need to review your details and approve your account. Please reply to this email and tell us if you will be:\n";
	    $message.=" a) sending traffic to our websites (i.e. as an affiliate),\n";
	    $message.=" b) generating leads on your own websites and integrating with our API, or\n";
	    $message.=" c) have data that you want to upload to our List Management service.\n\n";

	    $message.="ADVERTISERS: please send us your Agreement and integration specs so we can setup your buyer accounts.\n\n";

	    $message.="If you have any questions, simply email me - I'm here to help!\n\n";

	    $message.="Kind regards,\n";
	    $message.= $SiteName." Team\n";
	    mail($to,$subject,$message,$headers);
	}

	public function UserAddView(Request $request)
	{
		if(!$request->session()->has('user_id'))
		{
		    return redirect()->action('LoginController@Login');
		}
		$UserRole 	= Session::get('user_role');
		$CompanyID 	= Session::get('user_companyID');
		$UserID 	= Session::get('user_id');
		$Data['CompanyDetails']   = $this->UMModel->GetCompanyDetails($CompanyID);
		$Data['Menu']   = "Account";
        $Data['Title']  = "Admin | UserManagement";
        
		return view('UserAddView')->with($Data);
	}
	public function AddUserDetails(Request $request)
	{
		$Data 		= $request->all();

		$Details['UserRole']   		= $Data['UserRole'];
		$CompanyID   				= $Data['CompanyID'];
		$Details['CompanyID']   	= $Data['CompanyID'];
		if(isset($Data['UserStatus']) && $Data['UserStatus']=='on')
		{
			$Details['UserStatus']   	= '1';			
		}
		$Details['UserFirstName']   = $Data['UserFirstName'];
		$Details['UserLastName']	= '';
		if($Data['UserLastName']!='')
		{
			$Details['UserLastName']   	= $Data['UserLastName'];			
		}
		$Details['UserEmail']   	= $Data['UserEmail'];
		$Details['UserPassword']   	= md5($Data['Password']);
		$Details['UserTel']   		= '';
		if($Data['UserTel']!='')
		{
			$Details['UserTel']   	= $Data['UserTel'];			
		}
		$Details['UserSkype']	= '';
		if($Data['UserSkype']!='')
		{
			$Details['UserSkype']   	= $Data['UserSkype'];			
		}
		
		$SaveUserDetails = $this->UMModel->AddUserDetails($Details);
		if($SaveUserDetails)
		{
			if(isset($Data['SendCredentials']) && $Data['SendCredentials']=='on')
			{
				$this->SendNewCredentialsMail($Data);
			}
			Session::flash('message', 'User Details Has been Added.');
            Session::flash('alert-class', 'alert-success');
            if(isset($Data['From']) && $Data['From']=='Company')
            {
            	return Redirect::route('admin.CompanyEditView',base64_encode($CompanyID));      
            }
            else
            {
            	return Redirect::route('admin.UserManagementView');            	
            }
		}
		else
		{
			Session::flash('message', 'OOPS! Something Wrong Please Try Again.');
            Session::flash('alert-class', 'alert-danger');
            if(isset($Data['From']) && $Data['From']=='Company')
            {
            	return Redirect::route('admin.CompanyEditView',base64_encode($CompanyID));      
            }
            else
            {
            	return Redirect::route('admin.UserManagementView');            	
            }
		}
	}

	public function GeneratePassword(Request $request) 
	{
        $Numeric = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9');
        $SpecialChar = array('!', '@', '#', '$', '%', '^', '&', '*');
        $string = $this->GenerateRandomString(6);
        $Num = $Numeric[array_rand($Numeric)];
        $Char = $SpecialChar[array_rand($SpecialChar)];
        echo $string . $Char . $Num;
        exit();
    }

    public function GenerateRandomString($length = 6) 
    {
        $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function SendNewCredentialsMail($Details)
    {
		$MarketingEmail = $this->MasterConfigModel->getMarketingEmail();     
        $SiteName 		= $this->MasterConfigModel->getSiteName();
        $DashboardDomain= $this->MasterConfigModel->getDashboardDomain();
        
        $to     = $Details['UserEmail'];
        $subject = "Welcome To ".$SiteName;
        $headers = "From: <".$MarketingEmail.">";
        $message='';
        $message.="Thank you for signing up with us!\n\n";

        $message.="To login to your dashboard, please visit https://".$DashboardDomain."\n\n";

        $message.="Username: ".$Details['UserEmail']."\n";
        $message.="Password: ".$Details['Password']."\n\n";
        $message.="NOTE: For security purposes, you will be asked to change your password upon your initial login.\n\n";

        $message.="Once you have logged in, you will be able to view and edit various details within your account.\n\n";

        $message.="PUBLISHERS: we will need to review your details and approve your account. Please reply to this email and tell us if you will be:\n";
        $message.=" a) sending traffic to our websites (i.e. as an affiliate),\n";
        $message.=" b) generating leads on your own websites and integrating with our API, or\n";
        $message.=" c) have data that you want to upload to our List Management service.\n\n";

        $message.="ADVERTISERS: please send us your Agreement and integration specs so we can setup your buyer accounts.\n\n";

        $message.="If you have any questions, simply email me - I'm here to help!\n\n";

        $message.="Kind regards,\n";
        $message.= $SiteName." Team\n";
        mail($to,$subject,$message,$headers);
    }

    public function ChangeUserStatus(Request $request)
	{
		$Data 		= $request->all();
		$Response 	=array();
		$UserID   	 	   = $Data['UID'];
		$Details['UserStatus'] = $Data['Status'];
		$ChangeUserStatus = $this->UMModel->SaveUserDetails($UserID,$Details);
		if($ChangeUserStatus)
		{
			$Massage = '<div class="alert alert-success alert-dismissible fade show" role="alert">
						  User Status Has Been Changed.
						  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
						    <span aria-hidden="true">&times;</span>
						  </button>
						</div>';
			$Response['Status'] = 1;
			$Response['Msg'] = $Massage;
		}
		else
		{
			$Massage = '<div class="alert alert-danger alert-dismissible fade show" role="alert">
						  Something Wrong Please Try Again.
						  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
						    <span aria-hidden="true">&times;</span>
						  </button>
						</div>';
			$Response['Status'] = 0;
			$Response['Msg'] = 1;
		}
		echo json_encode($Response);
		exit();
	}
	/////////////////////
	public function GetUserDetails(Request $request)
	{
		$Data 		= $request->all();
		$UserID  	= $Data['UserID'];
		$CompanyList 	= $this->UMModel->CompanyList();
		$UserRole 		= $this->UMModel->GetUserRoles();
		$UserDetails 	= $this->UMModel->UserDetails($UserID);
		?>
		<form action="<?php echo route('admin.SaveUserDetails'); ?>" name="UserSaveForm" id="UserSaveForm" method="POST">
	        <div class="row">
	        <input type="hidden" name="UserID" id="UserID" value="<?php echo $UserDetails->UserID; ?>">
	        <input type="hidden" name="_token" id="_token" value="<?php echo  csrf_token(); ?>">
	          <div class="col-md-1 form-group tadmPadd labFontt">
	              <label for="exampleInputEmail1">UserID:</label>
	              <input disabled type="text" onkeypress="HideErr('UserIDErr');" value="<?=$UserDetails->UserID;?>"  class="form-control txtPadd" placeholder="UserID">
	              <span id="UserIDErr" class="errordisp"></span>
	           </div>
	           <div class="col-md-3 form-group tadmPadd labFontt">
	            <label for="exampleInputEmail1">Company Name:</label>
	              <select class="form-control fonT" id="CompanyID" name="CompanyID" onchange="HideErr('CompanyIDErr');">
	                <option value="">Select Company</option>
	               	<?php foreach($CompanyList as $cl){ ?>
	                <option value="<?=$cl->CompanyID;?>"
	                  <?php if($cl->CompanyID==$UserDetails->CompanyID){ echo "selected";} ?>><?=$cl->CompanyName;?> (CID:<?=$cl->CompanyID;?></option>
	                <?php }?>
	              </select>
	              <span id="CompanyIDErr" class="errordisp"></span>
	           </div>                               
	            <div class="col-md-2 form-group tadmPadd labFontt">
	            <label for="exampleInputEmail1">User Role:</label>
	              <select class="form-control fonT" id="UserRoleEdit" name="UserRole" onchange="HideErr('UserRoleEditErr');">
	                <option value="">Select User Role</option>
	                  <?php foreach($UserRole as $Key=>$Value){ ?>
	                  <option value="<?=$Key;?>"
	                  <?php if($Key==$UserDetails->UserRole){ echo "selected";} ?>><?=$Value;?></option>
	                  <?php } ?>
	              </select>
	              <span id="UserRoleEditErr" class="errordisp"></span>
	           </div>
	            <div class="col-md-3 form-group tadmPadd labFontt">
	              <label for="exampleInputEmail1">First Name:</label>
	              <input type="text" onkeypress="HideErr('UserFirstNameErr');" value="<?=$UserDetails->UserFirstName;?>" id="UserFirstName" name="UserFirstName" class="form-control txtPadd" placeholder="First Name">
	              <span id="UserFirstNameErr" class="errordisp"></span>
	           </div>
	           <div class="col-md-3 form-group tadmPadd labFontt">
	              <label for="exampleInputEmail1">Last Name:</label>
	              <input type="text" onkeypress="HideErr('UserLastNameErr');" value="<?=$UserDetails->UserLastName;?>" id="UserLastName" name="UserLastName" class="form-control txtPadd" placeholder="Last Name">
	              <span id="UserLastNameErr" class="errordisp"></span>
	           </div>                               
	        </div>
	        <div class="row">	          
	          <div class="col-md-3 form-group tadmPadd labFontt">
	              <label for="exampleInputEmail1">Email:</label>
	              <input type="email" onkeypress="HideErr('UserEmailErr');" value="<?=$UserDetails->UserEmail;?>" id="UserEmail" name="UserEmail" class="form-control camH" id="exampleInputEmail1" placeholder="Email">
	              <span id="UserEmailErr" class="errordisp"></span>
	           </div>
	            <div class="col-md-3 form-group tadmPadd labFontt">
	              <label for="exampleInputEmail1">Telephone:</label>
	              <input type="text" onkeypress="HideErr('UserTelErr');" value="<?=$UserDetails->UserTel;?>" id="UserTel" name="UserTel" class="form-control txtPadd" placeholder="Telephone">
	              <span id="UserTelErr" class="errordisp"></span>
	           </div>
	           <div class="col-md-3 form-group tadmPadd labFontt">
	              <label for="exampleInputEmail1">Skype ID:</label>
	              <input type="text" onkeypress="HideErr('UserSkypeErr');" value="<?=$UserDetails->UserSkype;?>" id="UserSkype" name="UserSkype" class="form-control txtPadd" placeholder="Skype ID">
	              <span id="UserSkypeErr" class="errordisp"></span>
	           </div>
	           <?php if($UserDetails->UserStatus=='1'){?>
	            <div class="col-md-3 form-group tadmPadd labFontt">
	              <label for="exampleInputEmail1">User Status:</label>
	              <div class="onoffswitch4">
	                <input type="checkbox" name="UserStatus" class="onoffswitch4-checkbox" id="UserStatusEdit" 
	                  checked>
	                <label class="onoffswitch4-label" for="UserStatusEdit">
	                    <span class="onoffswitch4-inner"></span>
	                    <span class="onoffswitch4-switch"></span>
	                </label>
	              </div>
	            </div> 
	            <?php } else { ?>
	             <div class="col-md-3 form-group tadmPadd labFontt">
	              <label for="exampleInputEmail1">User Status:</label>
	              <div class="onoffswitch4">
	                <input type="checkbox" name="UserStatus" class="onoffswitch4-checkbox" id="UserStatusEdit" 
	                  >
	                <label class="onoffswitch4-label" for="UserStatusEdit">
	                    <span class="onoffswitch4-inner"></span>
	                    <span class="onoffswitch4-switch"></span>
	                </label>
	              </div>
	            </div> 
	        </div>
	        <div class="row" >
	            <?php } ?>
	            <?php if($UserDetails->ShowAPISpecs=='1'){?>                                
	            <div class="col-md-3 form-group tadmPadd labFontt">
	              <label for="exampleInputEmail1">Show Specs:</label>
	              <div class="onoffswitch6">
	                <input type="checkbox" class="onoffswitch6-checkbox" 
	                id="ShowAPISpecs" name="ShowAPISpecs" checked>
	                <label class="onoffswitch6-label" for="ShowAPISpecs">
	                    <span class="onoffswitch6-inner"></span>
	                    <span class="onoffswitch6-switch"></span>
	                </label>
	              </div>
	            </div>
	            <?php } else { ?>
	            <div class="col-md-3 form-group tadmPadd labFontt">
	              <label for="exampleInputEmail1">Show Specs:</label>
	              <div class="onoffswitch6">
	                <input type="checkbox" class="onoffswitch6-checkbox" 
	                id="ShowAPISpecs" name="ShowAPISpecs" >
	                <label class="onoffswitch6-label" for="ShowAPISpecs">
	                    <span class="onoffswitch6-inner"></span>
	                    <span class="onoffswitch6-switch"></span>
	                </label>
	              </div>
	            </div>
	            <?php } ?>
	            <?php if($UserDetails->ShowAffLinks=='1'){?>                                 
	            <div class="col-md-3 form-group tadmPadd labFontt">
	              <label for="exampleInputEmail1">Show Affiliate Links:</label>
	              <div class="onoffswitch7">
	                <input type="checkbox" class="onoffswitch7-checkbox" 
	                id="ShowAffLinks" name="ShowAffLinks" checked>
	                <label class="onoffswitch7-label" for="ShowAffLinks">
	                    <span class="onoffswitch7-inner"></span>
	                    <span class="onoffswitch7-switch"></span>
	                </label>
	              </div>
	            </div>
	            <?php } else { ?>
	            <div class="col-md-3 form-group tadmPadd labFontt">
	              <label for="exampleInputEmail1">Show Affiliate Links:</label>
	              <div class="onoffswitch7">
	                <input type="checkbox" class="onoffswitch7-checkbox"
	                id="ShowAffLinks" name="ShowAffLinks">
	                <label class="onoffswitch7-label" for="ShowAffLinks">
	                    <span class="onoffswitch7-inner"></span>
	                    <span class="onoffswitch7-switch"></span>
	                </label>
	              </div>
	            </div>
	            <?php } ?> 
	            <div class="col-md-6 form-group tadmPadd labFonsize">
		          <div class="form-check form-check-flat form-check-primary adComp adComp-primary mt-4">
		             <label class="form-check-label adComp-label labLh">
		             <input type="checkbox" class="form-check-input " 
		             	id="ChangePassword" name="ChangePassword" onclick="ChangePasswordShowHide();">
		             Do You Want To Change Password? 
		             <i class="input-helper"></i>
		             </label>
		          </div>
	            </div>                                                            
	        </div>
	        <div class="row" >           
	           <div class="col-md-4 form-group tadmPadd labFontt" id="PasswordDiv" style="display:none;">
	              <label for="exampleInputPassword1">Password:</label>
	              <input type="text" onkeypress="HideErr('PasswordErr');" class="form-control camH" id="Password" name="Password" placeholder="Password">
	              <span id="PasswordErr" class="errordisp"></span>
	           </div>
	           <div class="col-md-4 form-group tadmPadd labFontt" id="ConfirmPasswordDiv" style="display:none;">
	              <label for="exampleInputPassword1">Confirm Password:</label>
	              <input type="text" onkeypress="HideErr('ConfirmPasswordErr');" class="form-control camH" id="ConfirmPassword" name="ConfirmPassword" placeholder="Confirm Password">
	              <span id="ConfirmPasswordErr" class="errordisp"></span>
	           </div>
	           <div class="col-md-4 form-group tadmPadd labFontt" id="GeneratePasswordDiv" style="display:none;">
	              <label class="gP" for="exampleInputPassword1">Generate Password:</label>
	              <button type="button" onclick="GeneratePassword();" class="btn btn-success fonT noBoradi cbtnPadd sercFontt gpBg">Generate Password</button>
	           </div>
	        </div>
	         <div class="row" >
	         	<div class="col-md-6 form-group mb-0 labFontt">
	              <label for="exampleFormControlTextarea1">User Notes:</label>
	              <textarea class="form-control" id="UserNotes" name="UserNotes" rows="3"><?=$UserDetails->UserNotes;?></textarea>
	            </div>
	           	<div class="col-md-6 form-group tadmPadd labFonsize">
	              <div class="form-check form-check-flat form-check-primary adComp adComp-primary mt-4">
	                 <label class="form-check-label adComp-label labLh">
	                 <input type="checkbox" class="form-check-input " id="SendCredentials" name="SendCredentials">
	                  Would you like to email credentials to the user?
	                 <i class="input-helper"></i>
	                 </label>
	              </div>
	           	</div>            
	        </div>
	        <div class="row btnFlo">
	           <button type="button" id="UserSaveBtn" class="btn btn-success btn-fw noBoradi mr-2" onclick="SaveUserDetails();" >Update</button>
	           <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
	        </div>
	     </form>
		<?php
	}
}
