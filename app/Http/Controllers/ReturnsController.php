<?php
namespace App\Http\Controllers;
use Route;
use Mail;
use Auth, Hash;
use Validator;
use DB;
use Session;
use Excel;
use PDF;
use Response;
use Redirect;
use Illuminate\Http\Request;
use App\Http\Controllers\Pagination;
use App\Http\Models\ReturnsModel;
use App\Http\Models\MasterConfigModel;

class ReturnsController extends Controller
{
	public function __construct(Request $request)
	{		
		$this->Pagination 		= new Pagination();
		$this->MasterConfigModel= new MasterConfigModel();
		$this->RModel 			= new ReturnsModel();
	}

	public function ImportReturnsView(Request $request)
	{
		if(!$request->session()->has('user_id'))
		{
		    return redirect()->action('LoginController@Login');
		}
		$UserRole 	= Session::get('user_role');
		$CompanyID 	= Session::get('user_companyID');
		$UserID 	= Session::get('user_id');

		$StartDate  = date("Y-m-01");
        $EndDate    = date("Y-m-t");

		$CampaignDropDown 	= $this->MasterConfigModel->getCampaignList();
		$Data =array('CampaignDropDown' => $CampaignDropDown,
                     'StartDate'		=> $StartDate,
                     'EndDate' 			=> $EndDate);
		$Data['Menu'] 	= "Returns";
		$Data['Title'] 	= "Admin | Import Returns";
		return view('ImportReturnView')->with($Data);
	}
	public function GetReturnCodeList(Request $request)
	{
		$Data = $request->all();
		$CampaignID = $Data['CampaignID'];
		$GetReturnCodeList = $this->RModel->GetReturnCodeList($CampaignID);
		if(!empty($GetReturnCodeList))
		{
			foreach($GetReturnCodeList as $rcs)
			{
			?>
			<tr>
				<td><?php echo $rcs->ReturnCode;?></td>
				<td><?php echo $rcs->ReturnReason;?></td>
			</tr>
			<?php
			}
		}
		else
		{
			?>
			<tr>
				<td colspan="2">Return Code Not Available?></td>
			</tr>
			<?php
		}
	}

	public function ImportReturnsProcess(Request $request)
	{
		$userRole = Session::get('user_role');
        $companyID = Session::get('user_companyID');
        $userID = Session::get('user_id');

		$data = $request->all();
		$CampaignID = $data['Campaign'];
		$BuyerCompanyID = "";
		$startDate = $data['start'];
		$endDate = $data['end'];

		$file = $_FILES['file']['tmp_name'];
        $Records = fopen($file,"r");
        while(($data = fgetcsv($Records, 1000, ",")) !== FALSE)
        {
			$RecordsArr[] = $data;
		}
		if(empty($RecordsArr) || count($RecordsArr)==1)
		{
			Session::flash('message', 'CSV File Is Blank');
	        Session::flash('alert-class', 'alert-danger');
	        return Redirect::route('ImportReturnsPage');
		}
		else
		{
			$FileName = $_FILES['file']['name'];
			$FileDetails=array(
				"CampaignID"=> $CampaignID,
				"BuyerCompanyID"=>$BuyerCompanyID,
				"StartDate"=>$startDate." 00:00:00",
				"EndDate"=>$endDate." 23:59:59",
				"FileName"=>$FileName,
			);
			$FileNo = $this->RModel->SaveFileName($FileDetails);
			$ResponseArray=array();


			for($i=1;$i<count($RecordsArr); $i++)
			{
				$LeadID = $RecordsArr[$i][0];
				$LeadEmail = $RecordsArr[$i][1];
				$ReturnCode = $RecordsArr[$i][2];
				$ReturnMessage = $RecordsArr[$i][3];
				$Datas=array('CampaignID'=>$CampaignID,
							'LeadID'=>$LeadID,
							'BuyerCompanyID'=>$BuyerCompanyID,
							'LeadEmail'=>$LeadEmail,
							'ReturnCode'=>$ReturnCode,
							'ReturnMessage'=>$ReturnMessage
							);

				if($ReturnCode=="" || $ReturnCode <= 0)
				{
					$this->SaveUploadedDetails($Datas,$FileNo,'Rejected','ReturnCode Is Required or Not Valid');
				} 
				else if($userRole=="1" && $BuyerCompanyID=="")
				{
					$this->SaveUploadedDetails($Datas,$FileNo,'Rejected','BuyerCompany Is Required');
				}
				else
				{
					if($LeadID!="" && $LeadEmail!="")
					{
						$LeadData = $this->RModel->GetLeadDataWithLeadIDLeadEmail($CampaignID,$LeadID,$LeadEmail,$startDate,$endDate,$BuyerCompanyID);
						if(empty($LeadData))
						{
							$this->SaveUploadedDetails($Datas,$FileNo,'Rejected','Returned Lead Not Found');
						}
					}
					else if($LeadID!="" && $LeadEmail=="")
					{
						$LeadData = $this->RModel->GetLeadDataWithLeadID($CampaignID, $LeadID,$startDate,$endDate,$BuyerCompanyID);
						if(empty($LeadData))
						{
							$this->SaveUploadedDetails($Datas,$FileNo,'Rejected','Returned Lead Not Found');
						}
					}
					else if($LeadID=="" && $LeadEmail!="")
					{
						$LeadData = $this->RModel->GetLeadDataWithLeadEmail($CampaignID,$LeadEmail,$startDate,$endDate,$BuyerCompanyID);
						if(empty($LeadData))
						{
							$this->SaveUploadedDetails($Datas,$FileNo,'Rejected','Returned Lead Not Found');
						}
					}
					if(!empty($LeadData))
					{	
						$CompanyID = $LeadData->CompanyID;
						$VendorID = $LeadData->VendorID;
						$VendorLeadPrice = $LeadData->VendorLeadPrice;
						$BuyerID = $LeadData->BuyerID;
						$BuyerLeadPrice = $LeadData->BuyerLeadPrice;
						$VendorRefPayout = $LeadData->VendorRefPayout;
						$BuyerRefPayout = $LeadData->BuyerRefPayout;
						$LeadId = $LeadData->LeadID;
						$Details=array('CampaignID'=>$CampaignID,
										'CompanyID'=>$CompanyID,
										'LeadID'=>$LeadId,
										'VendorID'=>$VendorID,
										'VendorLeadPrice'=>$VendorLeadPrice,
										'BuyerID'=>$BuyerID,
										'BuyerLeadPrice'=>$BuyerLeadPrice,
										'VendorRefPayout'=>$VendorRefPayout,
										'BuyerRefPayout'=>$BuyerRefPayout,
										'ReturnCode'=>$ReturnCode,
										'ReturnMessage'=>$ReturnMessage);

						$Add = $this->RModel->AddReturnLeadData($Details);
						
						if($Add)
						{
							$this->SaveUploadedDetails($Datas,$FileNo,'Accepted','Accepted');
						}
						else
						{
							$this->SaveUploadedDetails($Datas,$FileNo,'Rejected','Rejected');
						}
					}
				}
			}
			Session::flash('message', 'Imported returns have been processed. Please download the Returns Report to see the status.');
	        Session::flash('alert-class', 'alert-success');
	        return Redirect::route('admin.ImportReturnsView');
		}
	}
	public function SaveUploadedDetails($Data,$FileNo,$Status,$Response)
	{
		$userRole = Session::get('user_role');
        $companyID = Session::get('user_companyID');
        $userID = Session::get('user_id');

		$UploadDetails=array('CampaignID'=>$Data['CampaignID'],
							'LeadID'=>$Data['LeadID'],
							'BuyerCompanyID'=>$Data['BuyerCompanyID'],
							'LeadEmail'=>$Data['LeadEmail'],
							'ReturnCode'=>$Data['ReturnCode'],
							'ReturnMessage'=>$Data['ReturnMessage'],
							'UserID'=>$userID,
							'FileNo'=>$FileNo,
							'Status'=>$Status,
							'Response'=>$Response,
							'RequestType'=>'WEB',
							);
		$this->RModel->AddReturnLeadDetails($UploadDetails);
	}

	public function UploadedFilesDetails(Request $request)
	{
        $Data   = $request->all();
		$userRole   = Session::get('user_role');
		$companyID  = Session::get('user_companyID');
		$userID     = Session::get('user_id');

		$NumOfRecords = $Data['numofrecords'];
        $page = $Data['page'];
        $cur_page = $page;

        $Limitpage = $page - 1;
        $Start = $Limitpage * $NumOfRecords;
      
        $Search= array('Start' 		=> $Start,
		            	'NumOfRecords' 	=> $NumOfRecords); 

		$UploadedFilesData = $this->RModel->UploadedFilesData($Search);
		
		$UploadedFiles = $UploadedFilesData['Res'];
		$UploadedCount = $UploadedFilesData['Count'];
		$Pagination = $this->Pagination->Links($NumOfRecords, $UploadedCount, $page);
		?>
		<div class="row">
	        <div class="table-responsive">
	           	<table class="table table-bordered tableFont txtL nTabl">
	              	<thead>
	                	<tr>
	                    	<th colspan="8" class="text-center pt-1 pb-1" style="font-size: 1rem;">Uploaded Returns Report</th>
	                	</tr>
	              	</thead>
	              	<thead class="thead-dark">
	                 	<tr>
		                    <th scope="col" style="padding: 0.684rem; font-size: 0.8rem; font-weight: 700;">Uploaded Date</th>
		                    <th scope="col" style="padding: 0.684rem; font-size: 0.8rem; font-weight: 700;">Campaign</th>
		                    <th scope="col" style="padding: 0.684rem; font-size: 0.8rem; font-weight: 700;">Company Name</th>
		                    <th scope="col" style="padding: 0.684rem; font-size: 0.8rem; font-weight: 700;">File Name</th>
		                    <th scope="col" style="padding: 0.684rem; font-size: 0.8rem; font-weight: 700;">Accepted</th>
		                    <th scope="col" style="padding: 0.684rem; font-size: 0.8rem; font-weight: 700;">Rejected</th>
		                    <th scope="col" style="padding: 0.684rem; font-size: 0.8rem; font-weight: 700;">Total Row</th>
		                    <th scope="col" style="padding: 0.684rem; font-size: 0.8rem; font-weight: 700;">Download</th>
	                 	</tr>
	              	</thead>
		           	<thead class="thead-dark">
						<?php
						if(!empty($UploadedFiles))
						{
							foreach($UploadedFiles as $ufd)
							{
								$FileNo 		= $ufd->FileNo;
								$TotalRow 		= $this->RModel->TotalRow($FileNo);
								$AcceptedRow 	= $this->RModel->AcceptedOrRejectedRow($FileNo,'Accepted');
								$RejectedRow 	= $this->RModel->AcceptedOrRejectedRow($FileNo,'Rejected');
								$CampaignInfo 	= $this->RModel->GetCampaignDetails($ufd->CampaignID);
								if ($ufd->BuyerCompanyID > 0) {
									$BuyerCompany = $this->RModel->GetBuyerCompanyDetails($ufd->BuyerCompanyID);
									$CompanyName = $BuyerCompany->CompanyName." (CID:".$BuyerCompany->CompanyID.")";
								} else {
									$CompanyName = " - ";
								}
								?>
								<tr>
									<td><?php echo $ufd->UploadedDate; ?></td>
									<td><?php echo $CampaignInfo->CampaignName; ?></td>
									<td><?php echo $CompanyName;?></td>
									<td><?php echo $ufd->FileName; ?></td>
									<td><?php echo $AcceptedRow; ?></td>
									<td><?php echo $RejectedRow; ?></td>
									<td><?php echo $TotalRow; ?></td>
									<td style="text-align: center;">
										<a href="<?php echo 'DownloadUploadedReturns/'.$ufd->FileNo; ?>" 
												class="btn btn-success noBoradi nPadd cbtnPadd sercFontt ndwSCSV">
											<i class="fa fa-cloud-download"></i> Download
										</a>
									</td>
								</tr>
								<?php
							}						
						}
						else
						{
							?>
							<tr>
				            	<td colspan="8" style="color: #c94542;text-align:center;">No Record Found.</td>
				            	<input type="hidden" name="numofrecords" id="numofrecords" value='10'>
				            </tr>
							<?php
						}
						?>
					</thead>
	           </table>
	        </div>        
        </div>        
		<?php
		if($UploadedCount>0)		            
		{
		?>
		<div class="row mt-3">			
			<div class="col-md-6 col-sm-6 col-12">
				<div class="dataTables_length mCenter" id="dataTable_length">
					<label>Records/Pages
						<select id="numofrecords" onchange="SearchUploadedFilesDetails();" name="dataTable_length" aria-controls="dataTable" class="custom-select custom-select-sm form-control form-control-sm">
						<option value="10" <?php if($NumOfRecords=='10'){ echo 'selected'; } ?> >10</option>
						<option value="20" <?php if($NumOfRecords=='20'){ echo 'selected'; } ?> >20</option>
						<option value="50" <?php if($NumOfRecords=='50'){ echo 'selected'; } ?> >50</option>
						<option value="100" <?php if($NumOfRecords=='100'){ echo 'selected'; } ?> >100</option>
						<option value="150" <?php if($NumOfRecords=='150'){ echo 'selected'; } ?> >150</option>
						<option value="200" <?php if($NumOfRecords=='200'){ echo 'selected'; } ?> >200</option>
						</select>
					</label>
				</div>
			</div>
			<div class="col-md-6 col-sm-6 col-12 grid-margin stretch-card">
				<div class="card noBg border-0">
				    <nav>
				      <?php echo $Pagination; ?>
				    </nav>
				</div>
			</div>
		</div>
		<?php
		}
		exit();

	}
	public function DownloadUploadedReturns(Request $request,Response $response, $FileNo)
	{
		if(!$request->session()->has('user_id'))
		{
		    return redirect()->action('LoginController@view');
		}
		$GetFileName = $this->RModel->GetFileName($FileNo);
        header("Content-type: text/csv");
	    header("Content-Disposition: attachment; filename=".$GetFileName."");
	    header("Pragma: no-cache");
	    header("Expires: 0");
	    $DownloadUploadedReturns = $this->RModel->DownloadUploadedReturns($FileNo);
	    $columns = array('LeadID', 'LeadEmail', 'ReturnCode', 'ReturnMessage', 'Status', 'Response');
		$file = fopen('php://output', 'w');
        fputcsv($file, $columns);

        foreach($DownloadUploadedReturns as $dur) {
            fputcsv($file, array($dur->LeadID, $dur->LeadEmail, $dur->ReturnCode, $dur->ReturnMessage,$dur->Status,$dur->Response));
        }
        fclose($file);
        exit();
	}
	///////////////////////////////////////////////////
	public function ExportReturnsView(Request $request)
	{
		if(!$request->session()->has('user_id'))
		{
		    return redirect()->action('LoginController@Login');
		}
		$UserRole 	= Session::get('user_role');
		$CompanyID 	= Session::get('user_companyID');
		$UserID 	= Session::get('user_id');

		$StartDate  = date("Y-m-01");
        $EndDate    = date("Y-m-t");

		$CampaignDropDown 	= $this->MasterConfigModel->getCampaignList();
		$Data =array('CampaignDropDown' => $CampaignDropDown,
                     'StartDate'		=> $StartDate,
                     'EndDate' 			=> $EndDate);
		$Data['Menu'] 	= "Returns";
		$Data['Title'] 	= "Admin | Export Returns";
		return view('ExportReturnView')->with($Data);
	}

	public function GetExportReport(Request $request)
	{
		$data   = $request->all();
		$userRole   = Session::get('user_role');
		$companyID  = Session::get('user_companyID');
		$userID     = Session::get('user_id');

		$numofrecords   = $data['numofrecords'];
		$page           = $data['page'];
		$cur_page       = $page;

		$Limitpage      = $page-1;
		$start          = $Limitpage * $numofrecords;

		$CampaignID = $data['CampaignID'];
		$startDate = date('Y-m-d',strtotime($data['StartDate'])).' 00:00:00';
		$endDate = date('Y-m-d',strtotime($data['EndDate'])).' 23:59:59';

		$VendorCompany ="";
		$Search=array("CampaignID"=>$CampaignID,
					  "startDate"=>$startDate,
					  "endDate"=>$endDate,
					  "start"=>$start,
					  "numofrecords"=>$numofrecords,
					  "VendorCompany"=> $VendorCompany
					);
		$GetExportDate 		= $this->RModel->GetExportDate($Search);

		$GetExportReport 	= $GetExportDate['Res'];
		$GetExportReportCount = $GetExportDate['Count'];
		$Pagination 		= $this->Pagination->Links($numofrecords, $GetExportReportCount, $page);
		?>
		 <div class="row">
            <div class="table-responsive">
               <table class="table table-bordered tableFont txtL nTabl">
                  <thead class="thead-dark">
                     <tr>
                        <th scope="col" style="padding: 0.684rem; font-size: 0.8rem; font-weight: 700;">Row</th>
                        <th scope="col" style="padding: 0.684rem; font-size: 0.8rem; font-weight: 700;">Lead Date-Time</th>
                        <th scope="col" style="padding: 0.684rem; font-size: 0.8rem; font-weight: 700;">Return Lead Date-Time</th>
                        <th scope="col" style="padding: 0.684rem; font-size: 0.8rem; font-weight: 700;">Company</th>
                        <th scope="col" style="padding: 0.684rem; font-size: 0.8rem; font-weight: 700;">Vendor ID</th>
                        <th scope="col" style="padding: 0.684rem; font-size: 0.8rem; font-weight: 700;">Campaign</th>
                        <th scope="col" style="padding: 0.684rem; font-size: 0.8rem; font-weight: 700;">Consumer Email</th>
                        <th scope="col" style="padding: 0.684rem; font-size: 0.8rem; font-weight: 700;">Return Code</th>
                        <th scope="col" style="padding: 0.684rem; font-size: 0.8rem; font-weight: 700;">Return Code Description</th>
                        <th scope="col" style="padding: 0.684rem; font-size: 0.8rem; font-weight: 700;">Return Message</th>
                     </tr>
                  </thead>
                  	<tbody>
					<?php
					if(!empty($GetExportReport))
					{
						$i = $start + 1;
						foreach($GetExportReport as $ger)
						{
							$ConsumerEmail = $this->RModel->GetConsumerEmail($CampaignID,$ger->LeadID);
							?>
								<tr>
									<td ><?=$i?></td>
									<td><?php echo $ger->LeadDateTime; ?></td>
									<td><?php echo $ger->DateTime; ?></td>
									<td><?php echo $ger->Company; ?></td>
									<td><?php echo $ger->Vendor; ?></td>
									<td><?php echo $ger->Campaign; ?></td>									
									<td><?php echo $ConsumerEmail ; ?></td>
									<td><?php echo $ger->ReturnCode; ?></td>
									<td><?php echo $ger->ReturnReason; ?></td>
									<td><?php echo $ger->ReturnMessage; ?></td>
								</tr>
							<?php
							$i++;
						}			
					}
					else
					{
						?>
						<tr>
			            	<td colspan="8" style="color: #c94542;text-align:center;">No Record Found.</td>
			            	<input type="hidden" name="numofrecords" id="numofrecords" value='10'>
			            </tr>
						<?php
					}
					?>
					<tbody>
	           </table>
	        </div>        
        </div> 
		<?php
		if($GetExportReportCount>0)
        {
        	?>
			<div class="row mt-3">			
				<div class="col-md-6 col-sm-6 col-12">
					<div class="dataTables_length mCenter" id="dataTable_length">
						<label>Records/Pages
							<select id="numofrecords" onchange="SearchExportReport();" name="dataTable_length" aria-controls="dataTable" class="custom-select custom-select-sm form-control form-control-sm">
							<option value="10" <?php if($numofrecords=='10'){ echo 'selected'; } ?> >10</option>
							<option value="20" <?php if($numofrecords=='20'){ echo 'selected'; } ?> >20</option>
							<option value="50" <?php if($numofrecords=='50'){ echo 'selected'; } ?> >50</option>
							<option value="100" <?php if($numofrecords=='100'){ echo 'selected'; } ?> >100</option>
							<option value="150" <?php if($numofrecords=='150'){ echo 'selected'; } ?> >150</option>
							<option value="200" <?php if($numofrecords=='200'){ echo 'selected'; } ?> >200</option>
							</select>
						</label>
					</div>
				</div>
				<div class="col-md-6 col-sm-6 col-12 grid-margin stretch-card">
					<div class="card noBg border-0">
					    <nav>
					      <?php echo $Pagination; ?>
					    </nav>
					</div>
				</div>
			</div>
			<?php
		}

	}
	public function IsExportCSVOrNot(Request $request)
	{
		$data   = $request->all();
		$userRole   = Session::get('user_role');
		$companyID  = Session::get('user_companyID');
		$userID     = Session::get('user_id');

		$CampaignID = $data['CampaignID'];
		$startDate = date('Y-m-d',strtotime($data['StartDate'])).' 00:00:00';
		$endDate = date('Y-m-d',strtotime($data['EndDate'])).' 23:59:59';

		$numofrecords   = $data['numofrecords'];
		$page           = $data['page'];
		$cur_page       = $page;

		$Limitpage      = $page-1;
		$start          = $Limitpage * $numofrecords;


		$VendorCompany = "";

		$Search=array("CampaignID"=>$CampaignID,
					  "startDate"=>$startDate,
					  "endDate"=>$endDate,
					  "start"=>$start,
					  "numofrecords"=>$numofrecords,
					  "VendorCompany"=> $VendorCompany
					);
		$GetExportDate 		= $this->RModel->GetExportDate($Search);

		$GetExportReportCount = $GetExportDate['Count'];
		if($GetExportReportCount!=0)
		{
			$VendorCompany = 0;
			$URL="ExportReturns/".$CampaignID.'/'.$startDate.'/'.$endDate.'/'.$VendorCompany;
			?>
			<a href="<?php echo $URL; ?>" target="_blank" class="btn btn-success noBoradi nPadd cbtnPadd sercFontt dwSCSV " >
                <i class="fa fa-cloud-download"></i> Download Return CSV
            </a>

            <a href="javascript:void(0);" 
            	onclick="EmailExportReturnCSV('<?php echo $CampaignID; ?>','<?php echo $startDate; ?>','<?php echo $endDate; ?>','<?php echo $VendorCompany; ?>');"
            	class="btn btn-success noBoradi nPadd cbtnPadd sercFontt dwSCSV " >
                <i class="fa fa-cloud-download"></i> Email Return
            </a>            
			<?php
		}
		exit();
	}
	public function ExportReturns(Request $request,$CampaignID,$startDate,$endDate,$VendorCID)
	{
		$data   = $request->all();
		$userRole   = Session::get('user_role');
		$companyID  = Session::get('user_companyID');
		$userID     = Session::get('user_id');

		$CampaignID = $CampaignID;
		$startDate = $startDate.' 00:00:00';
		$endDate = $endDate.' 23:59:59';

		$Search=array("CampaignID"=>$CampaignID,
					  "startDate"=>$startDate,
					  "endDate"=>$endDate,
					  "VendorCompany"=>$VendorCID
					);
		$GetExportReport = $this->RModel->GetExportReportAll($Search);
		if(!empty($GetExportReport))
		{
			$FileName="ExportReturn-".date("Y-m-d").'.csv';
			header("Content-type: text/csv");
		    header("Content-Disposition: attachment; filename=".$FileName."");
		    header("Pragma: no-cache");
		    header("Expires: 0");
		    $columns = array('Row','Lead DateTime','ReturnedLead DateTime', 'Company', 'VendorID', 'Campaign', 'ConsumerEmail', 'ReturnCode', 'ReturnReason','ReturnMessage');
			$file = fopen('php://output', 'w');
	        $content = fputcsv($file, $columns);
	        $i=1;
			foreach($GetExportReport as $ger)
			{
				$ConsumerEmail = $this->RModel->GetConsumerEmail($CampaignID,$ger->LeadID);
				$content.=fputcsv($file, array($i,$ger->LeadDateTime,$ger->DateTime,$ger->Company,$ger->Vendor, $ger->Campaign, $ConsumerEmail,$ger->ReturnCode,$ger->ReturnReason,$ger->ReturnMessage));
				$i++;
			}
			fclose($file);
			exit();
		}
	}
	
	public function ExportReturnsMail(Request $request,$CampaignID,$startDate,$endDate,$VendorCID)
	{
		$data   = $request->all();
		$userRole   = Session::get('user_role');
		$companyID  = Session::get('user_companyID');
		$userID     = Session::get('user_id');

		$CampaignID = $CampaignID;
		$startDate = $startDate.' 00:00:00';
		$endDate = $endDate.' 23:59:59';

		$Search=array("CampaignID"=>$CampaignID,
					  "startDate"=>$startDate,
					  "endDate"=>$endDate,
					  "VendorCompany"=>$VendorCID
					);
		$GetExportReport = $this->RModel->GetExportReportAll($Search);
		if(!empty($GetExportReport))
		{
			$FileName="ExportReturn-".date("Y-m-d").'.csv';
			if($userRole == 1) {
		    	$columns = array('Lead DateTime','ReturnedLead DateTime','Company' ,'Vendor', 'Campaign', 'Lead Price', 'ConsumerEmail', 'ReturnCode', 'ReturnReason','ReturnMessage');
		    } else {
		    	$columns = array('Lead DateTime','ReturnedLead DateTime', 'Company', 'VendorID', 'Campaign', 'ConsumerEmail', 'ReturnCode', 'ReturnReason','ReturnMessage');
			}
			$file = fopen('php://temp', 'w+');
	        $content = fputcsv($file, $columns);
			foreach($GetExportReport as $ger)
			{
				 $ConsumerEmail = $this->RModel->GetConsumerEmail($CampaignID,$ger->LeadID);
				 if($userRole == 1) {
				 	$content.=fputcsv($file, array($ger->LeadDateTime,$ger->DateTime,$ger->Company,$ger->Vendor, $ger->Campaign, '$'.number_format($ger->VendorLeadPrice,2) , $ConsumerEmail,$ger->ReturnCode,$ger->ReturnReason,$ger->ReturnMessage));
				 } else {
				 	$content.=fputcsv($file, array($ger->LeadDateTime,$ger->DateTime,$ger->Company,$ger->Vendor, $ger->Campaign, $ConsumerEmail,$ger->ReturnCode,$ger->ReturnReason,$ger->ReturnMessage));
				 }
			}
			rewind($file);

			$File = stream_get_contents($file);

			$MarketingEmail = $this->MCModel->getMarketingEmail();
        	$to      = "vishalmansuriya@gmail.com, ashish.pandey6565@gmail.com";
            //,ashish.pandey6565@gmail.com"; //,oobhsuhk@gmail.com";
			$subject = "Export Returns";    

			$multipartSep = '-----'.md5(time()).'-----';

		    // Arrays are much more readable
		    $headers = array(
		        "From: $MarketingEmail",
		        "Content-Type: multipart/mixed; boundary=$multipartSep"
		    );
		    //Message
		    $msg="Hello, \nPlease Find Export Report Data Below:";
		    // Make the attachment
		    $attachment = chunk_split(base64_encode($File)); 

		    // Make the body of the message
		    $body = "--$multipartSep\r\n"
		        . "Content-Type: text/plain; charset=ISO-8859-1; format=flowed\r\n"
		        . "Content-Transfer-Encoding: 7bit\r\n"
		        . "\r\n"
		        . "$msg\r\n"
		        . "--$multipartSep\r\n"
		        . "Content-Type: text/csv\r\n"
		        . "Content-Transfer-Encoding: base64\r\n"
		        . "Content-Disposition: attachment; filename=".$FileName.""
		        . "\r\n"
		        . "$attachment\r\n"
		        . "--$multipartSep--";

		   	mail($to, $subject, $body, implode("\r\n", $headers)); 

			if($VendorCID > 0) {
				$GetEmails = $this->RModel->GetVendorEmails($VendorCID);
				if(count($GetEmails) > 0) {
					foreach ($GetEmails as $key => $value) {
			        	$mail = trim($value->UserEmail);
			        }	
				}
			}

			Session::flash('message', 'Mail Sent Successfully.');
	        Session::flash('alert-class', 'alert-success');
	        return Redirect::route('ExportReturnsPage');
			
		}
		exit();
	}

	public function EmailExportReturnCSV(Request $request)
	{
		$data   	= $request->all();
		$userRole   = Session::get('user_role');
		$companyID  = Session::get('user_companyID');
		$userID     = Session::get('user_id');

		$CampaignID = $data['CampaignID'];
		$startDate = date('Y-m-d',strtotime($data['StartDate'])).' 00:00:00';
		$endDate = date('Y-m-d',strtotime($data['EndDate'])).' 23:59:59';

		$VendorCompany ="";
		$Search=array("CampaignID"=>$CampaignID,
					  "startDate"=>$startDate,
					  "endDate"=>$endDate,
					  "VendorCompany"=> $VendorCompany
					);
		$GetEmailExportReturnCSV 		= $this->RModel->GetEmailExportReturnCSV($Search);

		$content = '
		<html>
		<div class="col-sm-12">
			<p>Hello,</p>
			<p>Please Find Export Report Data Below: </p>
		</div>
		<div class="col-sm-12">
            <table class="table table-striped table-bordered" style="background-color: white;" border="1">
                <thead>
                    <tr class="table_thead">
                        <th>Row</th>
                        <th>Lead DateTime</th>
                        <th>ReturnedLead DateTime</th>
                        <th>Cpmpany</th>
                        <th>VendorID</th>
                        <th>Campaign</th>
                        <th>ConsumerEmail</th>
                		<th>ReturnCode</th>
                		<th>ReturnReason</th>
                		<th>ReturnMessage</th>
                	</tr>
            </thead>
            <thead>';
            $i =  1;
            foreach ($GetEmailExportReturnCSV as $key => $value) 
            {
            	$ConsumerEmail = $this->RModel->GetConsumerEmail($CampaignID,$value->LeadID);
            	$content.='<tr>';
				$content.='<td >'.$i.'</td>';
				$content.='<td>'.$value->LeadDateTime.'</td>';
				$content.='<td>'.$value->DateTime.'></td>';
				$content.='<td>'.$value->Company.'</td>';
				$content.='<td>'.$value->Vendor.'</td>';
				$content.='<td>'.$value->Campaign.'</td>';									
				$content.='<td>'.$ConsumerEmail.'</td>';
				$content.='<td>'.$value->ReturnCode.'</td>';
				$content.='<td>'.$value->ReturnReason.'</td>';
				$content.='<td>'.$value->ReturnMessage.'</td>';
				$content.='</tr>';
            	$i++;
            }
        $content.='</thead>
            </table>
        </div>
        </html>
        ';
        $MarketingEmail = $this->MasterConfigModel->getMarketingEmail();

        $to      = "honey@nettechnocrats.com";
		$subject = "Export Returns";
		
		
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers.= "Content-type:text/html;charset=UTF-8" . "\r\n";
		$headers.= "From: <".$MarketingEmail.">";
		
        $message = $content;
		$sent = mail($to,$subject,$message,$headers);
		if($sent)
		{
            ?>
            <div class="alert alert-success alert-dismissible fade show altExp" role="alert">
			  Returned Leads Are Send On Your Email.
			  <button type="button" class="close altExpBtn" data-dismiss="alert" aria-label="Close">
			    <span aria-hidden="true">&times;</span>
			  </button>
			</div>
            <?php
		}
		else
		{
            ?>
            <div class="alert alert-danger alert-dismissible fade show altExp" role="alert">
			  OOPS! Something Wrong Please Try Again.
			  <button type="button" class="close altExpBtn" data-dismiss="alert" aria-label="Close">
			    <span aria-hidden="true">&times;</span>
			  </button>
			</div>
            <?php
		}
		exit();
	}
}

