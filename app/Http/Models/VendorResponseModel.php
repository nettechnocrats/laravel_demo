<?php
namespace App\Http\Models;
use Illuminate\Database\Eloquent\Model;
use DB;
use Session;
class VendorResponseModel extends Model 
{
    public static function CampaignDropDown()
    {
        $getCampaign = DB::table('master_campaigns')
                    ->select('CampaignID','CampaignName')
                    ->where('CampaignID', '!=','0' )
                    ->where('MaintenanceMode', '!=','1' )
                    ->orderBy('CampaignName','ASC')
                    ->groupBy('CampaignID')
                    ->get();

        return $getCampaign;
    }
    public function GetCampaignDetails($CampaignID)
    {
        $CampaignInfo = DB::table('master_campaigns')
                    ->select('CampaignID','CampaignName','TablePrefix','CampaignType')
                    ->where('CampaignID', $CampaignID )
                    ->where('CampaignID','!=','0')
                    ->where('MaintenanceMode','!=','1')
                    ->first();
        return $CampaignInfo;
    }

    public function VendorResponse($CampaignID,$Action) 
    {
        $TablePrefix = $this->GetCampaignDetails($CampaignID);
        
        if($Action == 'DirectPost') {
            $Select = "DirectPostResponse";
        } else if($Action == "Ping") {
            $Select = "PingResponse";
        } else if($Action == "Post" || $Action == "PingPost") {
            $Select = "PostResponse";
        }
        
        $Response = DB::table('api_data_inbound')
                    ->select($Select)
                    ->where('CampaignID', $CampaignID)
                    ->where($Select,'!=',"")
                    ->groupBy($Select)
                    ->get();
        
        return $Response;
    }

    
    public function GetVendorResponseList($Searcher) 
    {
        
        $CampaignID     = $Searcher['CampaignID'];
        $VendorCompany  = $Searcher['VendorCompany'];
        $Vendor         = $Searcher['Vendor'];
        $Response       = $Searcher['Response'];
        $StartDate      = date('Y-m-d',strtotime($Searcher['StartDate'])).' 00:00:00';
        $EndDate        = date('Y-m-d',strtotime($Searcher['EndDate'])).' 23:59:59';
        $Sort           = $Searcher['Sort'];
        $Action         = $Searcher['Action'];
        $Start          = $Searcher['Start'];
        $NumOfRecords   = $Searcher['NumOfRecords'];
        

        $GetVendorResponse = DB::table('api_data_inbound as adi');
        
        if($Action == 'DirectPost') {
            $GetVendorResponse->select(
                'adi.DirectPostResponse as Response',
                'adi.DirectPostDate as Date',
                DB::raw('COUNT(adi.DirectPostResponse) as Count')
            ); 
        } else if($Action == "Ping"){
            $GetVendorResponse->select(
                'adi.PingResponse as Response', 
                'adi.PingDate as Date',
                DB::raw('COUNT(adi.PingResponse) as Count')
            ); 
        } else if($Action == "Post" || $Action == "PingPost") {
            $GetVendorResponse->select(
                'adi.PostResponse as Response', 
                'adi.PostDate as Date',
                DB::raw('COUNT(adi.PostResponse) as Count')
            ); 
        }
        $GetVendorResponse->addSelect(
            'mc.CampaignName',
            'adi.CheckPoint as Message',
            DB::raw('CONCAT(pvc.AdminLabel, " ", "(VID:", pvc.VendorID, ")") AS VendorName')
        );

        $GetVendorResponse->join('master_campaigns as mc', 'mc.CampaignID', '=','adi.CampaignID')
            ->join('partner_vendor_campaigns as pvc', 'pvc.VendorID', '=','adi.VendorID')
        ->where('adi.CampaignID', $CampaignID);

        if ($Vendor > 0) {
            $GetVendorResponse->where('adi.VendorID', $Vendor);
        }
        
        if($Action == 'DirectPost') { 
            if ($Response != "") {
                $GetVendorResponse->where('adi.DirectPostResponse', "LIKE" ,"%".$Response."%");
            }
            $GetVendorResponse->whereBetween('adi.DirectPostDate', array($StartDate, $EndDate))
                    ->groupBy('adi.CheckPoint')
                    ->groupBy('adi.VendorID')
                    ->orderBy('Count',$Sort);
                    
        } else if($Action == "Ping") {
            if ($Response != "") {
                $GetVendorResponse->where('adi.PingResponse', "LIKE" ,"%".$Response."%");
            }
            $GetVendorResponse->whereBetween('adi.PingDate', array($StartDate, $EndDate))
                    ->groupBy('adi.CheckPoint')
                    ->groupBy('adi.VendorID')
                    ->orderBy('Count',$Sort);
        } else if($Action == "Post" || $Action == "PingPost") { 
            if ($Response != "") {
                $GetVendorResponse->where('adi.PingResponse', "LIKE" ,"%".$Response."%");
            }
            $GetVendorResponse->whereBetween('adi.PostDate', array($StartDate, $EndDate))
                    ->groupBy('adi.CheckPoint')
                    ->groupBy('adi.VendorID')
                    ->orderBy('Count',$Sort);
        }
        
        $VendorResponse['Count'] = count($GetVendorResponse->get());
        $VendorResponse['Res'] = $GetVendorResponse
                        ->offset($Start)
                        ->limit($NumOfRecords)
                        ->get()
                        ->toArray();

        return $VendorResponse;
    }
}
