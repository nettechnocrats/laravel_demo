<?php

namespace App\Http\Models;
use Illuminate\Database\Eloquent\Model;
use DB;
use Session;

class MasterConfigModel extends Model {

	/*
	 * Get user and Company info
	 *
	 * @param
	 * @return array
	 */

    public function getParntnerBuyerIDs()
    {
        $BuyerIDsArray=array();
        $CompanyID = Session::get('user_companyID');
        $Buyer = DB::table('partner_buyer_campaigns')
                        ->select('BuyerID')
                        ->where('CompanyID', $CompanyID)
                        ->get()
                        ->toArray();
        if(!empty($Buyer))
        {
            foreach ($Buyer as $b) 
            {
                $BuyerIDsArray[] = $b->BuyerID;
            }
        }
        return $BuyerIDsArray;
    }
    public function HasVendor()
    {
        $CompanyID = Session::get('user_companyID');
        return $HasVendor = DB::table('partner_vendor_campaigns')
                ->select('*')
                ->where('CompanyID', $CompanyID)
                ->count();
    }
    public function getPartnertVendorCampaign()
    {
        $VendorCampaignArray=array();
        $CompanyID = Session::get('user_companyID');
        $VendorCampaign = DB::table('partner_vendor_campaigns')
                        ->select('CampaignID')
                        ->distinct('CampaignID')
                        ->where('CompanyID', $CompanyID)
                        ->get()
                        ->toArray();
        if(!empty($VendorCampaign))
        {
            foreach ($VendorCampaign as $vc) 
            {
                $VendorCampaignArray[] = $vc->CampaignID;
            }
        }
        return $VendorCampaignArray;
    }
    public function HasBuyer()
    {
        $CompanyID = Session::get('user_companyID');
        return $HasBuyer = DB::table('partner_buyer_campaigns')
                    ->select('*')
                    ->where('CompanyID', $CompanyID)
                    ->count();
    }
    public function getPartnertBuyerCampaign()
    {
        $BuyerCampaignArray=array();
        $CompanyID = Session::get('user_companyID');
        $BuyerCampaign = DB::table('partner_buyer_campaigns')
                        ->select('CampaignID')
                        ->distinct('CampaignID')
                        ->where('CompanyID', $CompanyID)
                        ->get()
                        ->toArray();
        if(!empty($BuyerCampaign))
        {
            foreach ($BuyerCampaign as $bc) 
            {
                $BuyerCampaignArray[] = $bc->CampaignID;
            }
        }
        return $BuyerCampaignArray;
    }
    public  function getCampaignList()
    {
        $Query = DB::table('master_campaigns')
                    ->select('*')
                    ->where('CampaignID', '!=','0' )
                    ->where('MaintenanceMode', '!=','1' );
        if($this->HasVendor()>0)
        {
            $VendorCampaign = $this->getPartnertVendorCampaign();
            $Query->whereIn('CampaignID',$VendorCampaign);
        }
        if($this->HasBuyer()>0)
        {
            $BuyerCampaign = $this->getPartnertBuyerCampaign();
            $Query->whereIn('CampaignID',$BuyerCampaign);
        }

        $CampaignDropDown  = $Query->orderBy('CampaignName','ASC')
                            ->get();
                   
        return $CampaignDropDown;
    }

    public  function getMarketingEmail()
    {
    	$MarketingEmailName = "MarketingEmail";
    	$MarketingEmailArray = DB::table('master_config')
                    ->select('ParameterValue')
                    ->where('ParameterName',$MarketingEmailName)
                    ->get()
                    ->first();
       $MarketingEmail= $MarketingEmailArray->ParameterValue;
       return $MarketingEmail;
    }


    public function getSiteName()
    {
    	 $SiteNameName = "SiteName";
    	 $SiteNameArray = DB::table('master_config')
                    ->select('ParameterValue')
                    ->where('ParameterName',$SiteNameName)
                    ->get()
                    ->first();
       
         $SiteName = $SiteNameArray->ParameterValue;
         return $SiteName;
    }

    public function getDashboardDomain()
    {

    	$DashboardDomainName = "DashboardDomain";
    	$Dashboard = DB::table('master_config')
    	->select('ParameterValue')
    	->where('ParameterName',$DashboardDomainName)
    	->get()
    	->first();

    	$Dashboard = $Dashboard->ParameterValue;
    	return $Dashboard;
    }
    public function getAdminEmail()
    {
    	$AdminEmailName = "AdminEmail";
    	$AdminEmailArray = DB::table('master_config')
                ->select('ParameterValue')
                ->where('ParameterName',$AdminEmailName)
                ->first();
		$AdminEmail = $AdminEmailArray->ParameterValue; 
		return $AdminEmail;
    }

    public function getDashboardAlert()
    {
    	$DashboardAlertName = "DashboardAlert";
		$DashboardAlertArray = DB::table('master_config')
                ->select('ParameterValue')
                ->where('ParameterName',$DashboardAlertName)
                ->first();
		$DashboardAlert = $DashboardAlertArray->ParameterValue;
		return $DashboardAlert; 
    }

    public function getReturnCode()
    {
        $ReturnCode = DB::table('master_return_codes')
                ->select('*')
                ->where('CampaignID','0')
                ->get()
                ->toArray();
        return $ReturnCode; 
    }
   

}
