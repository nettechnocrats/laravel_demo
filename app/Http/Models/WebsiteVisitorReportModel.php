<?php
namespace App\Http\Models;
use Illuminate\Database\Eloquent\Model;
use DB;
use Session;
class WebsiteVisitorReportModel extends Model 
{
	public function GetVendorList()
    {
        $CompanyID      = Session::get('user_companyID');
        $GetVendorList = DB::table('partner_vendor_campaigns as pvc')
                        ->select('pvc.VendorID','pvc.PartnerLabel')
                        ->join('partner_companies as pc','pvc.CompanyID','=','pc.CompanyID')    
                        ->where('pvc.CompanyID', $CompanyID ); 
        $Vendor = $GetVendorList->orderBy('pc.CompanyName','ASC')
                    ->orderBy('pvc.VendorID','ASC')
                    ->get();
        
        return $Vendor;
    }

    public function GetWebsiteVisitorReport($Search)
    {
        $Reports['Count'] = "";
        $Reports['Res']   = "";
        $URL            = $Search['URL'];
        $VendorID       = $Search['VendorID'];
        $SubID          = $Search['SubID'];
        $IPAddress      = $Search['IPAddress'];

        $CompanyID      = $Search['CompanyID'];
        
        $Start          = $Search['Start'];
        $NumOfRecords   = $Search['NumOfRecords'];

        $StartDate      = $Search['StartDate'];
        $EndDate        = $Search['EndDate'];

        $WebsiteVisitor = DB::table('website_visitor_analytics as wva')
                        ->whereBetween('wva.DateTime', array($StartDate, $EndDate));

    
        $GetVendorIDList=$this->GetVendorIDList($CompanyID);
        $WebsiteVisitor->whereIN('wva.VendorID', $GetVendorIDList);
        if($URL!="")
        {
            $WebsiteVisitor->where('wva.URL', 'like', "%$URL%");
        }
        if($VendorID!="")
        {
            $WebsiteVisitor->where('wva.VendorID',$VendorID);
        }
        if($SubID!="")
        {
            $WebsiteVisitor->where('wva.SubID',$SubID);
        }
        if($IPAddress!="")
        {
            $WebsiteVisitor->where('wva.IPAddress','like', "%$IPAddress%");
        }

        $Reports['Count'] = $WebsiteVisitor->count();
        
        $Reports['Res'] = $WebsiteVisitor
                        ->orderBy('Datetime','DESC')
                        ->offset($Start)
                        ->limit($NumOfRecords)
                        ->get()
                        ->toArray();    

        return $Reports;
    }

    public function GetVendorIDList($CompanyID)
    {
        $VendorIDList=array();
        $Data = DB::table('partner_vendor_campaigns')
        ->where('CompanyID',$CompanyID)
        ->select('VendorID')
        ->get();
        foreach($Data as $val)
        {
            $VendorIDList[]=$val->VendorID;
        }
        return $VendorIDList;
    }

    public function GetWebsiteVisitorSummary($Search)
    {
        $Reports['Count'] = "";
        $Reports['Res']   = "";
        $URL            = $Search['URL'];
        $VendorID       = $Search['VendorID'];
        $SubID          = $Search['SubID'];
        $IPAddress      = $Search['IPAddress'];

        $CompanyID      = $Search['CompanyID'];
        
        $StartDate      = $Search['StartDate'];
        $EndDate        = $Search['EndDate'];

        $WebsiteVisitor = DB::table('website_visitor_analytics as wva')
                        ->whereBetween('wva.DateTime', array($startDate, $endDate));
        $GetVendorIDList=$this->GetVendorIDList($companyID);
        if($userRole!='1')
        {
            $WebsiteVisitor->whereIN('wva.VendorID', $GetVendorIDList);
        }
        if($URL!="")
        {
            $WebsiteVisitor->where('wva.URL', 'like', '%'.$URL.'%');
        }
        if($VendorID!="")
        {
            $WebsiteVisitor->where('wva.VendorID',$VendorID);
        }
        if($SubID!="")
        {
            $WebsiteVisitor->where('wva.SubID',$SubID);
        }
        if($IPAddress!="")
        {
            $WebsiteVisitor->where('wva.IPAddress',$IPAddress);
        }
        $result = $WebsiteVisitor->get();
        return $result;
    }

    public function GetChartDataForWebsiteVisitor($Search)
    {
        $URL            = $Search['URL'];
        $VendorID       = $Search['VendorID'];
        $SubID          = $Search['SubID'];
        $IPAddress      = $Search['IPAddress'];

        $CompanyID      = $Search['CompanyID'];
        
        $StartDate      = $Search['StartDate'];
        $EndDate        = $Search['EndDate'];

        if($StartDate == $EndDate)
        {
            $GetVendorIDList=$this->GetVendorIDList($CompanyID);
            $WebsiteVisitor = DB::table('website_visitor_analytics as wva')
                            ->select(DB::raw('Date(DateTime) AS Date'),DB::raw('Hour(DateTime) AS Hour'),
                                    DB::raw('count(IPAddress) as Hits'),DB::raw('count(DISTINCT IPAddress) as IPAddress'))
                            ->whereBetween('wva.DateTime', array($StartDate, $EndDate));
            $WebsiteVisitor->whereIN('wva.VendorID', $GetVendorIDList);
            if($URL!="")
            {
                $WebsiteVisitor->where('wva.URL', 'like', '%'.$URL.'%');
            }
            if($VendorID!="")
            {
                $WebsiteVisitor->where('wva.VendorID',$VendorID);
            }
            if($SubID!="")
            {
                $WebsiteVisitor->where('wva.SubID',$SubID);
            }
            if($IPAddress!="")
            {
                $WebsiteVisitor->where('wva.IPAddress',$IPAddress);
            }
            $WebsiteVisitor->groupBy(DB::raw('Date(DateTime)'),DB::raw('Hour(DateTime)'));
            $result = $WebsiteVisitor->get();
        }
        else
        {
            $GetVendorIDList=$this->GetVendorIDList($CompanyID);
            $WebsiteVisitor = DB::table('website_visitor_analytics as wva')
                            ->select(DB::raw('Date(DateTime) AS Date'),
                                DB::raw('count(IPAddress) as Hits'),DB::raw('count(DISTINCT IPAddress) as IPAddress'))
                            ->whereBetween('wva.DateTime', array($StartDate, $EndDate));
            $WebsiteVisitor->whereIN('wva.VendorID', $GetVendorIDList);
            if($URL!="")
            {
                $WebsiteVisitor->where('wva.URL', 'like', '%'.$URL.'%');
            }
            if($VendorID!="")
            {
                $WebsiteVisitor->where('wva.VendorID',$VendorID);
            }
            if($SubID!="")
            {
                $WebsiteVisitor->where('wva.SubID',$SubID);
            }
            if($IPAddress!="")
            {
                $WebsiteVisitor->where('wva.IPAddress',$IPAddress);
            }
            $WebsiteVisitor->groupBy(DB::raw('Date(DateTime)'));
            $result = $WebsiteVisitor->get();
        }
        
        return $result;
    }

     public function GetWebsiteVisitorReportCount($Search)
    {
        $Reports['Count'] = "";
        $Reports['Res']   = "";
        $URL            = $Search['URL'];
        $VendorID       = $Search['VendorID'];
        $SubID          = $Search['SubID'];
        $IPAddress      = $Search['IPAddress'];

        $CompanyID      = $Search['CompanyID'];
        

        $StartDate      = $Search['StartDate'];
        $EndDate        = $Search['EndDate'];

        $WebsiteVisitor = DB::table('website_visitor_analytics as wva')
                        ->whereBetween('wva.DateTime', array($StartDate, $EndDate));

    
        $GetVendorIDList=$this->GetVendorIDList($CompanyID);
        $WebsiteVisitor->whereIN('wva.VendorID', $GetVendorIDList);
        if($URL!="")
        {
            $WebsiteVisitor->where('wva.URL', 'like', '%'.$URL.'%');
        }
        if($VendorID!="")
        {
            $WebsiteVisitor->where('wva.VendorID',$VendorID);
        }
        if($SubID!="")
        {
            $WebsiteVisitor->where('wva.SubID',$SubID);
        }
        if($IPAddress!="")
        {
            $WebsiteVisitor->where('wva.IPAddress',$IPAddress);
        }

        $Reports['Count'] = $WebsiteVisitor->count();           
        $Reports['Res'] = $WebsiteVisitor
                        ->orderBy('Datetime','DESC')
                        ->get()
                        ->toArray();  
        return $Reports;
    }
}
