<?php
namespace App\Http\Models;
use Illuminate\Database\Eloquent\Model;
use DB;
use Session;
class DashboardModel extends Model 
{
    public function __construct()
    {   
        $this->MasterConfigModel    = new MasterConfigModel();
    }
	public function GetUserInfo()
    {
    	$GetUserInfo = DB::table('partner_users as pu')
                        ->leftJoin('partner_companies as pc', 'pu.CompanyID', '=', 'pc.CompanyID')
                        ->where('pu.UserID', session('user_id'))
                        ->get()
                        ->first();

        return $GetUserInfo;
    }
/*
    public function DashboardVendorReport()
    {
    	$StartDate 		=  date('Y-m-d').' 00:00:00';
		$EndDate 		=  date('Y-m-d').' 23:59:59';
    	$DashboardVendorReport=array();
    	$CampaignsList = $this->MasterConfigModel->getCampaignList();
        $CompanyID = Session::get('user_companyID');
        $sample = array();   			
        if(!empty($CampaignsList))
        {     
        	foreach ($CampaignsList as $Campaigns) 
        	{
        	   	$CampaignID 	= $Campaigns->CampaignID; 
				$CampaignType 	= $Campaigns->CampaignType;
				$TablePrefix	= $Campaigns->TablePrefix;

				$api_data_inbound = "api_data_inbound as adi";
				$partner_vendor_campaigns = "partner_vendor_campaigns as pvc";
				$x_lead_data = $TablePrefix."_lead_data as ld";

				$GetProcessedLeads = DB::table(DB::raw($api_data_inbound))
												->select(DB::raw("COUNT(*) AS Total"),										
													DB::raw('SUM(ld.VendorLeadPrice) AS TotalVendorPrice'),
                                                    DB::raw('SUM(ld.BuyerLeadPrice) AS TotalBuyerPrice'))
												->leftjoin($x_lead_data, 'ld.LeadID', '=', 'adi.LeadID')
												->leftjoin($partner_vendor_campaigns, 'adi.VendorID', '=', 'pvc.VendorID')
												->where('adi.CampaignID','=',$CampaignID)
												->where('adi.LeadID', '!=', '0')
												->where('adi.TestMode','=','0');	
				$GetProcessedLeads->addSelect(DB::raw("SUM(IF(ld.LeadStatus='1',1,0)) AS Accepted"),
											  DB::raw("SUM(IF(ld.LeadStatus='2',1,0)) AS Returned"));
				if($Campaigns->CampaignType=="DirectPost"){
					$GetProcessedLeads->addSelect(DB::raw("SUM(IF(ld.IsRedirected='1',1,0)) AS Redirected"));
				}	
				if($CampaignType=="DirectPost")
				{
					$GetProcessedLeads->whereBetween('adi.DirectPostDate', array($StartDate, $EndDate));
				}
				else
				{
					$GetProcessedLeads->whereBetween('adi.PingDate', array($StartDate, $EndDate));
				}
				$GetProcessedLeads->where('ld.CompanyID', $CompanyID);

				$DashboardVendorReport[$Campaigns->CampaignName] = $GetProcessedLeads->first();                
        	}   	
        }
        return $DashboardVendorReport;

    }
     public function DashboardBuyerReport()
    {
        $StartDate      =  date('Y-m-d').' 00:00:00';
        $EndDate        =  date('Y-m-d').' 23:59:59';
        $DashboardBuyerReport=array();
        $CampaignsList = $this->MasterConfigModel->getCampaignList();
        
        $sample = array();              
        if(!empty($CampaignsList))
        {     
            $ParntnerBuyerIDs = $this->MasterConfigModel->getParntnerBuyerIDs();
            foreach ($CampaignsList as $Campaigns) 
            {
                $CampaignID     = $Campaigns->CampaignID; 
                $CampaignType   = $Campaigns->CampaignType;
                $TablePrefix    = $Campaigns->TablePrefix;

                $api_data_inbound = "api_data_inbound as adi";
                $partner_vendor_campaigns = "partner_vendor_campaigns as pvc";
                $x_lead_data = $TablePrefix."_lead_data as ld";

                $GetProcessedLeads = DB::table(DB::raw($x_lead_data))
                                                ->select(DB::raw("COUNT(*) AS Total"),                   
                                                    DB::raw('SUM(ld.BuyerLeadPrice) AS TotalBuyerPrice'))
                                                ->leftjoin($api_data_inbound, 'ld.LeadID', '=', 'adi.LeadID')
                                                ->where('adi.TestMode','=','0');    
                $GetProcessedLeads->addSelect(DB::raw("SUM(IF(ld.LeadStatus='1',1,0)) AS Accepted"),
                                              DB::raw("SUM(IF(ld.LeadStatus='2',1,0)) AS Returned"));
                if($Campaigns->CampaignType=="DirectPost"){
                    $GetProcessedLeads->addSelect(DB::raw("SUM(IF(ld.IsRedirected='1',1,0)) AS Redirected"));
                }   

                if($CampaignType=="DirectPost")
                {
                    $GetProcessedLeads->whereBetween('adi.DirectPostTime', array($StartDate, $EndDate));
                }
                else
                {
                    $GetProcessedLeads->whereBetween('adi.PingDate', array($StartDate, $EndDate));
                }
                $GetProcessedLeads->whereIn('ld.BuyerID', $ParntnerBuyerIDs);

                $DashboardBuyerReport[$Campaigns->CampaignName] = $GetProcessedLeads->first();                
            }       
        }
        return $DashboardBuyerReport;

    }*/

    public function DashboardVendorReport()
    {
        $StartDate      =  date('Y-m-d').' 00:00:00';
        $EndDate        =  date('Y-m-d').' 23:59:59';
        $DashboardVendorReport=array();
        $CampaignsList = $this->MasterConfigModel->getCampaignList();
        $CompanyID = Session::get('user_companyID');
        $sample = array();              
        if(!empty($CampaignsList))
        {     
            foreach ($CampaignsList as $Campaigns) 
            {
                $CampaignID     = $Campaigns->CampaignID; 
                $CampaignType   = $Campaigns->CampaignType;
                $TablePrefix    = $Campaigns->TablePrefix;

                $api_data_inbound = "api_data_inbound as adi";
                $partner_vendor_campaigns = "partner_vendor_campaigns as pvc";
                $x_lead_data = $TablePrefix."_lead_data as ld";

                $GetProcessedLeads = DB::table(DB::raw($api_data_inbound))
                                                ->select(DB::raw("COUNT(*) AS Total"),                                      
                                                    DB::raw('SUM(ld.VendorLeadPrice) AS TotalVendorPrice'),
                                                    DB::raw('SUM(ld.BuyerLeadPrice) AS TotalBuyerPrice'))
                                                ->leftjoin($x_lead_data, 'ld.LeadID', '=', 'adi.LeadID')
                                                ->leftjoin($partner_vendor_campaigns, 'adi.VendorID', '=', 'pvc.VendorID')
                                                ->where('adi.CampaignID','=',$CampaignID)
                                                ->where('adi.LeadID', '!=', '0')
                                                ->where('adi.TestMode','=','0');    
                $GetProcessedLeads->addSelect(DB::raw("SUM(IF(ld.LeadStatus='1',1,0)) AS Accepted"),
                                              DB::raw("SUM(IF(ld.LeadStatus='2',1,0)) AS Returned"));
                if($Campaigns->CampaignType=="DirectPost"){
                    $GetProcessedLeads->addSelect(DB::raw("SUM(IF(ld.IsRedirected='1',1,0)) AS Redirected"));
                }   
                if($CampaignType=="DirectPost")
                {
                    $GetProcessedLeads->whereBetween('adi.DirectPostDate', array($StartDate, $EndDate));
                }
                else
                {
                    $GetProcessedLeads->whereBetween('adi.PingDate', array($StartDate, $EndDate));
                }
                $GetProcessedLeads->where('ld.CompanyID', $CompanyID);

                $DashboardVendorReport[$Campaigns->CampaignID] = $GetProcessedLeads->first();                
            }       
        }
        return $DashboardVendorReport;

    }
     public function DashboardBuyerReport()
    {
        $StartDate      =  date('Y-m-d').' 00:00:00';
        $EndDate        =  date('Y-m-d').' 23:59:59';
        $DashboardBuyerReport=array();
        $CampaignsList = $this->MasterConfigModel->getCampaignList();
        
        $sample = array();              
        if(!empty($CampaignsList))
        {     
            $ParntnerBuyerIDs = $this->MasterConfigModel->getParntnerBuyerIDs();
            foreach ($CampaignsList as $Campaigns) 
            {
                //DB::enableQueryLog();
                /*$CampaignID     = $Campaigns->CampaignID; 
                $CampaignType   = $Campaigns->CampaignType;
                $TablePrefix    = $Campaigns->TablePrefix;

                $api_data_inbound = "api_data_inbound as adi";
                $partner_vendor_campaigns = "partner_vendor_campaigns as pvc";
                $x_lead_data = $TablePrefix."_lead_data as ld";

                $GetProcessedLeads = DB::table(DB::raw($x_lead_data))
                                                ->select(DB::raw("COUNT(*) AS Total"),                   
                                                    DB::raw('SUM(ld.BuyerLeadPrice) AS TotalBuyerPrice'))
                                                ->leftjoin($api_data_inbound, 'ld.LeadID', '=', 'adi.LeadID')
                                                ->where('adi.TestMode','=','0');    
                $GetProcessedLeads->addSelect(DB::raw("SUM(IF(ld.LeadStatus='1',1,0)) AS Accepted"),
                                              DB::raw("SUM(IF(ld.LeadStatus='2',1,0)) AS Returned"));
                
                if($CampaignType=="DirectPost")
                {
                    $GetProcessedLeads->whereBetween('adi.DirectPostDate', array($StartDate, $EndDate));
                }
                else
                {
                    $GetProcessedLeads->whereBetween('adi.PingDate', array($StartDate, $EndDate));
                }
                $GetProcessedLeads->whereIn('ld.BuyerID', $ParntnerBuyerIDs);

                $DashboardBuyerReport[$Campaigns->CampaignID] = $GetProcessedLeads->first(); */ 
                //$queries = DB::getQueryLog();
                //dd($queries);   


                //DB::enableQueryLog();
                $CompanyID      = Session::get('user_companyID');
                $CampaignID     = $Campaigns->CampaignID; 
                $CampaignType   = $Campaigns->CampaignType;
                $TablePrefix    = $Campaigns->TablePrefix;
                $api_data_inbound = "api_data_inbound as adi";
                $partner_vendor_campaigns = "partner_buyer_campaigns as pbc";
                $x_lead_data = $TablePrefix."_lead_data as ld";
                $x_outbound_requests = $TablePrefix."_outbound_requests as ors";
                $GetProcessedLeads = DB::table(DB::raw($x_outbound_requests))
                                          ->select(DB::raw("COUNT(DISTINCT(ors.LeadID)) AS Total"))
                                          ->leftjoin($x_lead_data, 'ors.LeadID', '=', 'ld.LeadID')
                                          ->leftjoin($partner_vendor_campaigns, 'ors.BuyerID', '=', 'pbc.BuyerID')
                                          ->leftjoin($api_data_inbound, 'ors.LeadID', '=', 'adi.LeadID')
                                          ->where('adi.CampaignID','=',$CampaignID)   
                                          ->where('pbc.CompanyID','=',$CompanyID)    
                                          ->where('adi.LeadID','!=','0')   
                                          ->where('adi.TestMode','=','0'); 

                if($CampaignType=="DirectPost")
                {
                    $GetProcessedLeads->addSelect(DB::raw('SUM(ors.DirectPostPrice) AS TotalBuyerPrice'),
                                        DB::raw("SUM(IF(ors.DirectPostStatus='1',1,0)) AS Accepted"),
                                        DB::raw("SUM(IF(ors.DirectPostStatus='2',1,0)) AS Returned"));
                }
                else
                {                  
                    $GetProcessedLeads->addSelect(DB::raw('SUM(ors.PingPrice) AS TotalBuyerPrice'),
                                        DB::raw("SUM(IF(ors.PostStatus='1',1,0)) AS Accepted"),
                                        DB::raw("SUM(IF(ors.PostStatus='2',1,0)) AS Returned"));
                }

                if($CampaignType=="DirectPost")
                {
                $GetProcessedLeads->whereBetween('ors.DirectPostDate', array($StartDate, $EndDate));
                }
                else
                {
                $GetProcessedLeads->whereBetween('ors.PingDate', array($StartDate, $EndDate));
                } 
                $DashboardBuyerReport[$Campaigns->CampaignID] = $GetProcessedLeads->first(); 
                //$queries = DB::getQueryLog();
                //dd($queries);          
            }       
        }
        return $DashboardBuyerReport;

    }
    public function ProfileUpdate($Data)
    {
    	$UserData['UserFirstName'] 	= $Data['UserFirstName'];
    	$UserData['UserLastName'] 	= $Data['UserLastName'];
    	if($Data['UserPassword']!='')
    	{
    		$UserData['UserPassword'] 	= md5($Data['UserPassword']);
    	}

    	DB::table('partner_users')->where('UserID',Session::get('user_id'))->update($UserData);
/*
    	$CompanyData['CompanyName'] 		= $Data['CompanyName'];
    	$CompanyData['CompanyAddress1'] 	= $Data['CompanyAddress1'];
    	$CompanyData['CompanyAddress2'] 	= $Data['CompanyAddress2'];
    	$CompanyData['CompanyCity'] 		= $Data['CompanyCity'];
    	$CompanyData['CompanyStateCounty'] 	= $Data['CompanyStateCountry'];
    	$CompanyData['CompanyZipPostCode'] 	= $Data['CompanyZipPostCode'];

    	DB::table('partner_companies')->where('CompanyID',Session::get('user_companyID'))->update($CompanyData);*/

    	return true;
    }

    public function CampaignDetails($Campaign)
    {
        $CampaignDetails = DB::table('master_campaigns')
            ->where('CampaignName',$Campaign)
            ->get()
            ->first();

        return $CampaignDetails;
    }
}
