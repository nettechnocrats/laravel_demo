<?php
namespace App\Http\Models;
use Illuminate\Database\Eloquent\Model;
use DB;
use Session;
class VendorManagementModel extends Model 
{
    public function CampaignDropDown()
    {
        $CampaignDropDown = DB::table('master_campaigns')
                    ->select('CampaignID','CampaignName')
                    ->where('CampaignID', '!=','0' )
                    ->where('MaintenanceMode', '!=','1' )
                    ->orderBy('CampaignName','ASC')
                    ->get();

        return $CampaignDropDown;
    }
    public function CompanyList()
    {
        $CompanyList = DB::table('partner_companies')
                    ->select('CompanyID','CompanyName')
                    //->where('CompanyStatus', '=','1' )
                    //->where('CompanyTestMode', '=','0' )
                    ->orderBy('CompanyName','ASC')
                    ->get();

        return $CompanyList;
    }
    
	public function GetVendorList($Search)
	{
		$Users['Count'] = "";
		$Users['Res'] 	= "";

        $CompanyID      = $Search['CompanyID'];
        $CampaignID     = $Search['CampaignID'];
        $VendorID       = $Search['VendorID'];
        $VendorName     = $Search['VendorName'];
        $VendorStatus   = $Search['VendorStatus'];
        $ShowInactive   = $Search['ShowInactive'];
        $ShowTest       = $Search['ShowTest'];

        $Start 		    = $Search['Start'];
        $NumOfRecords 	= $Search['NumOfRecords'];
        //DB::enableQueryLog();            
		$GetVendorList = DB::table('partner_vendor_campaigns as pvc')
						->select('pvc.*','mt.CampaignName','pc.CompanyName')
                        ->join('partner_companies as pc','pvc.CompanyID','=','pc.CompanyID')
                        ->join('master_campaigns as mt','mt.CampaignID','=','pvc.CampaignID');
		if ($CompanyID != "" && $CompanyID != "All") 
        {            
            $GetVendorList->where('pvc.CompanyID', $CompanyID );   
        }
        if ($CampaignID != "") 
		{
			$GetVendorList->where('pvc.CampaignID', $CampaignID );
        }
        if ($VendorID != "") 
        {
            $GetVendorList->where('pvc.VendorID',$VendorID);
        }
        if ($VendorName != "") 
        {
            $GetVendorList->where('pvc.AdminLabel','like','%' . $VendorName. '%');
        }        
        if ($VendorStatus != "" && $VendorStatus != "All") 
        {
            $GetVendorList->where('pvc.VendorStatus',$VendorStatus);
        }

        
        if($ShowInactive=='1' && $ShowTest=='0')
        {
            $GetVendorList->where('pc.CompanyStatus','=',0);
            $GetVendorList->where('pc.CompanyTestMode','=',0);
        }
        else if($ShowInactive=='0' && $ShowTest=='1')
        {
            $GetVendorList->where('pc.CompanyStatus','=',1);
            $GetVendorList->where('pc.CompanyTestMode','=',1);
        }
        else if($ShowInactive=='0' && $ShowTest=='0')
        {
            $GetVendorList->where('pc.CompanyStatus','=',1);
            $GetVendorList->where('pc.CompanyTestMode','=',0);
        }
         else if($ShowInactive=='1' && $ShowTest=='1')
        {
            $data->where('pc.CompanyStatus','=',0);
            $data->where('pc.CompanyTestMode','=',1);
        }


        $Users['Count'] = $GetVendorList->count();
	   	$Users['Res'] = $GetVendorList->orderBy('pc.CompanyName','ASC')
                    ->orderBy('pvc.PartnerLabel','ASC')
                    ->orderBy('mt.CampaignName','ASC')
                    ->orderBy('pvc.VendorID','ASC')
					->offset($Start)
            		->limit($NumOfRecords)
	                ->get()
	                ->toArray();
        //$queries = DB::getQueryLog();
        //dd($queries);
		return $Users;
  	}

    public function VendorDetails($VendorID)
    {
        $VendorDetails = DB::table('partner_vendor_campaigns as pvc')
                        ->select('pvc.*','mt.CampaignName')
                        ->join('master_campaigns as mt','mt.CampaignID','=','pvc.CampaignID')
                        ->where('pvc.VendorID', $VendorID)
                        ->first();

        return $VendorDetails;
    }

    public function SaveVendorDetails($VendorID,$Details)
    {
        //DB::enableQueryLog();
        $SaveVendorDetails = DB::table('partner_vendor_campaigns')
                ->where('VendorID',$VendorID)
                ->update($Details);
        //$queries = DB::getQueryLog();
        //dd($queries);
        return true;
    }
    public function AddVendorDetails($Details)
    {
        $AddVendorDetails = DB::table('partner_vendor_campaigns')
                ->insert($Details);
        if($AddVendorDetails)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    public function CompanyDetails($CompanyID)
    {
        $CompanyDetails = DB::table('partner_companies as pc')
                        ->select('pc.*')
                        ->where('pc.CompanyID', $CompanyID)
                        ->first();

        return $CompanyDetails;
    } 
     public function CompanyListForEdit($CompanyTestMode,$CompanyStatus)
    {
        $CompanyList = DB::table('partner_companies')
                    ->select('CompanyID','CompanyName')
                    ->where('CompanyStatus', '=',$CompanyStatus)
                    ->where('CompanyTestMode', '=',$CompanyTestMode )
                    ->orderBy('CompanyName','ASC')
                    ->get();

        return $CompanyList;
    }

}
