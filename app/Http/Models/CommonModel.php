<?php
namespace App\Http\Models;
use Illuminate\Database\Eloquent\Model;
use DB;
use Session;
class CommonModel extends Model 
{
	public function TiersDropDown()
	{
		$Tiers = DB::table('master_tiers')
					->select('*')
					->orderBy('TierPayout','ASC')
					->get();

	    return $Tiers;
	}	
	public function GetVendorsUsingCampaignID($CampaignID)
	{
		$GetVendorsUsingCampaignID = DB::table('partner_vendor_campaigns')
							->select('VendorID','AdminLabel')
							->where('CampaignID',$CampaignID)
							->orderBy('AdminLabel','ASC')
	    					->get();
	    
    	return $GetVendorsUsingCampaignID;
	}
	public function GetVendorUsingVendorCompany($VendorCompanyID,$CampaignID)
	{
		$Query = DB::table('partner_vendor_campaigns')
							->select('VendorID','AdminLabel')
							->where('CampaignID',$CampaignID);		
		if($VendorCompanyID!=='All')
		{
			$Query->where('CompanyID',$VendorCompanyID);			
		}
		$GetVendorUsingVendorCompany = $Query->orderBy('AdminLabel','ASC')
	    							->get();
	    
    	return $GetVendorUsingVendorCompany;
	}
	public function GetBuyersUsingCampaignID($CampaignID)
	{
		$GeBuyersUsingCampaignID = DB::table('partner_buyer_campaigns')
							->select('BuyerID','AdminLabel')
							->where('CampaignID',$CampaignID)
							->orderBy('AdminLabel','ASC')
	    					->get();
	    
    	return $GeBuyersUsingCampaignID;
	}
	public function GetBuyerUsingBuyerCompany($BuyerCompanyID,$CampaignID)
	{
		$Query = DB::table('partner_buyer_campaigns')
							->select('BuyerID','AdminLabel')
							->where('CampaignID',$CampaignID);
		if($BuyerCompanyID!=='All')
		{
			$Query->where('CompanyID',$BuyerCompanyID);			
		}
		$GetBuyerUsingBuyerCompany = $Query->orderBy('AdminLabel','ASC')
	    						->get();
	    
    	return $GetBuyerUsingBuyerCompany;
	}
	public function GetCampaignDetails($CampaignID)
	{
		$CampaignInfo = DB::table('master_campaigns')
					->select('CampaignID','CampaignName','TablePrefix','CampaignType','HasSpecs')
					->where('CampaignID', $CampaignID )
					->where('CampaignID','!=','0')
					->where('MaintenanceMode','!=','1')
					->first();
    	return $CampaignInfo;
	}
	public function GetVendorCompany($CampaignID)
	{
		$VendorCompany = DB::table('partner_vendor_campaigns as pvc')
				    		->join('partner_companies as pc', 'pvc.CompanyID', '=', 'pc.CompanyID')
				    		->where('pvc.CampaignID','=',$CampaignID)
							->groupBy('pc.CompanyID')
							->orderBy('pc.CompanyName','ASC')
							->select('pc.CompanyID','pc.CompanyName')
							->get();

        return $VendorCompany;
	}	
	public function GetBuyerCompany($CampaignID)
	{
		$VendorCompany = DB::table('partner_buyer_campaigns as pbc')
				    		->join('partner_companies as pc', 'pbc.CompanyID', '=', 'pc.CompanyID')
				    		->where('pbc.CampaignID','=',$CampaignID)
							->groupBy('pc.CompanyID')
							->orderBy('pc.CompanyName','ASC')
							->select('pc.CompanyID','pc.CompanyName')
							->get();

        return $VendorCompany;
	}	

	
	public function GetUserUsingCompanyID($VendorCompanyID,$CampaignID)
	{
		$Query = DB::table('partner_users as pu')
					->select('pu.UserID','pu.UserFirstName','pu.UserLastName');
		if($VendorCompanyID!='All')
		{
			$Query->where('pu.CompanyID','=',$VendorCompanyID);			
		}				    		
		$User = $Query->orderBy('pu.UserFirstName','ASC')
				->get();

        return $User;
	}
	//////////////////////////////
	public function GetVendorCompanyUsingShowInactiveAndTest($ShowInactiveVendorCompanyID,$ShowTestVendorCompanyID,$CampaignID)
	{
		$Query = DB::table('partner_vendor_campaigns as pvc')
				    		->join('partner_companies as pc', 'pvc.CompanyID', '=', 'pc.CompanyID')
				    		->where('pvc.CampaignID','=',$CampaignID);
		if($ShowInactiveVendorCompanyID=='1' && $ShowTestVendorCompanyID=='0')
        {
            $Query->where('pc.CompanyStatus','=',0);
            $Query->where('pc.CompanyTestMode','=',0);
        }
        else if($ShowInactiveVendorCompanyID=='0' && $ShowTestVendorCompanyID=='1')
        {
            $Query->where('pc.CompanyStatus','=',1);
            $Query->where('pc.CompanyTestMode','=',1);
        }
        else if($ShowInactiveVendorCompanyID=='0' && $ShowTestVendorCompanyID=='0')
        {
            $Query->where('pc.CompanyStatus','=',1);
            $Query->where('pc.CompanyTestMode','=',0);
        }
         else if($ShowInactiveVendorCompanyID=='1' && $ShowTestVendorCompanyID=='1')
        {
            $Query->where('pc.CompanyStatus','=',0);
            $Query->where('pc.CompanyTestMode','=',1);
        }		    		
		
		$VendorCompany = $Query->groupBy('pc.CompanyID')
							->orderBy('pc.CompanyName','ASC')
							->select('pc.CompanyID','pc.CompanyName')
							->get();

        return $VendorCompany;
	}
	public function GetBuyerCompanyUsingShowInactiveAndTest($ShowInactiveBuyerCompanyID,$ShowTestBuyerCompanyID,$CampaignID)
	{
		$Query = DB::table('partner_buyer_campaigns as pbc')
				    		->join('partner_companies as pc', 'pbc.CompanyID', '=', 'pc.CompanyID')
				    		->where('pbc.CampaignID','=',$CampaignID);
		if($ShowInactiveBuyerCompanyID=='1' && $ShowTestBuyerCompanyID=='0')
        {
            $Query->where('pc.CompanyStatus','=',0);
            $Query->where('pc.CompanyTestMode','=',0);
        }
        else if($ShowInactiveBuyerCompanyID=='0' && $ShowTestBuyerCompanyID=='1')
        {
            $Query->where('pc.CompanyStatus','=',1);
            $Query->where('pc.CompanyTestMode','=',1);
        }
        else if($ShowInactiveBuyerCompanyID=='0' && $ShowTestBuyerCompanyID=='0')
        {
            $Query->where('pc.CompanyStatus','=',1);
            $Query->where('pc.CompanyTestMode','=',0);
        }
         else if($ShowInactiveBuyerCompanyID=='1' && $ShowTestBuyerCompanyID=='1')
        {
            $Query->where('pc.CompanyStatus','=',0);
            $Query->where('pc.CompanyTestMode','=',1);
        }		    		
		
		$VendorCompany = $Query->groupBy('pc.CompanyID')
							->orderBy('pc.CompanyName','ASC')
							->select('pc.CompanyID','pc.CompanyName')
							->get();

        return $VendorCompany;
	}

	public function GetVendorsUsingCampaignIDAndShowInactiveAndTest($ShowInactiveVendor,$ShowTestVendor,$CampaignID)
	{
		$Query = DB::table('partner_vendor_campaigns')
							->select('VendorID','AdminLabel');
		if($ShowInactiveVendor=='1' && $ShowTestVendor=='0')
        {
            $Query->where('VendorStatus','=',0);
            $Query->where('VendorTestMode','=',0);
        }
        else if($ShowInactiveVendor=='0' && $ShowTestVendor=='1')
        {
            $Query->where('VendorStatus','=',1);
            $Query->where('VendorTestMode','=',1);
        }
        else if($ShowInactiveVendor=='0' && $ShowTestVendor=='0')
        {
            $Query->where('VendorStatus','=',1);
            $Query->where('VendorTestMode','=',0);
        }
         else if($ShowInactiveVendor=='1' && $ShowTestVendor=='1')
        {
            $Query->where('VendorStatus','=',0);
            $Query->where('VendorTestMode','=',1);
        }
		$GetVendorsUsingCampaignID = $Query->where('CampaignID',$CampaignID)
							->orderBy('AdminLabel','ASC')
	    					->get();
	    
    	return $GetVendorsUsingCampaignID;
	}


	public function GetCompanyUsingShwowInactiveAndTest($ShowInactive, $ShowTest) 
     {
        $getCompany = array();
        $data = DB::table('partner_companies as pc');

        if($ShowInactive=='1' && $ShowTest=='0')
        {
            $data->where('pc.CompanyTestMode','=',0);
            $data->where('pc.CompanyStatus','=',0);
        }
        else if($ShowInactive=='0' && $ShowTest=='1')
        {
            $data->where('pc.CompanyStatus','=',1);
            $data->where('pc.CompanyTestMode','=',1);
        }
        else if($ShowInactive=='0' && $ShowTest=='0')
        {
            $data->where('pc.CompanyStatus','=',1);
            $data->where('pc.CompanyTestMode','=',0);
        } 
        else if($ShowInactive=='1' && $ShowTest=='1')
        {
            $data->where('pc.CompanyStatus','=',0);
            $data->where('pc.CompanyTestMode','=',1);
        }
        $data->orderBy('pc.CompanyName', 'ASC')
                ->select('pc.CompanyID', 'pc.CompanyName');

        $res = $data->get();
        foreach ($res as $d) {
            $getCompany[$d->CompanyID] = $d->CompanyName;
        }
        return $getCompany;
    }
    public function GetCompanyDetails($CompanyID)
	{
		$GetCompanyDetails = DB::table('partner_companies')
					->select('*')
					->where('CompanyID', $CompanyID )
					->first();
    	return $GetCompanyDetails;
	}
}

