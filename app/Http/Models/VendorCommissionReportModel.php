<?php
namespace App\Http\Models;
use Illuminate\Database\Eloquent\Model;
use DB;
use Session;
class VendorCommissionReportModel extends Model 
{
	public function CampaignDropDown()
	{
		$CampaignDropDown = DB::table('master_campaigns')
					->select('CampaignID','CampaignName')
					->where('CampaignID', '!=','0' )
					->where('MaintenanceMode', '!=','1' )
                    ->orderBy('CampaignName','ASC')
					->get();

	    return $CampaignDropDown;
	}	

	public function GetCampaignDetails($CampaignID)
	{
		$CampaignInfo = DB::table('master_campaigns')
					->select('CampaignID','CampaignName','TablePrefix','CampaignType')
					->where('CampaignID', $CampaignID )
					->where('CampaignID','!=','0')
					->where('MaintenanceMode','!=','1')
					->first();
    	return $CampaignInfo;
	}

  	public function GetVendorCommissionReport($Search)
    {
        $StartDate  = $Search['StartDate'];
        $EndDate    = $Search['EndDate'];
        $CompanyID  = $Search['CompanyID'];
        $CampaignID = $Search['CampaignID'];
        $VendorCompanyID  = $Search['VendorCompanyID'];
        $BuyerCompanyID   = $Search['BuyerCompanyID'];
        $BuyerID          = $Search['BuyerID'];
        $userRole   = $Search['UserRole'];
        $userID     = $Search['UserID'];
       
        $Campaigns=$this->GetCampaignDetails($CampaignID);

        $LeadDataTable = $Campaigns->TablePrefix."_lead_data AS ld";

        if($Campaigns->CampaignType=="DirectPost"){
            $getCommissionReport = "SELECT CompanyName, CompanyID, AdminLabel, VendorID, Tier, TierPayout, SUM(LeadsIn) AS LeadsIn, SUM(LeadsAccepted) AS LeadsAccepted, SUM(Commission) AS Commission, CONCAT(((SUM(LeadsAccepted) / SUM(LeadsIn)) * 100)) AS AcceptPercent, SUM(Redirected) AS Redirected, IF ( SUM(Redirected) = '0','0%',CONCAT(((SUM(Redirected) / SUM(LeadsAccepted)) * 100))) AS RedirectPercent, SUM(LeadsReturned) as LeadsReturned,
                SUM(VendorPrice) AS ReturnedPrice, (SUM(Commission) + SUM(VendorPrice)) AS SoldAmount FROM (SELECT `pc`.`CompanyName`, `pc`.`CompanyID`, `pvc`.`AdminLabel`, `pvc`.`VendorID`, `ld`.`Tier`, mt.`TierPayout`, COUNT(*) AS LeadsIn, COUNT(*) AS LeadsAccepted, SUM(adi.`VendorPrice`) AS Commission, SUM(IF(ld.`IsRedirected` = '1', 1, 0)) AS Redirected, '0' AS LeadsReturned,'0' AS VendorPrice 
            FROM `api_data_inbound` AS `adi` JOIN ".$LeadDataTable." ON `ld`.`LeadID` = `adi`.`LeadID`
            JOIN `partner_companies` AS `pc` ON `ld`.`CompanyID` = `pc`.`CompanyID`
            JOIN `partner_buyer_campaigns` AS `pbc` ON `pbc`.`BuyerID` = `ld`.`BuyerID` 
            JOIN `partner_vendor_campaigns` AS `pvc` ON `pvc`.`VendorID` = `ld`.`VendorID`
            JOIN `master_tiers` AS `mt` ON `mt`.`TierID` = `ld`.`Tier`
            WHERE `ld`.`DirectPostTime` BETWEEN '".$StartDate."' AND '".$EndDate."' AND `adi`.`TestMode` = '0' AND `adi`.`LeadID` != '0' AND adi.DirectPostStatus = '1' ";
            if($VendorCompanyID!="" && $VendorCompanyID!="All"){
                $getCommissionReport .= "AND `pc`.`CompanyID` = '".$VendorCompanyID."'";
            }if($BuyerCompanyID!="" && $BuyerCompanyID!="All"){
                $getCommissionReport .= "AND `pbc`.`CompanyID` = '".$BuyerCompanyID."'";
            }if($BuyerID!='' && $BuyerID!="All"){
                $getCommissionReport .= "AND `pbc`.`BuyerID` = '".$BuyerID."'";
            }
            $getCommissionReport .= " GROUP BY `pc`.`CompanyName`,  `pvc`.`AdminLabel`, `pvc`.`VendorID`, ld.`Tier` ";
            $getCommissionReport .= " UNION 
            SELECT `pc`.`CompanyName`, `pc`.`CompanyID`, `pvc`.`AdminLabel`,  `pvc`.`VendorID`, `ld`.`Tier`, mt.`TierPayout`, COUNT(*) AS LeadsIn,
            '0' AS LeadsAccepted, '0.00' AS Commission, '0' AS Redirected, '0' AS LeadsReturned,
            '0' AS VendorPrice 
            FROM `api_data_inbound` AS `adi` JOIN ".$LeadDataTable." ON `ld`.`LeadID` = `adi`.`LeadID` JOIN `partner_companies` AS `pc` ON `ld`.`CompanyID` = `pc`.`CompanyID` JOIN `partner_vendor_campaigns` AS `pvc` ON `pvc`.`VendorID` = `ld`.`VendorID` JOIN `master_tiers` AS `mt` ON `mt`.`TierID` = `ld`.`Tier` WHERE `ld`.`DirectPostTime` BETWEEN '".$StartDate."' AND '".$EndDate."' AND `adi`.`TestMode` = '0' AND `adi`.`LeadID` != '0' AND adi.DirectPostStatus != '1'";            
            if($VendorCompanyID!="" && $VendorCompanyID!="All"){
                $getCommissionReport .= "AND `pc`.`CompanyID` = '".$VendorCompanyID."'";
            }           
            $getCommissionReport .= " GROUP BY `pc`.`CompanyName`, `pc`.`CompanyID`, `pvc`.`AdminLabel`, `pvc`.`VendorID`, ld.`Tier`";
            $getCommissionReport .= " UNION
            SELECT `pc`.`CompanyName`, `pc`.`CompanyID`, `pvc`.`AdminLabel`, `pvc`.`VendorID`, `ld`.`Tier`, mt.`TierPayout`, '0' AS LeadsIn, '0' AS LeadsAccepted, '0.00' AS Commission, '0' AS Redirected, COUNT(*) AS LeadsReturned, SUM(VendorPrice) as VendorPrice 
            FROM `api_data_inbound` AS `adi` JOIN ".$LeadDataTable." ON `ld`.`LeadID` = `adi`.`LeadID` JOIN `partner_companies` AS `pc` ON `ld`.`CompanyID` = `pc`.`CompanyID` JOIN `partner_vendor_campaigns` AS `pvc` ON `pvc`.`VendorID` = `ld`.`VendorID` JOIN `master_tiers` AS `mt` ON `mt`.`TierID` = `ld`.`Tier` WHERE `ld`.`DirectPostTime` BETWEEN '".$StartDate."' AND '".$EndDate."' AND `adi`.`TestMode` = '0' AND `adi`.`LeadID` != '0' AND adi.DirectPostStatus = '2'";
            if($VendorCompanyID!="" && $VendorCompanyID!="All"){
                $getCommissionReport .= "AND `pc`.`CompanyID` = '".$VendorCompanyID."'";
            }
            $getCommissionReport .= " GROUP BY `pc`.`CompanyName`, `pc`.`CompanyID`, `pvc`.`AdminLabel`, `pvc`.`VendorID`, ld.`Tier`";
            $getCommissionReport .=" ) AS ans GROUP BY CompanyName, CompanyID, AdminLabel, VendorID, Tier, TierPayout ORDER BY CompanyName, TierPayout ";
        }
        else if($Campaigns->CampaignType=="PingPost")
        {
            $getCommissionReport = "SELECT CompanyName, CompanyID, AdminLabel, VendorID, SUM(LeadsIn) AS LeadsIn, SUM(LeadsAccepted) AS LeadsAccepted, SUM(Commission) AS Commission, CONCAT(((SUM(LeadsAccepted) / SUM(LeadsIn)) * 100)) AS AcceptPercent,SUM(LeadsReturned) AS LeadsReturned,
                SUM(VendorPrice) AS ReturnedPrice, (SUM(Commission) + SUM(VendorPrice)) AS SoldAmount 
            FROM (SELECT `pc`.`CompanyName`, `pc`.`CompanyID`, `pvc`.`AdminLabel`, `pvc`.`VendorID`, COUNT(*) AS LeadsIn, COUNT(*) AS LeadsAccepted, SUM(adi.`VendorPrice`) AS Commission, '0' AS LeadsReturned,'0' AS VendorPrice 
            FROM `api_data_inbound` AS `adi` JOIN ".$LeadDataTable." ON `ld`.`LeadID` = `adi`.`LeadID`
            JOIN `partner_companies` AS `pc` ON `ld`.`CompanyID` = `pc`.`CompanyID`
            JOIN `partner_buyer_campaigns` AS `pbc` ON `pbc`.`BuyerID` = `ld`.`BuyerID` 
            JOIN `partner_vendor_campaigns` AS `pvc` ON `pvc`.`VendorID` = `ld`.`VendorID`
            WHERE `ld`.`PostTime` BETWEEN '".$StartDate."' AND '".$EndDate."' AND `adi`.`TestMode` = '0' AND `adi`.`LeadID` != '0' AND adi.PostStatus = '1' ";
            if($VendorCompanyID!="" && $VendorCompanyID!="All"){
                $getCommissionReport .= "AND `pc`.`CompanyID` = '".$VendorCompanyID."'";
            }if($BuyerCompanyID!="" && $BuyerCompanyID!="All"){
                $getCommissionReport .= "AND `pbc`.`CompanyID` = '".$BuyerCompanyID."'";
            }if($BuyerID!='' && $BuyerID!="All"){
                $getCommissionReport .= "AND `pbc`.`BuyerID` = '".$BuyerID."'";
            }
            $getCommissionReport .= " GROUP BY `pc`.`CompanyName`, `pc`.`CompanyID`, `pvc`.`AdminLabel`, `pvc`.`VendorID` ";
            $getCommissionReport .= " UNION 
            SELECT `pc`.`CompanyName`, `pc`.`CompanyID`, `pvc`.`AdminLabel`,  `pvc`.`VendorID`, COUNT(*) AS LeadsIn,
            '0' AS LeadsAccepted, '0.00' AS Commission,  '0' AS LeadsReturned, '0' AS VendorPrice  
            FROM `api_data_inbound` AS `adi` JOIN ".$LeadDataTable." ON `ld`.`LeadID` = `adi`.`LeadID` JOIN `partner_companies` AS `pc` ON `ld`.`CompanyID` = `pc`.`CompanyID` JOIN `partner_vendor_campaigns` AS `pvc` ON `pvc`.`VendorID` = `ld`.`VendorID` WHERE `ld`.`PostTime` BETWEEN '".$StartDate."' AND '".$EndDate."' AND `adi`.`TestMode` = '0' AND `adi`.`LeadID` != '0' AND adi.PostStatus != '1'";
            if($VendorCompanyID!="" && $VendorCompanyID!="All"){
                $getCommissionReport .= "AND `pc`.`CompanyID` = '".$VendorCompanyID."'";
            }     
            $getCommissionReport .= " GROUP BY `pc`.`CompanyName`, `pc`.`CompanyID`, `pvc`.`AdminLabel`, `pvc`.`VendorID` ";
            $getCommissionReport .= "UNION 
            SELECT `pc`.`CompanyName`, `pc`.`CompanyID`, `pvc`.`AdminLabel`,  `pvc`.`VendorID`, COUNT(*) AS LeadsIn,
            '0' AS LeadsAccepted, '0.00' AS Commission,   COUNT(*) AS LeadsReturned, SUM(VendorPrice) as VendorPrice  
            FROM `api_data_inbound` AS `adi` JOIN ".$LeadDataTable." ON `ld`.`LeadID` = `adi`.`LeadID` JOIN `partner_companies` AS `pc` ON `ld`.`CompanyID` = `pc`.`CompanyID` JOIN `partner_vendor_campaigns` AS `pvc` ON `pvc`.`VendorID` = `ld`.`VendorID` WHERE `ld`.`PostTime` BETWEEN '".$StartDate."' AND '".$EndDate."' AND `adi`.`TestMode` = '0' AND `adi`.`LeadID` != '0' AND adi.PostStatus = '2'";
            if($VendorCompanyID!="" && $VendorCompanyID!="All"){
                $getCommissionReport .= "AND `pc`.`CompanyID` = '".$VendorCompanyID."'";
            }          
            $getCommissionReport .= " GROUP BY `pc`.`CompanyName`, `pc`.`CompanyID`, `pvc`.`AdminLabel`, `pvc`.`VendorID` ";
            $getCommissionReport .=") AS ans GROUP BY CompanyName, CompanyID, AdminLabel, VendorID ORDER BY CompanyName ";    
        }       

        $result = DB::select($getCommissionReport);
        return $result;
    }

}
