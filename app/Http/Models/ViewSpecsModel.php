<?php
namespace App\Http\Models;
use Illuminate\Database\Eloquent\Model;
use DB;
use Session;
class ViewSpecsModel extends Model 
{
    public function GetCampaignDetails($CampaignID) {
        
        $CampaignInfo = DB::table('master_campaigns as mt')
                ->select('mt.*')
                ->where('mt.CampaignID', $CampaignID)
                ->where('mt.CampaignID', '!=', '0')
                ->first();

        return $CampaignInfo;
    }
    public function CampaignSpecsDetails($CampaignID)
    {
        $data = DB::table('campaign_specs')
                    ->select('*')
                    ->where('CampaignID',$CampaignID)
                    ->first();
        return $data;
    }
    public function VendorDetailsForSpecs($VendorID)
    {
        $data = DB::table('partner_vendor_campaigns')
                    ->select('*')
                    ->where('VendorID',$VendorID)
                    ->first();
        return $data;
    }

    public function GetCompanyDetails($CompanyID)
    {
        $data = DB::table('partner_companies')
                    ->select('*')
                    ->where('CompanyID',$CompanyID)
                    ->first();
        return $data;
    }
    public function UserDetails($UserID)
    {
        $data = DB::table('partner_users')
                    ->select('*')
                    ->where('UserID',$UserID)
                    ->first();
        return $data;
    }

    public function GetCampaignSpecsDetails($CampaignID) {
        
        $GetCampaignSpecsDetails = DB::table('master_campaigns as mt')
                ->select('mt.*','cs.Specs')
                ->join('campaign_specs as cs','cs.CampaignID','=','mt.CampaignID')
                ->where('mt.CampaignID', $CampaignID)
                ->first();

        return $GetCampaignSpecsDetails;
    }
}
