<?php
namespace App\Http\Models;
use Illuminate\Database\Eloquent\Model;
use DB;
use Session;
class UserManagementModel extends Model 
{
    public function GetUserRoles()
    {
        $data = DB::table('user_permissions')
                ->select('UserRoleID', 'UserRole')
                ->orderBy('UserRoleID','ASC')
                ->get();

        $UserRole = array();

        foreach ($data as $ur) {
           $UserRole[$ur->UserRoleID] = $ur->UserRole;
        }
        return $UserRole;
    }
	public function GetUserList($Search)
	{
		$Users['Count'] = "";
		$Users['Res'] 	= "";

		$FirstName 	= $Search['FirstName'];
        $LastName 	= $Search['LastName'];
        $Email 		= $Search['Email'];
        $Phone 		= $Search['Phone'];
        $UserStatus	= $Search['UserStatus'];
        $UserRole 	= $Search['UserRole'];
        $CompanyID  = $Search['CompanyID'];
        $Start 		= $Search['Start'];
        $NumOfRecords 	= $Search['NumOfRecords'];
                
		$GetUserList = DB::table('partner_users as pu')
                            ->join('partner_companies as pc','pc.CompanyID','=','pu.CompanyID')
						->select('pu.*','pc.CompanyName');
		if ($FirstName != "") 
		{
			$GetUserList->where('pu.UserFirstName','like',"%$FirstName%");
        }
        if ($LastName != "") 
        {
            $GetUserList->where('pu.UserFirstName','like',"%$LastName%");
        }
        if ($Email != "") 
        {
            $GetUserList->where('pu.UserEmail','like',"%$Email%");
        }
        if ($Phone != "") 
        {
        	$GetUserList->where('pu.UserTel','like',"%$Phone%");
        }
        if ($UserRole != "") 
        {
        	$GetUserList->where('pu.UserRole',$UserRole);
        }
       	if ($UserStatus != "") 
        {
        	$GetUserList->where('pu.UserStatus',$UserStatus);
        }   
        if ($CompanyID != "All" && $CompanyID != "") 
        {
            $GetUserList->where('pu.CompanyID',$CompanyID);             
        }  
        //$GetUserList->where('pu.UserRole','!=','1');
        $Users['Count'] = $GetUserList->count();
	   	$Users['Res'] = $GetUserList
					->orderBy('pu.UserEmail','ASC')
					->offset($Start)
            		->limit($NumOfRecords)
	                ->get()
	                ->toArray();
        
		return $Users;
  	}

    public function UserDetails($UserID)
    {
        $data = DB::table('partner_users')
                ->select('*')
                ->where('UserID',$UserID)
                ->get()
                ->first();
        return $data;
    }

    public function SaveUserDetails($UserID,$Details)
    {
        $SaveUserDetails = DB::table('partner_users')
                ->where('UserID',$UserID)
                ->update($Details);
        return true;
    }
    public function AddUserDetails($Details)
    {
        $AddUserDetails = DB::table('partner_users')
                                  ->insert($Details);
        if($AddUserDetails)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    public function GetCompanyDetails($CompanyID)
    {
        $GetCompanyDetails = DB::table('partner_companies')
                    ->select('*')
                    ->where('CompanyID', $CompanyID )
                    ->first();
        return $GetCompanyDetails;
    }
    public function CompanyList()
    {
        $CompanyList = DB::table('partner_companies')
                    ->select('CompanyID','CompanyName')
                    ->get();
        return $CompanyList;
    }

}
