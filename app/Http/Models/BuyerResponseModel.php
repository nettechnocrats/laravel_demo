<?php
namespace App\Http\Models;
use Illuminate\Database\Eloquent\Model;
use DB;
use Session;
class BuyerResponseModel extends Model 
{
    public static function CampaignDropDown()
    {
        $getCampaign = DB::table('master_campaigns')
                    ->select('CampaignID','CampaignName')
                    ->where('CampaignID', '!=','0' )
                    ->where('MaintenanceMode', '!=','1' )
                    ->orderBy('CampaignName','ASC')
                    ->groupBy('CampaignID')
                    ->get();

        return $getCampaign;
    }
    public function GetCampaignDetails($CampaignID)
    {
        $CampaignInfo = DB::table('master_campaigns')
                    ->select('CampaignID','CampaignName','TablePrefix','CampaignType')
                    ->where('CampaignID', $CampaignID )
                    ->where('CampaignID','!=','0')
                    ->where('MaintenanceMode','!=','1')
                    ->first();
        return $CampaignInfo;
    }

    public function BuyerResponse($CampaignID,$Action) 
    {
        $TablePrefix = $this->GetCampaignDetails($CampaignID);
        $TableName = $TablePrefix->TablePrefix.'_outbound_requests';
        
        if($Action == 'DirectPost') {
            $Select = "DirectPostResponse";
        } else if($Action == "Ping") {
            $Select = "PingResponse";
        } else if($Action == "Post" || $Action == "PingPost") {
            $Select = "PostResponse";
        }
        
        $Response = DB::table($TableName)
                    ->select($Select)
                    ->where('CampaignID', $CampaignID)
                    ->where($Select,'!=',"")
                    ->groupBy($Select)
                    ->get();
        
        return $Response;
    }

    
    public function GetBuyerResponseList($Searcher) 
    {
        
        $CampaignID     = $Searcher['CampaignID'];
        $BuyerCompany   = $Searcher['BuyerCompany'];
        $Buyer          = $Searcher['Buyer'];
        $Response       = $Searcher['Response'];
        $StartDate      = date('Y-m-d',strtotime($Searcher['StartDate'])).' 00:00:00';
        $EndDate        = date('Y-m-d',strtotime($Searcher['EndDate'])).' 23:59:59';
        $Sort           = $Searcher['Sort'];
        $Action         = $Searcher['Action'];
        $Start          = $Searcher['Start'];
        $NumOfRecords   = $Searcher['NumOfRecords'];
        

        $TablePrefix = $this->GetCampaignDetails($CampaignID);
        $TableName = $TablePrefix->TablePrefix.'_outbound_requests as obd';

        $GetBuyerResponseList = DB::table($TableName);

        if($Action == 'DirectPost') {
            $GetBuyerResponseList->select(
                'obd.DirectPostResponse as Response',
                'obd.DirectPostDate as Date',
                DB::raw('COUNT(obd.DirectPostResponse) as Count')
            ); 
        } else if($Action == "Ping"){
            $GetBuyerResponseList->select(
                'obd.PingResponse as Response', 
                'obd.PingDate as Date',
                DB::raw('COUNT(obd.PingResponse) as Count')
            ); 
        } else if($Action == "Post" || $Action == "PingPost") {
            $GetBuyerResponseList->select(
                'obd.PostResponse as Response', 
                'obd.PostDate as Date',
                DB::raw('COUNT(obd.PostResponse) as Count')
            ); 
        }

        $GetBuyerResponseList->addSelect('mc.CampaignName','obd.BuyerResponseMessage as Message',
            DB::raw('CONCAT(pbc.AdminLabel, " ", "(BID:", pbc.BuyerID, ")") AS BuyerName')
        );

        $GetBuyerResponseList->join('master_campaigns as mc', 'mc.CampaignID', '=','obd.CampaignID')
            ->join('partner_buyer_campaigns as pbc', 'pbc.BuyerID', '=','obd.BuyerID')
            ->where('obd.CampaignID', $CampaignID);

        if ($Buyer > 0) {
            $GetBuyerResponseList->where('obd.BuyerID', $Buyer);
        }

        if($Action == 'DirectPost') { 
            if ($Response != "") {
                $GetBuyerResponseList->where('obd.DirectPostResponse', "LIKE" ,"%".$Response."%");
            }
            $GetBuyerResponseList->whereBetween('obd.DirectPostDate', array($StartDate, $EndDate))
                    ->groupBy('obd.BuyerResponseMessage')
                    ->groupBy('obd.BuyerID')
                    ->orderBy('Count',$Sort);
        } else if($Action == "Ping") {
            if ($Response != "") {
                $GetBuyerResponseList->where('obd.PingResponse', "LIKE" ,"%".$Response."%");
            }
            $GetBuyerResponseList->whereBetween('obd.PingDate', array($StartDate, $EndDate))
                    ->groupBy('obd.BuyerResponseMessage')
                    ->groupBy('obd.BuyerID')
                    ->orderBy('Count',$Sort);
        } else if($Action == "Post" || $Action == "PingPost") { 
            if ($Response != "") {
                $GetBuyerResponseList->where('obd.PingResponse', "LIKE" ,"%".$Response."%");
            }
            $GetBuyerResponseList->whereBetween('obd.PostDate', array($StartDate, $EndDate))
                    ->groupBy('obd.BuyerResponseMessage')
                    ->groupBy('obd.BuyerID')
                    ->orderBy('Count',$Sort);
        }

        $BuyerResponse['Count'] = count($GetBuyerResponseList->get());
        $BuyerResponse['Res'] = $GetBuyerResponseList
                        ->offset($Start)
                        ->limit($NumOfRecords)
                        ->get()
                        ->toArray();

        return $BuyerResponse;
    }
}
