<?php
namespace App\Http\Models;
use Illuminate\Database\Eloquent\Model;
use DB;
use Session;
class BuyerInvoiceReportModel extends Model 
{
	public function CampaignDropDown()
	{
		$CampaignDropDown = DB::table('master_campaigns')
					->select('CampaignID','CampaignName')
					->where('CampaignID', '!=','0' )
					->where('MaintenanceMode', '!=','1' )
                    ->orderBy('CampaignName','ASC')
					->get();

	    return $CampaignDropDown;
	}	

	public function GetCampaignDetails($CampaignID)
	{
		$CampaignInfo = DB::table('master_campaigns')
					->select('CampaignID','CampaignName','TablePrefix','CampaignType')
					->where('CampaignID', $CampaignID )
					->where('CampaignID','!=','0')
					->where('MaintenanceMode','!=','1')
					->first();
    	return $CampaignInfo;
	}

  	public function GetBuyerInvoiceReport($Search)
    {
        $StartDate  = $Search['StartDate'];
        $EndDate    = $Search['EndDate'];
        $ShowAll    = $Search['ShowAll'];
        $CampaignID = $Search['CampaignID'];
        $BuyerCompanyID     = $Search['BuyerCompanyID'];
        $VendorCompanyID    = $Search['VendorCompanyID'];
        $VendorID           = $Search['VendorID'];
        $TierID              = $Search['Tiers'];
        $userRole   = $Search['UserRole'];
        $userID     = $Search['UserID'];
              
        $Campaigns=$this->GetCampaignDetails($CampaignID);

        $X_outbound_requests = $Campaigns->TablePrefix."_outbound_requests AS caor";
        $X_lead_data = $Campaigns->TablePrefix."_lead_data AS ld";
        $partner_buyer_campaigns = "partner_buyer_campaigns AS pbc";
        $partner_companies = "partner_companies AS pc";
        if($Campaigns->CampaignType=="DirectPost")
        {
            $sql = "SELECT CompanyID, CompanyName, AdminLabel,FixedPrice, BuyerID, SUM(Attempted) AS Attempted,
            SUM(Accepted) AS Accepted, (SUM(Accepted) /   SUM(Attempted)) * 100 AS AcceptPercent,
            SUM(Total) AS Total, SUM(Redirected) AS Redirected,
            (SUM(Redirected) / SUM(Accepted)) * 100 AS RedirectRate, SUM(LeadsReturned) AS LeadsReturned, SUM(DirectPostPrice) AS ReturnedPrice,
            (SUM(Total) + SUM(DirectPostPrice)) AS SoldAmount,
            (SUM(Attempted) / FixedPrice) * 100 AS EPC
            FROM (
            SELECT `pbc`.`CompanyID`, `pc`.`CompanyName`, `pbc`.`AdminLabel`,`pbc`.`FixedPrice`,`pbc`.`BuyerID`, COUNT(*) AS Attempted,
            SUM(IF(caor.DirectPostStatus='1',1,0)) AS Accepted,
            SUM(caor.DirectPostPrice) AS Total,
            SUM(IF(ld.IsRedirected=1,1,0)) AS Redirected,
            '0' AS LeadsReturned,
            '0' AS DirectPostPrice
            FROM ".$X_outbound_requests." 
            INNER JOIN ".$partner_buyer_campaigns." ON `caor`.`BuyerID` = `pbc`.`BuyerID`
            INNER JOIN ".$partner_companies." ON `pbc`.`CompanyID` = `pc`.`CompanyID`
            INNER JOIN ".$X_lead_data." ON `ld`.`LeadID` = `caor`.`LeadID`
            WHERE `caor`.`DirectPostDate` BETWEEN '".$StartDate."' AND '".$EndDate."'
            AND `caor`.`TestMode` = '1'
            AND `caor`.`DirectPostStatus` = '1' ";
            if($BuyerCompanyID!="All"){
                $sql .= "AND `pc`.`CompanyID` = ".$BuyerCompanyID." ";
            }if($VendorCompanyID!= 'All' && $VendorID == 'All'){
                $sql .= "AND `caor`.`VendorID` IN (select VendorID  from partner_vendor_campaigns where CompanyID = ".$VendorCompany.")";
            }if($VendorID!="All"){
                $sql .= "AND `caor`.`VendorID` = ".$VendorID." ";
            }           
            if($TierID!="All"){
                $sql .= "AND `caor`.`Tier` = '".$TierID."' ";
            }
            $sql .= " GROUP BY `caor`.`BuyerID`
            UNION
            SELECT `pbc`.`CompanyID`, `pc`.`CompanyName`, `pbc`.`AdminLabel`,`pbc`.`FixedPrice`,`pbc`.`BuyerID`, COUNT(*) AS Attempted,
            0 AS Accepted,
            0 AS Total,
            0 AS Redirected,
            '0' AS LeadsReturned,
            '0' AS DirectPostPrice
            FROM ".$X_outbound_requests." 
            INNER JOIN ".$partner_buyer_campaigns." ON `caor`.`BuyerID` = `pbc`.`BuyerID`
            INNER JOIN ".$partner_companies." ON `pbc`.`CompanyID` = `pc`.`CompanyID`
            INNER JOIN ".$X_lead_data." ON `ld`.`LeadID` = `caor`.`LeadID`
            WHERE `caor`.`DirectPostDate` BETWEEN '".$StartDate."' AND '".$EndDate."'
            AND `caor`.`TestMode` = '1'
            AND `caor`.`DirectPostStatus` = '0' ";
            if($BuyerCompanyID!="All"){
                $sql .= "AND `pc`.`CompanyID` = ".$BuyerCompanyID." ";
            }if($VendorCompanyID!= 'All' && $VendorID == 'All'){
                $sql .= "AND `caor`.`VendorID` IN (select VendorID  from partner_vendor_campaigns where CompanyID = ".$VendorCompany.")";
            }if($VendorID!="All"){
                $sql .= "AND `caor`.`VendorID` = ".$VendorID." ";
            }           
            if($TierID!="All"){
                $sql .= "AND `caor`.`Tier` = '".$TierID."' ";
            }
            $sql .=" GROUP BY `caor`.`BuyerID`";
            //Count ReturndLeads
            $sql .= "UNION
            SELECT `pbc`.`CompanyID`, `pc`.`CompanyName`, `pbc`.`AdminLabel`,`pbc`.`FixedPrice`,`pbc`.`BuyerID`, COUNT(*) AS Attempted,
            0 AS Accepted,
            0 AS Total,
            0 AS Redirected,
            COUNT(*) AS LeadsReturned,
            SUM(DirectPostPrice) AS DirectPostPrice
            FROM ".$X_outbound_requests."
            INNER JOIN ".$partner_buyer_campaigns." ON `caor`.`BuyerID` = `pbc`.`BuyerID`
            INNER JOIN ".$partner_companies." ON `pbc`.`CompanyID` = `pc`.`CompanyID`
            INNER JOIN ".$X_lead_data." ON `ld`.`LeadID` = `caor`.`LeadID`
            WHERE `caor`.`DirectPostDate` BETWEEN '".$StartDate."' AND '".$EndDate."'
            AND `caor`.`TestMode` = '1'
            AND `caor`.`DirectPostStatus` = '2' ";
            if($BuyerCompanyID!="All"){
                $sql .= "AND `pc`.`CompanyID` = ".$BuyerCompanyID." ";
            }if($VendorCompanyID!= 'All' && $VendorID == 'All'){
                $sql .= "AND `caor`.`VendorID` IN (select VendorID  from partner_vendor_campaigns where CompanyID = ".$VendorCompany.")";
            }if($VendorID!="All"){
                $sql .= "AND `caor`.`VendorID` = ".$VendorID." ";
            }           
            if($TierID!="All"){
                $sql .= "AND `caor`.`Tier` = '".$TierID."' ";
            }
            $sql .=" GROUP BY `caor`.`BuyerID`";

            $sql .=") AS ans ";
            if($ShowAll=='0'){
                $sql .= "WHERE Total != '0.00' ";
            }
            $sql .=" GROUP BY BuyerID, CompanyID, CompanyName, FixedPrice, AdminLabel
            ORDER BY CompanyName, FixedPrice";
        }
        else if($Campaigns->CampaignType=="PingPost")
        {
            $sql = "SELECT CompanyID, CompanyName, AdminLabel, FixedPrice, BuyerID, SUM(Attempted) AS Attempted,
            SUM(Accepted) AS Accepted, (SUM(Accepted) /   SUM(Attempted)) * 100 AS AcceptPercent,
            SUM(Total) AS Total,SUM(LeadsReturned) AS LeadsReturned, SUM(PostPrice) AS ReturnedPrice,
            (SUM(Total) + SUM(PostPrice)) AS SoldAmount,
            (SUM(Attempted) / FixedPrice) AS EPC
            FROM (
            SELECT `pbc`.`CompanyID`, `pc`.`CompanyName`,`pbc`.`AdminLabel`,`pbc`.`FixedPrice`,`pbc`.`BuyerID`, COUNT(*) AS Attempted,
            SUM(IF(caor.PostStatus='1',1,0)) AS Accepted,
            SUM(caor.PostPrice) AS Total,
            '0' AS LeadsReturned,
            '0' AS PostPrice
            FROM ".$X_outbound_requests." INNER JOIN ".$partner_buyer_campaigns." ON `caor`.`BuyerID` = `pbc`.`BuyerID`
            INNER JOIN ".$partner_companies." ON `pbc`.`CompanyID` = `pc`.`CompanyID`
            INNER JOIN ".$X_lead_data." ON `ld`.`LeadID` = `caor`.`LeadID`
            WHERE `caor`.`PostDate` BETWEEN '".$StartDate."' AND '".$EndDate."'
            AND `caor`.`TestMode` = '1'
            AND `caor`.`PostStatus` = '1' ";
            if($BuyerCompanyID!="All"){
                $sql .= "AND `pc`.`CompanyID` = ".$BuyerCompanyID." ";
            }if($VendorCompanyID!= 'All' && $VendorID == 'All'){
                $sql .= "AND `caor`.`VendorID` IN (select VendorID  from partner_vendor_campaigns where CompanyID = ".$VendorCompany.")";
            }if($VendorID!="All"){
                $sql .= "AND `caor`.`VendorID` = ".$VendorID." ";
            }           
            if($TierID!="All"){
                $sql .= "AND `caor`.`Tier` = '".$TierID."' ";
            }
            $sql .= " GROUP BY `caor`.`BuyerID`
            UNION
            SELECT `pbc`.`CompanyID`, `pc`.`CompanyName`,`pbc`.`AdminLabel`,`pbc`.`FixedPrice`,`pbc`.`BuyerID`, COUNT(*) AS Attempted,
            0 AS Accepted,
            0 AS Total,
            '0' AS LeadsReturned,
            '0' AS PostPrice
            FROM ".$X_outbound_requests." INNER JOIN ".$partner_buyer_campaigns." ON `caor`.`BuyerID` = `pbc`.`BuyerID`
            INNER JOIN ".$partner_companies." ON `pbc`.`CompanyID` = `pc`.`CompanyID`
            INNER JOIN ".$X_lead_data." ON `ld`.`LeadID` = `caor`.`LeadID`
            WHERE `caor`.`PostDate` BETWEEN '".$StartDate."' AND '".$EndDate."'
            AND `caor`.`TestMode` = '1'
            AND `caor`.`PostStatus` = '0' ";
             if($BuyerCompanyID!="All"){
                $sql .= "AND `pc`.`CompanyID` = ".$BuyerCompanyID." ";
            }if($VendorCompanyID!= 'All' && $VendorID == 'All'){
                $sql .= "AND `caor`.`VendorID` IN (select VendorID  from partner_vendor_campaigns where CompanyID = ".$VendorCompany.")";
            }if($VendorID!="All"){
                $sql .= "AND `caor`.`VendorID` = ".$VendorID." ";
            }           
            if($TierID!="All"){
                $sql .= "AND `caor`.`Tier` = '".$TierID."' ";
            }
            $sql .= "  GROUP BY `caor`.`BuyerID`";
            //Count LeadsReturned
            $sql .= "UNION
            SELECT `pbc`.`CompanyID`, `pc`.`CompanyName`,`pbc`.`AdminLabel`,`pbc`.`FixedPrice`,`pbc`.`BuyerID`, COUNT(*) AS Attempted,
            0 AS Accepted,
            0 AS Total,
            COUNT(*) AS LeadsReturned,
            SUM(PostPrice) AS PostPrice
            FROM ".$X_outbound_requests." INNER JOIN ".$partner_buyer_campaigns." ON `caor`.`BuyerID` = `pbc`.`BuyerID`
            INNER JOIN ".$partner_companies." ON `pbc`.`CompanyID` = `pc`.`CompanyID`
            INNER JOIN ".$X_lead_data." ON `ld`.`LeadID` = `caor`.`LeadID`
            WHERE `caor`.`PostDate` BETWEEN '".$StartDate."' AND '".$EndDate."'
            AND `caor`.`TestMode` = '1'
            AND `caor`.`PostStatus` = '2' ";
            if($BuyerCompanyID!="All"){
                $sql .= "AND `pc`.`CompanyID` = ".$BuyerCompanyID." ";
            }if($VendorCompanyID!= 'All' && $VendorID == 'All'){
                $sql .= "AND `caor`.`VendorID` IN (select VendorID  from partner_vendor_campaigns where CompanyID = ".$VendorCompany.")";
            }if($VendorID!="All"){
                $sql .= "AND `caor`.`VendorID` = ".$VendorID." ";
            }           
            if($TierID!="All"){
                $sql .= "AND `caor`.`Tier` = '".$TierID."' ";
            }
            $sql .= "  GROUP BY `caor`.`BuyerID`";
            $sql .=") AS ans ";
            if($ShowAll=='0'){
                $sql .= "WHERE Total != '0.00' ";
            }
            $sql .=" GROUP BY BuyerID, CompanyID, CompanyName, FixedPrice, AdminLabel
            ORDER BY CompanyName, FixedPrice";
        }
        
        $results = DB::select($sql);
        return $results;
        
    }

}
