<?php
namespace App\Http\Models;
use Illuminate\Database\Eloquent\Model;
use DB;
use Session;
class CampaignManagementModel extends Model 
{
    public function GetCampaignType()
    {
        $CampaignType = DB::table('master_campaigns')
                            ->select('*')
                            ->groupBy('CampaignType')
                            ->orderBy('CampaignType','ASC')
                            ->get();
        return $CampaignType;
    }
	public function GetCampaignList($Search)
	{
		$Users['Count'] = "";
		$Users['Res'] 	= "";

		$CampaignName       = $Search['CampaignName'];
        $CampaignType       = $Search['CampaignType'];
        $MaintenanceMode    = $Search['MaintenanceMode'];
        $NumOfRecords       = $Search['NumOfRecords'];
        $Start 	            = $Search['Start'];
                
		$GetCampaignList = DB::table('master_campaigns as mc')
						->select('mc.*');
		if($CampaignName != "") 
		{
			$GetCampaignList->where('mc.CampaignName','like',"%$CampaignName%");
        }
        if($CampaignType != "") 
        {
            $GetCampaignList->where('mc.CampaignType',$CampaignType);
        }
        if($MaintenanceMode != "") 
        {
            $GetCampaignList->where('mc.MaintenanceMode',$MaintenanceMode);
        }
            
        $Campaign['Count'] = $GetCampaignList->count();
	   	$Campaign['Res'] = $GetCampaignList
					->orderBy('mc.CampaignName','ASC')
					->offset($Start)
            		->limit($NumOfRecords)
	                ->get()
	                ->toArray();
        
		return $Campaign;
  	}

    public function AddCampaignDetails($Details,$Specs)
    {
        $Add = DB::table('master_campaigns')->insertGetId($Details);
        if($Add)
        {
            $Details=array('CampaignID'=>$Add,
                            'Specs'=>$Specs);
            $this->AddCampaignSpecs($Details);
            return true;
        }
        else 
        {
          return false;  
        }
    }
    public function AddCampaignSpecs($Details)
    {
        $AddCampaignSpecs = DB::table('campaign_specs')->insert($Details);
        return true;
    }

    public function SaveCampaignDetails($CampaignID,$Details,$Specs)
    {
        $Update=DB::table('master_campaigns')
                    ->where('CampaignID',$CampaignID)
                    ->update($Details);
        if($Specs!="")
        {            
            $SpecsDetails=array("Specs"=>$Specs);
            $this->UpdateCampaignSpecs($CampaignID,$SpecsDetails);
        }
        return true;
    }

    public function GetCampaignSpecs($CampaignID)
    {
        $GetCampaignSpecs = DB::table('campaign_specs')
                ->select('*')
                ->where('CampaignID',$CampaignID)
                ->first();
        return $GetCampaignSpecs;
    }
    public function GetCampaignDetail($CampaignID)
    {
        $GetCampaignDetail = DB::table('master_campaigns')
                ->select('*')
                ->where('CampaignID',$CampaignID)
                ->first();
        return $GetCampaignDetail;
    }

    public function UpdateCampaignSpecs($CampaignID,$Details)
    {
        $Add="";
        $Add=DB::table('campaign_specs')
                ->where('CampaignID',$CampaignID)
                ->update($Details);
        return true;
    }
    public function ChangeMaintenanceMode($CampaignID,$Details)
    {
        $Change=DB::table('master_campaigns')
                ->where('CampaignID',$CampaignID)
                ->update($Details);
        if($Change)
        {            
            return true;
        }
        else 
        {
          return false;  
        }
    }
    public function ChangeShowInDashboardMode($CampaignID,$Details)
    {
        $Change=DB::table('master_campaigns')
                ->where('CampaignID',$CampaignID)
                ->update($Details);
        if($Change)
        {            
            return true;
        }
        else 
        {
          return false;  
        }
    }
    
}
