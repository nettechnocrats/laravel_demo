<?php
namespace App\Http\Models;
use Illuminate\Database\Eloquent\Model;
use DB;
use Session;
class CompanyManagementModel extends Model 
{
    public function StatesList()
    {
        $StateList=DB::table('zipcodes')
                        ->groupBy('State')
                        ->get();
        return $StateList;
    }
    public function CountryList()
    {
        $CountryList=DB::table('country_list')
                        ->get();
        return $CountryList;
    }
    public function GetStateDetails($ZipCode)
    {
        $Details=DB::table('zipcodes')
                        ->where('ZipCode',$ZipCode)
                        ->first();
        
        return $Details;
    }
	public function GetCompanyList($Search)
	{
		$Users['Count'] = "";
		$Users['Res'] 	= "";

        $CompanyName        = $Search['CompanyName'];
		$CompanyTel         = $Search['CompanyTel'];
        $CompanyStatus      = $Search['CompanyStatus'];
        $CompanyTestMode    = $Search['CompanyTestMode'];
        $NumOfRecords       = $Search['NumOfRecords'];
        $Start 	            = $Search['Start'];
                

        $StartDate      = date('Y-m-d',strtotime($Search['StartDate'])).' 00:00:00';
        $EndDate        = date('Y-m-d',strtotime($Search['EndDate'])).' 23:59:59';

        //DB::enableQueryLog();
		$GetCompanyList = DB::table('partner_companies as mc')
						->select('mc.*');
		if($CompanyName != "") 
		{
			$GetCompanyList->where('mc.CompanyName','like',"%$CompanyName%");
        }
        if($CompanyTel != "") 
        {
            $GetCompanyList->where('mc.CompanyTel','like',"%$CompanyTel%");
        }
        if($CompanyStatus != "") 
        {
            $GetCompanyList->where('mc.CompanyStatus',$CompanyStatus);
        }
        if($CompanyTestMode != "") 
        {
            $GetCompanyList->where('mc.CompanyTestMode',$CompanyTestMode);
        }
        $GetCompanyList->whereBetween('mc.CreatedDateTime', array($StartDate, $EndDate));   
        $Campaign['Count'] = $GetCompanyList->count();
	   	$Campaign['Res'] = $GetCompanyList
					->orderBy('mc.CompanyName','ASC')
					->offset($Start)
            		->limit($NumOfRecords)
	                ->get()
	                ->toArray();
        //$queries = DB::getQueryLog();
        //dd($queries);
		return $Campaign;
  	}

    public function AddCompanyDetails($Details)
    {
         
        $CompanyDetails  = $Details['CompanyDetails'];
        $UserDetails    = $Details['UserDetails'];
        $VendorDetails  = $Details['VendorDetails'];
        $BuyerDetails   = $Details['BuyerDetails'];
        
        $CompanyID = DB::table('partner_companies')->insertGetId($CompanyDetails);
        if($CompanyID)
        {
            $UserDetails['CompanyID'] = $CompanyID;
            DB::table('partner_users')->insert($UserDetails);            
            if(!empty($VendorDetails))
            {
                $VendorDetails['CompanyID'] = $CompanyID;  
                DB::table('partner_vendor_campaigns')->insert($VendorDetails);              
            }
            if(!empty($BuyerDetails))
            {
                $BuyerDetails['CompanyID'] = $CompanyID;
                DB::table('partner_buyer_campaigns')->insert($BuyerDetails);                
            }
            return true;
        }
        else 
        {
          return false;  
        }
    }
    
    public function SaveCompanyDetails($CompanyID,$Details)
    {
        $Update=DB::table('partner_companies')
                    ->where('CompanyID',$CompanyID)
                    ->update($Details);
        
        return true;
    }

    public function GetCompanyDetail($CompanyID)
    {
        $GetCampaignDetail = DB::table('partner_companies')
                ->select('*')
                ->where('CompanyID',$CompanyID)
                ->first();
        return $GetCampaignDetail;
    }
    public function GetUsersList($CompanyID)
    {
        $GetUsersList = DB::table('partner_users')
                ->select('*')
                ->where('CompanyID',$CompanyID)
                ->get()
                ->toArray();
        return $GetUsersList;
    } 
    public function GetVendorsList($CompanyID)
    {
        $GetUsersList = DB::table('partner_vendor_campaigns')
                ->select('*')
                ->where('CompanyID',$CompanyID)
                ->get()
                ->toArray();
        return $GetUsersList;
    } 
    public function GetBuyersList($CompanyID)
    {
        $GetUsersList = DB::table('partner_buyer_campaigns')
                ->select('*')
                ->where('CompanyID',$CompanyID)
                ->get()
                ->toArray();
        return $GetUsersList;
    } 
    public function ChangeCompanyTestMode($CompanyID,$Details)
    {
        $Change=DB::table('partner_companies')
                ->where('CompanyID',$CompanyID)
                ->update($Details);
        if($Change)
        {            
            return true;
        }
        else 
        {
          return false;  
        }
    }
    public function ChangeCompanyStatus($CompanyID,$Details)
    {
        $Change=DB::table('partner_companies')
                ->where('CompanyID',$CompanyID)
                ->update($Details);
        if($Change)
        {            
            return true;
        }
        else 
        {
          return false;  
        }
    }
    public function IsUserEmailExist($UserEmail)
    {
        $IsUserEmailExist = DB::table('partner_users')
                ->select('*')
                ->where('UserEmail',$UserEmail)
                ->count();
        return $IsUserEmailExist;
    }
    public function IsCompanyNameExist($CompanyName)
    {
        $IsCompanyNameExist = DB::table('partner_companies')
                ->select('*')
                ->where('CompanyName',$CompanyName)
                ->count();
        return $IsCompanyNameExist;
    }
    public function IsBuyerNameExist($BuyerName)
    {
        $IsBuyerNameExist = DB::table('partner_buyer_campaigns')
                ->select('*')
                ->where('AdminLabel',$BuyerName)
                ->count();
        return $IsBuyerNameExist;
    }
    public function IsVendorNameExist($VendorName)
    {
        $IsVendorNameExist = DB::table('partner_vendor_campaigns')
                ->select('*')
                ->where('AdminLabel',$VendorName)
                ->count();
        return $IsVendorNameExist;
    }
}
