<?php
namespace App\Http\Models;
use Illuminate\Database\Eloquent\Model;
use DB;
use Session;
class ErrorLogModel extends Model 
{
    public function GetErrorLogList($Search)
    {
        $Start          = $Search['Start'];
        $NumOfRecords   = $Search['NumOfRecords'];
        
        $GetErrorLogList = DB::table('error_log')
                        ->select('*');

        $ErrorLog['Count'] = count($GetErrorLogList->get());
        
        $ErrorLog['Res'] = $GetErrorLogList
                    ->orderBy('DateTime','DESC')
                    ->offset($Start)
                    ->limit($NumOfRecords)
                    ->get()
                    ->toArray();
        return $ErrorLog;

    }
}
