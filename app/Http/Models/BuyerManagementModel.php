<?php
namespace App\Http\Models;
use Illuminate\Database\Eloquent\Model;
use DB;
use Session;
class BuyerManagementModel extends Model 
{
    public function CampaignDropDown()
    {
        $CampaignDropDown = DB::table('master_campaigns')
                    ->select('CampaignID','CampaignName')
                    ->where('CampaignID', '!=','0' )
                    ->where('MaintenanceMode', '!=','1' )
                    ->orderBy('CampaignName','ASC')
                    ->get();

        return $CampaignDropDown;
    }
	public function GetBuyerList($Search)
	{
		$Users['Count'] = "";
		$Users['Res'] 	= "";
        $ShowInactive   = $Search['ShowInactive'];
        $ShowTest       = $Search['ShowTest'];

        $CompanyID      = $Search['CompanyID'];
		$CampaignID 	= $Search['CampaignID'];
        $BuyerName      = $Search['BuyerName'];
        $Status         = $Search['Status'];
        $BuyerTestMode 	= $Search['BuyerMode'];

        $Start 		    = $Search['Start'];
        $NumOfRecords 	= $Search['NumOfRecords'];
                
		$GetBuyerList = DB::table('partner_buyer_campaigns as pbc')
						->select('pbc.*','mt.CampaignName','pc.CompanyName')
                        ->join('partner_companies as pc','pbc.CompanyID','=','pc.CompanyID')
                        ->join('master_campaigns as mt','mt.CampaignID','=','pbc.CampaignID');
		if ($CompanyID != "" && $CompanyID != "All") 
        {
             $GetBuyerList->where('pbc.CompanyID', $CompanyID );  
        }
        if ($CampaignID != "") 
		{
			$GetBuyerList->where('pbc.CampaignID', $CampaignID );
        }
        if ($BuyerName != "") 
        {
            $GetBuyerList->where('pbc.AdminLabel','like','%' . $BuyerName. '%');
        }
        if ($BuyerTestMode != "") 
        {
            $GetBuyerList->where('pbc.BuyerTestMode', $BuyerTestMode );
        }
        if ($Status != "") 
        {
            $GetBuyerList->where('pbc.BuyerStatus', $Status );
        }
        
        if($ShowInactive=='1' && $ShowTest=='0')
        {
            $GetBuyerList->where('pc.CompanyStatus','=',0);
            $GetBuyerList->where('pc.CompanyTestMode','=',0);
        }
        else if($ShowInactive=='0' && $ShowTest=='1')
        {
            $GetBuyerList->where('pc.CompanyStatus','=',1);
            $GetBuyerList->where('pc.CompanyTestMode','=',1);
        }
        else if($ShowInactive=='0' && $ShowTest=='0')
        {
            $GetBuyerList->where('pc.CompanyStatus','=',1);
            $GetBuyerList->where('pc.CompanyTestMode','=',0);
        }
         else if($ShowInactive=='1' && $ShowTest=='1')
        {
            $GetBuyerList->where('pc.CompanyStatus','=',0);
            $GetBuyerList->where('pc.CompanyTestMode','=',1);
        }
        
        $Users['Count'] = $GetBuyerList->count();
	   	$Users['Res'] = $GetBuyerList->orderBy('pc.CompanyName','ASC')
                    ->orderBy('pbc.PartnerLabel','ASC')
                    ->orderBy('pbc.BuyerID','ASC')
					->offset($Start)
            		->limit($NumOfRecords)
	                ->get()
	                ->toArray();
        
		return $Users;
  	}

    public function BuyerDetails($BuyerID)
    {
        $BuyerDetails = DB::table('partner_buyer_campaigns as pbc')
                        ->select('pbc.*','mt.CampaignName')
                        ->join('master_campaigns as mt','mt.CampaignID','=','pbc.CampaignID')
                        ->where('pbc.BuyerID', $BuyerID)
                        ->first();

        return $BuyerDetails;
    }

    public function SaveBuyerDetails($BuyerID,$Details)
    {
        $SaveBuyerDetails = DB::table('partner_buyer_campaigns')
                ->where('BuyerID',$BuyerID)
                ->update($Details);
        return true;
    }

     public function AddBuyerDetails($Details)
    {
        $AddBuyerDetails = DB::table('partner_buyer_campaigns')
                ->insert($Details);
        if($AddBuyerDetails)
        {
            return true;            
        }
        else
        {
            return false;                        
        }
    }
   

}
