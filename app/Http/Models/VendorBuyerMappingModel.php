<?php
namespace App\Http\Models;
use Illuminate\Database\Eloquent\Model;
use DB;
use session;

class VendorBuyerMappingModel extends Model
{
    public function GetAllCampaign()
    {
        $GetAllCampaign=array();
        $data = DB::table('master_campaigns')
                    ->select('CampaignID','CampaignName')
                    ->where('CampaignID', '!=','0' )
                    ->where('MaintenanceMode', '!=','1' )
                    ->get();

        foreach($data as $d)
        {
           $GetAllCampaign[$d->CampaignID]=$d->CampaignName;
        }
        return $GetAllCampaign;
    }
    ///////////listing
    public function GetAllMapping($Searcher)
    {
        $VendorID           = $Searcher['vendor_id'];
        $VendorCompanyID    = $Searcher['vendor_company_id'];
        $ShowInactiveVendor = $Searcher['ShowInactiveVendor'];
        $ShowTestVendor     = $Searcher['ShowTestVendor'];
        $CampaignID         = $Searcher['Campaign'];
       // $Tier               = $Searcher['buyer_tier_id'];
        $SortOrderClause    = $Searcher['sort_order_clause'];
        $BuyerID            = $Searcher['buyer_id'];

        $sql='select vbm.*,
                pvc.AdminLabel as VendorAdminLabel,
                pbc.AdminLabel as BuyerAdminLabel,
                pbc.FixedPrice as FixedPrice,
                pcv.CompanyID as VendorCompanyID,
                pcb.CompanyID as BuyerCompanyID,
                pcb.CompanyStatus as CompanyStatus,
                pvc.VendorID as VendorID,
                pbc.BuyerID as BuyerID,
                pbc.BuyerStatus as BuyerStatus
                from vendor_buyer_mapping as vbm
                join partner_vendor_campaigns as pvc on vbm.VendorID = pvc.VendorID
                join partner_buyer_campaigns as pbc on vbm.BuyerID = pbc.BuyerID
                join partner_companies as pcv on pcv.CompanyID = pvc.CompanyID 
                join master_tiers as mt on mt.TierID = vbm.Tier';

        $sql.= ' join partner_companies as pcb on pcb.CompanyID = pbc.CompanyID ';

        if($ShowInactiveVendor=='1' && $ShowTestVendor=='0')
        {
            $sql.= ' AND pcv.CompanyTestMode = 0 ';
        }
        else if($ShowInactiveVendor=='0' && $ShowTestVendor=='1')
        {
            $sql.= ' AND pcv.CompanyStatus = 1 ';
        }
        else if($ShowInactiveVendor=='0' && $ShowTestVendor=='0')
        {
            $sql.= ' AND pcv.CompanyStatus = 1 ';
            $sql.= ' AND pcv.CompanyTestMode = 0 ';
        }
        $sql.=' where 1 ';

        if($VendorCompanyID!='All' && $VendorCompanyID!='')
        {
            $sql.=' AND pcv.CompanyID = '.$VendorCompanyID.'';
        }

        if($VendorID!="")
        {
            $sql.=' AND vbm.VendorID = '.$VendorID.'';
        }/*
        if($Tier!="")
        {
            $sql.=' AND vbm.Tier = "'.$Tier.'"';
        }
        if($BuyerID!="")
        {
            $sql.=' AND pbc.BuyerID = '.$BuyerID.'';
        }*/
        if($CampaignID == '1') {
            $sql.=' order by mt.TierPayout DESC, vbm.BuyerSortOrder ASC, pbc.FixedPrice DESC, pbc.AdminLabel ASC';
        } else {
            $sql.=' order by pbc.AdminLabel ASC';
        }

        $results = DB::select($sql);
        return $results;
    }
    public static function VendorList($CampaignID,$CompanyID,$ShowInactiveVendor,$ShowTestVendor,$ShowInactiveVendorList,$ShowTestVendorList)
    {
        $data = DB::table('partner_vendor_campaigns as pvc')
                    ->join('partner_companies as pc', 'pvc.CompanyID', '=', 'pc.CompanyID')
                    ->where('pvc.CampaignID',$CampaignID);
        //Logic For Company ID
        if($CompanyID!='All' && $CompanyID!='')
        {
            $data->where('pvc.CompanyID',$CompanyID);
        }
        //Logic With Company DropDown Show Status
        if($ShowInactiveVendor=='1' && $ShowTestVendor=='0')
        {
            $data->where('pc.CompanyStatus','=',0);
            $data->where('pc.CompanyTestMode','=',0);
        }
        else if($ShowInactiveVendor=='0' && $ShowTestVendor=='1')
        {
            $data->where('pc.CompanyStatus','=',1);
            $data->where('pc.CompanyTestMode','=',1);
        }
        else if($ShowInactiveVendor=='0' && $ShowTestVendor=='0')
        {
            $data->where('pc.CompanyStatus','=',1);
            $data->where('pc.CompanyTestMode','=',0);
        }
         else if($ShowInactiveVendor=='1' && $ShowTestVendor=='1')
        {
            $data->where('pc.CompanyStatus','=',0);
            $data->where('pc.CompanyTestMode','=',1);
        }
        //Logic With List Show Status
        if($ShowInactiveVendorList=='1' && $ShowTestVendorList=='0')
        {
            $data->where('pvc.VendorStatus','=',0);
            $data->where('pvc.VendorTestMode','=',0);
        }
        else if($ShowInactiveVendorList=='0' && $ShowTestVendorList=='1')
        {
            $data->where('pvc.VendorStatus','=',1);
            $data->where('pvc.VendorTestMode','=',1);
        }
        else if($ShowInactiveVendorList=='0' && $ShowTestVendorList=='0')
        {
            $data->where('pvc.VendorStatus','=',1);
            $data->where('pvc.VendorTestMode','=',0);
        }
        else if($ShowInactiveVendorList=='1' && $ShowTestVendorList=='1')
        {
            $data->where('pvc.VendorStatus','=',0);
            $data->where('pvc.VendorTestMode','=',1);
        }
        $data->select('pvc.VendorID','pvc.AdminLabel','pvc.CompanyID');
        $data->orderBy('pvc.AdminLabel','ASC');
        $res =$data->get();
        return $res;
    }
    public static function BuyerList($CompanyID,$ShowInactiveBuyer,$ShowTestBuyer,$ShowInactiveBuyerList,$ShowTestBuyerList)
    {
        $data = DB::table('partner_buyer_campaigns as pbc')
                        ->join('partner_companies as pc', 'pbc.CompanyID', '=', 'pc.CompanyID');
        //Logic For Company ID
        if($CompanyID!='All' && $CompanyID!='')
        {
            $data->where('pbc.CompanyID',$CompanyID);
        }
        //Logic With Company DropDown Show Status
        if($ShowInactiveBuyer=='1' && $ShowTestBuyer=='0')
        {
            $data->where('pc.CompanyTestMode','=',0);
        }
        else if($ShowInactiveBuyer=='0' && $ShowTestBuyer=='1')
        {
            $data->where('pc.CompanyStatus','=',1);
        }
        else if($ShowInactiveBuyer=='0' && $ShowTestBuyer=='0')
        {
            $data->where('pc.CompanyStatus','=',1);
            $data->where('pc.CompanyTestMode','=',0);
        }
        //Logic With List Show Status
        if($ShowInactiveBuyerList=='1' && $ShowTestBuyerList=='0')
        {
            $data->where('pbc.BuyerTestMode','=',1);
        }
        else if($ShowInactiveBuyerList=='0' && $ShowTestBuyerList=='1')
        {
            $data->where('pbc.BuyerStatus','=',1);
        }
        else if($ShowInactiveBuyerList=='0' && $ShowTestBuyerList=='0')
        {
            $data->where('pbc.BuyerStatus','=',1);
            $data->where('pbc.BuyerTestMode','=',1);
        }
        $data->select('pbc.BuyerID','pbc.AdminLabel','pbc.CompanyID');
        $res =$data->get();
        return $res;
    }

    public static function VendorCompanyList()
    {
        $getVendorCompany=array();
        $data = DB::table('partner_vendor_campaigns as pvc')
                    ->join('partner_companies as pc', 'pvc.CompanyID', '=', 'pc.CompanyID')
                    ->where('CompanyStatus','=',1)
                    ->where('CompanyTestMode','=',0)
                    ->orderBy('CompanyName','ASC')
                    ->select('pc.CompanyID','pc.CompanyName')
                    ->get();
        foreach($data as $d)
        {
           $getVendorCompany[$d->CompanyID]=$d->CompanyName;
        }
        return $getVendorCompany;
    }

    public static function BuyerCompanyList($CampaignID)
    {
        $getBuyerCompany=array();
        $data = DB::table('partner_buyer_campaigns as pbc')
                    ->join('partner_companies as pc', 'pbc.CompanyID', '=', 'pc.CompanyID')
                    ->where('pbc.CampaignID',$CampaignID)
                    ->where('pc.CompanyStatus','=',1)
                    ->where('pc.CompanyTestMode','=',0)
                    ->orderBy('CompanyName','ASC')
                    ->select('pc.CompanyID','pc.CompanyName')
                    ->get();
        foreach($data as $d)
        {
           $getBuyerCompany[$d->CompanyID]=$d->CompanyName;
        }
        return $getBuyerCompany;
    }

    public function BuyerTierList()
    {
       $getTier=array();
        $data = DB::table('master_tiers')
                    ->select('TierID','TierPayout')
                    ->orderBy('TierPayout','ASC')
                    ->get();
        foreach($data as $d)
        {
           $getTier[$d->TierID]=$d->TierPayout;
        }
        return $getTier;
    }

    public function AddMappingDetails($Details)
    {
        $Add=DB::table('vendor_buyer_mapping')->insert($Details);
        if($Add)
        {
          return true;
        }
        else
        {
          return false;
        }
    }
    public function MappingDetails($MappingID)
    {
        $data = DB::table('vendor_buyer_mapping')
                    ->select('*')->where('ID',$MappingID)
                    ->first();
        return $data;
    }

    public function EditMappingDetails($MappingID,$Details)
    {
        $Update=DB::table('vendor_buyer_mapping')->where('ID',$MappingID)->update($Details);
        return true;
    }




    public static function GetSelectedVendorCompany($CampaignID,$ShowInactive,$ShowTest)
    {
        $getVendorCompany=array();
        $data = DB::table('partner_vendor_campaigns as pvc')
                    ->join('partner_companies as pc', 'pvc.CompanyID', '=', 'pc.CompanyID')
                    ->where('pvc.CampaignID',$CampaignID);
        if($ShowInactive=='1' && $ShowTest=='0')
        {
            $data->where('pc.CompanyStatus','=',0);
            $data->where('pc.CompanyTestMode','=',0);
        }
        else if($ShowInactive=='0' && $ShowTest=='1')
        {
            $data->where('pc.CompanyStatus','=',1);
            $data->where('pc.CompanyTestMode','=',1);
        }
        else if($ShowInactive=='0' && $ShowTest=='0')
        {
            $data->where('pc.CompanyStatus','=',1);
            $data->where('pc.CompanyTestMode','=',0);
        }
        else if($ShowInactive=='1' && $ShowTest=='1')
        {
            $data->where('pc.CompanyStatus','=',0);
            $data->where('pc.CompanyTestMode','=',1);
        }
        $data->orderBy('pc.CompanyName','ASC')
                    ->select('pc.CompanyID','pc.CompanyName');

        $res =$data->get();
        foreach($res as $d)
        {
           $getVendorCompany[$d->CompanyID]=$d->CompanyName;
        }
        return $getVendorCompany;
    }
    public static function GetSelectedVendorCompany2($ShowInactive,$ShowTest,$CampaignID)
    {
        $getVendorCompany=array();
        $data = DB::table('partner_vendor_campaigns as pvc')
                    ->join('partner_companies as pc', 'pvc.CompanyID', '=', 'pc.CompanyID')
                    ->where('pvc.CampaignID',$CampaignID);
        if($ShowInactive=='1' && $ShowTest=='0')
        {
            $data->where('pc.CompanyStatus','=',0);
            $data->where('pc.CompanyTestMode','=',0);
        }
        else if($ShowInactive=='0' && $ShowTest=='1')
        {
            $data->where('pc.CompanyStatus','=',1);
            $data->where('pc.CompanyTestMode','=',1);
        }
        else if($ShowInactive=='0' && $ShowTest=='0')
        {
            $data->where('pc.CompanyStatus','=',1);
            $data->where('pc.CompanyTestMode','=',0);
        }
        else if($ShowInactive=='1' && $ShowTest=='1')
        {
            $data->where('pc.CompanyStatus','=',0);
            $data->where('pc.CompanyTestMode','=',1);
        }
        $data->orderBy('pc.CompanyName','ASC')
                    ->select('pc.CompanyID','pc.CompanyName');

        $res =$data->get();
        foreach($res as $d)
        {
           $getVendorCompany[$d->CompanyID]=$d->CompanyName;
        }
        return $getVendorCompany;
    }
    public static function GetSelectedBuyerCompany($CampaignID,$ShowInactive,$ShowTest)
    {
        $getBuyerCompany=array();
        $data = DB::table('partner_buyer_campaigns as pbc')
                    ->join('partner_companies as pc', 'pbc.CompanyID', '=', 'pc.CompanyID')
                    ->where('pbc.CampaignID',$CampaignID);
        if($ShowInactive=='1' && $ShowTest=='0')
        {
            $data->where('pc.CompanyStatus','=',0);
            $data->where('pc.CompanyTestMode','=',0);
        }
        else if($ShowInactive=='0' && $ShowTest=='1')
        {
            $data->where('pc.CompanyStatus','=',1);
            $data->where('pc.CompanyTestMode','=',1);
        }
        else if($ShowInactive=='0' && $ShowTest=='0')
        {
            $data->where('pc.CompanyStatus','=',1);
            $data->where('pc.CompanyTestMode','=',0);
        }
        else if($ShowInactive=='1' && $ShowTest=='1')
        {
            $data->where('pc.CompanyStatus','=',0);
            $data->where('pc.CompanyTestMode','=',1);
        }
        $data->orderBy('pc.CompanyName','ASC')
                    ->select('pc.CompanyID','pc.CompanyName');

        $res =$data->get();
        foreach($res as $d)
        {
           $getBuyerCompany[$d->CompanyID]=$d->CompanyName;
        }
        return $getBuyerCompany;
    }
    public function GetAdminLabel($VendorID)
    {
        $data = DB::table('partner_vendor_campaigns')
                    ->select('AdminLabel')
                    ->where('VendorID',$VendorID)
                    ->first();
        return $data->AdminLabel;
    }
    public function GetTierLabel($TierID)
    {
        $data = DB::table('master_tiers')
                    ->select('TierID')
                    ->where('TierID',$TierID)
                    ->first();
        return $data->TierID;
    }
    public function VendorListByCompanyID($companyID,$userRole)
    {
        $CompanyList=array();
        if($userRole!='1')
        {
            $data = DB::table('partner_vendor_campaigns')
                    ->select('*')
                    ->where('CompanyID',$companyID)
                    ->get();
        }
        else
        {
            $data = DB::table('partner_vendor_campaigns')
                    ->select('*')
                    ->get();
        }
        foreach($data as $c)
        {
            $CompanyList[$c->VendorID]=$c->AdminLabel;
        }
        return $CompanyList;
    }
    public function GetTierBG()
    {
        $Tier=array();
        $data = DB::table('master_tiers')
                    ->select('*')
                    ->get();
        $Color="lightgray";
        foreach($data as $c)
        {
            $Tier[$c->TierID]=$Color;
            $Color="white";
        }
        return $Tier;
    }
    public function GetTierC()
    {
        $Tier=array();
        $data = DB::table('master_tiers')
                    ->select('*')
                    ->get();
        $Color="white";
        foreach($data as $c)
        {
            $Tier[$c->TierID]=$Color;
            $Color="black";
        }
        return $Tier;
    }
    public function BuyerListByCompanyID($companyID,$userRole)
    {
        $CompanyList=array();
        if($userRole!='1')
        {
            $data = DB::table('partner_buyer_campaigns')
                    ->select('*')
                    ->where('CompanyID',$companyID)
                    ->get();
        }
        else
        {
            $data = DB::table('partner_buyer_campaigns')
                    ->select('*')
                    ->get();
        }
        foreach($data as $c)
        {
            $CompanyList[$c->BuyerID]=$c->AdminLabel;
        }
        return $CompanyList;
    }
    public function UpdatSortOrdersForSelectedMapping($SortOrders,$MappingIDs)
    {
        $countIDs=count($MappingIDs);
        $count=1;
        for($i=0;$i<$countIDs;$i++)
        {
            $Details=array('BuyerSortOrder'=>$SortOrders[$i]);
            DB::table('vendor_buyer_mapping')
                    ->where('ID',$MappingIDs[$i])
                    ->update($Details);

        }
        return true;
    }
    public function SortWithFixedPrice($VendorID) // VendorID, Tier
    {
        $data = DB::table('vendor_buyer_mapping AS vbm')
                    ->join('partner_buyer_campaigns AS pbc', 'pbc.BuyerID', '=', 'vbm.BuyerID')
                    ->join('partner_companies AS pc', 'pc.CompanyID', '=', 'pbc.CompanyID')
                    ->where('vbm.VendorID', $VendorID)
                    //->where('vbm.Tier', $TierID)
                    ->orderBy('vbm.Tier','DESC')
                    ->orderBy('pbc.FixedPrice','DESC')
                    ->orderBy('CompanyDefaultBuyerSortOrder','ASC')
                    ->orderBy('pbc.PayoutCalculation','ASC')
                    ->select("vbm.*")
                    ->get();
        $SortOrder=1;
        foreach ($data as $row)
        {
            $Details=array('BuyerSortOrder'=>$SortOrder);
            DB::table('vendor_buyer_mapping')
                    ->where('ID',$row->ID)
                    ->update($Details);
            $SortOrder++;
        }

        if(count($data)==$SortOrder-1)
        {
            return true;
        }
        else
        {
            return false;
        }

    }
    public function ChangeMappingStatus($MappingID,$Status)
    {
        $Details=array('MappingStatus'=>$Status);
        $Update=DB::table('vendor_buyer_mapping')
                ->where('ID',$MappingID)
                ->update($Details);
        if($Update)
        {
          return true;
        }
        else
        {
          return false;
        }
    }
    public function DeleteVendorBuyerMappingRow($MappingID)
    {
        $Del=DB::table('vendor_buyer_mapping')
            ->where('ID', '=', $MappingID)
            ->delete();
        if($Del)
        {
          return true;
        }
        else
        {
          return false;
        }
    }

    //////Phase 2
    public static function BuyerListForModal($CampaignID,$CompanyID,$ShowInactive,$ShowTest)
    {
        $data = DB::table('partner_buyer_campaigns as pbc')
                        ->join('partner_companies as pc', 'pbc.CompanyID', '=', 'pc.CompanyID')
                        ->where('pbc.CampaignID',$CampaignID);
        //Logic For Company ID
        if($CompanyID!='All' && $CompanyID!='')
        {
            $data->where('pbc.CompanyID',$CompanyID);
        }
        // Show Test Or Inactive
        if($ShowInactive=='1' && $ShowTest=='0')
        {
            $data->where('pc.CompanyStatus','=',0);
            $data->where('pc.CompanyTestMode','=',0);
            $data->where('pbc.BuyerStatus','=',0);
            $data->where('pbc.BuyerTestMode','=',0);
        }
        else if($ShowInactive=='0' && $ShowTest=='1')
        {
            $data->where('pc.CompanyStatus','=',1);
            $data->where('pc.CompanyTestMode','=',1);
            $data->where('pbc.BuyerStatus','=',1);
            $data->where('pbc.BuyerTestMode','=',1);
        }
        else if($ShowInactive=='0' && $ShowTest=='0')
        {
            $data->where('pc.CompanyStatus','=',1);
            $data->where('pc.CompanyTestMode','=',0);
            $data->where('pbc.BuyerStatus','=',1);
            $data->where('pbc.BuyerTestMode','=',0);
        }
        else if($ShowInactive=='1' && $ShowTest=='1')
        {
            $data->where('pc.CompanyStatus','=',0);
            $data->where('pc.CompanyTestMode','=',1);
            $data->where('pbc.BuyerStatus','=',0);
            $data->where('pbc.BuyerTestMode','=',1);
        }

        $data->select('pbc.BuyerID','pbc.FixedPrice','pbc.AdminLabel','pbc.CompanyID');
        $data->orderBy('pbc.AdminLabel','ASC');
        $res =$data->get();
        return $res;
    }
    public static function AllBuyerListForModal($CampaignID)
    {
        $data = DB::table('partner_buyer_campaigns as pbc')
                        ->join('partner_companies as pc', 'pbc.CompanyID', '=', 'pc.CompanyID')
                        ->where('pbc.CampaignID',$CampaignID)
                        ->where('pc.CompanyStatus',1)
                        ->where('pc.CompanyTestMode',0)
                        ->where('pbc.BuyerStatus',1)
                        ->where('pbc.BuyerTestMode',0);
        $data->select('pbc.BuyerID','pbc.FixedPrice','pbc.AdminLabel','pbc.CompanyID');
        $data->orderBy('pbc.AdminLabel','ASC');
        $res =$data->get();
        return $res;
    }
    public function GetTiersList($VendorID)
    {
        $data = DB::table('vendor_buyer_mapping')
                ->distinct()
                ->select('Tier')
                ->where('VendorID', '=', $VendorID)
                ->groupBy('Tier')
                ->get();

        return $data;
    }
    public function GetAllTiersList()
    {
        $data = DB::table('master_tiers')
                ->select('*')
                ->orderBy('TierPayout')
                ->get();

        return $data;
    }
    public function GetTierPayout($tier_id)
    {
         $data = DB::table('master_tiers')
                ->select('TierPayout')
                ->where('TierID', '=', $tier_id)
                ->first();

        return $data->TierPayout;
    }
    public function SaveNewMultipleMapping($VendorID,$TierID,$BuyerIDs)
    {
        $flag=1;
        foreach($BuyerIDs as $val)
        {
            $Details=array('VendorID'=>$VendorID,
                        'Tier'=>$TierID,
                        'BuyerID'=>$val,
                        'BuyerSortOrder'=>1,
                        'MappingStatus'=>1
                        );
            $data = DB::table('vendor_buyer_mapping')
                        ->select('*')
                        ->where('VendorID',$VendorID)
                        ->where('Tier',$TierID)
                        ->where('BuyerID',$val)
                        ->first();
            if(empty($data))
            {
                DB::table('vendor_buyer_mapping')->insert($Details);
                $flag = 1;
            } else {
                $flag = 0;
            }
        }
        if($flag==1){
            return true;
        } else {
            return false;
        }
        
    }

    //////////////clone Mapping/////////
    public static function VendorListForModal($CompanyID,$ShowInactiveVendor,$ShowTestVendor,$CampaignID)
    {
        $data = DB::table('partner_vendor_campaigns as pvc')
                    ->join('partner_companies as pc', 'pvc.CompanyID', '=', 'pc.CompanyID')
                    ->where('pvc.CampaignID',$CampaignID);
        //Logic For Company ID
        if($CompanyID!='All' && $CompanyID!='')
        {
            $data->where('pvc.CompanyID',$CompanyID);
        }
        //Logic With Company DropDown Show Status
        if($ShowInactiveVendor=='1' && $ShowTestVendor=='0')
        {
            $data->where('pc.CompanyStatus','=',0);
            $data->where('pc.CompanyTestMode','=',0);
            $data->where('pvc.VendorStatus','=',0);
            $data->where('pvc.VendorTestMode','=',0);
        }
        else if($ShowInactiveVendor=='0' && $ShowTestVendor=='1')
        {
            $data->where('pc.CompanyStatus','=',1);
            $data->where('pc.CompanyTestMode','=',1);
            $data->where('pvc.VendorStatus','=',1);
            $data->where('pvc.VendorTestMode','=',1);
        }
        else if($ShowInactiveVendor=='0' && $ShowTestVendor=='0')
        {
            $data->where('pc.CompanyStatus','=',1);
            $data->where('pc.CompanyTestMode','=',0);
            $data->where('pvc.VendorStatus','=',1);
            $data->where('pvc.VendorTestMode','=',0);
        }
        else if($ShowInactiveVendor=='1' && $ShowTestVendor=='1')
        {
            $data->where('pc.CompanyStatus','=',0);
            $data->where('pc.CompanyTestMode','=',1);
            $data->where('pvc.VendorStatus','=',0);
            $data->where('pvc.VendorTestMode','=',1);
        }
        $data->select('pvc.VendorID','pvc.AdminLabel','pvc.CompanyID');
        $data->orderBy('pvc.AdminLabel','ASC');
        $res =$data->get();
        return $res;
    }
    public function SaveMultipleCloneMapping($TargerVendorIDs,$SourceVendorID)
    {
        $data = DB::table('vendor_buyer_mapping')
                ->where('VendorID', '=', $SourceVendorID)
                ->select('*')
                ->get();

        DB::table('vendor_buyer_mapping')->whereIn('VendorID', $TargerVendorIDs)->delete();
        foreach($data as $val)
        {
            foreach($TargerVendorIDs as $TVID)
            {
                $Tier=$val->Tier;
                $BuyerID=$val->BuyerID;
                $BuyerSortOrder=$val->BuyerSortOrder;
                $MappingStatus=$val->MappingStatus;
                $Details=array('VendorID'=>$TVID,
                            'Tier'=>$Tier,
                            'BuyerSortOrder'=>$BuyerSortOrder,
                            'BuyerID'=>$BuyerID,
                            'MappingStatus'=>$MappingStatus
                            );
                $Exist = DB::table('vendor_buyer_mapping')
                        ->select('*')
                        ->where('VendorID',$TVID)
                        ->where('Tier',$Tier)
                        ->where('BuyerID',$BuyerID)
                        ->first();
                if(empty($Exist))
                {
                    DB::table('vendor_buyer_mapping')->insert($Details);
                }
            }
        }
        return true;
    }
    public function DeleteMultipleMappings($MappingIDs)
    {
        $Del=DB::table('vendor_buyer_mapping')->whereIn('ID',$MappingIDs)->delete();
        if($Del)
        {
            return true;
        }
        else
        {
            return false;
        }

    }

    public function GetAllBuyerList($CampaignID,$ShowInactive,$ShowTest)
    {
        if($ShowInactive=='1'){
            $ShowInactive = '0';
        } else {
            $ShowInactive = '1';
        }

        $Q1 =  DB::table('partner_buyer_campaigns as pbc')
                    ->select('pc.CompanyID','pc.CompanyName')
                    ->distinct()
                    ->join('partner_companies as pc', 'pbc.CompanyID','=','pc.CompanyID')
                    ->where('pbc.CampaignID',$CampaignID)
                    // ->where('pbc.BuyerStatus','=',$ShowInactive)
                    // ->where('pbc.BuyerTestMode','=',$ShowTest)
                    ->where('pc.CompanyStatus','=',$ShowInactive)
                    ->where('pc.CompanyTestMode','=',$ShowTest)
                    ->orderBy('pc.CompanyName','ASC')
                    ->get();
        //print_r($Q1);exit();
        return $Q1;
        
    }

    public function GetMappedVendor($CampaignID,$VendorCompanyID,$BuyerCID) {

        $Q1 = DB::table('partner_buyer_campaigns as pbc')
            ->select('pbc.BuyerID')
            ->where('pbc.CompanyID',$BuyerCID)
            ->where('pbc.BuyerStatus','1')
            ->get();
        $A1 = array();
        foreach ($Q1 as $key => $value) {
            $A1[] = $value->BuyerID;
        }
        
        $Q2 =  DB::table('vendor_buyer_mapping as vbm')
                ->select('pvc.VendorID','pvc.AdminLabel','pvc.CompanyID')
                ->distinct()
                ->join('partner_vendor_campaigns as pvc', 'vbm.VendorID','=','pvc.VendorID')
                ->where('pvc.CampaignID',$CampaignID)
                ->whereIn('vbm.BuyerID',$A1);
        if($VendorCompanyID!="All") {
                $Q2->where('pvc.CompanyID',$VendorCompanyID);
        }
        $Q2->orderBy('pvc.AdminLabel','ASC');
        $return = $Q2
        ->get();
        // $Q1 =  DB::table('vendor_buyer_mapping as vbm')
        //         ->select('pvc.VendorID','pvc.AdminLabel','pvc.CompanyID')
        //         ->distinct()
        //         ->join('partner_vendor_campaigns as pvc', 'vbm.VendorID','=','pvc.VendorID')
        //         ->where('pvc.CampaignID',$CampaignID)
        //         ->where('vbm.BuyerID','=',$BuyerID);
        // if($VendorCompanyID!="All") {
        //         $Q1->where('pvc.CompanyID',$VendorCompanyID);
        // }
        // $Q1->orderBy('pvc.AdminLabel','ASC');
        // $return = $Q1
        // ->get();
        return $return;
    }
}
