<?php
namespace App\Http\Models;
use Illuminate\Database\Eloquent\Model;
use DB;
use Session;
class AffiliateReportModel extends Model 
{
	public function CampaignDropDown()
	{
		$CampaignDropDown = DB::table('master_campaigns')
					->select('CampaignID','CampaignName')
					->where('CampaignID', '!=','0' )
					->where('MaintenanceMode', '!=','1' )
                    ->orderBy('CampaignName','ASC')
					->get();

	    return $CampaignDropDown;
	}	

	public function GetCampaignDetails($CampaignID)
	{
		$CampaignInfo = DB::table('master_campaigns')
					->select('CampaignID','CampaignName','TablePrefix','CampaignType')
					->where('CampaignID', $CampaignID )
					->where('CampaignID','!=','0')
					->where('MaintenanceMode','!=','1')
					->first();
    	return $CampaignInfo;
	}
    
    public function UserDropDownWithCompanyID($CompanyID)
    {
        $User=array();
        if($CompanyID!="")
        {
            $User=DB::table('partner_users')
                    ->select('UserID','UserFirstName','UserLastName')
                    ->where('CompanyID',$CompanyID)
                    ->get();
        }         
        return $User;   
    }

  	public function GetVendorCommissionReport($Search)
    {
        $StartDate  = $Search['StartDate'];
        $EndDate    = $Search['EndDate'];
        $UserID     = $Search['UserID'];
        $CompanyID  = $Search['VendorCompanyID'];
        $CampaignID  = $Search['CampaignID'];

        //DB::enableQueryLog();

        $Campaigns=$this->GetCampaignDetails($CampaignID);
        $x_lead_data = $Campaigns->TablePrefix."_lead_data AS ld";
        $GetVendorCommission = DB::table($x_lead_data)
                        ->select('pc.CompanyName',
                            DB::raw('CONCAT(pu.UserFirstName," ",pu.UserLastName," (UID:",pu.UserID,")") AS UserName'),
                            DB::raw('COUNT(*) AS NumLeads'),
                            DB::raw('SUM(ld.VendorRefPayout) AS VendorRefPayout')
                            )
                            ->join('partner_vendor_campaigns as pvc', 'pvc.VendorID', '=', 'ld.VendorID')
                            ->join('partner_companies as pc', 'pc.CompanyID', '=', 'pvc.CompanyID')
                            ->join('partner_users as pu', 'pu.UserID', '=', 'ld.VendorRefUserID');                          
            if($Campaigns->CampaignType=="DirectPost"){
                $GetVendorCommission->whereBetween('ld.DirectPostTime', array($StartDate, $EndDate));
            }else{
                $GetVendorCommission->whereBetween('ld.PingTime', array($StartDate, $EndDate));
                $GetVendorCommission->whereBetween('ld.PostTime', array($StartDate, $EndDate));
            } 
            if($CompanyID=="" && $UserID=="")
            {
                $GetVendorCommission->where('ld.VendorRefUserID','!=',0);   
            }
            if($CompanyID!="" && $UserID=="")
            {
                $GetVendorCommission->where('ld.VendorRefUserID','!=',0);
            }
            if($CompanyID!="" && $UserID!="All")
            {
                $GetVendorCommission->where('ld.VendorRefUserID','=',$UserID);  
            }
                            
        $result = $GetVendorCommission->groupBy('pc.CompanyName','UserName')
                            ->orderBy('pc.CompanyName','UserName')
                            ->get();
        //$queries = DB::getQueryLog();
        //dd($queries);                    
        return $result;
       
    }
    public function GetBuyerCommissionReport($Search)
    {
        $StartDate  = $Search['StartDate'];
        $EndDate    = $Search['EndDate'];
        $UserID     = $Search['UserID'];
        $CompanyID  = $Search['VendorCompanyID'];
        $CampaignID  = $Search['CampaignID'];

        $Campaigns=$this->GetCampaignDetails($CampaignID);
        $x_lead_data = $Campaigns->TablePrefix."_lead_data AS ld";
        $GetBuyerCommission = DB::table($x_lead_data)
                            ->select('pc.CompanyName',
                                DB::raw('CONCAT(pu.UserFirstName," ",pu.UserLastName," (UID:",pu.UserID,")") AS UserName'),
                                DB::raw('COUNT(*) AS NumLeads'),
                                DB::raw('SUM(ld.BuyerRefPayout) AS BuyerRefPayout'))
                            ->join('partner_buyer_campaigns as pbc', 'pbc.BuyerID', '=', 'ld.BuyerID')
                            ->join('partner_companies as pc', 'pc.CompanyID', '=', 'pbc.CompanyID')
                            ->join('partner_users as pu', 'pu.UserID', '=', 'ld.BuyerRefUserID');
            if($Campaigns->CampaignType=="DirectPost"){
                $GetBuyerCommission->whereBetween('ld.DirectPostTime', array($StartDate, $EndDate));
            }else{
                $GetBuyerCommission->whereBetween('ld.PingTime', array($StartDate, $EndDate));
                $GetBuyerCommission->whereBetween('ld.PostTime', array($StartDate, $EndDate));
            } 
            
            if($CompanyID=="" && $UserID=="")
            {
                $GetBuyerCommission->where('ld.BuyerRefUserID','!=',0); 
            }
            if($CompanyID!="" && $UserID=="")
            {
                $GetBuyerCommission->where('ld.BuyerRefUserID','!=',0);
            }
            if($CompanyID!="" && $UserID!="All")
            {
                $GetBuyerCommission->where('ld.BuyerRefUserID','=',$UserID);    
            }
            $result = $GetBuyerCommission->groupBy('pc.CompanyName','UserName')
                                ->orderBy('pc.CompanyName','UserName')
                                ->get();
        
        return $result;
    }

    public function GetCommissionSummary($Search){
        $StartDate  = $Search['StartDate'];
        $EndDate    = $Search['EndDate'];
        $UserID     = $Search['UserID'];
        $CompanyID  = $Search['VendorCompanyID'];
        $CampaignID  = $Search['CampaignID'];

        $Campaigns=$this->GetCampaignDetails($CampaignID);
        $x_lead_data = $Campaigns->TablePrefix."_lead_data AS ld";

        $Query = "SELECT 
                    SUM(NumLeads) as NumLeads,
                    SUM(VendorRefPayout) as VendorRefPayout,
                    SUM(GrossRevenue) as GrossRevenue,
                    SUM(GrossCOGS) as GrossCOGS,
                    SUM(TotalReturnedRevenue) as TotalReturnedRevenue,
                    SUM(TotalReturnedCOGS) as TotalReturnedCOGS
                    FROM (
                        SELECT
                            COUNT(*) AS NumLeads,
                        SUM(ld.VendorRefPayout) AS VendorRefPayout,
                        SUM(
                            IF(
                                ld.LeadStatus != '2',
                                ld.BuyerLeadPrice,
                                0
                            )
                        ) AS GrossRevenue,
                        SUM(
                            IF(
                                ld.LeadStatus != '2',
                                ld.VendorLeadPrice,
                                0
                            )
                        ) AS GrossCOGS,
                        SUM(
                            IF(
                                ld.LeadStatus = '2',
                                rld.BuyerLeadPrice,
                                0
                            )
                        ) AS TotalReturnedRevenue,
                        SUM(
                            IF(
                                ld.LeadStatus = '2',
                                rld.VendorLeadPrice,
                                0
                            )
                        ) AS TotalReturnedCOGS
                        FROM
                        $x_lead_data
                        INNER JOIN `partner_vendor_campaigns` AS `pvc`
                        ON `pvc`.`VendorID` = `ld`.`VendorID`
                        INNER JOIN `partner_companies` AS `pc`
                        ON `pc`.`CompanyID` = `pvc`.`CompanyID`
                        INNER JOIN `partner_users` AS `pu`
                        ON `pu`.`UserID` = `ld`.`VendorRefUserID`
                        LEFT JOIN `returned_lead_details` AS `rld`
                        ON `ld`.`LeadID` = `rld`.`LeadID`";
                        if($Campaigns->CampaignType=="DirectPost"){
                            $Query.= "  WHERE `ld`.`DirectPostTime` 
                                        BETWEEN '".$StartDate."' 
                                        AND '".$EndDate."'";
                        } else {
                             $Query.= " WHERE `ld`.`PingTime` 
                                        BETWEEN '".$StartDate."' AND '".$EndDate."' 
                                        AND `ld`.`PostTime` 
                                        BETWEEN '".$StartDate."' AND '".$EndDate."'";
                        }

                        if($CompanyID=="" && $UserID=="")
                        {
                            $Query.=" AND `ld`.`VendorRefUserID` != 0";
                        }
                        if($CompanyID!="" && $UserID=="")
                        {
                            $Query.=" AND `ld`.`VendorRefUserID` != 0";
                        }
                        if($CompanyID!="" && $UserID!="All")
                        {
                            $Query.=" AND `ld`.`VendorRefUserID` = '".$UserID."'";  
                        }
        $Query.="   UNION 
                        (SELECT
                        COUNT(*) AS NumLeads,
                        SUM(ld.VendorRefPayout) AS VendorRefPayout,
                        SUM(
                            IF(
                                ld.LeadStatus != 2,
                                ld.BuyerLeadPrice,
                                0
                            )
                        ) AS GrossRevenue,
                        SUM(
                            IF(
                                ld.LeadStatus != 2,
                                ld.VendorLeadPrice,
                                0
                            )
                        ) AS GrossCOGS,
                        SUM(
                            IF(
                                ld.LeadStatus = 2,
                                rld.BuyerLeadPrice,
                                0
                            )
                        ) AS TotalReturnedRevenue,
                        SUM(
                            IF(
                                ld.LeadStatus = 2,
                                rld.VendorLeadPrice,
                                0
                            )
                        ) AS TotalReturnedCOGS
                        FROM
                        $x_lead_data 
                        INNER JOIN `partner_buyer_campaigns` AS `pbc`
                        ON `pbc`.`BuyerID` = `ld`.`BuyerID`
                        INNER JOIN `partner_companies` AS `pc`
                        ON `pc`.`CompanyID` = `pbc`.`CompanyID`
                        INNER JOIN `partner_users` AS `pu`
                        ON `pu`.`UserID` = `ld`.`BuyerRefUserID`
                        LEFT JOIN `returned_lead_details` AS `rld`
                        ON `ld`.`LeadID` = `rld`.`LeadID`";

                        if($Campaigns->CampaignType=="DirectPost"){
                            $Query.= "  WHERE `ld`.`DirectPostTime` 
                                        BETWEEN '".$StartDate."' 
                                        AND '".$EndDate."'";
                        } else {
                             $Query.= " WHERE `ld`.`PingTime` 
                                        BETWEEN '".$StartDate."' AND '".$EndDate."' 
                                        AND `ld`.`PostTime` 
                                        BETWEEN '".$StartDate."' AND '".$EndDate."'";
                        }

                        if($CompanyID=="" && $UserID=="")
                        {
                            $Query.=" AND `ld`.`BuyerRefUserID` != 0";
                        }
                        if($CompanyID!="" && $UserID=="")
                        {
                            $Query.=" AND `ld`.`BuyerRefUserID` != 0";
                        }
                        if($CompanyID!="" && $UserID!="All")
                        {
                            $Query.=" AND `ld`.`BuyerRefUserID` = '".$UserID."'";  
                        }
                    $Query.=" )) AS Ans 
                    ";
            //echo $Query;exit();        
            $data =  DB::select($Query);
            return $data;
    }

}
