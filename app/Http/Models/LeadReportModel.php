<?php
namespace App\Http\Models;
use Illuminate\Database\Eloquent\Model;
use DB;
use Session;
class LeadReportModel extends Model 
{	
	public function GetCampaignDetails($CampaignID)
	{
		$CampaignInfo = DB::table('master_campaigns')
					->select('CampaignID','CampaignName','TablePrefix','CampaignType')
					->where('CampaignID', $CampaignID )
					->where('CampaignID','!=','0')
					->where('MaintenanceMode','!=','1')
					->first();
    	return $CampaignInfo;
	}
	public function GetAllLeadReport($Search)
	{
		$Leads['Count'] = "";
		$Leads['Res'] 	= "";
		$CampaignID 	= $Search['CampaignID'];
		$CompanyID 		= $Search['CompanyID'];
		$VendorCompanyID 	= $Search['VendorCompanyID'];
		$VendorID 			= $Search['VendorID'];
		$BuyerCompanyID 	= $Search['BuyerCompanyID'];
		$BuyerID 			= $Search['BuyerID'];
		$Redirected 	= $Search['Redirected'];
		$Status 		= $Search['Status'];
		$PingPostStatus = $Search['PingPostStatus'];

		$SubID 			= $Search['SubID'];
		$Email 			= $Search['Email'];
		$LeadID 		= $Search['LeadID'];

		$StartDate 		= date('Y-m-d',strtotime($Search['StartDate'])).' 00:00:00';
		$EndDate 		= date('Y-m-d',strtotime($Search['EndDate'])).' 23:59:59';

		$Start 			= $Search['Start'];
		$NumOfRecords 	= $Search['NumOfRecords'];
		
		$Campaigns = $this->GetCampaignDetails($CampaignID);

		if(!empty($Campaigns))
		{

			//DB::enableQueryLog();
			$api_data_inbound 			= "api_data_inbound as adi";
			$partner_vendor_campaigns 	= "partner_vendor_campaigns as pvc";
			$partner_buyer_campaigns 	= "partner_buyer_campaigns as pbc";
			$x_lead_data 				= $Campaigns->TablePrefix."_lead_data as ld";
			$x_outbound_requests 		= $Campaigns->TablePrefix."_outbound_requests as or";
			$returned_lead_details 		= "returned_lead_details as rld";
			$GetProcessedLeads 			= DB::table(DB::raw($api_data_inbound))
											->select('adi.ID','adi.LeadID','adi.CampaignID','ld.Email',
											'ld.Action','ld.Zip','adi.VendorID','ld.BuyerID',
											'ld.VendorLeadPrice','ld.BuyerLeadPrice',
											'pvc.CompanyID as VendorCompanyID',
											'pbc.CompanyID as BuyerCompanyID',
											'pvc.AdminLabel as VendorName',
											'pbc.AdminLabel as BuyerName',
											'adi.DirectPostTime as DirectPostTime',
											'adi.PingTime as PingTime',
											'adi.PostTime as PostTime'
											);
			if($Status == '0_2' || $Status == '1_2' ){
				$GetProcessedLeads->addSelect('rld.VendorLeadPrice as ReturnedVendorPrice');
				$GetProcessedLeads->addSelect('rld.BuyerLeadPrice as ReturnedBuyerPrice');
			}

			if($Campaigns->CampaignType=="DirectPost"){
				$GetProcessedLeads->addSelect('adi.DirectPostResponse','adi.DirectPostDate')
								  ->addSelect('ld.IsRedirected','ld.IsRedirectedTime');
			}
			else
			{
				$GetProcessedLeads->addSelect('adi.PingResponse','adi.PingDate');
				$GetProcessedLeads->addSelect('adi.PostResponse','adi.PostDate');
			}

			$GetProcessedLeads->leftjoin($x_lead_data, 'adi.LeadID', '=', 'ld.LeadID')
								->leftjoin($x_outbound_requests, 'adi.LeadID', '=', 'or.LeadID')
								->leftjoin($partner_vendor_campaigns, 'adi.VendorID', '=', 'pvc.VendorID')
								->leftjoin($partner_buyer_campaigns, 'or.BuyerID', '=', 'pbc.BuyerID');

			if($Status == '0_2' || $Status == '1_2' )
			{
				$GetProcessedLeads->leftjoin($returned_lead_details, 'or.LeadID','=','rld.LeadID');
			}

				$GetProcessedLeads->where('adi.CampaignID','=',$CampaignID)
								  ->where('adi.TestMode','=','1')
								  ->where('adi.LeadID','!=','0');
			
			if($Campaigns->CampaignType=="DirectPost")
			{
				$GetProcessedLeads->whereBetween('adi.DirectPostDate', array($StartDate, $EndDate));
			}
			else
			{
				$GetProcessedLeads->whereBetween('adi.PingDate', array($StartDate, $EndDate));
			}

			if($Campaigns->CampaignType=="DirectPost")
			{
				if($Redirected!="" && $Redirected!="All"){
					$GetProcessedLeads->where('ld.IsRedirected','=',$Redirected);
				}
			}

			if ($VendorCompanyID != 'All'){
				$GetProcessedLeads->where('pvc.CompanyID','=',$VendorCompanyID);
			}
			if ($VendorID != 'All'){
				$GetProcessedLeads->where('pvc.VendorID','=',$VendorID);
			}
			if ($BuyerCompanyID != 'All'){
				$GetProcessedLeads->where('pbc.CompanyID','=',$BuyerCompanyID);
			}
			if ($BuyerID != 'All'){
				$GetProcessedLeads->where('pbc.BuyerID','=',$BuyerID);
			}

			if($Status == '0'){
				$GetProcessedLeads->where('adi.TestMode','=','0');
			}else if($Status == '0_1'){
				$GetProcessedLeads->where('adi.TestMode','=','0');
				$GetProcessedLeads->where('ld.LeadStatus','=','1');
			}else if($Status == '0_0'){
				$GetProcessedLeads->where('adi.TestMode','=','0');
				$GetProcessedLeads->where('ld.LeadStatus','=','0');
			}else if($Status == '1'){
				$GetProcessedLeads->where('adi.TestMode','=','1');
			}else if($Status == '1_1'){
				$GetProcessedLeads->where('adi.TestMode','=','1');
				$GetProcessedLeads->where('ld.LeadStatus','=','1');				
			}else if($Status == '1_0'){
				$GetProcessedLeads->where('adi.TestMode','=','1');
				$GetProcessedLeads->where('ld.LeadStatus','=','0');
			}else if($Status == '1_2'){
				$GetProcessedLeads->where('adi.TestMode','=','1');
				$GetProcessedLeads->where('ld.LeadStatus','=','2');
			}else if($Status == '0_2'){
				$GetProcessedLeads->where('adi.TestMode','=','0');
				$GetProcessedLeads->where('ld.LeadStatus','=','2');
			}
			if($Campaigns->CampaignType!="DirectPost")
			{
				if($PingPostStatus!="")
				{
					$GetProcessedLeads->where('ld.Action','=',$PingPostStatus);
				}
			}

			if($SubID!="")
			{
				$GetProcessedLeads->where('ld.SubID', $SubID);
			}
			if($LeadID!="")
			{
				$GetProcessedLeads->where('ld.LeadID', $LeadID);
			}
			if($Email!="")
			{
				$GetProcessedLeads->where('ld.Email', $Email);
			}

	        $Leads['Count'] = count($GetProcessedLeads->groupBy('ld.LeadID')->get());
		
        	$Leads['Res'] = $GetProcessedLeads        	
						->groupBy('adi.LeadID')
						->orderBy('adi.LeadID','DESC')
						->offset($Start)
	            		->limit($NumOfRecords)
		                ->get()
		                ->toArray();
    		//$queries = DB::getQueryLog();
			//dd($queries);
		}
		return $Leads;
  	}

  	public function GetSummaryLeadsReport($Search)
	{
		$Leads =array();
		$CampaignID 	= $Search['CampaignID'];
		$CompanyID 		= $Search['CompanyID'];
		$VendorCompanyID 	= $Search['VendorCompanyID'];
		$VendorID 			= $Search['VendorID'];
		$BuyerCompanyID 	= $Search['BuyerCompanyID'];
		$BuyerID 			= $Search['BuyerID'];
		$Redirected 	= $Search['Redirected'];
		$Status 		= $Search['Status'];
		$PingPostStatus = $Search['PingPostStatus'];

		$SubID 			= $Search['SubID'];
		$Email 			= $Search['Email'];
		$LeadID 		= $Search['LeadID'];

		$StartDate 		= date('Y-m-d',strtotime($Search['StartDate'])).' 00:00:00';
		$EndDate 		= date('Y-m-d',strtotime($Search['EndDate'])).' 23:59:59';
		
		$Campaigns = $this->GetCampaignDetails($CampaignID);

		if(!empty($Campaigns))
		{
			//DB::enableQueryLog();
			$api_data_inbound 			= "api_data_inbound as adi";
			$partner_vendor_campaigns 	= "partner_vendor_campaigns as pvc";			
			$partner_buyer_campaigns 	= "partner_buyer_campaigns as pbc";
			$x_lead_data 				= $Campaigns->TablePrefix."_lead_data as ld";
			$x_outbound_requests 		= $Campaigns->TablePrefix."_outbound_requests as or";
			$returned_lead_details 		= "returned_lead_details as rld";
			$GetProcessedLeads = DB::table(DB::raw($api_data_inbound))
									->select(DB::raw("COUNT(*) AS Total"),
										DB::raw('SUM(ld.VendorLeadPrice) AS TotalVendorPrice'),
										DB::raw('SUM(ld.BuyerLeadPrice) AS TotalBuyerPrice'))
									->leftjoin($x_lead_data, 'ld.LeadID', '=', 'adi.LeadID')
									->leftjoin($x_outbound_requests, 'adi.LeadID', '=', 'or.LeadID')
									->leftjoin($partner_vendor_campaigns, 'adi.VendorID', '=', 'pvc.VendorID')
									->leftjoin($partner_buyer_campaigns, 'or.BuyerID', '=', 'pbc.BuyerID');


			if($Status == '0_2' || $Status == '1_2'){
					$GetProcessedLeads->leftjoin($returned_lead_details, 'or.LeadID','=','rld.LeadID')
									  ->addSelect(DB::raw('SUM(rld.VendorLeadPrice) AS ReturnedVendorPrice'),
								  				 DB::raw('SUM(rld.BuyerLeadPrice) AS ReturnedBuyerPrice'));
			}

			$GetProcessedLeads->where('adi.CampaignID','=',$CampaignID)
								  ->where('adi.TestMode','=','1')
								  ->where('adi.LeadID','!=','0');

			$GetProcessedLeads->addSelect(
				DB::raw("SUM(IF(ld.LeadStatus='1',1,0)) AS Accepted"),
				DB::raw("SUM(IF(ld.LeadStatus='0',1,0)) AS Rejected"),
				DB::raw("SUM(IF(ld.LeadStatus='2',1,0)) AS Returned"));
			if($Campaigns->CampaignType=="DirectPost"){
				$GetProcessedLeads->addSelect(DB::raw("SUM(IF(ld.IsRedirected='1',1,0)) AS Redirected"));
			}

			if($Campaigns->CampaignType=="DirectPost"){
				if($Redirected!="" && $Redirected!="All"){
					$GetProcessedLeads->where('ld.IsRedirected','=',$Redirected);;
				}
			}
			if($Campaigns->CampaignType=="DirectPost")
			{
				$GetProcessedLeads->whereBetween('adi.DirectPostDate', array($StartDate, $EndDate));
			}
			else
			{
				$GetProcessedLeads->whereBetween('adi.PingDate', array($StartDate, $EndDate));
			}
			if ($VendorCompanyID != 'All'){
				$GetProcessedLeads->where('pvc.CompanyID','=',$VendorCompanyID);
			}
			if ($VendorID != 'All'){
				$GetProcessedLeads->where('pvc.VendorID','=',$VendorID);
			}
			if ($BuyerCompanyID != 'All'){
				$GetProcessedLeads->where('pbc.CompanyID','=',$BuyerCompanyID);
			}
			if ($BuyerID != 'All'){
				$GetProcessedLeads->where('pbc.BuyerID','=',$BuyerID);
			}
			if($Status == '0'){
				$GetProcessedLeads->where('adi.TestMode','=','0');
			}else if($Status == '0_1'){
				$GetProcessedLeads->where('adi.TestMode','=','0');
				$GetProcessedLeads->where('ld.LeadStatus','=','1');
			}else if($Status == '0_0'){
				$GetProcessedLeads->where('adi.TestMode','=','0');
				$GetProcessedLeads->where('ld.LeadStatus','=','0');
			}else if($Status == '1'){
				$GetProcessedLeads->where('adi.TestMode','=','1');
			}else if($Status == '1_1'){
				$GetProcessedLeads->where('adi.TestMode','=','1');
				$GetProcessedLeads->where('ld.LeadStatus','=','1');								
			}else if($Status == '1_0'){
				$GetProcessedLeads->where('adi.TestMode','=','1');
				$GetProcessedLeads->where('ld.LeadStatus','=','0');
			}else if($Status == '1_2'){
				$GetProcessedLeads->where('adi.TestMode','=','1');
				$GetProcessedLeads->where('ld.LeadStatus','=','2');
			}else if($Status == '0_2'){
				$GetProcessedLeads->where('adi.TestMode','=','0');
				$GetProcessedLeads->where('ld.LeadStatus','=','2');
			}
			if($Campaigns->CampaignType!="DirectPost")
			{
				if($PingPostStatus!="")
				{
					$GetProcessedLeads->where('ld.Action','=',$PingPostStatus);
				}
			}
			if($SubID!="")
			{
				$GetProcessedLeads->where('ld.SubID', $SubID);
			}
			if($LeadID!="")
			{
				$GetProcessedLeads->where('ld.LeadID', $LeadID);
			}
			if($Email!="")
			{
				$GetProcessedLeads->where('ld.Email', $Email);
			}
			$Leads = $GetProcessedLeads->limit(1)->first();
	       	//$queries = DB::getQueryLog();
			//dd($queries);    	  
		}
		return $Leads;
  	}

  	public function LeadInboundDetails($RequestID)
	{
		return $LeadInboundDetails = DB::table('api_data_inbound as adi')
								->select('adi.*','pvc.*')
								->where('ID', '=',$RequestID)
								->leftjoin('partner_vendor_campaigns as pvc','adi.VendorID','=','pvc.VendorID')
		    					->first();
	}

	public function LeadOutboundDetails($LeadID,$CampaignID)
	{
		$CompanyID = Session::get('user_companyID');
		$CampaignDetails = DB::table('master_campaigns')
								->select('CampaignID','CampaignName','CampaignType','TablePrefix')
								->where('CampaignID', '=',$CampaignID)
		    					->first();
		$x_outbound_requests = $CampaignDetails->TablePrefix.'_outbound_requests as ors';
		return $LeadOutboundDetails = DB::table($x_outbound_requests)
								->select('ors.*','pbc.*')
								->where('LeadID', '=',$LeadID)
								->leftjoin('partner_buyer_campaigns as pbc','ors.BuyerID','=','pbc.BuyerID')
								->where('pbc.CompanyID', '=',$CompanyID)
		    					->first();
	}

	public function BuyerAttemptedDetails($LeadID,$CampaignID)
	{
		$CampaignDetails = $this->GetCampaignDetails($CampaignID);
		$LeadTable = $CampaignDetails->TablePrefix.'_lead_data as ld';
		$RequestTable = $CampaignDetails->TablePrefix.'_outbound_requests as or';
		$BuyerAttemptedDetails = DB::table($LeadTable)
		        				->join($RequestTable, 'ld.LeadID', '=', 'or.LeadID')
		        				->join('partner_buyer_campaigns', 'or.BuyerID', '=', 'partner_buyer_campaigns.BuyerID')
								->where('ld.LeadID', '=',$LeadID );
								if($CampaignID != 1) {
									$BuyerAttemptedDetails->orderBy('partner_buyer_campaigns.AdminLabel', 'ASC');
								}
		    				 	$query = $BuyerAttemptedDetails->get();

		    				  	return $query;
	}
	public function getLeadDetails($lead_id,$CampaignID)
	{
		$CampaignDetails = $this->GetCampaignDetails($CampaignID);
		$TableName = $CampaignDetails->TablePrefix.'_lead_data';
		return $getVendorInfo = DB::table($TableName)
						->where('LeadID', '=',$lead_id )
    					->first();
	}
	public function GetTotalTime($lead_id,$CampaignID)
	{
		$CampaignDetails = $this->GetCampaignDetails($CampaignID);
		$TableName = $CampaignDetails->TablePrefix.'_outbound_requests';

	 	$query = DB::table($TableName);

	 	if($CampaignDetails->CampaignType=="DirectPost")
	 	{
	 		$query->addSelect(DB::raw('SUM(DirectPostTime) AS DirectPostTime'));
	 	}
	 	else
	 	{
	 		$query->addSelect(DB::raw('SUM(PostTime) as PostTime'));
	 		$query->addSelect(DB::raw('SUM(PingTime) as PingTime'));
	 	}

		$query->where('LeadID', '=',$lead_id);

		return $getVendorInfo = $query->first();

	}
	public static function getBuyerCampaign()
	{
		$getBuyer = DB::table('partner_buyer_campaigns')
    				->get()
        			->toArray();

        $buyerArray = array();
        foreach ($getBuyer as $key => $value)
        {
        	$buyerArray[$value->BuyerID] = $value->PartnerLabel.' ('. $value->BuyerID .')';
        }

        return $buyerArray;
	}

	public function GetReturnModalUsingLeadID($LeadID,$CampaignID)
	{
			$Campaigns = $this->GetCampaignDetails($CampaignID);
			$x_lead_data 				= $Campaigns->TablePrefix."_lead_data ld";

			$GetReturnModalUsingLeadID 	= DB::table(DB::raw($x_lead_data))
											->select('ld.*','pvc.ReturnAPIURL','pbc.PartnerLabel as BuyerName','pvc.PartnerLabel as VendorName')
											->join('partner_buyer_campaigns as pbc', 'ld.BuyerID', '=', 'pbc.BuyerID')
											->join('partner_vendor_campaigns as pvc', 'ld.VendorID', '=', 'pvc.VendorID')
											->where('ld.LeadID','=',$LeadID)
											->get()
		                					->first();
			return $GetReturnModalUsingLeadID;     
	}

	public function AddReturnCode($Details) {
        $LeadID = $Details['LeadID'];
        $CampaignID = $Details['CampaignID'];
        $Add = DB::table('returned_lead_details')->insert($Details);
        if ($Add) {
            $this->UpdateXLeadData($Details,$CampaignID);
            $this->UpdateXOutboundRequests($LeadID,$CampaignID);
            $this->UpdateXInboundRequests($LeadID,$CampaignID);
            return true;
        } else {
            return false;
        }
    }
    public function UpdateXLeadData($data, $CampaignID) {
        $CampaignDetails = $this->GetCampaignDetails($CampaignID);
        $Table = $CampaignDetails->TablePrefix . "_lead_data";
        $Details = array('VendorLeadPrice' => 0.00,
            'BuyerLeadPrice' => 0.00,
            'BuyerID' => $data['BuyerID'],
            'LeadStatus' => 2,
            'VendorRefPayout' => 0.00,
            'BuyerRefPayout' => 0.00);
        DB::table($Table)->where('LeadID', $data['LeadID'])->update($Details);
        return true;
    }    
    public function UpdateXOutboundRequests($LeadID,$CampaignID) {
        $CampaignDetails = $this->GetCampaignDetails($CampaignID);
        $Table = $CampaignDetails->TablePrefix . "_outbound_requests";
        if($CampaignID == '1')
            $Details = array('DirectPostStatus' => '2',
                            'DirectPostResponse' => 'Lead Returned');
        else
            $Details = array('PostStatus' => '2',
                            'PostResponse' => 'Lead Returned');
        DB::table($Table)->where('LeadID', $LeadID)->update($Details);
        return true;
    }
    public function UpdateXInboundRequests($LeadID,$CampaignID)
    {
        $Table = "api_data_inbound";
        if($CampaignID == '1')
            $Details = array('DirectPostStatus' => '2',
                            'DirectPostResponse' => 'Lead Returned');
        else
            $Details = array('PostStatus' => '2',
                            'PostResponse' => 'Lead Returned');
        DB::table($Table)->where('LeadID', $LeadID)->update($Details);
        return true;
    }

   
    //////////////////////////////////////////////////////////
    public function GetAllViewPurchasedLeadsAdmin($Search)
	{
		$Leads['Count'] = "";
		$Leads['Res'] 	= "";
		$CampaignID 	= $Search['CampaignID'];
		$CompanyID 		= $Search['CompanyID'];
		$BuyerID 		= $Search['BuyerID'];
		$Redirected 	= $Search['Redirected'];
		$Status 		= $Search['Status'];
		$PingPostStatus = $Search['PingPostStatus'];

		$SubID 			= $Search['SubID'];
		$Email 			= $Search['Email'];
		$LeadID 		= $Search['LeadID'];

		$StartDate 		= date('Y-m-d',strtotime($Search['StartDate'])).' 00:00:00';
		$EndDate 		= date('Y-m-d',strtotime($Search['EndDate'])).' 23:59:59';

		$Start 			= $Search['Start'];
		$NumOfRecords 	= $Search['NumOfRecords'];
		
		$Campaigns = $this->GetCampaignDetails($CampaignID);

		if(!empty($Campaigns))
		{
			//DB::enableQueryLog();
			$partner_buyer_campaigns 	= "partner_buyer_campaigns as pbc";
			$api_data_inbound 			= "api_data_inbound as adi";
			$x_lead_data 				= $Campaigns->TablePrefix."_lead_data as ld";
			$x_outbound_requests 		= $Campaigns->TablePrefix."_outbound_requests as ors";
			$returned_lead_details 		= "returned_lead_details as rld";
			$GetProcessedLeads 			= DB::table(DB::raw($x_outbound_requests))
											->select('ors.ID','ors.LeadID','ld.Email',
											'ld.Action','ld.Zip','ors.BuyerID','ld.BuyerLeadPrice',
											'pbc.CompanyID','pbc.PartnerLabel as BuyerName'
											);
			if($Status == '0_2' || $Status == '1_2' ){
				$GetProcessedLeads->addSelect('rld.BuyerLeadPrice as ReturnedBuyerPrice');
			}

			if($Campaigns->CampaignType=="DirectPost"){
			$GetProcessedLeads->addSelect('ors.DirectPostResponse','ors.DirectPostDate','ors.DirectPostTime')
								->addSelect('ld.IsRedirected','ld.IsRedirectedTime');
								
											
											
			}
			else
			{
				$GetProcessedLeads->addSelect('ors.PingResponse','ors.PingDate','ors.PingTime');
				$GetProcessedLeads->addSelect('ors.PostResponse','ors.PostDate','ors.PostTime');
				
			}

			$GetProcessedLeads->leftjoin($x_lead_data, 'ors.LeadID', '=', 'ld.LeadID')
								->leftjoin($partner_buyer_campaigns, 'ors.BuyerID', '=', 'pbc.BuyerID')
								->leftjoin($api_data_inbound, 'ld.LeadID', '=', 'adi.LeadID');
			if($Status == '0_2' || $Status == '1_2' ){
				$GetProcessedLeads->leftjoin($returned_lead_details, 'ld.LeadID','=','rld.LeadID');
			}

				$GetProcessedLeads->where('adi.CampaignID','=',$CampaignID)
								  ->where('pbc.CompanyID','=',$CompanyID)
								  ->where('adi.LeadID','!=','0')
								  ->where('adi.TestMode','=','0');
			
			if($Campaigns->CampaignType=="DirectPost")
			{
				$GetProcessedLeads->whereBetween('ors.DirectPostDate', array($StartDate, $EndDate));
			}
			else
			{
				$GetProcessedLeads->whereBetween('ors.PingDate', array($StartDate, $EndDate));
			}
			if($Campaigns->CampaignType=="DirectPost")
			{
				if($Redirected!="" && $Redirected!="All"){
					$GetProcessedLeads->where('ld.IsRedirected','=',$Redirected);
				}
			}
			if ($BuyerID != 'All' ){
				$GetProcessedLeads->where('pbc.BuyerID','=',$BuyerID);
			}
			if($Status == '0'){
				$GetProcessedLeads->where('adi.TestMode','=','0');
			}else if($Status == '0_1'){
				$GetProcessedLeads->where('adi.TestMode','=','0');
				$GetProcessedLeads->where('ld.LeadStatus','=','1');
			}else if($Status == '0_0'){
				$GetProcessedLeads->where('adi.TestMode','=','0');
				$GetProcessedLeads->where('ld.LeadStatus','=','0');
			}else if($Status == '1'){
				$GetProcessedLeads->where('adi.TestMode','=','1');
			}else if($Status == '1_1'){
				$GetProcessedLeads->where('adi.TestMode','=','1');
				$GetProcessedLeads->where('ld.LeadStatus','=','1');				
			}else if($Status == '1_0'){
				$GetProcessedLeads->where('adi.TestMode','=','1');
				$GetProcessedLeads->where('ld.LeadStatus','=','0');
			}else if($Status == '1_2'){
				$GetProcessedLeads->where('adi.TestMode','=','1');
				$GetProcessedLeads->where('ld.LeadStatus','=','2');
			}else if($Status == '0_2'){
				$GetProcessedLeads->where('adi.TestMode','=','0');
				$GetProcessedLeads->where('ld.LeadStatus','=','2');
			}
			if($Campaigns->CampaignType!="DirectPost")
			{
				if($PingPostStatus!="")
				{
					$GetProcessedLeads->where('ld.Action','=',$PingPostStatus);
				}
			}

			if($SubID!="")
			{
				$GetProcessedLeads->where('ld.SubID', $SubID);
			}
			if($LeadID!="")
			{
				$GetProcessedLeads->where('ld.LeadID', $LeadID);
			}
			if($Email!="")
			{
				$GetProcessedLeads->where('ld.Email', $Email);
			}

	        $Leads['Count'] = $GetProcessedLeads->count();
		
        	$Leads['Res'] = $GetProcessedLeads
        				->groupBy('ors.LeadID')
						->orderBy('adi.LeadID','DESC')
						->offset($Start)
	            		->limit($NumOfRecords)
		                ->get()
		                ->toArray();
    		//$queries = DB::getQueryLog();
			//dd($queries);
		}
		return $Leads;
  	}

  	public function GetSummaryViewPurchasedLeadsAdmin($Search)
	{
		$Leads =array();
		$CampaignID 	= $Search['CampaignID'];
		$CompanyID 		= $Search['CompanyID'];
		$BuyerID 		= $Search['BuyerID'];
		$Redirected 	= $Search['Redirected'];
		$Status 		= $Search['Status'];
		$PingPostStatus = $Search['PingPostStatus'];

		$SubID 			= $Search['SubID'];
		$Email 			= $Search['Email'];
		$LeadID 		= $Search['LeadID'];

		$StartDate 		= date('Y-m-d',strtotime($Search['StartDate'])).' 00:00:00';
		$EndDate 		= date('Y-m-d',strtotime($Search['EndDate'])).' 23:59:59';
		
		$Campaigns = $this->GetCampaignDetails($CampaignID);

		if(!empty($Campaigns))
		{
			//DB::enableQueryLog();
			/*$partner_buyer_campaigns 	= "partner_buyer_campaigns as pbc";
			$api_data_inbound 			= "api_data_inbound as adi";
			$x_lead_data 				= $Campaigns->TablePrefix."_lead_data as ld";
			$x_outbound_requests 		= $Campaigns->TablePrefix."_outbound_requests as ors";
			$returned_lead_details 		= "returned_lead_details as rld";
			$GetProcessedLeads 			= DB::table(DB::raw($x_outbound_requests))
											->select(DB::raw('COUNT(*) AS Total'));
			if($Campaigns->CampaignType=="DirectPost")
			{
				$GetProcessedLeads->addSelect(DB::raw('SUM(ors.DirectPostPrice) AS TotalBuyerPrice'),
											DB::raw('SUM(IF(ors.DirectPostStatus="1",1,0)) AS Accepted'),
											DB::raw('SUM(IF(ors.DirectPostStatus="0",1,0)) AS Rejected'),
											DB::raw('SUM(IF(ors.DirectPostStatus="2",1,0)) AS Returned '));
			}
			else
			{
				if($PingPostStatus!="")
				{
					if($PingPostStatus=="Ping")
					{
						$GetProcessedLeads->addSelect(DB::raw('SUM(ors.PingPrice) AS TotalBuyerPrice'),
													DB::raw('SUM(IF(ors.PingStatus="1",1,0)) AS Accepted'),
													DB::raw('SUM(IF(ors.PingStatus="0",1,0)) AS Rejected'),
													DB::raw('SUM(IF(ors.PingStatus="2",1,0)) AS Returned '));
					}
					else if($PingPostStatus=="Post")
					{
						$GetProcessedLeads->addSelect(DB::raw('SUM(ors.PostPrice) AS TotalBuyerPrice'),
													DB::raw('SUM(IF(ors.PostStatus="1",1,0)) AS Accepted'),
													DB::raw('SUM(IF(ors.PostStatus="0",1,0)) AS Rejected'),
													DB::raw('SUM(IF(ors.PostStatus="2",1,0)) AS Returned '));
					}
					
				}
				else
				{
					$GetProcessedLeads->addSelect(DB::raw('SUM(ors.PingPrice) AS TotalBuyerPrice'),
													DB::raw('SUM(IF(ors.PostStatus="1",1,0)) AS Accepted'),
													DB::raw('SUM(IF(ors.PingStatus="0",1,0)) AS Rejected'),
													DB::raw('SUM(IF(ors.PostStatus="2",1,0)) AS Returned'));
				}

				
			}
			$GetProcessedLeads->leftjoin($x_lead_data, 'ors.LeadID', '=', 'ld.LeadID')
								->leftjoin($partner_buyer_campaigns, 'ors.BuyerID', '=', 'pbc.BuyerID')
								->leftjoin($api_data_inbound, 'ld.LeadID', '=', 'adi.LeadID');
			
			$GetProcessedLeads->where('adi.CampaignID','=',$CampaignID)
							  ->where('pbc.CompanyID','=',$CompanyID)
							  ->where('adi.LeadID','!=','0')
							  ->where('adi.TestMode','=','0');

			if($Campaigns->CampaignType=="DirectPost")
			{
				$GetProcessedLeads->whereBetween('ors.DirectPostDate', array($StartDate, $EndDate));
			}
			else
			{
				$GetProcessedLeads->whereBetween('ors.PingDate', array($StartDate, $EndDate));
			}
			if($Campaigns->CampaignType=="DirectPost")
			{
				if($Redirected!="" && $Redirected!="All"){
					$GetProcessedLeads->where('ld.IsRedirected','=',$Redirected);
				}
			}
			if ($BuyerID != 'All' ){
				$GetProcessedLeads->where('pbc.BuyerID','=',$BuyerID);
			}
			if($Status == '0'){
				$GetProcessedLeads->where('adi.TestMode','=','0');
			}else if($Status == '0_1'){
				$GetProcessedLeads->where('adi.TestMode','=','0');
				$GetProcessedLeads->where('ld.LeadStatus','=','1');
			}else if($Status == '0_0'){
				$GetProcessedLeads->where('adi.TestMode','=','0');
				$GetProcessedLeads->where('ld.LeadStatus','=','0');
			}else if($Status == '1'){
				$GetProcessedLeads->where('adi.TestMode','=','1');
			}else if($Status == '1_1'){
				$GetProcessedLeads->where('adi.TestMode','=','1');
				$GetProcessedLeads->where('ld.LeadStatus','=','1');				
			}else if($Status == '1_0'){
				$GetProcessedLeads->where('adi.`','=','1');
				$GetProcessedLeads->where('ld.LeadStatus','=','0');
			}else if($Status == '1_2'){
				$GetProcessedLeads->where('adi.TestMode','=','1');
				$GetProcessedLeads->where('ld.LeadStatus','=','2');
			}else if($Status == '0_2'){
				$GetProcessedLeads->where('adi.TestMode','=','0');
				$GetProcessedLeads->where('ld.LeadStatus','=','2');
			}
			if($Campaigns->CampaignType!="DirectPost")
			{
				if($PingPostStatus!="")
				{
					$GetProcessedLeads->where('ld.Action','=',$PingPostStatus);
				}
			}

			if($SubID!="")
			{
				$GetProcessedLeads->where('ld.SubID', $SubID);
			}
			if($LeadID!="")
			{
				$GetProcessedLeads->where('ld.LeadID', $LeadID);
			}
			if($Email!="")
			{
				$GetProcessedLeads->where('ld.Email', $Email);
			}

			$DashboardBuyerReport = $GetProcessedLeads->first();*/
			//$queries = DB::getQueryLog();
			//dd($queries);

			//DB::enableQueryLog();
			$CampaignID     = $Campaigns->CampaignID; 
			$CampaignType   = $Campaigns->CampaignType;
			$TablePrefix    = $Campaigns->TablePrefix;
			$api_data_inbound = "api_data_inbound as adi";
			$partner_vendor_campaigns = "partner_buyer_campaigns as pbc";
			$x_lead_data = $TablePrefix."_lead_data as ld";
			$x_outbound_requests = $TablePrefix."_outbound_requests as ors";
			$GetProcessedLeads = DB::table(DB::raw($x_outbound_requests))
			                          ->select(DB::raw("COUNT(DISTINCT(ors.LeadID)) AS Total"))
			                          ->leftjoin($x_lead_data, 'ors.LeadID', '=', 'ld.LeadID')
			                          ->leftjoin($partner_vendor_campaigns, 'ors.BuyerID', '=', 'pbc.BuyerID')
			                          ->leftjoin($api_data_inbound, 'ors.LeadID', '=', 'adi.LeadID')
			                          ->where('adi.CampaignID','=',$CampaignID)   
			                          ->where('pbc.CompanyID','=',$CompanyID)    
			                          ->where('adi.LeadID','!=','0')   
			                          ->where('adi.TestMode','=','0'); 

			if($Campaigns->CampaignType=="DirectPost")
			{
			  $GetProcessedLeads->addSelect(DB::raw('SUM(ors.DirectPostPrice) AS TotalBuyerPrice'),
			                DB::raw('SUM(IF(ors.DirectPostStatus="1",1,0)) AS Accepted'),
			                DB::raw('SUM(IF(ors.DirectPostStatus="2",1,0)) AS Returned '));
			}
			else
			{
			  if($PingPostStatus!="")
			  {
			    if($PingPostStatus=="Ping")
			    {
			      $GetProcessedLeads->addSelect(DB::raw('SUM(ors.PingPrice) AS TotalBuyerPrice'),
			                    DB::raw('SUM(IF(ors.PingStatus="1",1,0)) AS Accepted'),
			                    DB::raw('SUM(IF(ors.PingStatus="2",1,0)) AS Returned '));
			    }
			    else if($PingPostStatus=="Post")
			    {
			      $GetProcessedLeads->addSelect(DB::raw('SUM(ors.PostPrice) AS TotalBuyerPrice'),
			                    DB::raw('SUM(IF(ors.PostStatus="1",1,0)) AS Accepted'),
			                    DB::raw('SUM(IF(ors.PostStatus="2",1,0)) AS Returned '));
			    }
			    
			  }
			  else
			  {
			    $GetProcessedLeads->addSelect(DB::raw('SUM(ors.PingPrice) AS TotalBuyerPrice'),
			                    DB::raw('SUM(IF(ors.PostStatus="1",1,0)) AS Accepted'),
			                    DB::raw('SUM(IF(ors.PostStatus="2",1,0)) AS Returned'));
			  }

			  
			}

			if($CampaignType=="DirectPost")
			{
			$GetProcessedLeads->whereBetween('ors.DirectPostDate', array($StartDate, $EndDate));
			}
			else
			{
			$GetProcessedLeads->whereBetween('ors.PingDate', array($StartDate, $EndDate));
			} 
			if($Campaigns->CampaignType=="DirectPost")
			{
			  if($Redirected!="" && $Redirected!="All"){
			    $GetProcessedLeads->where('ld.IsRedirected','=',$Redirected);
			  }
			}
			      
			if ($BuyerID != 'All' ){
			  $GetProcessedLeads->where('pbc.BuyerID','=',$BuyerID);
			}
			if($Status == '0'){
			  $GetProcessedLeads->where('adi.TestMode','=','0');
			}else if($Status == '0_1'){
			  $GetProcessedLeads->where('adi.TestMode','=','0');
			  $GetProcessedLeads->where('ld.LeadStatus','=','1');
			}else if($Status == '0_0'){
			  $GetProcessedLeads->where('adi.TestMode','=','0');
			  $GetProcessedLeads->where('ld.LeadStatus','=','0');
			}else if($Status == '1'){
			  $GetProcessedLeads->where('adi.TestMode','=','1');
			}else if($Status == '1_1'){
			  $GetProcessedLeads->where('adi.TestMode','=','1');
			  $GetProcessedLeads->where('ld.LeadStatus','=','1');       
			}else if($Status == '1_0'){
			  $GetProcessedLeads->where('adi.`','=','1');
			  $GetProcessedLeads->where('ld.LeadStatus','=','0');
			}else if($Status == '1_2'){
			  $GetProcessedLeads->where('adi.TestMode','=','1');
			  $GetProcessedLeads->where('ld.LeadStatus','=','2');
			}else if($Status == '0_2'){
			  $GetProcessedLeads->where('adi.TestMode','=','0');
			  $GetProcessedLeads->where('ld.LeadStatus','=','2');
			}
			if($Campaigns->CampaignType!="DirectPost")
			{
			  if($PingPostStatus!="")
			  {
			    $GetProcessedLeads->where('ld.Action','=',$PingPostStatus);
			  }
			}

			if($SubID!="")
			{
			  $GetProcessedLeads->where('ld.SubID', $SubID);
			}
			if($LeadID!="")
			{
			  $GetProcessedLeads->where('ld.LeadID', $LeadID);
			}
			if($Email!="")
			{
			  $GetProcessedLeads->where('ld.Email', $Email);
			}

			$DashboardBuyerReport = $GetProcessedLeads->first();
			//$queries = DB::getQueryLog();
			//dd($queries);
		}
		return $DashboardBuyerReport;
  	}


  	public function BuyerAttemptedList($LeadID,$CampaignID)
	{
		$CampaignDetails = $this->GetCampaignDetails($CampaignID);
		$LeadTable = $CampaignDetails->TablePrefix.'_lead_data as ld';
		$RequestTable = $CampaignDetails->TablePrefix.'_outbound_requests as or';
		$BuyerAttemptedDetails = DB::table($LeadTable)
								->select('or.*','ld.*','pbc.AdminLabel','pbc.BuyerID')
		        				->join($RequestTable, 'ld.LeadID', '=', 'or.LeadID')
		        				->join('partner_buyer_campaigns as pbc', 'or.BuyerID', '=', 'pbc.BuyerID')
								->where('ld.LeadID', '=',$LeadID );
								if($CampaignID != 1) {
									$BuyerAttemptedDetails->orderBy('pbc.AdminLabel', 'ASC');
								}
		    				 	$query = $BuyerAttemptedDetails->get();

		    				  	return $query;
	}
   
}
