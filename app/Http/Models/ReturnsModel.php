<?php
namespace App\Http\Models;
use Illuminate\Database\Eloquent\Model;
use DB;
use Session;
class ReturnsModel extends Model 
{
	public function CampaignDropDown()
	{
		$CampaignDropDown = DB::table('master_campaigns')
					->select('CampaignID','CampaignName')
					->where('CampaignID', '!=','0' )
					->where('MaintenanceMode', '!=','1' )
                    ->orderBy('CampaignName','ASC')
					->get();

	    return $CampaignDropDown;
	}

	public function GetReturnCodeList($CampaignID) {
        $data = DB::table("master_return_codes")
                ->select('*')
                ->where('CampaignID', $CampaignID)
                ->orWhere('CampaignID', 0)
                ->get();
        return $data;
    }
    public function SaveFileName($FileDetails) {
        $FileNo = DB::table('uploaded_files_details')->insertGetId($FileDetails);
        return $FileNo;
    }

    public function AddReturnLeadDetails($Details) {
        $Add = DB::table('uploaded_returns_details')->insert($Details);
        return true;
    }

    public function AddReturnLeadData($Details) {
        $LeadID = $Details['LeadID'];
        $CampaignID = $Details['CampaignID'];
        $Add = DB::table('returned_lead_details')->insert($Details);
        if ($Add) {
            $this->UpdateXLeadData($Details,$CampaignID);
            $this->UpdateXOutboundRequests($LeadID,$CampaignID);
            $this->UpdateXInboundRequests($LeadID,$CampaignID);
            return true;
        } else {
            return false;
        }
    }

     public function UpdateXInboundRequests($LeadID,$CampaignID)
    {
        $Table = "api_data_inbound";
        if($CampaignID == '1')
            $Details = array('DirectPostStatus' => '2',
                            'DirectPostResponse' => 'Lead Returned');
        else
            $Details = array('PostStatus' => '2',
                            'PostResponse' => 'Lead Returned');
        DB::table($Table)->where('LeadID', $LeadID)->update($Details);
        return true;
    }

    public function UpdateXLeadData($data, $CampaignID) {
        $CampaignDetails = $this->GetCampaignDetails($CampaignID);
        $Table = $CampaignDetails->TablePrefix . "_lead_data";
        $Details = array('VendorLeadPrice' => 0.00,
            'BuyerLeadPrice' => 0.00,
            'BuyerID' => $data['BuyerID'],
            'LeadStatus' => 2,
            'VendorRefPayout' => 0.00,
            'BuyerRefPayout' => 0.00);
        DB::table($Table)->where('LeadID', $data['LeadID'])->update($Details);
        return true;
    }

    public function UpdateXOutboundRequests($LeadID,$CampaignID) {
        $CampaignDetails = $this->GetCampaignDetails($CampaignID);
        $Table = $CampaignDetails->TablePrefix . "_outbound_requests";
        if($CampaignID == '1')
            $Details = array('DirectPostStatus' => '2',
                            'DirectPostResponse' => 'Lead Returned');
        else
            $Details = array('PostStatus' => '2',
                            'PostResponse' => 'Lead Returned');
        DB::table($Table)->where('LeadID', $LeadID)->update($Details);
        return true;
    }

     public function GetLeadDataWithLeadID($CampaignID,$LeadID, $startDate='', $endDate='',$CompanyID='') {
        
        $startDate1 = $startDate.' 00:00:00';
        $endDate1 = $endDate." 23:59:59";
        
        $CampaignDetails = $this->GetCampaignDetails($CampaignID);
        $Table = $CampaignDetails->TablePrefix . "_lead_data as cald";
        $data = DB::table($Table)
                ->select('cald.VendorID','cald.LeadID', 'cald.VendorLeadPrice', 'cald.BuyerID', 'cald.BuyerLeadPrice', 'pvc.CompanyID', 'cald.BuyerRefPayout', 'cald.VendorRefPayout','pvc.ReturnAPIURL'
                )
                ->join('partner_buyer_campaigns as pbc', 'cald.BuyerID', '=', 'pbc.BuyerID')
                ->join('partner_vendor_campaigns as pvc', 'cald.VendorID', '=', 'pvc.VendorID')
                ->where('cald.LeadStatus','1')
                ->where('cald.LeadID', $LeadID);
        if($CompanyID!="")
        {
            $data->where('pbc.CompanyID',$CompanyID);
        }
        if($startDate!='' && $CampaignDetails->CampaignID == '1')
        {
            $data->whereBetween('cald.DirectPostTime', array($startDate1, $endDate1));
        }
        else if($startDate!='' && $CampaignDetails->CampaignID != '1')
        { 
            $data->whereBetween('cald.PostTime', array($startDate1, $endDate1));
        }
        $LeadData = $data
        ->first();
        return $LeadData;
    }

    public function GetLeadDataWithLeadEmail($CampaignID, $LeadEmail,$startDate='',$endDate='',$CompanyID='') {
       
        $startDate1 = $startDate.' 00:00:00';
        $endDate1 = $endDate." 23:59:59";
       
        $CampaignDetails = $this->GetCampaignDetails($CampaignID);
        $Table = $CampaignDetails->TablePrefix . "_lead_data as cald";
        $data = DB::table($Table)
                ->select('cald.VendorID','cald.LeadID', 'cald.VendorLeadPrice', 'cald.BuyerID', 'cald.BuyerLeadPrice', 'pvc.CompanyID', 'cald.BuyerRefPayout', 'cald.VendorRefPayout','pvc.ReturnAPIURL'
                )
                ->join('partner_buyer_campaigns as pbc', 'cald.BuyerID', '=', 'pbc.BuyerID')
                ->join('partner_vendor_campaigns as pvc', 'cald.VendorID', '=', 'pvc.VendorID')
                ->where('cald.LeadStatus','1')
                ->where('cald.Email', $LeadEmail);
        if($CompanyID!="")
        {
            $data->where('pbc.CompanyID',$CompanyID);
        }
        if($startDate!='' && $CampaignDetails->CampaignID == '1')
        {
            $data->whereBetween('cald.DirectPostTime', array($startDate1, $endDate1));
        }
        elseif($startDate!='' && $CampaignDetails->CampaignID != '1')
        {            
            $data->whereBetween('cald.PostTime', array($startDate1, $endDate1));
        }
        $LeadData = $data->first();
        return $LeadData;
    }
    public function GetLeadDataWithLeadIDLeadEmail($CampaignID, $LeadID, $LeadEmail, $startDate='', $endDate='', $CompanyID='') {
        
        $startDate1 = $startDate.' 00:00:00';
        $endDate1 = $endDate." 23:59:59";
        
        $CampaignDetails = $this->GetCampaignDetails($CampaignID);
        $Table = $CampaignDetails->TablePrefix . "_lead_data as cald";
        $data = DB::table($Table)
                ->select('cald.VendorID','cald.LeadID', 'cald.VendorLeadPrice', 'cald.BuyerID', 'cald.BuyerLeadPrice', 'pvc.CompanyID', 'cald.BuyerRefPayout', 'cald.VendorRefPayout','pvc.ReturnAPIURL'
                )
                ->join('partner_buyer_campaigns as pbc', 'cald.BuyerID', '=', 'pbc.BuyerID')
                ->join('partner_vendor_campaigns as pvc', 'cald.VendorID', '=', 'pvc.VendorID')
                ->where('cald.LeadStatus','1')
                ->where('cald.LeadID', $LeadID)
                ->where('cald.Email', $LeadEmail);
        if($CompanyID!=""){
            $data->where('pbc.CompanyID',$CompanyID);
        }
        if($startDate!='' && $CampaignDetails->CampaignID == '1')
        {
            $data->whereBetween('cald.DirectPostTime', array($startDate1, $endDate1));
        }
        elseif($startDate!='' && $CampaignDetails->CampaignID != '1')
        {
            $data->whereBetween('cald.PostTime', array($startDate1, $endDate1));            
        }
        $LeadData = $data->first();
        return $LeadData;
    }
     public function GetCampaignDetails($CampaignID) {
        $CampaignInfo = DB::table('master_campaigns')
                ->select('CampaignID', 'CampaignName', 'TablePrefix', 'CampaignType')
                ->where('CampaignID', $CampaignID)
                ->where('CampaignID', '!=', '0')
                ->where('MaintenanceMode', '!=', '1')
                ->first();
        return $CampaignInfo;
    }


    public function UploadedFilesData($Search) 
    {     
        $Start = $Search['Start'];
        $NumOfRecords =  $Search['NumOfRecords'];
        $FilesData = DB::table("uploaded_files_details as ufd")
            ->select('ufd.*','urd.CampaignID','urd.BuyerCompanyID')
            ->join('uploaded_returns_details as urd', 'urd.FileNO', '=', 'ufd.FileNO');                    
               
        $Users['Count'] = $FilesData->groupBy('urd.FileNo')->count();
        $Users['Res'] = $FilesData
                    ->groupBy('urd.FileNo')
                    ->orderBy('ufd.UploadedDate', 'DESC') 
                    ->offset($Start)
                    ->limit($NumOfRecords)
                    ->get()
                    ->toArray();
               
        return $Users;
    }

    public function TotalRow($FileNo) {
        $data = DB::table("uploaded_returns_details")
                ->where('FileNo', $FileNo)
                ->get();
        return count($data);
    }

    public function AcceptedOrRejectedRow($FileNo, $Status) {
        $data = DB::table("uploaded_returns_details")
                ->where('FileNo', $FileNo)
                ->where('Status', $Status)
                ->get();
        return count($data);
    }

    public function GetBuyerCompanyDetails($BCID){
        $Q1 = DB::table('partner_companies')
            ->select('CompanyName','CompanyID')
            ->where('CompanyID',$BCID)
            ->first();
        return $Q1;
    }
    public function DownloadUploadedReturns($FileNo) {
        $data = DB::table("uploaded_returns_details")
                ->where('FileNo', $FileNo)
                ->get();
        return $data;
    }
     public function GetFileName($FileNo) {
        $data = DB::table("uploaded_files_details")
                ->where('FileNo', $FileNo)
                ->first();
        return $data->FileName;
    }
    ////////////////////////////////////
    public function GetExportDate($Search) 
    {
        $userRole = Session::get('user_role');
        $companyID = Session::get('user_companyID');
        $userID = Session::get('user_id');

        $campaignID = $Search['CampaignID'];
        $startDate = $Search['startDate'];
        $endDate = $Search['endDate'];
        $start = $Search['start'];
        $numofrecords = $Search['numofrecords'];
        $VendorCompany = $Search['VendorCompany'];

        $CampaignDetails = $this->GetCampaignDetails($campaignID);
        $LeadDataTable = $CampaignDetails->TablePrefix . "_lead_data";
        
        if($campaignID == '1') {
            $dateBetween = 'pld.DirectPostTime';
        } else {
            $dateBetween = 'pld.PostTime';
        }
        
        $GetExportReport = DB::table('returned_lead_details as rld')
                ->select('rld.*', 'mrc.ReturnReason', 'mc.CampaignName as Campaign','pld.LeadDateTime as LeadDateTime','pld.DirectPostTime','pld.PostTime',DB::raw('CONCAT(pvc.AdminLabel," (VID:",rld.VendorID,")") AS Vendor'),
                    DB::raw('CONCAT(pc.CompanyName," (CID:",rld.CompanyID,")") AS Company')
                )
                ->join('master_return_codes as mrc', 'mrc.ReturnCode', '=', 'rld.ReturnCode')
                ->join('master_campaigns as mc', 'mc.CampaignID', '=', 'rld.CampaignID')
                ->join($LeadDataTable.' as pld', 'pld.LeadID','=','rld.LeadID')
                ->join('partner_vendor_campaigns as pvc','rld.VendorID','=','pvc.VendorID')
                ->join('partner_companies as pc','rld.CompanyID','=','pc.CompanyID')
                ->where('pld.LeadStatus','=','2')
                ->where('rld.CampaignID', '=', $campaignID);
                
                if($VendorCompany!=""){
                    $GetExportReport->where('pvc.CompanyID','=',$VendorCompany);
                }

                if($userRole!="1"){
                    $GetExportReport->where('pvc.CompanyID','=',$companyID);
                }
        $GetExportReport->whereBetween($dateBetween, array($startDate, $endDate));
                
       
          
        $Users['Count'] = $GetExportReport->count();

        $Users['Res'] = $GetExportReport
                    ->orderBy($dateBetween, 'DESC') 
                    ->offset($start)
                    ->limit($numofrecords)
                    ->get()
                    ->toArray();   
        
        return $Users;
    }
    public function GetConsumerEmail($CampaignID, $LeadID) {
        $Res = DB::table('master_campaigns')
                ->select('TablePrefix')
                ->where('CampaignID', $CampaignID)
                ->first();
        $FileName = $Res->TablePrefix . '_lead_data';
        $Data = DB::table($FileName)
                ->select('Email')
                ->where('LeadID', $LeadID)
                ->first();
        return $Data->Email;
    }

    public function GetExportReportAll($Search) {
        $userRole = Session::get('user_role');
        $companyID = Session::get('user_companyID');
        $userID = Session::get('user_id');

        $campaignID = $Search['CampaignID'];
        $startDate = $Search['startDate'];
        $endDate = $Search['endDate'];
        $VendorCompany = $Search['VendorCompany'];

        $CampaignDetails = $this->GetCampaignDetails($campaignID);
        $LeadDataTable = $CampaignDetails->TablePrefix . "_lead_data";

        if($campaignID == '1') {
            $dateBetween = 'pld.DirectPostTime';
        } else {
            $dateBetween = 'pld.PostTime';
        }

        $GetExportReport = DB::table('returned_lead_details as rld')
                ->select('rld.*', 'mrc.ReturnReason', 'mc.CampaignName as Campaign', 'pld.LeadDateTime as LeadDateTime',
                    DB::raw('CONCAT(pvc.AdminLabel," (VID:",rld.VendorID,")") AS Vendor'),
                    DB::raw('CONCAT(pc.CompanyName," (CID:",rld.CompanyID,")") AS Company')
                )
                ->join('master_return_codes as mrc', 'mrc.ReturnCode', '=', 'rld.ReturnCode')
                ->join('master_campaigns as mc', 'mc.CampaignID', '=', 'rld.CampaignID')
                ->join($LeadDataTable.' as pld', 'pld.LeadID','=','rld.LeadID')
                ->join('partner_vendor_campaigns as pvc','rld.VendorID','=','pvc.VendorID')
                ->join('partner_companies as pc','rld.CompanyID','=','pc.CompanyID')
                ->where('rld.CampaignID', '=', $campaignID);
        
        if($VendorCompany!='0'){
            $GetExportReport->where('pvc.CompanyID','=',$VendorCompany);
        }

        if($userRole!="1"){
            $GetExportReport->where('pvc.CompanyID','=',$companyID);
        }
                //->whereBetween('rld.DateTime', array($startDate, $endDate))
        $GetExportReport->whereBetween($dateBetween, array($startDate, $endDate))
                //->orderBy('rld.DateTime', 'DESC')
                ->orderBy($dateBetween, 'DESC');
        $response = $GetExportReport->get();
        return $response;
    }

    ////////////////////////////////////
    public function GetEmailExportReturnCSV($Search) 
    {
        $userRole = Session::get('user_role');
        $companyID = Session::get('user_companyID');
        $userID = Session::get('user_id');

        $campaignID = $Search['CampaignID'];
        $startDate = $Search['startDate'];
        $endDate = $Search['endDate'];
        $VendorCompany = $Search['VendorCompany'];

        $CampaignDetails = $this->GetCampaignDetails($campaignID);
        $LeadDataTable = $CampaignDetails->TablePrefix . "_lead_data";
        
        if($campaignID == '1') {
            $dateBetween = 'pld.DirectPostTime';
        } else {
            $dateBetween = 'pld.PostTime';
        }
        
        $GetExportReport = DB::table('returned_lead_details as rld')
                ->select('rld.*', 'mrc.ReturnReason', 'mc.CampaignName as Campaign','pld.LeadDateTime as LeadDateTime','pld.DirectPostTime','pld.PostTime',DB::raw('CONCAT(pvc.AdminLabel," (VID:",rld.VendorID,")") AS Vendor'),
                    DB::raw('CONCAT(pc.CompanyName," (CID:",rld.CompanyID,")") AS Company')
                )
                ->join('master_return_codes as mrc', 'mrc.ReturnCode', '=', 'rld.ReturnCode')
                ->join('master_campaigns as mc', 'mc.CampaignID', '=', 'rld.CampaignID')
                ->join($LeadDataTable.' as pld', 'pld.LeadID','=','rld.LeadID')
                ->join('partner_vendor_campaigns as pvc','rld.VendorID','=','pvc.VendorID')
                ->join('partner_companies as pc','rld.CompanyID','=','pc.CompanyID')
                ->where('pld.LeadStatus','=','2')
                ->where('rld.CampaignID', '=', $campaignID);
                
                if($VendorCompany!=""){
                    $GetExportReport->where('pvc.CompanyID','=',$VendorCompany);
                }

                if($userRole!="1"){
                    $GetExportReport->where('pvc.CompanyID','=',$companyID);
                }
        $GetExportReport->whereBetween($dateBetween, array($startDate, $endDate));
                
       

        $Users  = $GetExportReport
                    ->orderBy($dateBetween, 'DESC') 
                    ->get()
                    ->toArray();   
        
        return $Users;
    }
}

